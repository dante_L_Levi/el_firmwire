#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "QDebug"

#define TIME_Request         35
SerialPort_crc8 *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
int16_t index_request=1;
Autopilot_Logic *mainData;

typedef enum
{

    Update_BoardName=3,
    Update_Firmware=4,

    Update__DigIn=5,
    Update__deltes=6,
    Update__Omega=7,
    Update__mOmega=8,
    Update__YPT3=9,
    Update__YPT4=10,
    Update__YPT5=11,
    Update__YPT13=12,
    Update__YPT14=13,
    Update__Outs=14,
    Update__duu_v=15,
    Update__dlu_n=16


}Update_step_UI;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Load_Styles();
    mainData=new Autopilot_Logic();
    Init_StartBox_Element();
    mainData->Start_Init_Model();
     ui->Index_POT->addItems(mainData->dataPOT);
     ui->Index_POT_2->addItems(mainData->dataPOT);
    connect(mainData->serial_Data, SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this, SLOT(Get_Read_Data_Serial(QByteArray)));

}


void MainWindow:: Load_Styles(void)
{
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();

    palette.setColor(ui->label->foregroundRole(), Qt::white);
     ui->label->setPalette(palette);

     palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
      ui->label_2->setPalette(palette2);

      ui->groupBox_2->setForegroundRole(QPalette::Light);
}


void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
        const auto infos = QSerialPortInfo::availablePorts();
                for (const QSerialPortInfo &info : infos)
                {
                    ui->NameSerialPort_Box->addItem(info.portName());
                }



}


void MainWindow:: Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);

        RequestReadWrite->setInterval(TIME_Request);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));


    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow:: ParseRX(uint8_t cmd)
{
    switch(cmd)
    {
       case Update_BoardName:
        {
           mainData->Update_UI(cmd, ui->lbl_Name);
           break;
        }

    case Update_Firmware:
     {
        mainData->Update_UI(cmd, ui->lbl_Firmw);
        break;
     }

    case Update__DigIn:
     {
       mainData->Update_UI(ui->_otd,ui->_razor_dlu,ui->_srez_cnt,ui->_on_ctrl,ui->_ready);

        break;
     }

    case Update__deltes:
     {
        mainData->Update_UI(cmd, ui->_AIN_delta1,ui->_AIN_delta2);

        break;
     }
    case Update__Omega:
     {
        mainData->Update_UI(cmd, ui->_AIN_Om1,ui->_AIN_Om2);
        break;
     }
    case Update__mOmega:
     {
        mainData->Update_UI(cmd, ui->_AIN_mOm1,ui->_AIN_mOm2);
        break;
     }
    case Update__YPT3:
     {
        mainData->Update_UI(cmd, ui->_AIN_YPT3_I,ui->_AIN_YPT3_II);
        break;
     }
    case Update__YPT4:
     {
        mainData->Update_UI(cmd, ui->_AIN_YPT4_I,ui->_AIN_YPT4_II);
        break;
     }
    case Update__YPT5:
     {
        mainData->Update_UI(cmd, ui->_AIN_YPT5_I,ui->_AIN_YPT5_II);
        break;
     }
    case Update__YPT13:
     {
        mainData->Update_UI(cmd, ui->_AIN_YPT13_I,ui->_AIN_YPT13_II);
        break;
     }
    case Update__YPT14:
     {
        mainData->Update_UI(cmd, ui->_AIN_YPT14_I,ui->_AIN_YPT14_II);
        break;
     }
    case Update__Outs:
     {
        mainData->Update_UI(cmd, ui->_AIN_out1,ui->_AIN_out2);
        break;
     }
    case Update__duu_v:
     {
         mainData->Update_UI(cmd, ui->_AIN_duu_v1,ui->_AIN_duu_v2);
        break;
     }
    case Update__dlu_n:
     {
        mainData->Update_UI(cmd, ui->_AIN_dlu_n1,ui->_AIN_dlu_n2);
        break;
     }

    }
}


void MainWindow:: Get_Read_Data_Serial(QByteArray data)
{
    qDebug()<<data;
    uint8_t status_update=mainData->Read_Model_Status_PayLoad(data);
    status_update&=0x7F;
    ParseRX(status_update);
}





void MainWindow::UPDATE_UI_TimRequest(void)
{
    mainData->Update_UI_ID(ui->label_ID);
    if(index_request>16)
    {
       index_request=1;
       return;
    }
    mainData->Request_Board((Parse_data_Type)index_request);
    index_request++;

}

//todo : add update port name
void MainWindow::on_Name_update_btn_clicked()
{
    ui->SpeedSerialPort_Box->clear();
    ui->NameSerialPort_Box->clear();
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
        const auto infos = QSerialPortInfo::availablePorts();
                for (const QSerialPortInfo &info : infos)
                {
                    ui->NameSerialPort_Box->addItem(info.portName());
                }
}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
             count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connected");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);

        bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                 (ui->SpeedSerialPort_Box->currentText()).toLong());
        RequestReadWrite=new QTimer();
        if(statusConfig)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");
            qDebug()<<"Port Open!!";
        }

    }

    else
    {
        if(mainData->serial_Data->isOpen())
        {
            mainData->serial_Data->close();
            ui->on_ConnectButton->setText("Disconnected");
            if(RequestReadWrite!=NULL)
                    RequestReadWrite->stop();

        }

        QMessageBox::information(this,"ERROR","None Open!!");
        ui->NameSerialPort_Box->setEnabled(true);
        ui->SpeedSerialPort_Box->setEnabled(true);

    }
}


void MainWindow::on_Start_Request_Btn_clicked()
{
    count_start_Transmit++;
        if(count_start_Transmit%2==0)
        {
            Activate_UI(true);
            mainData->RS_485_OK_Request();
           RequestReadWrite->start();

        }
        else
        {
            Activate_UI(false);


            RequestReadWrite->stop();
        }
}


void MainWindow::on_Btn_Boot_clicked()
{
    Activate_UI(false);
    count_start_Transmit=1;
    mainData->serial_Data->GotoBootLoader();
}








/***************************HUNDLER Digital Potenciometer************************/

void MainWindow::on_btn_set_writePOT_clicked()
{

    mainData->RS485_Request_Write_POTx();
}

void MainWindow::on_POT_Set_clicked()
{
    RequestReadWrite->stop();
    if(ui->Index_POT->currentIndex()>=0)
    {
        mainData->RS_485_SetPOTx((uint8_t)(ui->Index_POT->currentIndex()),
                                 (uint8_t)(ui->spinBox_POTVal->value()));
    }

    RequestReadWrite->start();


}


void MainWindow::on_testBtn_AIN_clicked()
{
    mainData->Request_Board(Get_YPT13);
}


void MainWindow::on_testBtn_DI_clicked()
{
    mainData->Request_Board(Get_DigIn);
}


void MainWindow::on_test_Btn_POTx_clicked()
{

        mainData->RS_485_SetPOTx((uint8_t)(ui->Index_POT_2->currentIndex()),
                                 (uint8_t)(rand()%255));


}

