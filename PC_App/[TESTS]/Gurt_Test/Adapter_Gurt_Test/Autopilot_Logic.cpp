#include "Autopilot_Logic.h"

static volatile const uint8_t _board_id [4] = {0x12,0xF6,'G','T'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;
Model_data_def data_model;

void Autopilot_Logic::Start_Init_Model()
{
    serial_Data=new SerialPort_crc8();
   serial_Data->Set_ID(*rs485_id);
    data_model._id=*rs485_id;


}

uint16_t Autopilot_Logic::Get_Model_ID()
{
    return *rs485_id;
}

bool Autopilot_Logic::Config_Settings(QString Name, long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
         return status;
}



void Autopilot_Logic::Request_Board(Parse_data_Type cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    this->serial_Data->Transmit_Packet((CommandDef)cmd,buffer,MODEL_SIZE);
}



void Autopilot_Logic::Request_TO_MCU(bool status)
{
    isRequest=status;
}

void Autopilot_Logic::Test_GenerateData()
{

}

void Autopilot_Logic::RS_Request_GetFirmwire()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)getFirmware,buffer,MODEL_SIZE);
}

void Autopilot_Logic::RS_Request_GetNameBoard()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)getBoardName,buffer,MODEL_SIZE);

}

void Autopilot_Logic::RS_485_OK_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,MODEL_SIZE);
}

void Autopilot_Logic::RS_485_Close_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)RS485_Disconnect,buffer,MODEL_SIZE);
}

void Autopilot_Logic::RS_485_SetPOTx(uint8_t pot, uint8_t val)
{
    if(pot<8)
    {
        uint8_t buffer[MODEL_SIZE]={0};
        buffer[0]=val;
        this->serial_Data->Transmit_Packet((CommandDef)(pot+17),buffer,MODEL_SIZE);
    }
}

void Autopilot_Logic::RS485_Request_Write_POTx()
{
     uint8_t buffer[MODEL_SIZE]={0};
     this->serial_Data->Transmit_Packet((CommandDef)Set_Write_POT_EEPROM,buffer,MODEL_SIZE);
}

uint8_t Autopilot_Logic::Read_Model_Status_PayLoad(QByteArray data)
{
    QByteArray dataBuffer;
    //other funchtion check summ
    dataBuffer=serial_Data->Get_CHECK_Result_Recieve_Data(data);
    if(dataBuffer!=""&& serial_Data->IsRx_DataReady())
    {
        uint8_t data[MODEL_SIZE]={0};


        if((uint8_t)dataBuffer[0]==_board_id[0]&&(uint8_t)dataBuffer[1]==_board_id[1])
        {


            for(int i=0;i<(int)MODEL_SIZE;i++)
            {
                data[i]=dataBuffer[i+OFFSET_DataPayload];
            }

            uint8_t cmd=(uint8_t)dataBuffer[INDEX_CMD];
            //qDebug()<<"command:"<<cmd;
            cmd&=0x7F;
            Hundler_read_dataModel(cmd,data);

            return cmd;
        }

        return 0xFF;
    }
    else
    {
        return 0xFF;
    }
}

void Autopilot_Logic::Update_UI_ID(QLabel *data1)
{
    data1->setText(QString::number(data_model._id));
}

void Autopilot_Logic::Update_UI(uint8_t status, QLabel *data1)
{
    switch(status)
    {
        case getBoardName:
        {
            data1->setText(data_model.modelName);
            break;
        }

        case getFirmware:
        {
           data1->setText(data_model.firmware);
            break;
        }
    }
}

void Autopilot_Logic::Update_UI(uint8_t status, QLabel *data1,QLabel *data2)
{
    switch(status)
    {

        case Get_deltes:
        {
            data1->setText(QString::number(data_model._analog_val.fields.beta1));
            data2->setText(QString::number(data_model._analog_val.fields.beta2));
            break;
        }

        case Get_Omega:
        {
            data1->setText(QString::number(data_model._analog_val.fields.omega1));
            data2->setText(QString::number(data_model._analog_val.fields.omega2));
            break;
        }

    case Get_mOmega:
    {
        data1->setText(QString::number(data_model._analog_val.fields.m_omega1));
        data2->setText(QString::number(data_model._analog_val.fields.m_omega_2));
        break;
    }
    case Get_YPT3:
    {
        data1->setText(QString::number(data_model._analog_val.fields.upt3_1));
        data2->setText(QString::number(data_model._analog_val.fields.upt3_2));
        break;
    }
    case Get_YPT4:
    {
        data1->setText(QString::number(data_model._analog_val.fields.upt4_1));
        data2->setText(QString::number(data_model._analog_val.fields.upt4_2));
        break;
    }
    case Get_YPT5:
    {
        data1->setText(QString::number(data_model._analog_val.fields.upt5_1));
        data2->setText(QString::number(data_model._analog_val.fields.upt5_2));
        break;
    }
    case Get_YPT13:
    {
        data1->setText(QString::number(data_model._analog_val.fields._1_out_oy13));
        data2->setText(QString::number(data_model._analog_val.fields._2_out_oy13));
        break;
    }
    case Get_YPT14:
    {
        data1->setText(QString::number(data_model._analog_val.fields._1_out_oy14));
        data2->setText(QString::number(data_model._analog_val.fields._2_out_oy14));
        break;
    }
    case Get_Outs:
    {
        data1->setText(QString::number(data_model._analog_val.fields._1_cp1_2));
        data2->setText(QString::number(data_model._analog_val.fields._2_cp1_2));
        break;
    }

    case Get_duu_v:
    {
        data1->setText(QString::number(data_model._analog_val.fields.duu_v1));
        data2->setText(QString::number(data_model._analog_val.fields.duu_v2));
        break;
    }


    case Get_dlu_n:
    {
        data1->setText(QString::number(data_model._analog_val.fields.dlu_n1));
        data2->setText(QString::number(data_model._analog_val.fields.dlu_n2));
        break;
    }

    }
}

void Autopilot_Logic::Update_UI(QCheckBox *ch1, QCheckBox *ch2, QCheckBox *ch3, QCheckBox *ch4, QCheckBox *ch5)
{
    (data_model._input_state.fields.otd!=0)?(ch1->setChecked(true)):ch1->setChecked(false);
    (data_model._input_state.fields.raz_dlu!=0)?(ch2->setChecked(true)):ch2->setChecked(false);
    (data_model._input_state.fields.srez_cnt_inner!=0)?(ch3->setChecked(true)):ch3->setChecked(false);
    (data_model._input_state.fields.on_cnt!=0)?(ch4->setChecked(true)):ch4->setChecked(false);
    (data_model._input_state.fields.ready!=0)?(ch5->setChecked(true)):ch5->setChecked(false);

}

void Autopilot_Logic::Hundler_read_dataModel(uint8_t status_cmd, uint8_t *dt)
{
    switch(status_cmd)
    {
        case getBoardName:
        {
            char boardname[8];
            memcpy(boardname,(void *)dt,sizeof(boardname));
            memcpy(data_model.modelName,(void *)boardname,sizeof (boardname));

            break;
        }

        case getFirmware:
        {
            char _firmwire[8];
            memcpy(_firmwire,(void *)dt,sizeof(_firmwire));
            memcpy(data_model.firmware,_firmwire,sizeof(_firmwire));
            break;
        }

        case Get_DigIn:
        {
            memcpy(&data_model._input_state,dt,sizeof(dig_in_s));
            break;
        }

        case Get_deltes:
        {
        int32_t data[2]={0};
            memcpy(data,dt,sizeof(data));
            data_model._analog_val.fields.beta1=data[0];
            data_model._analog_val.fields.beta2=data[1];
            break;
        }

        case Get_Omega:
        {
            int32_t data[2]={0};
            memcpy(data,dt,sizeof(data));
            data_model._analog_val.fields.omega1=data[0];
            data_model._analog_val.fields.omega2=data[1];
            break;
        }



    case Get_mOmega:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields.m_omega1=data[0];
        data_model._analog_val.fields.m_omega_2=data[1];
        break;
    }
    case Get_YPT3:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields.upt3_1=data[0];
        data_model._analog_val.fields.upt3_2=data[1];
        break;
    }
    case Get_YPT4:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields.upt4_1=data[0];
        data_model._analog_val.fields.upt4_2=data[1];
        break;
    }
    case Get_YPT5:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields.upt5_1=data[0];
        data_model._analog_val.fields.upt5_2=data[1];
        break;
    }
    case Get_YPT13:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields._1_out_oy13=data[0];
        data_model._analog_val.fields._2_out_oy13=data[1];
        break;
    }
    case Get_YPT14:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields._1_out_oy14=data[0];
        data_model._analog_val.fields._2_out_oy14=data[1];
        break;
    }
    case Get_Outs:
    {
        int32_t data[2]={0};
        memcpy(data,dt,sizeof(data));
        data_model._analog_val.fields._1_cp1_2=data[0];
        data_model._analog_val.fields._2_cp1_2=data[1];
        break;
    }
        case Get_duu_v:
        {
            int32_t data[2]={0};
            memcpy(data,dt,sizeof(data));
            data_model._analog_val.fields.duu_v1=data[0];
            data_model._analog_val.fields.duu_v2=data[1];
            break;
        }

        case Get_dlu_n:
        {
            int32_t data[2]={0};
            memcpy(data,dt,sizeof(data));
            data_model._analog_val.fields.dlu_n1=data[0];
            data_model._analog_val.fields.dlu_n2=data[1];
            break;
        }



    }
}


Autopilot_Logic::Autopilot_Logic()
{

}
