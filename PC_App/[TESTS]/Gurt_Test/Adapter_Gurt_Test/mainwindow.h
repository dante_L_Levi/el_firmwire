#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SerialPort/SerialPort_crc8.h"
#include "style_classes/window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"
#include "Autopilot_Logic.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Name_update_btn_clicked();

    void on_on_ConnectButton_clicked();

    void on_Start_Request_Btn_clicked();

    void on_Btn_Boot_clicked();

    void on_btn_set_writePOT_clicked();





    void on_POT_Set_clicked();


    void Get_Read_Data_Serial(QByteArray data);
    void UPDATE_UI_TimRequest(void);


    void on_testBtn_AIN_clicked();

    void on_testBtn_DI_clicked();

    void on_test_Btn_POTx_clicked();

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
    void Init_StartBox_Element(void);
    void Activate_UI(bool status);
    void Load_Styles(void);
    void ParseRX(uint8_t cmd);

};
#endif // MAINWINDOW_H
