#include "SerialPort_crc8.h"

const uint32_t  GOtoBOOT=0xF2784C28;

SerialProtocol_crc8_Def _mainDataProtocol;
#define SizeBUFFER      sizeof(_mainDataProtocol)
uint8_t TX_BUFF[SizeBUFFER];
uint8_t RX_BUFF[SizeBUFFER];

QByteArray ReadBuffer;

uint16_t Calculate_CRC(uint8_t *data,uint8_t len);
bool checkCRC(uint8_t *data,uint8_t len);

SerialPort_crc8::SerialPort_crc8()
{

}


/************************Function Set ID Device********************/
 void SerialPort_crc8:: Set_ID(uint16_t id)
 {
     if(id!=0x00)
     {
         _mainDataProtocol.ID=id;
     }

 }


 /************************Function Get Speed********************/
 QStringList SerialPort_crc8::Get_BaudRate()
 {
     QStringList speed=
     {
         "1200",
         "2400",
         "9600",
        "19200",
         "38400",
         "115200",
         "460800",
         "921600"
     };

     return  speed;
 }


 /********************Function Init SerialPort******************/
 bool SerialPort_crc8::  Init_OpenPort(QString _Name, long int _baud)
 {
     IsRxAvalible=false;
     this->setPortName(_Name);
     this->setBaudRate(_baud);
     this->setDataBits(QSerialPort::Data8);
     this->setParity(QSerialPort::NoParity);
     this->setStopBits(QSerialPort::OneStop);
     this->setFlowControl(QSerialPort::NoFlowControl);
      //connect(this,SIGNAL(readyRead()),this,SLOT(ReadSerialPort_data()));
     connect(this,&QSerialPort::readyRead,this,&SerialPort_crc8::ReadSerialPort_data);
     bool status=this->open(QIODevice::ReadWrite);
     ReadBuffer.resize(SizeBUFFER);

    return status;
 }


 /*************************Function result read*********************/
 QByteArray SerialPort_crc8:: Get_Result_Recieve_Data(void)
 {

     qDebug()<<"Input Data:";
     qDebug()<<ReadBuffer;
     return ReadBuffer;
 }


 /*************************Function result read*********************/
 QByteArray SerialPort_crc8:: Get_CHECK_Result_Recieve_Data(void)
 {
     //ReadBuffer.clear();
     //ReadBuffer.resize(SizeBUFFER);
    // ReadBuffer=this->readAll();
     qDebug()<<"Input Data:";
     qDebug()<<ReadBuffer;
     uint8_t data[SizeBUFFER]={0x00};
     for(int i=0;i<(int)SizeBUFFER;i++)
     {
         data[i]=ReadBuffer[i];
     }
     if(checkCRC(data,SizeBUFFER))
     {
         qDebug()<<"CRC OK!!";
         return ReadBuffer;
     }
     else
     {
         qDebug()<<"CRC ERROR!!";
         return 0;
     }
 }

 /*************************Function result read*********************/
 QByteArray SerialPort_crc8:: Get_CHECK_Result_Recieve_Data(QByteArray data_Array)
 {
     //ReadBuffer.clear();
     //ReadBuffer.resize(SizeBUFFER);
    // ReadBuffer=this->readAll();
     qDebug()<<"Input Data:";
     qDebug()<<data_Array;
     //uint8_t data[SizeBUFFER]={0x00};
     //for(int i=0;i<(int)SizeBUFFER;i++)
     //{
     //    data[i]=data_Array[i];
     //}
     uint8_t *data=reinterpret_cast<uint8_t*>(data_Array.data());
     if(checkCRC(data,SizeBUFFER))
     {
         qDebug()<<"CRC OK!!";
         return data_Array;
     }
     else
     {
         qDebug()<<"CRC ERROR!!";
         return 0;
     }
 }

 void SerialPort_crc8::Set_IsRxAvalible(bool)
 {
     IsRxAvalible=true;
 }

 void SerialPort_crc8::GotoBootLoader()
 {
     uint8_t data[8]={0};
     memcpy(data,&GOtoBOOT,sizeof(data));
     Transmit_Packet(BOOT_CMD,data,sizeof(data));
 }



 /**************Read data Serial Port *********************/
 void SerialPort_crc8 :: ReadSerialPort_data()
 {
     ReadBuffer.clear();
     ReadBuffer.resize(SizeBUFFER);
     //if(this->waitForReadyRead(10))
     //        ReadBuffer=this->readAll();

        if(this->bytesAvailable()==SizeBUFFER)
        {
            ReadBuffer=this->readAll();
            IsRxAvalible=true;
            emit Get_data_Recieved_Serial(ReadBuffer);
        }

 }



 /************************Function Transmit Packet********************/
void SerialPort_crc8:: Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length)
{
    _mainDataProtocol.cmd=(uint8_t)cmd;
    if(length<=PayloadSize)
    {
        memcpy((void*)(_mainDataProtocol.payload),(void *)data,sizeof(data));
        QByteArray databuffer;
        databuffer.resize(SizeBUFFER);
        memcpy((void*)(TX_BUFF),&_mainDataProtocol,sizeof(SizeBUFFER));
        uint8_t crc=Calculate_CRC(TX_BUFF,SizeBUFFER-1);
        _mainDataProtocol.CRC=crc;
        TX_BUFF[SizeBUFFER-1]=_mainDataProtocol.CRC;
        for(int i=0;i<(int)SizeBUFFER;i++)
        {
            databuffer[i]=TX_BUFF[i];
        }
        qDebug()<<"Request:"<<databuffer;
        this->write((databuffer));
    }



}

/******************Transmit Calculate CRC************************/
uint16_t Calculate_CRC(uint8_t *data,uint8_t len)
{
    uint8_t i;
    uint8_t sum=0x00;
    for (i = 0; i < len; i++)
        {
            sum += data[i];
        }
    sum = ~sum;
    return sum;
}



bool checkCRC(uint8_t *data,uint8_t len)
{
    uint8_t crc = 0x00;
    //uint8_t des_buf=data[len-1];
    uint8_t i;
    for (i = 0; i < len; i++)
    {
         crc += data[i];
    }
    return (crc == 0xFF) ? 1 : 0;

}




