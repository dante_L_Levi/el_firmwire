#ifndef AUTOPILOT_LOGIC_H
#define AUTOPILOT_LOGIC_H


#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"
#include "SerialPort/SerialPort_crc8.h"

#define MODEL_SIZE                  8
#define INDEX_CMD                   2
#define OFFSET_DataPayload          3

#pragma pack(push, 1)
typedef union
{
    struct
    {
        uint8_t srez_cnt_inner :1;
        uint8_t otd :1;
        uint8_t on_cnt :1;
        uint8_t d2 :1;
        uint8_t pps_inner :1;
        uint8_t raz_dlu :1;
        uint8_t ready :1;
    } fields;
    uint8_t all;
} dig_in_s;

typedef union
{
    struct
    {
        int32_t omega1;
        int32_t omega2;
        int32_t beta1;
        int32_t beta2;
        int32_t m_omega1;
        int32_t upt3_1;
        int32_t _1_out_oy13;
        int32_t dlu_n1;
        int32_t _1_cp1_2;
        int32_t k_omega_2;
        int32_t _2_out_oy13;
        int32_t dlu_n2;
        int32_t k_omega1;
        int32_t upt3_2;
        int32_t upt5_2;
        int32_t upt4_2;
        int32_t upt5_1;
        int32_t upt4_1;

        int32_t m_omega_2;
        int32_t duu_v2;
        int32_t _2_out_oy14;
        int32_t _2_cp1_2;
        int32_t duu_v1;
        int32_t _1_out_oy14;

    } fields;
    uint32_t all[24];
} analog2_s;

typedef struct
{
    int32_t dataValuePOT[8];

}Potenciometer_Def;


typedef struct
{
    char                    modelName[8];
    char                    firmware[8];
    uint16_t                _id;
    analog2_s               _analog_val;
    dig_in_s                _input_state;
    Potenciometer_Def       _pot;


}Model_data_def;


#pragma pack(pop)


typedef enum
{
    rs485_default_handler=0,
    RS485_Connect=1,
    RS485_Disconnect=2,
    getBoardName=3,
    getFirmware=4,
    Get_DigIn=5,
    Get_deltes=6,
    Get_Omega=7,
    Get_mOmega=8,
    Get_YPT3=9,
    Get_YPT4=10,
    Get_YPT5=11,
    Get_YPT13=12,
    Get_YPT14=13,
    Get_Outs=14,
    Get_duu_v=15,
    Get_dlu_n=16,
    Set_POT1=17,
    Set_POT2=18,
    Set_POT3=19,
    Set_POT4=20,
    Set_POT5=21,
    Set_POT6=22,
    Set_POT7=23,
    Set_POT8=24,
    Set_Write_POT_EEPROM=25


}Parse_data_Type;






class Autopilot_Logic
{
public:
    Autopilot_Logic();
    bool isRequest;
    SerialPort_crc8 *serial_Data;

    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint16_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);


    void Request_Board_Number(uint8_t index_request);
    void Request_Board(Parse_data_Type);
    /**************************Model Control Parse********************/
    void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);

    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    void Test_GenerateData(void);


    /*************************Request Start Connection****************/
     void RS_485_OK_Request(void);
     /*************************Request Start Connection****************/
     void RS_485_Close_Request(void);
     /**********************Function Request Firmwire*******************/
     void RS_Request_GetFirmwire(void);
     /**********************Function Request NameBoard*******************/
     void RS_Request_GetNameBoard(void);


     /*************************Request OUT POTs****************/
     void RS_485_SetPOTx(uint8_t ch,uint8_t val);
     void RS485_Request_Write_POTx(void);

    uint8_t Read_Model_Status_PayLoad(QByteArray data);

    void Update_UI_ID( QLabel *data1);
    void Update_UI(uint8_t status, QLabel *data1,QLabel *data2);
    void Update_UI(QCheckBox *ch1,QCheckBox *ch2,QCheckBox *ch3,QCheckBox *ch4,
                   QCheckBox *ch5);
    void Update_UI(uint8_t status, QLabel *data1);


    const QStringList dataPOT =
    {
        "HP-I",
        "УПТ13-I",
        "HO-I",
        "УПТ4-I",
         "HP-II",
        "УПТ13-II",
        "УПТ4-II",
        "HO-II",


    };



private:
         void Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt);
         /************************Request Set Data DAC********************/




};

#endif // AUTOPILOT_LOGIC_H
