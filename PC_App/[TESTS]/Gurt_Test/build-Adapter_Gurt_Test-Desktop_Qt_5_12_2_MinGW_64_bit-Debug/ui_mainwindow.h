/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout;
    QLabel *label_13;
    QLabel *label_ID;
    QLabel *label_14;
    QLabel *lbl_Name;
    QLabel *label_16;
    QLabel *lbl_Firmw;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QPushButton *Start_Request_Btn;
    QPushButton *Btn_Boot;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_32;
    QHBoxLayout *horizontalLayout_21;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QLabel *_AIN_YPT3_I;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QLabel *_AIN_YPT3_II;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_5;
    QLabel *_AIN_YPT4_I;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_8;
    QLabel *_AIN_YPT4_II;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_6;
    QLabel *_AIN_YPT5_I;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_7;
    QLabel *_AIN_YPT5_II;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_9;
    QLabel *_AIN_mOm1;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_10;
    QLabel *_AIN_mOm2;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_11;
    QLabel *_AIN_Om1;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_12;
    QLabel *_AIN_Om2;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_17;
    QLabel *_AIN_kOm1;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_15;
    QLabel *_AIN_kOm2;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_19;
    QLabel *_AIN_dlu_n1;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_18;
    QLabel *_AIN_dlu_n2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_29;
    QLabel *_AIN_YPT13_I;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_30;
    QLabel *_AIN_YPT13_II;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_31;
    QLabel *_AIN_duu_v1;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_32;
    QLabel *_AIN_duu_v2;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_33;
    QLabel *_AIN_delta1;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_34;
    QLabel *_AIN_delta2;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_35;
    QLabel *_AIN_out1;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_36;
    QLabel *_AIN_out2;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_37;
    QLabel *_AIN_YPT14_I;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_38;
    QLabel *_AIN_YPT14_II;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_20;
    QComboBox *Index_POT;
    QSpinBox *spinBox_POTVal;
    QPushButton *POT_Set;
    QPushButton *btn_set_writePOT;
    QSpacerItem *verticalSpacer;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout_20;
    QGroupBox *groupBox;
    QCheckBox *_otd;
    QCheckBox *_razor_dlu;
    QCheckBox *_srez_cnt;
    QCheckBox *_on_ctrl;
    QCheckBox *_ready;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_33;
    QVBoxLayout *verticalLayout_7;
    QComboBox *Index_POT_2;
    QPushButton *test_Btn_POTx;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_6;
    QPushButton *testBtn_AIN;
    QPushButton *testBtn_DI;
    QSpacerItem *verticalSpacer_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(500, 530);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_3->addWidget(groupBox_2);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_5 = new QHBoxLayout(tab);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_13 = new QLabel(tab);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_13);

        label_ID = new QLabel(tab);
        label_ID->setObjectName(QString::fromUtf8("label_ID"));
        label_ID->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_ID);

        label_14 = new QLabel(tab);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_14);

        lbl_Name = new QLabel(tab);
        lbl_Name->setObjectName(QString::fromUtf8("lbl_Name"));
        lbl_Name->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbl_Name);

        label_16 = new QLabel(tab);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_16);

        lbl_Firmw = new QLabel(tab);
        lbl_Firmw->setObjectName(QString::fromUtf8("lbl_Firmw"));
        lbl_Firmw->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbl_Firmw);


        horizontalLayout_5->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(265, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        Start_Request_Btn = new QPushButton(tab);
        Start_Request_Btn->setObjectName(QString::fromUtf8("Start_Request_Btn"));

        verticalLayout_2->addWidget(Start_Request_Btn);

        Btn_Boot = new QPushButton(tab);
        Btn_Boot->setObjectName(QString::fromUtf8("Btn_Boot"));

        verticalLayout_2->addWidget(Btn_Boot);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        horizontalLayout_5->addLayout(verticalLayout_2);

        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        horizontalLayout_32 = new QHBoxLayout(tab_3);
        horizontalLayout_32->setObjectName(QString::fromUtf8("horizontalLayout_32"));
        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_6->addWidget(label_3);

        _AIN_YPT3_I = new QLabel(tab_3);
        _AIN_YPT3_I->setObjectName(QString::fromUtf8("_AIN_YPT3_I"));
        _AIN_YPT3_I->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(_AIN_YPT3_I);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(tab_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_7->addWidget(label_4);

        _AIN_YPT3_II = new QLabel(tab_3);
        _AIN_YPT3_II->setObjectName(QString::fromUtf8("_AIN_YPT3_II"));
        _AIN_YPT3_II->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(_AIN_YPT3_II);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_5 = new QLabel(tab_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_8->addWidget(label_5);

        _AIN_YPT4_I = new QLabel(tab_3);
        _AIN_YPT4_I->setObjectName(QString::fromUtf8("_AIN_YPT4_I"));
        _AIN_YPT4_I->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(_AIN_YPT4_I);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_8 = new QLabel(tab_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_11->addWidget(label_8);

        _AIN_YPT4_II = new QLabel(tab_3);
        _AIN_YPT4_II->setObjectName(QString::fromUtf8("_AIN_YPT4_II"));
        _AIN_YPT4_II->setAlignment(Qt::AlignCenter);

        horizontalLayout_11->addWidget(_AIN_YPT4_II);


        verticalLayout_4->addLayout(horizontalLayout_11);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_9->addWidget(label_6);

        _AIN_YPT5_I = new QLabel(tab_3);
        _AIN_YPT5_I->setObjectName(QString::fromUtf8("_AIN_YPT5_I"));
        _AIN_YPT5_I->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(_AIN_YPT5_I);


        verticalLayout_4->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_7 = new QLabel(tab_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_10->addWidget(label_7);

        _AIN_YPT5_II = new QLabel(tab_3);
        _AIN_YPT5_II->setObjectName(QString::fromUtf8("_AIN_YPT5_II"));
        _AIN_YPT5_II->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(_AIN_YPT5_II);


        verticalLayout_4->addLayout(horizontalLayout_10);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_9 = new QLabel(tab_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_12->addWidget(label_9);

        _AIN_mOm1 = new QLabel(tab_3);
        _AIN_mOm1->setObjectName(QString::fromUtf8("_AIN_mOm1"));
        _AIN_mOm1->setAlignment(Qt::AlignCenter);

        horizontalLayout_12->addWidget(_AIN_mOm1);


        verticalLayout_4->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_10 = new QLabel(tab_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_13->addWidget(label_10);

        _AIN_mOm2 = new QLabel(tab_3);
        _AIN_mOm2->setObjectName(QString::fromUtf8("_AIN_mOm2"));
        _AIN_mOm2->setAlignment(Qt::AlignCenter);

        horizontalLayout_13->addWidget(_AIN_mOm2);


        verticalLayout_4->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_11 = new QLabel(tab_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_14->addWidget(label_11);

        _AIN_Om1 = new QLabel(tab_3);
        _AIN_Om1->setObjectName(QString::fromUtf8("_AIN_Om1"));
        _AIN_Om1->setAlignment(Qt::AlignCenter);

        horizontalLayout_14->addWidget(_AIN_Om1);


        verticalLayout_4->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_12 = new QLabel(tab_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_15->addWidget(label_12);

        _AIN_Om2 = new QLabel(tab_3);
        _AIN_Om2->setObjectName(QString::fromUtf8("_AIN_Om2"));
        _AIN_Om2->setAlignment(Qt::AlignCenter);

        horizontalLayout_15->addWidget(_AIN_Om2);


        verticalLayout_4->addLayout(horizontalLayout_15);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_17 = new QLabel(tab_3);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_17->addWidget(label_17);

        _AIN_kOm1 = new QLabel(tab_3);
        _AIN_kOm1->setObjectName(QString::fromUtf8("_AIN_kOm1"));
        _AIN_kOm1->setAlignment(Qt::AlignCenter);

        horizontalLayout_17->addWidget(_AIN_kOm1);


        verticalLayout_4->addLayout(horizontalLayout_17);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_15 = new QLabel(tab_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_16->addWidget(label_15);

        _AIN_kOm2 = new QLabel(tab_3);
        _AIN_kOm2->setObjectName(QString::fromUtf8("_AIN_kOm2"));
        _AIN_kOm2->setAlignment(Qt::AlignCenter);

        horizontalLayout_16->addWidget(_AIN_kOm2);


        verticalLayout_4->addLayout(horizontalLayout_16);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_19 = new QLabel(tab_3);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_19->addWidget(label_19);

        _AIN_dlu_n1 = new QLabel(tab_3);
        _AIN_dlu_n1->setObjectName(QString::fromUtf8("_AIN_dlu_n1"));
        _AIN_dlu_n1->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(_AIN_dlu_n1);


        verticalLayout_4->addLayout(horizontalLayout_19);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_18 = new QLabel(tab_3);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_18->addWidget(label_18);

        _AIN_dlu_n2 = new QLabel(tab_3);
        _AIN_dlu_n2->setObjectName(QString::fromUtf8("_AIN_dlu_n2"));
        _AIN_dlu_n2->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(_AIN_dlu_n2);


        verticalLayout_4->addLayout(horizontalLayout_18);


        horizontalLayout_21->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_29 = new QLabel(tab_3);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        horizontalLayout_22->addWidget(label_29);

        _AIN_YPT13_I = new QLabel(tab_3);
        _AIN_YPT13_I->setObjectName(QString::fromUtf8("_AIN_YPT13_I"));
        _AIN_YPT13_I->setAlignment(Qt::AlignCenter);

        horizontalLayout_22->addWidget(_AIN_YPT13_I);


        verticalLayout_5->addLayout(horizontalLayout_22);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_30 = new QLabel(tab_3);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        horizontalLayout_23->addWidget(label_30);

        _AIN_YPT13_II = new QLabel(tab_3);
        _AIN_YPT13_II->setObjectName(QString::fromUtf8("_AIN_YPT13_II"));
        _AIN_YPT13_II->setAlignment(Qt::AlignCenter);

        horizontalLayout_23->addWidget(_AIN_YPT13_II);


        verticalLayout_5->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_31 = new QLabel(tab_3);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        horizontalLayout_24->addWidget(label_31);

        _AIN_duu_v1 = new QLabel(tab_3);
        _AIN_duu_v1->setObjectName(QString::fromUtf8("_AIN_duu_v1"));
        _AIN_duu_v1->setAlignment(Qt::AlignCenter);

        horizontalLayout_24->addWidget(_AIN_duu_v1);


        verticalLayout_5->addLayout(horizontalLayout_24);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        label_32 = new QLabel(tab_3);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        horizontalLayout_25->addWidget(label_32);

        _AIN_duu_v2 = new QLabel(tab_3);
        _AIN_duu_v2->setObjectName(QString::fromUtf8("_AIN_duu_v2"));
        _AIN_duu_v2->setAlignment(Qt::AlignCenter);

        horizontalLayout_25->addWidget(_AIN_duu_v2);


        verticalLayout_5->addLayout(horizontalLayout_25);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        label_33 = new QLabel(tab_3);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        horizontalLayout_26->addWidget(label_33);

        _AIN_delta1 = new QLabel(tab_3);
        _AIN_delta1->setObjectName(QString::fromUtf8("_AIN_delta1"));
        _AIN_delta1->setAlignment(Qt::AlignCenter);

        horizontalLayout_26->addWidget(_AIN_delta1);


        verticalLayout_5->addLayout(horizontalLayout_26);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        label_34 = new QLabel(tab_3);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        horizontalLayout_27->addWidget(label_34);

        _AIN_delta2 = new QLabel(tab_3);
        _AIN_delta2->setObjectName(QString::fromUtf8("_AIN_delta2"));
        _AIN_delta2->setAlignment(Qt::AlignCenter);

        horizontalLayout_27->addWidget(_AIN_delta2);


        verticalLayout_5->addLayout(horizontalLayout_27);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setObjectName(QString::fromUtf8("horizontalLayout_28"));
        label_35 = new QLabel(tab_3);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        horizontalLayout_28->addWidget(label_35);

        _AIN_out1 = new QLabel(tab_3);
        _AIN_out1->setObjectName(QString::fromUtf8("_AIN_out1"));
        _AIN_out1->setAlignment(Qt::AlignCenter);

        horizontalLayout_28->addWidget(_AIN_out1);


        verticalLayout_5->addLayout(horizontalLayout_28);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setObjectName(QString::fromUtf8("horizontalLayout_29"));
        label_36 = new QLabel(tab_3);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        horizontalLayout_29->addWidget(label_36);

        _AIN_out2 = new QLabel(tab_3);
        _AIN_out2->setObjectName(QString::fromUtf8("_AIN_out2"));
        _AIN_out2->setAlignment(Qt::AlignCenter);

        horizontalLayout_29->addWidget(_AIN_out2);


        verticalLayout_5->addLayout(horizontalLayout_29);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        label_37 = new QLabel(tab_3);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        horizontalLayout_30->addWidget(label_37);

        _AIN_YPT14_I = new QLabel(tab_3);
        _AIN_YPT14_I->setObjectName(QString::fromUtf8("_AIN_YPT14_I"));
        _AIN_YPT14_I->setAlignment(Qt::AlignCenter);

        horizontalLayout_30->addWidget(_AIN_YPT14_I);


        verticalLayout_5->addLayout(horizontalLayout_30);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        label_38 = new QLabel(tab_3);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        horizontalLayout_31->addWidget(label_38);

        _AIN_YPT14_II = new QLabel(tab_3);
        _AIN_YPT14_II->setObjectName(QString::fromUtf8("_AIN_YPT14_II"));
        _AIN_YPT14_II->setAlignment(Qt::AlignCenter);

        horizontalLayout_31->addWidget(_AIN_YPT14_II);


        verticalLayout_5->addLayout(horizontalLayout_31);


        horizontalLayout_21->addLayout(verticalLayout_5);


        horizontalLayout_32->addLayout(horizontalLayout_21);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_20 = new QLabel(tab_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(label_20);

        Index_POT = new QComboBox(tab_3);
        Index_POT->setObjectName(QString::fromUtf8("Index_POT"));

        verticalLayout_16->addWidget(Index_POT);

        spinBox_POTVal = new QSpinBox(tab_3);
        spinBox_POTVal->setObjectName(QString::fromUtf8("spinBox_POTVal"));

        verticalLayout_16->addWidget(spinBox_POTVal);

        POT_Set = new QPushButton(tab_3);
        POT_Set->setObjectName(QString::fromUtf8("POT_Set"));

        verticalLayout_16->addWidget(POT_Set);

        btn_set_writePOT = new QPushButton(tab_3);
        btn_set_writePOT->setObjectName(QString::fromUtf8("btn_set_writePOT"));

        verticalLayout_16->addWidget(btn_set_writePOT);

        verticalSpacer = new QSpacerItem(20, 228, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer);


        horizontalLayout_32->addLayout(verticalLayout_16);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        horizontalLayout_20 = new QHBoxLayout(tab_4);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        _otd = new QCheckBox(groupBox);
        _otd->setObjectName(QString::fromUtf8("_otd"));
        _otd->setGeometry(QRect(10, 20, 81, 20));
        _razor_dlu = new QCheckBox(groupBox);
        _razor_dlu->setObjectName(QString::fromUtf8("_razor_dlu"));
        _razor_dlu->setGeometry(QRect(10, 40, 81, 20));
        _srez_cnt = new QCheckBox(groupBox);
        _srez_cnt->setObjectName(QString::fromUtf8("_srez_cnt"));
        _srez_cnt->setGeometry(QRect(10, 60, 81, 20));
        _on_ctrl = new QCheckBox(groupBox);
        _on_ctrl->setObjectName(QString::fromUtf8("_on_ctrl"));
        _on_ctrl->setGeometry(QRect(10, 80, 81, 20));
        _ready = new QCheckBox(groupBox);
        _ready->setObjectName(QString::fromUtf8("_ready"));
        _ready->setGeometry(QRect(10, 100, 81, 20));

        horizontalLayout_20->addWidget(groupBox);

        tabWidget->addTab(tab_4, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_33 = new QHBoxLayout(tab_2);
        horizontalLayout_33->setObjectName(QString::fromUtf8("horizontalLayout_33"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        Index_POT_2 = new QComboBox(tab_2);
        Index_POT_2->setObjectName(QString::fromUtf8("Index_POT_2"));

        verticalLayout_7->addWidget(Index_POT_2);

        test_Btn_POTx = new QPushButton(tab_2);
        test_Btn_POTx->setObjectName(QString::fromUtf8("test_Btn_POTx"));

        verticalLayout_7->addWidget(test_Btn_POTx);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_4);


        horizontalLayout_33->addLayout(verticalLayout_7);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        testBtn_AIN = new QPushButton(tab_2);
        testBtn_AIN->setObjectName(QString::fromUtf8("testBtn_AIN"));

        verticalLayout_6->addWidget(testBtn_AIN);

        testBtn_DI = new QPushButton(tab_2);
        testBtn_DI->setObjectName(QString::fromUtf8("testBtn_DI"));

        verticalLayout_6->addWidget(testBtn_DI);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_3);


        horizontalLayout_33->addLayout(verticalLayout_6);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_3->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "ID:", nullptr));
        label_ID->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "NameBoard:", nullptr));
        lbl_Name->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Firmwire:", nullptr));
        lbl_Firmw->setText(QApplication::translate("MainWindow", "0", nullptr));
        Start_Request_Btn->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        Btn_Boot->setText(QApplication::translate("MainWindow", "Boot", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Main Fill", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2423-I:", nullptr));
        _AIN_YPT3_I->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2423-II:", nullptr));
        _AIN_YPT3_II->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2424-I:", nullptr));
        _AIN_YPT4_I->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2424-II:", nullptr));
        _AIN_YPT4_II->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2425-I:", nullptr));
        _AIN_YPT5_I->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\2425-II:", nullptr));
        _AIN_YPT5_II->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "-OmegaI:", nullptr));
        _AIN_mOm1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "-OmegaII:", nullptr));
        _AIN_mOm2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "OmegaI:", nullptr));
        _AIN_Om1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "OmegaII:", nullptr));
        _AIN_Om2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "kOmegaI:", nullptr));
        _AIN_kOm1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "kOmegaII:", nullptr));
        _AIN_kOm2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "dlu-n1:", nullptr));
        _AIN_dlu_n1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "dlu-n2:", nullptr));
        _AIN_dlu_n2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\24213-I:", nullptr));
        _AIN_YPT13_I->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "\320\243\320\237\320\24213-II:", nullptr));
        _AIN_YPT13_II->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "duu-v1:", nullptr));
        _AIN_duu_v1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "duu-v2:", nullptr));
        _AIN_duu_v2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "delta1::", nullptr));
        _AIN_delta1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "delta2:", nullptr));
        _AIN_delta2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "I-CP1-2:", nullptr));
        _AIN_out1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_36->setText(QApplication::translate("MainWindow", "II-CP1-2:", nullptr));
        _AIN_out2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_37->setText(QApplication::translate("MainWindow", "YPT14-I:", nullptr));
        _AIN_YPT14_I->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_38->setText(QApplication::translate("MainWindow", "YPT14-II:", nullptr));
        _AIN_YPT14_II->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Name POT:", nullptr));
        POT_Set->setText(QApplication::translate("MainWindow", "Set", nullptr));
        btn_set_writePOT->setText(QApplication::translate("MainWindow", "write", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Analog", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Inputs:", nullptr));
        _otd->setText(QApplication::translate("MainWindow", "OTD", nullptr));
        _razor_dlu->setText(QApplication::translate("MainWindow", "Razor DLU", nullptr));
        _srez_cnt->setText(QApplication::translate("MainWindow", "Srez_CNT", nullptr));
        _on_ctrl->setText(QApplication::translate("MainWindow", "On_CTRL", nullptr));
        _ready->setText(QApplication::translate("MainWindow", "Ready", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "Inputs", nullptr));
        test_Btn_POTx->setText(QApplication::translate("MainWindow", "Request POT", nullptr));
        testBtn_AIN->setText(QApplication::translate("MainWindow", "Request AIN", nullptr));
        testBtn_DI->setText(QApplication::translate("MainWindow", "Request DI", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
