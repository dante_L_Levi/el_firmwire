/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Adapter_Gurt_Test/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[14];
    char stringdata0[280];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 26), // "on_Name_update_btn_clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 27), // "on_on_ConnectButton_clicked"
QT_MOC_LITERAL(4, 67, 28), // "on_Start_Request_Btn_clicked"
QT_MOC_LITERAL(5, 96, 19), // "on_Btn_Boot_clicked"
QT_MOC_LITERAL(6, 116, 27), // "on_btn_set_writePOT_clicked"
QT_MOC_LITERAL(7, 144, 18), // "on_POT_Set_clicked"
QT_MOC_LITERAL(8, 163, 20), // "Get_Read_Data_Serial"
QT_MOC_LITERAL(9, 184, 4), // "data"
QT_MOC_LITERAL(10, 189, 20), // "UPDATE_UI_TimRequest"
QT_MOC_LITERAL(11, 210, 22), // "on_testBtn_AIN_clicked"
QT_MOC_LITERAL(12, 233, 21), // "on_testBtn_DI_clicked"
QT_MOC_LITERAL(13, 255, 24) // "on_test_Btn_POTx_clicked"

    },
    "MainWindow\0on_Name_update_btn_clicked\0"
    "\0on_on_ConnectButton_clicked\0"
    "on_Start_Request_Btn_clicked\0"
    "on_Btn_Boot_clicked\0on_btn_set_writePOT_clicked\0"
    "on_POT_Set_clicked\0Get_Read_Data_Serial\0"
    "data\0UPDATE_UI_TimRequest\0"
    "on_testBtn_AIN_clicked\0on_testBtn_DI_clicked\0"
    "on_test_Btn_POTx_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    0,   73,    2, 0x08 /* Private */,
       7,    0,   74,    2, 0x08 /* Private */,
       8,    1,   75,    2, 0x08 /* Private */,
      10,    0,   78,    2, 0x08 /* Private */,
      11,    0,   79,    2, 0x08 /* Private */,
      12,    0,   80,    2, 0x08 /* Private */,
      13,    0,   81,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Name_update_btn_clicked(); break;
        case 1: _t->on_on_ConnectButton_clicked(); break;
        case 2: _t->on_Start_Request_Btn_clicked(); break;
        case 3: _t->on_Btn_Boot_clicked(); break;
        case 4: _t->on_btn_set_writePOT_clicked(); break;
        case 5: _t->on_POT_Set_clicked(); break;
        case 6: _t->Get_Read_Data_Serial((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 7: _t->UPDATE_UI_TimRequest(); break;
        case 8: _t->on_testBtn_AIN_clicked(); break;
        case 9: _t->on_testBtn_DI_clicked(); break;
        case 10: _t->on_test_Btn_POTx_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
