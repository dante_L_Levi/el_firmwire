#ifndef FORMGRAPH_H
#define FORMGRAPH_H

#include <QWidget>

namespace Ui {
class formgraph;
}

class formgraph : public QWidget
{
    Q_OBJECT

public:
    explicit formgraph(QWidget *parent = nullptr);
    ~formgraph();

private:
    Ui::formgraph *ui;
};

#endif // FORMGRAPH_H
