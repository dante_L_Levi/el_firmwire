/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *G_1;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QLabel *G_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QLabel *G_3;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_4;
    QLabel *A_1;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_5;
    QLabel *A_2;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QLabel *A_3;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_7;
    QLabel *M_1;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_8;
    QLabel *M_2;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_9;
    QLabel *M_3;
    QPushButton *pushButton;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(152, 233);
        verticalLayout = new QVBoxLayout(Widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(Widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);

        G_1 = new QLabel(Widget);
        G_1->setObjectName(QString::fromUtf8("G_1"));
        G_1->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(G_1);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_2 = new QLabel(Widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);

        G_2 = new QLabel(Widget);
        G_2->setObjectName(QString::fromUtf8("G_2"));
        G_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(G_2);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_3 = new QLabel(Widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_3);

        G_3 = new QLabel(Widget);
        G_3->setObjectName(QString::fromUtf8("G_3"));
        G_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(G_3);


        horizontalLayout->addLayout(verticalLayout_4);


        verticalLayout_11->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_4 = new QLabel(Widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_4);

        A_1 = new QLabel(Widget);
        A_1->setObjectName(QString::fromUtf8("A_1"));
        A_1->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(A_1);


        horizontalLayout_2->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_5 = new QLabel(Widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_5);

        A_2 = new QLabel(Widget);
        A_2->setObjectName(QString::fromUtf8("A_2"));
        A_2->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(A_2);


        horizontalLayout_2->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_6 = new QLabel(Widget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_6);

        A_3 = new QLabel(Widget);
        A_3->setObjectName(QString::fromUtf8("A_3"));
        A_3->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(A_3);


        horizontalLayout_2->addLayout(verticalLayout_7);


        verticalLayout_11->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_7 = new QLabel(Widget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_7);

        M_1 = new QLabel(Widget);
        M_1->setObjectName(QString::fromUtf8("M_1"));
        M_1->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(M_1);


        horizontalLayout_3->addLayout(verticalLayout_8);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_8 = new QLabel(Widget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(label_8);

        M_2 = new QLabel(Widget);
        M_2->setObjectName(QString::fromUtf8("M_2"));
        M_2->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(M_2);


        horizontalLayout_3->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_9 = new QLabel(Widget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_9);

        M_3 = new QLabel(Widget);
        M_3->setObjectName(QString::fromUtf8("M_3"));
        M_3->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(M_3);


        horizontalLayout_3->addLayout(verticalLayout_10);


        verticalLayout_11->addLayout(horizontalLayout_3);


        verticalLayout->addLayout(verticalLayout_11);

        pushButton = new QPushButton(Widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        label->setText(QApplication::translate("Widget", "GX:", nullptr));
        G_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_2->setText(QApplication::translate("Widget", "GY:", nullptr));
        G_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_3->setText(QApplication::translate("Widget", "GZ:", nullptr));
        G_3->setText(QApplication::translate("Widget", "0", nullptr));
        label_4->setText(QApplication::translate("Widget", "AX:", nullptr));
        A_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_5->setText(QApplication::translate("Widget", "AY:", nullptr));
        A_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_6->setText(QApplication::translate("Widget", "AZ:", nullptr));
        A_3->setText(QApplication::translate("Widget", "0", nullptr));
        label_7->setText(QApplication::translate("Widget", "MX:", nullptr));
        M_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_8->setText(QApplication::translate("Widget", "MY:", nullptr));
        M_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_9->setText(QApplication::translate("Widget", "MZ:", nullptr));
        M_3->setText(QApplication::translate("Widget", "0", nullptr));
        pushButton->setText(QApplication::translate("Widget", "\320\241\321\202\320\260\321\200\321\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
