/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_14;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_12;
    QLabel *LBL_Pitch;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_16;
    QLabel *LBL_YAW;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_14;
    QLabel *LBL_ROLL;
    QVBoxLayout *verticalLayout_18;
    QLabel *label_18;
    QLabel *LBL_THROT;
    QPushButton *pushButton_4;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_19;
    QVBoxLayout *verticalLayout_20;
    QLabel *label_20;
    QLabel *DATA_ACCEL_X;
    QVBoxLayout *verticalLayout_21;
    QLabel *label_21;
    QLabel *DATA_ACCEL_Y;
    QVBoxLayout *verticalLayout_22;
    QLabel *label_22;
    QLabel *DATA_ACCEL_Z;
    QVBoxLayout *verticalLayout_23;
    QVBoxLayout *verticalLayout_24;
    QLabel *label_23;
    QLabel *DATA_GYRO_X;
    QVBoxLayout *verticalLayout_25;
    QLabel *label_24;
    QLabel *DATA_GYRO_Y;
    QVBoxLayout *verticalLayout_26;
    QLabel *label_25;
    QLabel *DATA_GYRO_Z;
    QVBoxLayout *verticalLayout_27;
    QVBoxLayout *verticalLayout_28;
    QLabel *label_26;
    QLabel *DATA_MAGN_X;
    QVBoxLayout *verticalLayout_29;
    QLabel *label_27;
    QLabel *DATA_MAGN_Y;
    QVBoxLayout *verticalLayout_30;
    QLabel *label_28;
    QLabel *DATA_MAGN_Z;
    QVBoxLayout *verticalLayout_33;
    QVBoxLayout *verticalLayout_31;
    QLabel *label_29;
    QLabel *ID_LSM6;
    QVBoxLayout *verticalLayout_32;
    QLabel *label_30;
    QLabel *ID_LIS3;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_13;
    QPushButton *RS_CONN;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLabel *SERVICE_ACCEL_X;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_5;
    QLabel *SERVICE_ACCEL_Y;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_7;
    QLabel *SERVICE_ACCEL_Z;
    QVBoxLayout *verticalLayout_5;
    QPushButton *pushButton_2;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_4;
    QLabel *SERVICE_GYRO_X;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QLabel *SERVICE_GYRO_Y;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_8;
    QLabel *SERVICE_GYRO_Z;
    QVBoxLayout *verticalLayout_9;
    QPushButton *pushButton_3;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_9;
    QLabel *SERVICE_MAGN_X;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_10;
    QLabel *SERVICE_MAGN_Y;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_11;
    QLabel *SERVICE_MAGN_Z;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(511, 303);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_14 = new QVBoxLayout(centralwidget);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_14->addWidget(groupBox_2);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_7 = new QHBoxLayout(tab);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        label_12 = new QLabel(tab);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(label_12);

        LBL_Pitch = new QLabel(tab);
        LBL_Pitch->setObjectName(QString::fromUtf8("LBL_Pitch"));
        LBL_Pitch->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(LBL_Pitch);


        horizontalLayout_6->addLayout(verticalLayout_15);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_16 = new QLabel(tab);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(label_16);

        LBL_YAW = new QLabel(tab);
        LBL_YAW->setObjectName(QString::fromUtf8("LBL_YAW"));
        LBL_YAW->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(LBL_YAW);


        horizontalLayout_6->addLayout(verticalLayout_17);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_14 = new QLabel(tab);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(label_14);

        LBL_ROLL = new QLabel(tab);
        LBL_ROLL->setObjectName(QString::fromUtf8("LBL_ROLL"));
        LBL_ROLL->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(LBL_ROLL);


        horizontalLayout_6->addLayout(verticalLayout_16);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        label_18 = new QLabel(tab);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setAlignment(Qt::AlignCenter);

        verticalLayout_18->addWidget(label_18);

        LBL_THROT = new QLabel(tab);
        LBL_THROT->setObjectName(QString::fromUtf8("LBL_THROT"));
        LBL_THROT->setAlignment(Qt::AlignCenter);

        verticalLayout_18->addWidget(LBL_THROT);


        horizontalLayout_6->addLayout(verticalLayout_18);


        horizontalLayout_7->addLayout(horizontalLayout_6);

        pushButton_4 = new QPushButton(tab);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_7->addWidget(pushButton_4);

        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        horizontalLayout_8 = new QHBoxLayout(tab_3);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        verticalLayout_20 = new QVBoxLayout();
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        label_20 = new QLabel(tab_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setAlignment(Qt::AlignCenter);

        verticalLayout_20->addWidget(label_20);

        DATA_ACCEL_X = new QLabel(tab_3);
        DATA_ACCEL_X->setObjectName(QString::fromUtf8("DATA_ACCEL_X"));
        DATA_ACCEL_X->setAlignment(Qt::AlignCenter);

        verticalLayout_20->addWidget(DATA_ACCEL_X);


        verticalLayout_19->addLayout(verticalLayout_20);

        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        label_21 = new QLabel(tab_3);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setAlignment(Qt::AlignCenter);

        verticalLayout_21->addWidget(label_21);

        DATA_ACCEL_Y = new QLabel(tab_3);
        DATA_ACCEL_Y->setObjectName(QString::fromUtf8("DATA_ACCEL_Y"));
        DATA_ACCEL_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_21->addWidget(DATA_ACCEL_Y);


        verticalLayout_19->addLayout(verticalLayout_21);

        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        label_22 = new QLabel(tab_3);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setAlignment(Qt::AlignCenter);

        verticalLayout_22->addWidget(label_22);

        DATA_ACCEL_Z = new QLabel(tab_3);
        DATA_ACCEL_Z->setObjectName(QString::fromUtf8("DATA_ACCEL_Z"));
        DATA_ACCEL_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_22->addWidget(DATA_ACCEL_Z);


        verticalLayout_19->addLayout(verticalLayout_22);


        horizontalLayout_8->addLayout(verticalLayout_19);

        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setObjectName(QString::fromUtf8("verticalLayout_24"));
        label_23 = new QLabel(tab_3);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setAlignment(Qt::AlignCenter);

        verticalLayout_24->addWidget(label_23);

        DATA_GYRO_X = new QLabel(tab_3);
        DATA_GYRO_X->setObjectName(QString::fromUtf8("DATA_GYRO_X"));
        DATA_GYRO_X->setAlignment(Qt::AlignCenter);

        verticalLayout_24->addWidget(DATA_GYRO_X);


        verticalLayout_23->addLayout(verticalLayout_24);

        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setObjectName(QString::fromUtf8("verticalLayout_25"));
        label_24 = new QLabel(tab_3);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setAlignment(Qt::AlignCenter);

        verticalLayout_25->addWidget(label_24);

        DATA_GYRO_Y = new QLabel(tab_3);
        DATA_GYRO_Y->setObjectName(QString::fromUtf8("DATA_GYRO_Y"));
        DATA_GYRO_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_25->addWidget(DATA_GYRO_Y);


        verticalLayout_23->addLayout(verticalLayout_25);

        verticalLayout_26 = new QVBoxLayout();
        verticalLayout_26->setObjectName(QString::fromUtf8("verticalLayout_26"));
        label_25 = new QLabel(tab_3);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setAlignment(Qt::AlignCenter);

        verticalLayout_26->addWidget(label_25);

        DATA_GYRO_Z = new QLabel(tab_3);
        DATA_GYRO_Z->setObjectName(QString::fromUtf8("DATA_GYRO_Z"));
        DATA_GYRO_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_26->addWidget(DATA_GYRO_Z);


        verticalLayout_23->addLayout(verticalLayout_26);


        horizontalLayout_8->addLayout(verticalLayout_23);

        verticalLayout_27 = new QVBoxLayout();
        verticalLayout_27->setObjectName(QString::fromUtf8("verticalLayout_27"));
        verticalLayout_28 = new QVBoxLayout();
        verticalLayout_28->setObjectName(QString::fromUtf8("verticalLayout_28"));
        label_26 = new QLabel(tab_3);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(label_26);

        DATA_MAGN_X = new QLabel(tab_3);
        DATA_MAGN_X->setObjectName(QString::fromUtf8("DATA_MAGN_X"));
        DATA_MAGN_X->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(DATA_MAGN_X);


        verticalLayout_27->addLayout(verticalLayout_28);

        verticalLayout_29 = new QVBoxLayout();
        verticalLayout_29->setObjectName(QString::fromUtf8("verticalLayout_29"));
        label_27 = new QLabel(tab_3);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setAlignment(Qt::AlignCenter);

        verticalLayout_29->addWidget(label_27);

        DATA_MAGN_Y = new QLabel(tab_3);
        DATA_MAGN_Y->setObjectName(QString::fromUtf8("DATA_MAGN_Y"));
        DATA_MAGN_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_29->addWidget(DATA_MAGN_Y);


        verticalLayout_27->addLayout(verticalLayout_29);

        verticalLayout_30 = new QVBoxLayout();
        verticalLayout_30->setObjectName(QString::fromUtf8("verticalLayout_30"));
        label_28 = new QLabel(tab_3);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(label_28);

        DATA_MAGN_Z = new QLabel(tab_3);
        DATA_MAGN_Z->setObjectName(QString::fromUtf8("DATA_MAGN_Z"));
        DATA_MAGN_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(DATA_MAGN_Z);


        verticalLayout_27->addLayout(verticalLayout_30);


        horizontalLayout_8->addLayout(verticalLayout_27);

        verticalLayout_33 = new QVBoxLayout();
        verticalLayout_33->setObjectName(QString::fromUtf8("verticalLayout_33"));
        verticalLayout_31 = new QVBoxLayout();
        verticalLayout_31->setObjectName(QString::fromUtf8("verticalLayout_31"));
        label_29 = new QLabel(tab_3);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setAlignment(Qt::AlignCenter);

        verticalLayout_31->addWidget(label_29);

        ID_LSM6 = new QLabel(tab_3);
        ID_LSM6->setObjectName(QString::fromUtf8("ID_LSM6"));
        ID_LSM6->setAlignment(Qt::AlignCenter);

        verticalLayout_31->addWidget(ID_LSM6);


        verticalLayout_33->addLayout(verticalLayout_31);

        verticalLayout_32 = new QVBoxLayout();
        verticalLayout_32->setObjectName(QString::fromUtf8("verticalLayout_32"));
        label_30 = new QLabel(tab_3);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(label_30);

        ID_LIS3 = new QLabel(tab_3);
        ID_LIS3->setObjectName(QString::fromUtf8("ID_LIS3"));
        ID_LIS3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(ID_LIS3);


        verticalLayout_33->addLayout(verticalLayout_32);


        horizontalLayout_8->addLayout(verticalLayout_33);

        tabWidget->addTab(tab_3, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_13 = new QVBoxLayout(tab_2);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        RS_CONN = new QPushButton(tab_2);
        RS_CONN->setObjectName(QString::fromUtf8("RS_CONN"));

        verticalLayout_13->addWidget(RS_CONN);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        pushButton = new QPushButton(tab_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_4->addWidget(pushButton);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        SERVICE_ACCEL_X = new QLabel(tab_2);
        SERVICE_ACCEL_X->setObjectName(QString::fromUtf8("SERVICE_ACCEL_X"));
        SERVICE_ACCEL_X->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(SERVICE_ACCEL_X);


        verticalLayout_4->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_5);

        SERVICE_ACCEL_Y = new QLabel(tab_2);
        SERVICE_ACCEL_Y->setObjectName(QString::fromUtf8("SERVICE_ACCEL_Y"));
        SERVICE_ACCEL_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(SERVICE_ACCEL_Y);


        verticalLayout_4->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_7);

        SERVICE_ACCEL_Z = new QLabel(tab_2);
        SERVICE_ACCEL_Z->setObjectName(QString::fromUtf8("SERVICE_ACCEL_Z"));
        SERVICE_ACCEL_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(SERVICE_ACCEL_Z);


        verticalLayout_4->addLayout(verticalLayout_3);


        horizontalLayout_5->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        pushButton_2 = new QPushButton(tab_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_5->addWidget(pushButton_2);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_4);

        SERVICE_GYRO_X = new QLabel(tab_2);
        SERVICE_GYRO_X->setObjectName(QString::fromUtf8("SERVICE_GYRO_X"));
        SERVICE_GYRO_X->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(SERVICE_GYRO_X);


        verticalLayout_5->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_6);

        SERVICE_GYRO_Y = new QLabel(tab_2);
        SERVICE_GYRO_Y->setObjectName(QString::fromUtf8("SERVICE_GYRO_Y"));
        SERVICE_GYRO_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(SERVICE_GYRO_Y);


        verticalLayout_5->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_8);

        SERVICE_GYRO_Z = new QLabel(tab_2);
        SERVICE_GYRO_Z->setObjectName(QString::fromUtf8("SERVICE_GYRO_Z"));
        SERVICE_GYRO_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(SERVICE_GYRO_Z);


        verticalLayout_5->addLayout(verticalLayout_8);


        horizontalLayout_5->addLayout(verticalLayout_5);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        pushButton_3 = new QPushButton(tab_2);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_9->addWidget(pushButton_3);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_9);

        SERVICE_MAGN_X = new QLabel(tab_2);
        SERVICE_MAGN_X->setObjectName(QString::fromUtf8("SERVICE_MAGN_X"));
        SERVICE_MAGN_X->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(SERVICE_MAGN_X);


        verticalLayout_9->addLayout(verticalLayout_10);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(label_10);

        SERVICE_MAGN_Y = new QLabel(tab_2);
        SERVICE_MAGN_Y->setObjectName(QString::fromUtf8("SERVICE_MAGN_Y"));
        SERVICE_MAGN_Y->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(SERVICE_MAGN_Y);


        verticalLayout_9->addLayout(verticalLayout_11);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_11 = new QLabel(tab_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(label_11);

        SERVICE_MAGN_Z = new QLabel(tab_2);
        SERVICE_MAGN_Z->setObjectName(QString::fromUtf8("SERVICE_MAGN_Z"));
        SERVICE_MAGN_Z->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(SERVICE_MAGN_Z);


        verticalLayout_9->addLayout(verticalLayout_12);


        horizontalLayout_5->addLayout(verticalLayout_9);


        verticalLayout_13->addLayout(horizontalLayout_5);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_14->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QCoreApplication::translate("MainWindow", "update", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QCoreApplication::translate("MainWindow", "Connection", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "Pitch:", nullptr));
        LBL_Pitch->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "Yaw:", nullptr));
        LBL_YAW->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Roll:", nullptr));
        LBL_ROLL->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_18->setText(QCoreApplication::translate("MainWindow", "Throttle:", nullptr));
        LBL_THROT->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "Start Request", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "IMU", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "Accel X:", nullptr));
        DATA_ACCEL_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_21->setText(QCoreApplication::translate("MainWindow", "Accel Y:", nullptr));
        DATA_ACCEL_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_22->setText(QCoreApplication::translate("MainWindow", "Accel Z:", nullptr));
        DATA_ACCEL_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_23->setText(QCoreApplication::translate("MainWindow", "Gyro X:", nullptr));
        DATA_GYRO_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_24->setText(QCoreApplication::translate("MainWindow", "Gyro Y:", nullptr));
        DATA_GYRO_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_25->setText(QCoreApplication::translate("MainWindow", "Gyro Z:", nullptr));
        DATA_GYRO_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_26->setText(QCoreApplication::translate("MainWindow", "Magn X:", nullptr));
        DATA_MAGN_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_27->setText(QCoreApplication::translate("MainWindow", "Magn Y:", nullptr));
        DATA_MAGN_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_28->setText(QCoreApplication::translate("MainWindow", "Magn Z:", nullptr));
        DATA_MAGN_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_29->setText(QCoreApplication::translate("MainWindow", "LSM6DS0 ID:", nullptr));
        ID_LSM6->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_30->setText(QCoreApplication::translate("MainWindow", "LIS3DLM:", nullptr));
        ID_LIS3->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("MainWindow", "Sensor data", nullptr));
        RS_CONN->setText(QCoreApplication::translate("MainWindow", "RS_Connect", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Request", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Accel X:", nullptr));
        SERVICE_ACCEL_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Accel Y:", nullptr));
        SERVICE_ACCEL_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "Accel Z:", nullptr));
        SERVICE_ACCEL_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "Request", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Gyro X:", nullptr));
        SERVICE_GYRO_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Gyro Y:", nullptr));
        SERVICE_GYRO_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Gyro Z:", nullptr));
        SERVICE_GYRO_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "Request", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Magn X:", nullptr));
        SERVICE_MAGN_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "Magn Y:", nullptr));
        SERVICE_MAGN_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "Magn Z:", nullptr));
        SERVICE_MAGN_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "SERVICE", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
