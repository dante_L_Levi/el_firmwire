#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>


SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
IMU_System *mainData;

int16_t index_request=1;
int16_t index_Madgvick_Update=1;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label->setStyleSheet("color: rgb(255, 255, 255)");
    ui->label_2->setStyleSheet("color: rgb(255, 255, 255)");

    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    _serial= new SerialProtocol_data();//Start Serial Protocol

    mainData=new IMU_System();
    Init_StartBox_Element();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                ui->NameSerialPort_Box->addItem(info.portName());
            }

}


void MainWindow::Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);
        RequestReadWrite->setInterval(50);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }
}

void MainWindow:: UPDATE_UI_TimRequest(void)
{
    //по таймеру начинаем генерировать запросы(стартуем таймер)

    //считываем ответ(обработчик таймера)
    //парсим обратно в стуктуру(обработчик таймера)
    //обновляет графический интерфейс(обработчик таймера)
    if(index_request>3)
    {
        index_request=1;
    }
    if(index_request==1)
    {
        mainData->Main_Request_Data(Request_Accel);//посылаем запрос
        mainData->Read_dataModel();//считываем ответ
    }
    else if(index_request==2)
    {
        mainData->Main_Request_Data(Request_Gyro);//посылаем запрос
        mainData->Read_dataModel();//считываем ответ
    }
    else if(index_request==3)
    {
        mainData->Main_Request_Data(Request_Magn);//посылаем запрос
        mainData->Read_dataModel();//считываем ответ
    }
    if(index_Madgvick_Update>1000)
    {
        index_Madgvick_Update=1;
    }
    if(index_Madgvick_Update%3==0)
    {
        mainData->Get_Quaternion_Data();
        mainData->Update_UI_Model(ui->LBL_Pitch,ui->LBL_YAW,ui->LBL_ROLL,ui->LBL_THROT);
    }
   index_request++;
   index_Madgvick_Update++;

      //mainData->Test_GenerateData();//test function check

        mainData->Update_UI_Model(ui->DATA_ACCEL_X,ui->DATA_ACCEL_Y,ui->DATA_ACCEL_Z,
                                  ui->DATA_GYRO_X,ui->DATA_GYRO_Y,ui->DATA_GYRO_Z,
                                  ui->DATA_MAGN_X,ui->DATA_MAGN_Y,ui->DATA_MAGN_Z,ui->ID_LSM6,ui->ID_LIS3);






}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
          count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        if(ui->NameSerialPort_Box->currentText()!="\0" &&
                                ui->SpeedSerialPort_Box->currentText()!="\0")
        {
            ui->NameSerialPort_Box->setEnabled(false);
            ui->SpeedSerialPort_Box->setEnabled(false);

            mainData->Start_Init_Model();
            bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                     (ui->SpeedSerialPort_Box->currentText()).toLong());
            RequestReadWrite=new QTimer();
            if(statusConfig)
            {
                QMessageBox::information(this,"Connection!!","Connection OK!");
            }
        }
        else
        {

            mainData->serial_Data->close();
            QMessageBox::information(this,"ERROR","Close Port");

        }
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    count_start_Transmit++;
    if(count_start_Transmit%2==0)
    {

        Activate_UI(true);
        mainData->RS_485_OK_Request();
        RequestReadWrite->start();

    }
    else
    {
        Activate_UI(false);
    }
}

void MainWindow::on_pushButton_clicked()
{
    mainData->RS_Request_Accel();
    mainData->Read_dataModel();//считываем ответ
    mainData->Update_UI_Model(ui->SERVICE_ACCEL_X,ui->SERVICE_ACCEL_Y,ui->SERVICE_ACCEL_Z,
                              ui->SERVICE_GYRO_X,ui->SERVICE_GYRO_Y,ui->SERVICE_GYRO_Z,
                              ui->SERVICE_MAGN_X,ui->SERVICE_MAGN_Y,ui->SERVICE_MAGN_Z,ui->ID_LSM6,ui->ID_LIS3);
}

void MainWindow::on_pushButton_2_clicked()
{
    mainData->RS_Request_Gyro();
    mainData->Read_dataModel();//считываем ответ
    mainData->Update_UI_Model(ui->SERVICE_ACCEL_X,ui->SERVICE_ACCEL_Y,ui->SERVICE_ACCEL_Z,
                              ui->SERVICE_GYRO_X,ui->SERVICE_GYRO_Y,ui->SERVICE_GYRO_Z,
                              ui->SERVICE_MAGN_X,ui->SERVICE_MAGN_Y,ui->SERVICE_MAGN_Z,ui->ID_LSM6,ui->ID_LIS3);
}

void MainWindow::on_pushButton_3_clicked()
{
     mainData->RS_Request_Magn();
     mainData->Read_dataModel();//считываем ответ
     mainData->Update_UI_Model(ui->SERVICE_ACCEL_X,ui->SERVICE_ACCEL_Y,ui->SERVICE_ACCEL_Z,
                               ui->SERVICE_GYRO_X,ui->SERVICE_GYRO_Y,ui->SERVICE_GYRO_Z,
                               ui->SERVICE_MAGN_X,ui->SERVICE_MAGN_Y,ui->SERVICE_MAGN_Z,ui->ID_LSM6,ui->ID_LIS3);

}

void MainWindow::on_Name_update_btn_clicked()
{

}

void MainWindow::on_RS_CONN_clicked()
{
    mainData->RS_485_OK_Request();
}
