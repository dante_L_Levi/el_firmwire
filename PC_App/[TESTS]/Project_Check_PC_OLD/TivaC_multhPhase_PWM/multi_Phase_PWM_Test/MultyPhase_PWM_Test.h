#ifndef MULTYPHASE_PWM_TEST_H
#define MULTYPHASE_PWM_TEST_H

#include <QObject>
#include "SerialPort/serialprotocol_data.h"


#define MODEL_SIZE                  8
#define INDEX_CMD                   2
#define OFFSET_DataPayload          3

#pragma pack(push, 1)

typedef struct
{

        uint16_t shift_Phase;
        uint16_t pwm_val;

}PWM_Value;



typedef  enum
{
    TivaC=0x00,
    STM32F334_NUCLEO=0xFF

}NameBoard;


typedef enum
{
    Status_Work=7,
    OUTPUT_Parse=6,
    INPUT_Parse=5,
    PWM_Parse=3,
    ADC_Parse=4
}Parse_data_Type;


typedef enum
{
    RS485_IDLE=0,
    RS485_Connect,
    RS485_Disconnect,
    Set_PWM_Value,
    Get_ADC_Values,
    Get_GPIO_Input_Flags,
    Set_GPIO_Output_Flags,
    Set_Status_flag


}command_hundler_MCU_def;




typedef struct
{
    uint16_t Id_board;
    NameBoard      _name;
    PWM_Value      _pwmData;
}Model_data_def;

#pragma pack(pop)


class MultyPhase_PWM_Test : public QObject
{
    Q_OBJECT
public:
    explicit MultyPhase_PWM_Test(QObject *parent = nullptr);

    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint16_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);

    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /************************Test Function Generate Struct**************************/
    void Test_GenerateData(void);
    /***********************Convert Read Buffer in Struct ****************************/
    void  Read_dataModel(void);
    /**********************Function Request Data *******************/
    void Main_Request_Data(command_hundler_MCU_def _type);


    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);

    /************************Request Set Data PWM********************/
    void RS_Request_Set_PWM_Value(uint16_t duty);


};

#endif // MULTYPHASE_PWM_TEST_H
