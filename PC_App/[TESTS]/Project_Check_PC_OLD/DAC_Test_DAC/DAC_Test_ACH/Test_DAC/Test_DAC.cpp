#include "Test_DAC.h"

static volatile const uint8_t _board_id [4] = {0x11,0xF7,'D','T'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;
Model_data_def data_model;

int16_t dataDAC[4];

Test_DAC::Test_DAC()
{

}

void Test_DAC::Start_Init_Model()
{
    serial_Data=new SerialProtocol_data();
   serial_Data->Set_ID(*rs485_id);



}

uint16_t Test_DAC::Get_Model_ID()
{
   return *rs485_id;
}

bool Test_DAC::Config_Settings(QString Name, long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
         return status;
}

void Test_DAC::Request_Board_Number(uint8_t index_request)
{
    switch (index_request)
    {

        case getBoardName:
        {

            break;
        }

        case getFirmware:
        {

            break;
        }



        case Get_DAC_Outputs:
        {

            break;
        }

        case Get_Dig_In:
        {

            break;
        }

    default:
        break;



    }
}

void Test_DAC::ParseToModelBytes(Parse_data_Type type, uint8_t *dt, uint8_t length)
{
    switch(type)
       {
           case getBoardName:
           {
                memcpy(&data_model.modelName,dt,length);
               break;
           }

           case getFirmware:
           {
                memcpy(&data_model.firmware,dt,length);
               break;
           }

           case Get_DAC_Outputs:
           {
               memcpy(&data_model._dac_out,dt,length);
               break;
           }

           case Get_Dig_In:
           {
               memcpy(&data_model._input_data,dt,length);
               break;
           }
            default:
                break;

       }
}

void Test_DAC::Request_TO_MCU(bool status)
{
    isRequest=status;
}

void Test_DAC::Test_GenerateData()
{

}

bool Test_DAC::Read_dataModel()
{
    QByteArray dataBuffer;
        dataBuffer=serial_Data->Get_CHECK_Result_Recieve_Data();
        if(dataBuffer!=""&& serial_Data->IsRx_DataReady())
        {
            uint8_t data[MODEL_SIZE]={0};


            if((uint8_t)dataBuffer[0]==_board_id[0]&&(uint8_t)dataBuffer[1]==_board_id[1])
            {

                for(int i=0;i<(int)MODEL_SIZE;i++)
                {
                    data[i]=dataBuffer[i+OFFSET_DataPayload];
                }
                uint8_t cmd=(uint8_t)dataBuffer[INDEX_CMD];
                //qDebug()<<"command:"<<cmd;
                Hundler_read_dataModel(cmd,data);

                //qDebug()<<"Input:"<<data_model.gpio_Input<<"OUTPUT:"<<data_model.gpio_Output<<"\r\n";
                qDebug()<<"GETPAYLOAD OK!!";
                serial_Data->Set_IsRxAvalible(false);
                return true;
            }

        }

            return false;

}

void Test_DAC::RS_485_OK_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,MODEL_SIZE);
}

void Test_DAC::RS_485_Close_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)RS485_Disconnect,buffer,MODEL_SIZE);
}

void Test_DAC::RS_485_SetDACx(uint8_t ch, int16_t val)
{

    switch (ch)
    {
            case 0:
        {
            data_model._dac_out.channelA=val;
            break;
        }

            case 1:
        {
            data_model._dac_out.channelB=val;
            break;
        }


    default:

        break;

    }


    uint8_t buffer[8]={0};
    memcpy(buffer,&data_model._dac_out,sizeof (_LTC2602_DEF));
    qDebug()<<"CHA:"<<data_model._dac_out.channelA;
    qDebug()<<"CHB:"<<data_model._dac_out.channelB;



    this->serial_Data->Transmit_Packet((CommandDef)Set_DAC_Outputs,buffer,MODEL_SIZE);
    //_LTC2602_DEF tesdt;
   //memcpy(&tesdt,buffer,sizeof (_LTC2602_DEF));
   // qDebug()<<"TEST_CHA:"<<tesdt.channelA;
   // qDebug()<<"TEST_CHB:"<<tesdt.channelB;

}



void Test_DAC::RS_Request_GetFirmwire()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)getFirmware,buffer,MODEL_SIZE);
}

void Test_DAC::RS_Request_GetNameBoard()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)getBoardName,buffer,MODEL_SIZE);

}

void Test_DAC::RS_Request_GetDigIn()
{
    uint8_t buffer[MODEL_SIZE]={0};

    this->serial_Data->Transmit_Packet((CommandDef)Get_Dig_In,buffer,MODEL_SIZE);
}

void Test_DAC::RS_Request_GetDAC_OUT()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Get_DAC_Outputs,buffer,MODEL_SIZE);
}

void Test_DAC::Update_UI_ID(QLabel *_id)
{
    _id->setText(QString::number(*rs485_id));
    qDebug()<<"update!!";
}

void Test_DAC::Update_UI_FIrmwire(QLabel *_Firmwire)
{
    _Firmwire->setText(data_model.firmware);
    qDebug()<<"update!!";
}

void Test_DAC::Update_UI_Name(QLabel *_Name)
{
    _Name->setText(data_model.modelName);
}

void Test_DAC::Update_UI(QLabel *_id, QLabel *_Name, QLabel *_Firmwire, QLabel *_DigIn)
{
     _id->setText(QString::number(*rs485_id));
      _Name->setText(data_model.modelName);
     _Firmwire->setText(data_model.firmware);
     _DigIn->setText(QString::number(data_model._input_data.AllData));
     qDebug()<<"update!!";

}

void Test_DAC::Hundler_read_dataModel(uint8_t status_cmd, uint8_t *dt)
{
    switch(status_cmd)
       {
           case getBoardName:
           {
               char boardname[8];
               memcpy(boardname,(void *)dt,sizeof(boardname));
               memcpy(data_model.modelName,(void *)boardname,sizeof (boardname));

               break;
           }

           case getFirmware:
           {
               char _firmwire[8];
               memcpy(_firmwire,(void *)dt,sizeof(_firmwire));
               memcpy(data_model.firmware,_firmwire,sizeof(_firmwire));
               break;
           }

            case Get_DAC_Outputs:
            {
                _LTC2602_DEF _data_out_DAC;;
                memcpy(&_data_out_DAC,(void *)dt,sizeof(_data_out_DAC));
                data_model._dac_test_in=_data_out_DAC;
                break;
            }

            case Get_Dig_In:
            {
                Digital_Input _input;
                memcpy(&_input,(void *)dt,sizeof(_input));
                data_model._input_data=_input;
                break;
            }

    }
}


