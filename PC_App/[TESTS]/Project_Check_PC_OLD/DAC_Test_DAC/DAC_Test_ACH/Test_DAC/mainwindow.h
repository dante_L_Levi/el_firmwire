#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SerialPort/serialprotocol_data.h"
#include "style_classes/window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"
#include "Test_DAC.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ValueDAC_1_valueChanged(int value);

    void on_pushButton_clicked();

    void on_ValueDAC_2_valueChanged(int value);



    void on_on_ConnectButton_clicked();

    void UPDATE_UI_TimRequest(void);

    void on_btn_CH_A1_clicked();

    void on_btn_CH_B1_clicked();

    void on_btn_CH_A2_clicked();

    void on_btn_CH_B2_clicked();

    void on_Name_update_btn_clicked();

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
    void Init_StartBox_Element(void);
    void Activate_UI(bool status);
};
#endif // MAINWINDOW_H
