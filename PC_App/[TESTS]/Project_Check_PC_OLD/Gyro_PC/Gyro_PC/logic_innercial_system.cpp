#include "logic_innercial_system.h"

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include<QRandomGenerator>
#include "QDebug"


typedef enum
{
    RS485_IDLE=0x00,
    RS485_Connect,
    RS485_Disconnect,
    Response_Model


}command_hundler_MCU_def;

Model_data_def data_model;
#define SIZE_PAYLOAD_LOGIC_DATA sizeof(Model_data_def)
const uint8_t ID_DEV=0x02;

float Sensitivity_Gyro=70.0;
float Sensitivity_Accel=0.488;
float Sensitivity_Magn=6842.0;
float Kpressure=4096.0;


Logic_Innercial_System::Logic_Innercial_System()
{

}



/******************Inicilize Model*****************************/
void Logic_Innercial_System:: Start_Init_Model(void)
{

        for(int i=0;i<3;i++)
        {
            data_model.Gyro[i]=0;
            data_model.Accel[i]=0;
        }
        data_model.ID=0;
        data_model.Temperature=0;
    serial_Data=new SerialProtocol_data();
    serial_Data->Set_ID(ID_DEV);

}

/**********************Get ID***************************/
uint8_t Logic_Innercial_System:: Get_Model_ID(void)
{
    return ID_DEV;
}

/************************Settings SerialPort**************************/
bool Logic_Innercial_System::Config_Settings(QString Name,long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
     return status;
}


/**************************Model Control Parse********************/
Model_data_def Logic_Innercial_System:: ParseToModelBytes(uint8_t* dt,uint8_t length)
{
    memcpy(&data_model,dt,length);
    return data_model;
}


/***********************Update Struct ************************/
void Logic_Innercial_System:: Update_DataStruct(Model_data_def *data)
{
    memcpy(&data_model,data,sizeof (Model_data_def));
}

/**********************Transmit Packet*******************/
void Logic_Innercial_System:: Transmit_DataModel(Model_data_def *dt)
{
    uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};
    memcpy((void*)buffer,dt,SIZE_PAYLOAD_LOGIC_DATA);
    serial_Data->Transmit_Packet((CommandDef)Response_Model,buffer,sizeof(buffer));
}

/**********************Transmit Packet*******************/
void Logic_Innercial_System:: Transmit_DataModel(Model_data_def *dt,uint8_t cmd)
{
    uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};
    memcpy((void*)buffer,dt,SIZE_PAYLOAD_LOGIC_DATA);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

/**********************Get Model*******************/
int16_t Logic_Innercial_System:: Get_Data_Model(int index)
{
    int16_t temp_data=0;
    switch(index)
    {
            case 1:
        {
            temp_data=data_model.Gyro[0];
            break;

        }
            case 2:
        {
            temp_data=data_model.Gyro[1];
            break;

        }
            case 3:
        {
            temp_data=data_model.Gyro[2];
            break;

        }
            case 4:
        {
            temp_data=data_model.Accel[0];
            break;

        }
            case 5:
        {
            temp_data=data_model.Accel[1];
            break;

        }
            case 6:
        {
            temp_data=data_model.Accel[2];
            break;

        }
            case 7:
        {
            temp_data=data_model.Temperature;
            break;

        }
        case 8:
        {
            temp_data=data_model.ID;
            break;
        }


    }

    return temp_data;
}


/**********************Function Request Data Enable*******************/
void Logic_Innercial_System:: Request_TO_MCU(bool status)
{
    isRequest=status;
}

/************************Test Function Generate Struct**************************/
void Logic_Innercial_System:: Test_GenerateData(void)
{
    data_model.ID=rand()%255;


    for(int i=0;i<3;i++)
    {
        data_model.Gyro[i]=rand()%1000-1000;
        data_model.Accel[i]=rand()%1000-1000;


    }
    data_model.Temperature=rand()%25+0;

}


/***********************Convert Read Buffer in Struct ****************************/
void Logic_Innercial_System::  Read_dataModel(void)
{
    QByteArray dataBuffer;
    dataBuffer=serial_Data->Get_CHECK_Result_Recieve_Data();
    if(dataBuffer!="")
    {
        uint8_t data[MODEL_SIZE]={0};
        if((uint8_t)(dataBuffer[0])==ID_DEV)
        {
            for(int i=0;i<(int)MODEL_SIZE;i++)
            {
                data[i]=dataBuffer[i+3];
            }

            memcpy(&data_model,(void *)data,sizeof(data_model));
            //qDebug()<<"Input:"<<data_model.gpio_Input<<"OUTPUT:"<<data_model.gpio_Output<<"\r\n";
        }

    }
    else
    {
        return;
    }

}


/**********************Function Request Data *******************/
void Logic_Innercial_System::  Main_Request_Data(void)
{
    if(isRequest)
        {
        uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};

        //buffer[0]=SIZE_PAYLOAD_LOGIC_DATA;

        this->serial_Data->Transmit_Packet((CommandDef)Response_Model,buffer,SIZE_PAYLOAD_LOGIC_DATA);

        }
        else
        {
            return;
        }
}


/****************************Update Interface*********************************************/
void Logic_Innercial_System:: Update_UI_Model(QLabel *_giroX,QLabel *_giroY,QLabel *_giroZ,

                     QLabel *_accelX,QLabel *_accelY,QLabel *_accelZ,QLabel* temp,
                     QLabel *ID_G_Accel)
{

    ID_G_Accel->setText(QString::number(data_model.ID));

    _giroX->setText(QString::number(data_model.Gyro[0]));
    _giroY->setText(QString::number(data_model.Gyro[1]));
    _giroZ->setText(QString::number(data_model.Gyro[2]));


    _accelX->setText(QString::number(data_model.Accel[0]));
    _accelY->setText(QString::number(data_model.Accel[1]));
    _accelZ->setText(QString::number(data_model.Accel[2]));


    temp->setText(QString::number(data_model.Temperature));




}




/*************************Request Start Connection****************/
void Logic_Innercial_System:: RS_485_OK_Request(void)
{
    if(isRequest)
        {
        uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};

        buffer[0]=SIZE_PAYLOAD_LOGIC_DATA;

        this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,SIZE_PAYLOAD_LOGIC_DATA);

        }
        else
        {
            return;
        }
}



