#include "Main_Data.h"

#define PI 3.14

Main_Data::Main_Data()
{
    for(int i=0;i<3;i++)
    {

         _mainData.A[i]=0;
         _mainData.G[i]=0;
         _mainData.M[i]=0;

    }
}

void Main_Data::Generate_Data()
{
    for(int i=0;i<3;i++)
    {

         _mainData.A[i]=rand()%16;
         _mainData.G[i]=((rand()%2000-2000)*PI)/180.0;
         _mainData.M[i]=rand()%10000-10000;

    }

}

Data_IMU Main_Data::Get_Data()
{
    return _mainData;
}

void Main_Data::Update_UI(QLabel *GX, QLabel *GY, QLabel *GZ, QLabel *AX, QLabel *AY, QLabel *AZ, QLabel *MX, QLabel *MY, QLabel *MZ)
{
    GX->setText(QString::number(_mainData.G[0]));
    GY->setText(QString::number(_mainData.G[1]));
    GZ->setText(QString::number(_mainData.G[2]));

    AX->setText(QString::number(_mainData.A[0]));
    AY->setText(QString::number(_mainData.A[1]));
    AZ->setText(QString::number(_mainData.A[2]));

    MX->setText(QString::number(_mainData.M[0]));
    MY->setText(QString::number(_mainData.M[1]));
    MZ->setText(QString::number(_mainData.M[2]));


}
