#ifndef MAIN_DATA_H
#define MAIN_DATA_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <QLabel>


#pragma pack(push,1)
typedef struct
{
    float G[3];//rad/s
    float A[3];//g
    float M[3];//Gauss


}Data_IMU;
#pragma pack(pop)



class Main_Data
{
public:
    Main_Data();
    void Generate_Data(void);
    Data_IMU Get_Data(void);
    void Update_UI(QLabel *GX,QLabel *GY,QLabel *GZ,
                   QLabel *AX,QLabel *AY,QLabel *AZ,
                   QLabel *MX,QLabel *MY,QLabel *MZ);


private:
    Data_IMU _mainData;
};

#endif // MAIN_DATA_H
