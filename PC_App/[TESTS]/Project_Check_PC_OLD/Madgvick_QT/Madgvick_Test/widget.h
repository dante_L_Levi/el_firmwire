#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include "Madgwick.h"
#include "Main_Data.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_clicked();
    void UPDATE_UI_Tim(void);

private:
    Ui::Widget          *ui;
    QTimer              *timeUpdate;
    Madgwick            *_madgv_alg;
    Main_Data           *IMU_SYS;

    float  Pitch;
    float  yaw;
    float  roll;

};
#endif // WIDGET_H
