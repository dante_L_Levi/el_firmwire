/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_15;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *G_1;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QLabel *G_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QLabel *G_3;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_4;
    QLabel *A_1;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_5;
    QLabel *A_2;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QLabel *A_3;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_7;
    QLabel *M_1;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_8;
    QLabel *M_2;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_9;
    QLabel *M_3;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_10;
    QLabel *PITCH;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_11;
    QLabel *ROLL;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_12;
    QLabel *YAW;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_13;
    QLabel *Q1;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_14;
    QLabel *Q2;
    QVBoxLayout *verticalLayout_19;
    QLabel *label_16;
    QLabel *Q3;
    QVBoxLayout *verticalLayout_18;
    QLabel *label_15;
    QLabel *Q4;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(364, 301);
        verticalLayout = new QVBoxLayout(Widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(Widget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_15 = new QVBoxLayout(tab);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);

        G_1 = new QLabel(tab);
        G_1->setObjectName(QString::fromUtf8("G_1"));
        G_1->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(G_1);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);

        G_2 = new QLabel(tab);
        G_2->setObjectName(QString::fromUtf8("G_2"));
        G_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(G_2);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_3);

        G_3 = new QLabel(tab);
        G_3->setObjectName(QString::fromUtf8("G_3"));
        G_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(G_3);


        horizontalLayout->addLayout(verticalLayout_4);


        verticalLayout_11->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_4);

        A_1 = new QLabel(tab);
        A_1->setObjectName(QString::fromUtf8("A_1"));
        A_1->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(A_1);


        horizontalLayout_2->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_5);

        A_2 = new QLabel(tab);
        A_2->setObjectName(QString::fromUtf8("A_2"));
        A_2->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(A_2);


        horizontalLayout_2->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_6);

        A_3 = new QLabel(tab);
        A_3->setObjectName(QString::fromUtf8("A_3"));
        A_3->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(A_3);


        horizontalLayout_2->addLayout(verticalLayout_7);


        verticalLayout_11->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_7);

        M_1 = new QLabel(tab);
        M_1->setObjectName(QString::fromUtf8("M_1"));
        M_1->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(M_1);


        horizontalLayout_3->addLayout(verticalLayout_8);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_8 = new QLabel(tab);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(label_8);

        M_2 = new QLabel(tab);
        M_2->setObjectName(QString::fromUtf8("M_2"));
        M_2->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(M_2);


        horizontalLayout_3->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_9 = new QLabel(tab);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_9);

        M_3 = new QLabel(tab);
        M_3->setObjectName(QString::fromUtf8("M_3"));
        M_3->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(M_3);


        horizontalLayout_3->addLayout(verticalLayout_10);


        verticalLayout_11->addLayout(horizontalLayout_3);


        verticalLayout_15->addLayout(verticalLayout_11);

        pushButton = new QPushButton(tab);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_15->addWidget(pushButton);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_10 = new QLabel(tab);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(label_10);

        PITCH = new QLabel(tab);
        PITCH->setObjectName(QString::fromUtf8("PITCH"));
        PITCH->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(PITCH);


        horizontalLayout_4->addLayout(verticalLayout_12);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_11 = new QLabel(tab);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(label_11);

        ROLL = new QLabel(tab);
        ROLL->setObjectName(QString::fromUtf8("ROLL"));
        ROLL->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(ROLL);


        horizontalLayout_4->addLayout(verticalLayout_13);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        label_12 = new QLabel(tab);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(label_12);

        YAW = new QLabel(tab);
        YAW->setObjectName(QString::fromUtf8("YAW"));
        YAW->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(YAW);


        horizontalLayout_4->addLayout(verticalLayout_14);


        verticalLayout_15->addLayout(horizontalLayout_4);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_5 = new QHBoxLayout(tab_2);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_13 = new QLabel(tab_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(label_13);

        Q1 = new QLabel(tab_2);
        Q1->setObjectName(QString::fromUtf8("Q1"));
        Q1->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(Q1);


        horizontalLayout_5->addLayout(verticalLayout_16);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_14 = new QLabel(tab_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(label_14);

        Q2 = new QLabel(tab_2);
        Q2->setObjectName(QString::fromUtf8("Q2"));
        Q2->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(Q2);


        horizontalLayout_5->addLayout(verticalLayout_17);

        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        label_16 = new QLabel(tab_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setAlignment(Qt::AlignCenter);

        verticalLayout_19->addWidget(label_16);

        Q3 = new QLabel(tab_2);
        Q3->setObjectName(QString::fromUtf8("Q3"));
        Q3->setAlignment(Qt::AlignCenter);

        verticalLayout_19->addWidget(Q3);


        horizontalLayout_5->addLayout(verticalLayout_19);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        label_15 = new QLabel(tab_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setAlignment(Qt::AlignCenter);

        verticalLayout_18->addWidget(label_15);

        Q4 = new QLabel(tab_2);
        Q4->setObjectName(QString::fromUtf8("Q4"));
        Q4->setAlignment(Qt::AlignCenter);

        verticalLayout_18->addWidget(Q4);


        horizontalLayout_5->addLayout(verticalLayout_18);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);


        retranslateUi(Widget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        label->setText(QApplication::translate("Widget", "GX:", nullptr));
        G_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_2->setText(QApplication::translate("Widget", "GY:", nullptr));
        G_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_3->setText(QApplication::translate("Widget", "GZ:", nullptr));
        G_3->setText(QApplication::translate("Widget", "0", nullptr));
        label_4->setText(QApplication::translate("Widget", "AX:", nullptr));
        A_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_5->setText(QApplication::translate("Widget", "AY:", nullptr));
        A_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_6->setText(QApplication::translate("Widget", "AZ:", nullptr));
        A_3->setText(QApplication::translate("Widget", "0", nullptr));
        label_7->setText(QApplication::translate("Widget", "MX:", nullptr));
        M_1->setText(QApplication::translate("Widget", "0", nullptr));
        label_8->setText(QApplication::translate("Widget", "MY:", nullptr));
        M_2->setText(QApplication::translate("Widget", "0", nullptr));
        label_9->setText(QApplication::translate("Widget", "MZ:", nullptr));
        M_3->setText(QApplication::translate("Widget", "0", nullptr));
        pushButton->setText(QApplication::translate("Widget", "Start Request", nullptr));
        label_10->setText(QApplication::translate("Widget", "pitch:", nullptr));
        PITCH->setText(QApplication::translate("Widget", "0", nullptr));
        label_11->setText(QApplication::translate("Widget", "roll:", nullptr));
        ROLL->setText(QApplication::translate("Widget", "0", nullptr));
        label_12->setText(QApplication::translate("Widget", "yaw:", nullptr));
        YAW->setText(QApplication::translate("Widget", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Widget", "\320\236\320\261\321\211\320\270\320\265 \320\264\320\260\320\275\320\275\321\213\320\265", nullptr));
        label_13->setText(QApplication::translate("Widget", "Q1:", nullptr));
        Q1->setText(QApplication::translate("Widget", "0", nullptr));
        label_14->setText(QApplication::translate("Widget", "Q2:", nullptr));
        Q2->setText(QApplication::translate("Widget", "0", nullptr));
        label_16->setText(QApplication::translate("Widget", "Q3:", nullptr));
        Q3->setText(QApplication::translate("Widget", "0", nullptr));
        label_15->setText(QApplication::translate("Widget", "Q4:", nullptr));
        Q4->setText(QApplication::translate("Widget", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Widget", "\320\232\320\262\320\260\321\202\320\265\321\200\320\275\320\270\320\276\320\275\321\213", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
