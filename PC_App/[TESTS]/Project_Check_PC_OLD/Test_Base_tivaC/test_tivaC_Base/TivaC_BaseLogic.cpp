#include "TivaC_BaseLogic.h"


#define K_adc           3.3/4095.0
const uint8_t ID_DEV=0x05;
Model_data_def data_model;



TivaC_BaseLogic::TivaC_BaseLogic()
{

}



void TivaC_BaseLogic::Start_Init_Model()
{

    serial_Data=new SerialProtocol_data();
    serial_Data->Set_ID(ID_DEV);

}

uint16_t TivaC_BaseLogic::Get_Model_ID()
{
    return ID_DEV;
}

bool TivaC_BaseLogic::Config_Settings(QString Name, long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
     return status;
}

void TivaC_BaseLogic::ParseToModelBytes(Parse_data_Type type, uint8_t *dt, uint8_t length)
{
    switch(type)
    {
        case PWM_Parse:
        {
             memcpy(&data_model._pwmData,dt,length);
            break;
        }

        case ADC_Parse:
        {
             memcpy(&data_model._ainput_value,dt,length);
            break;
        }

        case INPUT_Parse:
        {
            memcpy(&data_model._dinput_status,dt,length);
            break;
        }

        case OUTPUT_Parse:
        {
            memcpy(&data_model._doutput_status,dt,length);
            break;
        }
        case Status_Work:
        {
            memcpy(&data_model._workStatus,dt,length);
            break;
        }
    }
}

void TivaC_BaseLogic::Update_DataStruct(Digital_Output *data)
{

     memcpy(&data_model._doutput_status,data,sizeof (Digital_Output));
}

void TivaC_BaseLogic::Update_DataStruct(Digital_Input *data)
{
     memcpy(&data_model._dinput_status,data,sizeof (Digital_Input));
}

void TivaC_BaseLogic::Update_DataStruct(Analog_Input *data)
{
    memcpy(&data_model._ainput_value,data,sizeof (Analog_Input));
}

void TivaC_BaseLogic::Update_DataStruct(Flag_Work *data)
{
    memcpy(&data_model._workStatus,data,sizeof (Flag_Work));
}

void TivaC_BaseLogic::Transmit_DataModel(Digital_Output *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Set_GPIO_Output_Flags,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Digital_Input *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Get_GPIO_Input_Flags,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Analog_Input *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Get_ADC_Values,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Flag_Work *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Set_Status_flag,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Digital_Output *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Digital_Input *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Analog_Input *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Transmit_DataModel(Flag_Work *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void TivaC_BaseLogic::Request_TO_MCU(bool status)
{
    isRequest=status;
}

void TivaC_BaseLogic::Test_GenerateData()
{

    data_model._ainput_value.ADC_In1=rand()%4095;
    data_model._ainput_value.ADC_In2=rand()%4095;
    data_model._ainput_value.ADC_In3=rand()%4095;
    data_model._ainput_value.ADC_In4=rand()%4095;

    data_model._dinput_status.AllData=rand()%255;


}

void TivaC_BaseLogic::Read_dataModel()
{
    QByteArray dataBuffer;
    dataBuffer=serial_Data->Get_CHECK_Result_Recieve_Data();
    if(dataBuffer!="")
    {
        uint8_t data[MODEL_SIZE]={0};
        if((uint8_t)(dataBuffer[0])==ID_DEV)
        {

            for(int i=0;i<(int)MODEL_SIZE;i++)
            {
                data[i]=dataBuffer[i+OFFSET_DataPayload];
            }
            Hundler_read_dataModel(dataBuffer[INDEX_CMD],data);

            //qDebug()<<"Input:"<<data_model.gpio_Input<<"OUTPUT:"<<data_model.gpio_Output<<"\r\n";
        }

    }
    else
    {
        return;
    }

}

void TivaC_BaseLogic::Main_Request_Data(command_hundler_MCU_def _type)
{
    if(isRequest)
        {

        uint8_t buffer[MODEL_SIZE]={0};
        this->serial_Data->Transmit_Packet((CommandDef)_type,buffer,MODEL_SIZE);
        }
        else
        {
            return;
        }
}

void TivaC_BaseLogic::RS_485_OK_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,MODEL_SIZE);
}

void TivaC_BaseLogic::RS_Request_Output(uint8_t status)
{
    uint8_t buffer[MODEL_SIZE]={0};
    data_model._doutput_status.AllData=status;
    memcpy(buffer,&data_model._doutput_status,sizeof(data_model._doutput_status));
    this->serial_Data->Transmit_Packet((CommandDef)Set_GPIO_Output_Flags,buffer,MODEL_SIZE);
}

void TivaC_BaseLogic::RS_Request_Input()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Get_GPIO_Input_Flags,buffer,MODEL_SIZE);
}

void TivaC_BaseLogic::RS_Request_Analog_Input()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Get_ADC_Values,buffer,MODEL_SIZE);
}

void TivaC_BaseLogic::RS_Request_flagWork(uint8_t status)
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=status;
    this->serial_Data->Transmit_Packet((CommandDef)Set_GPIO_Output_Flags,buffer,MODEL_SIZE);

}

void TivaC_BaseLogic::RS_Request_Set_PWM_Value(uint16_t duty)
{
    uint8_t buffer[MODEL_SIZE]={0};
    data_model._pwmData.pwm_val=duty;
    memcpy(buffer,&data_model._pwmData,sizeof(data_model._pwmData));
    this->serial_Data->Transmit_Packet((CommandDef)Set_PWM_Value,buffer,MODEL_SIZE);
}

void TivaC_BaseLogic::Service_Update_UI(QLabel *_dataService1,QLabel *_dataService2,QLabel *_dataService3,QLabel *_dataService4,command_hundler_MCU_def _UpdateType)
{


    switch (_UpdateType)
    {
        case Set_PWM_Value:
        {
            _dataService1->setText(QString::number(data_model._pwmData.pwm_val));
            break;
        }
        case Get_ADC_Values:
        {

             _dataService1->setText(QString::number(data_model._ainput_value.ADC_In1));
              _dataService2->setText(QString::number(data_model._ainput_value.ADC_In2));
               _dataService3->setText(QString::number(data_model._ainput_value.ADC_In3));
                _dataService4->setText(QString::number(data_model._ainput_value.ADC_In4));

            break;
        }

        case Get_GPIO_Input_Flags:
        {
            _dataService1->setText(QString::number(data_model._dinput_status.AllData));
            break;
        }

    case Set_GPIO_Output_Flags:
    {
          _dataService1->setText(QString::number(data_model._doutput_status.AllData));
        break;
    }
        default:
        {

            break;
        }

    }
}

void TivaC_BaseLogic::Update_UI(QLabel *ADC1, QLabel *ADC2, QLabel *ADC3, QLabel *ADC4, QCheckBox *In1, QCheckBox *In2, QCheckBox *In3, QCheckBox *In4, QCheckBox *In5, QCheckBox *In6, QCheckBox *In7, QCheckBox *In8,
                                command_hundler_MCU_def _UpdateType)
{
    switch(_UpdateType)
    {
        case Get_ADC_Values:
        {
            ADC1->setText(QString::number((float)(data_model._ainput_value.ADC_In1/1000.0)));
            ADC2->setText(QString::number((float)(data_model._ainput_value.ADC_In2/1000.0)));
            ADC3->setText(QString::number((float)(data_model._ainput_value.ADC_In3/1000.0)));
            ADC4->setText(QString::number((float)(data_model._ainput_value.ADC_In4/1000.0)));

            break;
        }

        case Get_GPIO_Input_Flags:
        {


            data_model._dinput_status.fields.In1!=0?In1->setChecked(true):In1->setChecked(false);
            data_model._dinput_status.fields.In2!=0?In2->setChecked(true):In2->setChecked(false);
            data_model._dinput_status.fields.In3!=0?In3->setChecked(true):In3->setChecked(false);
            data_model._dinput_status.fields.In4!=0?In4->setChecked(true):In4->setChecked(false);
            data_model._dinput_status.fields.In5!=0?In5->setChecked(true):In5->setChecked(false);
            data_model._dinput_status.fields.In6!=0?In6->setChecked(true):In6->setChecked(false);
            data_model._dinput_status.fields.In7!=0?In7->setChecked(true):In7->setChecked(false);
            data_model._dinput_status.fields.In8!=0?In8->setChecked(true):In8->setChecked(false);
            break;
        }
    default:
    {

        break;
    }
    }






}



void TivaC_BaseLogic::Hundler_read_dataModel(uint8_t status_cmd, uint8_t *dt)
{
    switch(status_cmd)
    {
        case Get_GPIO_Input_Flags:
        {
            Digital_Input _Data;_Data.AllData=0x00;
            memcpy(&_Data,(void *)dt,sizeof(_Data));
            data_model._dinput_status=_Data;
            break;
        }

        case Get_ADC_Values:
        {
            Analog_Input _Data={0,0,0,0};
            memcpy(&_Data,(void *)dt,sizeof(_Data));
            data_model._ainput_value=_Data;
            break;
        }

    }
}





