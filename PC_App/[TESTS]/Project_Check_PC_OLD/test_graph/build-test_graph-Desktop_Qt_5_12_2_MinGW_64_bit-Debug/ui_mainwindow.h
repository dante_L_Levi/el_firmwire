/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *Btn_PlotGraph;
    QPushButton *Generate_Data;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *GYRO_1;
    QLabel *GYRO_2;
    QLabel *GYRO_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *ACCEL_1;
    QLabel *ACCEL_2;
    QLabel *ACCEL_3;
    QWidget *widget1;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *ADC_1;
    QLabel *ADC_2;
    QLabel *ADC_3;
    QLabel *ADC_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *ADC_5;
    QLabel *ADC_6;
    QLabel *ADC_7;
    QLabel *ADC_8;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(315, 152);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        Btn_PlotGraph = new QPushButton(centralwidget);
        Btn_PlotGraph->setObjectName(QString::fromUtf8("Btn_PlotGraph"));
        Btn_PlotGraph->setGeometry(QRect(9, 16, 80, 21));
        Generate_Data = new QPushButton(centralwidget);
        Generate_Data->setObjectName(QString::fromUtf8("Generate_Data"));
        Generate_Data->setGeometry(QRect(9, 50, 104, 21));
        widget = new QWidget(centralwidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(170, 10, 141, 51));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        GYRO_1 = new QLabel(widget);
        GYRO_1->setObjectName(QString::fromUtf8("GYRO_1"));
        GYRO_1->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(GYRO_1);

        GYRO_2 = new QLabel(widget);
        GYRO_2->setObjectName(QString::fromUtf8("GYRO_2"));
        GYRO_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(GYRO_2);

        GYRO_3 = new QLabel(widget);
        GYRO_3->setObjectName(QString::fromUtf8("GYRO_3"));
        GYRO_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(GYRO_3);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        ACCEL_1 = new QLabel(widget);
        ACCEL_1->setObjectName(QString::fromUtf8("ACCEL_1"));
        ACCEL_1->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(ACCEL_1);

        ACCEL_2 = new QLabel(widget);
        ACCEL_2->setObjectName(QString::fromUtf8("ACCEL_2"));
        ACCEL_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(ACCEL_2);

        ACCEL_3 = new QLabel(widget);
        ACCEL_3->setObjectName(QString::fromUtf8("ACCEL_3"));
        ACCEL_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(ACCEL_3);


        horizontalLayout->addLayout(verticalLayout_2);

        widget1 = new QWidget(centralwidget);
        widget1->setObjectName(QString::fromUtf8("widget1"));
        widget1->setGeometry(QRect(170, 70, 141, 71));
        horizontalLayout_2 = new QHBoxLayout(widget1);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        ADC_1 = new QLabel(widget1);
        ADC_1->setObjectName(QString::fromUtf8("ADC_1"));
        ADC_1->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(ADC_1);

        ADC_2 = new QLabel(widget1);
        ADC_2->setObjectName(QString::fromUtf8("ADC_2"));
        ADC_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(ADC_2);

        ADC_3 = new QLabel(widget1);
        ADC_3->setObjectName(QString::fromUtf8("ADC_3"));
        ADC_3->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(ADC_3);

        ADC_4 = new QLabel(widget1);
        ADC_4->setObjectName(QString::fromUtf8("ADC_4"));
        ADC_4->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(ADC_4);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        ADC_5 = new QLabel(widget1);
        ADC_5->setObjectName(QString::fromUtf8("ADC_5"));
        ADC_5->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(ADC_5);

        ADC_6 = new QLabel(widget1);
        ADC_6->setObjectName(QString::fromUtf8("ADC_6"));
        ADC_6->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(ADC_6);

        ADC_7 = new QLabel(widget1);
        ADC_7->setObjectName(QString::fromUtf8("ADC_7"));
        ADC_7->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(ADC_7);

        ADC_8 = new QLabel(widget1);
        ADC_8->setObjectName(QString::fromUtf8("ADC_8"));
        ADC_8->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(ADC_8);


        horizontalLayout_2->addLayout(verticalLayout_4);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        Btn_PlotGraph->setText(QApplication::translate("MainWindow", "\320\223\321\200\320\260\321\204\320\270\320\272\320\270", nullptr));
        Generate_Data->setText(QApplication::translate("MainWindow", "\320\223\320\265\320\275\320\265\321\200\320\260\321\206\320\270\321\217 \320\264\320\260\320\275\320\275\321\213\321\205", nullptr));
        GYRO_1->setText(QApplication::translate("MainWindow", "0", nullptr));
        GYRO_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        GYRO_3->setText(QApplication::translate("MainWindow", "0", nullptr));
        ACCEL_1->setText(QApplication::translate("MainWindow", "0", nullptr));
        ACCEL_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        ACCEL_3->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_1->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_3->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_4->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_5->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_6->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_7->setText(QApplication::translate("MainWindow", "0", nullptr));
        ADC_8->setText(QApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
