/********************************************************************************
** Form generated from reading UI file 'formgraph.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMGRAPH_H
#define UI_FORMGRAPH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_formgraph
{
public:
    QGridLayout *gridLayout;
    QCustomPlot *CUSTOMEPLOT;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboBoxSize;
    QPushButton *pushButtonReset;
    QPushButton *pushButtonPause;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonColor;
    QComboBox *comboBoxGraph;
    QPushButton *pushButtonAdd;
    QPushButton *pushButtonAxis;

    void setupUi(QWidget *formgraph)
    {
        if (formgraph->objectName().isEmpty())
            formgraph->setObjectName(QString::fromUtf8("formgraph"));
        formgraph->resize(602, 399);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(formgraph->sizePolicy().hasHeightForWidth());
        formgraph->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(formgraph);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        CUSTOMEPLOT = new QCustomPlot(formgraph);
        CUSTOMEPLOT->setObjectName(QString::fromUtf8("CUSTOMEPLOT"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(CUSTOMEPLOT->sizePolicy().hasHeightForWidth());
        CUSTOMEPLOT->setSizePolicy(sizePolicy1);
        CUSTOMEPLOT->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(CUSTOMEPLOT, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        comboBoxSize = new QComboBox(formgraph);
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->addItem(QString());
        comboBoxSize->setObjectName(QString::fromUtf8("comboBoxSize"));

        horizontalLayout->addWidget(comboBoxSize);

        pushButtonReset = new QPushButton(formgraph);
        pushButtonReset->setObjectName(QString::fromUtf8("pushButtonReset"));

        horizontalLayout->addWidget(pushButtonReset);

        pushButtonPause = new QPushButton(formgraph);
        pushButtonPause->setObjectName(QString::fromUtf8("pushButtonPause"));

        horizontalLayout->addWidget(pushButtonPause);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButtonColor = new QPushButton(formgraph);
        pushButtonColor->setObjectName(QString::fromUtf8("pushButtonColor"));

        horizontalLayout->addWidget(pushButtonColor);

        comboBoxGraph = new QComboBox(formgraph);
        comboBoxGraph->setObjectName(QString::fromUtf8("comboBoxGraph"));
        comboBoxGraph->setEditable(false);

        horizontalLayout->addWidget(comboBoxGraph);

        pushButtonAdd = new QPushButton(formgraph);
        pushButtonAdd->setObjectName(QString::fromUtf8("pushButtonAdd"));

        horizontalLayout->addWidget(pushButtonAdd);

        pushButtonAxis = new QPushButton(formgraph);
        pushButtonAxis->setObjectName(QString::fromUtf8("pushButtonAxis"));

        horizontalLayout->addWidget(pushButtonAxis);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(formgraph);

        comboBoxSize->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(formgraph);
    } // setupUi

    void retranslateUi(QWidget *formgraph)
    {
        formgraph->setWindowTitle(QApplication::translate("formgraph", "Form", nullptr));
        comboBoxSize->setItemText(0, QApplication::translate("formgraph", "1", nullptr));
        comboBoxSize->setItemText(1, QApplication::translate("formgraph", "2", nullptr));
        comboBoxSize->setItemText(2, QApplication::translate("formgraph", "5", nullptr));
        comboBoxSize->setItemText(3, QApplication::translate("formgraph", "10", nullptr));
        comboBoxSize->setItemText(4, QApplication::translate("formgraph", "20", nullptr));
        comboBoxSize->setItemText(5, QApplication::translate("formgraph", "50", nullptr));
        comboBoxSize->setItemText(6, QApplication::translate("formgraph", "100", nullptr));
        comboBoxSize->setItemText(7, QApplication::translate("formgraph", "200", nullptr));
        comboBoxSize->setItemText(8, QApplication::translate("formgraph", "500", nullptr));
        comboBoxSize->setItemText(9, QApplication::translate("formgraph", "1000", nullptr));
        comboBoxSize->setItemText(10, QApplication::translate("formgraph", "2000", nullptr));
        comboBoxSize->setItemText(11, QApplication::translate("formgraph", "5000", nullptr));
        comboBoxSize->setItemText(12, QApplication::translate("formgraph", "10000", nullptr));

        pushButtonReset->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201", nullptr));
        pushButtonPause->setText(QApplication::translate("formgraph", "\320\237\320\260\321\203\320\267\320\260", nullptr));
        pushButtonColor->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201\320\270\321\202\321\214 \321\206\320\262\320\265\321\202\320\260", nullptr));
        pushButtonAdd->setText(QApplication::translate("formgraph", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        pushButtonAxis->setText(QApplication::translate("formgraph", "--", nullptr));
    } // retranslateUi

};

namespace Ui {
    class formgraph: public Ui_formgraph {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMGRAPH_H
