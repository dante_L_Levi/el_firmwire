#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SerialPort/serialprotocol_data.h"
#include "style_classes/window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"
#include "DAC_Logic.h"





QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

     void Activate_UI(bool status);


private slots:

     void UPDATE_UI_TimRequest(void);




    void on_pushButton_clicked();

    void on_Name_update_btn_clicked();

    void on_on_ConnectButton_clicked();

    void on_Sld_A1_valueChanged(int value);

    void on_Sld_A2_valueChanged(int value);

    void on_Sld_B1_valueChanged(int value);

    void on_Sld_B2_valueChanged(int value);

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
    void Init_StartBox_Element(void);
};
#endif // MAINWINDOW_H
