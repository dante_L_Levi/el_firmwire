#include "DAC_Logic.h"

const uint8_t ID_DEV=0x0C;
Model_data_def data_model;





DAC_Logic::DAC_Logic()
{

}


void DAC_Logic::Start_Init_Model()
{

    serial_Data=new SerialProtocol_data();
    serial_Data->Set_ID(ID_DEV);

}

uint16_t DAC_Logic::Get_Model_ID()
{
    return ID_DEV;
}

bool DAC_Logic::Config_Settings(QString Name, long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
    return status;
}

void DAC_Logic::Request_TO_MCU(bool status)
{
    isRequest=status;
}



void DAC_Logic::RS_485_OK_Request()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,MODEL_SIZE);

}

void DAC_Logic::Set_request_DAC(command_hundler_MCU_def _ch, uint16_t out)
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=(uint8_t)out;
    buffer[1]=(uint8_t)(out>>8);
    this->serial_Data->Transmit_Packet((CommandDef)_ch,buffer,MODEL_SIZE);
}


