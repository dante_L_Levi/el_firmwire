#ifndef DAC_LOGIC_H
#define DAC_LOGIC_H


#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"
#include "SerialPort/serialprotocol_data.h"

#define MODEL_SIZE                  8
#define INDEX_CMD                   2
#define OFFSET_DataPayload          3


#pragma pack(push, 1)

typedef struct
{
    uint16_t DAC_CHANNEL_A[2];
    uint16_t DAC_CHANNEL_B[2];
}DAC_OUTPUT_Data;



typedef struct
{
    DAC_OUTPUT_Data      _outDac;

}Model_data_def;

#pragma pack(pop)


typedef enum
{
    Status_Work=7,
    OUTPUT_Parse=6,
    INPUT_Parse=5,
    PWM_Parse=3,
    ADC_Parse=4
}Parse_data_Type;

typedef enum
{
    RS485_IDLE=0,
    RS485_Connect,
    RS485_Disconnect,
    Set_CHANNELA1,
    Set_CHANNELA2,
    Set_CHANNELB1,
    Set_CHANNELB2


}command_hundler_MCU_def;

class DAC_Logic
{
public:
    DAC_Logic();

    bool isRequest;
    SerialProtocol_data *serial_Data;
    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint16_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);


    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);
    /*************************Request DAC OUT SET****************/
    void Set_request_DAC(command_hundler_MCU_def _ch,uint16_t out);


private:

    void Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt);


};

#endif // DAC_LOGIC_H
