/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_15;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *VREF;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *IREF;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *Vin;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *KPD;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLineEdit *N21;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_6;
    QLineEdit *N11;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QLineEdit *K;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_13;
    QLineEdit *RLoad;
    QComboBox *comboBox_2;
    QPushButton *pushButton_3;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_8;
    QLineEdit *Kp;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_9;
    QLineEdit *Ki;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QLineEdit *Kd;
    QComboBox *comboBox;
    QPushButton *pushButton_2;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_15;
    QLineEdit *Duty;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_11;
    QLineEdit *Error;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_12;
    QLineEdit *Vout;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_14;
    QLineEdit *Iout;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(585, 379);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout_15 = new QHBoxLayout(centralwidget);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        VREF = new QLineEdit(groupBox);
        VREF->setObjectName(QString::fromUtf8("VREF"));

        horizontalLayout->addWidget(VREF);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        IREF = new QLineEdit(groupBox);
        IREF->setObjectName(QString::fromUtf8("IREF"));

        horizontalLayout_2->addWidget(IREF);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        Vin = new QLineEdit(groupBox);
        Vin->setObjectName(QString::fromUtf8("Vin"));

        horizontalLayout_3->addWidget(Vin);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        KPD = new QLineEdit(groupBox);
        KPD->setObjectName(QString::fromUtf8("KPD"));

        horizontalLayout_4->addWidget(KPD);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout_4->addWidget(groupBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_5->addWidget(label_5);

        N21 = new QLineEdit(centralwidget);
        N21->setObjectName(QString::fromUtf8("N21"));

        horizontalLayout_5->addWidget(N21);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_6->addWidget(label_6);

        N11 = new QLineEdit(centralwidget);
        N11->setObjectName(QString::fromUtf8("N11"));

        horizontalLayout_6->addWidget(N11);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_7->addWidget(label_7);

        K = new QLineEdit(centralwidget);
        K->setObjectName(QString::fromUtf8("K"));

        horizontalLayout_7->addWidget(K);


        verticalLayout->addLayout(horizontalLayout_7);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);


        verticalLayout_4->addLayout(verticalLayout);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_13 = new QLabel(centralwidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_13->addWidget(label_13);

        RLoad = new QLineEdit(centralwidget);
        RLoad->setObjectName(QString::fromUtf8("RLoad"));

        horizontalLayout_13->addWidget(RLoad);


        verticalLayout_4->addLayout(horizontalLayout_13);

        comboBox_2 = new QComboBox(centralwidget);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        verticalLayout_4->addWidget(comboBox_2);

        pushButton_3 = new QPushButton(centralwidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_4->addWidget(pushButton_3);


        horizontalLayout_15->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_8->addWidget(label_8);

        Kp = new QLineEdit(groupBox_2);
        Kp->setObjectName(QString::fromUtf8("Kp"));

        horizontalLayout_8->addWidget(Kp);


        verticalLayout_3->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_9->addWidget(label_9);

        Ki = new QLineEdit(groupBox_2);
        Ki->setObjectName(QString::fromUtf8("Ki"));

        horizontalLayout_9->addWidget(Ki);


        verticalLayout_3->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_10->addWidget(label_10);

        Kd = new QLineEdit(groupBox_2);
        Kd->setObjectName(QString::fromUtf8("Kd"));

        horizontalLayout_10->addWidget(Kd);


        verticalLayout_3->addLayout(horizontalLayout_10);

        comboBox = new QComboBox(groupBox_2);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        verticalLayout_3->addWidget(comboBox);

        pushButton_2 = new QPushButton(groupBox_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_3->addWidget(pushButton_2);


        verticalLayout_5->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(centralwidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_6 = new QVBoxLayout(groupBox_3);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_15 = new QLabel(groupBox_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_16->addWidget(label_15);

        Duty = new QLineEdit(groupBox_3);
        Duty->setObjectName(QString::fromUtf8("Duty"));

        horizontalLayout_16->addWidget(Duty);


        verticalLayout_6->addLayout(horizontalLayout_16);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_11->addWidget(label_11);

        Error = new QLineEdit(groupBox_3);
        Error->setObjectName(QString::fromUtf8("Error"));

        horizontalLayout_11->addWidget(Error);


        verticalLayout_6->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_12->addWidget(label_12);

        Vout = new QLineEdit(groupBox_3);
        Vout->setObjectName(QString::fromUtf8("Vout"));

        horizontalLayout_12->addWidget(Vout);


        verticalLayout_6->addLayout(horizontalLayout_12);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_14->addWidget(label_14);

        Iout = new QLineEdit(groupBox_3);
        Iout->setObjectName(QString::fromUtf8("Iout"));

        horizontalLayout_14->addWidget(Iout);


        verticalLayout_6->addLayout(horizontalLayout_14);


        verticalLayout_5->addWidget(groupBox_3);


        horizontalLayout_15->addLayout(verticalLayout_5);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Config:", nullptr));
        label->setText(QApplication::translate("MainWindow", "Vref", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Iref", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Vin Start:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "KPD:", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "N21:", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "N11:", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "K::", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "calculate", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "Rl", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "Start calculate", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "PID", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Ki:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Kd:", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "Set", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "OUT", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Duty:", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Error", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Vout", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Iout", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
