/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QLabel *label_17;
    QLabel *Load;
    QLineEdit *VoltageInput;
    QLineEdit *RLoad;
    QGroupBox *groupBox_2;
    QWidget *layoutWidget_7;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_9;
    QLineEdit *SAT_C_M;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_10;
    QLineEdit *SAT_C_P;
    QWidget *layoutWidget_8;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_11;
    QLineEdit *CURRENT_Kp;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_12;
    QLineEdit *CURRENT_Ki;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_13;
    QLineEdit *CURRENT_Kd;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_14;
    QLineEdit *CURRENT_MAX;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_15;
    QLineEdit *CURRENT_MIN;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_16;
    QLineEdit *CURRENT_REF;
    QGroupBox *groupBox;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *SAT_V_M;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QLineEdit *SAT_V_P;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLineEdit *Voltage_Kp;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLineEdit *Voltage_Ki;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QLineEdit *Voltage_Kd;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QLineEdit *Voltage_MAX;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_7;
    QLineEdit *Voltage_MIN;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_8;
    QLineEdit *Voltage_Ref;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *MainData;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(471, 615);
        MainWindow->setMaximumSize(QSize(471, 1000));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_17 = new QLabel(centralwidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_17, 0, 0, 1, 1);

        Load = new QLabel(centralwidget);
        Load->setObjectName(QString::fromUtf8("Load"));
        Load->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(Load, 0, 1, 1, 1);

        VoltageInput = new QLineEdit(centralwidget);
        VoltageInput->setObjectName(QString::fromUtf8("VoltageInput"));

        gridLayout->addWidget(VoltageInput, 1, 0, 1, 1);

        RLoad = new QLineEdit(centralwidget);
        RLoad->setObjectName(QString::fromUtf8("RLoad"));

        gridLayout->addWidget(RLoad, 1, 1, 1, 1);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(221, 271));
        layoutWidget_7 = new QWidget(groupBox_2);
        layoutWidget_7->setObjectName(QString::fromUtf8("layoutWidget_7"));
        layoutWidget_7->setGeometry(QRect(10, 20, 191, 49));
        horizontalLayout_8 = new QHBoxLayout(layoutWidget_7);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_9 = new QLabel(layoutWidget_7);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_9);

        SAT_C_M = new QLineEdit(layoutWidget_7);
        SAT_C_M->setObjectName(QString::fromUtf8("SAT_C_M"));

        verticalLayout_4->addWidget(SAT_C_M);


        horizontalLayout_8->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_10 = new QLabel(layoutWidget_7);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_10);

        SAT_C_P = new QLineEdit(layoutWidget_7);
        SAT_C_P->setObjectName(QString::fromUtf8("SAT_C_P"));

        verticalLayout_5->addWidget(SAT_C_P);


        horizontalLayout_8->addLayout(verticalLayout_5);

        layoutWidget_8 = new QWidget(groupBox_2);
        layoutWidget_8->setObjectName(QString::fromUtf8("layoutWidget_8"));
        layoutWidget_8->setGeometry(QRect(10, 90, 167, 181));
        verticalLayout_6 = new QVBoxLayout(layoutWidget_8);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_11 = new QLabel(layoutWidget_8);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(label_11);

        CURRENT_Kp = new QLineEdit(layoutWidget_8);
        CURRENT_Kp->setObjectName(QString::fromUtf8("CURRENT_Kp"));

        horizontalLayout_9->addWidget(CURRENT_Kp);


        verticalLayout_6->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_12 = new QLabel(layoutWidget_8);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_12);

        CURRENT_Ki = new QLineEdit(layoutWidget_8);
        CURRENT_Ki->setObjectName(QString::fromUtf8("CURRENT_Ki"));

        horizontalLayout_10->addWidget(CURRENT_Ki);


        verticalLayout_6->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_13 = new QLabel(layoutWidget_8);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setAlignment(Qt::AlignCenter);

        horizontalLayout_11->addWidget(label_13);

        CURRENT_Kd = new QLineEdit(layoutWidget_8);
        CURRENT_Kd->setObjectName(QString::fromUtf8("CURRENT_Kd"));

        horizontalLayout_11->addWidget(CURRENT_Kd);


        verticalLayout_6->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_14 = new QLabel(layoutWidget_8);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        horizontalLayout_12->addWidget(label_14);

        CURRENT_MAX = new QLineEdit(layoutWidget_8);
        CURRENT_MAX->setObjectName(QString::fromUtf8("CURRENT_MAX"));

        horizontalLayout_12->addWidget(CURRENT_MAX);


        verticalLayout_6->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_15 = new QLabel(layoutWidget_8);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setAlignment(Qt::AlignCenter);

        horizontalLayout_13->addWidget(label_15);

        CURRENT_MIN = new QLineEdit(layoutWidget_8);
        CURRENT_MIN->setObjectName(QString::fromUtf8("CURRENT_MIN"));

        horizontalLayout_13->addWidget(CURRENT_MIN);


        verticalLayout_6->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_16 = new QLabel(layoutWidget_8);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setAlignment(Qt::AlignCenter);

        horizontalLayout_14->addWidget(label_16);

        CURRENT_REF = new QLineEdit(layoutWidget_8);
        CURRENT_REF->setObjectName(QString::fromUtf8("CURRENT_REF"));

        horizontalLayout_14->addWidget(CURRENT_REF);


        verticalLayout_6->addLayout(horizontalLayout_14);


        gridLayout->addWidget(groupBox_2, 2, 0, 1, 1);

        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(221, 271));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 20, 191, 49));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        SAT_V_M = new QLineEdit(layoutWidget);
        SAT_V_M->setObjectName(QString::fromUtf8("SAT_V_M"));

        verticalLayout->addWidget(SAT_V_M);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_2);

        SAT_V_P = new QLineEdit(layoutWidget);
        SAT_V_P->setObjectName(QString::fromUtf8("SAT_V_P"));

        verticalLayout_2->addWidget(SAT_V_P);


        horizontalLayout->addLayout(verticalLayout_2);

        layoutWidget_2 = new QWidget(groupBox);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(10, 90, 167, 181));
        verticalLayout_3 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_3 = new QLabel(layoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_3);

        Voltage_Kp = new QLineEdit(layoutWidget_2);
        Voltage_Kp->setObjectName(QString::fromUtf8("Voltage_Kp"));

        horizontalLayout_2->addWidget(Voltage_Kp);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_4 = new QLabel(layoutWidget_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_4);

        Voltage_Ki = new QLineEdit(layoutWidget_2);
        Voltage_Ki->setObjectName(QString::fromUtf8("Voltage_Ki"));

        horizontalLayout_3->addWidget(Voltage_Ki);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(layoutWidget_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(label_5);

        Voltage_Kd = new QLineEdit(layoutWidget_2);
        Voltage_Kd->setObjectName(QString::fromUtf8("Voltage_Kd"));

        horizontalLayout_4->addWidget(Voltage_Kd);


        verticalLayout_3->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_6 = new QLabel(layoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_6);

        Voltage_MAX = new QLineEdit(layoutWidget_2);
        Voltage_MAX->setObjectName(QString::fromUtf8("Voltage_MAX"));

        horizontalLayout_5->addWidget(Voltage_MAX);


        verticalLayout_3->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_7 = new QLabel(layoutWidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_7);

        Voltage_MIN = new QLineEdit(layoutWidget_2);
        Voltage_MIN->setObjectName(QString::fromUtf8("Voltage_MIN"));

        horizontalLayout_6->addWidget(Voltage_MIN);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_8 = new QLabel(layoutWidget_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_8);

        Voltage_Ref = new QLineEdit(layoutWidget_2);
        Voltage_Ref->setObjectName(QString::fromUtf8("Voltage_Ref"));

        horizontalLayout_7->addWidget(Voltage_Ref);


        verticalLayout_3->addLayout(horizontalLayout_7);


        gridLayout->addWidget(groupBox, 2, 1, 1, 1);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_15->addWidget(pushButton);

        pushButton_2 = new QPushButton(centralwidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_15->addWidget(pushButton_2);


        gridLayout->addLayout(horizontalLayout_15, 3, 0, 1, 2);

        MainData = new QLabel(centralwidget);
        MainData->setObjectName(QString::fromUtf8("MainData"));
        QFont font;
        font.setPointSize(18);
        font.setItalic(true);
        MainData->setFont(font);
        MainData->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(MainData, 4, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "Input Voltage:", nullptr));
        Load->setText(QApplication::translate("MainWindow", "Load:", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Config Current:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "SAT-:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "SAT+:", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Ki:", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "Kd:", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Max:", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Min:", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Ref:", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Config Voltage:", nullptr));
        label->setText(QApplication::translate("MainWindow", "SAT-:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "SAT+:", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Ki:", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Kd:", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "Max:", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Min:", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Ref:", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Calculate", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "Error", nullptr));
        MainData->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
