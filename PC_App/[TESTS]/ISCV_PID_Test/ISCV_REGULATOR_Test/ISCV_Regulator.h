#ifndef ISCV_REGULATOR_H
#define ISCV_REGULATOR_H

#include <QObject>
#include "stdint.h"

typedef struct
{
    float error;
    float integralFilter;
    float derivativeFilter;
    float proportionalComponent;
    float integralComponent;
    float derivativeComponent;
    float tempPID;
    float outputPID;

}Main_PID;


typedef struct
{
    float feedback;
    float reference;
    float deltaTimeSampling;
    float MaxOutValue;
    float MinimumOutValue;

    struct {
       float proportional;
       float integral;
       float derivative;
       float coefficientBackSaturation;
       float filterDerivative;
            } coefficient;

            struct {
                        float lowThershold;
                        float highThershold;
                    } saturation;


}Config_PWR_PID;


typedef struct
{
    Main_PID	 		_mainPID;
    Config_PWR_PID		_configure_PID;
}PID_Controller;

class ISCV_Regulator : public QObject
{
    Q_OBJECT
public:
    explicit ISCV_Regulator(QObject *parent = nullptr);

    void  SetReference(float ref,PID_Controller *pid);
    void SetFeedBack(float feedback,PID_Controller *pid);

    void SetCoefficient(float Kp, float Ki, float Kd, float BackSaturation, float filterDerivative,
            PID_Controller *pid);

    void SetCoefficient_Kp(float Kp,PID_Controller *pid);
    void SetCoefficient_Ki(float Ki,PID_Controller *pid);
    void SetCoefficient_Kd(float Kd,PID_Controller *pid);

    void  SetSaturation(float lowLimit,float highLimit,PID_Controller *pid);

    void Compute_PID(PID_Controller *pid);

    float GetPID_Value(PID_Controller *pid);

signals:

};

#endif // ISCV_REGULATOR_H
