#include "ISCV_Regulator.h"
#include "math.h"

ISCV_Regulator::ISCV_Regulator(QObject *parent) : QObject(parent)
{

}

void ISCV_Regulator::SetReference(float ref, PID_Controller *pid)
{
    pid->_configure_PID.reference=ref;
}

void ISCV_Regulator::SetFeedBack(float feedback, PID_Controller *pid)
{
    pid->_configure_PID.feedback=feedback;
}

void ISCV_Regulator::SetCoefficient(float Kp, float Ki, float Kd, float BackSaturation, float filterDerivative, PID_Controller *pid)
{
    pid->_configure_PID.coefficient.proportional=Kp;
        pid->_configure_PID.coefficient.integral=Ki;
        pid->_configure_PID.coefficient.derivative=Kd;

        pid->_configure_PID.coefficient.filterDerivative=filterDerivative;
        pid->_configure_PID.coefficient.coefficientBackSaturation=BackSaturation;
}

void ISCV_Regulator::SetCoefficient_Kp(float Kp, PID_Controller *pid)
{
    pid->_configure_PID.coefficient.proportional=Kp;
}

void ISCV_Regulator::SetCoefficient_Ki(float Ki, PID_Controller *pid)
{
    pid->_configure_PID.coefficient.integral=Ki;
}

void ISCV_Regulator::SetCoefficient_Kd(float Kd, PID_Controller *pid)
{
    pid->_configure_PID.coefficient.derivative=Kd;
}

void ISCV_Regulator::SetSaturation(float lowLimit, float highLimit, PID_Controller *pid)
{
    pid->_configure_PID.saturation.highThershold=highLimit;
        pid->_configure_PID.saturation.lowThershold=lowLimit;
}

void ISCV_Regulator::Compute_PID(PID_Controller *pid)
{
    pid->_mainPID.error=pid->_configure_PID.reference-pid->_configure_PID.feedback;

        pid->_mainPID.proportionalComponent=pid->_configure_PID.coefficient.proportional*pid->_mainPID.error;

        pid->_mainPID.integralComponent+=pid->_configure_PID.deltaTimeSampling*pid->_mainPID.integralFilter;
        pid->_mainPID.integralFilter=pid->_configure_PID.coefficient.integral*pid->_mainPID.error+
                pid->_configure_PID.coefficient.coefficientBackSaturation
                *(pid->_mainPID.outputPID-pid->_mainPID.tempPID);

        pid->_mainPID.derivativeFilter+=pid->_configure_PID.deltaTimeSampling*pid->_mainPID.derivativeComponent;
        pid->_mainPID.derivativeComponent=(pid->_configure_PID.coefficient.derivative*pid->_mainPID.error-
                pid->_mainPID.derivativeFilter)*pid->_configure_PID.coefficient.filterDerivative;

        pid->_mainPID.tempPID=pid->_mainPID.proportionalComponent+pid->_mainPID.integralComponent+
                pid->_mainPID.derivativeComponent;
        pid->_mainPID.outputPID=pid->_mainPID.tempPID;

        pid->_mainPID.outputPID=fmin(fmax(pid->_mainPID.outputPID,pid->_configure_PID.saturation.lowThershold),
                pid->_configure_PID.saturation.highThershold);
}

float ISCV_Regulator::GetPID_Value(PID_Controller *pid)
{
    return pid->_mainPID.outputPID;
}
