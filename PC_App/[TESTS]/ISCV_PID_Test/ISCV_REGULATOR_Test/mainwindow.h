#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QTimer"
#include "stdint.h"
#include "ISCV_Regulator.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void Time_Calculate_Hundler(void);

private:
    Ui::MainWindow *ui;
    QTimer *timer;

    float Ktr=0.47f;//коэффициент трансформации
    float n=0.95;//спрогнозированный КПД
    float test_duty=0.18;
    float Vin=0;
    float Vout=0;
    float Iout=0;
    float RLoad=0;



    void Calculate_Vout(void);
    void Config_PID(void);
    void Calculate_PID(void);

    PID_Controller _data[2];
    ISCV_Regulator *dataPID;
};
#endif // MAINWINDOW_H
