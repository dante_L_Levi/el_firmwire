#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
bool statusStart=false;
bool status_rand_Error=false;
#define INDEX_VOLTAGE 0
#define INDEX_CURRENT 1

typedef enum
{
    CV_Work,
    CC_Work,
    NONE_Work

}Stautus_Regulator;

Stautus_Regulator _state;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer=new QTimer();
    timer->setInterval(100);
    connect(timer,SIGNAL(timeout()),this,SLOT(Time_Calculate_Hundler()));

    dataPID=new ISCV_Regulator();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: Time_Calculate_Hundler(void)
{
    qDebug()<<"Timer Work!!";



    Calculate_PID();
    Calculate_Vout();
    Iout=Vout/RLoad;
    QString dataBoard=QString(
                           "Vin:%1V\n"
                           "Vout:%2V\n"
                           "Iout:%3A\n"
                           "Duty:%4  \n"

                        ).arg(Vin)
                        .arg(Vout)
                        .arg(Iout)
                        .arg(test_duty);


                    ui->MainData->setText(dataBoard) ;
}

void MainWindow::on_pushButton_clicked()
{
    statusStart=!statusStart;
    if(statusStart)
    {
        Vin=ui->VoltageInput->text().toFloat();
        RLoad=ui->RLoad->text().toFloat();
        Config_PID();
        timer->start();
        ui->pushButton->setText("Go Calculated...");
    }
    else
    {
       timer->stop();
       ui->pushButton->setText("Stop Calculated....");
    }
}


void MainWindow:: Calculate_Vout(void)
{
    Vout=n*2*test_duty*(1/Ktr)*Vin;
}

void MainWindow::Config_PID()
{
    float Kp_V=ui->Voltage_Kp->text().toFloat();
    float Ki_V=ui->Voltage_Ki->text().toFloat();
    float Kd_V=ui->Voltage_Kd->text().toFloat();


    dataPID->SetCoefficient_Kp(Kp_V,&_data[0]);
    dataPID->SetCoefficient_Ki(Ki_V,&_data[0]);
    dataPID->SetCoefficient_Kd(Kd_V,&_data[0]);

    float Saturation_M_V=ui->SAT_V_M->text().toFloat();
    float Saturation_P_V=ui->SAT_V_P->text().toFloat();

    dataPID->SetSaturation(Saturation_M_V,Saturation_P_V,&_data[0]);

    float Kp_C=ui->Voltage_Kp->text().toFloat();
    float Ki_C=ui->Voltage_Ki->text().toFloat();
    float Kd_C=ui->Voltage_Kd->text().toFloat();

    dataPID->SetCoefficient_Kp(Kp_C,&_data[1]);
    dataPID->SetCoefficient_Ki(Ki_C,&_data[1]);
    dataPID->SetCoefficient_Kd(Kd_C,&_data[1]);

    float Saturation_M_C=ui->SAT_C_M->text().toFloat();
    float Saturation_P_C=ui->SAT_C_P->text().toFloat();

    dataPID->SetSaturation(Saturation_M_C,Saturation_P_C,&_data[1]);

    float Ref[2];
    Ref[0]=ui->Voltage_Ref->text().toFloat();
    Ref[1]=ui->CURRENT_REF->text().toFloat();
    qDebug()<<"Reference V:"<<Ref[0];
    qDebug()<<"Reference I:"<<Ref[1];
    dataPID->SetReference(Ref[0],&_data[0]);
    dataPID->SetReference(Ref[1],&_data[1]);


    test_duty=0.18;


}

void MainWindow::Calculate_PID()
{

    if(Vout<_data[0]._configure_PID.reference)
    {
        _state=CC_Work;
        dataPID->SetFeedBack(Iout,&_data[INDEX_CURRENT]);
    }
    if(Iout<_data[0]._configure_PID.reference || Vout<=_data[0]._configure_PID.reference-0.5)
    {
        _state=CV_Work;
        dataPID->SetFeedBack(Vout,&_data[INDEX_VOLTAGE]);
    }

    switch(_state)
    {
        case CC_Work:
        {
            dataPID->Compute_PID(&_data[INDEX_CURRENT]);
            test_duty+=dataPID->GetPID_Value(&_data[INDEX_CURRENT]);
            break;
        }

        case CV_Work:
        {
            dataPID->Compute_PID(&_data[INDEX_VOLTAGE]);
            test_duty+=dataPID->GetPID_Value(&_data[INDEX_VOLTAGE]);;
            break;
        }

        default:
            break;
    }


}

void MainWindow::on_pushButton_2_clicked()
{
    status_rand_Error=!status_rand_Error;
    if(status_rand_Error)
        Vin+=rand()%10;
    else
        Vin-=rand()%10;
    qDebug()<<Vin;
}



