#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include "SerialPort/serialprotocol_data.h"
#include "Test_DAC.h"
#include "stdint.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_on_ConnectButton_clicked();
    void Get_Read_Data_Serial(QByteArray data);


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
