#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "SerialPort/serialprotocol_data.h"
#include "QDebug"
#include "QMessageBox"



SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
Test_DAC *mainData;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
     _serial=new   SerialProtocol_data();

     mainData=new Test_DAC();


     ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
     const auto infos = QSerialPortInfo::availablePorts();
     for (const QSerialPortInfo &info : infos)
     {
        ui->NameSerialPort_Box->addItem(info.portName());
     }






}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
             count_ConnectButton=1;
    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);
        mainData->Start_Init_Model();
       bool statusConfig= mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                               (ui->SpeedSerialPort_Box->currentText()).toLong());

        connect(mainData->serial_Data, SIGNAL(Get_data_Recieved_Serial(QByteArray)),
                this, SLOT(Get_Read_Data_Serial(QByteArray)));
        if(statusConfig)
            QMessageBox::information(this,"Connection!!","Connection OK!");
        else
             QMessageBox::information(this," Not Connection!!","Connection error!");


    }
}

void MainWindow::Get_Read_Data_Serial(QByteArray data)
{
    qDebug()<<data;

}

