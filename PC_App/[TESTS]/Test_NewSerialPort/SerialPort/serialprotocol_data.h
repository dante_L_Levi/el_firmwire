#ifndef SERIALPROTOCOL_DATA_H
#define SERIALPROTOCOL_DATA_H


#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "QString"
#include "QtSerialPort/QSerialPortInfo"
#include "QtSerialPort/QSerialPort"
#include "QDebug"

typedef enum
{
    ECHO_CMD=0x56,
    BOOT_CMD,
    REQUEST_PC_CMD,
    RESPONSE_PC_CMD,
    REQUEST_MCU_CMD,
    RESPONSE_MCU_CMD,
    RESPONSE_MCU_CONTINUE_CMD,

}CommandDef;

#define PayloadSize 8

#pragma pack(push, 1)
typedef  struct
{
    uint16_t ID;
    uint8_t cmd;
    uint8_t payload[PayloadSize];
    uint16_t CRC;

}SerialProtocolDef;
#pragma pack(pop)

class SerialProtocol_data:public QSerialPort, public QSerialPortInfo
{
     Q_OBJECT

public:
   // SerialProtocol_data();
    /************************Function Transmit Packet********************/
   void Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length);
   /************************Function Set ID Device********************/
    void Set_ID(uint16_t id);
    /************************Function Get Speed********************/
    QStringList Get_BaudRate();
    /********************Function Init SerialPort******************/
    bool  Init_OpenPort(QString _Name, long int _baud);
    /*************************Function result read*********************/
    QByteArray Get_Result_Recieve_Data(void);
    /*************************Function result read*********************/
    QByteArray Get_CHECK_Result_Recieve_Data(void);

    bool IsRx_DataReady(){return IsRxAvalible;}
    /***********Set State Rx hundler**********/
    void Set_IsRxAvalible(bool);
private:
     bool IsRxAvalible;

signals:
    void Get_data_Recieved_Serial(QByteArray data);

private slots:
    /**************Read data Serial Port *********************/
    void ReadSerialPort_data();

   // SerialProtocol_data();
};

#endif // SERIALPROTOCOL_DATA_H
