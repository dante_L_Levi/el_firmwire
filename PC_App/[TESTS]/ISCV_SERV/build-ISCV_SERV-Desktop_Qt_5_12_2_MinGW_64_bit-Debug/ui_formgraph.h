/********************************************************************************
** Form generated from reading UI file 'formgraph.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMGRAPH_H
#define UI_FORMGRAPH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_formgraph
{
public:
    QVBoxLayout *verticalLayout;
    QCustomPlot *CUSTOMEPLOT;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *comboBoxSize_3;
    QPushButton *pushButtonReset_3;
    QPushButton *pushButtonPause_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButtonColor_3;
    QComboBox *comboBoxGraph_3;
    QPushButton *pushButtonAdd_3;
    QPushButton *pushButtonAxis_3;

    void setupUi(QWidget *formgraph)
    {
        if (formgraph->objectName().isEmpty())
            formgraph->setObjectName(QString::fromUtf8("formgraph"));
        formgraph->resize(717, 428);
        verticalLayout = new QVBoxLayout(formgraph);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CUSTOMEPLOT = new QCustomPlot(formgraph);
        CUSTOMEPLOT->setObjectName(QString::fromUtf8("CUSTOMEPLOT"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CUSTOMEPLOT->sizePolicy().hasHeightForWidth());
        CUSTOMEPLOT->setSizePolicy(sizePolicy);
        CUSTOMEPLOT->setMinimumSize(QSize(0, 0));

        verticalLayout->addWidget(CUSTOMEPLOT);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        comboBoxSize_3 = new QComboBox(formgraph);
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->addItem(QString());
        comboBoxSize_3->setObjectName(QString::fromUtf8("comboBoxSize_3"));

        horizontalLayout_3->addWidget(comboBoxSize_3);

        pushButtonReset_3 = new QPushButton(formgraph);
        pushButtonReset_3->setObjectName(QString::fromUtf8("pushButtonReset_3"));

        horizontalLayout_3->addWidget(pushButtonReset_3);

        pushButtonPause_3 = new QPushButton(formgraph);
        pushButtonPause_3->setObjectName(QString::fromUtf8("pushButtonPause_3"));

        horizontalLayout_3->addWidget(pushButtonPause_3);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        pushButtonColor_3 = new QPushButton(formgraph);
        pushButtonColor_3->setObjectName(QString::fromUtf8("pushButtonColor_3"));

        horizontalLayout_3->addWidget(pushButtonColor_3);

        comboBoxGraph_3 = new QComboBox(formgraph);
        comboBoxGraph_3->setObjectName(QString::fromUtf8("comboBoxGraph_3"));
        comboBoxGraph_3->setEditable(false);

        horizontalLayout_3->addWidget(comboBoxGraph_3);

        pushButtonAdd_3 = new QPushButton(formgraph);
        pushButtonAdd_3->setObjectName(QString::fromUtf8("pushButtonAdd_3"));

        horizontalLayout_3->addWidget(pushButtonAdd_3);

        pushButtonAxis_3 = new QPushButton(formgraph);
        pushButtonAxis_3->setObjectName(QString::fromUtf8("pushButtonAxis_3"));

        horizontalLayout_3->addWidget(pushButtonAxis_3);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(formgraph);

        comboBoxSize_3->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(formgraph);
    } // setupUi

    void retranslateUi(QWidget *formgraph)
    {
        formgraph->setWindowTitle(QApplication::translate("formgraph", "Form", nullptr));
        comboBoxSize_3->setItemText(0, QApplication::translate("formgraph", "1", nullptr));
        comboBoxSize_3->setItemText(1, QApplication::translate("formgraph", "2", nullptr));
        comboBoxSize_3->setItemText(2, QApplication::translate("formgraph", "5", nullptr));
        comboBoxSize_3->setItemText(3, QApplication::translate("formgraph", "10", nullptr));
        comboBoxSize_3->setItemText(4, QApplication::translate("formgraph", "20", nullptr));
        comboBoxSize_3->setItemText(5, QApplication::translate("formgraph", "50", nullptr));
        comboBoxSize_3->setItemText(6, QApplication::translate("formgraph", "100", nullptr));
        comboBoxSize_3->setItemText(7, QApplication::translate("formgraph", "200", nullptr));
        comboBoxSize_3->setItemText(8, QApplication::translate("formgraph", "500", nullptr));
        comboBoxSize_3->setItemText(9, QApplication::translate("formgraph", "1000", nullptr));
        comboBoxSize_3->setItemText(10, QApplication::translate("formgraph", "2000", nullptr));
        comboBoxSize_3->setItemText(11, QApplication::translate("formgraph", "5000", nullptr));
        comboBoxSize_3->setItemText(12, QApplication::translate("formgraph", "10000", nullptr));

        pushButtonReset_3->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201", nullptr));
        pushButtonPause_3->setText(QApplication::translate("formgraph", "\320\237\320\260\321\203\320\267\320\260", nullptr));
        pushButtonColor_3->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201\320\270\321\202\321\214 \321\206\320\262\320\265\321\202\320\260", nullptr));
        pushButtonAdd_3->setText(QApplication::translate("formgraph", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        pushButtonAxis_3->setText(QApplication::translate("formgraph", "--", nullptr));
    } // retranslateUi

};

namespace Ui {
    class formgraph: public Ui_formgraph {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMGRAPH_H
