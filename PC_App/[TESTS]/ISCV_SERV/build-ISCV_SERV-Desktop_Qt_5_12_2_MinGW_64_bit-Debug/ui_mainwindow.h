/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_6;
    QGroupBox *groupBox_Settings;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_8;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *Name_update_btn_2;
    QLabel *LabelName;
    QComboBox *NameSerialPort_Box_2;
    QHBoxLayout *horizontalLayout_10;
    QLabel *LabelSpeed;
    QComboBox *SpeedSerialPort_Box_2;
    QPushButton *on_ConnectButton_2;
    QLabel *Label_Board_Info_;
    QTabWidget *tabWidget;
    QWidget *MAINFILL;
    QHBoxLayout *horizontalLayout_15;
    QVBoxLayout *verticalLayout_5;
    QLabel *MainDataFill;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton_4;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_11;
    QSpinBox *ValuePWM;
    QPushButton *BTN_SET_PWM;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_12;
    QSpinBox *Value_DAC;
    QPushButton *DAC_Set;
    QGroupBox *groupBox_5;
    QHBoxLayout *horizontalLayout_13;
    QListWidget *Gpio_List;
    QWidget *Settings;
    QVBoxLayout *verticalLayout_11;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_30;
    QHBoxLayout *horizontalLayout_31;
    QVBoxLayout *verticalLayout_13;
    QVBoxLayout *verticalLayout_12;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(621, 504);
        MainWindow->setMinimumSize(QSize(620, 504));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_6 = new QVBoxLayout(centralwidget);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        groupBox_Settings = new QGroupBox(centralwidget);
        groupBox_Settings->setObjectName(QString::fromUtf8("groupBox_Settings"));
        groupBox_Settings->setMaximumSize(QSize(16777215, 60));
        groupBox_Settings->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_Settings);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        Name_update_btn_2 = new QPushButton(groupBox_Settings);
        Name_update_btn_2->setObjectName(QString::fromUtf8("Name_update_btn_2"));

        horizontalLayout_9->addWidget(Name_update_btn_2);

        LabelName = new QLabel(groupBox_Settings);
        LabelName->setObjectName(QString::fromUtf8("LabelName"));

        horizontalLayout_9->addWidget(LabelName);

        NameSerialPort_Box_2 = new QComboBox(groupBox_Settings);
        NameSerialPort_Box_2->setObjectName(QString::fromUtf8("NameSerialPort_Box_2"));

        horizontalLayout_9->addWidget(NameSerialPort_Box_2);


        horizontalLayout_8->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        LabelSpeed = new QLabel(groupBox_Settings);
        LabelSpeed->setObjectName(QString::fromUtf8("LabelSpeed"));

        horizontalLayout_10->addWidget(LabelSpeed);

        SpeedSerialPort_Box_2 = new QComboBox(groupBox_Settings);
        SpeedSerialPort_Box_2->setObjectName(QString::fromUtf8("SpeedSerialPort_Box_2"));

        horizontalLayout_10->addWidget(SpeedSerialPort_Box_2);


        horizontalLayout_8->addLayout(horizontalLayout_10);

        on_ConnectButton_2 = new QPushButton(groupBox_Settings);
        on_ConnectButton_2->setObjectName(QString::fromUtf8("on_ConnectButton_2"));

        horizontalLayout_8->addWidget(on_ConnectButton_2);


        horizontalLayout_7->addLayout(horizontalLayout_8);


        horizontalLayout_6->addWidget(groupBox_Settings);

        Label_Board_Info_ = new QLabel(centralwidget);
        Label_Board_Info_->setObjectName(QString::fromUtf8("Label_Board_Info_"));

        horizontalLayout_6->addWidget(Label_Board_Info_);


        verticalLayout_6->addLayout(horizontalLayout_6);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        MAINFILL = new QWidget();
        MAINFILL->setObjectName(QString::fromUtf8("MAINFILL"));
        horizontalLayout_15 = new QHBoxLayout(MAINFILL);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        MainDataFill = new QLabel(MAINFILL);
        MainDataFill->setObjectName(QString::fromUtf8("MainDataFill"));

        verticalLayout_5->addWidget(MainDataFill);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        pushButton = new QPushButton(MAINFILL);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_14->addWidget(pushButton);

        pushButton_3 = new QPushButton(MAINFILL);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_14->addWidget(pushButton_3);


        verticalLayout_4->addLayout(horizontalLayout_14);

        pushButton_2 = new QPushButton(MAINFILL);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_4->addWidget(pushButton_2);

        pushButton_4 = new QPushButton(MAINFILL);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        verticalLayout_4->addWidget(pushButton_4);


        verticalLayout_5->addLayout(verticalLayout_4);


        horizontalLayout_15->addLayout(verticalLayout_5);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(MAINFILL);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_11 = new QHBoxLayout(groupBox);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        ValuePWM = new QSpinBox(groupBox);
        ValuePWM->setObjectName(QString::fromUtf8("ValuePWM"));
        ValuePWM->setMaximum(22500);

        horizontalLayout_11->addWidget(ValuePWM);

        BTN_SET_PWM = new QPushButton(groupBox);
        BTN_SET_PWM->setObjectName(QString::fromUtf8("BTN_SET_PWM"));

        horizontalLayout_11->addWidget(BTN_SET_PWM);


        verticalLayout_3->addWidget(groupBox);

        groupBox_4 = new QGroupBox(MAINFILL);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        horizontalLayout_12 = new QHBoxLayout(groupBox_4);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        Value_DAC = new QSpinBox(groupBox_4);
        Value_DAC->setObjectName(QString::fromUtf8("Value_DAC"));
        Value_DAC->setMaximum(4095);

        horizontalLayout_12->addWidget(Value_DAC);

        DAC_Set = new QPushButton(groupBox_4);
        DAC_Set->setObjectName(QString::fromUtf8("DAC_Set"));

        horizontalLayout_12->addWidget(DAC_Set);


        verticalLayout_3->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(MAINFILL);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        horizontalLayout_13 = new QHBoxLayout(groupBox_5);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        Gpio_List = new QListWidget(groupBox_5);
        Gpio_List->setObjectName(QString::fromUtf8("Gpio_List"));

        horizontalLayout_13->addWidget(Gpio_List);


        verticalLayout_3->addWidget(groupBox_5);


        horizontalLayout_15->addLayout(verticalLayout_3);

        tabWidget->addTab(MAINFILL, QString());
        Settings = new QWidget();
        Settings->setObjectName(QString::fromUtf8("Settings"));
        verticalLayout_11 = new QVBoxLayout(Settings);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        tabWidget->addTab(Settings, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_30 = new QHBoxLayout(tab);
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        pushButton_5 = new QPushButton(tab);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        verticalLayout_12->addWidget(pushButton_5);

        pushButton_6 = new QPushButton(tab);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        verticalLayout_12->addWidget(pushButton_6);

        pushButton_7 = new QPushButton(tab);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        verticalLayout_12->addWidget(pushButton_7);


        verticalLayout_13->addLayout(verticalLayout_12);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_13->addItem(verticalSpacer);


        horizontalLayout_31->addLayout(verticalLayout_13);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_31->addItem(horizontalSpacer_3);


        horizontalLayout_30->addLayout(horizontalLayout_31);

        tabWidget->addTab(tab, QString());

        verticalLayout_6->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_Settings->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn_2->setText(QApplication::translate("MainWindow", "update", nullptr));
        LabelName->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        LabelSpeed->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton_2->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        Label_Board_Info_->setText(QApplication::translate("MainWindow", "ID:\n"
"Version:\n"
"NameBoard:", nullptr));
        MainDataFill->setText(QApplication::translate("MainWindow", "Vin:		10.0V\n"
"Vout:		10.0V\n"
"Iin:		10.0A\n"
"Iout:		1.0A\n"
"V_D:		12V\n"
"Temperature:	27 C\n"
"Pwm A:		12000\n"
"PWM A:		0.45\n"
"Pwm B:		12000\n"
"PWM B:		0.45\n"
"DAC :		1024\n"
"DAC:		2.5V\n"
"Status: 		Status_NORMAL", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "EN", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "CTRL_M", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "CTRL_R", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "Plot Graph", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "PWM:", nullptr));
        BTN_SET_PWM->setText(QApplication::translate("MainWindow", "SET", nullptr));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "DAC:", nullptr));
        DAC_Set->setText(QApplication::translate("MainWindow", "SET", nullptr));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "GPIO:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(MAINFILL), QApplication::translate("MainWindow", "MAINFILL", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Settings), QApplication::translate("MainWindow", "Settings", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "Test DATA", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "Test Board", nullptr));
        pushButton_7->setText(QApplication::translate("MainWindow", "Test Settings", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Test", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
