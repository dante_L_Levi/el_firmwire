#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "style_classes/window_styles.h"
#include "stdint.h"
#include "QTimer"
#include "style_classes/window_styles.h"
#include "SerialPort/QSerialProtocol.h"
#include "SerialPort/SerialPort.h"
#include "qcustomplot.h"
#include "Logic/ISCV_Logic.h"
#include "formgraph.h"


#define ID_DEVICE       0x1001


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void serialReceived(QByteArray data);
    void receivedMsg(protocol_Frame_data* data);
    void Request_Board_Tim();
    void Immulator_Get_recieve_Data();
    void slotTimerAlarm(void);



    void on_on_ConnectButton_2_clicked();

    void on_Name_update_btn_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_4_clicked();

    void on_BTN_SET_PWM_clicked();

    void on_DAC_Set_clicked();

private:
    Ui::MainWindow *ui;

    Window_Styles *stylesw;
    QTimer *timer;
    QTimer *TimerAlarm;
    QTime *timeWork;
    ISCV_Logic *Logic;

    SerialPort *serial;
    QSerialProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;
    formgraph *graphOutput;

    void Update_ComBoBox_Config(void);
    void Load_dataGraph(void);



};
#endif // MAINWINDOW_H
