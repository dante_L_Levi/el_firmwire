#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"

#define PERIOD_Request_MS          50
bool count_ConnectButton=false;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("ISCV SERVICE");
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
        QPalette palette=ui->LabelName->palette();
        QPalette palette2=ui->LabelSpeed->palette();
        QPalette palette3=ui->Label_Board_Info_->palette();
        palette.setColor(ui->LabelSpeed->foregroundRole(), Qt::white);
        ui->LabelName->setPalette(palette);

         palette2.setColor(ui->LabelName->foregroundRole(), Qt::white);
         ui->LabelSpeed->setPalette(palette2);
         palette3.setColor(ui->Label_Board_Info_->foregroundRole(), Qt::white);
         ui->Label_Board_Info_->setPalette(palette3);
         ui->groupBox_Settings->setForegroundRole(QPalette::Light);

         timer=new QTimer();

         TimerAlarm=new QTimer();

         serial=new SerialPort();
         protocol=new QSerialProtocol();
         protocol->setID(ID_DEVICE);
         Logic=new ISCV_Logic();

         for(int i=0;i<6;i++)
         {
             QListWidgetItem *itemGpio=new QListWidgetItem;
             itemGpio->setText(Logic->NameGPIO[i]);
             itemGpio->setCheckState(Qt::Unchecked);
             ui->Gpio_List->addItem(itemGpio);

         }

         Update_ComBoBox_Config();

         connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
                 this,SLOT(serialReceived(QByteArray)));
         connect(protocol,SIGNAL(receivedMsgFrame(protocol_Frame_data*)),
                 this,SLOT(receivedMsg(protocol_Frame_data*)));
         connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
                 serial,SLOT(transmitData(QByteArray*)));


        connect(TimerAlarm,SIGNAL(timeout()),this,SLOT(slotTimerAlarm()));

        graphOutput=new formgraph();
        graphOutput->setIncrement(0.025);

        TimerAlarm->setInterval(25);



        Load_dataGraph();



}



void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box_2->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box_2->addItem(info.portName());
         }
}
void MainWindow:: Load_dataGraph(void)
{
    timeWork=new QTime();
    TypeGraphData gData;
    gData.plot=false;
    gData.axisType=QCPAxis::atLeft;
    gData.valueType=float_type;

    for(int i=0;i<Logic->NameVoltage->size();i++)
    {
        gData.name=Logic->NameVoltage[i];
        gData.value=Logic->Get_Voltage();
        graphOutput->addGraph(&gData);
    }


    for(int i=0;i<Logic->NameCurrent->size();i++)
    {
        gData.name=Logic->NameCurrent[i];
        gData.value=Logic->Get_Current();
        graphOutput->addGraph(&gData);
    }
}

void MainWindow:: serialReceived(QByteArray data)
{
    protocol->serialProtReceived(data);

}
void MainWindow:: receivedMsg(protocol_Frame_data* data)
{
    Logic->Parse_Data_RX((Command_Board)data->command,data);
    switch(data->command)
    {
        case Get_Board:
        {
           Logic->Update_UI((Command_Board)data->command,ui->Label_Board_Info_);

            break;
        }

        case Get_main_data:
        {
            Logic->Update_UI((Command_Board)data->command,ui->MainDataFill);

            break;
        }

        case Get_setting_peripheral_board:
        {


            /*Logic->Update_UI(ui->lineEdit_K_1,
                             ui->lineEdit_K_2,
                             ui->lineEdit_K_3,
                             ui->lineEdit_K_4,
                             ui->lineEdit_K_5,
                             ui->lineEdit_K_6,
                             ui->lineEdit_K_7,
                             ui->lineEdit_K_8,
                             ui->lineEdit_K_9,
                             ui->lineEdit_K_10,
                             ui->lineEdit_K_11,
                             ui->lineEdit_K_12,
                             ui->lineEdit_K_13,
                             ui->lineEdit_K_14,
                             ui->lineEdit_K_15);
            */
break;
        }
    }
}
void MainWindow:: Request_Board_Tim()
{

    if(status_start_request)
    {
        //function Request RS_Connected
        Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }
    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            Logic->Request_Get_MainData(protocol);
            break;
        }

    }
    count_request++;
    if(count_request>2)
        count_request=1;

}
void MainWindow:: Immulator_Get_recieve_Data()
{

}
void MainWindow:: slotTimerAlarm(void)
{
     graphOutput->updateGraph(timeWork->elapsed()/1000.0);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_on_ConnectButton_2_clicked()
{

    count_ConnectButton=!count_ConnectButton;
    if(count_ConnectButton)
    {
        ui->on_ConnectButton_2->setText("Connect");
        ui->NameSerialPort_Box_2->setEnabled(false);
        ui->SpeedSerialPort_Box_2->setEnabled(false);
        Logic->Set_ID(ID_DEVICE);

        bool status_Config=serial->ConnectPort(ui->NameSerialPort_Box_2->currentText(),
                                               (ui->SpeedSerialPort_Box_2->currentText()).toLong());

        if(status_Config)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");
            timer->setInterval(PERIOD_Request_MS);
            connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));
           timer->start();
           timeWork->start();
            status_start_request=true;
            count_request=1;
        }
        else
            QMessageBox::information(this," Not Connection!!","Connection error!");

    }
    else
    {
        if(serial->isOpen())
                serial->DisconnectedPort();
        ui->on_ConnectButton_2->setText("Disconnected");
        ui->NameSerialPort_Box_2->setEnabled(true);
        ui->SpeedSerialPort_Box_2->setEnabled(true);
        timer->stop();
        status_start_request=false;

        QMessageBox::information(this,"Connector Status","Close Port!!");

    }
}


void MainWindow::on_Name_update_btn_2_clicked()
{
    ui->SpeedSerialPort_Box_2->clear();
    ui->NameSerialPort_Box_2->clear();
   Update_ComBoBox_Config();
}


void MainWindow::on_pushButton_5_clicked()
{
    Logic->Request_Get_MainData(protocol);
}


void MainWindow::on_pushButton_6_clicked()
{

     Logic->Request_Get_Config_Board(protocol);
}


void MainWindow::on_pushButton_7_clicked()
{
    Logic->Request_read_Settings_Peripheral(protocol);
}


void MainWindow::on_pushButton_4_clicked()
{
    TimerAlarm->start();
    graphOutput->show();
    graphOutput->resetGraph();
}







void MainWindow::on_BTN_SET_PWM_clicked()
{
    timer->stop();
    uint16_t data[2]={(uint16_t)ui->ValuePWM->value(),(uint16_t)ui->ValuePWM->value()};
    Logic->Request_Set_PWM(protocol,data);
    timer->start();
}


void MainWindow::on_DAC_Set_clicked()
{
    timer->stop();
     Logic->Request_Set_DAC(protocol,(uint16_t)ui->Value_DAC->value());
     timer->start();

}

