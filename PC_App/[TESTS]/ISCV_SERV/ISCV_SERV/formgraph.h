#ifndef FORMGRAPH_H
#define FORMGRAPH_H

#include <QWidget>
#include <QTimer>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "qcustomplot.h"

typedef enum{
    int8_type,
    int16_type,
    int32_type,
    uint8_type,
    uint16_type,
    uint32_type,
    float_type,
    double_type
}TypeOfType;

typedef struct{
    bool plot;
    void* value;
    TypeOfType valueType;
    QCPAxis::AxisType axisType;
    QString name;
    QCPGraph *graph;
    QColor color;
} TypeGraphData;

namespace Ui {
class formgraph;
}

class formgraph : public QWidget
{
    Q_OBJECT

public:
    explicit formgraph(QWidget *parent = nullptr);
    ~formgraph();
    void addGraphData(int nuberGraph, double value);
    int addGraph(QString name, QCPAxis::AxisType type);
    int addGraph(TypeGraphData* graphData);
    void deleteGraph(int numberGraph);
    void showGraph(int index);
    void hideGraph(int index);
    void endGraphData();
    void resetGraph();
    void updateGraph();
    void updateGraph(double time);
    void setIncrement(double inc);
    void colorGenerator();
    void resizeEvent(QResizeEvent *event);

private slots:
    void on_pushButtonReset_3_clicked();

    void on_pushButtonPause_3_clicked();

    void on_pushButtonColor_3_clicked();

    void on_pushButtonAdd_3_clicked();

    void on_pushButtonAxis_3_clicked();

    void on_comboBoxSize_3_activated(const QString &arg1);

    void on_comboBoxGraph_3_activated(int index);

private:
    Ui::formgraph *uiC;
    QTimer dataTimer;
    bool leftUpdate;
    bool rightUpdate;
    TypeGraphData numbersGraph[100];
    int count;
    double dataTime;
    float data1;
    float data2;
    float data3;
    double sizeGraph;
    double increment;
    bool pause;
    void updateComboBox();

};

#endif // FORMGRAPH_H
