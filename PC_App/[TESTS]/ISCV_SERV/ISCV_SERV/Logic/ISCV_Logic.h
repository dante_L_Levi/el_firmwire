#ifndef ISCV_LOGIC_H
#define ISCV_LOGIC_H

#include <QObject>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QSerialPort"
#include "SerialPort/QSerialProtocol.h"


#define INDEX_PID_CURRENT           0
#define INDEX_PID_Voltage           1

#define INDEX_RECIEVE               0
#define INDEX_TRANSMIT              1


#define INDEX_DUTY_A                0
#define INDEX_DUTY_B                1


typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,//ID,FIRMWIRE,NAME
    Get_main_data,//Vin,Vout,Iout,Iin,PWM,DAC,Mode
    Get_setting_peripheral_board,//Kp,Ki,Kd,Saturation+,Saturation-,Max,Min

    Set_Mode_Work,
    Set_DAC,
    Set_PWM,
    Set_Gpio,
    Set_setting_peripheral_board,
    Set_Default_peripheral_board,
    Set_cmd_flash

}Command_Board;

typedef enum
{
    STATUS_INIT=0,
    STATUS_NORMAL=1,
    STATUS_NORMAL_GEN_ON,
    STATUS_NORMAL_GEN_OFF,
    STATUS_NORMAL_Charge_GEN_OFF,//перекачка энергии из GB1 в GB2
    STATUS_TEST,

    STATUS_ERROR_Input_Voltage,//необходимо обслуживание GB1
    STATUS_ERROR_OUTPUT_Voltage,//необходимо обслуживание GB2
    STATUS_ERROR_Breakage_OUT_NET,//обрыв цепи заряда
    STATUS_ERROR_Short_OUT_NET,		//кз цепи заряда
    STATUS_ERROR_Short_GB1,//КЗ банок GB1
    STATUS_ERROR_Short_GB2,//КЗ банок GB2
    STATUS_ERROR_Low_Voltage_D,//низкое напряжение на клеме D
    STATUS_ERROR_Low_Input_Voltage,
    STATUS_ERROR_High_Input_Voltage,//высокое входное напряжение

        STATUS_ERROR

}State_push_pull_Converter;



#pragma pack(push, 1)
typedef struct
{
    uint16_t ID;
    uint8_t dataName[8];
    uint8_t datafirmwire[8];

}Recieve_data1_Model_DataBoard;
#pragma pack(pop)

#pragma pack(push, 1)

typedef union
{
    struct
    {
        int16_t 	input_current;
        int16_t 	output_current;
        int16_t 	output_28V;
        int16_t 	output_14V;

    }fields;
    int16_t all_channel[4];

}analog1_s;


typedef union
{
    struct
    {
        int16_t 	temperature_sense;
        int16_t 	extern_in_D;

    }fields;
    uint16_t all_channel[2];

}analog2_s;

typedef union
{
    struct
    {
        uint8_t CTRL_M:1;
        uint8_t CTRL_LED:1;
        uint8_t CTRL_R:1;
        uint8_t ENA:1;
        uint8_t ENB:1;
    }fields;
    uint8_t all;
}gpio_out_s;

typedef struct
{
    uint16_t outDAC;
}DAC_Value_Def;


typedef struct
{
    uint16_t dutyA;
    uint16_t dutyB;
}pwm_value_s;

typedef struct
{
    analog1_s       data1;//Vin,Vo,Io,Iin
    analog2_s       data2;//EXT_D,Temperature;

    gpio_out_s      data3_gpio_state;//ENA,ENB,CTRL_M,CTRL_LED,CTRL_R
    uint8_t         data4_protected_pin;

    DAC_Value_Def   _dac_hw;
    pwm_value_s     _pwm_hw;

    uint8_t         Mode_work;

}Recieve_data2_Model_MainData;
#pragma pack(pop)

#pragma pack(push, 1)

typedef struct
{
    float Kp;
    float Ki;
    float Kd;
    float Saturation_P;
    float Saturation_N;
    float MAX;
    float MIN;
    float REF;

}Setting_Reg_PID;

typedef struct
{
    Setting_Reg_PID _pid[2];

}Recieve_data3_Model_SettingsReg;
#pragma pack(pop)




#pragma pack(push, 1)

typedef  struct
{
    float pwm_duty[2];//в процентах
    uint16_t PWM_Code[2];
}PWM_value;


typedef struct
{
    float DAC_Voltage;//в процентах
    uint16_t DAC_Code;
}DAC_OUT_value;


typedef struct
{
    uint8_t firmwire[8];
    char Board_name[8];
    uint16_t Id_Recieve;

    float Voltage[4];
    float Current[2];

    uint8_t gpio_state[2];//Recieve,Transmit

    PWM_value       _pwm[2];//Recieve,Transmit
    DAC_OUT_value   _dac[2];

    State_push_pull_Converter       _status;
    QString                         _NameStatus;
    Setting_Reg_PID                 _settings[2];


}Model_MainData_def;
#pragma pack(pop)







class ISCV_Logic : public QObject
{
    Q_OBJECT
public:
    explicit ISCV_Logic(QObject *parent = nullptr);

    void Set_ID(uint16_t Id_dev);
    void Set_Seettings_Board(QSerialProtocol *protocol,uint8_t *data, uint16_t length);
    void Request_Flash(QSerialProtocol *protocol);
    void Parse_Data_RX(Command_Board cmd,protocol_Frame_data *data);
    void Update_UI(Command_Board cmd,QLabel *data);

     void Imulator_recieve_data_Start(void);
     void Request_RS_Connect(QSerialProtocol *protocol);
     void Request_Get_Config_Board(QSerialProtocol *protocol);
     void Request_Get_MainData(QSerialProtocol *protocol);
     void Request_read_Settings_Peripheral(QSerialProtocol *protocol);

     void Request_Set_PWM(QSerialProtocol *protocol,uint16_t data[]);
     void Request_Set_DAC(QSerialProtocol *protocol,uint16_t data);
     void Request_Set_GPIO(QSerialProtocol *protocol,uint8_t gpio);
     void Request_Set_Mode_Test(QSerialProtocol *protocol);


     float* Get_Voltage();
     float* Get_Current();





public:
     QString Name[13]=
     {
         "Входное напряжение",
         "Выходное напряжение",
         "Входной ток",
         "Выходной ток",
         "Уровень генератора",
         "Температура",
         "PWM HW A",
         "PWM A",
         "PWM HW B",
         "PWM B",
         "DAC HW",
         "DAC Voltage"
         "Status"

     };

     QString NameVoltage[4]=
     {
         "Vin",
         "Vout",
         "Vgen",
         "Temp"
     };

     QString NameCurrent[2]=
     {
         "Iin",
         "Iout"
     };



     QString NameGPIO[6]=
     {
         "CTRL_M",
         "CTRL_LED",
         "CTRL_R",
         "ENA",
         "ENB",
         "PIN_PROT"
     };



    QString Status_Text[15]=
     {
         "STATUS_INIT",
         "STATUS_NORMAL",
         "STATUS_NORMAL_GEN_ON",
         "STATUS_NORMAL_GEN_OFF",
         "STATUS_NORMAL_Charge_GEN_OFF",//перекачка энергии из GB1 в GB2
         "STATUS_TEST",

         "STATUS_ERROR_Input_Voltage",//необходимо обслуживание GB1
         "STATUS_ERROR_OUTPUT_Voltage",//необходимо обслуживание GB2
         "STATUS_ERROR_Breakage_OUT_NET",//обрыв цепи заряда
         "STATUS_ERROR_Short_OUT_NET",		//кз цепи заряда
         "STATUS_ERROR_Short_GB1",//КЗ банок GB1
         "STATUS_ERROR_Short_GB2",//КЗ банок GB2
         "STATUS_ERROR_Low_Voltage_D",//низкое напряжение на клеме D
         "STATUS_ERROR_Low_Input_Voltage",
         "STATUS_ERROR_High_Input_Voltage"//высокое входное напряжение
     };



private slots:
    void Timer_Imulator_reciever(void);

signals:
    void Signal_Timer_Imulator_reciever(void);

private:
    uint16_t ID=0;
    double count_Packet=0;

    Recieve_data1_Model_DataBoard       *recieve_data1;
    Recieve_data2_Model_MainData        *recieve_data2;
    Recieve_data3_Model_SettingsReg     *recieve_data3;


    Model_MainData_def                  _mainData;
     QTimer*                             _timer_work;

     void Update_Model(Recieve_data1_Model_DataBoard *data);
     void Update_Model(Recieve_data2_Model_MainData *data);
     void Update_Model(Recieve_data3_Model_SettingsReg *data);
     void Debug_info_Packet_2(void);
};

#endif // ISCV_LOGIC_H
