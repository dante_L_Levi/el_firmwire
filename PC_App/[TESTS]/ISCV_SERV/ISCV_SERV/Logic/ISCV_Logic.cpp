#include "ISCV_Logic.h"
#include "QDebug"

ISCV_Logic::ISCV_Logic(QObject *parent) : QObject(parent)
{
   for(int i=0;i<8;i++)
   {
       _mainData.Board_name[i]=' ';
       _mainData.firmwire[i]=' ';


   }
}

void ISCV_Logic::Set_ID(uint16_t Id_dev)
{
     ID=Id_dev;
}

void ISCV_Logic::Set_Seettings_Board(QSerialProtocol *protocol, uint8_t *data, uint16_t length)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_peripheral_board;
    sendMsg.length=length;
    sendMsg.data=data;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Flash(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_cmd_flash;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);

}

void ISCV_Logic::Parse_Data_RX(Command_Board cmd, protocol_Frame_data *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
            recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
            //memcpy(recieve_data1,data->data,data->length);
            count_Packet++;
            Update_Model(recieve_data1);

        break;
        }

        case Get_main_data:
        {
            recieve_data2=reinterpret_cast<Recieve_data2_Model_MainData*>(data->data);
            count_Packet++;
            Update_Model(recieve_data2);
            break;
        }

        case Get_setting_peripheral_board:
        {
            recieve_data3=reinterpret_cast<Recieve_data3_Model_SettingsReg*>(data->data);
            count_Packet++;
            Update_Model(recieve_data3);
            break;
        }

        default:
            break;

    }
}

void ISCV_Logic::Update_UI(Command_Board cmd, QLabel *data)
{
     switch (cmd)
     {
         case Get_Board:
         {
         char dataName[8]={0};
         int i=0;
         while(_mainData.Board_name[i]!=' ')
         {
             dataName[i]=_mainData.Board_name[i];
             i++;
         }

         QString dataBoard=QString(
                         "%1:%5\n"
                         "%2:%6\n"
                         "%3:%7\n"
                         "%4:%8\n"

                      ).arg("ID")
                      .arg("Version")
                      .arg("NameBoard")
                      .arg("Recieve Packetes:")
                      .arg(_mainData.Id_Recieve)
                 .arg((const char*)_mainData.firmwire)
                 .arg(dataName)
                 .arg(count_Packet);
                  data->setText(dataBoard) ;

            break;
         }

     case Get_main_data:
     {
         QString dataMain=QString(
                     "%1:\t\t%14 В\n"
                     "%2:\t\t%15 В\n"
                     "%3:\t\t%16 А\n"
                     "%4:\t\t%17 А\n"
                     "%5:\t\t%18 В\n"
                     "%6:\t\t%19 С\n"
                     "%7:\t\t%20 \n"
                     "%8:\t\t%21 %\n"
                     "%9:\t\t%22 \n"
                     "%10:\t\t%23 мВ\n"
                     "%11:\t\t%24 \n"
                     "%12:\t\t%25 \n"
                     "%13:\t\t%26 \n"


                     ).arg(Name[0])
                 .arg(Name[1])
                 .arg(Name[2])
                 .arg(Name[3])
                 .arg(Name[4])
                 .arg(Name[5])
                 .arg(Name[6])
                 .arg(Name[7])
                 .arg(Name[8])
                 .arg(Name[9])
                 .arg(Name[10])
                 .arg(Name[11])
                 .arg(Name[12])

                 .arg(_mainData.Voltage[0])
                 .arg(_mainData.Voltage[1])
                 .arg(_mainData.Current[0])
                 .arg(_mainData.Current[1])
                 .arg(_mainData.Voltage[2])
                 .arg(_mainData.Voltage[3])

                 .arg(_mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_A])
                 .arg(_mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_A])
                 .arg(_mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_B])
                 .arg(_mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_B])

                 .arg(_mainData._dac[INDEX_RECIEVE].DAC_Code)
                 .arg(_mainData._dac[INDEX_RECIEVE].DAC_Voltage)

                 .arg(_mainData._NameStatus);
                 data->setText(dataMain);


        break;
     }
     default:
         break;


     }
}

void ISCV_Logic::Imulator_recieve_data_Start()
{
    _timer_work=new QTimer();
    _timer_work->setInterval(250);
    connect(_timer_work, SIGNAL(timeout()), this,
            SLOT(Timer_Imulator_reciever()));
    _timer_work->start();
}

void ISCV_Logic::Request_RS_Connect(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Connect;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Get_Config_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_Board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Get_MainData(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_main_data;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_read_Settings_Peripheral(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Set_PWM(QSerialProtocol *protocol, uint16_t data[])
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_PWM;
    uint8_t dt[32]={0};
    memcpy(dt,data,4);
    sendMsg.length=sizeof (dt);
    sendMsg.data=(uint8_t*)dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Set_DAC(QSerialProtocol *protocol, uint16_t data)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_DAC;
    uint8_t dt[32]={0};
    memcpy(dt,&data,2);
    sendMsg.length=sizeof(dt);
    sendMsg.data=(uint8_t*)&dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Set_GPIO(QSerialProtocol *protocol, uint8_t gpio)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Gpio;
    uint8_t dt[32]={0};
    dt[0]=gpio;
    sendMsg.length=1;
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_Logic::Request_Set_Mode_Test(QSerialProtocol *protocol)
{
     protocol_Frame_data sendMsg;
     sendMsg.command=Set_Mode_Work;

}

float *ISCV_Logic::Get_Voltage()
{

    return _mainData.Voltage;
}

float *ISCV_Logic::Get_Current()
{
    return _mainData.Current;
}

void ISCV_Logic::Timer_Imulator_reciever()
{
    emit Signal_Timer_Imulator_reciever();
}


void ISCV_Logic::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->datafirmwire,8);
    memcpy(_mainData.Board_name,data->dataName,8);
    _mainData.Id_Recieve=data->ID;
}

void ISCV_Logic::Debug_info_Packet_2(void)
{
    qWarning()<<"---------------------------------------------------";
    qWarning()<<"data1.fields.output_14V:"<<_mainData.Voltage[0];
    qWarning()<<"data1.fields.output_28V:"<<_mainData.Voltage[1];
    qWarning()<<"data1.fields.extern_in_D:"<<_mainData.Voltage[2];
    qWarning()<<"data1.fields.temperature_sense:"<<_mainData.Voltage[3];

    qWarning()<<"data1.fields.input_current:"<<_mainData.Current[0];
    qWarning()<<"data1.fields.output_current:"<<_mainData.Current[1];

    qWarning()<<"DAC_Code:"<< _mainData._dac[INDEX_RECIEVE].DAC_Code;
    qWarning()<<"DAC_Voltage:"<<_mainData._dac[INDEX_RECIEVE].DAC_Voltage;

    qWarning()<<"PWM_Code[INDEX_DUTY_A]:"<<  _mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_A];
    qWarning()<<"pwm_duty[INDEX_DUTY_A]:"<<_mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_A];

    qWarning()<<"PWM_Code[INDEX_DUTY_B]:"<<  _mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_B];
    qWarning()<<"pwm_duty[INDEX_DUTY_B]:"<<_mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_B];

     qWarning()<<"State_push_pull_Converter:"<<_mainData._NameStatus;
     qWarning()<<"_mainData.gpio_state[INDEX_RECIEVE]:"<<_mainData.gpio_state[INDEX_RECIEVE];
     qWarning()<<"---------------------------------------------------";

}

void ISCV_Logic::Update_Model(Recieve_data2_Model_MainData *data)
{
    _mainData.Voltage[0]=(float)(data->data1.fields.output_14V/1000.0);
    _mainData.Voltage[1]=(float)(data->data1.fields.output_28V/1000.0);
    _mainData.Voltage[2]=(float)(data->data2.fields.extern_in_D/1000.0);
    _mainData.Voltage[3]=data->data2.fields.temperature_sense;

    _mainData.Current[0]=(float)(data->data1.fields.input_current/1000.0);
    _mainData.Current[1]=(float)(data->data1.fields.output_current/1000.0);

    _mainData._dac[INDEX_RECIEVE].DAC_Code=data->_dac_hw.outDAC;
    _mainData._dac[INDEX_RECIEVE].DAC_Voltage=(float)(_mainData._dac[INDEX_RECIEVE].DAC_Code*(3.3/4095.0));

    _mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_A]=data->_pwm_hw.dutyA;
    _mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_A]=((_mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_A]*1)/45000);


    _mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_B]=data->_pwm_hw.dutyB;
    _mainData._pwm[INDEX_RECIEVE].pwm_duty[INDEX_DUTY_B]=((_mainData._pwm[INDEX_RECIEVE].PWM_Code[INDEX_DUTY_B]*1)/45000);


    _mainData._status=(State_push_pull_Converter)(data->Mode_work);
    _mainData.gpio_state[INDEX_RECIEVE]=data->data3_gpio_state.all;

    _mainData._NameStatus=Status_Text[data->Mode_work];
    //Debug_info_Packet_2();

}

void ISCV_Logic::Update_Model(Recieve_data3_Model_SettingsReg *data)
{
    for(int i=0;i<2;i++)
    {
        _mainData._settings[i].Kp=data->_pid[i].Kp;
        _mainData._settings[i].Ki=data->_pid[i].Ki;
        _mainData._settings[i].Kd=data->_pid[i].Kd;
        _mainData._settings[i].MAX=data->_pid[i].MAX;
        _mainData._settings[i].MIN=data->_pid[i].MIN;
        _mainData._settings[i].Saturation_P=data->_pid[i].Saturation_P;
        _mainData._settings[i].Saturation_N=data->_pid[i].Saturation_N;
        _mainData._settings[i].REF=data->_pid[i].REF;

    }


}
