#ifndef SERIALPORT_CRC8_H
#define SERIALPORT_CRC8_H


#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "QString"
#include "QtSerialPort/QSerialPortInfo"
#include "QtSerialPort/QSerialPort"
#include "QDebug"
#include "QStringList"
#include <QTimer>


class SerialPort:public QSerialPort, public QSerialPortInfo
{
    Q_OBJECT
public:
    SerialPort();
    bool ConnectPort(QString Name,int _baudRate);
    void DisconnectedPort();
    bool status_connected(){return IsConnected;}
    QStringList Get_NamesPort();
    QStringList Get_Baud_rate();




    private:
        bool IsConnected=false;
        QStringList _dataRate=
        {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"

        };
        QStringList _namePort;


signals:
    void Get_data_Recieved_Serial(QByteArray data);
    void connectedPortChanged(bool connect);

private slots:
    /**************Read data Serial Port *********************/
    void ReadSerialPort_data();


public slots:
    void transmitData(QByteArray *data);



};

#endif // SERIALPORT_CRC8_H
