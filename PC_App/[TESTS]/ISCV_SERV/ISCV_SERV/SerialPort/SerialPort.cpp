#include "SerialPort.h"





SerialPort::SerialPort()
{

}

bool SerialPort::ConnectPort(QString Name, int _baudRate)
{

    this->setPortName(Name);
    this->setBaudRate(_baudRate);
    this->setDataBits(QSerialPort::Data8);
    this->setParity(QSerialPort::NoParity);
    this->setStopBits(QSerialPort::OneStop);
    this->setFlowControl(QSerialPort::NoFlowControl);
    connect(this,&QSerialPort::readyRead,this,&SerialPort::ReadSerialPort_data);
    IsConnected=this->open(QIODevice::ReadWrite);
    emit connectedPortChanged(IsConnected);
    return IsConnected;

}

void SerialPort::DisconnectedPort()
{
    if(IsConnected)
    {
        this->close();
        IsConnected=false;
        emit connectedPortChanged(IsConnected);
    }
}

QStringList SerialPort::Get_NamesPort()
{
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
    {
        _namePort.append(info.portName());
    }

    return _namePort;
}

QStringList SerialPort::Get_Baud_rate()
{
    return _dataRate;
}

void SerialPort:: ReadSerialPort_data()
{
    QByteArray buf=this->read(10000);

    emit Get_data_Recieved_Serial(buf);
}

void SerialPort::transmitData(QByteArray *data)
{
    if(!IsConnected)
        return;

    this->waitForBytesWritten(1000);
    char* dataOut=data->data();
    uint32_t len=data->length();
    this->write(dataOut,len);
}








