/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_12;
    QHBoxLayout *horizontalLayout_34;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QLabel *_status_ID;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QLabel *_status_firm;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_43;
    QLabel *_count_packet;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_5;
    QLabel *_status_NameBoard;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_22;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_13;
    QLabel *Voltage_Input;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_15;
    QLabel *Voltage_Output;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_17;
    QLabel *Current_Input;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_19;
    QLabel *Current_Output;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_21;
    QLabel *GB1;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_23;
    QLabel *GB2;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_9;
    QLabel *DAC_voltage;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QLabel *Current_THreshold;
    QHBoxLayout *horizontalLayout_11;
    QCheckBox *EN_CTRL_DAC;
    QSlider *CTRL_DAC;
    QVBoxLayout *verticalLayout_9;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *ENA;
    QCheckBox *EN_B;
    QCheckBox *CTRL_LED;
    QCheckBox *FLAG_PROTECTION;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_25;
    QLabel *Voltage_D;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_27;
    QLabel *pwm_A;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_29;
    QLabel *pwm_B;
    QHBoxLayout *horizontalLayout_21;
    QCheckBox *EN_CTRL_pwm;
    QSlider *CTRL_pwm;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_31;
    QPushButton *Btn_CTRL_M;
    QVBoxLayout *verticalLayout_11;
    QVBoxLayout *verticalLayout_10;
    QPushButton *_Btn_Start_request;
    QComboBox *comboBox;
    QPushButton *BTN_SET_Mode;
    QLabel *_Status_Work;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_33;
    QVBoxLayout *verticalLayout_13;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_33;
    QLineEdit *Kp_fill_V;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_34;
    QLineEdit *Kp_fill_I;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_36;
    QLineEdit *Ki_fill_V;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_35;
    QLineEdit *Ki_fill_I;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_38;
    QLineEdit *Kd_fill_V;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_37;
    QLineEdit *Kd_fill_I;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_40;
    QLineEdit *IREF;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_39;
    QLineEdit *VREF;
    QVBoxLayout *verticalLayout_17;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_41;
    QLineEdit *Kp_Analog;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_42;
    QLineEdit *lineEdit_10;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout_16;
    QVBoxLayout *verticalLayout_15;
    QPushButton *_Btn_SetSettings;
    QPushButton *_Btn_flash_Write;
    QPushButton *_Btn_Read_settings;
    QPushButton *pushButton_6;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_47;
    QHBoxLayout *horizontalLayout_46;
    QVBoxLayout *verticalLayout_22;
    QVBoxLayout *verticalLayout_21;
    QHBoxLayout *horizontalLayout_35;
    QLabel *label_6;
    QSlider *horizontalSlider;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_7;
    QSlider *horizontalSlider_2;
    QLineEdit *lineEdit_2;
    QHBoxLayout *horizontalLayout_37;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QHBoxLayout *horizontalLayout_45;
    QVBoxLayout *verticalLayout_18;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_14;
    QLabel *Voltage_Input_2;
    QHBoxLayout *horizontalLayout_39;
    QLabel *label_16;
    QLabel *Voltage_Output_2;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_18;
    QLabel *Current_Input_2;
    QHBoxLayout *horizontalLayout_41;
    QLabel *label_20;
    QLabel *Current_Output_2;
    QHBoxLayout *horizontalLayout_42;
    QLabel *label_22;
    QLabel *GB1_2;
    QHBoxLayout *horizontalLayout_43;
    QLabel *label_24;
    QLabel *GB2_2;
    QHBoxLayout *horizontalLayout_44;
    QLabel *label_26;
    QLabel *Voltage_D_2;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_20;
    QPushButton *pushButton_5;
    QPushButton *pushButton_9;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(679, 642);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_12 = new QVBoxLayout(centralwidget);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setObjectName(QString::fromUtf8("horizontalLayout_34"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 200));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_34->addWidget(groupBox_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_5->addWidget(label_3);

        _status_ID = new QLabel(centralwidget);
        _status_ID->setObjectName(QString::fromUtf8("_status_ID"));

        horizontalLayout_5->addWidget(_status_ID);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_6->addWidget(label_4);

        _status_firm = new QLabel(centralwidget);
        _status_firm->setObjectName(QString::fromUtf8("_status_firm"));

        horizontalLayout_6->addWidget(_status_firm);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_43 = new QLabel(centralwidget);
        label_43->setObjectName(QString::fromUtf8("label_43"));

        horizontalLayout_8->addWidget(label_43);

        _count_packet = new QLabel(centralwidget);
        _count_packet->setObjectName(QString::fromUtf8("_count_packet"));

        horizontalLayout_8->addWidget(_count_packet);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_7->addWidget(label_5);

        _status_NameBoard = new QLabel(centralwidget);
        _status_NameBoard->setObjectName(QString::fromUtf8("_status_NameBoard"));

        horizontalLayout_7->addWidget(_status_NameBoard);


        verticalLayout->addLayout(horizontalLayout_7);


        horizontalLayout_34->addLayout(verticalLayout);


        verticalLayout_12->addLayout(horizontalLayout_34);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_22 = new QHBoxLayout(tab);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_12->addWidget(label_13);

        Voltage_Input = new QLabel(groupBox);
        Voltage_Input->setObjectName(QString::fromUtf8("Voltage_Input"));

        horizontalLayout_12->addWidget(Voltage_Input);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_13->addWidget(label_15);

        Voltage_Output = new QLabel(groupBox);
        Voltage_Output->setObjectName(QString::fromUtf8("Voltage_Output"));

        horizontalLayout_13->addWidget(Voltage_Output);


        verticalLayout_3->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_17 = new QLabel(groupBox);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_14->addWidget(label_17);

        Current_Input = new QLabel(groupBox);
        Current_Input->setObjectName(QString::fromUtf8("Current_Input"));

        horizontalLayout_14->addWidget(Current_Input);


        verticalLayout_3->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_19 = new QLabel(groupBox);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_15->addWidget(label_19);

        Current_Output = new QLabel(groupBox);
        Current_Output->setObjectName(QString::fromUtf8("Current_Output"));

        horizontalLayout_15->addWidget(Current_Output);


        verticalLayout_3->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        horizontalLayout_16->addWidget(label_21);

        GB1 = new QLabel(groupBox);
        GB1->setObjectName(QString::fromUtf8("GB1"));

        horizontalLayout_16->addWidget(GB1);


        verticalLayout_3->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_23 = new QLabel(groupBox);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        horizontalLayout_17->addWidget(label_23);

        GB2 = new QLabel(groupBox);
        GB2->setObjectName(QString::fromUtf8("GB2"));

        horizontalLayout_17->addWidget(GB2);


        verticalLayout_3->addLayout(horizontalLayout_17);


        verticalLayout_4->addWidget(groupBox);

        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_9->addWidget(label_9);

        DAC_voltage = new QLabel(groupBox_3);
        DAC_voltage->setObjectName(QString::fromUtf8("DAC_voltage"));

        horizontalLayout_9->addWidget(DAC_voltage);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_10->addWidget(label_10);

        Current_THreshold = new QLabel(groupBox_3);
        Current_THreshold->setObjectName(QString::fromUtf8("Current_THreshold"));

        horizontalLayout_10->addWidget(Current_THreshold);


        verticalLayout_2->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        EN_CTRL_DAC = new QCheckBox(groupBox_3);
        EN_CTRL_DAC->setObjectName(QString::fromUtf8("EN_CTRL_DAC"));

        horizontalLayout_11->addWidget(EN_CTRL_DAC);

        CTRL_DAC = new QSlider(groupBox_3);
        CTRL_DAC->setObjectName(QString::fromUtf8("CTRL_DAC"));
        CTRL_DAC->setEnabled(false);
        CTRL_DAC->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(CTRL_DAC);


        verticalLayout_2->addLayout(horizontalLayout_11);


        verticalLayout_4->addWidget(groupBox_3);


        horizontalLayout_22->addLayout(verticalLayout_4);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        verticalLayout_6 = new QVBoxLayout(groupBox_4);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        ENA = new QCheckBox(groupBox_4);
        ENA->setObjectName(QString::fromUtf8("ENA"));

        verticalLayout_5->addWidget(ENA);

        EN_B = new QCheckBox(groupBox_4);
        EN_B->setObjectName(QString::fromUtf8("EN_B"));

        verticalLayout_5->addWidget(EN_B);

        CTRL_LED = new QCheckBox(groupBox_4);
        CTRL_LED->setObjectName(QString::fromUtf8("CTRL_LED"));

        verticalLayout_5->addWidget(CTRL_LED);

        FLAG_PROTECTION = new QCheckBox(groupBox_4);
        FLAG_PROTECTION->setObjectName(QString::fromUtf8("FLAG_PROTECTION"));

        verticalLayout_5->addWidget(FLAG_PROTECTION);


        verticalLayout_6->addLayout(verticalLayout_5);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_25 = new QLabel(groupBox_4);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        horizontalLayout_18->addWidget(label_25);

        Voltage_D = new QLabel(groupBox_4);
        Voltage_D->setObjectName(QString::fromUtf8("Voltage_D"));

        horizontalLayout_18->addWidget(Voltage_D);


        verticalLayout_6->addLayout(horizontalLayout_18);


        verticalLayout_9->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(tab);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        verticalLayout_7 = new QVBoxLayout(groupBox_5);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_27 = new QLabel(groupBox_5);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        horizontalLayout_19->addWidget(label_27);

        pwm_A = new QLabel(groupBox_5);
        pwm_A->setObjectName(QString::fromUtf8("pwm_A"));
        pwm_A->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(pwm_A);


        verticalLayout_7->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_29 = new QLabel(groupBox_5);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        horizontalLayout_20->addWidget(label_29);

        pwm_B = new QLabel(groupBox_5);
        pwm_B->setObjectName(QString::fromUtf8("pwm_B"));
        pwm_B->setAlignment(Qt::AlignCenter);

        horizontalLayout_20->addWidget(pwm_B);


        verticalLayout_7->addLayout(horizontalLayout_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        EN_CTRL_pwm = new QCheckBox(groupBox_5);
        EN_CTRL_pwm->setObjectName(QString::fromUtf8("EN_CTRL_pwm"));

        horizontalLayout_21->addWidget(EN_CTRL_pwm);

        CTRL_pwm = new QSlider(groupBox_5);
        CTRL_pwm->setObjectName(QString::fromUtf8("CTRL_pwm"));
        CTRL_pwm->setEnabled(false);
        CTRL_pwm->setOrientation(Qt::Horizontal);

        horizontalLayout_21->addWidget(CTRL_pwm);


        verticalLayout_7->addLayout(horizontalLayout_21);


        verticalLayout_9->addWidget(groupBox_5);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_31 = new QLabel(tab);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_31);

        Btn_CTRL_M = new QPushButton(tab);
        Btn_CTRL_M->setObjectName(QString::fromUtf8("Btn_CTRL_M"));

        verticalLayout_8->addWidget(Btn_CTRL_M);


        verticalLayout_9->addLayout(verticalLayout_8);


        horizontalLayout_22->addLayout(verticalLayout_9);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        _Btn_Start_request = new QPushButton(tab);
        _Btn_Start_request->setObjectName(QString::fromUtf8("_Btn_Start_request"));

        verticalLayout_10->addWidget(_Btn_Start_request);

        comboBox = new QComboBox(tab);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        verticalLayout_10->addWidget(comboBox);

        BTN_SET_Mode = new QPushButton(tab);
        BTN_SET_Mode->setObjectName(QString::fromUtf8("BTN_SET_Mode"));

        verticalLayout_10->addWidget(BTN_SET_Mode);

        _Status_Work = new QLabel(tab);
        _Status_Work->setObjectName(QString::fromUtf8("_Status_Work"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        _Status_Work->setFont(font);
        _Status_Work->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(_Status_Work);


        verticalLayout_11->addLayout(verticalLayout_10);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_11->addItem(verticalSpacer);


        horizontalLayout_22->addLayout(verticalLayout_11);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_33 = new QHBoxLayout(tab_2);
        horizontalLayout_33->setObjectName(QString::fromUtf8("horizontalLayout_33"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_33 = new QLabel(tab_2);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        horizontalLayout_23->addWidget(label_33);

        Kp_fill_V = new QLineEdit(tab_2);
        Kp_fill_V->setObjectName(QString::fromUtf8("Kp_fill_V"));

        horizontalLayout_23->addWidget(Kp_fill_V);


        verticalLayout_13->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_34 = new QLabel(tab_2);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        horizontalLayout_24->addWidget(label_34);

        Kp_fill_I = new QLineEdit(tab_2);
        Kp_fill_I->setObjectName(QString::fromUtf8("Kp_fill_I"));

        horizontalLayout_24->addWidget(Kp_fill_I);


        verticalLayout_13->addLayout(horizontalLayout_24);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        label_36 = new QLabel(tab_2);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        horizontalLayout_26->addWidget(label_36);

        Ki_fill_V = new QLineEdit(tab_2);
        Ki_fill_V->setObjectName(QString::fromUtf8("Ki_fill_V"));

        horizontalLayout_26->addWidget(Ki_fill_V);


        verticalLayout_13->addLayout(horizontalLayout_26);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        label_35 = new QLabel(tab_2);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        horizontalLayout_25->addWidget(label_35);

        Ki_fill_I = new QLineEdit(tab_2);
        Ki_fill_I->setObjectName(QString::fromUtf8("Ki_fill_I"));

        horizontalLayout_25->addWidget(Ki_fill_I);


        verticalLayout_13->addLayout(horizontalLayout_25);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setObjectName(QString::fromUtf8("horizontalLayout_28"));
        label_38 = new QLabel(tab_2);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        horizontalLayout_28->addWidget(label_38);

        Kd_fill_V = new QLineEdit(tab_2);
        Kd_fill_V->setObjectName(QString::fromUtf8("Kd_fill_V"));

        horizontalLayout_28->addWidget(Kd_fill_V);


        verticalLayout_13->addLayout(horizontalLayout_28);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        label_37 = new QLabel(tab_2);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        horizontalLayout_27->addWidget(label_37);

        Kd_fill_I = new QLineEdit(tab_2);
        Kd_fill_I->setObjectName(QString::fromUtf8("Kd_fill_I"));

        horizontalLayout_27->addWidget(Kd_fill_I);


        verticalLayout_13->addLayout(horizontalLayout_27);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        label_40 = new QLabel(tab_2);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        horizontalLayout_30->addWidget(label_40);

        IREF = new QLineEdit(tab_2);
        IREF->setObjectName(QString::fromUtf8("IREF"));

        horizontalLayout_30->addWidget(IREF);


        verticalLayout_13->addLayout(horizontalLayout_30);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setObjectName(QString::fromUtf8("horizontalLayout_29"));
        label_39 = new QLabel(tab_2);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        horizontalLayout_29->addWidget(label_39);

        VREF = new QLineEdit(tab_2);
        VREF->setObjectName(QString::fromUtf8("VREF"));

        horizontalLayout_29->addWidget(VREF);


        verticalLayout_13->addLayout(horizontalLayout_29);


        horizontalLayout_33->addLayout(verticalLayout_13);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        label_41 = new QLabel(tab_2);
        label_41->setObjectName(QString::fromUtf8("label_41"));

        horizontalLayout_31->addWidget(label_41);

        Kp_Analog = new QLineEdit(tab_2);
        Kp_Analog->setObjectName(QString::fromUtf8("Kp_Analog"));

        horizontalLayout_31->addWidget(Kp_Analog);


        verticalLayout_14->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setObjectName(QString::fromUtf8("horizontalLayout_32"));
        label_42 = new QLabel(tab_2);
        label_42->setObjectName(QString::fromUtf8("label_42"));

        horizontalLayout_32->addWidget(label_42);

        lineEdit_10 = new QLineEdit(tab_2);
        lineEdit_10->setObjectName(QString::fromUtf8("lineEdit_10"));

        horizontalLayout_32->addWidget(lineEdit_10);


        verticalLayout_14->addLayout(horizontalLayout_32);


        verticalLayout_17->addLayout(verticalLayout_14);

        verticalSpacer_3 = new QSpacerItem(20, 348, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer_3);


        horizontalLayout_33->addLayout(verticalLayout_17);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        _Btn_SetSettings = new QPushButton(tab_2);
        _Btn_SetSettings->setObjectName(QString::fromUtf8("_Btn_SetSettings"));

        verticalLayout_15->addWidget(_Btn_SetSettings);

        _Btn_flash_Write = new QPushButton(tab_2);
        _Btn_flash_Write->setObjectName(QString::fromUtf8("_Btn_flash_Write"));

        verticalLayout_15->addWidget(_Btn_flash_Write);

        _Btn_Read_settings = new QPushButton(tab_2);
        _Btn_Read_settings->setObjectName(QString::fromUtf8("_Btn_Read_settings"));

        verticalLayout_15->addWidget(_Btn_Read_settings);

        pushButton_6 = new QPushButton(tab_2);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        verticalLayout_15->addWidget(pushButton_6);


        verticalLayout_16->addLayout(verticalLayout_15);

        verticalSpacer_2 = new QSpacerItem(20, 278, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_2);


        horizontalLayout_33->addLayout(verticalLayout_16);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        horizontalLayout_47 = new QHBoxLayout(tab_3);
        horizontalLayout_47->setObjectName(QString::fromUtf8("horizontalLayout_47"));
        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setObjectName(QString::fromUtf8("horizontalLayout_46"));
        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setObjectName(QString::fromUtf8("horizontalLayout_35"));
        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_35->addWidget(label_6);

        horizontalSlider = new QSlider(tab_3);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_35->addWidget(horizontalSlider);

        lineEdit = new QLineEdit(tab_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_35->addWidget(lineEdit);


        verticalLayout_21->addLayout(horizontalLayout_35);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setObjectName(QString::fromUtf8("horizontalLayout_36"));
        label_7 = new QLabel(tab_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_36->addWidget(label_7);

        horizontalSlider_2 = new QSlider(tab_3);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        horizontalLayout_36->addWidget(horizontalSlider_2);

        lineEdit_2 = new QLineEdit(tab_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        horizontalLayout_36->addWidget(lineEdit_2);


        verticalLayout_21->addLayout(horizontalLayout_36);

        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setObjectName(QString::fromUtf8("horizontalLayout_37"));
        pushButton = new QPushButton(tab_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_37->addWidget(pushButton);

        pushButton_2 = new QPushButton(tab_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_37->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(tab_3);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_37->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(tab_3);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout_37->addWidget(pushButton_4);


        verticalLayout_21->addLayout(horizontalLayout_37);


        verticalLayout_22->addLayout(verticalLayout_21);

        horizontalLayout_45 = new QHBoxLayout();
        horizontalLayout_45->setObjectName(QString::fromUtf8("horizontalLayout_45"));
        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        groupBox_6 = new QGroupBox(tab_3);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        verticalLayout_19 = new QVBoxLayout(groupBox_6);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setObjectName(QString::fromUtf8("horizontalLayout_38"));
        label_14 = new QLabel(groupBox_6);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_38->addWidget(label_14);

        Voltage_Input_2 = new QLabel(groupBox_6);
        Voltage_Input_2->setObjectName(QString::fromUtf8("Voltage_Input_2"));

        horizontalLayout_38->addWidget(Voltage_Input_2);


        verticalLayout_19->addLayout(horizontalLayout_38);

        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setObjectName(QString::fromUtf8("horizontalLayout_39"));
        label_16 = new QLabel(groupBox_6);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_39->addWidget(label_16);

        Voltage_Output_2 = new QLabel(groupBox_6);
        Voltage_Output_2->setObjectName(QString::fromUtf8("Voltage_Output_2"));

        horizontalLayout_39->addWidget(Voltage_Output_2);


        verticalLayout_19->addLayout(horizontalLayout_39);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setObjectName(QString::fromUtf8("horizontalLayout_40"));
        label_18 = new QLabel(groupBox_6);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_40->addWidget(label_18);

        Current_Input_2 = new QLabel(groupBox_6);
        Current_Input_2->setObjectName(QString::fromUtf8("Current_Input_2"));

        horizontalLayout_40->addWidget(Current_Input_2);


        verticalLayout_19->addLayout(horizontalLayout_40);

        horizontalLayout_41 = new QHBoxLayout();
        horizontalLayout_41->setObjectName(QString::fromUtf8("horizontalLayout_41"));
        label_20 = new QLabel(groupBox_6);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_41->addWidget(label_20);

        Current_Output_2 = new QLabel(groupBox_6);
        Current_Output_2->setObjectName(QString::fromUtf8("Current_Output_2"));

        horizontalLayout_41->addWidget(Current_Output_2);


        verticalLayout_19->addLayout(horizontalLayout_41);

        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setObjectName(QString::fromUtf8("horizontalLayout_42"));
        label_22 = new QLabel(groupBox_6);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        horizontalLayout_42->addWidget(label_22);

        GB1_2 = new QLabel(groupBox_6);
        GB1_2->setObjectName(QString::fromUtf8("GB1_2"));

        horizontalLayout_42->addWidget(GB1_2);


        verticalLayout_19->addLayout(horizontalLayout_42);

        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setObjectName(QString::fromUtf8("horizontalLayout_43"));
        label_24 = new QLabel(groupBox_6);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        horizontalLayout_43->addWidget(label_24);

        GB2_2 = new QLabel(groupBox_6);
        GB2_2->setObjectName(QString::fromUtf8("GB2_2"));

        horizontalLayout_43->addWidget(GB2_2);


        verticalLayout_19->addLayout(horizontalLayout_43);


        verticalLayout_18->addWidget(groupBox_6);


        horizontalLayout_45->addLayout(verticalLayout_18);

        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setObjectName(QString::fromUtf8("horizontalLayout_44"));
        label_26 = new QLabel(tab_3);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        horizontalLayout_44->addWidget(label_26);

        Voltage_D_2 = new QLabel(tab_3);
        Voltage_D_2->setObjectName(QString::fromUtf8("Voltage_D_2"));

        horizontalLayout_44->addWidget(Voltage_D_2);


        horizontalLayout_45->addLayout(horizontalLayout_44);


        verticalLayout_22->addLayout(horizontalLayout_45);


        horizontalLayout_46->addLayout(verticalLayout_22);

        groupBox_7 = new QGroupBox(tab_3);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        verticalLayout_20 = new QVBoxLayout(groupBox_7);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        pushButton_5 = new QPushButton(groupBox_7);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        verticalLayout_20->addWidget(pushButton_5);

        pushButton_9 = new QPushButton(groupBox_7);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        verticalLayout_20->addWidget(pushButton_9);


        horizontalLayout_46->addWidget(groupBox_7);


        horizontalLayout_47->addLayout(horizontalLayout_46);

        tabWidget->addTab(tab_3, QString());

        verticalLayout_12->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "ID:", nullptr));
        _status_ID->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "firmwire:", nullptr));
        _status_firm->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_43->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\270\320\275\321\217\321\202\320\276 :", nullptr));
        _count_packet->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "NameBoard:", nullptr));
        _status_NameBoard->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Voltage/Current:", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "Voltage In:", nullptr));
        Voltage_Input->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Voltage Out:", nullptr));
        Voltage_Output->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "Current In:", nullptr));
        Current_Input->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "Current Out:", nullptr));
        Current_Output->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "GB1:", nullptr));
        GB1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "GB2:", nullptr));
        GB2->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "DAC:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Voltage:", nullptr));
        DAC_voltage->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Current:TH:", nullptr));
        Current_THreshold->setText(QApplication::translate("MainWindow", "0", nullptr));
        EN_CTRL_DAC->setText(QString());
        groupBox_4->setTitle(QApplication::translate("MainWindow", "GPIO STATE:", nullptr));
        ENA->setText(QApplication::translate("MainWindow", "ENA", nullptr));
        EN_B->setText(QApplication::translate("MainWindow", "ENB", nullptr));
        CTRL_LED->setText(QApplication::translate("MainWindow", "CTRL_R", nullptr));
        FLAG_PROTECTION->setText(QApplication::translate("MainWindow", "Flag Prot", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "Voltage D:", nullptr));
        Voltage_D->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "PWM:", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "ValueA:", nullptr));
        pwm_A->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "ValueB:", nullptr));
        pwm_B->setText(QApplication::translate("MainWindow", "0", nullptr));
        EN_CTRL_pwm->setText(QString());
        label_31->setText(QApplication::translate("MainWindow", "CTRL_M:", nullptr));
        Btn_CTRL_M->setText(QApplication::translate("MainWindow", "\320\262\321\213\320\272\320\273.", nullptr));
        _Btn_Start_request->setText(QApplication::translate("MainWindow", "Start", nullptr));
        BTN_SET_Mode->setText(QApplication::translate("MainWindow", "SET", nullptr));
        _Status_Work->setText(QApplication::translate("MainWindow", "STATUS", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "mainFill", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        label_36->setText(QApplication::translate("MainWindow", "Ki:", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "Ki:", nullptr));
        label_38->setText(QApplication::translate("MainWindow", "Kd:", nullptr));
        label_37->setText(QApplication::translate("MainWindow", "Kd:", nullptr));
        label_40->setText(QApplication::translate("MainWindow", "IREF:", nullptr));
        label_39->setText(QApplication::translate("MainWindow", "VREF:", nullptr));
        label_41->setText(QApplication::translate("MainWindow", "Kp analog:", nullptr));
        label_42->setText(QApplication::translate("MainWindow", "Kp:", nullptr));
        _Btn_SetSettings->setText(QApplication::translate("MainWindow", "SET", nullptr));
        _Btn_flash_Write->setText(QApplication::translate("MainWindow", "Flash", nullptr));
        _Btn_Read_settings->setText(QApplication::translate("MainWindow", "Read", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "RESET", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Settings", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "DAC:", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "PWM:", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "CTRL_R", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "CTRL_M", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "ENA", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "ENB", nullptr));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Voltage/Current:", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Voltage In:", nullptr));
        Voltage_Input_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Voltage Out:", nullptr));
        Voltage_Output_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "Current In:", nullptr));
        Current_Input_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Current Out:", nullptr));
        Current_Output_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "GB1:", nullptr));
        GB1_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "GB2:", nullptr));
        GB2_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "Voltage D:", nullptr));
        Voltage_D_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Request:", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "RequestSetting", nullptr));
        pushButton_9->setText(QApplication::translate("MainWindow", "Request AIN/DIN", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Testing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
