/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ISCV_SERVICE/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[25];
    char stringdata0[491];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 26), // "on_Name_update_btn_clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 27), // "on_on_ConnectButton_clicked"
QT_MOC_LITERAL(4, 67, 14), // "serialReceived"
QT_MOC_LITERAL(5, 82, 4), // "data"
QT_MOC_LITERAL(6, 87, 13), // "receivedFrame"
QT_MOC_LITERAL(7, 101, 20), // "protocol_Frame_data*"
QT_MOC_LITERAL(8, 122, 3), // "msg"
QT_MOC_LITERAL(9, 126, 13), // "Timer_Request"
QT_MOC_LITERAL(10, 140, 27), // "on_EN_CTRL_DAC_stateChanged"
QT_MOC_LITERAL(11, 168, 4), // "arg1"
QT_MOC_LITERAL(12, 173, 27), // "on_EN_CTRL_pwm_stateChanged"
QT_MOC_LITERAL(13, 201, 21), // "on_Btn_CTRL_M_clicked"
QT_MOC_LITERAL(14, 223, 23), // "on_BTN_SET_Mode_clicked"
QT_MOC_LITERAL(15, 247, 24), // "on_CTRL_DAC_valueChanged"
QT_MOC_LITERAL(16, 272, 5), // "value"
QT_MOC_LITERAL(17, 278, 24), // "on_CTRL_pwm_valueChanged"
QT_MOC_LITERAL(18, 303, 27), // "on__Btn_SetSettings_clicked"
QT_MOC_LITERAL(19, 331, 27), // "on__Btn_flash_Write_clicked"
QT_MOC_LITERAL(20, 359, 29), // "on__Btn_Read_settings_clicked"
QT_MOC_LITERAL(21, 389, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(22, 413, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(23, 437, 23), // "on_pushButton_9_clicked"
QT_MOC_LITERAL(24, 461, 29) // "on__Btn_Start_request_clicked"

    },
    "MainWindow\0on_Name_update_btn_clicked\0"
    "\0on_on_ConnectButton_clicked\0"
    "serialReceived\0data\0receivedFrame\0"
    "protocol_Frame_data*\0msg\0Timer_Request\0"
    "on_EN_CTRL_DAC_stateChanged\0arg1\0"
    "on_EN_CTRL_pwm_stateChanged\0"
    "on_Btn_CTRL_M_clicked\0on_BTN_SET_Mode_clicked\0"
    "on_CTRL_DAC_valueChanged\0value\0"
    "on_CTRL_pwm_valueChanged\0"
    "on__Btn_SetSettings_clicked\0"
    "on__Btn_flash_Write_clicked\0"
    "on__Btn_Read_settings_clicked\0"
    "on_pushButton_6_clicked\0on_pushButton_5_clicked\0"
    "on_pushButton_9_clicked\0"
    "on__Btn_Start_request_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x08 /* Private */,
       3,    0,  105,    2, 0x08 /* Private */,
       4,    1,  106,    2, 0x08 /* Private */,
       6,    1,  109,    2, 0x08 /* Private */,
       9,    0,  112,    2, 0x08 /* Private */,
      10,    1,  113,    2, 0x08 /* Private */,
      12,    1,  116,    2, 0x08 /* Private */,
      13,    0,  119,    2, 0x08 /* Private */,
      14,    0,  120,    2, 0x08 /* Private */,
      15,    1,  121,    2, 0x08 /* Private */,
      17,    1,  124,    2, 0x08 /* Private */,
      18,    0,  127,    2, 0x08 /* Private */,
      19,    0,  128,    2, 0x08 /* Private */,
      20,    0,  129,    2, 0x08 /* Private */,
      21,    0,  130,    2, 0x08 /* Private */,
      22,    0,  131,    2, 0x08 /* Private */,
      23,    0,  132,    2, 0x08 /* Private */,
      24,    0,  133,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Name_update_btn_clicked(); break;
        case 1: _t->on_on_ConnectButton_clicked(); break;
        case 2: _t->serialReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->receivedFrame((*reinterpret_cast< protocol_Frame_data*(*)>(_a[1]))); break;
        case 4: _t->Timer_Request(); break;
        case 5: _t->on_EN_CTRL_DAC_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_EN_CTRL_pwm_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_Btn_CTRL_M_clicked(); break;
        case 8: _t->on_BTN_SET_Mode_clicked(); break;
        case 9: _t->on_CTRL_DAC_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_CTRL_pwm_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on__Btn_SetSettings_clicked(); break;
        case 12: _t->on__Btn_flash_Write_clicked(); break;
        case 13: _t->on__Btn_Read_settings_clicked(); break;
        case 14: _t->on_pushButton_6_clicked(); break;
        case 15: _t->on_pushButton_5_clicked(); break;
        case 16: _t->on_pushButton_9_clicked(); break;
        case 17: _t->on__Btn_Start_request_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
