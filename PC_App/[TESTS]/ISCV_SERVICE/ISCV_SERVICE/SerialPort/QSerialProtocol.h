#ifndef QSERIALPROTOCOL_H
#define QSERIALPROTOCOL_H

#include <QObject>
#include <stdint.h>
#include <stdbool.h>
#include <QString>
#include <QByteArray>


typedef enum{
    SP_CMD_RESERVED_0,
    SP_CMD_SET_MODE,
    SP_CMD_MSG_ERROR,
    SP_CMD_MSG_SERVICE,
    SP_CMD_SET_SETTINGS,
    SP_CMD_ECHO_PAYLOAD,
    SP_CMD_GET_INPUTS,
    SP_CMD_SEND_INPUTS,
    SP_CMD_SET_START_MODE,
    SP_CMD_SET_START_PWM,
    SP_CMD_SET_RESISTORS,
    SP_CMD_GET_RESISTORS,
    SP_CMD_SEND_RESISTORS,
    SP_CMD_ADC_CALIBRATION,
    SP_CMD_ADC_COEF_SAVE,
    SP_CMD_SET_DAC_OUTPUT,
    SPROTOCOL_COMMANDS_COUNT
}cmd_protocol;

#pragma pack(push,1)
typedef struct
{
    uint8_t length;
    uint8_t command;
    uint8_t* data;
}protocol_Frame_data;
#pragma pack(pop)






class QSerialProtocol : public QObject
{
    Q_OBJECT
public:
    explicit QSerialProtocol();
    explicit QSerialProtocol(uint16_t id);

     void setID(uint16_t id);
     void sendGoBoot();
     void addTransferData(QByteArray data);
     void transmitData(protocol_Frame_data* _frame);

private:
     uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
     uint8_t checkCRC16(uint8_t *data,uint8_t len);
     uint8_t checkCRC16(QByteArray data,uint8_t len);
     void parseRecieverDataBuffer(QByteArray data);

public slots:
        void serialProtReceived(QByteArray data);


signals:
    void serialTransmit(QByteArray* data);
    void receivedMsgFrame(protocol_Frame_data* _frame);

private:
    bool startSystem;
    QByteArray inputData;
    uint16_t ID_DEV;
    uint16_t lengthExpectid;
    QList<protocol_Frame_data> getMsgList;


};

#endif // QSERIALPROTOCOL_H
