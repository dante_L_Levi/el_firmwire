#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include "QDebug"


#define TIME_Request         100
SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
int16_t index_request=0;
Test_DAC *mainData;

typedef enum
{
    Update_NameBoard=1,
    Update_Firmwire=2,
    Update_DAC_Get=3,
    Update_Dig_In=4

}Update_step_UI;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Load_Styles();
    mainData=new Test_DAC();
    Init_StartBox_Element();

    mainData->Start_Init_Model();
    connect(mainData->serial_Data, SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this, SLOT(Get_Read_Data_Serial(QByteArray)));


}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
        const auto infos = QSerialPortInfo::availablePorts();
                for (const QSerialPortInfo &info : infos)
                {
                    ui->NameSerialPort_Box->addItem(info.portName());
                }


                 ui->ValueDAC_1->setEnabled(false);
                 ui->ValueDAC_2->setEnabled(false);
                 ui->Btn_BOOT->setEnabled(false);
                 ui->BTN_RESET->setEnabled(false);
                 ui->pushButton->setEnabled(false);


}

void MainWindow:: Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);
        RequestReadWrite->setInterval(TIME_Request);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }

}


void MainWindow:: Load_Styles(void)
{
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();

    palette.setColor(ui->label->foregroundRole(), Qt::white);
     ui->label->setPalette(palette);

     palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
      ui->label_2->setPalette(palette2);

      ui->groupBox_2->setForegroundRole(QPalette::Light);
}


void MainWindow:: Get_Read_Data_Serial(QByteArray data)
{
    qDebug()<<data;
    uint8_t status_update=mainData->Read_Model_Status_PayLoad(data);
        switch(status_update)
        {
               case Update_NameBoard:
                {
                    mainData->Update_UI_Name(ui->lbl_Name);
                    break;
                }

                case Update_Firmwire:
                 {
                     mainData->Update_UI_FIrmwire(ui->lbl_Firmw);
                     break;
                 }

                case Update_DAC_Get:
                 {

            //reserve

                     break;
                 }

                case Update_Dig_In:
                 {
                    //reserve

                     break;
                 }
        }

}




void MainWindow::UPDATE_UI_TimRequest(void)
{
    mainData->Update_UI_ID(ui->label_ID);
    if(index_request>2)
    {
       index_request=0;
       return;
    }
    switch(index_request)
    {
            case 1://NameBoard
        {
             mainData->RS_Request_GetNameBoard();
            break;
        }
            case 2://Firmwire
        {
            mainData->RS_Request_GetFirmwire();
            break;
        }


    }

    index_request+=1;


}






void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
             count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connected");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);

        ui->ValueDAC_1->setEnabled(true);
        ui->ValueDAC_2->setEnabled(true);
        ui->Btn_BOOT->setEnabled(true);
        ui->BTN_RESET->setEnabled(true);
        ui->pushButton->setEnabled(true);


                    bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                             (ui->SpeedSerialPort_Box->currentText()).toLong());
                    RequestReadWrite=new QTimer();
                    if(statusConfig)
                    {
                        QMessageBox::information(this,"Connection!!","Connection OK!");
                        qDebug()<<"Port Open!!";
                    }
    }
    else
    {
        if(mainData->serial_Data->isOpen())
        {
            mainData->serial_Data->close();
            ui->on_ConnectButton->setText("Disconnected");
            QMessageBox::information(this,"ERROR","Close Port");
        }

        QMessageBox::information(this,"ERROR","None Open!!");

    }


}


void MainWindow::on_pushButton_clicked()
{
    count_start_Transmit++;
        if(count_start_Transmit%2==0)
        {

            Activate_UI(true);
            ui->ValueDAC_1->setEnabled(true);
             ui->ValueDAC_2->setEnabled(true);


            mainData->RS_485_OK_Request();

           RequestReadWrite->start();


        }
        else
        {
            Activate_UI(false);
            ui->ValueDAC_1->setEnabled(false);
             ui->ValueDAC_2->setEnabled(false);

            RequestReadWrite->stop();
        }
}


void MainWindow::on_ValueDAC_1_valueChanged(int value)
{
    mainData->RS_485_SetDACx(0,(int16_t)value);
}


void MainWindow::on_ValueDAC_2_valueChanged(int value)
{
    mainData->RS_485_SetDACx(1,(int16_t)value);
}


void MainWindow::on_Btn_BOOT_clicked()
{
    Activate_UI(false);
    count_start_Transmit=1;
    mainData->serial_Data->GotoBootLoader();
}


void MainWindow::on_BTN_RESET_clicked()
{

}

