#ifndef TEST_DAC_H
#define TEST_DAC_H


#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"
#include "SerialPort/serialprotocol_data.h"

#define MODEL_SIZE                  8
#define INDEX_CMD                   2
#define OFFSET_DataPayload          3








#pragma pack(push, 1)
typedef union
{
 struct
 {
     uint8_t In1:1;
     uint8_t In2:1;
     uint8_t In3:1;
     uint8_t In4:1;
     uint8_t In5:1;
     uint8_t In6:1;
     uint8_t In7:1;
     uint8_t In8:1;


 }fields;
 uint8_t AllData;

}Digital_Input;


typedef struct
{
    int16_t channelA;
    int16_t channelB;

}_LTC2602_DEF;

#pragma pack(pop)




#pragma pack(push, 1)

typedef struct
{
    char modelName[8];
    char firmware[8];
   _LTC2602_DEF _dac_out;
   _LTC2602_DEF _dac_test_in;
   Digital_Input  _input_data;
}Model_data_def;
#pragma pack(pop)

typedef enum
{
    rs485_default_handler=0,
    getBoardName=1,
    getFirmware=2,
    RS485_Connect=3,
    RS485_Disconnect=4,
    Set_DAC_Outputs=5,
    Get_DAC_Outputs=6,
    Get_Dig_In=7

}Parse_data_Type;


class Test_DAC
{
public:
    Test_DAC();

    bool isRequest;
       SerialProtocol_data *serial_Data;
       /******************Inicilize Model*****************************/
       void Start_Init_Model(void);
       /**********************Get ID***************************/
       uint16_t Get_Model_ID(void);
       /************************Settings SerialPort**************************/
       bool  Config_Settings(QString Name,long Baud);

       void Request_Board_Number(uint8_t index_request);
       /**************************Model Control Parse********************/
       void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);



       /**********************Function Request Data Enable*******************/
       void Request_TO_MCU(bool status);
       void Test_GenerateData(void);

       /***********************Convert Read Buffer in Struct ****************************/
       bool  Read_dataModel(void);


       /*************************Request Start Connection****************/
        void RS_485_OK_Request(void);
        /*************************Request Start Connection****************/
        void RS_485_Close_Request(void);
        /*************************Request OUT DAC****************/
        void RS_485_SetDACx(uint8_t ch,int16_t val);

        /**********************Function Request Firmwire*******************/
        void RS_Request_GetFirmwire(void);
        /**********************Function Request NameBoard*******************/
        void RS_Request_GetNameBoard(void);
        /**********************Function Request NameBoard*******************/
        void RS_Request_GetDigIn(void);
        /**********************Function Request NameBoard*******************/
        void RS_Request_GetDAC_OUT(void);


        uint8_t Read_Model_Status_PayLoad(QByteArray data);



        void Update_UI_ID(QLabel *_id);
        void Update_UI_FIrmwire(QLabel *_Firmwire);
        void Update_UI_Name(QLabel *_Name);
        void Update_UI(QLabel *_id,QLabel *_Name,QLabel *_Firmwire,QLabel *_DigIn);



private:
         void Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt);
         /************************Request Set Data DAC********************/
            void RS_Request_Set_DAC_Value(int16_t out_dac[]);






};

#endif // TEST_DAC_H
