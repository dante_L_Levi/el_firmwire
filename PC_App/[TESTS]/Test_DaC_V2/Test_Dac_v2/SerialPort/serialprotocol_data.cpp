#include "serialprotocol_data.h"



const uint32_t  GOtoBOOT=0xF2784C28;



SerialProtocolDef _mainDataProtocol;
#define SizeBUFFER      sizeof(_mainDataProtocol)
uint8_t TX_BUFF[SizeBUFFER];
uint8_t RX_BUFF[SizeBUFFER];

QByteArray ReadBuffer;

uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
bool checkCRC16(uint8_t *data,uint8_t len);

/************************Function Set ID Device********************/
 void SerialProtocol_data:: Set_ID(uint16_t id)
 {
     if(id!=0x00)
     {
         _mainDataProtocol.ID=id;
     }

 }


 /************************Function Get Speed********************/
 QStringList SerialProtocol_data::Get_BaudRate()
 {
     QStringList speed=
     {
         "1200",
         "2400",
         "9600",
        "19200",
         "38400",
         "115200",
         "921600"
     };

     return  speed;
 }



 /********************Function Init SerialPort******************/
 bool SerialProtocol_data::  Init_OpenPort(QString _Name, long int _baud)
 {
     IsRxAvalible=false;
     this->setPortName(_Name);
     this->setBaudRate(_baud);
     this->setDataBits(QSerialPort::Data8);
     this->setParity(QSerialPort::NoParity);
     this->setStopBits(QSerialPort::OneStop);
     this->setFlowControl(QSerialPort::NoFlowControl);
      //connect(this,SIGNAL(readyRead()),this,SLOT(ReadSerialPort_data()));
     connect(this,&QSerialPort::readyRead,this,&SerialProtocol_data::ReadSerialPort_data);
     bool status=this->open(QIODevice::ReadWrite);
     ReadBuffer.resize(SizeBUFFER);

    return status;
 }

 /*************************Function result read*********************/
 QByteArray SerialProtocol_data:: Get_Result_Recieve_Data(void)
 {
     //QByteArray ReadBuffer;
    // ReadBuffer.resize(SizeBUFFER);
    // ReadBuffer=this->readAll();
     qDebug()<<"Input Data:";
     qDebug()<<ReadBuffer;
     return ReadBuffer;
 }

 /*************************Function result read*********************/
 QByteArray SerialProtocol_data:: Get_CHECK_Result_Recieve_Data(void)
 {
     //ReadBuffer.clear();
     //ReadBuffer.resize(SizeBUFFER);
    // ReadBuffer=this->readAll();
     qDebug()<<"Input Data:";
     qDebug()<<ReadBuffer;
     uint8_t data[SizeBUFFER]={0x00};
     for(int i=0;i<(int)SizeBUFFER;i++)
     {
         data[i]=ReadBuffer[i];
     }
     if(checkCRC16(data,SizeBUFFER))
     {
         qDebug()<<"CRC OK!!";
         return ReadBuffer;
     }
     else
     {
         qDebug()<<"CRC ERROR!!";
         return 0;
     }
 }

 /*************************Function result read*********************/
 QByteArray SerialProtocol_data:: Get_CHECK_Result_Recieve_Data(QByteArray data_Array)
 {
     //ReadBuffer.clear();
     //ReadBuffer.resize(SizeBUFFER);
    // ReadBuffer=this->readAll();
     qDebug()<<"Input Data:";
     qDebug()<<data_Array;
     uint8_t data[SizeBUFFER]={0x00};
     for(int i=0;i<(int)SizeBUFFER;i++)
     {
         data[i]=data_Array[i];
     }
     if(checkCRC16(data,SizeBUFFER))
     {
         qDebug()<<"CRC OK!!";
         return data_Array;
     }
     else
     {
         qDebug()<<"CRC ERROR!!";
         return 0;
     }
 }

 void SerialProtocol_data::Set_IsRxAvalible(bool)
 {
     IsRxAvalible=true;
 }

 void SerialProtocol_data::GotoBootLoader()
 {
     uint8_t data[8]={0};
     memcpy(data,&GOtoBOOT,sizeof(data));
     Transmit_Packet(BOOT_CMD,data,sizeof(data));
 }


 /**************Read data Serial Port *********************/
 void SerialProtocol_data :: ReadSerialPort_data()
 {
     ReadBuffer.clear();
     ReadBuffer.resize(SizeBUFFER);
     //if(this->waitForReadyRead(10))
     //        ReadBuffer=this->readAll();

        if(this->bytesAvailable()==SizeBUFFER)
        {
            ReadBuffer=this->readAll();
            IsRxAvalible=true;
            emit Get_data_Recieved_Serial(ReadBuffer);
        }





 }



 /************************Function Transmit Packet********************/
void SerialProtocol_data:: Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length)
{
    _mainDataProtocol.cmd=(uint8_t)cmd;
    if(length<=PayloadSize)
    {
        memcpy((void*)(_mainDataProtocol.payload),(void *)data,sizeof(data));
        QByteArray databuffer;
        databuffer.resize(SizeBUFFER);
        memcpy((void*)(TX_BUFF),&_mainDataProtocol,sizeof(SizeBUFFER));
        uint16_t crc=Calculate_CRC16(TX_BUFF,SizeBUFFER-2);
        _mainDataProtocol.CRC=crc;
        TX_BUFF[SizeBUFFER-1]=(uint8_t)(_mainDataProtocol.CRC>>8);
        TX_BUFF[SizeBUFFER-2]=(uint8_t)_mainDataProtocol.CRC;
        //memcpy((void*)(TX_BUFF),&_mainDataProtocol,sizeof(SizeBUFFER));
        for(int i=0;i<(int)SizeBUFFER;i++)
        {
            databuffer[i]=TX_BUFF[i];
        }
        qDebug()<<"Request:"<<databuffer;
        this->write((databuffer));
    }



}

/******************Transmit Calculate CRC************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
    uint16_t crc = 0xFFFF;
       uint8_t i;

       while (len--)
       {
           crc ^= *data++ << 8;

           for (i = 0; i < 8; i++)
               crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
       }
       return crc;

}

/******************Recieve Calculate CRC************************/
 bool checkCRC16(uint8_t *data,uint8_t len)
 {
     uint16_t crc = 0xFFFF;
     uint8_t i;
     uint16_t des_buf=(data[len-1]<<8)|data[len-2];
     uint8_t len_dst=len-2;
     while (len_dst--)
     {
         crc ^= *data++ << 8;

         for (i = 0; i < 8; i++)
             crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
     }
     if(crc==des_buf)
     {
         return  true;
     }
     else
     {
        return  false;
     }

 }





