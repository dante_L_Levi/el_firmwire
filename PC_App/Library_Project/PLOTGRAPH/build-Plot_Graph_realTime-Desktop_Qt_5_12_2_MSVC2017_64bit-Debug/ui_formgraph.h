/********************************************************************************
** Form generated from reading UI file 'formgraph.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMGRAPH_H
#define UI_FORMGRAPH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_formgraph
{
public:
    QLabel *label;

    void setupUi(QWidget *formgraph)
    {
        if (formgraph->objectName().isEmpty())
            formgraph->setObjectName(QString::fromUtf8("formgraph"));
        formgraph->resize(470, 350);
        label = new QLabel(formgraph);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(150, 100, 201, 131));
        QFont font;
        font.setPointSize(14);
        label->setFont(font);

        retranslateUi(formgraph);

        QMetaObject::connectSlotsByName(formgraph);
    } // setupUi

    void retranslateUi(QWidget *formgraph)
    {
        formgraph->setWindowTitle(QApplication::translate("formgraph", "Form", nullptr));
        label->setText(QApplication::translate("formgraph", "TEST_WINDOW", nullptr));
    } // retranslateUi

};

namespace Ui {
    class formgraph: public Ui_formgraph {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMGRAPH_H
