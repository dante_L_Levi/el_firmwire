/****************************************************************************
** Meta object code from reading C++ file 'formgraph.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Plot_Graph_realTime/formgraph.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'formgraph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_formgraph_t {
    QByteArrayData data[11];
    char stringdata0[147];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_formgraph_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_formgraph_t qt_meta_stringdata_formgraph = {
    {
QT_MOC_LITERAL(0, 0, 9), // "formgraph"
QT_MOC_LITERAL(1, 10, 15), // "pushButtonReset"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 15), // "pushButtonPause"
QT_MOC_LITERAL(4, 43, 15), // "pushButtonColor"
QT_MOC_LITERAL(5, 59, 13), // "pushButtonAdd"
QT_MOC_LITERAL(6, 73, 14), // "pushButtonAxis"
QT_MOC_LITERAL(7, 88, 22), // "comboBoxSize_activated"
QT_MOC_LITERAL(8, 111, 5), // "args1"
QT_MOC_LITERAL(9, 117, 23), // "comboBoxGraph_activated"
QT_MOC_LITERAL(10, 141, 5) // "index"

    },
    "formgraph\0pushButtonReset\0\0pushButtonPause\0"
    "pushButtonColor\0pushButtonAdd\0"
    "pushButtonAxis\0comboBoxSize_activated\0"
    "args1\0comboBoxGraph_activated\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_formgraph[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    1,   54,    2, 0x08 /* Private */,
       9,    1,   57,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::Int,   10,

       0        // eod
};

void formgraph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<formgraph *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pushButtonReset(); break;
        case 1: _t->pushButtonPause(); break;
        case 2: _t->pushButtonColor(); break;
        case 3: _t->pushButtonAdd(); break;
        case 4: _t->pushButtonAxis(); break;
        case 5: _t->comboBoxSize_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->comboBoxGraph_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject formgraph::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_formgraph.data,
    qt_meta_data_formgraph,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *formgraph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *formgraph::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_formgraph.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int formgraph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
