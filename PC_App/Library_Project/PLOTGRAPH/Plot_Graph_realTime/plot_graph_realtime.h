#ifndef PLOT_GRAPH_REALTIME_H
#define PLOT_GRAPH_REALTIME_H

#include "Plot_Graph_realTime_global.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "qcustomplot.h"
#include "formgraph.h"

typedef enum{
    int8_type,
    int16_type,
    int32_type,
    uint8_type,
    uint16_type,
    uint32_type,
    float_type,
    double_type
}TypeOfType;

typedef struct{
    bool plot;
    void* value;
    TypeOfType valueType;
    QCPAxis::AxisType axisType;
    QString name;
    QCPGraph *graph;
    QColor color;
} TypeGraphData;

class PLOT_GRAPH_REALTIME_EXPORT Plot_Graph_realTime
{
public:
    Plot_Graph_realTime();

    void Add_DataGraphOnBoard(TypeGraphData *data);
    void Update_Graphics_Plot(float tick);

    void Graph_show(void);
    void Graph_Reset(void);

private:
    formgraph *form;

};

#endif // PLOT_GRAPH_REALTIME_H
