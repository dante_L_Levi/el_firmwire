#include "plot_graph_realtime.h"
#include "formgraph.h"
#include "QWidget"

Plot_Graph_realTime::Plot_Graph_realTime()
{
    form=new formgraph();

}

void Plot_Graph_realTime::Add_DataGraphOnBoard(TypeGraphData *data)
{
    if(form)
        form->addGraph(data);
}

void Plot_Graph_realTime::Update_Graphics_Plot(float tick)
{
    if(form)
        form->updateGraph(tick);
}



void Plot_Graph_realTime::Graph_show()
{
    if(form)
        form->show();
}

void Plot_Graph_realTime::Graph_Reset()
{
    if(form)
        form->resetGraph();
}
