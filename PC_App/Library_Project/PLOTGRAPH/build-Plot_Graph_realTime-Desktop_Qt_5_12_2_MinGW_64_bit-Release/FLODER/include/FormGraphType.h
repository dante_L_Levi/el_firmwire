#ifndef FORMGRAPHTYPE_H
#define FORMGRAPHTYPE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "qcustomplot.h"

typedef enum{
    int8_type,
    int16_type,
    int32_type,
    uint8_type,
    uint16_type,
    uint32_type,
    float_type,
    double_type
}TypeOfType;

typedef struct{
    bool plot;
    void* value;
    TypeOfType valueType;
    QCPAxis::AxisType axisType;
    QString name;
    QCPGraph *graph;
    QColor color;
} TypeGraphData;


#endif // FORMGRAPHTYPE_H
