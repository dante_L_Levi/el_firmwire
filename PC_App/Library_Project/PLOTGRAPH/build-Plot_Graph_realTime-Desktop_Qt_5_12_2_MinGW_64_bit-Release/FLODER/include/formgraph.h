#ifndef FORMGRAPH_H
#define FORMGRAPH_H

#include <QWidget>
#include <QTimer>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "qcustomplot.h"
#include "FormGraphType.h"



namespace Ui {
class formgraph;
}

class formgraph : public QWidget
{
    Q_OBJECT

public:
    explicit formgraph(QWidget *parent = nullptr);
    ~formgraph();

    void addGraphData(int nuberGraph, double value);
    int addGraph(QString name, QCPAxis::AxisType type);
    int addGraph(TypeGraphData* graphData);
    void deleteGraph(int numberGraph);
    void showGraph(int index);
    void hideGraph(int index);
    void endGraphData();
    void resetGraph();
    void updateGraph();
    void updateGraph(double time);
    void setIncrement(double inc);
    void colorGenerator();
    void resizeEvent(QResizeEvent *event);


private slots:

    void pushButtonReset(void);
    void pushButtonPause(void);
    void pushButtonColor(void);
    void pushButtonAdd(void);
    void pushButtonAxis(void);


    void  comboBoxSize_activated(const QString &args1);
    void  comboBoxGraph_activated(int index);



private:
    Ui::formgraph *uiC;
    QTimer dataTimer;
    bool leftUpdate;
    bool rightUpdate;
    TypeGraphData numbersGraph[100];
    int count;
    double dataTime;
    float data1;
    float data2;
    float data3;
    double sizeGraph;
    double increment;
    bool pause;
    void updateComboBox();
};

#endif // FORMGRAPH_H
