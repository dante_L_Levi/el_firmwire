#ifndef PLOT_GRAPH_REALTIME_H
#define PLOT_GRAPH_REALTIME_H

#include "Plot_Graph_realTime_global.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "qcustomplot.h"
#include "formgraph.h"



class PLOT_GRAPH_REALTIME_EXPORT Plot_Graph_realTime
{
public:
    Plot_Graph_realTime();

    void Add_DataGraphOnBoard(TypeGraphData *data);
    void Update_Graphics_Plot(float tick);

    void Graph_show(void);
    void Graph_Reset(void);

private:
    formgraph *form;

};

#endif // PLOT_GRAPH_REALTIME_H
