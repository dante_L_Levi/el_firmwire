#include "OPENGL_Widget.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QMouseEvent>


OPENGL_Widget::OPENGL_Widget(QWidget *parent) : QOpenGLWidget(parent)

{
    //setGeometry(400,200,400,420);

}

void OPENGL_Widget::initializeGL()
{
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

void OPENGL_Widget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();//загружаем единичную матрицу
    //glOrtho(-1,1,-1,1,1,2);//создание оргтоганальная проекция(размеры камеры и расстояния от камеры до объекта )
    glFrustum(-1,1,-1,1,1,3);
}

void OPENGL_Widget::paintGL()
{

    int side = qMin(this->width(), this->height());
    glViewport((this->width() - side) / 2, (this->height() - side) / 2, side, side);

    glClearColor(1,1,1,0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(0,0,-2);
    glRotatef(xRot,1,0,0);
    glRotatef(yRot,0,1,0);
    glRotatef(zRot,0,0,1);
    drawCube(0.5);
}

void OPENGL_Widget::rotateByQuat(QQuaternion quat)
{
    q_rotare=quat;
    update();
}

void OPENGL_Widget::rotateByEler(float w, float x, float y, float z)
{
    xRot=x;
    yRot=y;
    zRot=z;
    wRot=w;
    update();
}

void OPENGL_Widget::drawCube(float a)
{
    float ver_cub[]=
    {
        -a,-a,a,            a,-a,a,        a,a,a,          -a,a,a,
         a,-a,-a,           -a,-a,-a,       -a,a,-a,        a,a,-a,
        -a,-a,-a,           -a,-a,a,        -a,a,a,         -a,a,-a,
         a,-a, a,           a,-a,-a,        a,a,-a,         a,a,a,
        -a,-a, a,           a,-a,a,         a,-a,-a,        -a,-a,-a,
        -a, a, a,           a,a,a,          a,a,-a,         -a,a,-a
    };

    float collor_arr[]={
        1,0,0,             1,0,0,           1,0,0,          1,0,0,
        0,0,1,            0,0,1,            0,0,1,          0,0,1,
        1,1,0,            1,1,0.5,            1,1,0,          1,1,0,
         0,1,1,           0,1,1,            0,1,1,          0,1,1,
        1,0, 1,           1,0, 0.5,           1,0, 1,         1,0, 1,
        1, 0.5, 0.5,      1, 0.5, 0.5,      1, 0.5, 0.5,    1, 0.5, 0.5,
    };

    glVertexPointer(3,GL_FLOAT,0,&ver_cub);
    glEnableClientState(GL_VERTEX_ARRAY);

    glColorPointer(3,GL_FLOAT,0,&collor_arr);
    glEnableClientState(GL_COLOR_ARRAY);

    glDrawArrays(GL_QUADS,0,24);


     glDisableClientState(GL_COLOR_ARRAY);
     glDisableClientState(GL_VERTEX_ARRAY);
}
