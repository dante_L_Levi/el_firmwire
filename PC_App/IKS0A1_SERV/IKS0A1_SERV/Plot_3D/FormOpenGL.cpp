#include "FormOpenGL.h"
#include "ui_FormOpenGL.h"

bool status_test=false;


FormOpenGL::FormOpenGL(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormOpenGL)
{
    ui->setupUi(this);
    _3DPlot=new OPENGL_Widget();
    //ui->MaiLayout->addWidget(_3DPlot);
    _timer_test=new QTimer();
    _timer_updateUI=new QTimer();
    _timer_updateUI->setInterval(1);
    _timer_test->setInterval(25);
    connect(_timer_test,SIGNAL(timeout()),this,SLOT(Tick_test_Timer()));
    connect(_timer_updateUI,SIGNAL(timeout()),this,SLOT(Update_UI()));
    _timer_updateUI->start();

}

FormOpenGL::~FormOpenGL()
{
    delete ui;
}

void FormOpenGL::rotateQuaternion(QQuaternion qRotate)
{

}

void FormOpenGL::rotateQuaternion(float scalar, float xpos, float ypos, float zpos)
{
    w=scalar;
    x=xpos;
    y=ypos;
    z=zpos;
   ui->widget->rotateByEler(w,x,y,z);
}

void FormOpenGL::on_Btn_test_clicked()
{
    status_test=!status_test;

    if(status_test)
    _timer_test->start();
    else
        _timer_test->stop();

}


void FormOpenGL::on_Btn_Reset_clicked()
{
    ui->widget->rotateByEler(0,0,0,0);
}

void FormOpenGL::Tick_test_Timer()
{
    x+=0.25;
    y+=0.35;
    z+=0.45;
    ui->widget->rotateByEler(w,x,y,z);
}

void FormOpenGL::Update_UI()
{
    QString data=QString(
                "Angels W: \t%1\n"
                "Angels Xpos:\t%2\n"
                "Angels Ypos:\t%3\n"
                "Angels Zpos:\t%4\n"


                ).arg(w)
                 .arg(x)
                 .arg(y)
                 .arg(z)


            ;

    ui->Data_Log->setText(data);
}


