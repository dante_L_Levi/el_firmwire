#ifndef OPENGL_WIDGET_H
#define OPENGL_WIDGET_H


#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QQuaternion>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class OPENGL_Widget :public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit OPENGL_Widget(QWidget *parent = nullptr);
    void initializeGL() Q_DECL_OVERRIDE;//reimplementando las funciones virtuales
    void resizeGL(int w, int h)Q_DECL_OVERRIDE;
    void paintGL()Q_DECL_OVERRIDE;

    void rotateByQuat(QQuaternion quat);
    void rotateByEler(float w,float x,float y,float z);

private:
     void drawCube(float a);


private:
     QQuaternion q_rotare;
    float xRot,yRot,zRot,wRot;


signals:

};

#endif // OPENGL_WIDGET_H
