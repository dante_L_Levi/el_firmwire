#ifndef FORMOPENGL_H
#define FORMOPENGL_H

#include <QWidget>
#include <Plot_3D/OPENGL_Widget.h>
#include <QTimer>


namespace Ui {
class FormOpenGL;
}

class FormOpenGL : public QWidget
{
    Q_OBJECT

public:
    explicit FormOpenGL(QWidget *parent = nullptr);
    ~FormOpenGL();

public slots:
    void rotateQuaternion(QQuaternion qRotate);
    void rotateQuaternion(float scalar, float xpos, float ypos, float zpos);




private slots:
    void on_Btn_test_clicked();

    void on_Btn_Reset_clicked();
    void Tick_test_Timer(void);
    void Update_UI(void);

private:
    Ui::FormOpenGL *ui;
    OPENGL_Widget *_3DPlot;
    QTimer      *_timer_test;
    QTimer      *_timer_updateUI;
    float w=0,x=0,y=0,z=0;
};

#endif // FORMOPENGL_H
