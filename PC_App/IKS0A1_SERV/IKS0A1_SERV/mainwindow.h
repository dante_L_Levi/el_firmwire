#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "style_classes/window_styles.h"
#include "stdint.h"
#include "QTimer"
#include "style_classes/window_styles.h"
#include "SerialPort/QSerialProtocol.h"
#include "SerialPort/SerialPort.h"
#include "qcustomplot.h"
#include "Logic/iks01a1_logic.h"
#include <QFile>
#include <QTextStream>
#include "formgraph.h"
#include "CSV_Manager/CSV_Manager.h"
#include "Plot_3D/FormOpenGL.h"
#include "Plot_3D/OPENGL_Widget.h"

#include "Madgwick.h"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define ID_DEVICE       0xF601

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Name_update_btn_clicked();

    void on_on_ConnectButton_clicked();

    void on_Btn_PlotGraph_clicked();

    void on_Btn_3D_clicked();

    void on_btn_save_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();


    void serialReceived(QByteArray data);
    void receivedMsg(protocol_Frame_data* data);
    void Request_Board_Tim();
    void Immulator_Get_recieve_Data();
    void slotTimerAlarm(void);
    void slotTimerAlarmCSV(void);
    void Slot_Timer_TimeOut(void);

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *timer;
    QTimer *TimerAlarm;
    QTimer *TimerAlarmCSV;
    QTimer *timeout;
    QTime *timeWork;
    IKS01A1_Logic *Logic;

    SerialPort *serial;
    QSerialProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;
    formgraph *graphOutput;
    FormOpenGL  *_3DPlot;
    OPENGL_Widget *_3D_test;
    bool status_save_csv=false;
    CSV_Manager *_csv_hundler;
    double count_packet_CSV=0;
    Madgwick *data_IMU;
    bool status_3D_Window=false;
    float yaw=0,pitch=0,roll=0;


    void Update_ComBoBox_Config(void);
    void Load_dataGraph(void);
    void Load_Settings_CSV(void);
    void CSV_Config(void);
    void CSV_Load_NameTitle(void);


};
#endif // MAINWINDOW_H
