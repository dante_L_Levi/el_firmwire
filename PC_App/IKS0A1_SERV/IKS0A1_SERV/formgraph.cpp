#include "formgraph.h"
#include "ui_formgraph.h"

formgraph::formgraph(QWidget *parent) :
    QWidget(parent),
    uiC(new Ui::formgraph)
{
    uiC->setupUi(this);
    QCustomPlot *customPlot=uiC->CUSTOMEPLOT;
    customPlot->yAxis2->setVisible(true);
    customPlot->axisRect()->axis(QCPAxis::atLeft, 0)->setPadding(30); // add some padding to have space for tags
    customPlot->axisRect()->axis(QCPAxis::atRight, 0)->setPadding(30); // add some padding to have space for tags

    customPlot->legend->setVisible(true);
        // now we move the legend from the inset layout of the axis rect into the main grid layout.
        // We create a sub layout so we can generate a small gap between the plot layout cell border
        // and the legend border:
   QCPLayoutGrid *subLayout = new QCPLayoutGrid;
   customPlot->plotLayout()->addElement(1, 0, subLayout);
   subLayout->setMargins(QMargins(5, 0, 5, 5));
   subLayout->addElement(0, 0, customPlot->legend);
        // change the fill order of the legend, so it's filled left to right in columns:
   customPlot->legend->setFillOrder(QCPLegend::foColumnsFirst);
        // set legend's row stretch factor very small so it ends up with minimum height:
   customPlot->plotLayout()->setRowStretchFactor(1, 0.001);


   for(int i=0;i<100;i++){
           numbersGraph[i].plot=false;
           numbersGraph[i].valueType=uint8_type;
           numbersGraph[i].axisType=QCPAxis::atLeft;
           numbersGraph[i].graph=nullptr;
           numbersGraph[i].value=nullptr;
           numbersGraph[i].name="";
           colorGenerator();
       }
       sizeGraph=100;
       dataTime=0;
       increment=1;
       pause=false;
}

formgraph::~formgraph()
{
    delete uiC;
}


void formgraph:: addGraphData(int nuberGraph, double value)
{
    if((nuberGraph>=uiC->CUSTOMEPLOT->graphCount())||(nuberGraph<0)){
        return;
    }
    QCustomPlot *customPlot=uiC->CUSTOMEPLOT;
    customPlot->graph(nuberGraph)->addData(dataTime, value);
    customPlot->graph(nuberGraph)->rescaleValueAxis(true, true);
}
int formgraph:: addGraph(QString name, QCPAxis::AxisType type)
{
    uiC->CUSTOMEPLOT->addGraph(uiC->CUSTOMEPLOT->xAxis,
                              uiC->CUSTOMEPLOT->axisRect()->axis(QCPAxis::atLeft, 0));
    uiC->CUSTOMEPLOT->graph(1)->setName("1");
    uiC->CUSTOMEPLOT->graph(1)->setPen(QPen(QColor(0, 180, 60)));
    uiC->CUSTOMEPLOT->replot();

    qDebug()<<name;
    qDebug()<<type;
    return 1;
}
int formgraph:: addGraph(TypeGraphData* graphData)
{
    int n=-1;
    for(int i=0; i<100; i++){
        if(numbersGraph[i].value==nullptr){
            n=i;
            break;
        }
    }
    if (n==-1)
        return -1;
    numbersGraph[n].name=graphData->name;
    numbersGraph[n].plot=graphData->plot;
    numbersGraph[n].value=graphData->value;
    numbersGraph[n].axisType=graphData->axisType;
    numbersGraph[n].valueType=graphData->valueType;

    if(graphData->plot)
        showGraph(n);
//    numbersGraph[n].graph=uiC->customPlot->addGraph(uiC->customPlot->xAxis,
//                            uiC->customPlot->axisRect()->axis(numbersGraph[n].axisType, 0));
//    numbersGraph[n].graph->setName(numbersGraph[n].name);
//    numbersGraph[n].graph->setPen(QPen(numbersGraph[n].color));

//    uiC->customPlot->replot();
    updateComboBox();
    return n;
}
void formgraph:: deleteGraph(int numberGraph)
{
    uiC->CUSTOMEPLOT->removeGraph(2);
    numbersGraph[2].plot=false;
    qDebug()<<numberGraph;
}
void formgraph:: showGraph(int index)
{
    numbersGraph[index].plot=true;
    numbersGraph[index].graph=uiC->CUSTOMEPLOT->addGraph(uiC->CUSTOMEPLOT->xAxis,
                            uiC->CUSTOMEPLOT->axisRect()->axis(numbersGraph[index].axisType, 0));
    numbersGraph[index].graph->setName(numbersGraph[index].name);
    QPen* pen= new QPen(numbersGraph[index].color);
    pen->setWidth(uiC->CUSTOMEPLOT->size().height()/500+1);
    numbersGraph[index].graph->setPen(*pen);
    numbersGraph[index].graph->setName(numbersGraph[index].name);
}
void formgraph:: hideGraph(int index)
{
    numbersGraph[index].plot=false;
    uiC->CUSTOMEPLOT->removeGraph(numbersGraph[index].graph);
}
void formgraph:: endGraphData()
{
    uiC->CUSTOMEPLOT->xAxis->rescale();
    uiC->CUSTOMEPLOT->xAxis->setRange(uiC->CUSTOMEPLOT->xAxis->range().upper, 100, Qt::AlignRight);
    leftUpdate=false;
    rightUpdate=false;
    uiC->CUSTOMEPLOT->replot();
}
void formgraph:: resetGraph()
{
    int N=uiC->CUSTOMEPLOT->graphCount();
    for(int i=0;i<N;i++){
        uiC->CUSTOMEPLOT->graph(i)->rescaleValueAxis(false, true);
    }
}
void formgraph:: updateGraph()
{
    dataTime+=increment;
    updateGraph(dataTime);
}
void formgraph:: updateGraph(double time)
{
    if(time>dataTime)
        dataTime=time;

    for(int i=0;i<100;i++){
        if((numbersGraph[i].plot)&&(numbersGraph[i].graph!=nullptr)){
            double value;
            switch (numbersGraph[i].valueType) {
            case int8_type:
                value=*(int8_t *)numbersGraph[i].value;
                break;
            case int16_type:
                value=*(int16_t*)numbersGraph[i].value;
                break;
            case int32_type:
                value=*(int32_t*)numbersGraph[i].value;
                break;
            case uint8_type:
                value=*(uint8_t*)numbersGraph[i].value;
                break;
            case uint16_type:
                value=*(uint16_t*)numbersGraph[i].value;
                break;
            case uint32_type:
                value=*(uint32_t*)numbersGraph[i].value;
                break;
            case float_type:
                value=*(float*)numbersGraph[i].value;
                break;
            case double_type:
                value=*(double*)numbersGraph[i].value;
            }
            //QCustomPlot *customPlot=uiC->CUSTOMEPLOT;
            numbersGraph[i].graph->addData(dataTime, value);
            numbersGraph[i].graph->rescaleValueAxis(true, true);
        }
    }

        uiC->CUSTOMEPLOT->xAxis->rescale();
        uiC->CUSTOMEPLOT->xAxis->setRange(uiC->CUSTOMEPLOT->xAxis->range().upper, sizeGraph, Qt::AlignRight);
        leftUpdate=false;
        rightUpdate=false;

    if(!pause){
        uiC->CUSTOMEPLOT->replot();
    }
}
void formgraph:: setIncrement(double inc)
{
    increment=inc;
}
void formgraph:: colorGenerator()
{
    for(int i=0;i<100;i++){
        int r=rand()&0xFFFF;
        int g=rand()&0xFFFF;
        int b=rand()&0xFFFF;
        int summ=r+g+b;

        while((summ<300)||(r>((g+b)*2))||(g>((r+b)*2))||(b>((g+r)*2))){
            r=rand()&0xFFFF;
            g=rand()&0xFFFF;
            b=rand()&0xFFFF;
            summ=r+g+b;
        }
        r=r*300/summ;
        g=g*300/summ;
        b=b*300/summ;

        numbersGraph[i].color=QColor(r,g,b);
    }
}
void formgraph:: resizeEvent(QResizeEvent *event)
{
    for (int index=0;index<100;index++)
        if(numbersGraph[index].plot==true)
        {
            QPen* pen= new QPen(numbersGraph[index].color);
            pen->setWidth(uiC->CUSTOMEPLOT->size().height()/500+1);
            numbersGraph[index].graph->setPen(*pen);
            numbersGraph[index].graph->setName(numbersGraph[index].name);
        }

    qDebug()<<event;
}

void formgraph::updateComboBox()
{
    uiC->comboBoxGraph_2->clear();
    for(int n=0;n<100;n++){
        if(numbersGraph[n].value==nullptr)
            break;
        uiC->comboBoxGraph_2->addItem(numbersGraph[n].name);
    }
    on_comboBoxGraph_2_activated(0);
}

void formgraph::on_pushButtonReset_2_clicked()
{
resetGraph();
}


void formgraph::on_pushButtonPause_2_clicked()
{
    pause=!pause;
}


void formgraph::on_pushButtonColor_2_clicked()
{
    colorGenerator();
        for (int index=0;index<100;index++)
            if(numbersGraph[index].plot==true)
            {
                QPen* pen= new QPen(numbersGraph[index].color);
                pen->setWidth(uiC->CUSTOMEPLOT->size().height()/500+1);
                numbersGraph[index].graph->setPen(*pen);
                numbersGraph[index].graph->setName(numbersGraph[index].name);
            }
}


void formgraph::on_pushButtonAdd_2_clicked()
{
    qDebug()<<"-------------------Graph PloT!!!!---------------";
    for(int i=0;i<30;i++)
    {
        qDebug()<<"Graph-Data:"<<numbersGraph[i].value;
    }

    int index=uiC->comboBoxGraph_2->currentIndex();
        if(index>=0){
            if(numbersGraph[index].plot){
                hideGraph(index);
            }
            else {
                showGraph(index);
            }
            on_comboBoxGraph_2_activated(index);
        }
}



void formgraph::on_comboBoxSize_2_activated(const QString &arg1)
{
    sizeGraph=arg1.toDouble();
}


void formgraph::on_comboBoxGraph_2_activated(int index)
{
    if(index>=0){
            if(numbersGraph[index].plot){
                uiC->pushButtonAdd_2->setText("Удалить");
            }
            else {
                uiC->pushButtonAdd_2->setText("Добавить");
            }
            if(numbersGraph[index].axisType==QCPAxis::atLeft){
                uiC->pushButtonAdd_2->setText("->");
            }
            else {
                uiC->pushButtonAdd_2->setText("<-");
            }
        }
        else {
            uiC->pushButtonAdd_2->setText("--");
            uiC->pushButtonAdd_2->setText("<- выберите");
        }
}


void formgraph::on_pushButtonAxis_2_clicked()
{
    int index=uiC->comboBoxGraph_2->currentIndex();
        if(index>=0){
            if(numbersGraph[index].axisType==QCPAxis::atLeft){
                numbersGraph[index].axisType=QCPAxis::atRight;
            }
            else {
                numbersGraph[index].axisType=QCPAxis::atLeft;
            }
            if(numbersGraph[index].plot)
                numbersGraph[index].graph->setValueAxis(uiC->CUSTOMEPLOT->axisRect()->axis(numbersGraph[index].axisType, 0));
            on_comboBoxGraph_2_activated(index);
        }
}

