#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"

#define PERIOD_Request_MS          20
bool count_ConnectButton=false;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("IKS0A1 SERVICE");
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
        QPalette palette=ui->label->palette();
        QPalette palette2=ui->label_2->palette();
        QPalette palette3=ui->Label_Board_Info->palette();
        palette.setColor(ui->label->foregroundRole(), Qt::white);
         ui->label->setPalette(palette);

         palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
          ui->label_2->setPalette(palette2);
          palette3.setColor(ui->Label_Board_Info->foregroundRole(), Qt::white);
           ui->Label_Board_Info->setPalette(palette3);

          ui->groupBox_2->setForegroundRole(QPalette::Light);

    timer=new QTimer();
    TimerAlarm=new QTimer();
    TimerAlarmCSV  =new QTimer();
    timeout=new QTimer();
    TimerAlarmCSV->setInterval(25);
    //timeout->set
    serial=new SerialPort();
    protocol=new QSerialProtocol();
    protocol->setID(ID_DEVICE);
    Logic=new IKS01A1_Logic();



    Update_ComBoBox_Config();


    connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this,SLOT(serialReceived(QByteArray)));
    connect(protocol,SIGNAL(receivedMsgFrame(protocol_Frame_data*)),
            this,SLOT(receivedMsg(protocol_Frame_data*)));
    connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
            serial,SLOT(transmitData(QByteArray*)));



    connect(TimerAlarm,SIGNAL(timeout()),this,SLOT(slotTimerAlarm()));
    connect(TimerAlarmCSV,SIGNAL(timeout()),this,SLOT(slotTimerAlarmCSV()));
    connect(timeout,SIGNAL(timeout()),this,SLOT(Slot_Timer_TimeOut()));
    graphOutput=new formgraph();
    graphOutput->setIncrement(0.025);

    TimerAlarm->setInterval(25);



    Load_dataGraph();
    _csv_hundler=new CSV_Manager();
    _csv_hundler->CSV_Init();
    CSV_Load_NameTitle();

    _3DPlot=new FormOpenGL();
    _3D_test=new OPENGL_Widget();
    data_IMU=new Madgwick();

}

void MainWindow::Load_dataGraph(void)
{
    timeWork=new QTime();
    TypeGraphData gData;
    gData.plot=false;
    gData.axisType=QCPAxis::atLeft;
    gData.valueType=float_type;

    //Load  Accel
    for(int i=0;i<COUNT_AXES;i++)
    {
         gData.name=Logic->NameAccel[i];


         gData.value=Logic->Get_Accel_INDEX(i);
         graphOutput->addGraph(&gData);

    }
    //Load  Gyro
    for(int i=0;i<COUNT_AXES;i++)
    {
         gData.name=Logic->NameGyro[i];

         gData.value=Logic->Get_Gyro_INDEX(i);
         graphOutput->addGraph(&gData);

    }
    //Load  Magn
    for(int i=0;i<COUNT_AXES;i++)
    {
         gData.name=Logic->NameMagn[i];

         gData.value=Logic->Get_Magn_INDEX(i);
         graphOutput->addGraph(&gData);

    }

    //Load Pressure
    gData.name=Logic->NamePressure;

    gData.value=Logic->Get_Pressure();
    graphOutput->addGraph(&gData);

    //Load Huminity
    gData.name=Logic->NameHuminity;

    gData.value=Logic->Get_huminity();
    graphOutput->addGraph(&gData);

    //Load Temperature
    gData.name=Logic->NameTemperature;

    gData.value=Logic->Get_temperature();
    graphOutput->addGraph(&gData);

}





void MainWindow::CSV_Load_NameTitle()
{
    QString dt="Time";
    _csv_hundler->addNameTitle(dt);
    for(int i=0;i<COUNT_AXES;i++)
    {
       _csv_hundler->addNameTitle(Logic->NameAccel[i]);
    }

    for(int i=0;i<COUNT_AXES;i++)
    {
       _csv_hundler->addNameTitle(Logic->NameGyro[i]);
    }

    for(int i=0;i<COUNT_AXES;i++)
    {
       _csv_hundler->addNameTitle(Logic->NameMagn[i]);
    }

    _csv_hundler->addNameTitle(Logic->NamePressure);
    _csv_hundler->addNameTitle(Logic->NameHuminity);
    _csv_hundler->addNameTitle(Logic->NameTemperature);



}

void MainWindow::slotTimerAlarmCSV(void)
{
    if(status_save_csv)
    {
        if(_csv_hundler->CSV_IsOpen())
        {
            _csv_hundler->addData(timeWork->elapsed()/1000.0);
          //qDebug()<< "Write Dats CSV";
          //QString str;
          //Load  Accel
          for(int i=0;i<COUNT_AXES;i++)
          {
           _csv_hundler->addData(*Logic->Get_Accel_INDEX(i));
          }
          //Load  Gyro
          for(int i=0;i<COUNT_AXES;i++)
          {
             _csv_hundler->addData(*Logic->Get_Gyro_INDEX(i));
          }
          //Load  Magn
          for(int i=0;i<COUNT_AXES;i++)
          {

            _csv_hundler->addData(*Logic->Get_Magn_INDEX(i));
          }

          //Load Pressure
          //_csv_hundler->addData_csv(*Logic->Get_Pressure());
          _csv_hundler->addData(*Logic->Get_Pressure());

          //Load Huminity


         // _csv_hundler->addData_csv(*Logic->Get_huminity());
           _csv_hundler->addData(*Logic->Get_huminity());
          //Load Temperature
           _csv_hundler->addData(*Logic->Get_temperature());

           count_packet_CSV++;
          _csv_hundler->CSV_Save_Data();
        }


    }

}

void MainWindow::Slot_Timer_TimeOut()
{

}

MainWindow::~MainWindow()
{
    delete ui;

}

void MainWindow:: slotTimerAlarm(void)
{
     graphOutput->updateGraph(timeWork->elapsed()/1000.0);




}


void MainWindow:: serialReceived(QByteArray data)
{

     protocol->serialProtReceived(data);
     qDebug()<<data;
}


void MainWindow:: receivedMsg(protocol_Frame_data* data)
{
    Logic->Parse_Data_RX((Command_Board)data->command,data);
    switch(data->command)
    {
      /*  case Get_Board:
        {
           Logic->Update_UI((Command_Board)data->command,ui->Label_Board_Info);

            break;
        }*/

        case Get_main_data:
        {
            Logic->Update_UI((Command_Board)data->command,ui->MainDataFill,ui->Label_Board_Info);
            float dataGyro[3]={0};
            float dataAccel[3]={0};
            for(int i=0;i<3;i++)
            {
                dataGyro[i]=*Logic->Get_Gyro_INDEX(i)/1000.0;
                dataAccel[i]=*Logic->Get_Accel_INDEX(i)/1000.0;
            }
            data_IMU->MadgwickAHRSupdateIMU(dataGyro[0],dataGyro[1],dataGyro[2],
                   dataAccel[0],dataAccel[1],dataAccel[2]);

            break;
        }

        case Get_setting_peripheral_board:
        {


            Logic->Update_UI(ui->lineEdit_K_1,
                             ui->lineEdit_K_2,
                             ui->lineEdit_K_3,
                             ui->lineEdit_K_4,
                             ui->lineEdit_K_5,
                             ui->lineEdit_K_6,
                             ui->lineEdit_K_7,
                             ui->lineEdit_K_8,
                             ui->lineEdit_K_9,
                             ui->lineEdit_K_10,
                             ui->lineEdit_K_11,
                             ui->lineEdit_K_12,
                             ui->lineEdit_K_13,
                             ui->lineEdit_K_14,
                             ui->lineEdit_K_15);
            break;
        }
    }
}

void MainWindow:: Request_Board_Tim(void)
{
    //qDebug()<<"test timer!!";
    if(status_start_request)
    {
        //function Request RS_Connected
        Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }
    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            Logic->Request_Get_MainData(protocol);
            break;
        }

    }
    count_request++;
    if(count_request>2)
        count_request=2;

    if(status_3D_Window)
    {

        data_IMU->Get_Angles_Ealer(&yaw,&pitch,&roll);
       // _3DPlot->rotateQuaternion(0,yaw,pitch,roll);
    }




}



void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box->addItem(info.portName());
         }
}

void MainWindow::on_Name_update_btn_clicked()
{
     ui->SpeedSerialPort_Box->clear();
     ui->NameSerialPort_Box->clear();
    Update_ComBoBox_Config();
}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton=!count_ConnectButton;

    if(count_ConnectButton)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);
        Logic->Set_ID(ID_DEVICE);

        bool status_Config=serial->ConnectPort(ui->NameSerialPort_Box->currentText(),
                                               (ui->SpeedSerialPort_Box->currentText()).toLong());

        if(status_Config)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");
            timer->setInterval(PERIOD_Request_MS);
            connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));
            //timer->start();
            timeWork->start();
            status_start_request=true;
        }
        else
        {
            QMessageBox::information(this," Not Connection!!","Connection error!");
            ui->NameSerialPort_Box->setEnabled(true);
            ui->SpeedSerialPort_Box->setEnabled(true);
        }


    }
    else
    {
        ui->on_ConnectButton->setText("Disconnected");
        timer->stop();
        status_start_request=false;
        serial->close();
        QMessageBox::information(this,"Connector Status","Close Port!!");
        ui->NameSerialPort_Box->setEnabled(true);
        ui->SpeedSerialPort_Box->setEnabled(true);

    }


}

void MainWindow::Immulator_Get_recieve_Data()
{
     qDebug()<<"test immulator timer!!";
     int16_t dataTest[12];
     for(int i=0;i<12;i++)
     {
         dataTest[i]=rand()%1000-10000;
     }
     QString dataMain=QString(
                 "%1: \t%13\n"
                 "%2:\t%14\n"
                 "%3:\t%15\n"
                 "%4:\t%16\n"
                 "%5:\t%17\n"
                 "%6:\t%18\n"
                 "%7:\t%19\n"
                 "%8:\t%20\n"
                 "%9:\t%21\n"
                 "%10:\t%22\n"
                 "%11:\t%23\n"
                 "%12:\t%24\n"

                 ).arg("NameAccel[0]")
                  .arg("NameAccel[1]")
                  .arg("NameAccel[2]")
                  .arg("NameGyro[0]")
                  .arg("NameGyro[1]")
                  .arg("NameGyro[2]")
                  .arg("NameMagn[0]")
                  .arg("NameMagn[1]")
                  .arg("NameMagn[2]")
                  .arg("NamePressure")
                  .arg("NameHuminity")
                  .arg("NameTemperature")
                  .arg(dataTest[0])
                  .arg(dataTest[1])
                  .arg(dataTest[2])
                  .arg(dataTest[3])
                  .arg(dataTest[4])
                  .arg(dataTest[5])
                  .arg(dataTest[6])
                  .arg(dataTest[7])
                  .arg(dataTest[8])
                  .arg(dataTest[9])
                  .arg(dataTest[10])
                  .arg(dataTest[11])

             ;
     ui->MainDataFill->setText(dataMain);
}


void MainWindow::on_Btn_PlotGraph_clicked()
{
    TimerAlarm->start();
    graphOutput->show();
    graphOutput->resetGraph();
}


void MainWindow::on_Btn_3D_clicked()
{
    _3DPlot->show();
    status_3D_Window=!status_3D_Window;
    //_3D_test->show();
}


void MainWindow::on_btn_save_clicked()
{
    status_save_csv=!status_save_csv;
   _csv_hundler-> CSV_State(status_save_csv);
    if(status_save_csv)
    {
        qDebug()<<"--------Start .... Write CSV-------";
        ui->btn_save->setText("стоп");
        TimerAlarmCSV->start();
    }
    else
    {
        ui->btn_save->setText("Start");
        TimerAlarmCSV->stop();


    }

}


void MainWindow::on_pushButton_4_clicked()
{
    Logic->Request_default_Seettings_Board(protocol);
}


void MainWindow::on_pushButton_clicked()
{
    float dataBuffer[15]={ui->lineEdit_K_1->text().toFloat(),
                         ui->lineEdit_K_2->text().toFloat(),
                         ui->lineEdit_K_3->text().toFloat(),
                         ui->lineEdit_K_10->text().toFloat(),
                         ui->lineEdit_K_11->text().toFloat(),

                         ui->lineEdit_K_5->text().toFloat(),
                         ui->lineEdit_K_6->text().toFloat(),
                         ui->lineEdit_K_4->text().toFloat(),
                         ui->lineEdit_K_13->text().toFloat(),
                         ui->lineEdit_K_12->text().toFloat(),

                          ui->lineEdit_K_8->text().toFloat(),
                          ui->lineEdit_K_9->text().toFloat(),
                          ui->lineEdit_K_7->text().toFloat(),
                          ui->lineEdit_K_15->text().toFloat(),
                          ui->lineEdit_K_14->text().toFloat()

                         };

    uint8_t Buf[sizeof(dataBuffer)*sizeof (float)]={0};
    memcpy(Buf,dataBuffer,sizeof(dataBuffer)*sizeof (float));
    Logic->Set_Seettings_Board(protocol,Buf,sizeof(Buf));



}


void MainWindow::on_pushButton_3_clicked()
{
    Logic->Request_read_Settings_Peripheral(protocol);
}


void MainWindow::on_pushButton_2_clicked()
{
   Logic-> Request_Flash(protocol);
}


void MainWindow::on_pushButton_5_clicked()
{
    Logic->Request_Get_MainData(protocol);
}


void MainWindow::on_pushButton_6_clicked()
{
    Logic->Request_Get_Config_Board(protocol);
}


void MainWindow::on_pushButton_7_clicked()
{
    Logic->Request_read_Settings_Peripheral(protocol);
}

