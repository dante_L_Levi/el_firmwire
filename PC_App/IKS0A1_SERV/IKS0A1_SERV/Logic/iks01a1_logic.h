#ifndef IKS01A1_LOGIC_H
#define IKS01A1_LOGIC_H

#include <QObject>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "Madgwick.h"
#include "QSerialPort"
#include "SerialPort/QSerialProtocol.h"



#define INDEX_X     0
#define INDEX_Y     1
#define INDEX_Z     2

#define COUNT_AXES  3
#define COUNT_PERIPHERAL    3



typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,
    Get_main_data,
    Get_setting_peripheral_board,

    Set_setting_peripheral_board,
    Set_Default_peripheral_board,
    Set_cmd_flash

}Command_Board;

#pragma pack(push, 1)
typedef struct
{
    uint16_t ID;
    uint8_t dataName[8];
    uint8_t datafirmwire[8];

}Recieve_data1_Model_DataBoard;

typedef struct
{
    int16_t Gyro[COUNT_AXES];
    int16_t Accel[COUNT_AXES];

}lsm6ds0_def_Gyro_Accel_sensor;


typedef struct
{
     int16_t Magn[COUNT_AXES];
}LIS3MDL_def_Magnetometter_sensor;

typedef struct
{
   int32_t pressure;

}LPS25HB_def_Pressure_sensor;

typedef struct
{
    float  humidity;
    float  temperature;
}Hts221_def_humidity_temperature_sensor;


typedef struct
{
    lsm6ds0_def_Gyro_Accel_sensor               _lsm6ds0;
    LIS3MDL_def_Magnetometter_sensor            _LIS3MDL;
    LPS25HB_def_Pressure_sensor                 _LPS25HB;
    Hts221_def_humidity_temperature_sensor      _Hts221;

}Recieve_data2_Model_MainData;

typedef struct
{
    float Kp;
    float Ki;
    float Kd;
    float Saturation_P;
    float Saturation_M;
}Settings_def;


typedef struct
{
    Settings_def        dataSettings[COUNT_PERIPHERAL];

}Recieve_data3_Model_Settings;



typedef struct
{
    uint8_t firmwire[8]={0};
    char Board_name[8]={0};
    uint16_t Id_Recieve;

    float accel_g[COUNT_AXES];//in g
    float gyro_g[COUNT_AXES];//in degrees/s
    float Magn_Gaus[COUNT_AXES];//in Gaus
    float temperature;
    float Huminity;
    float Pressure;
    Settings_def        dataSettings[COUNT_PERIPHERAL];

}Model_MainData_def;



#pragma pack(pop)






class IKS01A1_Logic : public QObject
{
    Q_OBJECT
public:
    explicit IKS01A1_Logic(QObject *parent = nullptr);

    void Set_ID(uint16_t Id_dev);
    void Set_Seettings_Board(QSerialProtocol *protocol,uint8_t *data, uint16_t length);
    void Request_Flash(QSerialProtocol *protocol);
    void Parse_Data_RX(Command_Board cmd,protocol_Frame_data *data);
    void Update_UI(Command_Board cmd,QLabel *data);
    void Update_UI(Command_Board cmd, QLabel *data,QLabel *data2);
    void Update_UI(QLineEdit *data1,
                   QLineEdit *data2,
                   QLineEdit *data3,
                   QLineEdit *data4,
                   QLineEdit *data5,
                   QLineEdit *data6,
                   QLineEdit *data7,
                   QLineEdit *data8,
                   QLineEdit *data9,
                   QLineEdit *data10,
                   QLineEdit *data11,
                   QLineEdit *data12,
                   QLineEdit *data13,
                   QLineEdit *data14,
                   QLineEdit *data15);
    float* Get_Accel(void);
    float* Get_Accel_INDEX(uint8_t index);
    float* Get_Gyro(void);
    float* Get_Gyro_INDEX(uint8_t index);
    float* Get_Magn(void);
    float* Get_Magn_INDEX(uint8_t index);
    float* Get_Pressure(void);
    float* Get_temperature(void);
    float* Get_huminity(void);

    void Imulator_recieve_data_Start(void);

    void Request_RS_Connect(QSerialProtocol *protocol);
    void Request_Get_Config_Board(QSerialProtocol *protocol);
    void Request_Get_MainData(QSerialProtocol *protocol);
    void Request_default_Seettings_Board(QSerialProtocol *protocol);
    void Request_read_Settings_Peripheral(QSerialProtocol *protocol);

    QString NameAccel[3]=
    {
        "Accel x",
        "Accel y",
        "Accel z",

    };

    QString NameGyro[3]=
    {
        "Gyro x",
        "Gyro y",
        "Gyro z",

    };

    QString NameMagn[3]=
    {
        "Magn x",
        "Magn y",
        "Magn z",

    };
    QString NamePressure="Pressure";
    QString NameHuminity="Huminity";
    QString NameTemperature="Temperature";






private:
    Recieve_data1_Model_DataBoard       *recieve_data1;
    Recieve_data2_Model_MainData        *recieve_data2;
    Recieve_data3_Model_Settings        *recieve_data3;
    uint16_t ID=0;
    double count_Packet=0;

    Madgwick                            *madgwick_filter_data;
    Model_MainData_def                  _mainData;
    QTimer*                             _timer_work;

    void Update_Model(Recieve_data1_Model_DataBoard *data);
    void Update_Model(Recieve_data2_Model_MainData *data);
    void Update_Model(Recieve_data3_Model_Settings *data);


private slots:
    void Timer_Imulator_reciever(void);

signals:
    void Signal_Timer_Imulator_reciever(void);

};

#endif // IKS01A1_LOGIC_H
