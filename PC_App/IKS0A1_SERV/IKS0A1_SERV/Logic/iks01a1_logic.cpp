#include "iks01a1_logic.h"
#include "QDebug"

float Converted_K_Rad_s=0.017453;
float Sensivity_Gyro=70.0;//70
float Sensivity_Accel=0.488;//0.488
float Sensivity_Magn=6842.0;//6842


IKS01A1_Logic::IKS01A1_Logic(QObject *parent) : QObject(parent)
{

}

void IKS01A1_Logic::Set_ID(uint16_t Id_dev)
{
    ID=Id_dev;
}

void IKS01A1_Logic::Set_Seettings_Board(QSerialProtocol *protocol,uint8_t *data, uint16_t length)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_peripheral_board;
    sendMsg.length=length;
    sendMsg.data=data;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Request_Flash(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_cmd_flash;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Parse_Data_RX(Command_Board cmd, protocol_Frame_data *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
            recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
            //memcpy(recieve_data1,data->data,data->length);
            count_Packet++;
            Update_Model(recieve_data1);

        break;
        }

        case Get_main_data:
        {
            recieve_data2=reinterpret_cast<Recieve_data2_Model_MainData*>(data->data);
            count_Packet++;
            Update_Model(recieve_data2);
            break;
        }

        case Get_setting_peripheral_board:
        {
            recieve_data3=reinterpret_cast<Recieve_data3_Model_Settings*>(data->data);
            count_Packet++;
            Update_Model(recieve_data3);
            break;
        }

        default:
            break;

    }


}

void IKS01A1_Logic::Update_UI(Command_Board cmd, QLabel *data)
{
    switch (cmd)
    {
        case Get_Board:
        {

            char dataName[8]={0};
            int i=0;
            while(_mainData.Board_name[i]!=' ')
            {
                dataName[i]=_mainData.Board_name[i];
                i++;
            }
            //qDebug()<<"NAME:"<<dataName;
           QString dataBoard=QString(
                           "%1:%5\n"
                           "%2:%6\n"
                           "%3:%7\n"
                           "%4:%8\n"

                        ).arg("ID")
                        .arg("Version")
                        .arg("NameBoard")
                        .arg("Recieve Bytes:")
                        .arg(_mainData.Id_Recieve)
                   .arg((const char*)_mainData.firmwire)
                   .arg(dataName)
                   .arg(count_Packet);
                    data->setText(dataBoard) ;

        break;
        }

        case Get_main_data:
        {
            QString dataMain=QString(
                        "%1:\t\t\t\t%13 mg\n"
                        "%2:\t\t\t\t%14 mg\n"
                        "%3:\t\t\t\t%15 mg\n"
                        "%4:\t\t\t\t%16 mdps\n"
                        "%5:\t\t\t\t%17 mdps\n"
                        "%6:\t\t\t\t%18 mdps\n"
                        "%7:\t\t\t\t%19 mGaus\n"
                        "%8:\t\t\t\t%20 mGaus\n"
                        "%9:\t\t\t\t%21 mGaus\n"
                        "%10:\t\t\t\t%22 \n"
                        "%11:\t\t\t\t%23 %\n"
                        "%12:\t\t\t\t%24 C\n"

                        ).arg(NameAccel[0])
                         .arg(NameAccel[1])
                         .arg(NameAccel[2])
                         .arg(NameGyro[0])
                         .arg(NameGyro[1])
                         .arg(NameGyro[2])
                         .arg(NameMagn[0])
                         .arg(NameMagn[1])
                         .arg(NameMagn[2])
                         .arg(NamePressure)
                         .arg(NameHuminity)
                         .arg(NameTemperature)
                         .arg(_mainData.accel_g[0])
                         .arg(_mainData.accel_g[1])
                         .arg(_mainData.accel_g[2])
                         .arg(_mainData.gyro_g[0])
                         .arg(_mainData.gyro_g[1])
                         .arg(_mainData.gyro_g[2])
                         .arg(_mainData.Magn_Gaus[0])
                         .arg(_mainData.Magn_Gaus[1])
                         .arg(_mainData.Magn_Gaus[2])
                         .arg(_mainData.Pressure)
                         .arg(_mainData.Huminity)
                         .arg(_mainData.temperature)

                    ;
            data->setText(dataMain);
            break;
        }


    default:
        break;

    }
}

void IKS01A1_Logic::Update_UI(Command_Board cmd, QLabel *data,QLabel *data2)
{
    switch (cmd)
    {
        case Get_Board:
        {

            char dataName[8]={0};
            int i=0;
            while(_mainData.Board_name[i]!=' ')
            {
                dataName[i]=_mainData.Board_name[i];
                i++;
            }
            //qDebug()<<"NAME:"<<dataName;
           QString dataBoard=QString(
                           "%1:%5\n"
                           "%2:%6\n"
                           "%3:%7\n"
                           "%4:%8\n"

                        ).arg("ID")
                        .arg("Version")
                        .arg("NameBoard")
                        .arg("Recieve Bytes:")
                        .arg(_mainData.Id_Recieve)
                   .arg((const char*)_mainData.firmwire)
                   .arg(dataName)
                   .arg(count_Packet);
                    data->setText(dataBoard) ;

        break;
        }

        case Get_main_data:
        {
            QString dataMain=QString(
                        "%1:\t\t\t\t%13 mg\n"
                        "%2:\t\t\t\t%14 mg\n"
                        "%3:\t\t\t\t%15 mg\n"
                        "%4:\t\t\t\t%16 mdps\n"
                        "%5:\t\t\t\t%17 mdps\n"
                        "%6:\t\t\t\t%18 mdps\n"
                        "%7:\t\t\t\t%19 mGaus\n"
                        "%8:\t\t\t\t%20 mGaus\n"
                        "%9:\t\t\t\t%21 mGaus\n"
                        "%10:\t\t\t\t%22 \n"
                        "%11:\t\t\t\t%23 %\n"
                        "%12:\t\t\t\t%24 C\n"

                        ).arg(NameAccel[0])
                         .arg(NameAccel[1])
                         .arg(NameAccel[2])
                         .arg(NameGyro[0])
                         .arg(NameGyro[1])
                         .arg(NameGyro[2])
                         .arg(NameMagn[0])
                         .arg(NameMagn[1])
                         .arg(NameMagn[2])
                         .arg(NamePressure)
                         .arg(NameHuminity)
                         .arg(NameTemperature)
                         .arg(_mainData.accel_g[0])
                         .arg(_mainData.accel_g[1])
                         .arg(_mainData.accel_g[2])
                         .arg(_mainData.gyro_g[0])
                         .arg(_mainData.gyro_g[1])
                         .arg(_mainData.gyro_g[2])
                         .arg(_mainData.Magn_Gaus[0])
                         .arg(_mainData.Magn_Gaus[1])
                         .arg(_mainData.Magn_Gaus[2])
                         .arg(_mainData.Pressure)
                         .arg(_mainData.Huminity)
                         .arg(_mainData.temperature)

                    ;
            data->setText(dataMain);



            char dataName[8]={0};
            int i=0;
            while(_mainData.Board_name[i]!=' ')
            {
                dataName[i]=_mainData.Board_name[i];
                i++;
            }
            //qDebug()<<"NAME:"<<dataName;
           QString dataBoard=QString(
                           "%1:%5\n"
                           "%2:%6\n"
                           "%3:%7\n"
                           "%4:%8\n"

                        ).arg("ID")
                        .arg("Version")
                        .arg("NameBoard")
                        .arg("Recieve Bytes:")
                        .arg(_mainData.Id_Recieve)
                   .arg((const char*)_mainData.firmwire)
                   .arg(dataName)
                   .arg(count_Packet);
                    data2->setText(dataBoard) ;





            break;
        }


    default:
        break;

    }
}



void IKS01A1_Logic::Update_UI(QLineEdit *data1,
                              QLineEdit *data2,
                              QLineEdit *data3,
                              QLineEdit *data4,
                              QLineEdit *data5,
                              QLineEdit *data6,
                              QLineEdit *data7,
                              QLineEdit *data8,
                              QLineEdit *data9,
                              QLineEdit *data10,
                              QLineEdit *data11,
                              QLineEdit *data12,
                              QLineEdit *data13,
                              QLineEdit *data14,
                              QLineEdit *data15)
{
    data1->setText(QString::number(_mainData.dataSettings[0].Kp));
    data2->setText(QString::number(_mainData.dataSettings[0].Ki));
    data3->setText(QString::number(_mainData.dataSettings[0].Kd));
    data4->setText(QString::number(_mainData.dataSettings[0].Saturation_M));
    data5->setText(QString::number(_mainData.dataSettings[0].Saturation_P));

    data6->setText(QString::number(_mainData.dataSettings[1].Kp));
    data7->setText(QString::number(_mainData.dataSettings[1].Ki));
    data8->setText(QString::number(_mainData.dataSettings[1].Kd));
    data9->setText(QString::number(_mainData.dataSettings[1].Saturation_M));
    data10->setText(QString::number(_mainData.dataSettings[1].Saturation_P));

    data11->setText(QString::number(_mainData.dataSettings[2].Kp));
    data12->setText(QString::number(_mainData.dataSettings[2].Ki));
    data13->setText(QString::number(_mainData.dataSettings[2].Kd));
    data14->setText(QString::number(_mainData.dataSettings[2].Saturation_M));
    data15->setText(QString::number(_mainData.dataSettings[2].Saturation_P));

}

float *IKS01A1_Logic::Get_Accel(void)
{
    return _mainData.accel_g;
}


float* IKS01A1_Logic::Get_Accel_INDEX(uint8_t index)
{
    return &_mainData.accel_g[index];
}



float *IKS01A1_Logic::Get_Gyro(void)
{
    return _mainData.gyro_g;
}

float* IKS01A1_Logic::Get_Gyro_INDEX(uint8_t index)
{
    return &_mainData.gyro_g[index];
}

float *IKS01A1_Logic::Get_Magn(void)
{
    return _mainData.Magn_Gaus;
}

float* IKS01A1_Logic::Get_Magn_INDEX(uint8_t index)
{
    return &_mainData.Magn_Gaus[index];
}

float* IKS01A1_Logic::Get_Pressure()
{
    return &_mainData.Pressure;
}

float* IKS01A1_Logic::Get_temperature()
{
    return &_mainData.temperature;
}

float* IKS01A1_Logic::Get_huminity()
{
    return &_mainData.Huminity;
}

void IKS01A1_Logic::Imulator_recieve_data_Start()
{
    _timer_work=new QTimer();
    _timer_work->setInterval(250);
    connect(_timer_work, SIGNAL(timeout()), this,
            SLOT(Timer_Imulator_reciever()));
    _timer_work->start();

}

void IKS01A1_Logic::Request_RS_Connect(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Connect;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);

}

void IKS01A1_Logic::Request_Get_Config_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_Board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Request_Get_MainData(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_main_data;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Request_default_Seettings_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Default_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Request_read_Settings_Peripheral(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void IKS01A1_Logic::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->datafirmwire,8);
    memcpy(_mainData.Board_name,data->dataName,8);
    _mainData.Id_Recieve=data->ID;

}

void IKS01A1_Logic::Update_Model(Recieve_data2_Model_MainData *data)
{
    for(int i=0;i<COUNT_AXES;i++)
    {
        _mainData.accel_g[i]=data->_lsm6ds0.Accel[i]*Sensivity_Accel;
        _mainData.gyro_g[i]=data->_lsm6ds0.Gyro[i]*Sensivity_Gyro;
        _mainData.Magn_Gaus[i]=data->_LIS3MDL.Magn[i]*Sensivity_Magn;
       // qWarning()<<"Gyro["<<i<<"]:"<<data->_lsm6ds0.Gyro[i];
       // qWarning()<<"MAgn["<<i<<"]:"<<data->_LIS3MDL.Magn[i];
      //  qWarning()<<"Gyro REAL["<<i<<"]:"<<_mainData.gyro_g[i];
      //  qWarning()<<"MAgn REAL["<<i<<"]:"<<_mainData.Magn_Gaus[i];

    }
    _mainData.Pressure=data->_LPS25HB.pressure;
    _mainData.Huminity=data->_Hts221.humidity;
    _mainData.temperature=data->_Hts221.temperature;



}

void IKS01A1_Logic::Update_Model(Recieve_data3_Model_Settings *data)
{
    _mainData.dataSettings[0].Kp=data->dataSettings[0].Kp;
    _mainData.dataSettings[0].Ki=data->dataSettings[0].Ki;
    _mainData.dataSettings[0].Kd=data->dataSettings[0].Kd;
    _mainData.dataSettings[0].Saturation_M=data->dataSettings[0].Saturation_M;
    _mainData.dataSettings[0].Saturation_P=data->dataSettings[0].Saturation_P;

    _mainData.dataSettings[1].Kp=data->dataSettings[1].Kp;
    _mainData.dataSettings[1].Ki=data->dataSettings[1].Ki;
    _mainData.dataSettings[1].Kd=data->dataSettings[1].Kd;
    _mainData.dataSettings[1].Saturation_M=data->dataSettings[1].Saturation_M;
    _mainData.dataSettings[1].Saturation_P=data->dataSettings[1].Saturation_P;



}

void IKS01A1_Logic::Timer_Imulator_reciever()
{
    emit Signal_Timer_Imulator_reciever();
}
