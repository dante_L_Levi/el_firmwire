QT       += core gui serialport
QT       +=opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CSV_Manager/CSV_Manager.cpp \
    Logic/iks01a1_logic.cpp \
    Madgwick.cpp \
    Plot_3D/FormOpenGL.cpp \
    Plot_3D/OPENGL_Widget.cpp \
    SerialPort/QSerialProtocol.cpp \
    SerialPort/SerialPort.cpp \
    formgraph.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    style_classes/window_styles.cpp

HEADERS += \
    CSV_Manager/CSV_Manager.h \
    Logic/iks01a1_logic.h \
    Madgwick.h \
    Plot_3D/FormOpenGL.h \
    Plot_3D/OPENGL_Widget.h \
    SerialPort/QSerialProtocol.h \
    SerialPort/SerialPort.h \
    formgraph.h \
    mainwindow.h \
    qcustomplot.h \
    style_classes/window_styles.h

FORMS += \
    Plot_3D/FormOpenGL.ui \
    formgraph.ui \
    mainwindow.ui


LIBS    +=-lopengl32
LIBS    +=-lglu32

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource_data.qrc
