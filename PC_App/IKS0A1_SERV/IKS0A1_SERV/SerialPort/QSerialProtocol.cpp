#include "QSerialProtocol.h"
#include "QDebug"

#define SPROTOCOL_COMMAND_GO_BOOT       0xFF
#define SPROTOCOL_COMMAND_ENABLE        0xFE
#define GO_TO_BOOTLOADER_RS485_KEY      0xf2784c28


#define SProtocolMaster  1
#define SProtocolSlave   0

QByteArray data_frame_recieve;

#pragma pack(push, 1)
typedef union{
    struct SPpack{
        uint16_t        ID;
        uint8_t         len;
        uint8_t         cmd;
        uint8_t         payload[255];
        uint16_t        crc;
    }pack;
    uint8_t dataPack[261];
}ProtocolSendPack;
#pragma pack(pop)



QSerialProtocol::QSerialProtocol() : QObject()
{
    QSerialProtocol(0x0000);
}

QSerialProtocol::QSerialProtocol(uint16_t id) : QObject()
{
    setID(id);
    stateReceived=(uint8_t)RX_WAIT_ID_PART1;
    collectionData.clear();

}

void QSerialProtocol::setID(uint16_t id)
{
    ID_DEV=id;
    this->idSlave  = id&0xFFFF;
    this->idMaster = id|0x8000;



}

void QSerialProtocol::sendGoBoot()
{
    protocol_Frame_data sendMsg;
    sendMsg.command=SPROTOCOL_COMMAND_GO_BOOT;
    sendMsg.length=4;
    uint32_t data=GO_TO_BOOTLOADER_RS485_KEY;
    //sendMsg.data=static_cast<uint8_t*>(&data);
    sendMsg.data=(uint8_t*)(&data);
    transmitData(&sendMsg);
}



void QSerialProtocol::transmitData(protocol_Frame_data *_frame)
{
        ProtocolSendPack sendPack;
        sendPack.pack.ID=ID_DEV;
        sendPack.pack.cmd=_frame->command;
        sendPack.pack.len=_frame->length;
        memcpy(sendPack.pack.payload,_frame->data,_frame->length);
        sendPack.pack.crc=Calculate_CRC16(sendPack.dataPack, sendPack.pack.len+4);
        sendPack.pack.payload[sendPack.pack.len+1]=(sendPack.pack.crc>>8)&0xFF;
        sendPack.pack.payload[sendPack.pack.len]=sendPack.pack.crc&0xFF;
        QByteArray dataTx=QByteArray((char*)sendPack.dataPack, sendPack.pack.len+6);
        emit serialTransmit(&dataTx);
}
//need edit function
void QSerialProtocol::parseRecieverDataBuffer(QByteArray Array)
{
    //qDebug()<<"Length:"<<Array.size();
//    uint8_t buf[data.size()];
//    for(int i=0;i<data.size();i++)
//    {
//        buf[i]=data[i];
//    }
    //uint8_t *buf=reinterpret_cast<uint8_t*>(Array.data() );
   // uint8_t buf[7];
   // memcpy(buf,Array.data(),7);
    //qDebug()<<"Data:"<<buf;
   // if(checkCRC16(Array,Array.length()))
    //{
    //    qDebug()<<"CRC OK";
        //CRC passed
    //    protocol_Frame_data msg;
    //    msg.command=(uint8_t)Array[3];
    //    msg.length=(uint8_t)Array[2];
    //    uint8_t dataBuffer[msg.length];
    //    memcpy(dataBuffer,Array.right(msg.length+2).data(),msg.length);
    //    msg.data=dataBuffer;
    //    emit receivedMsgFrame(&msg);
    //}

    int16_t num=0;
    while(num<Array.length())
    {
        //qDebug()<<"state:"<<stateReceived;
        switch (stateReceived)
        {
            case RX_COLLECT_DATA:
            {
                collectionData.append(Array[num]);
                if (collectionData.length() >= lengthExpectid)
                {
                    //qDebug()<<"RECIEVE DATA:"<<collectionData;
                    //package received
                    stateReceived =(uint8_t) RX_WAIT_ID_PART1;
                    if(checkCRC16(collectionData,lengthExpectid))
                    {
                        //CRC passed
                         //qDebug()<<"CRC passed";
                        protocol_Frame_data msg;
                        msg.command=(uint8_t)collectionData[3];
                        msg.length=(uint8_t)collectionData[2];
                        uint8_t dataBuffer[msg.length];
                        memcpy(dataBuffer,collectionData.right(msg.length+2).data(),msg.length);
                        msg.data=dataBuffer;
                        emit receivedMsgFrame(&msg);

                    }
                    collectionData.clear();
                }
                break;
            }

            case RX_WAIT_LENGTH:
            {
                collectionData.append(Array[num]);
                lengthExpectid=Array[num]+6;          //head+crc
                stateReceived =(uint8_t) RX_COLLECT_DATA;
                break;
            }

            case RX_WAIT_ID_PART2:
            {
                uint8_t temp= (uint8_t)(this->idSlave >> 8);
                uint8_t temp2=Array[num];
                if (temp == temp2)
                {
                    stateReceived =(uint8_t) RX_WAIT_LENGTH;
                    collectionData.append(Array[num]);
                    break;
                }
                else
                {
                    stateReceived =(uint8_t) RX_WAIT_ID_PART1;
                }

            }

            case RX_WAIT_ID_PART1:
            {
                if ((this->idSlave & 0xFF) == Array[num])
                {
                    stateReceived =(uint8_t) RX_WAIT_ID_PART2;
                    collectionData.append(Array[num]);
                }
                break;
            }

            default:
               break;
        }
        num++;
    }

}

void QSerialProtocol::serialProtReceived(QByteArray data)
{
    data_frame_recieve=QByteArray(data);
    //qDebug()<<"Input Data: "<<collectionData;
    parseRecieverDataBuffer(data_frame_recieve);
}


/**********************Calculate CRC16*****************************/
uint16_t QSerialProtocol:: Calculate_CRC16(uint8_t *data,uint8_t len)
{
            uint16_t crc = 0xFFFF;
           uint8_t i;

           while (len--)
           {
               crc ^= *data++ << 8;

               for (i = 0; i < 8; i++)
                   crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
           }
           return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t QSerialProtocol:: checkCRC16(uint8_t *data,uint8_t len)
{
    uint16_t crc = 0xFFFF;
         uint8_t i;
         uint16_t des_buf=(data[len-1]<<8)|data[len-2];
         uint8_t len_dst=len-2;
         while (len_dst--)
         {
             crc ^= *data++ << 8;

             for (i = 0; i < 8; i++)
                 crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
         }
         if(crc==des_buf)
         {
             return  true;
         }
         else
         {
            return  false;
         }
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t QSerialProtocol:: checkCRC16(QByteArray data,uint8_t len)
{
    uint16_t crc = 0xFFFF;
         uint8_t i;
         uint8_t j=0;
         uint16_t des_buf=(data[len-2]<<8)|data[len-1];
         uint8_t len_dst=len-2;

         while (len_dst--)
         {
             crc ^= data[j] << 8;

             for (i = 0; i < 8; i++)
                 crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
            j++;
         }
         if(crc==des_buf)
         {
             return  true;
         }
         else
         {
            return  false;
         }
}
