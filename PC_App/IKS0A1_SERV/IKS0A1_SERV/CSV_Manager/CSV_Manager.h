#ifndef CSV_MANAGER_H
#define CSV_MANAGER_H

#include <QObject>
#include <QFile>
#include <QTextStream>


class CSV_Manager : public QObject
{
    Q_OBJECT
public:
    explicit CSV_Manager(QObject *parent = nullptr);


    void CSV_Init(void);
    void addNameTitle(QString name);
    void CSV_Save_Title(void);
    void addData(float data);
    void CSV_Save_Data(void);
    void CSV_State(bool status);
    bool CSV_IsOpen(void);



private:
    QFile *textFileSave;
    QTextStream *outTextFile;
     bool status_save_csv=false;
     QString Name;
     QString MainData;
     bool saveEnable=false;


signals:

};

#endif // CSV_MANAGER_H
