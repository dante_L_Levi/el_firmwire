#ifndef MAINLOGIC_H
#define MAINLOGIC_H

#include <QObject>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QSerialPort"
#include "SerialPort/QSerialProtocol.h"


typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,
    Get_main_data,
    Get_setting_peripheral_board,

    Set_setting_adc_Kp_peripheral_board,
    Set_setting_adc_offset_peripheral_board,
    Set_Default_setting_peripheral_board,
    Set_cmd_flash

}Command_Board;

#pragma pack(push, 1)
typedef struct
{
    uint16_t ID;
    uint8_t dataName[8];
    uint8_t datafirmwire[8];

}Recieve_data1_Model_DataBoard;


typedef union
{
    struct
        {
        int16_t channel1;
        int16_t channel2;
        int16_t channel3;
        int16_t channel4;
        int16_t channel5;
        int16_t channel6;

        }fields;
        int16_t All_adc_data[6];

}Recieve_data2_adc1_s;


typedef struct
{

    float adc_channels_Kp[6];
    float adc_ofsset_channels[6];


}FLASH_SETTINGS_Data_s;


typedef struct
{
    uint8_t firmwire[8];
    char Board_name[8];
    uint16_t Id_Recieve;

    float ADC_Voltage[6];//in Volt
    FLASH_SETTINGS_Data_s dataSettings;


}Model_MainData_def;

#pragma pack(pop)


typedef struct
{
    QLineEdit *data1;
    QLineEdit *data2;
    QLineEdit *data3;
    QLineEdit *data4;
    QLineEdit *data5;
    QLineEdit *data6;
    QLineEdit *data7;
    QLineEdit *data8;
    QLineEdit *data9;
    QLineEdit *data10;
    QLineEdit *data11;
    QLineEdit *data12;
}Settings_UI_def;

class MainLogic : public QObject
{
    Q_OBJECT
public:
    explicit MainLogic(QObject *parent = nullptr);

    void Set_ID(uint16_t Id_dev);
    void Request_Flash(QSerialProtocol *protocol);
    void Parse_Data_RX(Command_Board cmd,protocol_Frame_data *data);
    void Update_UI(Command_Board cmd,QLabel *data);
    void Update_UI(Settings_UI_def *dataUI);
    void Update_UI(QLineEdit *data1,
                   QLineEdit *data2,
                   QLineEdit *data3,
                   QLineEdit *data4,
                   QLineEdit *data5,
                   QLineEdit *data6,
                   QLineEdit *data7,
                   QLineEdit *data8,
                   QLineEdit *data9,
                   QLineEdit *data10,
                   QLineEdit *data11,
                   QLineEdit *data12);


    void Request_RS_Connect(QSerialProtocol *protocol);
    void Request_Get_Config_Board(QSerialProtocol *protocol);
    void Request_Get_MainData(QSerialProtocol *protocol);
    void Request_default_Seettings_Board(QSerialProtocol *protocol);
    void Request_read_Settings_Peripheral(QSerialProtocol *protocol);

    void Request_Set_Adc_Kp_Config_Peripheral(QSerialProtocol *protocol,float *data,uint8_t len);
    void Request_Set_Adc_Offset_Config_Peripheral(QSerialProtocol *protocol,float *data,uint8_t len);





private:
    Recieve_data1_Model_DataBoard       *recieve_data1;
    Recieve_data2_adc1_s                *recieve_data2;
    FLASH_SETTINGS_Data_s               *recieve_data3;

    uint16_t ID=0;
    double count_Packet=0;

    void Update_Model(Recieve_data1_Model_DataBoard *data);
    void Update_Model(Recieve_data2_adc1_s *data);
    void Update_Model(FLASH_SETTINGS_Data_s *data);

    Model_MainData_def _mainData;
    QStringList DataName=
    {
     "Channe1:",
     "Channe2:",
     "Channe3:",
     "Channe4:",
     "Channe5:",
     "Channe6:"
    };

signals:
    void Update_Packet_Count(double data);
};

#endif // MAINLOGIC_H
