#include "MainLogic.h"
#include "QDebug"
#include "QMessageBox"


MainLogic::MainLogic(QObject *parent) : QObject(parent)
{

}

void MainLogic::Set_ID(uint16_t Id_dev)
{
    ID=Id_dev;
}


void MainLogic::Request_Flash(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_cmd_flash;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Parse_Data_RX(Command_Board cmd, protocol_Frame_data *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
        recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
        //memcpy(recieve_data1,data->data,data->length);
        count_Packet++;
        Update_Model(recieve_data1);
            break;
        }

        case Get_main_data:
        {
        recieve_data2=reinterpret_cast<Recieve_data2_adc1_s*>(data->data);
        count_Packet++;
        Update_Model(recieve_data2);
            break;
        }

        case Get_setting_peripheral_board:
        {
            recieve_data3=reinterpret_cast<FLASH_SETTINGS_Data_s*>(data->data);

            Update_Model(recieve_data3);
            break;
        }
    default:
        break;

    }
     count_Packet++;
     emit Update_Packet_Count(count_Packet);
     //qDebug()<<count_Packet;
}

void MainLogic::Update_UI(Command_Board cmd, QLabel *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
        char dataName[8]={0};
        int i=0;
        while(_mainData.Board_name[i]!=' ')
        {
            dataName[i]=_mainData.Board_name[i];
            i++;
        }
        //qDebug()<<"NAME:"<<dataName;
       QString dataBoard=QString(
                       "%1:%4\n"
                       "%2:%5\n"
                       "%3:%6\n"


                    ).arg("ID")
                    .arg("Version")
                    .arg("NameBoard")
                    .arg(_mainData.Id_Recieve)
               .arg((const char*)_mainData.firmwire)
               .arg(dataName);

                data->setText(dataBoard) ;

    break;
            break;
        }
        case Get_main_data:
        {
        QString dataMain=QString(
                    "%1:\t\t\t\t%7 mV\n"
                    "%2:\t\t\t\t%8 mV\n"
                    "%3:\t\t\t\t%9 mV\n"
                    "%4:\t\t\t\t%10 mV\n"
                    "%5:\t\t\t\t%11 mV\n"
                    "%6:\t\t\t\t%12 mV\n"

                    ).arg(DataName[0])
                     .arg(DataName[1])
                     .arg(DataName[2])
                     .arg(DataName[3])
                     .arg(DataName[4])
                     .arg(DataName[5])
                     .arg(_mainData.ADC_Voltage[0])
                     .arg(_mainData.ADC_Voltage[1])
                     .arg(_mainData.ADC_Voltage[2])
                     .arg(_mainData.ADC_Voltage[3])
                     .arg(_mainData.ADC_Voltage[4])
                     .arg(_mainData.ADC_Voltage[5])

                ;
        data->setText(dataMain);
            break;
        }


        default:
            break;
    }

}



void MainLogic::Update_UI(Settings_UI_def *dataUI)
{
    try {
        dataUI->data1->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[0]));
        dataUI->data2->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[1]));
        dataUI->data3->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[2]));
        dataUI->data4->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[3]));
        dataUI->data5->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[4]));
        dataUI->data6->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[5]));


        dataUI->data7->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[0]));
        dataUI->data8->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[1]));
        dataUI->data9->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[2]));
        dataUI->data10->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[3]));
        dataUI->data11->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[4]));
        dataUI->data12->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[5]));


        dataUI->data1->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data2->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data3->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data4->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data5->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data6->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data7->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data8->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data9->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data10->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data11->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
        dataUI->data12->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    }
    catch(...)
    {
       qDebug()<<"ERROR UI!!";
    }



}

void MainLogic::Update_UI(QLineEdit *data1, QLineEdit *data2, QLineEdit *data3, QLineEdit *data4, QLineEdit *data5, QLineEdit *data6, QLineEdit *data7, QLineEdit *data8, QLineEdit *data9, QLineEdit *data10, QLineEdit *data11, QLineEdit *data12)
{
    data1->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[0]));
    data2->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[1]));
    data3->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[2]));
    data4->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[3]));
    data5->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[4]));
    data6->setText(QString::number(_mainData.dataSettings.adc_channels_Kp[5]));


    data7->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[0]));
    data8->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[1]));
    data9->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[2]));
    data10->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[3]));
    data11->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[4]));
    data12->setText(QString::number(_mainData.dataSettings.adc_ofsset_channels[5]));


    data1->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data2->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data3->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data4->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data5->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data6->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data7->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data8->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data9->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data10->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data11->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    data12->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
}

void MainLogic::Request_RS_Connect(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Connect;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Get_Config_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_Board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Get_MainData(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_main_data;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_default_Seettings_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Default_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_read_Settings_Peripheral(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Set_Adc_Kp_Config_Peripheral(QSerialProtocol *protocol, float *data,uint8_t len)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_adc_Kp_peripheral_board;
    uint8_t dt[32]={0};
   memcpy(dt,data,len);
   // uint8_t *dt=reinterpret_cast<uint8_t*>(data);
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Set_Adc_Offset_Config_Peripheral(QSerialProtocol *protocol, float *data, uint8_t len)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_adc_offset_peripheral_board;
    uint8_t dt[32]={0};
    memcpy(dt,data,len);
    //uint8_t *dt=reinterpret_cast<uint8_t*>(data);
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->datafirmwire,8);
    memcpy(_mainData.Board_name,data->dataName,8);
    _mainData.Id_Recieve=data->ID;
}

void MainLogic::Update_Model(Recieve_data2_adc1_s *data)
{
    for(int i=0;i<6;i++)
    {
        _mainData.ADC_Voltage[i]=data->All_adc_data[i];
    }
}

void MainLogic::Update_Model(FLASH_SETTINGS_Data_s *data)
{
    for(int i=0;i<6;i++)
    {
        _mainData.dataSettings.adc_channels_Kp[i]=data->adc_channels_Kp[i];
        _mainData.dataSettings.adc_ofsset_channels[i]=data->adc_ofsset_channels[i];
    }
}
