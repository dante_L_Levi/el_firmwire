#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"

#define PERIOD_Request_MS          25


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();
    QPalette palette3=ui->Label_Board_Info->palette();
    QPalette palette4=ui->Packet_Info->palette();
    palette.setColor(ui->label->foregroundRole(), Qt::white);
    ui->label->setPalette(palette);

    palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
    ui->label_2->setPalette(palette2);
    palette3.setColor(ui->Label_Board_Info->foregroundRole(), Qt::white);
    ui->Label_Board_Info->setPalette(palette3);
    ui->groupBox_2->setForegroundRole(QPalette::Light);

    palette4.setColor(ui->Packet_Info->foregroundRole(),Qt::white);
    ui->Packet_Info->setPalette(palette4);



    timer=new QTimer();
    serial=new SerialPort();
    protocol=new QSerialProtocol();
    protocol->setID(ID_DEVICE);
    Logic=new MainLogic();

    _setting_data_UI->data1=ui->ADC_Kp_CH1;
    _setting_data_UI->data2=ui->ADC_Kp_CH2;
    _setting_data_UI->data3=ui->ADC_Kp_CH3;
    _setting_data_UI->data4=ui->ADC_Kp_CH4;
    _setting_data_UI->data5=ui->ADC_Kp_CH5;
    _setting_data_UI->data6=ui->ADC_Kp_CH6;

    _setting_data_UI->data7=ui->ADC_offset1;
    _setting_data_UI->data8=ui->ADC_offset2;
    _setting_data_UI->data9=ui->ADC_offset3;
    _setting_data_UI->data10=ui->ADC_offset4;
    _setting_data_UI->data11=ui->ADC_offset5;
    _setting_data_UI->data12=ui->ADC_offset6;


    timer->setInterval(PERIOD_Request_MS);
    connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));

    connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this,SLOT(serialReceived(QByteArray)));

    connect(protocol,SIGNAL(receivedMsgFrame(protocol_Frame_data*)),
            this,SLOT(receivedMsg(protocol_Frame_data*)));

    connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
            serial,SLOT(transmitData(QByteArray*)));

    connect(Logic,&MainLogic::Update_Packet_Count,this,&MainWindow::Update_UI_Packet_Info);

    Update_ComBoBox_Config();
    Link_event_UI();

}

 void MainWindow:: Update_UI_Packet_Info(double data)
{
    QString out_String="Packets:"+QString::number(data);
    ui->Packet_Info->setText(out_String);

}
void MainWindow:: serialReceived(QByteArray data)
{
    protocol->serialProtReceived(data);
    //qDebug()<<data;
}

void MainWindow:: receivedMsg(protocol_Frame_data* data)
{
    Logic->Parse_Data_RX((Command_Board)data->command,data);
    switch(data->command)
    {
        case Get_Board:
         {
           Logic->Update_UI((Command_Board)data->command,ui->Label_Board_Info);
           break;
         }
        case Get_main_data:
        {
            Logic->Update_UI((Command_Board)data->command,ui->MainData);
            break;
        }
        case Get_setting_peripheral_board:
        {



            Logic->Update_UI(ui->ADC_Kp_CH1,ui->ADC_Kp_CH2,ui->ADC_Kp_CH3,ui->ADC_Kp_CH4,ui->ADC_Kp_CH5,ui->ADC_Kp_CH6,
                             ui->ADC_offset1,
                             ui->ADC_offset2,
                             ui->ADC_offset3,
                             ui->ADC_offset4,
                             ui->ADC_offset5,
                             ui->ADC_offset6 );
            break;
        }


    }
}

void MainWindow:: Request_Board_Tim(void)
{
    //qDebug()<<"test timer!!";
    if(status_start_request)
    {
        //function Request RS_Connected
        Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }

    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            Logic->Request_Get_MainData(protocol);
            break;
        }
    }

    count_request++;
    if(count_request>2)
        count_request=2;
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box->addItem(info.portName());
         }
}

void MainWindow::Link_event_UI()
{
    connect(ui->on_ConnectButton, SIGNAL (released()), this, SLOT (Connect_Btn()));
    connect(ui->Btn_Default, SIGNAL (released()), this, SLOT (Set_Default_Btn()));
    connect(ui->Btn_Flash, SIGNAL (released()), this, SLOT (Set_Flash_Btn()));
    connect(ui->Btn_Transmit, SIGNAL (released()), this, SLOT (Transmit_Console()));
    connect(ui->Btn_Write, SIGNAL (released()), this, SLOT (Set_Setting_Btn()));
    connect(ui->Btn_read, SIGNAL (released()), this, SLOT (Set_Read_Settings_Btn()));

    connect(ui->Name_update_btn,SIGNAL (released()), this, SLOT (update_NamePort()));

    connect(ui->btn_testSelf, SIGNAL (released()), this, SLOT (Test_Btn_Self()));
    connect(ui->btn_test_Brd, SIGNAL (released()), this, SLOT (Test_Btn_BRD()));
    connect(ui->Btn_test_data, SIGNAL (released()), this, SLOT (Test_Btn_Data()));

}





void MainWindow::Set_Setting_Btn()
{

    float dataSettings[6]={0};
    float dataSettings2[6]={0};
    dataSettings[0]=ui->ADC_Kp_CH1->text().toFloat();
    dataSettings[1]=ui->ADC_Kp_CH2->text().toFloat();
    dataSettings[2]=ui->ADC_Kp_CH3->text().toFloat();
    dataSettings[3]=ui->ADC_Kp_CH4->text().toFloat();
    dataSettings[4]=ui->ADC_Kp_CH5->text().toFloat();
    dataSettings[5]=ui->ADC_Kp_CH6->text().toFloat();

    dataSettings2[0]=ui->ADC_offset1->text().toFloat();
    dataSettings2[1]=ui->ADC_offset2->text().toFloat();
    dataSettings2[2]=ui->ADC_offset3->text().toFloat();
    dataSettings2[3]=ui->ADC_offset4->text().toFloat();
    dataSettings2[4]=ui->ADC_offset5->text().toFloat();
    dataSettings2[5]=ui->ADC_offset6->text().toFloat();

    Logic->Request_Set_Adc_Kp_Config_Peripheral(protocol,dataSettings,sizeof(dataSettings));
    QMessageBox msgBox;
    msgBox.setText("Settings Set!!!");

    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int res = msgBox.exec();
    if (res == QMessageBox::Ok) //нажата кнопка Ok
         Logic->Request_Set_Adc_Offset_Config_Peripheral(protocol,dataSettings2,sizeof(dataSettings2));


}

void MainWindow::Set_Flash_Btn()
{

    Logic-> Request_Flash(protocol);
    QMessageBox::information(this,"Flash","Settings Write To Flash!!!");
    Logic->Request_RS_Connect(protocol);

}

void MainWindow::Set_Default_Btn()
{

    Logic->Request_default_Seettings_Board(protocol);
     QMessageBox::information(this,"Flash","Default Flash Configurated!!!");
}

void MainWindow::Set_Read_Settings_Btn()
{

    Logic->Request_read_Settings_Peripheral(protocol);

}

void MainWindow::Transmit_Console()
{

}

void MainWindow::update_NamePort()
{
    ui->SpeedSerialPort_Box->clear();
    ui->NameSerialPort_Box->clear();
   Update_ComBoBox_Config();
}

void MainWindow::Connect_Btn()
{
     count_ConnectButton=!count_ConnectButton;
     if(count_ConnectButton)
     {
         ui->on_ConnectButton->setText("Connect");
         ui->NameSerialPort_Box->setEnabled(false);
         ui->SpeedSerialPort_Box->setEnabled(false);
         Logic->Set_ID(ID_DEVICE);

         bool status_Config=serial->ConnectPort(ui->NameSerialPort_Box->currentText(),
                                                (ui->SpeedSerialPort_Box->currentText()).toLong());

         if(status_Config)
         {
             QMessageBox::information(this,"Connection!!","Connection OK!");




         }
         else
         {
             QMessageBox::information(this," Not Connection!!","Connection error!");
             ui->NameSerialPort_Box->setEnabled(true);
             ui->SpeedSerialPort_Box->setEnabled(true);
         }

     }
     else
     {
         ui->on_ConnectButton->setText("Disconnected");
         timer->stop();
         ui->checkBox_EN->setChecked(false);
         status_start_request=false;
         serial->DisconnectedPort();
         protocol->QSerialProtocol_ClearBuffer();
         QMessageBox::information(this,"Connector Status","Close Port!!");
         ui->NameSerialPort_Box->setEnabled(true);
         ui->SpeedSerialPort_Box->setEnabled(true);

     }

}



void MainWindow:: Test_Btn_Self(void)
{
    Logic->Request_RS_Connect(protocol);
}
void MainWindow:: Test_Btn_BRD(void)
{
    Logic->Request_Get_Config_Board(protocol);
}
void MainWindow:: Test_Btn_Data(void)
{
     Logic->Request_Get_MainData(protocol);
}





void MainWindow::on_checkBox_EN_stateChanged(int arg1)
{
    if(arg1==2)
    {
         status_start_request=true;
        timer->start();
        //timeWork->start();
    }
    else
    {
         status_start_request=false;
       timer->stop();
    }
}


void MainWindow::on_pushButton_clicked()
{
    Logic->Request_read_Settings_Peripheral(protocol);
}


void MainWindow::on_pushButton_2_clicked()
{
    float dataSettings[6]={0};
    float dataSettings2[6]={0};
    dataSettings[0]=0.1;
    dataSettings[1]=0.2;
    dataSettings[2]=0.3;
    dataSettings[3]=0.4;
    dataSettings[4]=0.5;
    dataSettings[5]=0.6;

    dataSettings2[0]=0.6;
    dataSettings2[1]=0.6;
    dataSettings2[2]=0.6;
    dataSettings2[3]=0.6;
    dataSettings2[4]=0.6;
    dataSettings2[5]=0.6;

    Logic->Request_Set_Adc_Kp_Config_Peripheral(protocol,dataSettings,sizeof(dataSettings));
    QMessageBox msgBox;
    msgBox.setText("Settings Set!!!");

    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int res = msgBox.exec();
    if (res == QMessageBox::Ok) //нажата кнопка Ok
         Logic->Request_Set_Adc_Offset_Config_Peripheral(protocol,dataSettings2,sizeof(dataSettings2));
}

