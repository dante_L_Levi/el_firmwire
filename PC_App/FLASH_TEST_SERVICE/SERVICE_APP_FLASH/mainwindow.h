#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "style_classes/window_styles.h"
#include "stdint.h"
#include "QTimer"
#include "style_classes/window_styles.h"
#include "SerialPort/QSerialProtocol.h"
#include "SerialPort/SerialPort.h"
#include "MainLogic.h"

#define ID_DEVICE       0xF703

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
   void Set_Setting_Btn();
   void Set_Flash_Btn();
   void Set_Default_Btn();
   void Set_Read_Settings_Btn();

   void Transmit_Console(void);

   void update_NamePort(void);
   void Connect_Btn(void);


   void Test_Btn_Self(void);
   void Test_Btn_BRD(void);
   void Test_Btn_Data(void);

   void serialReceived(QByteArray data);
   void receivedMsg(protocol_Frame_data* data);
   void Request_Board_Tim();

   void Update_UI_Packet_Info(double data);



   void on_checkBox_EN_stateChanged(int arg1);

   void on_pushButton_clicked();

   void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    Window_Styles *stylesw;
    bool count_ConnectButton=false;

    MainLogic *Logic;

    SerialPort *serial;
    QSerialProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;

    Settings_UI_def *_setting_data_UI;

    void Link_event_UI(void);
    void Update_ComBoBox_Config(void);

};
#endif // MAINWINDOW_H
