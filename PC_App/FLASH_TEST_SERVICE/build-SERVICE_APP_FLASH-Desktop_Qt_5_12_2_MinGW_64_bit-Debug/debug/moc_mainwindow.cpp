/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../SERVICE_APP_FLASH/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[22];
    char stringdata0[338];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "Set_Setting_Btn"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 13), // "Set_Flash_Btn"
QT_MOC_LITERAL(4, 42, 15), // "Set_Default_Btn"
QT_MOC_LITERAL(5, 58, 21), // "Set_Read_Settings_Btn"
QT_MOC_LITERAL(6, 80, 16), // "Transmit_Console"
QT_MOC_LITERAL(7, 97, 15), // "update_NamePort"
QT_MOC_LITERAL(8, 113, 11), // "Connect_Btn"
QT_MOC_LITERAL(9, 125, 13), // "Test_Btn_Self"
QT_MOC_LITERAL(10, 139, 12), // "Test_Btn_BRD"
QT_MOC_LITERAL(11, 152, 13), // "Test_Btn_Data"
QT_MOC_LITERAL(12, 166, 14), // "serialReceived"
QT_MOC_LITERAL(13, 181, 4), // "data"
QT_MOC_LITERAL(14, 186, 11), // "receivedMsg"
QT_MOC_LITERAL(15, 198, 20), // "protocol_Frame_data*"
QT_MOC_LITERAL(16, 219, 17), // "Request_Board_Tim"
QT_MOC_LITERAL(17, 237, 21), // "Update_UI_Packet_Info"
QT_MOC_LITERAL(18, 259, 27), // "on_checkBox_EN_stateChanged"
QT_MOC_LITERAL(19, 287, 4), // "arg1"
QT_MOC_LITERAL(20, 292, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(21, 314, 23) // "on_pushButton_2_clicked"

    },
    "MainWindow\0Set_Setting_Btn\0\0Set_Flash_Btn\0"
    "Set_Default_Btn\0Set_Read_Settings_Btn\0"
    "Transmit_Console\0update_NamePort\0"
    "Connect_Btn\0Test_Btn_Self\0Test_Btn_BRD\0"
    "Test_Btn_Data\0serialReceived\0data\0"
    "receivedMsg\0protocol_Frame_data*\0"
    "Request_Board_Tim\0Update_UI_Packet_Info\0"
    "on_checkBox_EN_stateChanged\0arg1\0"
    "on_pushButton_clicked\0on_pushButton_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x08 /* Private */,
       3,    0,  100,    2, 0x08 /* Private */,
       4,    0,  101,    2, 0x08 /* Private */,
       5,    0,  102,    2, 0x08 /* Private */,
       6,    0,  103,    2, 0x08 /* Private */,
       7,    0,  104,    2, 0x08 /* Private */,
       8,    0,  105,    2, 0x08 /* Private */,
       9,    0,  106,    2, 0x08 /* Private */,
      10,    0,  107,    2, 0x08 /* Private */,
      11,    0,  108,    2, 0x08 /* Private */,
      12,    1,  109,    2, 0x08 /* Private */,
      14,    1,  112,    2, 0x08 /* Private */,
      16,    0,  115,    2, 0x08 /* Private */,
      17,    1,  116,    2, 0x08 /* Private */,
      18,    1,  119,    2, 0x08 /* Private */,
      20,    0,  122,    2, 0x08 /* Private */,
      21,    0,  123,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   13,
    QMetaType::Void, 0x80000000 | 15,   13,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   13,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Set_Setting_Btn(); break;
        case 1: _t->Set_Flash_Btn(); break;
        case 2: _t->Set_Default_Btn(); break;
        case 3: _t->Set_Read_Settings_Btn(); break;
        case 4: _t->Transmit_Console(); break;
        case 5: _t->update_NamePort(); break;
        case 6: _t->Connect_Btn(); break;
        case 7: _t->Test_Btn_Self(); break;
        case 8: _t->Test_Btn_BRD(); break;
        case 9: _t->Test_Btn_Data(); break;
        case 10: _t->serialReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 11: _t->receivedMsg((*reinterpret_cast< protocol_Frame_data*(*)>(_a[1]))); break;
        case 12: _t->Request_Board_Tim(); break;
        case 13: _t->Update_UI_Packet_Info((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->on_checkBox_EN_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_pushButton_clicked(); break;
        case 16: _t->on_pushButton_2_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
