/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QVBoxLayout *verticalLayout_2;
    QLabel *Label_Board_Info;
    QLabel *Packet_Info;
    QTabWidget *MainFill;
    QWidget *tab_2;
    QLabel *MainData;
    QCheckBox *checkBox_EN;
    QWidget *tab;
    QVBoxLayout *verticalLayout;
    QTextEdit *textEdit;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *Transmit_data_Fill;
    QPushButton *Btn_Transmit;
    QWidget *Settings_Flash;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *Btn_read;
    QPushButton *Btn_Write;
    QPushButton *Btn_Flash;
    QPushButton *Btn_Default;
    QHBoxLayout *horizontalLayout_21;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_7;
    QLineEdit *ADC_Kp_CH1;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_5;
    QLineEdit *ADC_Kp_CH2;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_6;
    QLineEdit *ADC_Kp_CH3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_4;
    QLineEdit *ADC_Kp_CH4;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_8;
    QLineEdit *ADC_Kp_CH5;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_3;
    QLineEdit *ADC_Kp_CH6;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_9;
    QLineEdit *ADC_offset1;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_10;
    QLineEdit *ADC_offset2;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_11;
    QLineEdit *ADC_offset3;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_12;
    QLineEdit *ADC_offset4;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_13;
    QLineEdit *ADC_offset5;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_14;
    QLineEdit *ADC_offset6;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_22;
    QVBoxLayout *verticalLayout_6;
    QPushButton *btn_testSelf;
    QPushButton *btn_test_Brd;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *Btn_test_data;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(585, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_7 = new QVBoxLayout(centralwidget);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_5->addWidget(groupBox_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        Label_Board_Info = new QLabel(centralwidget);
        Label_Board_Info->setObjectName(QString::fromUtf8("Label_Board_Info"));

        verticalLayout_2->addWidget(Label_Board_Info);

        Packet_Info = new QLabel(centralwidget);
        Packet_Info->setObjectName(QString::fromUtf8("Packet_Info"));

        verticalLayout_2->addWidget(Packet_Info);


        horizontalLayout_5->addLayout(verticalLayout_2);


        verticalLayout_7->addLayout(horizontalLayout_5);

        MainFill = new QTabWidget(centralwidget);
        MainFill->setObjectName(QString::fromUtf8("MainFill"));
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        MainData = new QLabel(tab_2);
        MainData->setObjectName(QString::fromUtf8("MainData"));
        MainData->setGeometry(QRect(10, 20, 531, 451));
        QFont font;
        font.setPointSize(14);
        font.setItalic(true);
        MainData->setFont(font);
        checkBox_EN = new QCheckBox(tab_2);
        checkBox_EN->setObjectName(QString::fromUtf8("checkBox_EN"));
        checkBox_EN->setGeometry(QRect(20, 10, 151, 20));
        MainFill->addTab(tab_2, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout = new QVBoxLayout(tab);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        textEdit = new QTextEdit(tab);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout->addWidget(textEdit);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        Transmit_data_Fill = new QLineEdit(tab);
        Transmit_data_Fill->setObjectName(QString::fromUtf8("Transmit_data_Fill"));

        horizontalLayout_6->addWidget(Transmit_data_Fill);

        Btn_Transmit = new QPushButton(tab);
        Btn_Transmit->setObjectName(QString::fromUtf8("Btn_Transmit"));

        horizontalLayout_6->addWidget(Btn_Transmit);


        verticalLayout->addLayout(horizontalLayout_6);

        MainFill->addTab(tab, QString());
        Settings_Flash = new QWidget();
        Settings_Flash->setObjectName(QString::fromUtf8("Settings_Flash"));
        verticalLayout_5 = new QVBoxLayout(Settings_Flash);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalLayout_14->setSizeConstraint(QLayout::SetFixedSize);
        horizontalSpacer = new QSpacerItem(298, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        Btn_read = new QPushButton(Settings_Flash);
        Btn_read->setObjectName(QString::fromUtf8("Btn_read"));

        horizontalLayout_7->addWidget(Btn_read);

        Btn_Write = new QPushButton(Settings_Flash);
        Btn_Write->setObjectName(QString::fromUtf8("Btn_Write"));

        horizontalLayout_7->addWidget(Btn_Write);

        Btn_Flash = new QPushButton(Settings_Flash);
        Btn_Flash->setObjectName(QString::fromUtf8("Btn_Flash"));

        horizontalLayout_7->addWidget(Btn_Flash);

        Btn_Default = new QPushButton(Settings_Flash);
        Btn_Default->setObjectName(QString::fromUtf8("Btn_Default"));

        horizontalLayout_7->addWidget(Btn_Default);


        horizontalLayout_14->addLayout(horizontalLayout_7);


        verticalLayout_5->addLayout(horizontalLayout_14);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        horizontalLayout_21->setSizeConstraint(QLayout::SetFixedSize);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_7 = new QLabel(Settings_Flash);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_12->addWidget(label_7);

        ADC_Kp_CH1 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH1->setObjectName(QString::fromUtf8("ADC_Kp_CH1"));

        horizontalLayout_12->addWidget(ADC_Kp_CH1);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_5 = new QLabel(Settings_Flash);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_10->addWidget(label_5);

        ADC_Kp_CH2 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH2->setObjectName(QString::fromUtf8("ADC_Kp_CH2"));

        horizontalLayout_10->addWidget(ADC_Kp_CH2);


        verticalLayout_3->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_6 = new QLabel(Settings_Flash);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_11->addWidget(label_6);

        ADC_Kp_CH3 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH3->setObjectName(QString::fromUtf8("ADC_Kp_CH3"));

        horizontalLayout_11->addWidget(ADC_Kp_CH3);


        verticalLayout_3->addLayout(horizontalLayout_11);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_4 = new QLabel(Settings_Flash);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_9->addWidget(label_4);

        ADC_Kp_CH4 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH4->setObjectName(QString::fromUtf8("ADC_Kp_CH4"));

        horizontalLayout_9->addWidget(ADC_Kp_CH4);


        verticalLayout_3->addLayout(horizontalLayout_9);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_8 = new QLabel(Settings_Flash);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_13->addWidget(label_8);

        ADC_Kp_CH5 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH5->setObjectName(QString::fromUtf8("ADC_Kp_CH5"));

        horizontalLayout_13->addWidget(ADC_Kp_CH5);


        verticalLayout_3->addLayout(horizontalLayout_13);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_3 = new QLabel(Settings_Flash);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_8->addWidget(label_3);

        ADC_Kp_CH6 = new QLineEdit(Settings_Flash);
        ADC_Kp_CH6->setObjectName(QString::fromUtf8("ADC_Kp_CH6"));

        horizontalLayout_8->addWidget(ADC_Kp_CH6);


        verticalLayout_3->addLayout(horizontalLayout_8);


        horizontalLayout_21->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_9 = new QLabel(Settings_Flash);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_15->addWidget(label_9);

        ADC_offset1 = new QLineEdit(Settings_Flash);
        ADC_offset1->setObjectName(QString::fromUtf8("ADC_offset1"));

        horizontalLayout_15->addWidget(ADC_offset1);


        verticalLayout_4->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_10 = new QLabel(Settings_Flash);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_16->addWidget(label_10);

        ADC_offset2 = new QLineEdit(Settings_Flash);
        ADC_offset2->setObjectName(QString::fromUtf8("ADC_offset2"));

        horizontalLayout_16->addWidget(ADC_offset2);


        verticalLayout_4->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_11 = new QLabel(Settings_Flash);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_17->addWidget(label_11);

        ADC_offset3 = new QLineEdit(Settings_Flash);
        ADC_offset3->setObjectName(QString::fromUtf8("ADC_offset3"));

        horizontalLayout_17->addWidget(ADC_offset3);


        verticalLayout_4->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_12 = new QLabel(Settings_Flash);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_18->addWidget(label_12);

        ADC_offset4 = new QLineEdit(Settings_Flash);
        ADC_offset4->setObjectName(QString::fromUtf8("ADC_offset4"));

        horizontalLayout_18->addWidget(ADC_offset4);


        verticalLayout_4->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_13 = new QLabel(Settings_Flash);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_19->addWidget(label_13);

        ADC_offset5 = new QLineEdit(Settings_Flash);
        ADC_offset5->setObjectName(QString::fromUtf8("ADC_offset5"));

        horizontalLayout_19->addWidget(ADC_offset5);


        verticalLayout_4->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_14 = new QLabel(Settings_Flash);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_20->addWidget(label_14);

        ADC_offset6 = new QLineEdit(Settings_Flash);
        ADC_offset6->setObjectName(QString::fromUtf8("ADC_offset6"));

        horizontalLayout_20->addWidget(ADC_offset6);


        verticalLayout_4->addLayout(horizontalLayout_20);


        horizontalLayout_21->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_21);

        MainFill->addTab(Settings_Flash, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        horizontalLayout_22 = new QHBoxLayout(tab_3);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        btn_testSelf = new QPushButton(tab_3);
        btn_testSelf->setObjectName(QString::fromUtf8("btn_testSelf"));

        verticalLayout_6->addWidget(btn_testSelf);

        btn_test_Brd = new QPushButton(tab_3);
        btn_test_Brd->setObjectName(QString::fromUtf8("btn_test_Brd"));

        verticalLayout_6->addWidget(btn_test_Brd);

        pushButton = new QPushButton(tab_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_6->addWidget(pushButton);

        pushButton_2 = new QPushButton(tab_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_6->addWidget(pushButton_2);

        Btn_test_data = new QPushButton(tab_3);
        Btn_test_data->setObjectName(QString::fromUtf8("Btn_test_data"));

        verticalLayout_6->addWidget(Btn_test_data);


        horizontalLayout_22->addLayout(verticalLayout_6);

        horizontalSpacer_2 = new QSpacerItem(388, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_2);

        MainFill->addTab(tab_3, QString());

        verticalLayout_7->addWidget(MainFill);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        MainFill->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        Label_Board_Info->setText(QString());
        Packet_Info->setText(QString());
        MainData->setText(QString());
        checkBox_EN->setText(QApplication::translate("MainWindow", "\320\276\320\277\321\200\320\260\321\210\320\270\320\262\320\260\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265 ", nullptr));
        MainFill->setTabText(MainFill->indexOf(tab_2), QApplication::translate("MainWindow", "Data", nullptr));
        Btn_Transmit->setText(QApplication::translate("MainWindow", "Transmit", nullptr));
        MainFill->setTabText(MainFill->indexOf(tab), QApplication::translate("MainWindow", "Console", nullptr));
        Btn_read->setText(QApplication::translate("MainWindow", "Read", nullptr));
        Btn_Write->setText(QApplication::translate("MainWindow", "Write", nullptr));
        Btn_Flash->setText(QApplication::translate("MainWindow", "Flash", nullptr));
        Btn_Default->setText(QApplication::translate("MainWindow", "Default", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Channel1 Kp:", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Channel2 Kp:", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "Channel3 Kp:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Channel4 Kp:", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Channel5 Kp:", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Channel6 Kp:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Ch1 offset:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Ch2 offset:", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Ch3 offset:", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Ch4 offset:", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "Ch5 offset:", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Ch6 offset:", nullptr));
        MainFill->setTabText(MainFill->indexOf(Settings_Flash), QApplication::translate("MainWindow", "Settings_Flash", nullptr));
        btn_testSelf->setText(QApplication::translate("MainWindow", "Test Connect", nullptr));
        btn_test_Brd->setText(QApplication::translate("MainWindow", "Board Test", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "ReadSettings", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "SetSettings", nullptr));
        Btn_test_data->setText(QApplication::translate("MainWindow", "Test Main Data", nullptr));
        MainFill->setTabText(MainFill->indexOf(tab_3), QApplication::translate("MainWindow", "\320\242\320\265\321\201\321\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
