#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"

#define PERIOD_Request_MS          50
#define ID_DEVICE       0xF111

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Init_styles();


    timer=new QTimer();
    serial=new SerialPort();
    protocol=new QSProtocol();
    protocol->setID(ID_DEVICE);

    Logic=new MainLogic();

    timer->setInterval(PERIOD_Request_MS);
    connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));

    connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this,SLOT(serialReceived(QByteArray)));

    connect(protocol,SIGNAL(receivedMsg(SProtocol_message*)),
            this,SLOT(receivedMsg(SProtocol_message*)));

    connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
            serial,SLOT(transmitData(QByteArray*)));

    connect(Logic, SIGNAL(Update_Packet_Count(double)),SLOT(Update_UI_Packet_Info(double)));

    Link_event_UI();
    Update_ComBoBox_Config();


}

void MainWindow:: Init_styles()
{
    this->setWindowTitle("ISCV REV1.1");
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles

    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();
    QPalette palette3=ui->Label_Board_Info->palette();
    QPalette palette4=ui->Packet_Info->palette();
    palette.setColor(ui->label->foregroundRole(), Qt::white);
    palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
    palette3.setColor(ui->Label_Board_Info->foregroundRole(), Qt::white);
    palette4.setColor(ui->Packet_Info->foregroundRole(), Qt::white);
    ui->label->setPalette(palette);


    ui->label_2->setPalette(palette2);

    ui->Label_Board_Info->setPalette(palette3);
    ui->groupBox_2->setForegroundRole(QPalette::Light);

    ui->Packet_Info->setPalette(palette4);
}

MainWindow::~MainWindow()
{
    delete ui;
}

 void MainWindow:: Update_UI_Packet_Info(double data)
 {
     ui->Packet_Info->setText("Packet:"+QString::number(data));
 }

void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box->addItem(info.portName());
         }
}

void MainWindow:: Request_Board_Tim()
{
    qDebug()<<"test timer!!";
    if(status_start_request)
    {
        //function Request RS_Connected
       // Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }

    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            Logic->Request_Get_MainData(protocol);
            break;
        }
    }

    count_request++;
    if(count_request>2)
        count_request=2;
}

void MainWindow::Set_Btn_Gpio1()
{
    gpio_state[0]=!gpio_state[0];
    Logic->Request_Set_Gpio(protocol,gpio_state);
}

void MainWindow::Set_Btn_Gpio2()
{
    gpio_state[1]=!gpio_state[1];
    Logic->Request_Set_Gpio(protocol,gpio_state);
}

void MainWindow:: update_NamePort(void)
{
    ui->SpeedSerialPort_Box->clear();
    ui->NameSerialPort_Box->clear();
   Update_ComBoBox_Config();
}

void MainWindow:: Connect_Btn(void)
{
    count_ConnectButton=!count_ConnectButton;
    if(count_ConnectButton)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);
        Logic->Set_ID(ID_DEVICE);

        bool status_Config=serial->ConnectPort(ui->NameSerialPort_Box->currentText(),
                                               (ui->SpeedSerialPort_Box->currentText()).toLong());

        if(status_Config)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");




        }
        else
        {
            QMessageBox::information(this," Not Connection!!","Connection error!");
            ui->NameSerialPort_Box->setEnabled(true);
            ui->SpeedSerialPort_Box->setEnabled(true);
        }

    }
    else
    {
        ui->on_ConnectButton->setText("Disconnected");
        timer->stop();
        ui->checkBox_EN->setChecked(false);
        status_start_request=false;
        serial->DisconnectedPort();

        QMessageBox::information(this,"Connector Status","Close Port!!");
        ui->NameSerialPort_Box->setEnabled(true);
        ui->SpeedSerialPort_Box->setEnabled(true);

    }
}

void MainWindow:: serialReceived(QByteArray data)
{
    qDebug()<<data;
    protocol->serialReceived(data);

}

void MainWindow:: receivedMsg(SProtocol_message* data)
{
    Logic->Parse_Data_RX((sprotocol_commands)data->command,data);
    switch(data->command)
    {
        case SP_CMD_FIRMWIRE:
         {
            qDebug()<<"[update]";
           Logic->Update_UI((sprotocol_commands)data->command,ui->Label_Board_Info);
           break;
         }
        case SP_CMD_GET_INPUTS:
        {
            qDebug()<<"[update]";
            Logic->Update_UI((sprotocol_commands)data->command,ui->MainData);
            break;
        }
        case SP_CMD_GET_SETTINGS:
        {

            break;
        }


    }
}


void MainWindow:: on_checkBox_EN_stateChanged(int arg1)
{
    if(arg1==2)
    {
         status_start_request=true;
        timer->start();
        //timeWork->start();
    }
    else
    {
         status_start_request=false;
       timer->stop();
    }
}

void MainWindow:: Request_Test_MainData()
{
    Logic->Request_Get_MainData(protocol);
}
void MainWindow:: Requrst_test_Firmwire()
{
    Logic->Request_Get_Config_Board(protocol);
}

void MainWindow:: Link_event_UI(void)
{
    connect(ui->on_ConnectButton, SIGNAL (released()), this, SLOT (Connect_Btn()));
    connect(ui->Name_update_btn,SIGNAL (released()), this, SLOT (update_NamePort()));

    connect(ui->Btn_1,SIGNAL (released()),this,SLOT(Set_Btn_Gpio1()));
    connect(ui->Btn_testgpio1,SIGNAL (released()),this,SLOT(Set_Btn_Gpio1()));

    connect(ui->Btn_2,SIGNAL (released()),this,SLOT(Set_Btn_Gpio2()));
    connect(ui->Btn_testgpio2,SIGNAL (released()),this,SLOT(Set_Btn_Gpio2()));


    connect(ui->Btn_testMainData,SIGNAL (released()),this,SLOT(Request_Test_MainData()));
    connect(ui->Btn_test_firmwire,SIGNAL (released()),this,SLOT(Requrst_test_Firmwire()));

    connect(ui->checkBox_EN,SIGNAL(stateChanged(int)),this,
           SLOT(on_checkBox_EN_stateChanged(int)));

}



