#include "MainLogic.h"

MainLogic::MainLogic(QObject *parent) : QObject(parent)
{

}

void MainLogic::Set_ID(uint16_t Id_dev)
{
    ID=Id_dev;
}

void MainLogic::Parse_Data_RX(sprotocol_commands cmd, SProtocol_message *data)
{
    switch(cmd)
    {
        case SP_CMD_FIRMWIRE:
        {
        recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
        //memcpy(recieve_data1,data->data,data->length);
        count_Packet++;
        Update_Model(recieve_data1);
        qDebug()<<"[parse]";
            break;
        }

        case SP_CMD_GET_INPUTS:
        {
        recieve_data2=reinterpret_cast<Recieve_data2_Model_MainData*>(data->data);
        count_Packet++;
        Update_Model(recieve_data2);
        qDebug()<<"[parse]";
            break;
        }

        case SP_CMD_GET_SETTINGS:
        {
            recieve_data3=reinterpret_cast<Recieve_data3_Model_Settings*>(data->data);

            Update_Model(recieve_data3);
            qDebug()<<"[parse]";
            break;
        }
    default:
        break;

    }
     count_Packet++;
     emit Update_Packet_Count(count_Packet);
}

void MainLogic::Update_UI(sprotocol_commands cmd, QLabel *data)
{
    switch(cmd)
    {
        case SP_CMD_FIRMWIRE:
        {
        qDebug()<<"[update]";
        char dataName[8]={0};
        int i=0;
        while(_mainData.Board_name[i]!=' ')
        {
            dataName[i]=_mainData.Board_name[i];
            i++;
        }
        //qDebug()<<"NAME:"<<dataName;
       QString dataBoard=QString(
                       "%1:%4\n"
                       "%2:%5\n"
                       "%3:%6\n"


                    ).arg("ID")
                    .arg("Version")
                    .arg("NameBoard")
                    .arg(_mainData.Id_Recieve)
               .arg((const char*)_mainData.firmwire)
               .arg(dataName);

                data->setText(dataBoard) ;

    break;
            break;
        }
        case SP_CMD_GET_INPUTS:
        {
        qDebug()<<"[update]";
        QString dataMain=QString(
                    "%1:\t\t%6\n"
                    "%2:\t\t%7\n"
                    "%3:\t%8\n"
                    "%4:\t%9\n"
                    "%5:\t\t\t%10\n"


                    ).arg(DataName[0])
                     .arg(DataName[1])
                     .arg(DataName[2])
                     .arg(DataName[3])
                     .arg("Mode")
                    .arg(_mainData.led_status[0])
                    .arg(_mainData.led_status[1])
                    .arg(_mainData.btn_status[0])
                    .arg(_mainData.btn_status[1])
                    .arg(DataNameStatusWork[_mainData.mode])



                ;
        data->setText(dataMain);
            break;
        }


        default:
            break;
    }
}

void MainLogic::Request_RS_Connect(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_CONNECT;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Get_Config_Board(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_FIRMWIRE;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Get_MainData(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_GET_INPUTS;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Set_Enable_Control(QSProtocol *protocol, bool status)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_SET_MODE;
    uint8_t dt=status;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void MainLogic::Request_Set_Gpio(QSProtocol *protocol, bool status[])
{
    uint8_t data=0x00;
    status[0]?(data|=1):(data&=0xFE);
    status[1]?(data|=2):(data&=0xFD);
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_SEND_INPUTS;

    sendMsg.length=sizeof(data);
    sendMsg.data=&data;
    protocol->transmitData(&sendMsg);


}

void MainLogic::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->firmwire,8);
    memcpy(_mainData.Board_name,data->Name,8);
    _mainData.Id_Recieve=data->ID_temp;
}

void MainLogic::Update_Model(Recieve_data2_Model_MainData *data)
{
    data->_out_state.fields.gpio1?( _mainData.led_status[0]=true):
                                    ( _mainData.led_status[0]=false);

    data->_out_state.fields.gpio2?( _mainData.led_status[1]=true):
                                    ( _mainData.led_status[1]=false);


    data->_input_state.fields.sw1?(_mainData.btn_status[0]=true):
        (_mainData.btn_status[0]=false);

    data->_input_state.fields.sw2?(_mainData.btn_status[1]=true):
        (_mainData.btn_status[1]=false);


    _mainData.mode=data->mode;


}

void MainLogic::Update_Model(Recieve_data3_Model_Settings *data)
{
    qDebug()<<data;
}
