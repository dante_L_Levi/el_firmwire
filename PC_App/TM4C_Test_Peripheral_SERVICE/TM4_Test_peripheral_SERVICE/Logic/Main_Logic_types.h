#ifndef MAIN_LOGIC_TYPES_H
#define MAIN_LOGIC_TYPES_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QStringList"

typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;







#pragma pack(push, 1)

typedef union
{
    struct
    {
        uint8_t sw1:1;
        uint8_t sw2:1;
        uint8_t reserved:6;

    }fields;
    uint8_t All;
}gpio_input_def;


typedef union
{
    struct
    {
        uint8_t gpio1:1;
        uint8_t gpio2:1;
        uint8_t reserved:6;

    }fields;
    uint8_t All;
}gpio_out_def;


typedef struct
{
    uint16_t ID_temp;
    uint8_t Name[8];
    uint8_t firmwire[8];

}Recieve_data1_Model_DataBoard;


typedef struct
{
    gpio_input_def  _input_state;
    gpio_out_def    _out_state;
    _mode_system_type mode;

}Recieve_data2_Model_MainData;


typedef struct
{


}Recieve_data3_Model_Settings;



typedef struct
{

    uint8_t firmwire[8];
    char Board_name[8];
    uint16_t Id_Recieve;

    bool led_status[2];
    bool btn_status[2];


    mode_system_e mode;

    gpio_out_def    Set_state_out;

}Model_MainData_def;

#pragma pack(pop)

#endif // MAIN_LOGIC_TYPES_H
