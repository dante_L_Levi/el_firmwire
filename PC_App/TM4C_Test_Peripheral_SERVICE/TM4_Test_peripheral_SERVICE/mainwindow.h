#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "stdint.h"
#include "QTimer"
#include "SerialPort/SerialPort.h"
#include "Protocol/SProtocol_connect.h"
#include "Protocol/qsprotocol.h"
#include "Protocol/SProtocol_struct.h"

#include "style_classes/window_styles.h"

#include "Logic/MainLogic.h"
#include "Logic/Main_Logic_types.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void Set_Btn_Gpio1(void);
    void Set_Btn_Gpio2(void);


    void update_NamePort(void);
    void Connect_Btn(void);

    void serialReceived(QByteArray data);
    void receivedMsg(SProtocol_message* data);

    void Request_Board_Tim();

    void on_checkBox_EN_stateChanged(int arg1);

    void Request_Test_MainData();
    void Requrst_test_Firmwire();

    void Update_UI_Packet_Info(double data);



private:
    Ui::MainWindow *ui;
    QTimer *timer;
    bool count_ConnectButton=false;
    SerialPort *serial;
    QSProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;
    bool gpio_state[2]={false,false};
    MainLogic* Logic;
    Window_Styles *stylesw;

private:
    void Link_event_UI(void);
    void Update_ComBoBox_Config(void);
    void Init_styles(void);
};
#endif // MAINWINDOW_H
