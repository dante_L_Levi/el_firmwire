/*
 * SProtocol_struct.h
 *
 *  Created on: 19 feb. 2020
 *      Author: peresaliak
 */

#ifndef SPROTOCOL_SPROTOCOL_STRUCT_H_
#define SPROTOCOL_SPROTOCOL_STRUCT_H_

#include <stdint.h>

const uint8_t SProtocolMaster = 1;
const uint8_t SProtocolSlave =  0;

#pragma pack(push, 1)
typedef union{
    struct SPpack{
        uint16_t        ID:15;
        uint16_t        master:1;
        uint8_t         len;
        uint8_t         cmd;
        uint8_t         payload[255];
        uint16_t        crc;
    }pack;
    uint8_t dataPack[261];
}SProtocolSendPack;
#pragma pack(pop)

#endif /* SPROTOCOL_SPROTOCOL_STRUCT_H_ */
