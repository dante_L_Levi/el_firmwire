QT       += core gui serialport printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Logic/MainLogic.cpp \
    Protocol/qsprotocol.cpp \
    SerialPort/SerialPort.cpp \
    main.cpp \
    mainwindow.cpp \
    style_classes/window_styles.cpp

HEADERS += \
    Logic/MainLogic.h \
    Logic/Main_Logic_types.h \
    Protocol/SProtocol_connect.h \
    Protocol/SProtocol_struct.h \
    Protocol/qsprotocol.h \
    SerialPort/SerialPort.h \
    mainwindow.h \
    style_classes/window_styles.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource_data.qrc
