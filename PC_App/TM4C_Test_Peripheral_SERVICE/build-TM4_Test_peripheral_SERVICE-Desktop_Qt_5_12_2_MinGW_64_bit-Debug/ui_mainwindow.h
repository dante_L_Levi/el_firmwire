/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QVBoxLayout *verticalLayout_2;
    QLabel *Label_Board_Info;
    QLabel *Packet_Info;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_6;
    QLabel *MainData;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBox_EN;
    QPushButton *Btn_1;
    QPushButton *Btn_2;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QWidget *tab_3;
    QPushButton *Btn_testgpio1;
    QPushButton *Btn_testgpio2;
    QPushButton *Btn_test_firmwire;
    QPushButton *Btn_testMainData;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(610, 600);
        MainWindow->setMinimumSize(QSize(610, 600));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_5->addWidget(groupBox_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        Label_Board_Info = new QLabel(centralwidget);
        Label_Board_Info->setObjectName(QString::fromUtf8("Label_Board_Info"));

        verticalLayout_2->addWidget(Label_Board_Info);

        Packet_Info = new QLabel(centralwidget);
        Packet_Info->setObjectName(QString::fromUtf8("Packet_Info"));

        verticalLayout_2->addWidget(Packet_Info);


        horizontalLayout_5->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout_5);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_6 = new QHBoxLayout(tab);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        MainData = new QLabel(tab);
        MainData->setObjectName(QString::fromUtf8("MainData"));
        MainData->setMinimumSize(QSize(421, 441));
        QFont font;
        font.setPointSize(14);
        font.setItalic(true);
        MainData->setFont(font);

        horizontalLayout_6->addWidget(MainData);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        checkBox_EN = new QCheckBox(tab);
        checkBox_EN->setObjectName(QString::fromUtf8("checkBox_EN"));

        verticalLayout->addWidget(checkBox_EN);

        Btn_1 = new QPushButton(tab);
        Btn_1->setObjectName(QString::fromUtf8("Btn_1"));

        verticalLayout->addWidget(Btn_1);

        Btn_2 = new QPushButton(tab);
        Btn_2->setObjectName(QString::fromUtf8("Btn_2"));

        verticalLayout->addWidget(Btn_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_6->addLayout(verticalLayout);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        Btn_testgpio1 = new QPushButton(tab_3);
        Btn_testgpio1->setObjectName(QString::fromUtf8("Btn_testgpio1"));
        Btn_testgpio1->setGeometry(QRect(10, 80, 93, 28));
        Btn_testgpio2 = new QPushButton(tab_3);
        Btn_testgpio2->setObjectName(QString::fromUtf8("Btn_testgpio2"));
        Btn_testgpio2->setGeometry(QRect(10, 120, 93, 28));
        Btn_test_firmwire = new QPushButton(tab_3);
        Btn_test_firmwire->setObjectName(QString::fromUtf8("Btn_test_firmwire"));
        Btn_test_firmwire->setGeometry(QRect(10, 40, 93, 28));
        Btn_testMainData = new QPushButton(tab_3);
        Btn_testMainData->setObjectName(QString::fromUtf8("Btn_testMainData"));
        Btn_testMainData->setGeometry(QRect(10, 160, 93, 28));
        tabWidget->addTab(tab_3, QString());

        verticalLayout_3->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        Label_Board_Info->setText(QString());
        Packet_Info->setText(QString());
        MainData->setText(QString());
        checkBox_EN->setText(QApplication::translate("MainWindow", "\320\236\320\277\321\200\320\260\321\210\320\270\320\262\320\260\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265", nullptr));
        Btn_1->setText(QApplication::translate("MainWindow", "GPIO1", nullptr));
        Btn_2->setText(QApplication::translate("MainWindow", "GPIO2", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Main Data", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Service Peripheral", nullptr));
        Btn_testgpio1->setText(QApplication::translate("MainWindow", "Test Btn1", nullptr));
        Btn_testgpio2->setText(QApplication::translate("MainWindow", "Test Btn2", nullptr));
        Btn_test_firmwire->setText(QApplication::translate("MainWindow", "Test Firmwire", nullptr));
        Btn_testMainData->setText(QApplication::translate("MainWindow", "Test MainData", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Testing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
