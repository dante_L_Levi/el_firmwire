/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../TM4_Test_peripheral_SERVICE/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[16];
    char stringdata0[236];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "Set_Btn_Gpio1"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 13), // "Set_Btn_Gpio2"
QT_MOC_LITERAL(4, 40, 15), // "update_NamePort"
QT_MOC_LITERAL(5, 56, 11), // "Connect_Btn"
QT_MOC_LITERAL(6, 68, 14), // "serialReceived"
QT_MOC_LITERAL(7, 83, 4), // "data"
QT_MOC_LITERAL(8, 88, 11), // "receivedMsg"
QT_MOC_LITERAL(9, 100, 18), // "SProtocol_message*"
QT_MOC_LITERAL(10, 119, 17), // "Request_Board_Tim"
QT_MOC_LITERAL(11, 137, 27), // "on_checkBox_EN_stateChanged"
QT_MOC_LITERAL(12, 165, 4), // "arg1"
QT_MOC_LITERAL(13, 170, 21), // "Request_Test_MainData"
QT_MOC_LITERAL(14, 192, 21), // "Requrst_test_Firmwire"
QT_MOC_LITERAL(15, 214, 21) // "Update_UI_Packet_Info"

    },
    "MainWindow\0Set_Btn_Gpio1\0\0Set_Btn_Gpio2\0"
    "update_NamePort\0Connect_Btn\0serialReceived\0"
    "data\0receivedMsg\0SProtocol_message*\0"
    "Request_Board_Tim\0on_checkBox_EN_stateChanged\0"
    "arg1\0Request_Test_MainData\0"
    "Requrst_test_Firmwire\0Update_UI_Packet_Info"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    1,   73,    2, 0x08 /* Private */,
       8,    1,   76,    2, 0x08 /* Private */,
      10,    0,   79,    2, 0x08 /* Private */,
      11,    1,   80,    2, 0x08 /* Private */,
      13,    0,   83,    2, 0x08 /* Private */,
      14,    0,   84,    2, 0x08 /* Private */,
      15,    1,   85,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, 0x80000000 | 9,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    7,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Set_Btn_Gpio1(); break;
        case 1: _t->Set_Btn_Gpio2(); break;
        case 2: _t->update_NamePort(); break;
        case 3: _t->Connect_Btn(); break;
        case 4: _t->serialReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 5: _t->receivedMsg((*reinterpret_cast< SProtocol_message*(*)>(_a[1]))); break;
        case 6: _t->Request_Board_Tim(); break;
        case 7: _t->on_checkBox_EN_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->Request_Test_MainData(); break;
        case 9: _t->Requrst_test_Firmwire(); break;
        case 10: _t->Update_UI_Packet_Info((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
