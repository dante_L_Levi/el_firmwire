/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Project_SERVICE_ISCV_re1_1/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[29];
    char stringdata0[436];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "Set_Setting_Btn"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 13), // "Set_Flash_Btn"
QT_MOC_LITERAL(4, 42, 15), // "Set_Default_Btn"
QT_MOC_LITERAL(5, 58, 21), // "Set_Read_Settings_Btn"
QT_MOC_LITERAL(6, 80, 15), // "update_NamePort"
QT_MOC_LITERAL(7, 96, 11), // "Connect_Btn"
QT_MOC_LITERAL(8, 108, 13), // "Test_Btn_Self"
QT_MOC_LITERAL(9, 122, 12), // "Test_Btn_BRD"
QT_MOC_LITERAL(10, 135, 13), // "Test_Btn_Data"
QT_MOC_LITERAL(11, 149, 14), // "serialReceived"
QT_MOC_LITERAL(12, 164, 4), // "data"
QT_MOC_LITERAL(13, 169, 11), // "receivedMsg"
QT_MOC_LITERAL(14, 181, 20), // "protocol_Frame_data*"
QT_MOC_LITERAL(15, 202, 17), // "Request_Board_Tim"
QT_MOC_LITERAL(16, 220, 27), // "on_checkBox_EN_stateChanged"
QT_MOC_LITERAL(17, 248, 4), // "arg1"
QT_MOC_LITERAL(18, 253, 21), // "Update_UI_Packet_Info"
QT_MOC_LITERAL(19, 275, 29), // "Enable_Test_Mode_stateChanged"
QT_MOC_LITERAL(20, 305, 12), // "Click_Ctrl_M"
QT_MOC_LITERAL(21, 318, 14), // "Click_Ctrl_LED"
QT_MOC_LITERAL(22, 333, 12), // "Click_Ctrl_R"
QT_MOC_LITERAL(23, 346, 14), // "Click_Ctrl_ENA"
QT_MOC_LITERAL(24, 361, 14), // "Click_Ctrl_ENB"
QT_MOC_LITERAL(25, 376, 14), // "Set_Slider_DAC"
QT_MOC_LITERAL(26, 391, 5), // "value"
QT_MOC_LITERAL(27, 397, 14), // "Set_Slider_PWM"
QT_MOC_LITERAL(28, 412, 23) // "on_pushButton_3_clicked"

    },
    "MainWindow\0Set_Setting_Btn\0\0Set_Flash_Btn\0"
    "Set_Default_Btn\0Set_Read_Settings_Btn\0"
    "update_NamePort\0Connect_Btn\0Test_Btn_Self\0"
    "Test_Btn_BRD\0Test_Btn_Data\0serialReceived\0"
    "data\0receivedMsg\0protocol_Frame_data*\0"
    "Request_Board_Tim\0on_checkBox_EN_stateChanged\0"
    "arg1\0Update_UI_Packet_Info\0"
    "Enable_Test_Mode_stateChanged\0"
    "Click_Ctrl_M\0Click_Ctrl_LED\0Click_Ctrl_R\0"
    "Click_Ctrl_ENA\0Click_Ctrl_ENB\0"
    "Set_Slider_DAC\0value\0Set_Slider_PWM\0"
    "on_pushButton_3_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x08 /* Private */,
       3,    0,  130,    2, 0x08 /* Private */,
       4,    0,  131,    2, 0x08 /* Private */,
       5,    0,  132,    2, 0x08 /* Private */,
       6,    0,  133,    2, 0x08 /* Private */,
       7,    0,  134,    2, 0x08 /* Private */,
       8,    0,  135,    2, 0x08 /* Private */,
       9,    0,  136,    2, 0x08 /* Private */,
      10,    0,  137,    2, 0x08 /* Private */,
      11,    1,  138,    2, 0x08 /* Private */,
      13,    1,  141,    2, 0x08 /* Private */,
      15,    0,  144,    2, 0x08 /* Private */,
      16,    1,  145,    2, 0x08 /* Private */,
      18,    1,  148,    2, 0x08 /* Private */,
      19,    1,  151,    2, 0x08 /* Private */,
      20,    0,  154,    2, 0x08 /* Private */,
      21,    0,  155,    2, 0x08 /* Private */,
      22,    0,  156,    2, 0x08 /* Private */,
      23,    0,  157,    2, 0x08 /* Private */,
      24,    0,  158,    2, 0x08 /* Private */,
      25,    1,  159,    2, 0x08 /* Private */,
      27,    1,  162,    2, 0x08 /* Private */,
      28,    0,  165,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   12,
    QMetaType::Void, 0x80000000 | 14,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Set_Setting_Btn(); break;
        case 1: _t->Set_Flash_Btn(); break;
        case 2: _t->Set_Default_Btn(); break;
        case 3: _t->Set_Read_Settings_Btn(); break;
        case 4: _t->update_NamePort(); break;
        case 5: _t->Connect_Btn(); break;
        case 6: _t->Test_Btn_Self(); break;
        case 7: _t->Test_Btn_BRD(); break;
        case 8: _t->Test_Btn_Data(); break;
        case 9: _t->serialReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 10: _t->receivedMsg((*reinterpret_cast< protocol_Frame_data*(*)>(_a[1]))); break;
        case 11: _t->Request_Board_Tim(); break;
        case 12: _t->on_checkBox_EN_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->Update_UI_Packet_Info((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->Enable_Test_Mode_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->Click_Ctrl_M(); break;
        case 16: _t->Click_Ctrl_LED(); break;
        case 17: _t->Click_Ctrl_R(); break;
        case 18: _t->Click_Ctrl_ENA(); break;
        case 19: _t->Click_Ctrl_ENB(); break;
        case 20: _t->Set_Slider_DAC((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->Set_Slider_PWM((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->on_pushButton_3_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
