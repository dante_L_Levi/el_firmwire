/********************************************************************************
** Form generated from reading UI file 'formgraph.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMGRAPH_H
#define UI_FORMGRAPH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_formgraph
{
public:
    QVBoxLayout *verticalLayout;
    QCustomPlot *CUSTOMEPLOT;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *comboBoxSize_2;
    QPushButton *pushButtonReset_2;
    QPushButton *pushButtonPause_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonColor_2;
    QComboBox *comboBoxGraph_2;
    QPushButton *pushButtonAdd_2;
    QPushButton *pushButtonAxis_2;

    void setupUi(QWidget *formgraph)
    {
        if (formgraph->objectName().isEmpty())
            formgraph->setObjectName(QString::fromUtf8("formgraph"));
        formgraph->resize(753, 442);
        verticalLayout = new QVBoxLayout(formgraph);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CUSTOMEPLOT = new QCustomPlot(formgraph);
        CUSTOMEPLOT->setObjectName(QString::fromUtf8("CUSTOMEPLOT"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CUSTOMEPLOT->sizePolicy().hasHeightForWidth());
        CUSTOMEPLOT->setSizePolicy(sizePolicy);
        CUSTOMEPLOT->setMinimumSize(QSize(0, 0));

        verticalLayout->addWidget(CUSTOMEPLOT);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        comboBoxSize_2 = new QComboBox(formgraph);
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->addItem(QString());
        comboBoxSize_2->setObjectName(QString::fromUtf8("comboBoxSize_2"));

        horizontalLayout_2->addWidget(comboBoxSize_2);

        pushButtonReset_2 = new QPushButton(formgraph);
        pushButtonReset_2->setObjectName(QString::fromUtf8("pushButtonReset_2"));

        horizontalLayout_2->addWidget(pushButtonReset_2);

        pushButtonPause_2 = new QPushButton(formgraph);
        pushButtonPause_2->setObjectName(QString::fromUtf8("pushButtonPause_2"));

        horizontalLayout_2->addWidget(pushButtonPause_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButtonColor_2 = new QPushButton(formgraph);
        pushButtonColor_2->setObjectName(QString::fromUtf8("pushButtonColor_2"));

        horizontalLayout_2->addWidget(pushButtonColor_2);

        comboBoxGraph_2 = new QComboBox(formgraph);
        comboBoxGraph_2->setObjectName(QString::fromUtf8("comboBoxGraph_2"));
        comboBoxGraph_2->setEditable(false);

        horizontalLayout_2->addWidget(comboBoxGraph_2);

        pushButtonAdd_2 = new QPushButton(formgraph);
        pushButtonAdd_2->setObjectName(QString::fromUtf8("pushButtonAdd_2"));

        horizontalLayout_2->addWidget(pushButtonAdd_2);

        pushButtonAxis_2 = new QPushButton(formgraph);
        pushButtonAxis_2->setObjectName(QString::fromUtf8("pushButtonAxis_2"));

        horizontalLayout_2->addWidget(pushButtonAxis_2);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(formgraph);

        comboBoxSize_2->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(formgraph);
    } // setupUi

    void retranslateUi(QWidget *formgraph)
    {
        formgraph->setWindowTitle(QApplication::translate("formgraph", "Form", nullptr));
        comboBoxSize_2->setItemText(0, QApplication::translate("formgraph", "1", nullptr));
        comboBoxSize_2->setItemText(1, QApplication::translate("formgraph", "2", nullptr));
        comboBoxSize_2->setItemText(2, QApplication::translate("formgraph", "5", nullptr));
        comboBoxSize_2->setItemText(3, QApplication::translate("formgraph", "10", nullptr));
        comboBoxSize_2->setItemText(4, QApplication::translate("formgraph", "20", nullptr));
        comboBoxSize_2->setItemText(5, QApplication::translate("formgraph", "50", nullptr));
        comboBoxSize_2->setItemText(6, QApplication::translate("formgraph", "100", nullptr));
        comboBoxSize_2->setItemText(7, QApplication::translate("formgraph", "200", nullptr));
        comboBoxSize_2->setItemText(8, QApplication::translate("formgraph", "500", nullptr));
        comboBoxSize_2->setItemText(9, QApplication::translate("formgraph", "1000", nullptr));
        comboBoxSize_2->setItemText(10, QApplication::translate("formgraph", "2000", nullptr));
        comboBoxSize_2->setItemText(11, QApplication::translate("formgraph", "5000", nullptr));
        comboBoxSize_2->setItemText(12, QApplication::translate("formgraph", "10000", nullptr));

        pushButtonReset_2->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201", nullptr));
        pushButtonPause_2->setText(QApplication::translate("formgraph", "\320\237\320\260\321\203\320\267\320\260", nullptr));
        pushButtonColor_2->setText(QApplication::translate("formgraph", "\320\241\320\261\321\200\320\276\321\201\320\270\321\202\321\214 \321\206\320\262\320\265\321\202\320\260", nullptr));
        pushButtonAdd_2->setText(QApplication::translate("formgraph", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        pushButtonAxis_2->setText(QApplication::translate("formgraph", "--", nullptr));
    } // retranslateUi

};

namespace Ui {
    class formgraph: public Ui_formgraph {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMGRAPH_H
