#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "QDebug"


#define PERIOD_Request_MS          100

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("ISCV REV1.1");
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles

    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();
    QPalette palette3=ui->Label_Board_Info->palette();
    QPalette palette4=ui->Packet_Info->palette();
    palette.setColor(ui->label->foregroundRole(), Qt::white);
    palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
    palette3.setColor(ui->Label_Board_Info->foregroundRole(), Qt::white);
    palette4.setColor(ui->Packet_Info->foregroundRole(), Qt::white);
    ui->label->setPalette(palette);


    ui->label_2->setPalette(palette2);

    ui->Label_Board_Info->setPalette(palette3);
    ui->groupBox_2->setForegroundRole(QPalette::Light);

    ui->Packet_Info->setPalette(palette4);



    timer=new QTimer();
    serial=new SerialPort();
    protocol=new QSerialProtocol();
    protocol->setID(ID_DEVICE);
    Logic=new ISCV_LOGIC();

    timer->setInterval(PERIOD_Request_MS);
    connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));

    connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
            this,SLOT(serialReceived(QByteArray)));

    connect(protocol,SIGNAL(receivedMsgFrame(protocol_Frame_data*)),
            this,SLOT(receivedMsg(protocol_Frame_data*)));

    connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
            serial,SLOT(transmitData(QByteArray*)));

    connect(Logic,&ISCV_LOGIC::Update_Packet_Count,this,&MainWindow::Update_UI_Packet_Info);

    Update_ComBoBox_Config();
    Link_event_UI();



}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow:: serialReceived(QByteArray data)
{
    protocol->serialProtReceived(data);
}



void MainWindow:: receivedMsg(protocol_Frame_data* data)
{
    Logic->Parse_Data_RX((Command_Board)data->command,data);
    switch(data->command)
    {
        case Get_Board:
         {
           Logic->Update_UI((Command_Board)data->command,ui->Label_Board_Info);
           break;
         }
        case Get_main_data:
        {
            Logic->Update_UI((Command_Board)data->command,ui->MainData);
            break;
        }
        case Get_setting_peripheral_board:
        {




            break;
        }


    }
}
void MainWindow:: Request_Board_Tim()
{
    //qDebug()<<"test timer!!";
    if(status_start_request)
    {
        //function Request RS_Connected
        Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }

    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            Logic->Request_Get_MainData(protocol);
            break;
        }
    }

    count_request++;
    if(count_request>2)
        count_request=2;
}

void MainWindow:: Update_UI_Packet_Info(double data)
{
    QString out_String="Packets:"+QString::number(data);
    ui->Packet_Info->setText(out_String);
}

void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box->addItem(info.portName());
         }
}

void MainWindow::Link_event_UI()
{
    connect(ui->on_ConnectButton, SIGNAL (released()), this, SLOT (Connect_Btn()));
    connect(ui->Btn_Default, SIGNAL (released()), this, SLOT (Set_Default_Btn()));
    connect(ui->Btn_Flash, SIGNAL (released()), this, SLOT (Set_Flash_Btn()));
    connect(ui->Btn_Write, SIGNAL (released()), this, SLOT (Set_Setting_Btn()));
    connect(ui->Btn_read, SIGNAL (released()), this, SLOT (Set_Read_Settings_Btn()));

    connect(ui->Name_update_btn,SIGNAL (released()), this, SLOT (update_NamePort()));

    connect(ui->btn_testSelf, SIGNAL (released()), this, SLOT (Test_Btn_Self()));
    connect(ui->btn_test_Brd, SIGNAL (released()), this, SLOT (Test_Btn_BRD()));
    connect(ui->Btn_test_data, SIGNAL (released()), this, SLOT (Test_Btn_Data()));


    connect(ui->Btn_ctrlLED,SIGNAL(released()),this,SLOT(Click_Ctrl_LED()));
    connect(ui->Btn_ctrlM,SIGNAL(released()),this,SLOT(Click_Ctrl_M()));
    connect(ui->Btn_ctrlR,SIGNAL(released()),this,SLOT(Click_Ctrl_R()));
    connect(ui->Btn_ENA,SIGNAL(released()),this,SLOT(Click_Ctrl_ENA()));
    connect(ui->Btn_ENB,SIGNAL(released()),this,SLOT(Click_Ctrl_ENB()));

    connect(ui->checkBox_EN,SIGNAL(stateChanged(int)),this,
           SLOT(on_checkBox_EN_stateChanged(int)));


    connect(ui->checkBox_ENABLE_Test,SIGNAL(stateChanged(int)),this,
           SLOT(Enable_Test_Mode_stateChanged(int)));

    connect(ui->Slider_DAC,SIGNAL(valueChanged(int)),this,SLOT(Set_Slider_DAC(int)));
    connect(ui->Slider_PWM,SIGNAL(valueChanged(int)),this,SLOT(Set_Slider_PWM(int)));


}

void MainWindow:: Connect_Btn(void)
{
    count_ConnectButton=!count_ConnectButton;
    if(count_ConnectButton)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);
        Logic->Set_ID(ID_DEVICE);

        bool status_Config=serial->ConnectPort(ui->NameSerialPort_Box->currentText(),
                                               (ui->SpeedSerialPort_Box->currentText()).toLong());

        if(status_Config)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");




        }
        else
        {
            QMessageBox::information(this," Not Connection!!","Connection error!");
            ui->NameSerialPort_Box->setEnabled(true);
            ui->SpeedSerialPort_Box->setEnabled(true);
        }

    }
    else
    {
        ui->on_ConnectButton->setText("Disconnected");
        timer->stop();
        ui->checkBox_EN->setChecked(false);
        status_start_request=false;
        serial->DisconnectedPort();
        protocol->QSerialProtocol_ClearBuffer();
        QMessageBox::information(this,"Connector Status","Close Port!!");
        ui->NameSerialPort_Box->setEnabled(true);
        ui->SpeedSerialPort_Box->setEnabled(true);

    }
}




void MainWindow::update_NamePort()
{
    ui->SpeedSerialPort_Box->clear();
    ui->NameSerialPort_Box->clear();
   Update_ComBoBox_Config();
}

void MainWindow::on_checkBox_EN_stateChanged(int arg1)
{
    if(arg1==2)
    {
         status_start_request=true;
        timer->start();
        //timeWork->start();
    }
    else
    {
         status_start_request=false;
       timer->stop();
    }
}



void MainWindow::Enable_Test_Mode_stateChanged(int arg1)
{

        arg1==2?(Enable_Test_Mode=true):(Enable_Test_Mode=false);
        ui->Btn_ENA->setEnabled(Enable_Test_Mode);
        ui->Btn_ENB->setEnabled(Enable_Test_Mode);
        ui->Btn_ctrlLED->setEnabled(Enable_Test_Mode);
        ui->Btn_ctrlM->setEnabled(Enable_Test_Mode);
        ui->Btn_ctrlR->setEnabled(Enable_Test_Mode);

        ui->Slider_DAC->setEnabled(Enable_Test_Mode);
        ui->Slider_PWM->setEnabled(Enable_Test_Mode);
        Logic->Request_Set_Enable_Control(protocol,Enable_Test_Mode);

}

void MainWindow:: Set_Setting_Btn()
{

}


void MainWindow:: Set_Flash_Btn()
{

}
void MainWindow:: Set_Default_Btn()
{

}
void MainWindow:: Set_Read_Settings_Btn()
{

}





void MainWindow:: Test_Btn_Self(void)
{
    Logic->Request_RS_Connect(protocol);
}
void MainWindow:: Test_Btn_BRD(void)
{
    Logic->Request_Get_Config_Board(protocol);
}
void MainWindow:: Test_Btn_Data(void)
{
    Logic->Request_Get_MainData(protocol);
}

void MainWindow:: Set_Slider_DAC(int value)
{
    Start_Stop_Timer(false);
    if(value>=0)
    {
        Logic->Request_DAC_value(protocol,uint16_t(value));
        ui->value_DAC->setText(QString::number(value));
    }
    Start_Stop_Timer(true);
}
void MainWindow:: Set_Slider_PWM(int value)
{
    Start_Stop_Timer(false);
    if(value>=0)
    {
        Logic->Request_PWM_Duty_Set(protocol,uint16_t(value));
        ui->value_PWM->setText(QString::number(value));
    }
    Start_Stop_Timer(true);
}


void MainWindow:: Set_helper_Gpio(uint8_t index)
{
    switch (index)
    {
        case 0:
        {
            button_states[index]=!button_states[index];
            button_states[index]?(gpio_state|=0x01):(gpio_state&=~0x01);
            break;
        }

        case 1:
        {
            button_states[index]=!button_states[index];
            button_states[index]?(gpio_state|=0x02):(gpio_state&=~0x02);
            break;
        }

        case 2:
        {
            button_states[index]=!button_states[index];
            button_states[index]?(gpio_state|=0x04):(gpio_state&=~0x04);
            break;
        }

        case 3:
        {
            button_states[index]=!button_states[index];
            button_states[index]?(gpio_state|=0x08):(gpio_state&=~0x08);
            break;
        }

        case 4:
        {
            button_states[index]=!button_states[index];
            button_states[index]?(gpio_state|=0x10):(gpio_state&=~0x10);
            break;
        }

        }
}


void MainWindow:: Click_Ctrl_M()
{

    Set_helper_Gpio(0);
    Start_Stop_Timer(false);
    Logic->Request_Set_GPIO_state(protocol,gpio_state);
    Start_Stop_Timer(true);
}
void MainWindow:: Click_Ctrl_LED()
{

     Set_helper_Gpio(1);
     Start_Stop_Timer(false);
     Logic->Request_Set_GPIO_state(protocol,gpio_state);
     Start_Stop_Timer(true);
}
void MainWindow:: Click_Ctrl_R()
{

    Set_helper_Gpio(2);
    Start_Stop_Timer(false);
    Logic->Request_Set_GPIO_state(protocol,gpio_state);
    Start_Stop_Timer(true);
}
void MainWindow:: Click_Ctrl_ENA()
{

    Set_helper_Gpio(3);
    Start_Stop_Timer(false);
    Logic->Request_Set_GPIO_state(protocol,gpio_state);
    Start_Stop_Timer(true);
}
void MainWindow:: Click_Ctrl_ENB()
{

    Set_helper_Gpio(4);
    Start_Stop_Timer(false);
    Logic->Request_Set_GPIO_state(protocol,gpio_state);
    Start_Stop_Timer(true);

}

void MainWindow:: Start_Stop_Timer(bool status)
{
    status?(timer->start()):(timer->stop());
}



void MainWindow::on_pushButton_3_clicked()
{
    uint16_t randData=rand()%200;
    Logic->Request_DAC_value(protocol,uint16_t(randData));
    ui->value_DAC->setText(QString::number(randData));
}

