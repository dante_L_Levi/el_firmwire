#include "ISCV_LOGIC.h"
#include "QDebug"
#include "ISCV_Types.h"
ISCV_LOGIC::ISCV_LOGIC(QWidget *parent) : QWidget(parent)
{

}

void ISCV_LOGIC::Set_ID(uint16_t Id_dev)
{
    ID=Id_dev;
}

void ISCV_LOGIC::Request_Flash(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_cmd_flash;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Parse_Data_RX(Command_Board cmd, protocol_Frame_data *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
        recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
        //memcpy(recieve_data1,data->data,data->length);
        count_Packet++;
        Update_Model(recieve_data1);
            break;
        }

        case Get_main_data:
        {
        recieve_data2=reinterpret_cast<Recieve_data2_adc1_s*>(data->data);
        count_Packet++;
        Update_Model(recieve_data2);
            break;
        }

        case Get_setting_peripheral_board:
        {
            recieve_data3=reinterpret_cast<FLASH_SETTINGS_Data_s*>(data->data);

            Update_Model(recieve_data3);
            break;
        }
    default:
        break;

    }
     count_Packet++;
     emit Update_Packet_Count(count_Packet);
     //qDebug()<<count_Packet;
}

void ISCV_LOGIC::Update_UI(Command_Board cmd, QLabel *data)
{
    switch(cmd)
    {
        case Get_Board:
        {
        char dataName[8]={0};
        int i=0;
        while(_mainData.Board_name[i]!=' ')
        {
            dataName[i]=_mainData.Board_name[i];
            i++;
        }
        //qDebug()<<"NAME:"<<dataName;
       QString dataBoard=QString(
                       "%1:%4\n"
                       "%2:%5\n"
                       "%3:%6\n"


                    ).arg("ID")
                    .arg("Version")
                    .arg("NameBoard")
                    .arg(_mainData.Id_Recieve)
               .arg((const char*)_mainData.firmwire)
               .arg(dataName);

                data->setText(dataBoard) ;

    break;
            break;
        }
        case Get_main_data:
        {
        QString dataMain=QString(
                    "%1:\t\t\t\t%18 mV\n"
                    "%2:\t\t\t\t%19 mV\n"
                    "%3:\t\t\t\t%20 mA\n"
                    "%4:\t\t\t\t%21 mA\n"
                    "%5:\t\t\t\t\t%22 V\n"
                    "%6:\t\t\t\t\t%23 V\n"
                    "%7:\t\t%24 \n"
                    "%8:\t\t\t%25 \n"
                    "%9:\t\t\t\t%26 \n"
                    "%10:\t\t\t\t%27 \n"
                    "%11:\t\t\t\t%28 \n"
                    "%12:\t\t\t\t\t%29 \n"
                    "%13:\t\t\t\t\t%30 \n"
                    "%14:\t\t\t\t\t%31 \n"
                    "%15:\t\t\t\t\t\t%32 \n"
                    "%16:\t\t\t\t\t\t%33 \n"
                    "%17:\t\t\t\t\t\t%34 \n"

                    ).arg(DataName[0])
                     .arg(DataName[1])
                     .arg(DataName[2])
                     .arg(DataName[3])
                     .arg(DataName[4])
                     .arg(DataName[5])
                     .arg(DataName[6])
                    .arg(DataName[7])
                    .arg(DataName[8])
                    .arg(DataName[9])
                    .arg(DataName[10])
                    .arg(DataName[11])
                    .arg(DataName[12])
                    .arg(DataName[13])
                    .arg(DataName[14])
                    .arg(DataName[15])
                    .arg(DataName[16])
                    .arg(_mainData.Voltage[INDEX_INPUT])
                    .arg(_mainData.Voltage[INDEX_OUT])
                    .arg(_mainData.Current[INDEX_INPUT])
                    .arg(_mainData.Current[INDEX_OUT])
                    .arg(_mainData.Voltage_Battery[INDEX_GB1])
                    .arg(_mainData.Voltage_Battery[INDEX_GB2])
                .arg((DataNameStatusWork[_mainData._status]))
                .arg(DataNameStatusRegulator[_mainData._status_regulator])
                .arg(_mainData.Get_Gpio_state_Data.fields.CTRL_M)
                .arg(_mainData.Get_Gpio_state_Data.fields.CTRL_LED)
                .arg(_mainData.Get_Gpio_state_Data.fields.CTRL_R)
                .arg(_mainData.Get_Gpio_state_Data.fields.ENA)
                .arg(_mainData.Get_Gpio_state_Data.fields.ENB)
                .arg(_mainData.extern_in_D)
                .arg(_mainData.temperature_sense)
                .arg(_mainData.pwmDuty[0])
                .arg(_mainData.Recieve_Dacvalue.Threshold_Protected)




                ;
        data->setText(dataMain);
            break;
        }


        default:
            break;
    }
}

void ISCV_LOGIC::Update_UI(QLineEdit *data1, QLineEdit *data2, QLineEdit *data3, QLineEdit *data4, QLineEdit *data5, QLineEdit *data6, QLineEdit *data7, QLineEdit *data8, QLineEdit *data9, QLineEdit *data10, QLineEdit *data11, QLineEdit *data12, QLineEdit *data13, QLineEdit *data14, QLineEdit *data15, QLineEdit *data16)
{
    data1->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.Kd));
    data2->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.Ki));
    data3->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.Kp));
    data4->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.MAX));
    data5->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.MIN));
    data6->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.REF));
    data7->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.Saturation_N));
    data8->setText(QString::number(_mainData.dataSettings.DataSettingsPID[0].fields.Saturation_P));


    data9->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.Kd));
    data10->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.Ki));
    data11->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.Kp));
    data12->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.MAX));
    data13->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.MIN));
    data14->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.REF));
    data15->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.Saturation_N));
    data16->setText(QString::number(_mainData.dataSettings.DataSettingsPID[1].fields.Saturation_P));



}

void ISCV_LOGIC::Request_RS_Connect(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Connect;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Get_Config_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_Board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Get_MainData(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_main_data;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_default_Seettings_Board(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Default_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_read_Settings_Peripheral(QSerialProtocol *protocol)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Get_setting_peripheral_board;
    uint8_t dt[32]={0};
    dt[0]=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Set_PID_Voltage_Config_Peripheral(QSerialProtocol *protocol, float *data, uint8_t len)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_peripheral_board_PID_Voltage;
    uint8_t dt[32]={0};
   memcpy(dt,data,len);
   // uint8_t *dt=reinterpret_cast<uint8_t*>(data);
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Set_PID_Current_Config_Peripheral(QSerialProtocol *protocol, float *data, uint8_t len)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_setting_peripheral_board_PID_Current;
    uint8_t dt[32]={0};
   memcpy(dt,data,len);
   // uint8_t *dt=reinterpret_cast<uint8_t*>(data);
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_PWM_Duty_Set(QSerialProtocol *protocol,uint16_t data)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_PWM_Duty;
    _mainData.pwmDuty[0]=data;
    _mainData.pwmDuty[1]=data;
    uint8_t dt[32]={0};
    memcpy(dt,&_mainData.pwmDuty,sizeof(_mainData.pwmDuty));
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_DAC_value(QSerialProtocol *protocol,uint16_t Data)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_DAC_state;
    _mainData.Dacvalue.Threshold_Protected=Data;
    uint8_t dt[32]={0};
    memcpy(dt,&_mainData.Dacvalue,sizeof(_mainData.Dacvalue));
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    qDebug()<<"DAC Valuye:"<<dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Set_GPIO_state(QSerialProtocol *protocol,uint8_t data)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Gpio_state;
    _mainData.Set_state_Data.all=data;
    uint8_t dt[32]={0};
    memcpy(dt,&_mainData.Set_state_Data,sizeof(_mainData.Set_state_Data));
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ISCV_LOGIC::Request_Set_Enable_Control(QSerialProtocol *protocol,bool status)
{
    protocol_Frame_data sendMsg;
    sendMsg.command=Set_Enable_CTRL;
    uint8_t dt[32]={0};
    dt[0]=(uint8_t)status;
    sendMsg.length=sizeof(dt);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}





void ISCV_LOGIC::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->datafirmwire,8);
    memcpy(_mainData.Board_name,data->dataName,8);
    _mainData.Id_Recieve=data->ID;
}

void ISCV_LOGIC::Update_Model(Recieve_data2_adc1_s *data)
{
    _mainData.Voltage[INDEX_INPUT]=data->_ainData1.fields.output_14V;
    _mainData.Voltage[INDEX_OUT]=data->_ainData1.fields.output_28V;

    _mainData.Current[INDEX_INPUT]=data->_ainData1.fields.input_current;
    _mainData.Current[INDEX_OUT]=data->_ainData1.fields.output_current;

    _mainData.extern_in_D=data->_ainData2.fields.extern_in_D;
    _mainData.temperature_sense=data->_ainData2.fields.temperature_sense;

    _mainData.Voltage_Battery[INDEX_GB1]=data->_Battery_State.GB1;
    _mainData.Voltage_Battery[INDEX_GB2]=data->_Battery_State.GB2;

    _mainData._status=(status_work_Converter)(data->_status);
    _mainData._status_regulator=(status_type_Regulator)data->_status_regulator;

    _mainData.Get_Gpio_state_Data=data->_gpioState;

    _mainData.Recieve_Dacvalue=data->_current_DAC;



}

void ISCV_LOGIC::Update_Model(FLASH_SETTINGS_Data_s *data)
{
    for(int i=0;i<2;i++)
    {

    }
}
