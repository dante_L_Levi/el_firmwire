#ifndef ISCV_TYPES_H
#define ISCV_TYPES_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,
    Get_main_data,
    Get_setting_peripheral_board,

    Set_Enable_CTRL,
    Set_setting_peripheral_board_PID_Voltage,
    Set_setting_peripheral_board_PID_Current,
    Set_Default_setting_peripheral_board,
    Set_Gpio_state,
    Set_PWM_Duty,
    Set_DAC_state,

    Set_cmd_flash

}Command_Board;

#pragma pack(push, 1)


typedef union
{
    struct
    {
        uint16_t 	temperature_sense;
        uint16_t 	extern_in_D;

    }fields;
    uint16_t all_channel[2];

}analog2_s;

typedef union
{
    struct
    {
        int16_t 	input_current;
        int16_t 	output_current;
        int16_t 	output_28V;
        int16_t 	output_14V;

    }fields;
    int16_t all_channel[4];

}analog1_s;

typedef union
{
    struct
    {
        uint8_t CTRL_M:1;
        uint8_t CTRL_LED:1;
        uint8_t CTRL_R:1;
        uint8_t ENA:1;
        uint8_t ENB:1;
    }fields;
    uint8_t all;
}gpio_out_s;

typedef struct
{
    uint16_t outDAC;
}DAC_Value_Def;


typedef struct
{
    uint16_t 					Threshold_Protected;
    DAC_Value_Def				_DAC_Value;

}Current_Protected_def;


typedef struct
{
    uint16_t dutyA;
    uint16_t dutyB;
}pwm_value_s;

typedef struct
{
    float GB1;
    float GB2;

    float Current_charge_GB2;

}push_pull_Battery_s;

typedef enum
{
    //todo add STATUS INIT
    STATUS_INIT,
    STATUS_NORMAL,
    STATUS_NORMAL_GEN_ON,
    STATUS_NORMAL_GEN_OFF,
    STATUS_NORMAL_Charge_GEN_OFF,//перекачка энергии из GB1 в GB2
    STATUS_TEST,

    STATUS_ERROR_Input_Voltage,//необходимо обслуживание GB1
    STATUS_ERROR_OUTPUT_Voltage,////необходимо обслуживание GB2
    STATUS_ERROR_Breakage_OUT_NET,//обрыв цепи заряда
    STATUS_ERROR_Short_OUT_NET,		//кз цепи заряда
    STATUS_ERROR_Short_GB1,//КЗ банок GB1
    STATUS_ERROR_Short_GB2,//КЗ банок GB2
    STATUS_ERROR_Low_Voltage_D,//низкое напряжение на клеме D
    STATUS_ERROR_Low_Input_Voltage,
    STATUS_ERROR_High_Input_Voltage,//высокое входное напряжение

    STATUS_ERROR
}status_work_Converter;

typedef enum
{
    CV_Work=0,
    CC_Work=1,
    None_Work=2


}status_type_Regulator;


typedef struct
{
    uint16_t ID;
    uint8_t dataName[8];
    uint8_t datafirmwire[8];

}Recieve_data1_Model_DataBoard;

typedef struct
{
    analog1_s 					_ainData1;
    analog2_s					_ainData2;
    gpio_out_s					_gpioState;
    Current_Protected_def		_current_DAC;
    pwm_value_s					_pwm_Value;
    push_pull_Battery_s			_Battery_State;
    uint8_t                     _status;
    uint8_t             		_status_regulator;



}Recieve_data2_adc1_s;

typedef union
{
    struct
    {
        int16_t Kp;
        int16_t Ki;
        int16_t Kd;
        int16_t Saturation_P;
        int16_t Saturation_N;
        int16_t MAX;
        int16_t MIN;
        int16_t REF;
    }fields;
    int16_t AllData[8];


}Setting_Reg_PID;


typedef struct
{
    Setting_Reg_PID				DataSettingsPID[2];

}FLASH_SETTINGS_Data_s;


#define INDEX_INPUT 0
#define INDEX_OUT   1

#define INDEX_GB1   0
#define INDEX_GB2   1


typedef struct
{
    uint8_t firmwire[8];
    char Board_name[8];
    uint16_t Id_Recieve;

    int16_t Voltage[2];
    int16_t Current[2];

    int16_t 	temperature_sense;
    int16_t 	extern_in_D;



    float Voltage_Battery[2];

    status_work_Converter		_status;
    status_type_Regulator		_status_regulator;

    uint16_t pwmDuty[2];
    Current_Protected_def		Dacvalue;
    Current_Protected_def		Recieve_Dacvalue;
    uint16_t Dac_Threhold;

    gpio_out_s Get_Gpio_state_Data;
    gpio_out_s Set_state_Data;

    FLASH_SETTINGS_Data_s dataSettings;

}Model_MainData_def;


#pragma pack(pop)



#endif // ISCV_TYPES_H
