#ifndef ISCV_LOGIC_H
#define ISCV_LOGIC_H

#include <QObject>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QSerialPort"
#include "SerialPort/QSerialProtocol.h"
#include "ISCV_Types.h"



class ISCV_LOGIC : public QWidget
{
    Q_OBJECT
public:
    explicit ISCV_LOGIC(QWidget *parent = nullptr);

    void Set_ID(uint16_t Id_dev);
    void Request_Flash(QSerialProtocol *protocol);
    void Parse_Data_RX(Command_Board cmd,protocol_Frame_data *data);
    void Update_UI(Command_Board cmd,QLabel *data);

    void Update_UI(QLineEdit *data1,
                   QLineEdit *data2,
                   QLineEdit *data3,
                   QLineEdit *data4,
                   QLineEdit *data5,
                   QLineEdit *data6,
                   QLineEdit *data7,
                   QLineEdit *data8,
                   QLineEdit *data9,
                   QLineEdit *data10,
                   QLineEdit *data11,
                   QLineEdit *data12,
                   QLineEdit *data13,
                   QLineEdit *data14,
                   QLineEdit *data15,QLineEdit *data16);

    void Request_RS_Connect(QSerialProtocol *protocol);
    void Request_Get_Config_Board(QSerialProtocol *protocol);
    void Request_Get_MainData(QSerialProtocol *protocol);
    void Request_default_Seettings_Board(QSerialProtocol *protocol);
    void Request_read_Settings_Peripheral(QSerialProtocol *protocol);

    void Request_Set_PID_Voltage_Config_Peripheral(QSerialProtocol *protocol,
                                              float *data,uint8_t len);
    void Request_Set_PID_Current_Config_Peripheral(QSerialProtocol *protocol,
                                                  float *data,
                                                  uint8_t len);

    void Request_PWM_Duty_Set(QSerialProtocol *protocol,uint16_t data);
    void Request_DAC_value(QSerialProtocol *protocol,uint16_t Data);
    void Request_Set_GPIO_state(QSerialProtocol *protocol,uint8_t data);
    void Request_Set_Enable_Control(QSerialProtocol *protocol,bool status);





signals:
    void Update_Packet_Count(double data);

private:
    Recieve_data1_Model_DataBoard       *recieve_data1;
    Recieve_data2_adc1_s                *recieve_data2;
    FLASH_SETTINGS_Data_s               *recieve_data3;

    uint16_t ID=0;
    double count_Packet=0;

    void Update_Model(Recieve_data1_Model_DataBoard *data);
    void Update_Model(Recieve_data2_adc1_s *data);
    void Update_Model(FLASH_SETTINGS_Data_s *data);

    Model_MainData_def _mainData;

    QStringList DataName=
    {
        "VoltageInput",
        "VoltageOut",
        "CurrentInput",
        "CurrentOut",
        "GB1",
        "GB2",
        "Status Work",
        "Status Regulator",
        "Gpio-CTRL_M",
        "Gpio-CTRL_LED",
        "Gpio-CTRL_R",
        "Gpio-ENA",
        "Gpio-ENB",
        "Analog D",
        "Tem",
        "PWM",
        "DAC"

    };

    QStringList DataNameStatusWork=
    {
        "STATUS_INIT",
        "STATUS_NORMAL",
        "STATUS_NORMAL_GEN_ON",
        "STATUS_NORMAL_GEN_OFF",
        "STATUS_NORMAL_Charge_GEN_OFF",//перекачка энергии из GB1 в GB2
        "STATUS_TEST",

        "STATUS_ERROR_Input_Voltage",//необходимо обслуживание GB1
        "STATUS_ERROR_OUTPUT_Voltage",////необходимо обслуживание GB2
        "STATUS_ERROR_Breakage_OUT_NET",//обрыв цепи заряда
        "STATUS_ERROR_Short_OUT_NET",		//кз цепи заряда
        "STATUS_ERROR_Short_GB1",//КЗ банок GB1
        "STATUS_ERROR_Short_GB2",//КЗ банок GB2
        "STATUS_ERROR_Low_Voltage_D",//низкое напряжение на клеме D
        "STATUS_ERROR_Low_Input_Voltage",
        "STATUS_ERROR_High_Input_Voltage"//высокое входное напряжение

    };

    QStringList DataNameStatusRegulator=
    {
        "CV_Work",
        "CC_Work",
        "None_Work"

    };

};

#endif // ISCV_LOGIC_H
