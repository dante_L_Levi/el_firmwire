#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "style_classes/window_styles.h"
#include "stdint.h"
#include "QTimer"
#include "style_classes/window_styles.h"
#include "SerialPort/QSerialProtocol.h"
#include "SerialPort/SerialPort.h"
#include "Logic/ISCV_LOGIC.h"

#define ID_DEVICE       0xF110

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void Set_Setting_Btn();
    void Set_Flash_Btn();
    void Set_Default_Btn();
    void Set_Read_Settings_Btn();

    void update_NamePort(void);
    void Connect_Btn(void);

    void Test_Btn_Self(void);
    void Test_Btn_BRD(void);
    void Test_Btn_Data(void);

    void serialReceived(QByteArray data);
    void receivedMsg(protocol_Frame_data* data);
    void Request_Board_Tim();

    void on_checkBox_EN_stateChanged(int arg1);
    void Update_UI_Packet_Info(double data);

    void Enable_Test_Mode_stateChanged(int arg1);

    void Click_Ctrl_M();
    void Click_Ctrl_LED();
    void Click_Ctrl_R();
    void Click_Ctrl_ENA();
    void Click_Ctrl_ENB();

    void Set_Slider_DAC(int value);
    void Set_Slider_PWM(int value);

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    Window_Styles *stylesw;
    bool count_ConnectButton=false;
    bool Enable_Test_Mode=false;
    ISCV_LOGIC* Logic;
    SerialPort *serial;
    QSerialProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;
    uint8_t gpio_state=0x00;
    bool button_states[5]={0};
    void Link_event_UI(void);
    void Update_ComBoBox_Config(void);
    void Set_helper_Gpio(uint8_t index);

    void Start_Stop_Timer(bool status);

};
#endif // MAINWINDOW_H
