QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Graph_plot/formgraph.cpp \
    Logic/ISCV_LOGIC.cpp \
    SerialPort/QSerialProtocol.cpp \
    SerialPort/SerialPort.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    style_classes/window_styles.cpp

HEADERS += \
    Graph_plot/formgraph.h \
    Logic/ISCV_LOGIC.h \
    Logic/ISCV_Types.h \
    SerialPort/QSerialProtocol.h \
    SerialPort/SerialPort.h \
    mainwindow.h \
    qcustomplot.h \
    style_classes/window_styles.h

FORMS += \
    Graph_plot/formgraph.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource_data.qrc
