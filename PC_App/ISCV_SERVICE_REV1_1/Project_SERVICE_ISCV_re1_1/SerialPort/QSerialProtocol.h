#ifndef QSERIALPROTOCOL_H
#define QSERIALPROTOCOL_H

#include <QObject>
#include <stdint.h>
#include <stdbool.h>
#include <QString>
#include <QByteArray>


typedef enum{
    SP_CMD_RESERVED_0,
    SP_CMD_SET_MODE,
    SP_CMD_MSG_ERROR,
    SP_CMD_MSG_SERVICE,
    SP_CMD_SET_SETTINGS,
    SP_CMD_ECHO_PAYLOAD,
    SP_CMD_GET_INPUTS,
    SP_CMD_SEND_INPUTS,
    SP_CMD_SET_START_MODE,
    SP_CMD_SET_START_PWM,
    SP_CMD_SET_RESISTORS,
    SP_CMD_GET_RESISTORS,
    SP_CMD_SEND_RESISTORS,
    SP_CMD_ADC_CALIBRATION,
    SP_CMD_ADC_COEF_SAVE,
    SP_CMD_SET_DAC_OUTPUT,
    SPROTOCOL_COMMANDS_COUNT
}cmd_protocol;


typedef enum
{
    RX_WAIT_ID_PART1=0,
    RX_WAIT_ID_PART2,
    RX_WAIT_LENGTH,
    RX_COLLECT_DATA,
    RX_TO_TX_STATE,
    TX_MODE
}SProtocol_states;





#pragma pack(push,1)
typedef struct
{
    uint8_t length;
    uint8_t command;
    uint8_t* data;
    uint8_t  dataRxPayload[32];
}protocol_Frame_data;
#pragma pack(pop)






class QSerialProtocol : public QObject
{
    Q_OBJECT
public:
    explicit QSerialProtocol();
    explicit QSerialProtocol(uint16_t id);

     void setID(uint16_t id);
     void sendGoBoot();
     void addTransferData(QByteArray data);
     void transmitData(protocol_Frame_data* _frame);
     void QSerialProtocol_ClearBuffer();

private:
     uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
     uint8_t checkCRC16(uint8_t *data,uint8_t len);
     uint8_t checkCRC16(QByteArray data,uint8_t len);
     void parseRecieverDataBuffer(QByteArray data);
     uint16_t getCrc16(uint8_t * pcBlock, uint16_t len);

public slots:
        void serialProtReceived(QByteArray data);


signals:
    void serialTransmit(QByteArray* data);
    void receivedMsgFrame(protocol_Frame_data* _frame);

private:
    bool startSystem;
    QByteArray collectionData;
    uint16_t ID_DEV;
    uint16_t lengthExpectid;
    QList<protocol_Frame_data> getMsgList;
    uint8_t stateReceived=0;
    uint16_t idSlave=0x0000;
    uint16_t idMaster=0x0000;

};

#endif // QSERIALPROTOCOL_H
