/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ADC_BOARD_SERVICE/MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[23];
    char stringdata0[364];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "update_NamePort"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 11), // "Connect_Btn"
QT_MOC_LITERAL(4, 40, 16), // "plot_Graph_Start"
QT_MOC_LITERAL(5, 57, 16), // "write_data_Start"
QT_MOC_LITERAL(6, 74, 14), // "serialReceived"
QT_MOC_LITERAL(7, 89, 4), // "data"
QT_MOC_LITERAL(8, 94, 11), // "receivedMsg"
QT_MOC_LITERAL(9, 106, 18), // "SProtocol_message*"
QT_MOC_LITERAL(10, 125, 17), // "Request_Board_Tim"
QT_MOC_LITERAL(11, 143, 27), // "on_checkBox_EN_stateChanged"
QT_MOC_LITERAL(12, 171, 4), // "arg1"
QT_MOC_LITERAL(13, 176, 13), // "Settings_read"
QT_MOC_LITERAL(14, 190, 12), // "Settings_Set"
QT_MOC_LITERAL(15, 203, 21), // "Settings_write_Memory"
QT_MOC_LITERAL(16, 225, 21), // "Request_Test_MainData"
QT_MOC_LITERAL(17, 247, 21), // "Requrst_test_Firmwire"
QT_MOC_LITERAL(18, 269, 14), // "slotTimerAlarm"
QT_MOC_LITERAL(19, 284, 17), // "slotTimerAlarmCSV"
QT_MOC_LITERAL(20, 302, 18), // "Slot_Timer_TimeOut"
QT_MOC_LITERAL(21, 321, 20), // "btn_rs_connect_ClICK"
QT_MOC_LITERAL(22, 342, 21) // "Update_UI_Packet_Info"

    },
    "MainWindow\0update_NamePort\0\0Connect_Btn\0"
    "plot_Graph_Start\0write_data_Start\0"
    "serialReceived\0data\0receivedMsg\0"
    "SProtocol_message*\0Request_Board_Tim\0"
    "on_checkBox_EN_stateChanged\0arg1\0"
    "Settings_read\0Settings_Set\0"
    "Settings_write_Memory\0Request_Test_MainData\0"
    "Requrst_test_Firmwire\0slotTimerAlarm\0"
    "slotTimerAlarmCSV\0Slot_Timer_TimeOut\0"
    "btn_rs_connect_ClICK\0Update_UI_Packet_Info"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x08 /* Private */,
       3,    0,  105,    2, 0x08 /* Private */,
       4,    0,  106,    2, 0x08 /* Private */,
       5,    0,  107,    2, 0x08 /* Private */,
       6,    1,  108,    2, 0x08 /* Private */,
       8,    1,  111,    2, 0x08 /* Private */,
      10,    0,  114,    2, 0x08 /* Private */,
      11,    1,  115,    2, 0x08 /* Private */,
      13,    0,  118,    2, 0x08 /* Private */,
      14,    0,  119,    2, 0x08 /* Private */,
      15,    0,  120,    2, 0x08 /* Private */,
      16,    0,  121,    2, 0x08 /* Private */,
      17,    0,  122,    2, 0x08 /* Private */,
      18,    0,  123,    2, 0x08 /* Private */,
      19,    0,  124,    2, 0x08 /* Private */,
      20,    0,  125,    2, 0x08 /* Private */,
      21,    0,  126,    2, 0x08 /* Private */,
      22,    1,  127,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, 0x80000000 | 9,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    7,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_NamePort(); break;
        case 1: _t->Connect_Btn(); break;
        case 2: _t->plot_Graph_Start(); break;
        case 3: _t->write_data_Start(); break;
        case 4: _t->serialReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 5: _t->receivedMsg((*reinterpret_cast< SProtocol_message*(*)>(_a[1]))); break;
        case 6: _t->Request_Board_Tim(); break;
        case 7: _t->on_checkBox_EN_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->Settings_read(); break;
        case 9: _t->Settings_Set(); break;
        case 10: _t->Settings_write_Memory(); break;
        case 11: _t->Request_Test_MainData(); break;
        case 12: _t->Requrst_test_Firmwire(); break;
        case 13: _t->slotTimerAlarm(); break;
        case 14: _t->slotTimerAlarmCSV(); break;
        case 15: _t->Slot_Timer_TimeOut(); break;
        case 16: _t->btn_rs_connect_ClICK(); break;
        case 17: _t->Update_UI_Packet_Info((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
