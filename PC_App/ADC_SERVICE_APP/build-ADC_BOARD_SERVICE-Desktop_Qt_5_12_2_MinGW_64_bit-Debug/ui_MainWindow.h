/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QVBoxLayout *verticalLayout_2;
    QLabel *Label_Board_Info;
    QLabel *Packet_Info;
    QTabWidget *tabWidget;
    QWidget *MainTab;
    QHBoxLayout *horizontalLayout_8;
    QLabel *MainData;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBox_EN;
    QPushButton *plotButton;
    QPushButton *btn_save;
    QSpacerItem *verticalSpacer;
    QWidget *ServiceTab;
    QHBoxLayout *horizontalLayout_57;
    QVBoxLayout *verticalLayout_12;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QLineEdit *settings_Kp_1;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QLineEdit *settings_bias_1;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_8;
    QLineEdit *settings_Kp_2;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_9;
    QLineEdit *settings_bias_2;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_10;
    QLineEdit *settings_Kp_3;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_11;
    QLineEdit *settings_bias_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_12;
    QLineEdit *settings_Kp_4;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_13;
    QLineEdit *settings_bias_4;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_14;
    QLineEdit *settings_Kp_5;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_15;
    QLineEdit *settings_bias_5;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_16;
    QLineEdit *settings_Kp_6;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_17;
    QLineEdit *settings_bias_6;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_18;
    QLineEdit *settings_Kp_7;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_19;
    QLineEdit *settings_bias_7;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_20;
    QLineEdit *settings_Kp_8;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_21;
    QLineEdit *settings_bias_8;
    QVBoxLayout *verticalLayout_21;
    QVBoxLayout *verticalLayout_13;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_22;
    QLineEdit *settings_Kp_9;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_23;
    QLineEdit *settings_bias_9;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_24;
    QLineEdit *settings_Kp_10;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_25;
    QLineEdit *settings_bias_10;
    QVBoxLayout *verticalLayout_15;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_26;
    QLineEdit *settings_Kp_11;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_27;
    QLineEdit *settings_bias_11;
    QVBoxLayout *verticalLayout_16;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_28;
    QLineEdit *settings_Kp_12;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_29;
    QLineEdit *settings_bias_12;
    QVBoxLayout *verticalLayout_17;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_30;
    QLineEdit *settings_Kp_13;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_31;
    QLineEdit *settings_bias_13;
    QVBoxLayout *verticalLayout_18;
    QHBoxLayout *horizontalLayout_35;
    QLabel *label_32;
    QLineEdit *settings_Kp_14;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_33;
    QLineEdit *settings_bias_14;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_37;
    QLabel *label_34;
    QLineEdit *settings_Kp_15;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_35;
    QLineEdit *settings_bias_15;
    QVBoxLayout *verticalLayout_20;
    QHBoxLayout *horizontalLayout_39;
    QLabel *label_36;
    QLineEdit *settings_Kp_16;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_37;
    QLineEdit *settings_bias_16;
    QVBoxLayout *verticalLayout_30;
    QVBoxLayout *verticalLayout_22;
    QHBoxLayout *horizontalLayout_41;
    QLabel *label_38;
    QLineEdit *settings_Kp_17;
    QHBoxLayout *horizontalLayout_42;
    QLabel *label_39;
    QLineEdit *settings_bias_17;
    QVBoxLayout *verticalLayout_23;
    QHBoxLayout *horizontalLayout_43;
    QLabel *label_40;
    QLineEdit *settings_Kp_18;
    QHBoxLayout *horizontalLayout_44;
    QLabel *label_41;
    QLineEdit *settings_bias_18;
    QVBoxLayout *verticalLayout_24;
    QHBoxLayout *horizontalLayout_45;
    QLabel *label_42;
    QLineEdit *settings_Kp_19;
    QHBoxLayout *horizontalLayout_46;
    QLabel *label_43;
    QLineEdit *settings_bias_19;
    QVBoxLayout *verticalLayout_25;
    QHBoxLayout *horizontalLayout_47;
    QLabel *label_44;
    QLineEdit *settings_Kp_20;
    QHBoxLayout *horizontalLayout_48;
    QLabel *label_45;
    QLineEdit *settings_bias_20;
    QVBoxLayout *verticalLayout_26;
    QHBoxLayout *horizontalLayout_49;
    QLabel *label_46;
    QLineEdit *settings_Kp_21;
    QHBoxLayout *horizontalLayout_50;
    QLabel *label_47;
    QLineEdit *settings_bias_21;
    QVBoxLayout *verticalLayout_27;
    QHBoxLayout *horizontalLayout_51;
    QLabel *label_48;
    QLineEdit *settings_Kp_22;
    QHBoxLayout *horizontalLayout_52;
    QLabel *label_49;
    QLineEdit *settings_bias_22;
    QVBoxLayout *verticalLayout_28;
    QHBoxLayout *horizontalLayout_53;
    QLabel *label_50;
    QLineEdit *settings_Kp_23;
    QHBoxLayout *horizontalLayout_54;
    QLabel *label_51;
    QLineEdit *settings_bias_23;
    QVBoxLayout *verticalLayout_29;
    QHBoxLayout *horizontalLayout_55;
    QLabel *label_52;
    QLineEdit *settings_Kp_24;
    QHBoxLayout *horizontalLayout_56;
    QLabel *label_53;
    QLineEdit *settings_bias_24;
    QVBoxLayout *verticalLayout_31;
    QPushButton *pushButton_read;
    QPushButton *pushButton_set;
    QPushButton *pushButton_write_eeprom;
    QSpacerItem *verticalSpacer_2;
    QWidget *TestingTab;
    QPushButton *Btn_test_firmwire;
    QPushButton *Btn_testMainData;
    QPushButton *Btn_testConnect;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(692, 690);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_4 = new QVBoxLayout(centralwidget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(5);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setSizeConstraint(QLayout::SetMinAndMaxSize);
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));
        sizePolicy.setHeightForWidth(Name_update_btn->sizePolicy().hasHeightForWidth());
        Name_update_btn->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));
        sizePolicy.setHeightForWidth(NameSerialPort_Box->sizePolicy().hasHeightForWidth());
        NameSerialPort_Box->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));
        sizePolicy.setHeightForWidth(SpeedSerialPort_Box->sizePolicy().hasHeightForWidth());
        SpeedSerialPort_Box->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));
        sizePolicy.setHeightForWidth(on_ConnectButton->sizePolicy().hasHeightForWidth());
        on_ConnectButton->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_5->addWidget(groupBox_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        Label_Board_Info = new QLabel(centralwidget);
        Label_Board_Info->setObjectName(QString::fromUtf8("Label_Board_Info"));

        verticalLayout_2->addWidget(Label_Board_Info);

        Packet_Info = new QLabel(centralwidget);
        Packet_Info->setObjectName(QString::fromUtf8("Packet_Info"));

        verticalLayout_2->addWidget(Packet_Info);


        horizontalLayout_5->addLayout(verticalLayout_2);


        verticalLayout_4->addLayout(horizontalLayout_5);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8(""));
        MainTab = new QWidget();
        MainTab->setObjectName(QString::fromUtf8("MainTab"));
        horizontalLayout_8 = new QHBoxLayout(MainTab);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        MainData = new QLabel(MainTab);
        MainData->setObjectName(QString::fromUtf8("MainData"));
        sizePolicy.setHeightForWidth(MainData->sizePolicy().hasHeightForWidth());
        MainData->setSizePolicy(sizePolicy);
        MainData->setMinimumSize(QSize(421, 441));
        QFont font;
        font.setPointSize(14);
        font.setItalic(true);
        MainData->setFont(font);
        MainData->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_8->addWidget(MainData);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        checkBox_EN = new QCheckBox(MainTab);
        checkBox_EN->setObjectName(QString::fromUtf8("checkBox_EN"));
        checkBox_EN->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(checkBox_EN->sizePolicy().hasHeightForWidth());
        checkBox_EN->setSizePolicy(sizePolicy2);
        checkBox_EN->setStyleSheet(QString::fromUtf8("QCheckBox::indicator{\n"
"width:50px;\n"
"height:50px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked\n"
"{\n"
"	image:url(\":/images/images/off_button.png\");\n"
"\n"
"}\n"
"\n"
"QCheckBox::indicator:checked\n"
"{\n"
"image:url(\":/images/images/on_button.png\");\n"
"}"));

        verticalLayout->addWidget(checkBox_EN);

        plotButton = new QPushButton(MainTab);
        plotButton->setObjectName(QString::fromUtf8("plotButton"));

        verticalLayout->addWidget(plotButton);

        btn_save = new QPushButton(MainTab);
        btn_save->setObjectName(QString::fromUtf8("btn_save"));

        verticalLayout->addWidget(btn_save);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_8->addLayout(verticalLayout);

        tabWidget->addTab(MainTab, QString());
        ServiceTab = new QWidget();
        ServiceTab->setObjectName(QString::fromUtf8("ServiceTab"));
        horizontalLayout_57 = new QHBoxLayout(ServiceTab);
        horizontalLayout_57->setObjectName(QString::fromUtf8("horizontalLayout_57"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(2);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_3 = new QLabel(ServiceTab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_6->addWidget(label_3);

        settings_Kp_1 = new QLineEdit(ServiceTab);
        settings_Kp_1->setObjectName(QString::fromUtf8("settings_Kp_1"));

        horizontalLayout_6->addWidget(settings_Kp_1);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(ServiceTab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_7->addWidget(label_4);

        settings_bias_1 = new QLineEdit(ServiceTab);
        settings_bias_1->setObjectName(QString::fromUtf8("settings_bias_1"));

        horizontalLayout_7->addWidget(settings_bias_1);


        verticalLayout_3->addLayout(horizontalLayout_7);


        verticalLayout_12->addLayout(verticalLayout_3);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_8 = new QLabel(ServiceTab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_11->addWidget(label_8);

        settings_Kp_2 = new QLineEdit(ServiceTab);
        settings_Kp_2->setObjectName(QString::fromUtf8("settings_Kp_2"));

        horizontalLayout_11->addWidget(settings_Kp_2);


        verticalLayout_5->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_9 = new QLabel(ServiceTab);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_12->addWidget(label_9);

        settings_bias_2 = new QLineEdit(ServiceTab);
        settings_bias_2->setObjectName(QString::fromUtf8("settings_bias_2"));

        horizontalLayout_12->addWidget(settings_bias_2);


        verticalLayout_5->addLayout(horizontalLayout_12);


        verticalLayout_12->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_10 = new QLabel(ServiceTab);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_13->addWidget(label_10);

        settings_Kp_3 = new QLineEdit(ServiceTab);
        settings_Kp_3->setObjectName(QString::fromUtf8("settings_Kp_3"));

        horizontalLayout_13->addWidget(settings_Kp_3);


        verticalLayout_6->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_11 = new QLabel(ServiceTab);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_14->addWidget(label_11);

        settings_bias_3 = new QLineEdit(ServiceTab);
        settings_bias_3->setObjectName(QString::fromUtf8("settings_bias_3"));

        horizontalLayout_14->addWidget(settings_bias_3);


        verticalLayout_6->addLayout(horizontalLayout_14);


        verticalLayout_12->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_12 = new QLabel(ServiceTab);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_15->addWidget(label_12);

        settings_Kp_4 = new QLineEdit(ServiceTab);
        settings_Kp_4->setObjectName(QString::fromUtf8("settings_Kp_4"));

        horizontalLayout_15->addWidget(settings_Kp_4);


        verticalLayout_7->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_13 = new QLabel(ServiceTab);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_16->addWidget(label_13);

        settings_bias_4 = new QLineEdit(ServiceTab);
        settings_bias_4->setObjectName(QString::fromUtf8("settings_bias_4"));

        horizontalLayout_16->addWidget(settings_bias_4);


        verticalLayout_7->addLayout(horizontalLayout_16);


        verticalLayout_12->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_14 = new QLabel(ServiceTab);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_17->addWidget(label_14);

        settings_Kp_5 = new QLineEdit(ServiceTab);
        settings_Kp_5->setObjectName(QString::fromUtf8("settings_Kp_5"));

        horizontalLayout_17->addWidget(settings_Kp_5);


        verticalLayout_8->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_15 = new QLabel(ServiceTab);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_18->addWidget(label_15);

        settings_bias_5 = new QLineEdit(ServiceTab);
        settings_bias_5->setObjectName(QString::fromUtf8("settings_bias_5"));

        horizontalLayout_18->addWidget(settings_bias_5);


        verticalLayout_8->addLayout(horizontalLayout_18);


        verticalLayout_12->addLayout(verticalLayout_8);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_16 = new QLabel(ServiceTab);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_19->addWidget(label_16);

        settings_Kp_6 = new QLineEdit(ServiceTab);
        settings_Kp_6->setObjectName(QString::fromUtf8("settings_Kp_6"));

        horizontalLayout_19->addWidget(settings_Kp_6);


        verticalLayout_9->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_17 = new QLabel(ServiceTab);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_20->addWidget(label_17);

        settings_bias_6 = new QLineEdit(ServiceTab);
        settings_bias_6->setObjectName(QString::fromUtf8("settings_bias_6"));

        horizontalLayout_20->addWidget(settings_bias_6);


        verticalLayout_9->addLayout(horizontalLayout_20);


        verticalLayout_12->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        label_18 = new QLabel(ServiceTab);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_21->addWidget(label_18);

        settings_Kp_7 = new QLineEdit(ServiceTab);
        settings_Kp_7->setObjectName(QString::fromUtf8("settings_Kp_7"));

        horizontalLayout_21->addWidget(settings_Kp_7);


        verticalLayout_10->addLayout(horizontalLayout_21);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_19 = new QLabel(ServiceTab);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_22->addWidget(label_19);

        settings_bias_7 = new QLineEdit(ServiceTab);
        settings_bias_7->setObjectName(QString::fromUtf8("settings_bias_7"));

        horizontalLayout_22->addWidget(settings_bias_7);


        verticalLayout_10->addLayout(horizontalLayout_22);


        verticalLayout_12->addLayout(verticalLayout_10);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_20 = new QLabel(ServiceTab);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_23->addWidget(label_20);

        settings_Kp_8 = new QLineEdit(ServiceTab);
        settings_Kp_8->setObjectName(QString::fromUtf8("settings_Kp_8"));

        horizontalLayout_23->addWidget(settings_Kp_8);


        verticalLayout_11->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_21 = new QLabel(ServiceTab);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        horizontalLayout_24->addWidget(label_21);

        settings_bias_8 = new QLineEdit(ServiceTab);
        settings_bias_8->setObjectName(QString::fromUtf8("settings_bias_8"));

        horizontalLayout_24->addWidget(settings_bias_8);


        verticalLayout_11->addLayout(horizontalLayout_24);


        verticalLayout_12->addLayout(verticalLayout_11);


        horizontalLayout_57->addLayout(verticalLayout_12);

        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setSpacing(2);
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        label_22 = new QLabel(ServiceTab);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        horizontalLayout_25->addWidget(label_22);

        settings_Kp_9 = new QLineEdit(ServiceTab);
        settings_Kp_9->setObjectName(QString::fromUtf8("settings_Kp_9"));

        horizontalLayout_25->addWidget(settings_Kp_9);


        verticalLayout_13->addLayout(horizontalLayout_25);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        label_23 = new QLabel(ServiceTab);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        horizontalLayout_26->addWidget(label_23);

        settings_bias_9 = new QLineEdit(ServiceTab);
        settings_bias_9->setObjectName(QString::fromUtf8("settings_bias_9"));

        horizontalLayout_26->addWidget(settings_bias_9);


        verticalLayout_13->addLayout(horizontalLayout_26);


        verticalLayout_21->addLayout(verticalLayout_13);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        label_24 = new QLabel(ServiceTab);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        horizontalLayout_27->addWidget(label_24);

        settings_Kp_10 = new QLineEdit(ServiceTab);
        settings_Kp_10->setObjectName(QString::fromUtf8("settings_Kp_10"));

        horizontalLayout_27->addWidget(settings_Kp_10);


        verticalLayout_14->addLayout(horizontalLayout_27);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setObjectName(QString::fromUtf8("horizontalLayout_28"));
        label_25 = new QLabel(ServiceTab);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        horizontalLayout_28->addWidget(label_25);

        settings_bias_10 = new QLineEdit(ServiceTab);
        settings_bias_10->setObjectName(QString::fromUtf8("settings_bias_10"));

        horizontalLayout_28->addWidget(settings_bias_10);


        verticalLayout_14->addLayout(horizontalLayout_28);


        verticalLayout_21->addLayout(verticalLayout_14);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setObjectName(QString::fromUtf8("horizontalLayout_29"));
        label_26 = new QLabel(ServiceTab);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        horizontalLayout_29->addWidget(label_26);

        settings_Kp_11 = new QLineEdit(ServiceTab);
        settings_Kp_11->setObjectName(QString::fromUtf8("settings_Kp_11"));

        horizontalLayout_29->addWidget(settings_Kp_11);


        verticalLayout_15->addLayout(horizontalLayout_29);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        label_27 = new QLabel(ServiceTab);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        horizontalLayout_30->addWidget(label_27);

        settings_bias_11 = new QLineEdit(ServiceTab);
        settings_bias_11->setObjectName(QString::fromUtf8("settings_bias_11"));

        horizontalLayout_30->addWidget(settings_bias_11);


        verticalLayout_15->addLayout(horizontalLayout_30);


        verticalLayout_21->addLayout(verticalLayout_15);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        label_28 = new QLabel(ServiceTab);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        horizontalLayout_31->addWidget(label_28);

        settings_Kp_12 = new QLineEdit(ServiceTab);
        settings_Kp_12->setObjectName(QString::fromUtf8("settings_Kp_12"));

        horizontalLayout_31->addWidget(settings_Kp_12);


        verticalLayout_16->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setObjectName(QString::fromUtf8("horizontalLayout_32"));
        label_29 = new QLabel(ServiceTab);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        horizontalLayout_32->addWidget(label_29);

        settings_bias_12 = new QLineEdit(ServiceTab);
        settings_bias_12->setObjectName(QString::fromUtf8("settings_bias_12"));

        horizontalLayout_32->addWidget(settings_bias_12);


        verticalLayout_16->addLayout(horizontalLayout_32);


        verticalLayout_21->addLayout(verticalLayout_16);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setObjectName(QString::fromUtf8("horizontalLayout_33"));
        label_30 = new QLabel(ServiceTab);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        horizontalLayout_33->addWidget(label_30);

        settings_Kp_13 = new QLineEdit(ServiceTab);
        settings_Kp_13->setObjectName(QString::fromUtf8("settings_Kp_13"));

        horizontalLayout_33->addWidget(settings_Kp_13);


        verticalLayout_17->addLayout(horizontalLayout_33);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setObjectName(QString::fromUtf8("horizontalLayout_34"));
        label_31 = new QLabel(ServiceTab);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        horizontalLayout_34->addWidget(label_31);

        settings_bias_13 = new QLineEdit(ServiceTab);
        settings_bias_13->setObjectName(QString::fromUtf8("settings_bias_13"));

        horizontalLayout_34->addWidget(settings_bias_13);


        verticalLayout_17->addLayout(horizontalLayout_34);


        verticalLayout_21->addLayout(verticalLayout_17);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setObjectName(QString::fromUtf8("horizontalLayout_35"));
        label_32 = new QLabel(ServiceTab);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        horizontalLayout_35->addWidget(label_32);

        settings_Kp_14 = new QLineEdit(ServiceTab);
        settings_Kp_14->setObjectName(QString::fromUtf8("settings_Kp_14"));

        horizontalLayout_35->addWidget(settings_Kp_14);


        verticalLayout_18->addLayout(horizontalLayout_35);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setObjectName(QString::fromUtf8("horizontalLayout_36"));
        label_33 = new QLabel(ServiceTab);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        horizontalLayout_36->addWidget(label_33);

        settings_bias_14 = new QLineEdit(ServiceTab);
        settings_bias_14->setObjectName(QString::fromUtf8("settings_bias_14"));

        horizontalLayout_36->addWidget(settings_bias_14);


        verticalLayout_18->addLayout(horizontalLayout_36);


        verticalLayout_21->addLayout(verticalLayout_18);

        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setObjectName(QString::fromUtf8("horizontalLayout_37"));
        label_34 = new QLabel(ServiceTab);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        horizontalLayout_37->addWidget(label_34);

        settings_Kp_15 = new QLineEdit(ServiceTab);
        settings_Kp_15->setObjectName(QString::fromUtf8("settings_Kp_15"));

        horizontalLayout_37->addWidget(settings_Kp_15);


        verticalLayout_19->addLayout(horizontalLayout_37);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setObjectName(QString::fromUtf8("horizontalLayout_38"));
        label_35 = new QLabel(ServiceTab);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        horizontalLayout_38->addWidget(label_35);

        settings_bias_15 = new QLineEdit(ServiceTab);
        settings_bias_15->setObjectName(QString::fromUtf8("settings_bias_15"));

        horizontalLayout_38->addWidget(settings_bias_15);


        verticalLayout_19->addLayout(horizontalLayout_38);


        verticalLayout_21->addLayout(verticalLayout_19);

        verticalLayout_20 = new QVBoxLayout();
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setObjectName(QString::fromUtf8("horizontalLayout_39"));
        label_36 = new QLabel(ServiceTab);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        horizontalLayout_39->addWidget(label_36);

        settings_Kp_16 = new QLineEdit(ServiceTab);
        settings_Kp_16->setObjectName(QString::fromUtf8("settings_Kp_16"));

        horizontalLayout_39->addWidget(settings_Kp_16);


        verticalLayout_20->addLayout(horizontalLayout_39);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setObjectName(QString::fromUtf8("horizontalLayout_40"));
        label_37 = new QLabel(ServiceTab);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        horizontalLayout_40->addWidget(label_37);

        settings_bias_16 = new QLineEdit(ServiceTab);
        settings_bias_16->setObjectName(QString::fromUtf8("settings_bias_16"));

        horizontalLayout_40->addWidget(settings_bias_16);


        verticalLayout_20->addLayout(horizontalLayout_40);


        verticalLayout_21->addLayout(verticalLayout_20);


        horizontalLayout_57->addLayout(verticalLayout_21);

        verticalLayout_30 = new QVBoxLayout();
        verticalLayout_30->setObjectName(QString::fromUtf8("verticalLayout_30"));
        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        horizontalLayout_41 = new QHBoxLayout();
        horizontalLayout_41->setObjectName(QString::fromUtf8("horizontalLayout_41"));
        label_38 = new QLabel(ServiceTab);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        horizontalLayout_41->addWidget(label_38);

        settings_Kp_17 = new QLineEdit(ServiceTab);
        settings_Kp_17->setObjectName(QString::fromUtf8("settings_Kp_17"));

        horizontalLayout_41->addWidget(settings_Kp_17);


        verticalLayout_22->addLayout(horizontalLayout_41);

        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setObjectName(QString::fromUtf8("horizontalLayout_42"));
        label_39 = new QLabel(ServiceTab);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        horizontalLayout_42->addWidget(label_39);

        settings_bias_17 = new QLineEdit(ServiceTab);
        settings_bias_17->setObjectName(QString::fromUtf8("settings_bias_17"));

        horizontalLayout_42->addWidget(settings_bias_17);


        verticalLayout_22->addLayout(horizontalLayout_42);


        verticalLayout_30->addLayout(verticalLayout_22);

        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setObjectName(QString::fromUtf8("horizontalLayout_43"));
        label_40 = new QLabel(ServiceTab);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        horizontalLayout_43->addWidget(label_40);

        settings_Kp_18 = new QLineEdit(ServiceTab);
        settings_Kp_18->setObjectName(QString::fromUtf8("settings_Kp_18"));

        horizontalLayout_43->addWidget(settings_Kp_18);


        verticalLayout_23->addLayout(horizontalLayout_43);

        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setObjectName(QString::fromUtf8("horizontalLayout_44"));
        label_41 = new QLabel(ServiceTab);
        label_41->setObjectName(QString::fromUtf8("label_41"));

        horizontalLayout_44->addWidget(label_41);

        settings_bias_18 = new QLineEdit(ServiceTab);
        settings_bias_18->setObjectName(QString::fromUtf8("settings_bias_18"));

        horizontalLayout_44->addWidget(settings_bias_18);


        verticalLayout_23->addLayout(horizontalLayout_44);


        verticalLayout_30->addLayout(verticalLayout_23);

        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setObjectName(QString::fromUtf8("verticalLayout_24"));
        horizontalLayout_45 = new QHBoxLayout();
        horizontalLayout_45->setObjectName(QString::fromUtf8("horizontalLayout_45"));
        label_42 = new QLabel(ServiceTab);
        label_42->setObjectName(QString::fromUtf8("label_42"));

        horizontalLayout_45->addWidget(label_42);

        settings_Kp_19 = new QLineEdit(ServiceTab);
        settings_Kp_19->setObjectName(QString::fromUtf8("settings_Kp_19"));

        horizontalLayout_45->addWidget(settings_Kp_19);


        verticalLayout_24->addLayout(horizontalLayout_45);

        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setObjectName(QString::fromUtf8("horizontalLayout_46"));
        label_43 = new QLabel(ServiceTab);
        label_43->setObjectName(QString::fromUtf8("label_43"));

        horizontalLayout_46->addWidget(label_43);

        settings_bias_19 = new QLineEdit(ServiceTab);
        settings_bias_19->setObjectName(QString::fromUtf8("settings_bias_19"));

        horizontalLayout_46->addWidget(settings_bias_19);


        verticalLayout_24->addLayout(horizontalLayout_46);


        verticalLayout_30->addLayout(verticalLayout_24);

        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setObjectName(QString::fromUtf8("verticalLayout_25"));
        horizontalLayout_47 = new QHBoxLayout();
        horizontalLayout_47->setObjectName(QString::fromUtf8("horizontalLayout_47"));
        label_44 = new QLabel(ServiceTab);
        label_44->setObjectName(QString::fromUtf8("label_44"));

        horizontalLayout_47->addWidget(label_44);

        settings_Kp_20 = new QLineEdit(ServiceTab);
        settings_Kp_20->setObjectName(QString::fromUtf8("settings_Kp_20"));

        horizontalLayout_47->addWidget(settings_Kp_20);


        verticalLayout_25->addLayout(horizontalLayout_47);

        horizontalLayout_48 = new QHBoxLayout();
        horizontalLayout_48->setObjectName(QString::fromUtf8("horizontalLayout_48"));
        label_45 = new QLabel(ServiceTab);
        label_45->setObjectName(QString::fromUtf8("label_45"));

        horizontalLayout_48->addWidget(label_45);

        settings_bias_20 = new QLineEdit(ServiceTab);
        settings_bias_20->setObjectName(QString::fromUtf8("settings_bias_20"));

        horizontalLayout_48->addWidget(settings_bias_20);


        verticalLayout_25->addLayout(horizontalLayout_48);


        verticalLayout_30->addLayout(verticalLayout_25);

        verticalLayout_26 = new QVBoxLayout();
        verticalLayout_26->setObjectName(QString::fromUtf8("verticalLayout_26"));
        horizontalLayout_49 = new QHBoxLayout();
        horizontalLayout_49->setObjectName(QString::fromUtf8("horizontalLayout_49"));
        label_46 = new QLabel(ServiceTab);
        label_46->setObjectName(QString::fromUtf8("label_46"));

        horizontalLayout_49->addWidget(label_46);

        settings_Kp_21 = new QLineEdit(ServiceTab);
        settings_Kp_21->setObjectName(QString::fromUtf8("settings_Kp_21"));

        horizontalLayout_49->addWidget(settings_Kp_21);


        verticalLayout_26->addLayout(horizontalLayout_49);

        horizontalLayout_50 = new QHBoxLayout();
        horizontalLayout_50->setObjectName(QString::fromUtf8("horizontalLayout_50"));
        label_47 = new QLabel(ServiceTab);
        label_47->setObjectName(QString::fromUtf8("label_47"));

        horizontalLayout_50->addWidget(label_47);

        settings_bias_21 = new QLineEdit(ServiceTab);
        settings_bias_21->setObjectName(QString::fromUtf8("settings_bias_21"));

        horizontalLayout_50->addWidget(settings_bias_21);


        verticalLayout_26->addLayout(horizontalLayout_50);


        verticalLayout_30->addLayout(verticalLayout_26);

        verticalLayout_27 = new QVBoxLayout();
        verticalLayout_27->setObjectName(QString::fromUtf8("verticalLayout_27"));
        horizontalLayout_51 = new QHBoxLayout();
        horizontalLayout_51->setObjectName(QString::fromUtf8("horizontalLayout_51"));
        label_48 = new QLabel(ServiceTab);
        label_48->setObjectName(QString::fromUtf8("label_48"));

        horizontalLayout_51->addWidget(label_48);

        settings_Kp_22 = new QLineEdit(ServiceTab);
        settings_Kp_22->setObjectName(QString::fromUtf8("settings_Kp_22"));

        horizontalLayout_51->addWidget(settings_Kp_22);


        verticalLayout_27->addLayout(horizontalLayout_51);

        horizontalLayout_52 = new QHBoxLayout();
        horizontalLayout_52->setObjectName(QString::fromUtf8("horizontalLayout_52"));
        label_49 = new QLabel(ServiceTab);
        label_49->setObjectName(QString::fromUtf8("label_49"));

        horizontalLayout_52->addWidget(label_49);

        settings_bias_22 = new QLineEdit(ServiceTab);
        settings_bias_22->setObjectName(QString::fromUtf8("settings_bias_22"));

        horizontalLayout_52->addWidget(settings_bias_22);


        verticalLayout_27->addLayout(horizontalLayout_52);


        verticalLayout_30->addLayout(verticalLayout_27);

        verticalLayout_28 = new QVBoxLayout();
        verticalLayout_28->setObjectName(QString::fromUtf8("verticalLayout_28"));
        horizontalLayout_53 = new QHBoxLayout();
        horizontalLayout_53->setObjectName(QString::fromUtf8("horizontalLayout_53"));
        label_50 = new QLabel(ServiceTab);
        label_50->setObjectName(QString::fromUtf8("label_50"));

        horizontalLayout_53->addWidget(label_50);

        settings_Kp_23 = new QLineEdit(ServiceTab);
        settings_Kp_23->setObjectName(QString::fromUtf8("settings_Kp_23"));

        horizontalLayout_53->addWidget(settings_Kp_23);


        verticalLayout_28->addLayout(horizontalLayout_53);

        horizontalLayout_54 = new QHBoxLayout();
        horizontalLayout_54->setObjectName(QString::fromUtf8("horizontalLayout_54"));
        label_51 = new QLabel(ServiceTab);
        label_51->setObjectName(QString::fromUtf8("label_51"));

        horizontalLayout_54->addWidget(label_51);

        settings_bias_23 = new QLineEdit(ServiceTab);
        settings_bias_23->setObjectName(QString::fromUtf8("settings_bias_23"));

        horizontalLayout_54->addWidget(settings_bias_23);


        verticalLayout_28->addLayout(horizontalLayout_54);


        verticalLayout_30->addLayout(verticalLayout_28);

        verticalLayout_29 = new QVBoxLayout();
        verticalLayout_29->setObjectName(QString::fromUtf8("verticalLayout_29"));
        horizontalLayout_55 = new QHBoxLayout();
        horizontalLayout_55->setObjectName(QString::fromUtf8("horizontalLayout_55"));
        label_52 = new QLabel(ServiceTab);
        label_52->setObjectName(QString::fromUtf8("label_52"));

        horizontalLayout_55->addWidget(label_52);

        settings_Kp_24 = new QLineEdit(ServiceTab);
        settings_Kp_24->setObjectName(QString::fromUtf8("settings_Kp_24"));

        horizontalLayout_55->addWidget(settings_Kp_24);


        verticalLayout_29->addLayout(horizontalLayout_55);

        horizontalLayout_56 = new QHBoxLayout();
        horizontalLayout_56->setObjectName(QString::fromUtf8("horizontalLayout_56"));
        label_53 = new QLabel(ServiceTab);
        label_53->setObjectName(QString::fromUtf8("label_53"));

        horizontalLayout_56->addWidget(label_53);

        settings_bias_24 = new QLineEdit(ServiceTab);
        settings_bias_24->setObjectName(QString::fromUtf8("settings_bias_24"));

        horizontalLayout_56->addWidget(settings_bias_24);


        verticalLayout_29->addLayout(horizontalLayout_56);


        verticalLayout_30->addLayout(verticalLayout_29);


        horizontalLayout_57->addLayout(verticalLayout_30);

        verticalLayout_31 = new QVBoxLayout();
        verticalLayout_31->setObjectName(QString::fromUtf8("verticalLayout_31"));
        pushButton_read = new QPushButton(ServiceTab);
        pushButton_read->setObjectName(QString::fromUtf8("pushButton_read"));

        verticalLayout_31->addWidget(pushButton_read);

        pushButton_set = new QPushButton(ServiceTab);
        pushButton_set->setObjectName(QString::fromUtf8("pushButton_set"));

        verticalLayout_31->addWidget(pushButton_set);

        pushButton_write_eeprom = new QPushButton(ServiceTab);
        pushButton_write_eeprom->setObjectName(QString::fromUtf8("pushButton_write_eeprom"));

        verticalLayout_31->addWidget(pushButton_write_eeprom);

        verticalSpacer_2 = new QSpacerItem(20, 318, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_31->addItem(verticalSpacer_2);


        horizontalLayout_57->addLayout(verticalLayout_31);

        tabWidget->addTab(ServiceTab, QString());
        TestingTab = new QWidget();
        TestingTab->setObjectName(QString::fromUtf8("TestingTab"));
        Btn_test_firmwire = new QPushButton(TestingTab);
        Btn_test_firmwire->setObjectName(QString::fromUtf8("Btn_test_firmwire"));
        Btn_test_firmwire->setGeometry(QRect(10, 40, 93, 28));
        Btn_testMainData = new QPushButton(TestingTab);
        Btn_testMainData->setObjectName(QString::fromUtf8("Btn_testMainData"));
        Btn_testMainData->setGeometry(QRect(10, 80, 93, 28));
        Btn_testConnect = new QPushButton(TestingTab);
        Btn_testConnect->setObjectName(QString::fromUtf8("Btn_testConnect"));
        Btn_testConnect->setGeometry(QRect(10, 120, 93, 28));
        tabWidget->addTab(TestingTab, QString());

        verticalLayout_4->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        Label_Board_Info->setText(QString());
        Packet_Info->setText(QString());
        MainData->setText(QApplication::translate("MainWindow", "Test", nullptr));
        checkBox_EN->setText(QString());
        plotButton->setText(QApplication::translate("MainWindow", "Plot Graph", nullptr));
        btn_save->setText(QApplication::translate("MainWindow", "Write Data", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(MainTab), QApplication::translate("MainWindow", "Main Data", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Kp1:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "bias1:", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Kp2:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "bias2:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Kp3:", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "bias3:", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Kp4:", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "bias4:", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Kp5:", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "bias5:", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Kp6:", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "bias6:", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "Kp7:", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "bias7:", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Kp8:", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "bias8:", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "Kp9:", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "bias9:", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "Kp10:", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "bias10:", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "Kp11:", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "bias11:", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "Kp12:", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "bias12:", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "Kp13:", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "bias13:", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "Kp14:", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "bias14:", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "Kp15:", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "bias15:", nullptr));
        label_36->setText(QApplication::translate("MainWindow", "Kp16:", nullptr));
        label_37->setText(QApplication::translate("MainWindow", "bias16:", nullptr));
        label_38->setText(QApplication::translate("MainWindow", "Kp17:", nullptr));
        label_39->setText(QApplication::translate("MainWindow", "bias17:", nullptr));
        label_40->setText(QApplication::translate("MainWindow", "Kp18:", nullptr));
        label_41->setText(QApplication::translate("MainWindow", "bias18:", nullptr));
        label_42->setText(QApplication::translate("MainWindow", "Kp19:", nullptr));
        label_43->setText(QApplication::translate("MainWindow", "bias19:", nullptr));
        label_44->setText(QApplication::translate("MainWindow", "Kp20:", nullptr));
        label_45->setText(QApplication::translate("MainWindow", "bias20:", nullptr));
        label_46->setText(QApplication::translate("MainWindow", "Kp21:", nullptr));
        label_47->setText(QApplication::translate("MainWindow", "bias21:", nullptr));
        label_48->setText(QApplication::translate("MainWindow", "Kp22:", nullptr));
        label_49->setText(QApplication::translate("MainWindow", "bias22:", nullptr));
        label_50->setText(QApplication::translate("MainWindow", "Kp23:", nullptr));
        label_51->setText(QApplication::translate("MainWindow", "bias23:", nullptr));
        label_52->setText(QApplication::translate("MainWindow", "Kp24:", nullptr));
        label_53->setText(QApplication::translate("MainWindow", "bias24:", nullptr));
        pushButton_read->setText(QApplication::translate("MainWindow", "Read", nullptr));
        pushButton_set->setText(QApplication::translate("MainWindow", "Set", nullptr));
        pushButton_write_eeprom->setText(QApplication::translate("MainWindow", "Write EEPROM", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(ServiceTab), QApplication::translate("MainWindow", "Service Peripheral", nullptr));
        Btn_test_firmwire->setText(QApplication::translate("MainWindow", "Test Firmwire", nullptr));
        Btn_testMainData->setText(QApplication::translate("MainWindow", "Test MainData", nullptr));
        Btn_testConnect->setText(QApplication::translate("MainWindow", "RS_Connect", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(TestingTab), QApplication::translate("MainWindow", "Testing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
