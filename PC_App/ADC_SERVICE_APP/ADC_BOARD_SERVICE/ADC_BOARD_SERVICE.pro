QT       += core gui serialport printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CSV_Manager/CSV_Manager.cpp \
    Graph_plot/formgraph.cpp \
    Logic/ADC_Logic.cpp \
    Protocol/qsprotocol.cpp \
    SerialPort/SerialPort.cpp \
    Styles/StylesUtils.cpp \
    main.cpp \
    MainWindow.cpp \
    qcustomplot.cpp

HEADERS += \
    CSV_Manager/CSV_Manager.h \
    Graph_plot/formgraph.h \
    Logic/ADC_Logic.h \
    Logic/ADC_types.h \
    MainWindow.h \
    Protocol/SProtocol_connect.h \
    Protocol/SProtocol_struct.h \
    Protocol/qsprotocol.h \
    SerialPort/SerialPort.h \
    Styles/StylesUtils.h \
    qcustomplot.h

FORMS += \
    Graph_plot/formgraph.ui \
    MainWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource_data.qrc
