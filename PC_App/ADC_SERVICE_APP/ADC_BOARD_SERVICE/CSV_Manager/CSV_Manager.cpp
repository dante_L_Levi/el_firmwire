#include "CSV_Manager.h"
#include "QDir"
#include "QDateTime"
#include "QDebug"


CSV_Manager::CSV_Manager(QObject *parent) : QObject(parent)
{

}

void CSV_Manager::CSV_Init()
{
    QDir dir(".\\data");
            if (!dir.exists()){
              dir.mkdir(".");
            }
         QString nameTextFileSave="data/default.csv";
         textFileSave=new QFile(nameTextFileSave);
}

void CSV_Manager::addNameTitle(QString name)
{
    Name=Name+QString(";%1").arg(name);
}

void CSV_Manager::CSV_Save_Title()
{
    Name=Name+"\r\n";
    *outTextFile<<Name;
}

void CSV_Manager::addData(float data)
{
    if(saveEnable)
    {
        if(textFileSave->isOpen())
        {
             MainData=MainData+QString(";%1").arg(data);
        }
    }


}

void CSV_Manager::CSV_Save_Data()
{
    if(saveEnable)
    {
        if(textFileSave->isOpen())
        {
            MainData=MainData+"\r\n";
            QChar separator =  QString("%L1").arg(0.1).at(1);
            *outTextFile<<MainData.replace(".",separator);
            MainData.clear();
        }
    }


}

void CSV_Manager::CSV_State(bool status)
{
    saveEnable=status;
    if(saveEnable)
    {
        if(textFileSave!=nullptr)
            if(textFileSave->isOpen())
                textFileSave->close();
         QString nameTextFileSave="data/CS "+QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")+".csv";
         textFileSave=new QFile(nameTextFileSave);
         if(textFileSave->open(QIODevice::WriteOnly))
         {
             outTextFile=new QTextStream(textFileSave);
             if(textFileSave->isOpen())
             {
                 CSV_Save_Title();
             }

         }
         else
         {
             qDebug()<< "File not open";
         }



    }
    else
    {

        if(textFileSave->isOpen())
                        textFileSave->close();
    }
}

bool CSV_Manager::CSV_IsOpen()
{
    if(textFileSave->isOpen())
    {
        return true;
    }
    else
        return false;
}


