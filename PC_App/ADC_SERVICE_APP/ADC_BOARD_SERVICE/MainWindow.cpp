#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Styles/StylesUtils.h"
#include "QStyleOption"
#include "QFontDatabase"
#include "QDebug"
#include "Logic/ADC_types.h"

#define PERIOD_Request_MS          25
#define ID_DEVICE       0xF001

bool status_Config;
uint8_t Tab_index=0;
;ADC_Board_hw settings;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    SetStyles_UI();

    timer=new QTimer();
    TimerAlarm=new QTimer();
    TimerAlarmCSV  =new QTimer();
    TimerAlarmCSV->setInterval(25);
    serial=new SerialPort();
    protocol=new QSProtocol();
    protocol->setID(ID_DEVICE);

    Logic=new ADC_Logic();

    timer->setInterval(PERIOD_Request_MS);
        connect(timer,SIGNAL(timeout()),this,SLOT(Request_Board_Tim()));

        connect(serial,SIGNAL(Get_data_Recieved_Serial(QByteArray)),
                this,SLOT(serialReceived(QByteArray)));

        connect(protocol,SIGNAL(receivedMsg(SProtocol_message*)),
                this,SLOT(receivedMsg(SProtocol_message*)));

        connect(protocol,SIGNAL(serialTransmit(QByteArray*)),
                serial,SLOT(transmitData(QByteArray*)));

        connect(Logic, SIGNAL(Update_Packet_Count(double)),SLOT(Update_UI_Packet_Info(double)));
        connect(TimerAlarm,SIGNAL(timeout()),this,SLOT(slotTimerAlarm()));
        connect(TimerAlarmCSV,SIGNAL(timeout()),this,SLOT(slotTimerAlarmCSV()));

        graphOutput=new formgraph();
        graphOutput->setIncrement(0.025);

        Link_event_UI();
        Update_ComBoBox_Config();

        Load_dataGraph();
        _csv_hundler=new CSV_Manager();
        _csv_hundler->CSV_Init();
        CSV_Load_NameTitle();
}

void MainWindow:: Link_event_UI(void)
{
    connect(ui->on_ConnectButton, SIGNAL (released()), this, SLOT (Connect_Btn()));
    connect(ui->Name_update_btn,SIGNAL (released()), this, SLOT (update_NamePort()));
    connect(ui->plotButton,SIGNAL(released()),this,SLOT(plot_Graph_Start()));
    connect(ui->btn_save,SIGNAL(released()),this,SLOT(write_data_Start()));
    connect(ui->Btn_testConnect,SIGNAL(released()),this,SLOT(btn_rs_connect_ClICK()));

    connect(ui->Btn_testMainData,SIGNAL (released()),this,SLOT(Request_Test_MainData()));
    connect(ui->Btn_test_firmwire,SIGNAL (released()),this,SLOT(Requrst_test_Firmwire()));

    connect(ui->pushButton_read,SIGNAL(released()),this,SLOT(Settings_read()));
    connect(ui->pushButton_set,SIGNAL(released()),this,SLOT(Settings_Set()));
    connect(ui->pushButton_write_eeprom,SIGNAL(released()),this,SLOT(Settings_write_Memory()));

    connect(ui->checkBox_EN,SIGNAL(stateChanged(int)),this,
           SLOT(on_checkBox_EN_stateChanged(int)));

}

void MainWindow:: Update_ComBoBox_Config(void)
{
    ui->SpeedSerialPort_Box->addItems(serial->Get_Baud_rate());
         const auto infos = QSerialPortInfo::availablePorts();
         for (const QSerialPortInfo &info : infos)
         {
            ui->NameSerialPort_Box->addItem(info.portName());
         }
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: serialReceived(QByteArray data)
{
    qDebug()<<data;
    protocol->serialReceived(data);
}



void MainWindow:: receivedMsg(SProtocol_message* data)
{

    Logic->Parse_Data_RX((sprotocol_commands)data->command,data);
    switch(data->command)
    {
        case SP_CMD_FIRMWIRE:
         {

           Logic->Update_UI((sprotocol_commands)data->command,ui->Label_Board_Info);
           break;
         }
        case SP_CMD_GET_INPUTS:
        {
                resize(this->width(),950);
                Logic->Update_UI((sprotocol_commands)data->command,ui->MainData);
            break;
        }
        case SP_CMD_GET_SETTINGS:
        {
            Logic->Get_Settings_data(&settings);
            Update_Fill_Settings();
            break;
        }


    }
}

void MainWindow:: Request_Board_Tim()
{
    Tab_index=ui->tabWidget->currentIndex();
    qDebug()<<"tab index:"<<Tab_index;

    if(status_start_request)
    {
        //function Request RS_Connected
       // Logic->Request_RS_Connect(protocol);
        status_start_request=false;
        return;
    }

    switch(count_request)
    {
        case 1:
        {
            Logic->Request_Get_Config_Board(protocol);
            break;
        }

        case 2:
        {
            if(Tab_index==0)
            {
                Logic->Request_Get_MainData(protocol);
            }
            break;
        }
    }

    count_request++;
    if(count_request>2)
        count_request=1;
}

void MainWindow:: update_NamePort(void)
{
    ui->SpeedSerialPort_Box->clear();
    ui->NameSerialPort_Box->clear();
   Update_ComBoBox_Config();
}

void MainWindow:: plot_Graph_Start(void)
{
    TimerAlarm->start();
    graphOutput->show();
    graphOutput->resetGraph();
}

void MainWindow:: write_data_Start(void)
{
    status_save_csv=!status_save_csv;
    _csv_hundler-> CSV_State(status_save_csv);
     if(status_save_csv)
     {

         qDebug()<<"--------Start .... Write CSV-------";
         ui->btn_save->setText("стоп");
         TimerAlarmCSV->start();
     }
     else
     {
         ui->btn_save->setText("Start");
         TimerAlarmCSV->stop();


     }
}
void MainWindow:: Connect_Btn(void)
{
    count_ConnectButton=!count_ConnectButton;
    if(count_ConnectButton)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);
        Logic->Set_ID(ID_DEVICE);

        status_Config=serial->ConnectPort(ui->NameSerialPort_Box->currentText(),
                                               (ui->SpeedSerialPort_Box->currentText()).toLong());

        if(status_Config)
        {
            QMessageBox::information(this,"Connection!!","Connection OK!");
            timeWork->start();
        }
        else
        {
            QMessageBox::information(this," Not Connection!!","Connection error!");
            ui->NameSerialPort_Box->setEnabled(true);
            ui->SpeedSerialPort_Box->setEnabled(true);
        }

    }
    else
    {
        ui->on_ConnectButton->setText("Disconnected");
        timer->stop();
        ui->checkBox_EN->setChecked(false);

        status_start_request=false;
        serial->DisconnectedPort();

        QMessageBox::information(this,"Connector Status","Close Port!!");
        ui->NameSerialPort_Box->setEnabled(true);
        ui->SpeedSerialPort_Box->setEnabled(true);

    }

    ui->checkBox_EN->setEnabled(status_Config&& count_ConnectButton);
}

void MainWindow:: on_checkBox_EN_stateChanged(int arg1)
{
    if(arg1==2)
    {
         status_start_request=true;
        timer->start();
        //timeWork->start();
    }
    else
    {
         status_start_request=false;
       timer->stop();
    }
}

void MainWindow:: Request_Test_MainData()
{
    Logic->Request_Get_MainData(protocol);
}
void MainWindow:: Requrst_test_Firmwire()
{
    Logic->Request_Get_Config_Board(protocol);
}

void MainWindow:: slotTimerAlarm(void)
{
    graphOutput->updateGraph(timeWork->elapsed()/1000.0);
}
void MainWindow:: slotTimerAlarmCSV(void)
{
   if(status_save_csv)
   {
       if(_csv_hundler->CSV_IsOpen())
       {
           _csv_hundler->addData(timeWork->elapsed()/1000.0);
         //qDebug()<< "Write Dats CSV";
         //QString str;
         //Load  Accel
         for(int i=0;i<ADC_CHANNEL_COUNT;i++)
         {
          _csv_hundler->addData(*Logic->Get_ADC_Value(i));
         }
         count_packet_CSV++;
         _csv_hundler->CSV_Save_Data();
       }
   }
}
void MainWindow:: Slot_Timer_TimeOut(void)
{

}

void MainWindow::btn_rs_connect_ClICK()
{
    Logic->Request_RS_Connect(protocol);
}

void MainWindow:: Update_UI_Packet_Info(double data)
{
    ui->Packet_Info->setText("Packet:"+QString::number(data));
}


void MainWindow::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);

    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget,&opt,&p,this);
    QWidget::paintEvent(event);
}

void MainWindow::SetStyles_UI()
{
    StylesUtils::Set_Image_BackGround_Default(this);
    ui->label->setStyleSheet(StylesUtils::getStyle_LabelText_Config_Part());
    ui->label_2->setStyleSheet(StylesUtils::getStyle_LabelText_Config_Part());
    ui->Label_Board_Info->setStyleSheet(StylesUtils::getStyle_LabelText_Firmwire_Part());
    ui->Name_update_btn->setStyleSheet(StylesUtils::getStyle_Button_Config_Part());
    ui->on_ConnectButton->setStyleSheet(StylesUtils::getStyle_Button_Config_Part());
    ui->NameSerialPort_Box->setStyleSheet(StylesUtils::getStyle_ComboBox_Config_Part());
    ui->SpeedSerialPort_Box->setStyleSheet(StylesUtils::getStyle_ComboBox_Config_Part());
}


void MainWindow:: Load_dataGraph(void)
{

    timeWork=new QTime();
    TypeGraphData gData;
    gData.plot=false;
    gData.axisType=QCPAxis::atLeft;
    gData.valueType=float_type;
    for(int i=0;i<ADC_CHANNEL_COUNT;i++)
    {
        gData.name=(QString)(Logic->DataName[i]);
        gData.value=Logic->Get_ADC_Value(i);
        graphOutput->addGraph(&gData);

    }

}

void MainWindow:: CSV_Load_NameTitle(void)
{
    QString dt="Time";
     _csv_hundler->addNameTitle(dt);
    for(int i=0;i<ADC_CHANNEL_COUNT;i++)
    {
        _csv_hundler->addNameTitle((QString)(Logic->DataName[i]));

    }
}
void MainWindow:: update_status_Settings_indicate(void)
{
    ui->settings_Kp_1->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_2->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_3->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_4->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_5->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_6->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_7->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_8->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_9->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_10->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_11->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_12->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_13->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_14->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_15->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_16->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_17->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_18->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_19->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_20->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_21->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_22->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_23->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_Kp_24->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");

    ui->settings_bias_1->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_2->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_3->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_4->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_5->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_6->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_7->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_8->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_9->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_10->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_11->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_12->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_13->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_14->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_15->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_16->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_17->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_18->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_19->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_20->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_21->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_22->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_23->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");
    ui->settings_bias_24->setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}");

}

void MainWindow::Update_Fill_Settings()
{

    ui->settings_Kp_1->setText(QString::number(settings._settings[0].k));
    ui->settings_Kp_2->setText(QString::number(settings._settings[1].k));
    ui->settings_Kp_3->setText(QString::number(settings._settings[2].k));
    ui->settings_Kp_4->setText(QString::number(settings._settings[3].k));
    ui->settings_Kp_5->setText(QString::number(settings._settings[4].k));
    ui->settings_Kp_6->setText(QString::number(settings._settings[5].k));
    ui->settings_Kp_7->setText(QString::number(settings._settings[6].k));
    ui->settings_Kp_8->setText(QString::number(settings._settings[7].k));
    ui->settings_Kp_9->setText(QString::number(settings._settings[8].k));
    ui->settings_Kp_10->setText(QString::number(settings._settings[9].k));
    ui->settings_Kp_11->setText(QString::number(settings._settings[10].k));
    ui->settings_Kp_12->setText(QString::number(settings._settings[11].k));
    ui->settings_Kp_13->setText(QString::number(settings._settings[12].k));
    ui->settings_Kp_14->setText(QString::number(settings._settings[13].k));
    ui->settings_Kp_15->setText(QString::number(settings._settings[14].k));
    ui->settings_Kp_16->setText(QString::number(settings._settings[15].k));
    ui->settings_Kp_17->setText(QString::number(settings._settings[16].k));
    ui->settings_Kp_18->setText(QString::number(settings._settings[17].k));
    ui->settings_Kp_19->setText(QString::number(settings._settings[18].k));
    ui->settings_Kp_20->setText(QString::number(settings._settings[19].k));
    ui->settings_Kp_21->setText(QString::number(settings._settings[20].k));
    ui->settings_Kp_22->setText(QString::number(settings._settings[21].k));
    ui->settings_Kp_23->setText(QString::number(settings._settings[22].k));
    ui->settings_Kp_24->setText(QString::number(settings._settings[23].k));

    ui->settings_bias_1->setText(QString::number(settings._settings[0].b));
    ui->settings_bias_2->setText(QString::number(settings._settings[1].b));
    ui->settings_bias_3->setText(QString::number(settings._settings[2].b));
    ui->settings_bias_4->setText(QString::number(settings._settings[3].b));
    ui->settings_bias_5->setText(QString::number(settings._settings[4].b));
    ui->settings_bias_6->setText(QString::number(settings._settings[5].b));
    ui->settings_bias_7->setText(QString::number(settings._settings[6].b));
    ui->settings_bias_8->setText(QString::number(settings._settings[7].b));
    ui->settings_bias_9->setText(QString::number(settings._settings[8].b));
    ui->settings_bias_10->setText(QString::number(settings._settings[9].b));
    ui->settings_bias_11->setText(QString::number(settings._settings[10].b));
    ui->settings_bias_12->setText(QString::number(settings._settings[11].b));
    ui->settings_bias_13->setText(QString::number(settings._settings[12].b));
    ui->settings_bias_14->setText(QString::number(settings._settings[13].b));
    ui->settings_bias_15->setText(QString::number(settings._settings[14].b));
    ui->settings_bias_16->setText(QString::number(settings._settings[15].b));
    ui->settings_bias_17->setText(QString::number(settings._settings[16].b));
    ui->settings_bias_18->setText(QString::number(settings._settings[17].b));
    ui->settings_bias_19->setText(QString::number(settings._settings[18].b));
    ui->settings_bias_20->setText(QString::number(settings._settings[19].b));
    ui->settings_bias_21->setText(QString::number(settings._settings[20].b));
    ui->settings_bias_22->setText(QString::number(settings._settings[21].b));
    ui->settings_bias_23->setText(QString::number(settings._settings[22].b));
    ui->settings_bias_24->setText(QString::number(settings._settings[23].b));
    update_status_Settings_indicate();
}


void MainWindow:: Settings_read(void)
{
    Logic->Request_Get_settings(protocol);
}
void MainWindow:: Settings_Set(void)
{
    ADC_Board_hw data;

    data._settings[0].k=ui->settings_Kp_1->text().toFloat();
    data._settings[1].k=ui->settings_Kp_2->text().toFloat();
    data._settings[2].k=ui->settings_Kp_3->text().toFloat();
    data._settings[3].k=ui->settings_Kp_4->text().toFloat();
    data._settings[4].k=ui->settings_Kp_5->text().toFloat();
    data._settings[5].k=ui->settings_Kp_6->text().toFloat();
    data._settings[6].k=ui->settings_Kp_7->text().toFloat();
    data._settings[7].k=ui->settings_Kp_8->text().toFloat();
    data._settings[8].k=ui->settings_Kp_9->text().toFloat();
    data._settings[9].k=ui->settings_Kp_10->text().toFloat();
    data._settings[10].k=ui->settings_Kp_11->text().toFloat();
    data._settings[11].k=ui->settings_Kp_12->text().toFloat();
    data._settings[12].k=ui->settings_Kp_13->text().toFloat();
    data._settings[13].k=ui->settings_Kp_14->text().toFloat();
    data._settings[14].k=ui->settings_Kp_15->text().toFloat();
    data._settings[15].k=ui->settings_Kp_16->text().toFloat();
    data._settings[16].k=ui->settings_Kp_17->text().toFloat();
    data._settings[17].k=ui->settings_Kp_18->text().toFloat();
    data._settings[18].k=ui->settings_Kp_19->text().toFloat();
    data._settings[19].k=ui->settings_Kp_20->text().toFloat();
    data._settings[20].k=ui->settings_Kp_21->text().toFloat();
    data._settings[21].k=ui->settings_Kp_22->text().toFloat();
    data._settings[22].k=ui->settings_Kp_23->text().toFloat();
    data._settings[23].k=ui->settings_Kp_24->text().toFloat();

    data._settings[0].b=ui->settings_bias_1->text().toFloat();
    data._settings[1].b=ui->settings_bias_2->text().toFloat();
    data._settings[2].b=ui->settings_bias_3->text().toFloat();
    data._settings[3].b=ui->settings_bias_4->text().toFloat();
    data._settings[4].b=ui->settings_bias_5->text().toFloat();
    data._settings[5].b=ui->settings_bias_6->text().toFloat();
    data._settings[6].b=ui->settings_bias_7->text().toFloat();
    data._settings[7].b=ui->settings_bias_8->text().toFloat();
    data._settings[8].b=ui->settings_bias_9->text().toFloat();
    data._settings[9].b=ui->settings_bias_10->text().toFloat();
    data._settings[10].b=ui->settings_bias_11->text().toFloat();
    data._settings[11].b=ui->settings_bias_12->text().toFloat();
    data._settings[12].b=ui->settings_bias_13->text().toFloat();
    data._settings[13].b=ui->settings_bias_14->text().toFloat();
    data._settings[14].b=ui->settings_bias_15->text().toFloat();
    data._settings[15].b=ui->settings_bias_16->text().toFloat();
    data._settings[16].b=ui->settings_bias_17->text().toFloat();
    data._settings[17].b=ui->settings_bias_18->text().toFloat();
    data._settings[18].b=ui->settings_bias_19->text().toFloat();
    data._settings[19].b=ui->settings_bias_20->text().toFloat();
    data._settings[20].b=ui->settings_bias_21->text().toFloat();
    data._settings[21].b=ui->settings_bias_22->text().toFloat();
    data._settings[22].b=ui->settings_bias_23->text().toFloat();
    data._settings[23].b=ui->settings_bias_24->text().toFloat();

    Logic->Request_Set_Settings(protocol,&data);

}
void MainWindow:: Settings_write_Memory(void)
{
    Logic->Request_Write_settings(protocol);
    QMessageBox::information(this,"EEPROM Config","Write config to EEPROM");
}




