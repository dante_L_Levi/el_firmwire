#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "stdint.h"
#include "QTimer"
#include "SerialPort/SerialPort.h"
#include "Protocol/SProtocol_connect.h"
#include "Protocol/qsprotocol.h"
#include "Protocol/SProtocol_struct.h"

#include "CSV_Manager/CSV_Manager.h"
#include "qcustomplot.h"
#include "Graph_plot/formgraph.h"
#include <QFile>
#include <QTextStream>

#include <QPainter>

#include "Logic/ADC_Logic.h"
#include "Logic/ADC_types.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
protected:
    void paintEvent(QPaintEvent* event);

private slots:
    void update_NamePort(void);
    void Connect_Btn(void);
    void plot_Graph_Start(void);
    void write_data_Start(void);

    void serialReceived(QByteArray data);
    void receivedMsg(SProtocol_message* data);

    void Request_Board_Tim();

    void on_checkBox_EN_stateChanged(int arg1);

    void Settings_read(void);
    void Settings_Set(void);
    void Settings_write_Memory(void);

    void Request_Test_MainData();
    void Requrst_test_Firmwire();

    void slotTimerAlarm(void);
    void slotTimerAlarmCSV(void);
    void Slot_Timer_TimeOut(void);
    void btn_rs_connect_ClICK(void);

    void Update_UI_Packet_Info(double data);
private:
    Ui::MainWindow *ui;
    void SetStyles_UI(void);
    void Link_event_UI(void);
    void Update_ComBoBox_Config(void);
    void Load_dataGraph(void);
    void CSV_Load_NameTitle(void);
    void Update_Fill_Settings(void);
    void update_status_Settings_indicate(void);


private:
    QTimer *timer;
    QTimer *TimerAlarm;
    QTimer *TimerAlarmCSV;
    QTime *timeWork;
    bool count_ConnectButton=false;
    SerialPort *serial;
    QSProtocol *protocol;
    bool status_start_request=false;
    uint8_t count_request=1;
    ADC_Logic* Logic;
    CSV_Manager *_csv_hundler;
    bool status_save_csv=false;

    double count_packet_CSV=0;
    formgraph *graphOutput;

};
#endif // MAINWINDOW_H
