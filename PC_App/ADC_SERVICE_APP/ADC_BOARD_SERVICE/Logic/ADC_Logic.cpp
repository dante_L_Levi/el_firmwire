#include "ADC_Logic.h"

ADC_Logic::ADC_Logic(QObject *parent) : QObject(parent)
{

}

void ADC_Logic::Set_ID(uint16_t Id_dev)
{
    ID=Id_dev;
}

void ADC_Logic::Parse_Data_RX(sprotocol_commands cmd, SProtocol_message *data)
{
    switch(cmd)
    {
        case SP_CMD_FIRMWIRE:
        {
        recieve_data1=reinterpret_cast<Recieve_data1_Model_DataBoard*>(data->data);
        //memcpy(recieve_data1,data->data,data->length);
        count_Packet++;
        Update_Model(recieve_data1);
        qDebug()<<"[parse]";
            break;
        }

        case SP_CMD_GET_INPUTS:
        {
        recieve_data2=reinterpret_cast<Recieve_data2_Model_MainData*>(data->data);
        count_Packet++;
        Update_Model(recieve_data2);
        qDebug()<<"[parse]";
            break;
        }

        case SP_CMD_GET_SETTINGS:
        {
            recieve_data3=reinterpret_cast<Recieve_data3_Model_Settings*>(data->data);

            Update_Model(recieve_data3);
            qDebug()<<"[parse]";
            break;
        }
    default:
        break;

    }
     count_Packet++;
     emit Update_Packet_Count(count_Packet);
}

void ADC_Logic::Update_UI(sprotocol_commands cmd, QLabel *data)
{
    switch(cmd)
    {
        case SP_CMD_FIRMWIRE:
        {
        qDebug()<<"[update]";
        char dataName[8]={0};
        for(uint8_t i=0;i<8;i++)
        {
            dataName[i]=_mainData.Board_name[i];
            if(_mainData.Board_name[i]=='d')
            {
                break;
            }
        }
        //memcpy(dataName,_mainData.Board_name,8);
       qDebug()<<"NAME:"<<dataName;
       QString dataBoard=QString(
                       "%1:%5\n"
                       "%2:%6\n"
                       "%3:%7\n"
                       "%4:%8 ms\n"

                    ).arg("ID")
                    .arg("Version")
                    .arg("NameBoard")
                    .arg("Time")
                    .arg(_mainData.Id_Recieve)
               .arg((const char*)_mainData.firmwire)
               .arg(dataName)
               .arg(_mainData.time_ms);

                data->setText(dataBoard) ;

    break;
            break;
        }
        case SP_CMD_GET_INPUTS:
        {
        qDebug()<<"[update]";
         QString dataMain;
        for(int i=0;i<24;i++)
        {
            QString temp=QString("%1:\t\t%2\n")
                    .arg(DataName[i])
                    .arg(_mainData._MainData.all_data[i]);
            dataMain+=temp;
        }
        dataMain+=QString("%1\t\t:%2\n")
                .arg("Mode")
                .arg(DataNameStatusWork[_mainData.mode]);
        data->setText(dataMain);
            break;
        }


        default:
            break;
    }
}

void ADC_Logic::Get_Settings_data(ADC_Board_hw *temp)
{
    temp=&_mainData._setting;
    qDebug()<<temp;
}

void ADC_Logic::Request_RS_Connect(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_CONNECT;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Get_Config_Board(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_FIRMWIRE;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Get_MainData(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_GET_INPUTS;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Get_settings(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_GET_SETTINGS;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Write_settings(QSProtocol *protocol)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_ADC_COEF_SAVE;
    uint8_t dt=0xFF;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Set_Settings(QSProtocol *protocol, ADC_Board_hw *data)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_SET_SETTINGS;
    uint8_t* dt=reinterpret_cast<uint8_t*>(data);
    sendMsg.length=sizeof(ADC_Board_hw);
    sendMsg.data=dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Request_Set_Enable_Control(QSProtocol *protocol, bool status)
{
    SProtocol_message sendMsg;
    sendMsg.command=SP_CMD_SET_MODE;
    uint8_t dt=status;
    sendMsg.length=sizeof(dt);
    sendMsg.data=&dt;
    protocol->transmitData(&sendMsg);
}

void ADC_Logic::Update_Model(Recieve_data1_Model_DataBoard *data)
{
    memcpy(_mainData.firmwire,data->firmwire,8);
    memcpy(_mainData.Board_name,data->Name,8);
    _mainData.Id_Recieve=data->ID_temp;
    _mainData.time_ms=data->time_ms;

}

void ADC_Logic::Update_Model(Recieve_data2_Model_MainData *data)
{
    for(int i=0;i<24;i++)
     {
           _mainData._MainData.all_data[i]=data->_MainData.all_data[i];
     }
    _mainData.mode=data->mode;

}

void ADC_Logic::Update_Model(Recieve_data3_Model_Settings *data)
{
    _mainData._setting=data->_setting_data;
}

float* ADC_Logic:: Get_ADC_Value(int index)
{
    if(index<0) return 0;
    return &_mainData._MainData.all_data[index];
}
