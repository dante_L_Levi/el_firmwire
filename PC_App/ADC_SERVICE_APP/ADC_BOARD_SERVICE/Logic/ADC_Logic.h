#ifndef ADC_LOGIC_H
#define ADC_LOGIC_H

#include <QObject>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QSerialPort"
#include "SerialPort/SerialPort.h"
#include "Protocol/SProtocol_connect.h"
#include "Protocol/SProtocol_struct.h"
#include "Protocol/qsprotocol.h"
#include "ADC_types.h"

class ADC_Logic : public QObject
{
    Q_OBJECT
public:
    explicit ADC_Logic(QObject *parent = nullptr);


    void Set_ID(uint16_t Id_dev);
    void Parse_Data_RX(sprotocol_commands cmd,SProtocol_message *data);
    void Update_UI(sprotocol_commands cmd,QLabel *data);
    void Get_Settings_data(ADC_Board_hw* temp);

    void Request_RS_Connect(QSProtocol *protocol);
    void Request_Get_Config_Board(QSProtocol *protocol);
    void Request_Get_MainData(QSProtocol *protocol);
    void Request_Get_settings(QSProtocol *protocol);
    void Request_Write_settings(QSProtocol *protocol);
    void Request_Set_Settings(QSProtocol *protocol,ADC_Board_hw* data);
    void Request_Set_Enable_Control(QSProtocol *protocol,bool status);

    QStringList DataName=
    {
        "AIN1","AIN2","AIN3","AIN4",
        "AIN5","AIN6","AIN7","AIN8",
        "AIN9","AIN10","AIN11","AIN12",
        "AIN13","AIN14","AIN15","AIN16",
        "AIN17","AIN18","AIN19","AIN20",
        "AIN21","AIN22","AIN23","AIN24"
    };

    float* Get_ADC_Value(int index);




signals:
    void Update_Packet_Count(double data);

private:
    Recieve_data1_Model_DataBoard       *recieve_data1;
    Recieve_data2_Model_MainData        *recieve_data2;
    Recieve_data3_Model_Settings        *recieve_data3;

    uint16_t ID=0;
    double count_Packet=0;

    void Update_Model(Recieve_data1_Model_DataBoard *data);
    void Update_Model(Recieve_data2_Model_MainData *data);
    void Update_Model(Recieve_data3_Model_Settings *data);

    Model_MainData_def _mainData;

    QStringList DataNameStatusWork=
    {
        "MODE_WORK_INIT",
        "MODE_WORK",
        "MODE_SETUP_INIT",
        "MODE_SETUP",
        "MODE_TEST_INIT",
        "MODE_TEST",

        "MODE_COUNT",

    };








};

#endif // ADC_LOGIC_H
