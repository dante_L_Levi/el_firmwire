#ifndef ADC_TYPES_H
#define ADC_TYPES_H


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "QStringList"

#define ADC_CHANNEL_COUNT       24

typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;

#pragma pack(push, 1)
typedef union
{

    struct
       {
           float AIN0;
           float AIN1;
           float AIN2;
           float AIN3;
           float AIN4;
           float AIN5;
           float AIN6;
           float AIN7;
           float AIN8;
           float AIN9;
           float AIN10;
           float AIN11;
           float AIN12;
           float AIN13;
           float AIN14;
           float AIN15;
           float AIN16;
           float AIN17;
           float AIN18;
           float AIN19;
           float AIN20;
           float AIN21;
           float AIN22;
           float AIN23;
       }fields;
       float all_data[24];

}analog_sense_s;//mV

#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint16_t ID_temp;
    uint8_t Name[8];
    uint8_t firmwire[8];
    uint32_t time_ms;

}Recieve_data1_Model_DataBoard;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    analog_sense_s      _MainData;
    _mode_system_type mode;

}Recieve_data2_Model_MainData;

#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
    float b;
    float k;
}map_t;

typedef struct
{
    map_t _settings[24];


}ADC_Board_hw;


typedef struct
{

    ADC_Board_hw            _setting_data;

}Recieve_data3_Model_Settings;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
    uint8_t firmwire[8];
    char Board_name[8];
    uint16_t Id_Recieve;
    uint32_t time_ms;

    mode_system_e mode;
    ADC_Board_hw            _setting;
    analog_sense_s          _MainData;

}Model_MainData_def;
#pragma pack(pop)



#endif // ADC_TYPES_H
