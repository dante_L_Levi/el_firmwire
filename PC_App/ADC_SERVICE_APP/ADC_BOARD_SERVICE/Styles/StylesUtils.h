#ifndef STYLESUTILS_H
#define STYLESUTILS_H

#include "string.h"
#include <QString>
#include <QMainWindow>


class StylesUtils
{
public:
    static QString getWindow_Style();
   static QString getStyle_LabelText_Config_Part();
   static QString getStyle_LabelText_Firmwire_Part();
   static QString getStyle_LabelText_MainFill_Part();
   static QString getStyle_Button_Config_Part();
   static QString getStyle_ComboBox_Config_Part();
   static QString getStyle_Button_MainFill_Part();
   static QString getMainFill_Tab();
   static QString getLabelStyle();
   static void Set_Image_BackGround_Default(QMainWindow *dt);
};

#endif // STYLESUTILS_H
