#include "StylesUtils.h"
#include "QMainWindow"

/******************************Setup default Image BackGround***********************/
void StylesUtils:: Set_Image_BackGround_Default(QMainWindow *dt)
{
    QPixmap bkgnd(":/images/images/MainFone.jpg");
    bkgnd = bkgnd.scaled(dt->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    dt->setPalette(palette);
}


QString StylesUtils::getWindow_Style()
{
    return  "QMainWindow{"
            "background-image:url(:/images/images/MainFone.jpg)"
            "}";
}

QString StylesUtils::getStyle_LabelText_Config_Part()
{
    return "QLabel"
   "{"
       "color:#fff;"
       "font-size:12px;"
           "}";
}

QString StylesUtils::getStyle_LabelText_Firmwire_Part()
{
    return "QLabel"
   "{"
       "color:#fff;"
       "font-family:'Times New Roman';"
       "font-size:12px;"
       "font:italic;"
           "}";
}

QString StylesUtils::getStyle_Button_Config_Part()
{
    return "QPushButton{"
            "color:#fff;"
            "background:none;"
            "border:none;"
            "border-radius:8px;"
            "background-color: qlineargradient(spread:pad, x1:0.03, y1:0.052, x2:1, y2:1,"
           " stop:0.0199005 rgba(0, 68, 208, 255),"
           " stop:1 rgba(255, 255, 255, 255));"
            "font-family:'Times New Roman';"
            "font-size:16px;"
                      "}"
            "QPushButton::hover{"
           "background-color:qlineargradient(spread:pad, x1:0.03, y1:0.052, x2:1, y2:1, stop:0.542289 rgba(0, 50, 151, 255),"
           " stop:1 rgba(255, 255, 255, 255));"
           "}"
           "QPushButton::pressed{"
           "background-color: qlineargradient(spread:pad, x1:0.03, y1:0.052, x2:1, y2:1,"
          " stop:0.0199005 rgba(0, 68, 208, 255),"
          " stop:1 rgba(255, 255, 255, 255));"
           "}";
}

QString StylesUtils::getStyle_ComboBox_Config_Part()
{
    return "QComboBox{"
            "color:#000;"
            "background:none;"
            "border:none;"
            "border-radius:4px;"
            "font-family:'Times New Roman';"
            "font-size:16px;"           
           "}";
}

QString StylesUtils::getMainFill_Tab()
{
    return "QTabBar::tab"
           "{"
           "background-color: qlineargradient(spread:pad, x1:0.830846,"
           " y1:0.211, x2:1, y2:1, stop:0"
           " rgba(112, 0, 181, 255),"
           " stop:1 rgba(255, 255, 255, 255));"
            "}"
            "QTabBar::tab:selected, "
            "QTabBar::tab:hover "
            "{"
                "border-top-color: #1D2A32;"
               "border-color: #40494E;"
                "color: black;"
                "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #C1D8E8, stop: 1 #F0F5F8);"
            "}";
}

QString StylesUtils::getLabelStyle()
{
    return "QLabel{"
            "color:#FFF;"
            "font:italic;"
            "font-family:'Times New Roman';"
            "font-size:18px;"
             "}";

}
