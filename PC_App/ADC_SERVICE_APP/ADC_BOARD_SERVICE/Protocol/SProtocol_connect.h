/*
 * SProtocol_parser.h
 *
 *  Created on: 19 feb. 2020
 *      Author: peresaliak
 */

#ifndef SPROTOCOL_SPROTOCOL_CONNECT_H_
#define SPROTOCOL_SPROTOCOL_CONNECT_H_

#define SPROTOCOL_COMMAND_GO_BOOT       0xFF
#define SPROTOCOL_COMMAND_ENABLE        0xFE

#define GO_TO_BOOTLOADER_RS485_KEY      0xf2784c28

#include <stdint.h>

typedef enum{
    SP_CMD_RESERVED_0,
        SP_CMD_CONNECT,
        SP_CMD_SET_MODE,
        SP_CMD_MSG_ERROR,
        SP_CMD_MSG_SERVICE,
        SP_CMD_SET_SETTINGS,
        SP_CMD_GET_SETTINGS,
        SP_CMD_ECHO_PAYLOAD,
        SP_CMD_FIRMWIRE,
        SP_CMD_GET_INPUTS,
        SP_CMD_SEND_INPUTS,
        SP_CMD_SET_START_MODE,
        SP_CMD_SET_START_PWM,
        SP_CMD_SET_RESISTORS,
        SP_CMD_GET_RESISTORS,
        SP_CMD_SEND_RESISTORS,
        SP_CMD_ADC_CALIBRATION,
        SP_CMD_ADC_COEF_SAVE,
        SP_CMD_SET_DAC_OUTPUT,
        SPROTOCOL_COMMANDS_COUNT
}sprotocol_commands;

typedef struct
{
    uint8_t length;
    uint8_t command;
    uint8_t* data;
}SProtocol_message;

typedef enum
{
    RX_WAIT_ID_PART1,
    RX_WAIT_ID_PART2,
    RX_WAIT_LENGTH,
    RX_COLLECT_DATA,
    RX_TO_TX_STATE,
    TX_MODE
}SProtocol_states;

#endif /* SPROTOCOL_SPROTOCOL_CONNECT_H_ */
