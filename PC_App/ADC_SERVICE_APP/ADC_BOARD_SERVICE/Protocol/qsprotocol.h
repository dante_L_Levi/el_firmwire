#ifndef QSPROTOCOL_H
#define QSPROTOCOL_H

#include <stdint.h>
#include <stdbool.h>
#include <QString>
#include <QByteArray>
#include <QObject>
#include "SProtocol_connect.h"

class QSProtocol : public QObject
{
    Q_OBJECT

public:
    explicit QSProtocol();
    explicit QSProtocol(uint16_t id);
    void setID(uint16_t id);
    void sendGoBoot();
    void addData(QByteArray data);
    void transmitData(SProtocol_message* mdg);

private:
    uint16_t getCrc16(uint8_t * pcBlock, uint16_t len);
    void parseRX(QByteArray data);

public slots:
    void serialReceived(QByteArray data);

signals:
    void serialTransmit(QByteArray* data);
    void receivedMsg(SProtocol_message* msg);

private:
    bool startSystem;
    QByteArray inputData;
    uint16_t idMaster;
    uint16_t idSlave;
    SProtocol_states stateReceived;
    QByteArray collectionData;
    uint16_t lengthExpectid;
    QList<SProtocol_message> getMsgList;
};

#endif // QSPROTOCOL_H
