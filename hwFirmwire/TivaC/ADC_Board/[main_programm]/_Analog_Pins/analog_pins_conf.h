#include "headers.h"
/**********************************************************
 * Author: FROLOV S
 *
 * Description:
 * Go to configuration section for setting configuration
 **********************************************************/

#define ADC_COUNT               2
#define MAX_COUNT_STEP          8
#define UPDATE_FRQ              1000

typedef struct
{
    uint32_t adc_base;                  //periphery base
    uint32_t adc_trigger;               //trigger
    bool    auto_reload;                //timer reload  after conversion
    uint8_t count_steps;                //count steps in sequence
    uint8_t an_in_step[MAX_COUNT_STEP]; //analog input in step
    float multi[MAX_COUNT_STEP];        //multiplier voltage
    float offset[MAX_COUNT_STEP];       //offset voltage
}ADC_config_s;


ADC_config_s ADC_config[ADC_COUNT] =
{
     {ADC0_BASE,
      ADC_TRIGGER_PROCESSOR,
      false,
      4,
      3,            1,             5,             6,             0,             0,             0,           0,
      -0.00537f,    -0.00537f,     -0.00537f,    -0.00537f,     -0.00537f,     -0.00537f,     -0.00537f,   -0.00537f,
      2047.5f,      2047.5f,       2047.5f,      2047.5f,       2047.5f,       2047.5f,       2047.5f,     2047.5f},          //steps ADC0

     {ADC1_BASE,
      ADC_TRIGGER_PROCESSOR,
      false,
      8,
      2,           0,              10,              8,             9,              11,         7,           4,              //steps ADC1
      -0.00537f,    -0.00537f,     -0.00537f,     -0.00537f,     -0.00537f,     -0.00537f,     -0.00537f,   -0.00537f,
      2047.5f,      2047.5f,       2047.5f,       2047.5f,       2047.5f,       2047.5f,       2047.5f,     2047.5f}
};

typedef struct
{
    uint32_t port;
    uint32_t pin;
}ADC_pins_s;

#ifdef PART_TM4C123GH6PM
ADC_pins_s adc_pin_s[] =
{
    GPIO_PORTE_BASE,GPIO_PIN_3,  //0
    GPIO_PORTE_BASE,GPIO_PIN_2,  //1
    GPIO_PORTE_BASE,GPIO_PIN_1,  //2
    GPIO_PORTE_BASE,GPIO_PIN_0,  //3
    GPIO_PORTD_BASE,GPIO_PIN_3,  //4
    GPIO_PORTD_BASE,GPIO_PIN_2,  //5
    GPIO_PORTD_BASE,GPIO_PIN_1,  //6
    GPIO_PORTD_BASE,GPIO_PIN_0,  //7
    GPIO_PORTE_BASE,GPIO_PIN_5,  //8
    GPIO_PORTE_BASE,GPIO_PIN_4,  //9
    GPIO_PORTB_BASE,GPIO_PIN_4,  //10
    GPIO_PORTB_BASE,GPIO_PIN_5,  //11
};
#endif

#ifdef PART_TM4C129ENCPDT
ADC_pins_s adc_pin_s[] =
{
    GPIO_PORTE_BASE,GPIO_PIN_3,  //0
    GPIO_PORTE_BASE,GPIO_PIN_2,  //1
    GPIO_PORTE_BASE,GPIO_PIN_1,  //2
    GPIO_PORTE_BASE,GPIO_PIN_0,  //3
    GPIO_PORTD_BASE,GPIO_PIN_7,  //4
    GPIO_PORTD_BASE,GPIO_PIN_6,  //5
    GPIO_PORTD_BASE,GPIO_PIN_5,  //6
    GPIO_PORTD_BASE,GPIO_PIN_4,  //7
    GPIO_PORTE_BASE,GPIO_PIN_5,  //8
    GPIO_PORTE_BASE,GPIO_PIN_4,  //9
    GPIO_PORTB_BASE,GPIO_PIN_4,  //10
    GPIO_PORTB_BASE,GPIO_PIN_5,  //11
    GPIO_PORTD_BASE,GPIO_PIN_3,  //12
    GPIO_PORTD_BASE,GPIO_PIN_2,  //13
    GPIO_PORTD_BASE,GPIO_PIN_1,  //14
    GPIO_PORTD_BASE,GPIO_PIN_0,  //15
    GPIO_PORTK_BASE,GPIO_PIN_0,  //16
    GPIO_PORTK_BASE,GPIO_PIN_1,  //17
    GPIO_PORTK_BASE,GPIO_PIN_2,  //18
    GPIO_PORTK_BASE,GPIO_PIN_3,  //19
};
#endif

#ifdef PART_TM4C123GH6PGE
ADC_pins_s adc_pin_s[] =
{
    GPIO_PORTE_BASE,GPIO_PIN_3,  //0
    GPIO_PORTE_BASE,GPIO_PIN_2,  //1
    GPIO_PORTE_BASE,GPIO_PIN_1,  //2
    GPIO_PORTE_BASE,GPIO_PIN_0,  //3
    GPIO_PORTD_BASE,GPIO_PIN_7,  //4
    GPIO_PORTD_BASE,GPIO_PIN_6,  //5
    GPIO_PORTD_BASE,GPIO_PIN_5,  //6
    GPIO_PORTD_BASE,GPIO_PIN_4,  //7
    GPIO_PORTE_BASE,GPIO_PIN_5,  //8
    GPIO_PORTE_BASE,GPIO_PIN_4,  //9
    GPIO_PORTB_BASE,GPIO_PIN_4,  //10
    GPIO_PORTB_BASE,GPIO_PIN_5,  //11
    GPIO_PORTD_BASE,GPIO_PIN_3,  //12
    GPIO_PORTD_BASE,GPIO_PIN_2,  //13
    GPIO_PORTD_BASE,GPIO_PIN_1,  //14
    GPIO_PORTD_BASE,GPIO_PIN_0,  //15
    GPIO_PORTK_BASE,GPIO_PIN_0,  //16
    GPIO_PORTK_BASE,GPIO_PIN_1,  //17
    GPIO_PORTK_BASE,GPIO_PIN_2,  //18
    GPIO_PORTK_BASE,GPIO_PIN_3,  //19
    GPIO_PORTE_BASE,GPIO_PIN_7,  //20
    GPIO_PORTE_BASE,GPIO_PIN_6,  //21
    GPIO_PORTP_BASE,GPIO_PIN_1,  //22
    GPIO_PORTP_BASE,GPIO_PIN_0   //23
};

#endif


