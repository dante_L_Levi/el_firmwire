/*
 * adc_24ch_config.h
 *
 *  Created on: 18 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef ANALOG_PINS_ADC_24CH_ADC_24CH_CONFIG_H_
#define ANALOG_PINS_ADC_24CH_ADC_24CH_CONFIG_H_


#define ADC_COUNT               2
#define MAX_COUNT_CH_ADC        16

#include "headers.h"
#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"


typedef struct
{
    uint32_t adc_base;                  //periphery base
    uint32_t  SYSCTL_PERIPH;
    uint32_t adc_trigger;               //trigger
    uint32_t ui32SeqeNum[MAX_COUNT_CH_ADC];
    uint32_t ui32Step[MAX_COUNT_CH_ADC];
    uint32_t channel[MAX_COUNT_CH_ADC];

}ADC_settings_s;

ADC_settings_s ADC_config_data[ADC_COUNT]=
{
 {
      ADC0_BASE,
      SYSCTL_PERIPH_ADC0,
      ADC_TRIGGER_TIMER,
      0, 0, 0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  2,  2,  2,  2,
      0, 1, 2,  3,  4,  5,  6,  7,  0,  1,  2,  3,  0,  1,  2,  3,
      ADC_CTL_CH0,ADC_CTL_CH1,ADC_CTL_CH2,ADC_CTL_CH3,ADC_CTL_CH4,ADC_CTL_CH5,ADC_CTL_CH6,(ADC_CTL_CH7|ADC_CTL_IE | ADC_CTL_END),
      ADC_CTL_CH8,ADC_CTL_CH9,ADC_CTL_CH10,(ADC_CTL_CH11|ADC_CTL_IE| ADC_CTL_END),
      ADC_CTL_CH12,ADC_CTL_CH13,ADC_CTL_CH14,(ADC_CTL_CH15|ADC_CTL_IE| ADC_CTL_END)
 },

 {
       ADC1_BASE,
       SYSCTL_PERIPH_ADC1,
       ADC_TRIGGER_TIMER,
       0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
       0, 1, 2,  3,  4,  5,  6,  7,  0,  0,  0,  0,  0,  0,  0,  0,
       ADC_CTL_CH16,ADC_CTL_CH17,ADC_CTL_CH18,ADC_CTL_CH19,ADC_CTL_CH20,ADC_CTL_CH21,ADC_CTL_CH22,(ADC_CTL_CH23|ADC_CTL_IE | ADC_CTL_END),
       0,0,0,(0| 0),
       0,0,0,(0| 0)
  }

};


#endif /* ANALOG_PINS_ADC_24CH_ADC_24CH_CONFIG_H_ */
