/*
 * adc_24ch_gh6pge.h
 *
 *  Created on: 18 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef ANALOG_PINS_ADC_24CH_ADC_24CH_GH6PGE_H_
#define ANALOG_PINS_ADC_24CH_ADC_24CH_GH6PGE_H_

#include "stdint.h"

typedef struct
{
    int32_t ch0;
    int32_t ch1;
    int32_t ch2;
    int32_t ch3;
    int32_t ch4;
    int32_t ch5;
    int32_t ch6;
    int32_t ch7;
    int32_t ch8;
    int32_t ch9;
    int32_t ch10;
    int32_t ch11;
    int32_t ch12;
    int32_t ch13;
    int32_t ch14;
    int32_t ch15;
    int32_t ch16;
    int32_t ch17;
    int32_t ch18;
    int32_t ch19;
    int32_t ch20;
    int32_t ch21;
    int32_t ch22;
    int32_t ch23;
} analog_sens_struct;

void Configure_adc(uint32_t timer_period);
void AnalogSens_GetData(analog_sens_struct * res);
void SetCallback_Interrupt(void (*InitFunction)(void));


#endif /* ANALOG_PINS_ADC_24CH_ADC_24CH_GH6PGE_H_ */
