/*
 * adc_24ch_gh6pge.c
 *
 *  Created on: 18 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "adc_24ch_gh6pge.h"
#include "adc_24ch_config.h"




typedef struct
{
    uint32_t timer_period;
    void (*InitFunction)(void);

    bool state;
    bool state1;
    bool state2;
    bool state3;

}adc_settings_Data;

static volatile analog_sens_struct results;
static volatile adc_settings_Data  _conf_ADC;

static void adcInterrupthandler(void)
{
    _conf_ADC.state = ADCIntStatus(ADC_config_data[0].adc_base, 0, true);
    _conf_ADC.state1 = ADCIntStatus(ADC_config_data[0].adc_base, 1, true);
    _conf_ADC.state2 = ADCIntStatus(ADC_config_data[0].adc_base, 2, true);
    _conf_ADC.state3 = ADCIntStatus(ADC_config_data[1].adc_base, 0, true);
    if (_conf_ADC.state == true)
    {
            ADCIntClear(ADC_config_data[0].adc_base, 0);
            ADCSequenceDataGet(ADC_config_data[0].adc_base, 0, (uint32_t *) &results.ch0);
    }
    if (_conf_ADC.state1 == true)
    {
            ADCIntClear(ADC_config_data[0].adc_base, 1);
            ADCSequenceDataGet(ADC_config_data[0].adc_base, 1, (uint32_t *) &results.ch8);
    }
    if (_conf_ADC.state2 == true)
    {
            ADCIntClear(ADC_config_data[0].adc_base, 2);
            ADCSequenceDataGet(ADC_config_data[0].adc_base, 2, (uint32_t *) &results.ch12);
     }
     if (_conf_ADC.state3 == true)
     {
            ADCIntClear(ADC_config_data[1].adc_base, 0);
            ADCSequenceDataGet(ADC_config_data[1].adc_base, 0, (uint32_t *) &results.ch16);
      }

    _conf_ADC.InitFunction();
}

static void timerInit(uint32_t timer_period)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER7);
    TimerConfigure(TIMER7_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER7_BASE, TIMER_A, timer_period);
    TimerControlTrigger(TIMER7_BASE, TIMER_A, true);
    TimerEnable(TIMER7_BASE, TIMER_A);
}




void SetCallback_Interrupt(void (*InitFunction)(void))
{
    _conf_ADC.InitFunction=InitFunction;
}


void Configure_adc(uint32_t timer_period)
{
    for(int i=0;i<ADC_COUNT;i++)
        SysCtlPeripheralEnable(ADC_config_data[i].SYSCTL_PERIPH);

    while (!SysCtlPeripheralReady(ADC_config_data[0].SYSCTL_PERIPH)
                | !SysCtlPeripheralReady(ADC_config_data[1].SYSCTL_PERIPH));

    ADCSequenceConfigure(ADC_config_data[0].adc_base, 0, ADC_config_data[0].adc_trigger, 0); //make adc sampling forever
    ADCSequenceConfigure(ADC_config_data[0].adc_base, 1, ADC_config_data[0].adc_trigger, 1); //make adc sampling forever
    ADCSequenceConfigure(ADC_config_data[0].adc_base, 2, ADC_config_data[0].adc_trigger, 2); //make adc sampling forever
    ADCSequenceConfigure(ADC_config_data[1].adc_base, 0, ADC_config_data[1].adc_trigger, 0); //make adc sampling forever


    for(uint8_t i=0;i<ADC_COUNT;i++)
    {
        if(i==0)//ADC_0
        {
            for(uint8_t j=0;j<MAX_COUNT_CH_ADC;j++)
            {
                ADCSequenceStepConfigure(ADC_config_data[i].adc_base,
                                         ADC_config_data[i].ui32SeqeNum[j],
                                         ADC_config_data[i].ui32Step[j],
                                         ADC_config_data[i].channel[j]);
            }
        }
        else if(i==1)//ADC1
        {
            for(uint8_t j=0;j<MAX_COUNT_CH_ADC/2;j++)
            {
                ADCSequenceStepConfigure(ADC_config_data[i].adc_base,
                                         ADC_config_data[i].ui32SeqeNum[j],
                                         ADC_config_data[i].ui32Step[j],
                                         ADC_config_data[i].channel[j]);
            }
        }
    }


    //interrupt settings
    ADCIntRegister(ADC_config_data[0].adc_base, 0, adcInterrupthandler);
    ADCIntEnable(ADC_config_data[0].adc_base, 0);
    ADCSequenceEnable(ADC_config_data[0].adc_base, 0);
    ADCIntRegister(ADC_config_data[0].adc_base, 1, adcInterrupthandler);
    ADCIntEnable(ADC_config_data[0].adc_base, 1);
    ADCSequenceEnable(ADC_config_data[0].adc_base, 1);
    ADCIntRegister(ADC_config_data[0].adc_base, 2, adcInterrupthandler);
    ADCIntEnable(ADC_config_data[0].adc_base, 2);
    ADCSequenceEnable(ADC_config_data[0].adc_base, 2);
    ADCIntRegister(ADC_config_data[1].adc_base, 0, adcInterrupthandler);
    ADCIntEnable(ADC_config_data[1].adc_base, 0);
    ADCSequenceEnable(ADC_config_data[1].adc_base, 0);

    timerInit(timer_period);

}


void AnalogSens_GetData(analog_sens_struct* res)
{
   *res=results;

}


