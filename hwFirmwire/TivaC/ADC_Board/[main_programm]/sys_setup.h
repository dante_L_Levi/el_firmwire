/*
 * sys_setup.h
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: frolov
 */
#include "headers.h"


#ifndef SYS_SETUP_H_
#define SYS_SETUP_H_

void Sys_Setup(void);
uint32_t Sys_freq(void);
uint8_t Sys_pinoutSet(void);

#endif /* SYS_SETUP_H_ */
