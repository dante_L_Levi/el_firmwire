/*
 * example_bridge.c
 *
 *  Created on: 24 feb. 2020
 *      Author: peresaliak
 */

#include "headers.h"

#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_Timers/timers.h"
#include "_Timers/timers.h"
#include "_RS485/rs485.h"
#include "_SProtocol/SProtocol.h"
#include "_SProtocol/SProtocol_connect.h"
#include "_LEDs/leds.h"
#include "sys_setup.h"
#include "settings.h"
#include "string.h"
#include "libMicro/uart/uart.h"

static int tim_proc; //interrupt occur flag for timer

bool startBridgeFlag;

static uart_channel_t my_channel;
static uint8_t rx_buffer[128];
static uint8_t tx_buffer[128];

static void Callback_Timer(void);
static void echoFunction(SProtocol_message* item);
static void startBridge(SProtocol_message* msg);
static void initUART0();
static void bridgeStep();

int main_SP_example_bridge(void)
{
  //  (void) _board_id[0];
    tim_proc=0;
    Sys_Setup();

    //InitWatchDog();
    //PermanentLockDebug();
    WriteBoardID();
    TIM_Set_Callback(2,Callback_Timer);
    Timers_Init();
    initUART0();
    RS485_Init(38400, *rs485_id, SProtocol_parserRX, 0, SProtocol_endRX);
    SProtocol_init(*rs485_id, RS485_dataTx);

    SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&echoFunction);
    SProtocol_setFunction(SP_CMD_BRIDGE_CH_UART0,&startBridge);

    uint8_t timeCounnt=0;

    while (1)
    {
        if(tim_proc)
        {
            tim_proc=0;

            bridgeStep();

            SProtocol_analysis();

            timeCounnt++;
            if(timeCounnt>=20){
                timeCounnt=0;
                SProtocol_message mess;
                uint8_t dataTx[20];
                mess.command=SP_CMD_DATA_OUT;
                mess.data=dataTx;
                mess.length=10;
                for(int i=0;i<10;i++)
                    mess.data[i]=i;
                SProtocol_addTxMessage(&mess);
            }

            LED_Blink_RG(5);
        }
    }

}

// example bridges UART0
static void bridgeStep()
{
    uint8_t data[256];
    if(startBridgeFlag){
        int16_t length=RS485_getBuffer(data);
        if(length>0){
            Uart_writeBytes(&my_channel, data, length);
        }
        int16_t i = Uart_rxAvailable(&my_channel);
        uint8_t tmp[i];
        Uart_readBytes(&my_channel, tmp, i);
        RS485_dataTx(tmp, i);
    }
}

static void echoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);
}

static void startBridge(SProtocol_message* msg)
{
    if(msg->data[0]==1){
        SProtocol_startAnalysis(false);
        startBridgeFlag=true;
        //clear buffers
        uint8_t tmpData[256];
        int16_t i = Uart_rxAvailable(&my_channel);
        Uart_readBytes(&my_channel, tmpData, i);
        i=RS485_getBuffer(tmpData);
    }
    else{
        SProtocol_startAnalysis(true);
        startBridgeFlag=false;
    }
}

static void initUART0()
{
    SysCtlClockFreqSet(
            (SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL
                    | SYSCTL_CFG_VCO_480),
            120000000);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    //
    // Enable processor interrupts.
    //
    IntMasterEnable();

    //
    // Set GPIO A0 and A1 as UART pins.
    //
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    Uart_init(&my_channel, 38400, 120000000, UART0, rx_buffer,
              sizeof(rx_buffer), tx_buffer, sizeof(tx_buffer));
}

//Callback func for selected timer
static void Callback_Timer(void)
{
    tim_proc=1;
}





