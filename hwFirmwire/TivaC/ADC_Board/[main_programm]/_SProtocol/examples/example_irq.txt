/*
 * example.c
 *
 *  Created on: 5 feb. 2020
 *      Author: peresaliak
 */

#include "headers.h"

#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_Timers/timers.h"
#include "_Timers/timers.h"
#include "_RS485/rs485.h"
#include "_SProtocol/SProtocol.h"
#include "_SProtocol/SProtocol_connect.h"
#include "_LEDs/leds.h"
#include "sys_setup.h"
#include "settings.h"
#include "string.h"

static int tim_proc; //interrupt occur flag for timer

static void Callback_Timer(void);
static void echoFunction(SProtocol_message* item);


int main_SP_example_irq(void)
{
    SysCtlMOSCConfigSet(SYSCTL_MOSC_HIGHFREQ);
    SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                        SYSCTL_OSC_MAIN |
                        SYSCTL_USE_PLL |
                        SYSCTL_CFG_VCO_480),
                       SYS_CLOCK);
    Sys_Setup();
    //InitWatchDog();
    //PermanentLockDebug();
   // WriteBoardAddress();
    (void) _board_id[0];

    Timers_Init();
    TIM_Set_Callback(2,Callback_Timer);

    RS485_Init(38400, *rs485_id, SProtocol_parserRX, 0, SProtocol_endRX);
    SProtocol_init(*rs485_id, RS485_dataTx);
    SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&echoFunction);

    SProtocol_message mess;
    uint8_t dataTx[20];
    tim_proc=0;

    while (1)
    {
        if(tim_proc)
        {
            tim_proc=0;

            mess.command=SP_CMD_DATA_OUT;
            mess.data=dataTx;
            mess.length=10;
            for(int i=0;i<10;i++)
                mess.data[i]=i;
            SProtocol_addTxMessage(&mess);

            SProtocol_analysis();

            LED_Blink_RG(5);
        }
    }
}

//echo function
static void echoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);
}

//Callback func for selected timer
static void Callback_Timer(void)
{
   tim_proc=1;
}



