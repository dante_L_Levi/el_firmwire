/*
 * ADG5401_settings.h
 *
 *  Created on: 25 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef ADG5401_SETTINGS_H_
#define ADG5401_SETTINGS_H_

#include "headers.h"

typedef struct {
    uint32_t    SYSCTL_PERIPH_GPIO;
    uint32_t    GPIO_PORT_BASE;
    uint8_t     GPIO_PIN;
}ADG5401_Pin_s;

typedef enum {
    ADG5401_NO_INIT,
    ADG5401_STATE_OPEN,
    ADG5401_STATE_CLOSE
}ADG5401_State_s;

typedef struct {
    ADG5401_Pin_s*   pin;
    ADG5401_State_s state;
}ADG5401_Chip_s;

#define ADG5401_CHIP_COUNT      2

ADG5401_Pin_s  ADG5401_pins[ADG5401_CHIP_COUNT]={
    {SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE, GPIO_PIN_2},
    {SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE, GPIO_PIN_2}
};

#endif /* ADG5401_ADG5401_SETTINGS_H_ */
