/*
 * Main_Logic.c
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */


#include "Main_Logic.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_SProtocol/SProtocol.h"
#include "_RS485/rs485.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_CAN/can_short.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"
#include "_SSI_Lib/SSI_Lib.h"
#include "_Analog_Pins/ADC_24CH/adc_24ch_gh6pge.h"



work_state_model_s _model_state;
analog_sens_struct* _datatemp;




static void callback_timSys(void)
{
    _model_state._time_ms++;
}

static void callback_TimSync(void)
{
    _model_state.sync_trigger=1;
}

static void callback_IRQ_ADC(void)
{

    AnalogSens_GetData(_datatemp);

}


static void modeSelectFunction(SProtocol_message* msg)
{
    _model_state.mode=(mode_system_e)msg->data[0];
}

static void spEchoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);
    _model_state.echo_count++;
}

static void spGetInputs(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_MainData=true;
}


static void spGetFirmwire(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_DataBoard=true;
}

static void spGetSettings(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_Settings=true;
}


static void spsetSettings(SProtocol_message* msg)
{

}

void Main_Init(void)
{
    _model_state.mode=MODE_SETUP_INIT;
     LED_Init();
     TIMERS_init();

     RS485_initHalf(2, 921600, GPIO_PORTG_BASE, GPIO_PIN_6, 0, 0);
     SProtocol_init(*rs485_id, RS485_dataTx);
     RS485_setCallbeack(SProtocol_parserRX, 0, SProtocol_endRX);
     SProtocol_resetAllFunctions();
     SProtocol_setFunction(SP_CMD_SET_MODE, &modeSelectFunction);
     SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&spEchoFunction);
     SProtocol_setFunction(SP_CMD_GET_INPUTS,&spGetInputs);
     SProtocol_setFunction(SP_CMD_FIRMWIRE, &spGetFirmwire);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spGetSettings);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spsetSettings);

     SetCallback_Interrupt(callback_IRQ_ADC);
     Configure_adc(SYS_CLOCK /FAST_LOOP_SPEED);

     _model_state.number_ms_Timer=TIMERS_getNewTimer(Sys_freq()/1000, true, &callback_timSys, true);
     _model_state.number_sync_Timer=TIMERS_getNewTimer(Sys_freq()/4000, true, &callback_TimSync, true);

}

void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}

uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}

static mode_system_e mode_workInit(void)
{

    _model_state.Led_count_ctrl=0;
    _model_state.rs485_flag_Model_DataBoard=false;
    _model_state.rs485_flag_Model_MainData=false;
    _model_state.rs485_flag_Model_Settings=false;


    return MODE_WORK;
}

static void Read_ADC_Data(analog_sens_struct* _datatemp)
{

}


static void Select_Mode_work(void)
{
    switch(_model_state.mode)
    {
    case MODE_WORK_INIT:
            {
                _model_state.mode=mode_workInit();
                break;
            }

            case MODE_WORK:
            {
                //Read Data
                Read_ADC_Data(_datatemp);
                //Main Logic

                break;
            }

            case MODE_SETUP_INIT:
            {

                break;
            }

            case MODE_SETUP:
            {
                break;
            }

            case MODE_TEST_INIT:
            {
                break;
            }

            case MODE_TEST:
            {

                break;
            }

    }
}




static void First_Loop(void)
{
    Select_Mode_work();
}

void Main_Logic_Sync()
{
    First_Loop();
}

void RS485_Async(void)
{
    if(_model_state.rs485_flag_Model_DataBoard)
    {
        _model_state.rs485_flag_Model_DataBoard=false;
        _model_state._data1.ID_temp=*rs485_id;
        memcpy(_model_state._data1.Name, model,sizeof(model));
        memcpy(_model_state._data1.firmwire, firmware,sizeof(firmware));

        SProtocol_message mess;
        mess.command=SP_CMD_FIRMWIRE;
        mess.data=(uint8_t*)&_model_state._data1;
        mess.length=sizeof(Transmit_data1_Model_DataBoard);
        SProtocol_addTxMessage(&mess);
    }
    else if(_model_state.rs485_flag_Model_MainData)
    {
        _model_state.rs485_flag_Model_MainData=false;
        _model_state._data2._MainData=_model_state._mainData._mainAnalogData;
        _model_state._data2.mode=_model_state.mode;

        SProtocol_message mess;
        mess.command=SP_CMD_GET_INPUTS;
        mess.data=(uint8_t*)&_model_state._data2;
        mess.length=sizeof(Transmit_data2_Model_MainData);
        SProtocol_addTxMessage(&mess);

    }
    else if(_model_state.rs485_flag_Model_Settings)
    {

    }
}
