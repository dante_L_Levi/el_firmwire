/*
 * timers_conf.h
 *
 *  Created on: 28 ���. 2020 �.
 *      Author: frolov
 */

#include "headers.h"
#include "sys_setup.h"

#ifndef TIMERS_TIMERS_CONF_H_
#define TIMERS_TIMERS_CONF_H_

typedef struct
{
    uint32_t sys_peryph;    //timer periph base address
    uint32_t base;          //timer base address
    uint32_t int_base;      //interrupt address
    uint32_t clock_sourse;  //
}TIMERS_config_s;

#define TIMER_QUANTITY          6

const TIMERS_config_s timers_config[TIMER_QUANTITY] =
{
  //Timer 0
      SYSCTL_PERIPH_TIMER0,
      TIMER0_BASE,
      INT_TIMER0A,
      TIMER_CLOCK_SYSTEM,

  //Timer 1
      SYSCTL_PERIPH_TIMER1,
      TIMER1_BASE,
      INT_TIMER1A,
      TIMER_CLOCK_SYSTEM,

  //Timer 2
      SYSCTL_PERIPH_TIMER2,
      TIMER2_BASE,
      INT_TIMER2A,
      TIMER_CLOCK_SYSTEM,

  //Timer 3
      SYSCTL_PERIPH_TIMER3,
      TIMER3_BASE,
      INT_TIMER3A,
      TIMER_CLOCK_SYSTEM,

  //Timer 4
      SYSCTL_PERIPH_TIMER4,
      TIMER4_BASE,
      INT_TIMER4A,
      TIMER_CLOCK_SYSTEM,

  //Timer 5
      SYSCTL_PERIPH_TIMER5,
      TIMER5_BASE,
      INT_TIMER5A,
      TIMER_CLOCK_SYSTEM
};

#endif /* TIMERS_TIMERS_CONF_H_ */
