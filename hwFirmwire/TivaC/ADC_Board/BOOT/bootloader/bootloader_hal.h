/*
 * bootloader_hal.h
 *
 *  Created on: 15 ����. 2017 �.
 *      Author: Gorudko
 */

#ifndef INCLUDES_BOOTLOADER_HAL_H_
#define INCLUDES_BOOTLOADER_HAL_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

//includes of peripheral lib
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "driverlib/flash.h"
#include "driverlib/uart.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/eeprom.h"

#include "bootloader_periph.h"

/*				FLASH MEMORY MAP
 * --------------------------------------------
 * |                                          |
 * |               BOOTLOADER                 |
 * |                                          |
 * --------------------------------------------
 * |          GO TO BOOTLOADER KEY            |
 * --------------------------------------------
 * |                                          |
 * |                                          |
 * |                 USER                     |
 * |              APPLICATION                 |
 * |                                          |
 * |                                          |
 * |                                          |
 * |                                          |
 * --------------------------------------------
 */

//-----------------Bootloader information------------------------------------------
#define BOOTLOADER_VERSION 0x0001
#define CPU_MODEL 0x0001
#define DEVICE_ID 0x00000001
#define SUBPAGE_SIZE 64 //size of one package with firmware(then read or write) data


//-----------------Flash sectors allocation------------------------------------------
#define FLASH_ADDRESS 						(uint32_t)0x0
#define FLASH_PAGE_SIZE 					0x400 //1kb
#define FLASH_NUM_OF_PAGES					256
//Bootloader configuration
#define BOOTLOADER_START_ADDRESS            FLASH_ADDRESS
#define BOOTLOADER_PAGE_SIZE              	8

//Bootloader key configuration
#define BOOTLOADER_KEY_START_ADDRESS        0x20001000 //use SRAM

#define FIRMWARE_UPDATE_KEYWORD 			0x4321

//Application configuration
#define APP_PROGRAM_START_ADDRESS			BOOTLOADER_START_ADDRESS + (FLASH_PAGE_SIZE * BOOTLOADER_PAGE_SIZE)
#define APP_NUM_OF_PAGES					FLASH_NUM_OF_PAGES - BOOTLOADER_PAGE_SIZE



//-----------------Hardware abstraction level functions and macros definition--------------------------


//----------------------------------MCU clock----------------------------------------------------------
#define MCU_SPEED 20000000

void Clock_Init();

//----------------------------------software delay-----------------------------------------------------
void delay(uint32_t x);

//----------------------------------rs485 direct pin----------------------------------------------------

void Rs485DirPin_Init();
void Rs485DirPin_Receiver();
void Rs485DirPin_Driver();

//----------------------------------------------------------------------------------------------
void Uart_Init();

void Uart_sendBytes(uint8_t * array, uint8_t len);
#define Uart_sendByte(data)     Uart_sendBytes(&data, 1)

void Uart_getBytes(uint8_t *array, uint8_t len);
void Uart_getByte(uint8_t * data);


//------------------------------------------flash---------------------------------------------------------



#define Flash_init()	                                    ;
#define Flash_writeBytes(flash_address, array_ptr, len)     FlashProgram((uint32_t *)array_ptr, flash_address, len);
#define Flash_readBytes(source_ptr, destination_ptr, len)   memcpy(destination_ptr, source_ptr, len);
#define Flash_erasePage(page_address)                       FlashErase(page_address & (~(FLASH_PAGE_SIZE - 1)) );

//----------------------------------------UID-------------------------------------------------------------
#define ID_ADDRESS 		                                    0x400FE000 + 0
#define ID_LEN                                              16
static const uint8_t * id_array = (const uint8_t *)ID_ADDRESS;



//common init

void Bootloader_InitAll();


#endif /* INCLUDES_BOOTLOADER_HAL_H_ */
