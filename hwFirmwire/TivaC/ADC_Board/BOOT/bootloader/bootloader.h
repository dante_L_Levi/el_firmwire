/*
 * bootloader.h
 *
 *  Created on: 15 ����. 2017 �.
 *      Author: Gorudko
 */

#ifndef INCLUDES_BOOTLOADER_H_
#define INCLUDES_BOOTLOADER_H_

#include "stdint.h"
#include "bootloader_hal.h"
//                        			   0xAABBCC, A - major update, B - minor update, C - _bug fix
static const uint32_t bootloader_ver = 0x010100;
static const uint32_t MCU_code = 0x004000;

void Bootloader_Main();


#endif /* INCLUDES_BOOLTLOADER_H_ */
