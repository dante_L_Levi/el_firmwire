/*
 * settings.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Kuntsevich
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <stdint.h>
#include <stdbool.h>
#include "_pinmux/pinout.h"

#define SYS_CLOCK               80000000

#define SSI_SPEED                   1000000


#pragma DATA_SECTION(_board_id, ".board_id")
static volatile const uint8_t _board_id [4] = {0x01,0xF0,'A','B'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;

#define FAST_LOOP_SPEED     4000
#define MIDDLE_LOOP_SPEED   10


static const char model[8] = "ADCBrd  ";
static const char firmware[8] = "0.0.1  ";
#define PART_TM4C123GH6PGE_MCU      1

#endif /* SETTINGS_H_ */
