/*
 * can.c
 *
 *  Created on: 26 ���. 2019 �.
 *      Author: Gorudko
 */

#include "can.h"
#include "driverlib/can.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_can.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"

static const uint32_t can_base_array[] = { CAN0_BASE, CAN1_BASE };
static const uint32_t sys_periph_can_array[] = { SYSCTL_PERIPH_CAN0,
                                                 SYSCTL_PERIPH_CAN1 };
static const uint32_t int_can_array[] = { INT_CAN0, INT_CAN1 };

static void default_callback(can_callback_data_s *callback_data);

static can_rx_callback callback[2] = { default_callback, default_callback };

static inline void CanIrqHandler(uint32_t can_num)
{
    uint32_t state = CANIntStatus(can_base_array[can_num],
                                      CAN_INT_STS_CAUSE);
    uint32_t obj_num = CANIntStatus(can_base_array[can_num],
                                          CAN_INT_STS_OBJECT);
    CANIntClear(can_base_array[can_num], state);
    for(uint8_t nPack=1; (obj_num)&&(nPack<32) ;nPack++)
        if((1<<(nPack-1))&obj_num){
            obj_num^=(1<<(nPack-1));
            tCANMsgObject sMsgObjectRx;
            uint8_t data[8];
            sMsgObjectRx.pui8MsgData = data;
            CANMessageGet(can_base_array[can_num], nPack, &sMsgObjectRx, true);
            can_callback_data_s callback_data;
            callback_data.box = nPack;
            callback_data.data = data;
            callback_data.id = sMsgObjectRx.ui32MsgID;
            callback_data.len = sMsgObjectRx.ui32MsgLen;
            callback[can_num](&callback_data);
        }
    if((state == CAN_INT_INTID_STATUS)||(obj_num))
    {
        uint32_t status = CANStatusGet(can_base_array[can_num], CAN_STS_CONTROL);
        //Check error
        if(status&CAN_STATUS_BUS_OFF){

        }
    }
}
static void CAN0IrqHandeler()
{
    CanIrqHandler(0);
}
static void CAN1IrqHandeler()
{
    CanIrqHandler(1);
}

int8_t CAN_Init(uint8_t can_num, uint32_t sys_clock, int32_t baudrate)
{

    if (can_num > 1)
    {
        return -1;
    }
    SysCtlPeripheralEnable(sys_periph_can_array[can_num]);

    int32_t timeout = 1000;
    while (!SysCtlPeripheralReady(sys_periph_can_array[can_num]))

        if (timeout-- == 0)
            return -2;

    CANInit(can_base_array[can_num]);

    CANBitRateSet(can_base_array[can_num], sys_clock, baudrate);

    CANIntRegister(can_base_array[can_num],
                   can_num ? CAN1IrqHandeler : CAN0IrqHandeler);
    CANIntEnable(can_base_array[can_num],
    CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

    IntEnable(int_can_array[can_num]);

    CANEnable(can_base_array[can_num]);
    return 0;
}

void CAN_initReceiveBox(uint8_t can_num, uint8_t box, uint8_t is_id_extended,
                        uint32_t id, uint32_t id_mask)
{
    if (box == 0 || box > 32)
        return;
    tCANMsgObject sMsgObjectRx;
    sMsgObjectRx.ui32MsgID = id;
    sMsgObjectRx.ui32MsgIDMask = id_mask;
    sMsgObjectRx.ui32Flags =
            is_id_extended ?
                    (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_EXTENDED_ID
                            | MSG_OBJ_USE_EXT_FILTER) :
                    MSG_OBJ_RX_INT_ENABLE;
    CANMessageSet(can_base_array[can_num], box, &sMsgObjectRx, MSG_OBJ_TYPE_RX);
}

void CAN_setRxCallback(uint8_t can_num, can_rx_callback new_callback)
{
    if (can_num > 1)
        return;
    callback[can_num] = *new_callback;
}

void CAN_sendPackage(uint8_t can_num, uint8_t tx_box, uint8_t is_id_extended,
                     uint32_t tx_id, uint8_t *data, uint8_t len)
{
    if (can_num > 1)
        return;
    CANMessageClear(can_base_array[can_num], tx_box);
    tCANMsgObject sMsgObjectTx;
    sMsgObjectTx.ui32MsgID = tx_id;
    sMsgObjectTx.ui32MsgIDMask = 0;
    sMsgObjectTx.ui32Flags = is_id_extended?MSG_OBJ_EXTENDED_ID:0;
    sMsgObjectTx.ui32MsgLen = len;
    sMsgObjectTx.pui8MsgData = data;
    CANMessageSet(can_base_array[can_num], tx_box, &sMsgObjectTx,
                  MSG_OBJ_TYPE_TX);

}

void CAN_recovery(uint8_t can_num)
{
    if (can_num > 1)
        return;
    uint32_t status = CANStatusGet(can_base_array[can_num], CAN_STS_CONTROL);
    //Check error
    if(status&CAN_STATUS_BUS_OFF){
        CANEnable(can_base_array[can_num]);
    }
}

static void default_callback(can_callback_data_s *callback_data)
{

}
