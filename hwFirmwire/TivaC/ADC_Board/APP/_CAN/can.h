/*
 * can.h
 *
 *  Created on: 26 ���. 2019 �.
 *      Author: Gorudko
 */

#ifndef CAN_H_
#define CAN_H_

#include "stdint.h"
#include "stdbool.h"

typedef struct
{
    uint32_t id;
    uint8_t *data;
    uint8_t len;
    uint8_t box;
} can_callback_data_s;
typedef void (*can_rx_callback)(can_callback_data_s *callback_data);

int8_t CAN_Init(uint8_t can_num, uint32_t sys_clock, int32_t baudrate);
void CAN_initReceiveBox(uint8_t can_num, uint8_t box, uint8_t is_id_extended,
                        uint32_t id, uint32_t id_mask);
void CAN_setRxCallback(uint8_t can_num, can_rx_callback new_callback);
void CAN_sendPackage(uint8_t can_num, uint8_t tx_box, uint8_t is_id_extended,
                     uint32_t tx_id, uint8_t *data, uint8_t len);
void CAN_recovery(uint8_t can_num);

#endif /* CAN_H_ */
