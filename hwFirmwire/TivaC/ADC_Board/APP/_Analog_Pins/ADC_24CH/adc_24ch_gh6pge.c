/*
 * adc_24ch_gh6pge.c
 *
 *  Created on: 18 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "adc_24ch_gh6pge.h"
#include "headers.h"

void (*_irq_adc)(void);

static volatile analog_sens_struct results;

static void adcInterrupthandler(void);
static void timerInit();


void SetCallback_Interrupt(void (*InitFunction)(void))
{
    _irq_adc=InitFunction;
}

void AnalogSens_GetData(analog_sens_struct * res)
{
    *res = results;


}

static void timerInit(uint32_t timer_period)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER4);
    TimerConfigure(TIMER4_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER4_BASE, TIMER_A, timer_period);
    TimerControlTrigger(TIMER4_BASE, TIMER_A, true);
    TimerEnable(TIMER4_BASE, TIMER_A);
}

static void adcInterrupthandler(void)
{

    bool state = ADCIntStatus(ADC0_BASE, 0, true);
    bool state1 = ADCIntStatus(ADC0_BASE, 1, true);
    bool state2 = ADCIntStatus(ADC0_BASE, 2, true);
    bool state3 = ADCIntStatus(ADC1_BASE, 0, true);
    if (state == true)
    {
        ADCIntClear(ADC0_BASE, 0);
        ADCSequenceDataGet(ADC0_BASE, 0, (uint32_t *) &results.ch0);
    }
    if (state1 == true)
    {
        ADCIntClear(ADC0_BASE, 1);
        ADCSequenceDataGet(ADC0_BASE, 1, (uint32_t *) &results.ch8);
    }
    if (state2 == true)
    {
        ADCIntClear(ADC0_BASE, 2);
        ADCSequenceDataGet(ADC0_BASE, 2, (uint32_t *) &results.ch12);
    }
    if (state3 == true)
    {
        ADCIntClear(ADC1_BASE, 0);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *) &results.ch16);
    }

    _irq_adc();
}

void Configure_adc(uint32_t timer_period)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    //ADCClockConfigSet(ADC0_BASE, MAIN_CLOCK | ADC_CLOCK_RATE_FULL, MAIN_CLOCK);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
    //ADCClockConfigSet(ADC1_BASE, MAIN_CLOCK | ADC_CLOCK_RATE_FULL, MAIN_CLOCK);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)
            | !SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1))
        ;

    ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_TIMER, 0); //make adc sampling forever
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_TIMER, 1); //make adc sampling forever
    ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_TIMER, 2); //make adc sampling forever
    ADCSequenceConfigure(ADC1_BASE, 0, ADC_TRIGGER_TIMER, 0); //make adc sampling forever
    //set up sequence
    ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 1, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 2, ADC_CTL_CH2);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 3, ADC_CTL_CH3);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 4, ADC_CTL_CH4);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 5, ADC_CTL_CH5);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 6, ADC_CTL_CH6);
    ADCSequenceStepConfigure(ADC0_BASE, 0, 7, ADC_CTL_CH7 |
    ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH8);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH9);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH10);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 3, ADC_CTL_CH11 |
    ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH12);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 1, ADC_CTL_CH13);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 2, ADC_CTL_CH14);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 3, ADC_CTL_CH15 |
    ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 0, ADC_CTL_CH16);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 1, ADC_CTL_CH17);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 2, ADC_CTL_CH18);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 3, ADC_CTL_CH19);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 4, ADC_CTL_CH20);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 5, ADC_CTL_CH21);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 6, ADC_CTL_CH22);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 7, ADC_CTL_CH23 |
    ADC_CTL_IE | ADC_CTL_END);
    //interrupt settings
    ADCIntRegister(ADC0_BASE, 0, adcInterrupthandler);
    ADCIntEnable(ADC0_BASE, 0);
    ADCSequenceEnable(ADC0_BASE, 0);
    ADCIntRegister(ADC0_BASE, 1, adcInterrupthandler);
    ADCIntEnable(ADC0_BASE, 1);
    ADCSequenceEnable(ADC0_BASE, 1);
    ADCIntRegister(ADC0_BASE, 2, adcInterrupthandler);
    ADCIntEnable(ADC0_BASE, 2);
    ADCSequenceEnable(ADC0_BASE, 2);
    ADCIntRegister(ADC1_BASE, 0, adcInterrupthandler);
    ADCIntEnable(ADC1_BASE, 0);
    ADCSequenceEnable(ADC1_BASE, 0);

    timerInit(timer_period);
}
