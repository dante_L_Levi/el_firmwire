//*****************************************************************************
//
// Configure the device pins for different signals
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// This file was automatically generated on 17.05.2022 at 18:44:51
// by TI PinMux version 1.12.1+2446
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "pinout.h"

//*****************************************************************************
//
//! \addtogroup pinout_api
//! @{
//
//*****************************************************************************

//*****************************************************************************
//
//! Configures the device pins for the customer specific usage.
//!
//! \return None.
//
//*****************************************************************************
void
PinoutSet(void)
{
    //
    // Enable Peripheral Clocks 
    //
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);

    //
    // Configure the GPIO Pin Mux for PD1
	// for AIN14
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PK0
	// for AIN16
    //
	MAP_GPIOPinTypeADC(GPIO_PORTK_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PD2
	// for AIN13
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_2);

    //
    // Configure the GPIO Pin Mux for PE3
	// for AIN0
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);

    //
    // Configure the GPIO Pin Mux for PK1
	// for AIN17
    //
	MAP_GPIOPinTypeADC(GPIO_PORTK_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PE4
	// for AIN9
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_4);

    //
    // Configure the GPIO Pin Mux for PE7
	// for AIN20
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_7);

    //
    // Configure the GPIO Pin Mux for PE6
	// for AIN21
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_6);

	//
	// Unlock the Port Pin and Set the Commit Bit
	//
	HWREG(GPIO_PORTD_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTD_BASE+GPIO_O_CR)   |= GPIO_PIN_7;
	HWREG(GPIO_PORTD_BASE+GPIO_O_LOCK) = 0x0;
	
    //
    // Configure the GPIO Pin Mux for PD7
	// for AIN4
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_7);

    //
    // Configure the GPIO Pin Mux for PE5
	// for AIN8
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_5);

    //
    // Configure the GPIO Pin Mux for PD5
	// for AIN6
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_5);

    //
    // Configure the GPIO Pin Mux for PD0
	// for AIN15
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PE2
	// for AIN1
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);

    //
    // Configure the GPIO Pin Mux for PD3
	// for AIN12
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_3);

    //
    // Configure the GPIO Pin Mux for PE1
	// for AIN2
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PE0
	// for AIN3
    //
	MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PK2
	// for AIN18
    //
	MAP_GPIOPinTypeADC(GPIO_PORTK_BASE, GPIO_PIN_2);

    //
    // Configure the GPIO Pin Mux for PB5
	// for AIN11
    //
	MAP_GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_5);

    //
    // Configure the GPIO Pin Mux for PP0
	// for AIN23
    //
	MAP_GPIOPinTypeADC(GPIO_PORTP_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PB4
	// for AIN10
    //
	MAP_GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_4);

    //
    // Configure the GPIO Pin Mux for PK3
	// for AIN19
    //
	MAP_GPIOPinTypeADC(GPIO_PORTK_BASE, GPIO_PIN_3);

    //
    // Configure the GPIO Pin Mux for PD4
	// for AIN7
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_4);

    //
    // Configure the GPIO Pin Mux for PP1
	// for AIN22
    //
	MAP_GPIOPinTypeADC(GPIO_PORTP_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PD6
	// for AIN5
    //
	MAP_GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_6);

    //
    // Configure the GPIO Pin Mux for PN0
	// for CAN0RX
    //
	MAP_GPIOPinConfigure(GPIO_PN0_CAN0RX);
	MAP_GPIOPinTypeCAN(GPIO_PORTN_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PN1
	// for CAN0TX
    //
	MAP_GPIOPinConfigure(GPIO_PN1_CAN0TX);
	MAP_GPIOPinTypeCAN(GPIO_PORTN_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PL0
	// for GPIO_PL0
    //
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PL1
	// for GPIO_PL1
    //
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PH1
	// for GPIO_PH1
    //
	MAP_GPIOPinTypeGPIOInput(GPIO_PORTH_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PH0
	// for GPIO_PH0
    //
	MAP_GPIOPinTypeGPIOInput(GPIO_PORTH_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PC7
	// for GPIO_PC7
    //
	MAP_GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_7);

    //
    // Configure the GPIO Pin Mux for PC6
	// for GPIO_PC6
    //
	MAP_GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_6);

    //
    // Configure the GPIO Pin Mux for PH3
	// for GPIO_PH3
    //
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_3);

    //
    // Configure the GPIO Pin Mux for PH2
	// for GPIO_PH2
    //
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_2);

    //
    // Configure the GPIO Pin Mux for PK6
	// for GPIO_PK6
    //
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTK_BASE, GPIO_PIN_6);

    //
    // Configure the GPIO Pin Mux for PF4
	// for GPIO_PF4
    //
	MAP_GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);

    //
    // Configure the GPIO Pin Mux for PH6
	// for SSI2RX
    //
	MAP_GPIOPinConfigure(GPIO_PH6_SSI2RX);
	MAP_GPIOPinTypeSSI(GPIO_PORTH_BASE, GPIO_PIN_6);

    //
    // Configure the GPIO Pin Mux for PH7
	// for SSI2TX
    //
	MAP_GPIOPinConfigure(GPIO_PH7_SSI2TX);
	MAP_GPIOPinTypeSSI(GPIO_PORTH_BASE, GPIO_PIN_7);

    //
    // Configure the GPIO Pin Mux for PH4
	// for SSI2CLK
    //
	MAP_GPIOPinConfigure(GPIO_PH4_SSI2CLK);
	MAP_GPIOPinTypeSSI(GPIO_PORTH_BASE, GPIO_PIN_4);

	//
	// Unlock the Port Pin and Set the Commit Bit
	//
	HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTF_BASE+GPIO_O_CR)   |= GPIO_PIN_0;
	HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = 0x0;
	
    //
    // Configure the GPIO Pin Mux for PF0
	// for SSI1RX
    //
	MAP_GPIOPinConfigure(GPIO_PF0_SSI1RX);
	MAP_GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PF3
	// for SSI1FSS
    //
	MAP_GPIOPinConfigure(GPIO_PF3_SSI1FSS);
	MAP_GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_3);

    //
    // Configure the GPIO Pin Mux for PF1
	// for SSI1TX
    //
	MAP_GPIOPinConfigure(GPIO_PF1_SSI1TX);
	MAP_GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_1);

    //
    // Configure the GPIO Pin Mux for PF2
	// for SSI1CLK
    //
	MAP_GPIOPinConfigure(GPIO_PF2_SSI1CLK);
	MAP_GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_2);

    //
    // Configure the GPIO Pin Mux for PG4
	// for U2RX
    //
	MAP_GPIOPinConfigure(GPIO_PG4_U2RX);
	MAP_GPIOPinTypeUART(GPIO_PORTG_BASE, GPIO_PIN_4);

    //
    // Configure the GPIO Pin Mux for PG5
	// for U2TX
    //
	MAP_GPIOPinConfigure(GPIO_PG5_U2TX);
	MAP_GPIOPinTypeUART(GPIO_PORTG_BASE, GPIO_PIN_5);

	//
	// Configure the GPIO Pin Mux for PG6
	// for GPIO_PG6
	//
	MAP_GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_6);

}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

