/*
 * ADG5401.h
 *
 *  Created on: 25 mar. 2020
 *      Author: peresaliak
 */

#ifndef ADG5401_H_
#define ADG5401_H_

#include "headers.h"

void ADG5401_init();
void ADG5401_setState(int8_t, bool);


#endif /* ADG5401_ADG5401_H_ */
