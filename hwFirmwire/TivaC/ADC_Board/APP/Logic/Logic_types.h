/*
 * Logic_types.h
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_LOGIC_TYPES_H_
#define LOGIC_LOGIC_TYPES_H_


#include "Main.h"
#include "settings.h"
#include "headers.h"
#include "map/map.h"



typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;



#pragma pack(push,1)
typedef union
{
    struct
    {
        int32_t AIN0;
        int32_t AIN1;
        int32_t AIN2;
        int32_t AIN3;
        int32_t AIN4;
        int32_t AIN5;
        int32_t AIN6;
        int32_t AIN7;
        int32_t AIN8;
        int32_t AIN9;
        int32_t AIN10;
        int32_t AIN11;
        int32_t AIN12;
        int32_t AIN13;
        int32_t AIN14;
        int32_t AIN15;
        int32_t AIN16;
        int32_t AIN17;
        int32_t AIN18;
        int32_t AIN19;
        int32_t AIN20;
        int32_t AIN21;
        int32_t AIN22;
        int32_t AIN23;
    }fields;
    int32_t all_data[24];
}analog_sense_hw_s;

#pragma pack(pop)

#pragma pack(push,1)
typedef union
{

    struct
       {
           float AIN0;
           float AIN1;
           float AIN2;
           float AIN3;
           float AIN4;
           float AIN5;
           float AIN6;
           float AIN7;
           float AIN8;
           float AIN9;
           float AIN10;
           float AIN11;
           float AIN12;
           float AIN13;
           float AIN14;
           float AIN15;
           float AIN16;
           float AIN17;
           float AIN18;
           float AIN19;
           float AIN20;
           float AIN21;
           float AIN22;
           float AIN23;
       }fields;
       float all_data[24];

}analog_sense_s;//mV
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    map_t _settings[24];


}ADC_Board_hw;

#pragma pack(pop)



#pragma pack(push,1)
typedef struct
{
    uint16_t ID_temp;
    uint8_t Name[8];
    uint8_t firmwire[8];
    uint32_t time_ms;

}Transmit_data1_Model_DataBoard;
#pragma pack(pop)




#pragma pack(push,1)
typedef struct
{
    analog_sense_s      _MainData;
    mode_system_e           mode;

}Transmit_data2_Model_MainData;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{

    ADC_Board_hw            _setting_data;

}Transmit_data3_Model_Settings;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    analog_sense_s             _mainData;
}canTransmitData;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{

    mode_system_e           mode;
    analog_sense_hw_s       _mainData_hw;
    analog_sense_s             _mainData;
    bool                    enable_analog_read;
    bool                    rs485_connect;
    ADC_Board_hw            _settings;



    struct{
            canTransmitData txData;
            uint8_t can_flag_work;
            uint8_t sys_can_num;

        }can;

    uint8_t                     sync_trigger;
    uint32_t                    _time_ms;
    uint32_t                    echo_count;
    uint32_t                    number_ms_Timer;
    uint32_t                    number_sync_Timer;
    uint32_t                    number_can_Timer;
    uint32_t                    Led_count_ctrl;

    Transmit_data1_Model_DataBoard  _data1;
    Transmit_data2_Model_MainData   _data2;
    Transmit_data3_Model_Settings   _data3;

    bool                    rs485_flag_Model_DataBoard;
    bool                    rs485_flag_Model_MainData;
    bool                    rs485_flag_Model_Settings;



}work_state_model_s;


#pragma pack(pop)




#endif /* LOGIC_LOGIC_TYPES_H_ */
