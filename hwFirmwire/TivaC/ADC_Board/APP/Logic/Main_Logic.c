/*
 * Main_Logic.c
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */


#include "Main_Logic.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_SProtocol/SProtocol.h"
#include "_RS485/rs485.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_CAN/can.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"
#include "_SSI_Lib/SSI_Lib.h"
#include "_Analog_Pins/ADC_24CH/adc_24ch_gh6pge.h"
#include "map/map.h"


#define AIN_1           0
#define AIN_2           1
#define AIN_3           2
#define AIN_4           3
#define AIN_5           4
#define AIN_6           5
#define AIN_7           6
#define AIN_8           7
#define AIN_9           8
#define AIN_10          9
#define AIN_11          10
#define AIN_12          11
#define AIN_13          12
#define AIN_14          13
#define AIN_15          14
#define AIN_16          15
#define AIN_17          16
#define AIN_18          17
#define AIN_19          18
#define AIN_20          19
#define AIN_21          20
#define AIN_22          21
#define AIN_23          22
#define AIN_24          23

#define Vin_Kp          3.3f/4095.0f

work_state_model_s _model_state;
analog_sens_struct _datatemp;
analog_sense_s tempSense;



static void callback_timSys(void)
{
    _model_state._time_ms++;
}

static void callback_TimSync(void)
{
    _model_state.sync_trigger=1;
}


static void callback_IRQ_ADC(void)
{

    AnalogSens_GetData(&_datatemp);
    _model_state.enable_analog_read=true;

}


static void callback_canSysRx(can_callback_data_s *callback_data)
{
    switch(callback_data->id)
    {
        case 0x11:
        {
            break;
        }
        default:
            break;
    }
}

static void Rs485_Connect(SProtocol_message* msg)
{
    _model_state.rs485_connect=true;
}
static void modeSelectFunction(SProtocol_message* msg)
{
    _model_state.mode=(mode_system_e)msg->data[0];
}

static void spEchoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);
    _model_state.echo_count++;
}

static void spGetInputs(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_MainData=true;
}


static void spGetFirmwire(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_DataBoard=true;
}

static void spGetSettings(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_Settings=true;
}


static void spsetSettings(SProtocol_message* msg)
{

}


void Read_Settings(void)
{
    _model_state.rs485_connect=false;
}

void Default_Settings(void)
{
    for(uint8_t i=0;i<24;i++)
    {
        tempSense.all_data[i]=0;
    }

    _model_state._settings._settings[AIN_14].k=1.0f;
    _model_state._settings._settings[AIN_14].b=0.0f;

    _model_state._settings._settings[AIN_1].k=6.67f;
    _model_state._settings._settings[AIN_1].b=1.65f;

    _model_state._settings._settings[AIN_2].k=6.67f;
    _model_state._settings._settings[AIN_2].b=1.65f;


    _model_state._settings._settings[AIN_3].k=6.67f;
    _model_state._settings._settings[AIN_3].b=1.65f;

    _model_state._settings._settings[AIN_4].k=6.67f;
    _model_state._settings._settings[AIN_4].b=1.65f;


    _model_state._settings._settings[AIN_5].k=6.67f;
    _model_state._settings._settings[AIN_5].b=1.65f;


    _model_state._settings._settings[AIN_6].k=6.67f;
    _model_state._settings._settings[AIN_6].b=1.65f;


    _model_state._settings._settings[AIN_7].k=6.67f;
    _model_state._settings._settings[AIN_7].b=1.65f;


    _model_state._settings._settings[AIN_8].k=6.67f;
    _model_state._settings._settings[AIN_8].b=1.65f;


    _model_state._settings._settings[AIN_9].k=6.67f;
    _model_state._settings._settings[AIN_9].b=1.65f;

    _model_state._settings._settings[AIN_10].k=6.67f;
    _model_state._settings._settings[AIN_10].b=1.65f;


    _model_state._settings._settings[AIN_11].k=6.67f;
    _model_state._settings._settings[AIN_11].b=1.65f;


    _model_state._settings._settings[AIN_12].k=6.67f;
    _model_state._settings._settings[AIN_12].b=1.65f;


    _model_state._settings._settings[AIN_13].k=6.67f;
    _model_state._settings._settings[AIN_13].b=1.65f;


     _model_state._settings._settings[AIN_15].k=6.67f;
     _model_state._settings._settings[AIN_15].b=1.65f;


     _model_state._settings._settings[AIN_16].k=6.67f;
     _model_state._settings._settings[AIN_16].b=1.65f;


     _model_state._settings._settings[AIN_17].k=6.67f;
     _model_state._settings._settings[AIN_17].b=1.65f;


      _model_state._settings._settings[AIN_18].k=6.67f;
      _model_state._settings._settings[AIN_18].b=1.65f;


      _model_state._settings._settings[AIN_19].k=6.67f;
      _model_state._settings._settings[AIN_19].b=1.65f;


       _model_state._settings._settings[AIN_20].k=6.67f;
       _model_state._settings._settings[AIN_20].b=1.65f;


       _model_state._settings._settings[AIN_21].k=6.67f;
       _model_state._settings._settings[AIN_21].b=1.65f;


       _model_state._settings._settings[AIN_22].k=6.67f;
       _model_state._settings._settings[AIN_22].b=1.65f;


       _model_state._settings._settings[AIN_23].k=6.67f;
       _model_state._settings._settings[AIN_23].b=1.65f;

       _model_state._settings._settings[AIN_24].k=6.67f;
       _model_state._settings._settings[AIN_24].b=1.65f;




}




void Main_Init(void)
{
    _model_state.mode=MODE_WORK_INIT;
    Default_Settings();
    LED_Init();
    TIMERS_init();

    GPIOPinWrite(GPIO_PORTG_BASE, GPIO_PIN_6, 0xFF);
     RS485_initHalf(2, 921600, GPIO_PORTG_BASE, GPIO_PIN_6, 0, 0);
     SProtocol_init(*rs485_id, RS485_dataTx);
     RS485_setCallbeack(SProtocol_parserRX, 0, SProtocol_endRX);
     SProtocol_resetAllFunctions();
     SProtocol_setFunction(SP_CMD_CONNECT, &Rs485_Connect);
     SProtocol_setFunction(SP_CMD_SET_MODE, &modeSelectFunction);
     SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&spEchoFunction);
     SProtocol_setFunction(SP_CMD_GET_INPUTS,&spGetInputs);
     SProtocol_setFunction(SP_CMD_FIRMWIRE, &spGetFirmwire);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spGetSettings);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spsetSettings);

     _model_state.can.can_flag_work=0;
     _model_state.can.sys_can_num=0;
     CAN_Init(_model_state.can.sys_can_num, Sys_freq(), 250000);
     CAN_setRxCallback(_model_state.can.sys_can_num, &callback_canSysRx);
     CAN_initReceiveBox(_model_state.can.sys_can_num, 1, 1, 0x11, 0xFFFFFFFF);

     SetCallback_Interrupt(callback_IRQ_ADC);
     Configure_adc(SYS_CLOCK /FAST_LOOP_SPEED);

     _model_state.number_ms_Timer=TIMERS_getNewTimer(Sys_freq()/1000, true, &callback_timSys, true);
     _model_state.number_sync_Timer=TIMERS_getNewTimer(Sys_freq()/4000, true, &callback_TimSync, true);


}

void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}

uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}

static mode_system_e mode_workInit(void)
{

    _model_state.Led_count_ctrl=0;
    _model_state.rs485_flag_Model_DataBoard=false;
    _model_state.rs485_flag_Model_MainData=false;
    _model_state.rs485_flag_Model_Settings=false;


    return MODE_WORK;
}

static void Read_ADC_Data(analog_sens_struct* _data)
{
    if(_model_state.enable_analog_read)
    {
        _model_state._mainData_hw.fields.AIN0=_data->ch0;
        _model_state._mainData_hw.fields.AIN1=_data->ch1;
        _model_state._mainData_hw.fields.AIN2=_data->ch2;
        _model_state._mainData_hw.fields.AIN3=_data->ch3;
        _model_state._mainData_hw.fields.AIN4=_data->ch4;
        _model_state._mainData_hw.fields.AIN5=_data->ch5;
        _model_state._mainData_hw.fields.AIN6=_data->ch6;
        _model_state._mainData_hw.fields.AIN7=_data->ch7;
        _model_state._mainData_hw.fields.AIN8=_data->ch8;
        _model_state._mainData_hw.fields.AIN9=_data->ch9;
        _model_state._mainData_hw.fields.AIN10=_data->ch10;
        _model_state._mainData_hw.fields.AIN11=_data->ch11;
        _model_state._mainData_hw.fields.AIN12=_data->ch12;
        _model_state._mainData_hw.fields.AIN13=_data->ch13;
        _model_state._mainData_hw.fields.AIN14=_data->ch14;
        _model_state._mainData_hw.fields.AIN15=_data->ch15;
        _model_state._mainData_hw.fields.AIN16=_data->ch16;
        _model_state._mainData_hw.fields.AIN17=_data->ch17;
        _model_state._mainData_hw.fields.AIN18=_data->ch18;
        _model_state._mainData_hw.fields.AIN19=_data->ch19;
        _model_state._mainData_hw.fields.AIN20=_data->ch20;
        _model_state._mainData_hw.fields.AIN21=_data->ch21;
        _model_state._mainData_hw.fields.AIN22=_data->ch22;
        _model_state._mainData_hw.fields.AIN23=_data->ch23;
    }



    for(uint8_t i=0;i<24;i++)
    {
        tempSense.all_data[i]=_model_state._mainData_hw.all_data[i]* Vin_Kp;
        if(i==AIN_14)
        {
            _model_state._mainData.all_data[AIN_14]=tempSense.all_data[AIN_14];
            for(uint8_t j=0;j<24;j++)
            {
                _model_state._settings._settings[j].b=_model_state._mainData.all_data[AIN_14];
            }
            continue;
        }

        _model_state._mainData.all_data[i]=Map_Converted(&_model_state._settings._settings[i], tempSense.all_data[i]);
    }



    _model_state.enable_analog_read=false;
}


static void Update_Status_CAN(void)
{
    if(_model_state.can.can_flag_work==1)
    {
        _model_state.can.can_flag_work=0;

        CAN_sendPackage(_model_state.can.sys_can_num, 31, 0, 0x11,
                         (uint8_t*)&_model_state.can.txData._mainData, sizeof(_model_state.can.txData._mainData));
    }
}

static void Update_Data_CAN_Packet(void)
{
    _model_state.can.txData._mainData=_model_state._mainData;
    _model_state.can.can_flag_work=1;

}


static void Select_Mode_work(void)
{
    switch(_model_state.mode)
    {
    case MODE_WORK_INIT:
            {
                _model_state.mode=mode_workInit();
                break;
            }

            case MODE_WORK:
            {
                //Read Data
                Read_ADC_Data(&_datatemp);
                //CAN update
                Update_Data_CAN_Packet();
                //Main Logic

                break;
            }

            case MODE_SETUP_INIT:
            {

                break;
            }

            case MODE_SETUP:
            {
                break;
            }

            case MODE_TEST_INIT:
            {
                break;
            }

            case MODE_TEST:
            {

                break;
            }

    }
}




static void First_Loop(void)
{
    Select_Mode_work();
}


static void Middle_Loop(void)
{
    static int32_t skip_counter = 1;
                   if (skip_counter--)
                       return;
    skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;

    /*SProtocol_message mess;
    mess.command=SP_CMD_RESERVED_0;
    uint8_t dt=0xFF;
    mess.data=&dt;
    mess.length=sizeof(dt);
    SProtocol_addTxMessage(&mess);*/

    Update_Status_CAN();



}

void Main_Logic_Sync()
{
    First_Loop();
    Middle_Loop();
}

void RS485_Async(void)
{
    SProtocol_analysis();
    if(_model_state.rs485_flag_Model_DataBoard)
    {
        _model_state.rs485_flag_Model_DataBoard=false;
        _model_state._data1.ID_temp=*rs485_id;
        _model_state._data1.time_ms=_model_state._time_ms;
        memcpy(_model_state._data1.Name, model,sizeof(model));
        memcpy(_model_state._data1.firmwire, firmware,sizeof(firmware));

        SProtocol_message mess;
        mess.command=SP_CMD_FIRMWIRE;
        mess.data=(uint8_t*)&_model_state._data1;
        mess.length=sizeof(Transmit_data1_Model_DataBoard);
        SProtocol_addTxMessage(&mess);
    }
    else if(_model_state.rs485_flag_Model_MainData)
    {
        _model_state.rs485_flag_Model_MainData=false;
        _model_state._data2._MainData=_model_state._mainData;
        _model_state._data2.mode=_model_state.mode;

        SProtocol_message mess;
        mess.command=SP_CMD_GET_INPUTS;
        mess.data=(uint8_t*)&_model_state._data2;
        mess.length=sizeof(Transmit_data2_Model_MainData);
        SProtocol_addTxMessage(&mess);

    }
    else if(_model_state.rs485_flag_Model_Settings)
    {
        _model_state.rs485_flag_Model_Settings=false;
        _model_state._data3._setting_data=_model_state._settings;

        SProtocol_message mess;
        mess.command=SP_CMD_GET_SETTINGS;
        mess.data=(uint8_t*)&_model_state._data3;
        mess.length=sizeof(Transmit_data3_Model_Settings);
        SProtocol_addTxMessage(&mess);
    }
}
