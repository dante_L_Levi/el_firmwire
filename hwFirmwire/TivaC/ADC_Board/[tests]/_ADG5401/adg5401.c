/*
 * ADG5401.c
 *
 *  Created on: 25 mar. 2020
 *      Author: peresaliak
 */

#include "_ADG5401/adg5401.h"
#include "_ADG5401/adg5401_settings.h"

static ADG5401_Chip_s ADG5401_chips[2];

void ADG5401_init()
{
    for(int8_t i=0;i<ADG5401_CHIP_COUNT;i++){
        ADG5401_chips[i].state=ADG5401_NO_INIT;
        ADG5401_chips[i].pin=&ADG5401_pins[i];
        SysCtlPeripheralEnable(ADG5401_chips[i].pin->SYSCTL_PERIPH_GPIO);
        GPIOPinTypeGPIOOutput(ADG5401_chips[i].pin->GPIO_PORT_BASE, ADG5401_chips[i].pin->GPIO_PIN);
        GPIOPinWrite(ADG5401_chips[i].pin->GPIO_PORT_BASE, ADG5401_chips[i].pin->GPIO_PIN,0);
        ADG5401_chips[i].state=ADG5401_STATE_CLOSE;
    }
}

void ADG5401_setState(int8_t chipNum, bool setOpen)
{
    if(setOpen){
        GPIOPinWrite(ADG5401_chips[chipNum].pin->GPIO_PORT_BASE,
                     ADG5401_chips[chipNum].pin->GPIO_PIN,
                     ADG5401_chips[chipNum].pin->GPIO_PIN);
        ADG5401_chips[chipNum].state=ADG5401_STATE_OPEN;
    }
    else{
        GPIOPinWrite(ADG5401_chips[chipNum].pin->GPIO_PORT_BASE,
                     ADG5401_chips[chipNum].pin->GPIO_PIN,
                     0);
        ADG5401_chips[chipNum].state=ADG5401_STATE_CLOSE;
    }
}
