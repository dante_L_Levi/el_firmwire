/*
 * SSI_Lib.c
 *
 *  Created on: 11 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "headers.h"
#include "settings.h"
#include "SSI_Lib.h"
#include "driverlib/interrupt.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"


uint32_t speed_data=0;
typedef struct
{
    uint32_t Port_CS;
    uint32_t Pin_CS;
    bool type_cs;

}config_chip_select;


typedef struct {
    uint32_t sysctl_periph;
    uint32_t base;
    uint32_t int_ssi;

    void (*pfnHandler)(void);

    uint32_t pin_TX;
    uint32_t pin_RX;
    uint32_t pin_SCK;
    uint32_t Port;

}ssi_const_t;

#define MAX_DATA_LEN    8
typedef union{
    uint8_t data_u8[MAX_DATA_LEN];
    uint16_t data_u16[MAX_DATA_LEN];
}ssi_data_t;
typedef struct{
    uint8_t flag_init;
    uint8_t flag_isLocked;
    spi_rx_callback_fptr_t callbackRx;
    spi_cs_fptr_t callbackCS;
    ssi_data_t buffer;
}state_spi_t;




#define SSI_COUNT   4
#define SSI1_standart
static void ssi0_irq_handeler(void);
static void ssi1_irq_handeler(void);
static void ssi2_irq_handeler(void);
static void ssi3_irq_handeler(void);

const ssi_const_t ssi_consts[SSI_COUNT]=
{
     {SYSCTL_PERIPH_SSI0, SSI0_BASE, INT_SSI0, &ssi0_irq_handeler,GPIO_PIN_5,GPIO_PIN_4,GPIO_PIN_2,GPIO_PORTA_BASE},
#ifdef SSI1_standart
     {SYSCTL_PERIPH_SSI1, SSI1_BASE, INT_SSI1, &ssi1_irq_handeler,GPIO_PIN_1,GPIO_PIN_0,GPIO_PIN_2,GPIO_PORTF_BASE},
#else
     {SYSCTL_PERIPH_SSI1, SSI1_BASE, INT_SSI1, &ssi1_irq_handeler,GPIO_PIN_3,GPIO_PIN_2,GPIO_PIN_0,GPIO_PORTD_BASE},
#endif
     {SYSCTL_PERIPH_SSI2, SSI2_BASE, INT_SSI2, &ssi2_irq_handeler,GPIO_PIN_7,GPIO_PIN_6,GPIO_PIN_4,GPIO_PORTB_BASE},
     {SYSCTL_PERIPH_SSI3, SSI3_BASE, INT_SSI3, &ssi3_irq_handeler,GPIO_PIN_3,GPIO_PIN_2,GPIO_PIN_0,GPIO_PORTD_BASE}
};

static const uint8_t ssi_modes[6] = { SSI_FRF_MOTO_MODE_0,
                                      SSI_FRF_MOTO_MODE_1,
                                      SSI_FRF_MOTO_MODE_2,
                                      SSI_FRF_MOTO_MODE_3,
                                      SSI_FRF_TI,
                                      SSI_FRF_NMW };

config_chip_select chipSelect_status;
state_spi_t state_spi[SSI_COUNT];

static inline void irq_common_handler(uint8_t spi_num);
//-------------------------------------------------------------------------------
static void ssi0_irq_handeler(void)
{
    irq_common_handler(0);
}
static void ssi1_irq_handeler(void)
{
    irq_common_handler(1);
}
static void ssi2_irq_handeler(void)
{
    irq_common_handler(2);
}
static void ssi3_irq_handeler(void)
{
    irq_common_handler(3);
}

static void default_cs(uint8_t state)
{
}

static void default_rx(uint8_t ssi_num, const uint8_t *rx_data, uint8_t len)
{
}


void SSI_Init_with_Interrupt(uint32_t spi_num,uint8_t ssi_mode)
{
    if(spi_num>=SSI_COUNT)
            return ;
        SysCtlPeripheralEnable(ssi_consts[spi_num].sysctl_periph);
        while (!SysCtlPeripheralReady(ssi_consts[spi_num].sysctl_periph));

        SSIConfigSetExpClk(ssi_consts[spi_num].base, SYS_CLOCK, ssi_modes[ssi_mode],
                           SSI_MODE_MASTER, speed_data, 8);
        SSIAdvFrameHoldEnable(ssi_consts[spi_num].base);
        SSIAdvModeSet(ssi_consts[spi_num].base, SSI_ADV_MODE_WRITE);

        SSIEnable(ssi_consts[spi_num].base);

        SSIIntEnable(ssi_consts[spi_num].base, SSI_TXEOT);
        IntEnable(ssi_consts[spi_num].int_ssi);
        SSIIntRegister(ssi_consts[spi_num].base, ssi_consts[spi_num].pfnHandler);

        state_spi[spi_num].callbackCS=default_cs;
        state_spi[spi_num].callbackRx=default_rx;
        state_spi[spi_num].flag_init=1;

}
int32_t spi_spiWriteRead_u8(uint8_t spi_num, const uint8_t *dataTx, uint8_t *dataRx, uint16_t len)
{
    if(spi_num>=SSI_COUNT)
        return __LINE__;
    if(state_spi[spi_num].flag_init==0)
        return __LINE__;
    uint32_t tmp=0;
    while(SSIDataGetNonBlocking(ssi_consts[spi_num].base, &tmp)>0);

    state_spi[spi_num].callbackCS(1);
    for (int i = 0; i < len; i++) {
        if(i!=(len-1))
            SSIDataPutNonBlocking(ssi_consts[spi_num].base, dataTx[i]);
        else
            SSIAdvDataPutFrameEnd(ssi_consts[spi_num].base, dataTx[i]);
    }

    int n=0;
    for(int i=0;(i<100)&&(n<len);i++){
        if(SSIDataGetNonBlocking(ssi_consts[spi_num].base, &tmp)>0){
            dataRx[n]=tmp;
            n++;
        }
        else
            SysCtlDelay(10);
    }
    memcpy(state_spi[spi_num].buffer.data_u8, dataRx, n);
    state_spi[spi_num].callbackRx(spi_num,
                                state_spi[spi_num].buffer.data_u8,
                                n);
    state_spi[spi_num].callbackCS(0);
    return 0;
}

static inline void irq_common_handler(uint8_t spi_num)
{

    uint32_t base = ssi_consts[spi_num].base;
    uint32_t status = SSIIntStatus(base, true);
    SSIIntClear(base, status);
    uint8_t count=0;
    for (uint8_t i = 0; i < MAX_DATA_LEN; i++)
    {
        uint32_t tmp=0;
        if (SSIDataGetNonBlocking(base, &tmp) == 0)
            break;
        state_spi[spi_num].buffer.data_u8[count] = tmp;
        count=i;
    }
    state_spi[spi_num].callbackRx(spi_num,
                                state_spi[spi_num].buffer.data_u8,
                                count);
    state_spi[spi_num].callbackCS(0);
    state_spi[spi_num].flag_isLocked = 0;
}


void spi_setCallbackRX(uint8_t spi_num, spi_rx_callback_fptr_t callback)
{
    if (spi_num >= 4)
        return;
    state_spi[spi_num].callbackRx = callback;
}

void spi_setCallbackCS(uint8_t spi_num, spi_cs_fptr_t callback)
{
    if (spi_num >= 4)
        return;
    state_spi[spi_num].callbackCS = callback;
}

//-------------------------------------------------------------------------------------------


void CS_Set(void)
{
    GPIOPinWrite(chipSelect_status.Port_CS,chipSelect_status.Pin_CS,0x00);
}

void CS_Reset(void)
{
    GPIOPinWrite(chipSelect_status.Port_CS,chipSelect_status.Pin_CS,0xFF);
}


void Init_chipSelect(uint32_t port,uint32_t pin)
{
    chipSelect_status.Port_CS=port;
    chipSelect_status.Pin_CS=pin;
}

void Set_Speed_SSI(uint32_t speed)
{
    speed_data=speed;
}

void SSI_Init(uint32_t number)
{
    if(number>=SSI_COUNT)
                return ;

    //Init chip Select Pin
        //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        GPIOPinTypeGPIOOutput(chipSelect_status.Port_CS,chipSelect_status.Pin_CS);
        GPIOPinWrite(chipSelect_status.Port_CS,chipSelect_status.Pin_CS, 0xFF);
        //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
        /*while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
        {
        }*/
        GPIODirModeSet(ssi_consts[number].Port,
                       ssi_consts[number].pin_TX | ssi_consts[number].pin_RX | ssi_consts[number].pin_SCK,
                           GPIO_DIR_MODE_HW);

/*
        //
        // Configure the GPIO Pin Mux for PD3
            // for SSI1TX
            //
            GPIOPinConfigure(SSI_TX_Conf);
            GPIOPinTypeSSI(PORT_SSI, PIN_TX);
            // Configure the GPIO Pin Mux for PD2
             // for SSI1RX
             //
            GPIOPinConfigure(SSI_RX_Conf);
            GPIOPinTypeSSI(PORT_SSI, PIN_RX);



            //
            // Configure the GPIO Pin Mux for PD0
            // for SSI1CLK
            //
            GPIOPinConfigure(SSI_SCK_Conf);
            GPIOPinTypeSSI(PORT_SSI, PIN_SCK);
*/

            //Setting SSI
            SysCtlPeripheralEnable(ssi_consts[number].sysctl_periph);
            while(!SysCtlPeripheralReady(ssi_consts[number].sysctl_periph))
            {
            }

            SSIClockSourceSet(ssi_consts[number].base, SSI_CLOCK_SYSTEM);
            SSIConfigSetExpClk(ssi_consts[number].base, SYS_CLOCK,
                                   SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER,
                                   speed_data, 8);


            SSIEnable(ssi_consts[number].base);

}


void SSI_read(uint32_t number,uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    uint32_t tmp;
        if(dev_addr)
        {
            CS_Set();
            SSIDataPut(ssi_consts[number].base, reg_addr);
            SSIDataGet(ssi_consts[number].base, &tmp);

            for (int i=0; i < len;++i)
            {
               SSIDataPut(ssi_consts[number].base, 0);
               SSIDataGet(ssi_consts[number].base, &tmp);
               data[i] = tmp;
            }

            CS_Reset();

        }
}
void SSI_write(uint32_t number,uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    uint32_t tmp;
        if(dev_addr)
        {
            CS_Set();
            SSIDataPut(ssi_consts[number].base, reg_addr);
            SSIDataGet(ssi_consts[number].base, &tmp);
            for(int i=0;i<len;++i)
            {
                SSIDataPut(ssi_consts[number].base, data[i]);
                SSIDataGet(ssi_consts[number].base, &tmp);
            }
            CS_Reset();
        }
}







