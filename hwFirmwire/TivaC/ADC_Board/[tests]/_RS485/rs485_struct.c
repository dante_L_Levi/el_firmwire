/*
 * rs485_struct.c
 *
 *  Created on: 14 ���. 2020 �.
 *      Author: peresaliak
 */

#include <stdint.h>
#include <stdbool.h>

#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "libs/ring_buffer.h"

#define TX_BUFFER_SIZE      270
#define RX_BUFFER_SIZE      270

typedef enum{
    RS485_MODE_HALF,
    RS485_MODE_FULL
}rs485_mode_e;

typedef enum {
    RX_STATE_NOINIT,
    RX_STATE_WAIT,
    RX_STATE_RECEIVE
}rs485_state_rx_e;

typedef enum {
    TX_STATE_NOINIT,
    TX_STATE_EMPTY,
    TX_STATE_TRANSMIT,
    TX_STATE_SILENT,
    TX_STATE_WAIT_RECEIVE
}rs485_state_tx_e;

typedef enum {
    RS485_NOINIT,
    RS485_INIT
}rs485_state_e;

typedef struct{
    struct{
        uint32_t port_rx;
        uint32_t port_tx;
        uint32_t pin_rx;
        uint32_t pin_tx;
    }pins;
    uint32_t uart_base;

    rs485_mode_e mode;
    rs485_state_rx_e state_rx;
    rs485_state_tx_e state_tx;
    rs485_state_e state;

    uint8_t dataTX[TX_BUFFER_SIZE];
    uint8_t dataRX[RX_BUFFER_SIZE];
    uint8_t dataRX_fifo[16];
    ringBuffer_s rx_ring;
    ringBuffer_s tx_ring;

    void (*callbackRx)(uint8_t* data, int16_t length);
    void (*callbackEndTx)(void);
    void (*callbackEndRx)(void);

    uint8_t muteCnt;

    uint8_t fifoLen;
}rs485_state_s;
