/*
 * adg520x_setting.h
 *
 *  Created on: 4 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef ADG520X_ADG520X_SETTING_H_
#define ADG520X_ADG520X_SETTING_H_

#define ADG5208_ADDR_MASK       0x07
#define ADG5209_ADDR_MASK       0x03

typedef struct{
    uint8_t numS[8];
}device_ADG5208;

typedef struct{
    uint8_t numSA[4];
    uint8_t numSB[4];
}device_ADG5209;

typedef struct{
    uint8_t* numS;
    uint8_t mask;
    uint8_t adc_ch;
    uint8_t adc_id;
}channel_ADG520x_mux;


#define ADG520x_ADDR_PERIPH                 SYSCTL_PERIPH_GPIOC
#define ADG520x_ADDR_PORT                   GPIO_PORTC_BASE
#define ADG520x_PIN_EN                      GPIO_PIN_4
#define ADG520x_PIN_A0                      GPIO_PIN_5
#define ADG520x_PIN_A1                      GPIO_PIN_6
#define ADG520x_PIN_A2                      GPIO_PIN_7

#define ADG520x_COUNT_MUX_CHANNELS          4
#define ADG520x_COUNT_CHANNELS              32

device_ADG5208 D13=   { 0, 1, 2, 3, 4, 5, 6, 7};
device_ADG5208 D14=   { 8, 9, 10, 11, 12, 13, 14, 15};
device_ADG5208 D15=   { 16, 17, 18, 19, 20, 21, 22, 23};
device_ADG5208 D16=   { 24, 25, 26, 27, 28, 29, 30, 31};

channel_ADG520x_mux channels_mux[ADG520x_COUNT_MUX_CHANNELS] = {
        D13.numS, ADG5208_ADDR_MASK, 0, 0,
        D14.numS, ADG5208_ADDR_MASK, 1, 0,
        D15.numS, ADG5208_ADDR_MASK, 2, 0,
        D16.numS, ADG5208_ADDR_MASK, 3, 0
};

#endif /* ADG520X_ADG520X_SETTING_H_ */
