/*
 * Logic_types.h
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_LOGIC_TYPES_H_
#define LOGIC_LOGIC_TYPES_H_


#include "Main.h"
#include "settings.h"
#include "headers.h"



typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;



#pragma pack(push,1)
typedef union
{
    struct
    {
        int32_t AIN0;
        int32_t AIN1;
        int32_t AIN2;
        int32_t AIN3;
        int32_t AIN4;
        int32_t AIN5;
        int32_t AIN6;
        int32_t AIN7;
        int32_t AIN8;
        int32_t AIN9;
        int32_t AIN10;
        int32_t AIN11;
        int32_t AIN12;
        int32_t AIN13;
        int32_t AIN14;
        int32_t AIN15;
        int32_t AIN16;
        int32_t AIN17;
        int32_t AIN18;
        int32_t AIN19;
        int32_t AIN20;
        int32_t AIN21;
        int32_t AIN22;
        int32_t AIN23;
    }fields;
    int32_t all_data[24];
}analog_sense_hw_s;

#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{

    struct
       {
           int32_t AIN0;
           int32_t AIN1;
           int32_t AIN2;
           int32_t AIN3;
           int32_t AIN4;
           int32_t AIN5;
           int32_t AIN6;
           int32_t AIN7;
           int32_t AIN8;
           int32_t AIN9;
           int32_t AIN10;
           int32_t AIN11;
           int32_t AIN12;
           int32_t AIN13;
           int32_t AIN14;
           int32_t AIN15;
           int32_t AIN16;
           int32_t AIN17;
           int32_t AIN18;
           int32_t AIN19;
           int32_t AIN20;
           int32_t AIN21;
           int32_t AIN22;
           int32_t AIN23;
       }fields;
       int32_t all_data[24];

}analog_sense_s;//mV
#pragma pack(pop)

#pragma pack(push,1)
typedef union
{
    struct
    {
        float Kp_Ain0;
        float Kp_Ain1;
        float Kp_Ain2;
        float Kp_Ain3;
        float Kp_Ain4;
        float Kp_Ain5;
        float Kp_Ain6;
        float Kp_Ain7;
        float Kp_Ain8;
        float Kp_Ain9;
        float Kp_Ain10;
        float Kp_Ain11;
        float Kp_Ain12;
        float Kp_Ain13;
        float Kp_Ain14;
        float Kp_Ain15;
        float Kp_Ain16;
        float Kp_Ain17;
        float Kp_Ain18;
        float Kp_Ain19;
        float Kp_Ain20;
        float Kp_Ain21;
        float Kp_Ain22;
        float Kp_Ain23;
    }fields;
    float allData[24];

}settings_Data_def;

#pragma pack(pop)

#pragma pack(push,1)

typedef struct
{
    analog_sense_s          _mainAnalogData;
    settings_Data_def       _settings;
}adc_sense_s;

#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    uint16_t ID_temp;
    uint8_t Name[8];
    uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;
#pragma pack(pop)




#pragma pack(push,1)
typedef struct
{
    analog_sense_s      _MainData;
    mode_system_e           mode;

}Transmit_data2_Model_MainData;
#pragma pack(pop)

typedef struct
{


}Transmit_data3_Model_Settings;





#pragma pack(push,1)
typedef struct
{

    mode_system_e           mode;
    adc_sense_s             _mainData;

    uint8_t                     sync_trigger;
    uint32_t                    _time_ms;
    uint32_t                    echo_count;
    uint32_t                    number_ms_Timer;
    uint32_t                    number_sync_Timer;
    uint32_t                    Led_count_ctrl;

    Transmit_data1_Model_DataBoard  _data1;
    Transmit_data2_Model_MainData   _data2;
    Transmit_data3_Model_Settings   _data3;

    bool                    rs485_flag_Model_DataBoard;
    bool                    rs485_flag_Model_MainData;
    bool                    rs485_flag_Model_Settings;

}work_state_model_s;


#pragma pack(pop)




#endif /* LOGIC_LOGIC_TYPES_H_ */
