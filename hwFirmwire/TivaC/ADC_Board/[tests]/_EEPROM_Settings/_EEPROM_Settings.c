/*
 * _EEPROM_Settings.c
 *
 *  Created on: 24 ���. 2020 �.
 *      Author: peresaliak
 */

#include "_EEPROM_Settings.h"

int32_t _EEPROM_settings_init()
{
    if(!SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0)){
        SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0));
    }
    while(EEPROMInit() != EEPROM_INIT_OK) {};
    return 0;
}

int32_t _EEPROM_settings_setSetting(_EEPROM_settings_e setting, uint32_t* data, uint16_t len)
{
    if(len!=_EEPROM_settings_len[setting])
        return 1;
    EEPROMProgram((uint32_t *)data, _EEPROM_settings_addr[setting], _EEPROM_settings_len[setting]);
    return 0;
}

int32_t _EEPROM_settings_getSetting(_EEPROM_settings_e setting, uint32_t* data, uint16_t len)
{
    if(len!=_EEPROM_settings_len[setting])
        return 1;
    EEPROMRead(data, _EEPROM_settings_addr[setting], _EEPROM_settings_len[setting]);
    return 0;
}
