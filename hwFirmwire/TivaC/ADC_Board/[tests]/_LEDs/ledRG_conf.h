/*
 * leds_conf.h
 *
 *  Created on: 23 July 2020
 *      Author: peresaliak
 */
#ifndef _LEDS_CONF_H
#define _LEDS_CONF_H

#include <_LEDs/ledRG.h>
#define PORT_RED        GPIO_PORTL_BASE
#define PIN_RED         GPIO_PIN_0
#define PORT_GREEN      GPIO_PORTL_BASE
#define PIN_GREEN       GPIO_PIN_1

typedef struct{
    uint8_t count;
    LED_state_e state[5];
}LED_blink_s;

static const LED_blink_s blinks[3]={
      {2, LED_RED, LED_GREEN, LED_OFF,  LED_OFF,    LED_OFF},
      {3, LED_RED, LED_GREEN, LED_BOTH, LED_OFF,    LED_OFF},
      {2, LED_GREEN, LED_OFF, LED_OFF,  LED_OFF,    LED_OFF}
};

#endif
