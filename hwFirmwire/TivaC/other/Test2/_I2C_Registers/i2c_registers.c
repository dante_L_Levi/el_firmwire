/*
 * chips_registers.c
 *
 *  Created on: 10 mar. 2020
 *      Author: peresaliak
 */

#include "i2c_registers_settings.h"
#include "i2c_registers_struct.c"
#include "i2c_registers.h"
#include "settings.h"
#include "headers.h"


static I2C_registers_channal_s channel[I2C_registers_periph_count];

static int16_t add_byte(uint8_t i2c_num, one_package_byte* data, int16_t last);
static void send_next_byte(uint8_t i2c_num);
static void send_package_byte(uint8_t i2c_num, one_package_byte* data);
static void interupt_handler(uint8_t i2c_num);
static void interupt_handler0();
static void interupt_handler1();
static void interupt_handler2();
static void interupt_handler3();
static void interupt_handler4();
static void interupt_handler5();
static void interupt_handler6();
static void interupt_handler7();


void I2C_REGISTERS_init(void)
{
    for(int i=0;i<I2C_registers_periph_count;i++)
        channel[i].state=STATE_NO_INIT;
}

void I2C_REGISTERS_activateI2C(uint8_t i2c_number)
{
    if(channel[i2c_number].state!=STATE_NO_INIT)
        return;
    SysCtlPeripheralEnable(I2C_registers_periph[i2c_number].SYS_PERIPH_I2C);
    while(!SysCtlPeripheralReady(I2C_registers_periph[i2c_number].SYS_PERIPH_I2C));

    SysCtlPeripheralEnable(I2C_registers_periph[i2c_number].SYS_PERIPH_GPIO);
    while(!SysCtlPeripheralReady(I2C_registers_periph[i2c_number].SYS_PERIPH_GPIO));

    GPIODirModeSet(I2C_registers_periph[i2c_number].PORT_BASE, I2C_registers_periph[i2c_number].SCL_PIN |
                   I2C_registers_periph[i2c_number].SDA_PIN, GPIO_DIR_MODE_HW);
    GPIOPinConfigure(I2C_registers_periph[i2c_number].GPIO_Pxx_SCL);
    GPIOPinConfigure(I2C_registers_periph[i2c_number].GPIO_Pxx_SDA);
    GPIOPinTypeI2CSCL(I2C_registers_periph[i2c_number].PORT_BASE, I2C_registers_periph[i2c_number].SCL_PIN);
    GPIOPinTypeI2C(I2C_registers_periph[i2c_number].PORT_BASE, I2C_registers_periph[i2c_number].SDA_PIN);


    I2CMasterEnable(I2C_registers_periph[i2c_number].I2C_BASE);
    I2CMasterInitExpClk(I2C_registers_periph[i2c_number].I2C_BASE, SYS_CLOCK, true); //true - 400kbps, false - 100kbps

    switch(i2c_number){
    case 0:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler0);
        break;
    case 1:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler1);
        break;
    case 2:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler2);
        break;
    case 3:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler3);
        break;
    case 4:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler4);
        break;
    case 5:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler5);
        break;
    case 6:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler6);
        break;
    case 7:
        I2CIntRegister(I2C_registers_periph[i2c_number].I2C_BASE, &interupt_handler7);
        break;
    }
//    I2CMasterIntEnableEx(I2C_registers_periph[i2c_number].I2C_BASE, (I2C_MASTER_INT_ARB_LOST |
//                        I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |
//                        I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));
    I2CMasterIntEnableEx(I2C_registers_periph[i2c_number].I2C_BASE, (
                        I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));

    if(channel[i2c_number].state==STATE_NO_INIT){
        channel[i2c_number].receive=malloc(sizeof(I2C_registers_receive_data_s));
        channel[i2c_number].streem=malloc(sizeof(I2C_registers_stream_s));
    }
//    channel[i2c_number].receive=malloc(sizeof(I2C_registers_receive_data_s));
    channel[i2c_number].receive->last_number=0;
    channel[i2c_number].receive->number=0;
    for(int i=0;i<I2C_REGISTERS_COUNT_RECEIVE_PACK;i++){
        channel[i2c_number].receive->pack[i].data=0;
        channel[i2c_number].receive->pack[i].dataCount=0;
        channel[i2c_number].receive->pack[i].dataLen=0;
        channel[i2c_number].receive->pack[i].isReady=0;
    }

//    channel[i2c_number].streem=malloc(sizeof(I2C_registers_stream_s));
    channel[i2c_number].streem->last_number=0;
    channel[i2c_number].streem->number=0;
    for(int i=0;i<I2C_REGISTERS_LENGTH_SEND_BUFFER;i++){
        channel[i2c_number].streem->pack[i].control=0;
        channel[i2c_number].streem->pack[i].data=0;
        channel[i2c_number].streem->pack[i].type=CHIPS_REGISRT_START_TRANSMIT;
    }
    channel[i2c_number].state=STATE_WAIT;
}

void I2C_REGISTERS_deactivateI2CAll(void)
{
    for(int i=0;i<I2C_registers_periph_count;i++){
        channel[i].state=STATE_NO_INIT;
        if(SysCtlPeripheralReady(I2C_registers_periph[i].SYS_PERIPH_I2C))
            SysCtlPeripheralDisable(I2C_registers_periph[i].SYS_PERIPH_I2C);
    }
}

void I2C_REGISTERS_resetI2C(uint8_t i2c_number)
{
    //I2CMasterIntClear(I2C_registers_periph[i2c_number].I2C_BASE);

    //channel[i2c_number].receive=malloc(sizeof(I2C_registers_receive_data_s));
    channel[i2c_number].receive->last_number=0;
    channel[i2c_number].receive->number=0;
    for(int i=0;i<I2C_REGISTERS_COUNT_RECEIVE_PACK;i++){
        channel[i2c_number].receive->pack[i].data=0;
        channel[i2c_number].receive->pack[i].dataCount=0;
        channel[i2c_number].receive->pack[i].dataLen=0;
        channel[i2c_number].receive->pack[i].isReady=0;
    }

    //channel[i2c_number].streem=malloc(sizeof(I2C_registers_stream_s));
    channel[i2c_number].streem->last_number=0;
    channel[i2c_number].streem->number=0;
    for(int i=0;i<I2C_REGISTERS_LENGTH_SEND_BUFFER;i++){
        channel[i2c_number].streem->pack[i].control=0;
        channel[i2c_number].streem->pack[i].data=0;
        channel[i2c_number].streem->pack[i].type=CHIPS_REGISRT_START_TRANSMIT;
    }

    channel[i2c_number].state=STATE_WAIT;

    //I2CMasterIntClear(I2C_registers_periph[i2c_number].I2C_BASE);
}

uint8_t I2C_REGISTERS_checkI2C(uint8_t i2c_number)
{
    if(channel[i2c_number].state==STATE_ERROR)
        return 1;
    return 0;
}

void I2C_REGISTERS_continueI2C(uint8_t i2c_number)
{
    if(channel[i2c_number].state==STATE_ERROR)
        channel[i2c_number].state=STATE_WAIT;
}

void I2C_REGISTERS_set_registr8(uint8_t i2c_number, uint8_t addr_slave, uint8_t addr_reg, uint8_t data)
{
    if(channel[i2c_number].state==STATE_NO_INIT)
        return;
    one_package_byte byte;
    int16_t last=channel[i2c_number].streem->last_number;

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_reg;
    byte.control=I2C_MASTER_CMD_BURST_SEND_START;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=data;
    byte.control=I2C_MASTER_CMD_BURST_SEND_FINISH;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    if(last>=0)
        channel[i2c_number].streem->last_number=last;
    else
        channel[i2c_number].streem->last_number=0;


    if(channel[i2c_number].state==STATE_WAIT)
        send_next_byte(i2c_number);
}

void I2C_REGISTERS_set_registr16(uint8_t i2c_number, uint8_t addr_slave, uint8_t addr_reg, uint16_t data)
{
    if(channel[i2c_number].state==STATE_NO_INIT)
        return;
    one_package_byte byte;
    int16_t last=channel[i2c_number].streem->last_number;

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_reg;
    byte.control=I2C_MASTER_CMD_BURST_SEND_START;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=(data>>8)&0xFF;
    byte.control=I2C_MASTER_CMD_BURST_SEND_CONT;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=data&0xFF;
    byte.control=I2C_MASTER_CMD_BURST_SEND_FINISH;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    if(last>=0)
        channel[i2c_number].streem->last_number=last;
    else
        channel[i2c_number].streem->last_number=0;


    if(channel[i2c_number].state==STATE_WAIT)
        send_next_byte(i2c_number);
}

I2C_REGISTERS_receive_package_s* I2C_REGISTERS_read_registr8(uint8_t i2c_number, uint8_t addr_slave, uint8_t addr_reg, uint8_t* data)
{
    if(channel[i2c_number].state==STATE_NO_INIT)
        return NULL;
    one_package_byte byte;
    int16_t last=last=channel[i2c_number].streem->last_number;

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_reg;
    byte.control=I2C_MASTER_CMD_SINGLE_SEND;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_RECEIVE;
    last=add_byte(i2c_number, &byte, last);

    uint8_t next_pack=(channel[i2c_number].receive->last_number+1)%I2C_REGISTERS_COUNT_RECEIVE_PACK;
    if((next_pack!=channel[i2c_number].receive->number)&&(last>=0)){
        channel[i2c_number].receive->pack[next_pack].data=data;
        channel[i2c_number].receive->pack[next_pack].dataCount=0;
        channel[i2c_number].receive->pack[next_pack].dataLen=1;
        channel[i2c_number].receive->pack[next_pack].isReady=false;
        data[0]=0;
        byte.data=next_pack;
        byte.control=I2C_MASTER_CMD_SINGLE_RECEIVE;
        byte.type=CHIPS_REGISRT_DATA_RECEIVE;
        last=add_byte(i2c_number, &byte, last);
        channel[i2c_number].receive->last_number=next_pack;
    }
    else{
        last=-2;
    }

    if(last>=0){
        last=channel[i2c_number].streem->last_number=last;
    }

    if(channel[i2c_number].state==STATE_WAIT)
        send_next_byte(i2c_number);
    return &(channel[i2c_number].receive->pack[next_pack]);
}

I2C_REGISTERS_receive_package_s* I2C_REGISTERS_read_mem(uint8_t i2c_number, uint8_t addr_slave, uint8_t addr_reg_start, uint8_t* data, uint8_t len)
{
    if(channel[i2c_number].state==STATE_NO_INIT)
        return NULL;
    if(len==0)
        return NULL;
    if(len==1)
        return I2C_REGISTERS_read_registr8(i2c_number, addr_slave, addr_reg_start, data);
    one_package_byte byte;
    int16_t last=channel[i2c_number].streem->last_number;

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_reg_start;
    byte.control=I2C_MASTER_CMD_SINGLE_SEND;
    byte.type=CHIPS_REGISRT_DATA_TRANSMIT;
    last=add_byte(i2c_number, &byte, last);

    byte.data=addr_slave;
    byte.control=0;
    byte.type=CHIPS_REGISRT_START_RECEIVE;
    last=add_byte(i2c_number, &byte, last);

    uint8_t next_pack=(channel[i2c_number].receive->last_number+1)%I2C_REGISTERS_COUNT_RECEIVE_PACK;
    if((next_pack!=channel[i2c_number].receive->number)&&(last>=0)){
        channel[i2c_number].receive->pack[next_pack].data=data;
        channel[i2c_number].receive->pack[next_pack].dataCount=0;
        channel[i2c_number].receive->pack[next_pack].dataLen=len;
        channel[i2c_number].receive->pack[next_pack].isReady=false;

        data[0]=0;
        byte.data=next_pack;
        byte.control=I2C_MASTER_CMD_BURST_RECEIVE_START;
        byte.type=CHIPS_REGISRT_DATA_RECEIVE;
        last=add_byte(i2c_number, &byte, last);
        for(int i=1;i<(len-1);i++){
            data[i]=0;
            byte.data=next_pack;
            byte.control=I2C_MASTER_CMD_BURST_RECEIVE_CONT;
            byte.type=CHIPS_REGISRT_DATA_RECEIVE;
            last=add_byte(i2c_number, &byte, last);
        }
        data[len-1]=0;
        byte.data=next_pack;
        byte.control=I2C_MASTER_CMD_FIFO_BURST_RECEIVE_FINISH;
        byte.type=CHIPS_REGISRT_DATA_RECEIVE;
        last=add_byte(i2c_number, &byte, last);

        channel[i2c_number].receive->last_number=next_pack;
    }
    else{
        last=-2;
    }

    if(last>=0){
        channel[i2c_number].streem->last_number=last;
    }

    if(channel[i2c_number].state==STATE_WAIT)
        send_next_byte(i2c_number);
    return &(channel[i2c_number].receive->pack[next_pack]);
}


static int16_t add_byte(uint8_t i2c_number, one_package_byte* data, int16_t last)
{
    if(last<0)
        return last;
    int16_t next_last=(last+1)%I2C_REGISTERS_LENGTH_SEND_BUFFER;
    if(next_last==channel[i2c_number].streem->number)
        return -1;
    channel[i2c_number].streem->pack[next_last].data=data->data;
    channel[i2c_number].streem->pack[next_last].type=data->type;
    channel[i2c_number].streem->pack[next_last].control=data->control;
    return next_last;
}

static void send_next_byte(uint8_t i2c_number)
{
    if(channel[i2c_number].streem->number!=channel[i2c_number].streem->last_number){
        channel[i2c_number].streem->number=(channel[i2c_number].streem->number+1)%I2C_REGISTERS_LENGTH_SEND_BUFFER;
        channel[i2c_number].state=STATE_TRANSMIT;
        send_package_byte(i2c_number, &(channel[i2c_number].streem->pack[channel[i2c_number].streem->number]));
    }
    else{
        channel[i2c_number].state=STATE_WAIT;
    }

}

static void send_package_byte(uint8_t i2c_number, one_package_byte* data)
{
    switch(data->type){
        case CHIPS_REGISRT_START_TRANSMIT:
            I2CMasterSlaveAddrSet(I2C_registers_periph[i2c_number].I2C_BASE, data->data, false);
            send_next_byte(i2c_number);
            break;
        case CHIPS_REGISRT_START_RECEIVE:
            channel[i2c_number].receive->number=(channel[i2c_number].receive->number+1)%I2C_REGISTERS_COUNT_RECEIVE_PACK;
            I2CMasterSlaveAddrSet(I2C_registers_periph[i2c_number].I2C_BASE, data->data, true);
            send_next_byte(i2c_number);
            break;
        case CHIPS_REGISRT_DATA_TRANSMIT:
            I2CMasterDataPut(I2C_registers_periph[i2c_number].I2C_BASE, data->data);
            I2CMasterControl(I2C_registers_periph[i2c_number].I2C_BASE, data->control);
            break;
        case CHIPS_REGISRT_DATA_RECEIVE:
            //I2CMasterDataPut(CHIPS_I2C_BASE, data->data);
            I2CMasterControl(I2C_registers_periph[i2c_number].I2C_BASE, data->control);
            break;
        default:
            break;
    }
}

static void interupt_handler(uint8_t i2c_num)
{
    uint32_t error=I2CMasterErr(I2C_registers_periph[i2c_num].I2C_BASE);
    uint32_t errorEx=I2CMasterIntStatusEx(I2C_registers_periph[i2c_num].I2C_BASE, true);
    uint32_t status = I2CMasterIntStatus(I2C_registers_periph[i2c_num].I2C_BASE, false);
    //
    I2CMasterIntClear(I2C_registers_periph[i2c_num].I2C_BASE);
    if(error!=0){
        if(error&(I2C_MASTER_ERR_DATA_ACK|I2C_MASTER_ERR_ADDR_ACK)){

        }
        else{
            channel[i2c_num].state=STATE_ERROR;
            //error i2c periphery
            return;
        }
    }
    if(errorEx!=0){
        I2CMasterIntClearEx(I2C_registers_periph[i2c_num].I2C_BASE, errorEx);
        //error i2c device
    }
    if(channel[i2c_num].streem->pack[channel[i2c_num].streem->number].type==CHIPS_REGISRT_DATA_RECEIVE){
        I2C_REGISTERS_receive_package_s* pack=&channel[i2c_num].receive->pack[channel[i2c_num].streem->pack[channel[i2c_num].streem->number].data];
        uint8_t dataR=(uint8_t)I2CMasterDataGet(I2C_registers_periph[i2c_num].I2C_BASE);
        if(pack->dataCount>=pack->dataLen){
            pack->dataCount=pack->dataLen;
        }
        else{
            pack->data[pack->dataCount]=dataR;
            pack->dataCount++;
        }
        if(pack->dataCount==pack->dataLen){
            pack->isReady=true;
            pack->dataCount=0;
        }

        uint32_t statusFIFO=I2CFIFOStatus(I2C_registers_periph[i2c_num].I2C_BASE);
        int16_t counter=30;
        while((channel[i2c_num].streem->pack[channel[i2c_num].streem->number].type==CHIPS_REGISRT_DATA_RECEIVE)&&
              ((statusFIFO&I2C_FIFOSTATUS_RXFE)==0)&&counter){
            counter--;
            uint8_t tempRX=(uint8_t)I2CFIFODataGet(I2C_registers_periph[i2c_num].I2C_BASE);
            statusFIFO=I2CFIFOStatus(I2C_registers_periph[i2c_num].I2C_BASE);
        }
        send_next_byte(i2c_num);
    }
    else{
        send_next_byte(i2c_num);
    }
}

static void interupt_handler0()
{
    interupt_handler(0);
}
static void interupt_handler1()
{
    interupt_handler(1);
}
static void interupt_handler2()
{
    interupt_handler(2);
}
static void interupt_handler3()
{
    interupt_handler(3);
}
static void interupt_handler4()
{
    interupt_handler(4);
}
static void interupt_handler5()
{
    interupt_handler(5);
}
static void interupt_handler6()
{
    interupt_handler(6);
}
static void interupt_handler7()
{
    interupt_handler(7);
}
