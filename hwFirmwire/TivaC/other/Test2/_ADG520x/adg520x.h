/*
 * adg5208.h
 *
 *  Created on: 4 mar. 2020 �.
 *      Author: peresaliak
 */

#ifndef ADG520X_ADG520X_H_
#define ADG520X_ADG520X_H_

#include <stdint.h>

void ADG520x_init();
float* ADG520x_getCopyData();
void ADG520x_adc0Update(void);
int32_t ADG520x_setMulti(uint8_t num, float value);
int32_t ADG520x_setOffset(uint8_t num, float value);

#endif /* ADG520X_ADG520X_H_ */
