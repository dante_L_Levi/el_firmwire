/*
 * timers_struct.h
 *
 *  Created on: 13 mar. 2020
 *      Author: peresaliak
 */

#ifndef TIMERS_TIMERS_STRUCT_H_
#define TIMERS_TIMERS_STRUCT_H_

typedef enum
{
    TIMERS_NO_INIT = 0x00,
    TIMERS_INIT,
    TIMERS_START,
    TIMERS_END,
    TIMERS_RESERVED
}TIMERS_state_e;

typedef struct
{
    TIMERS_state_e  state;          //timer OFF (0) or ON (1)
    uint8_t flag_lock;              //if flag_lock==1 timer is not reset
    uint32_t period;                //period system clock
    uint32_t match;                 //match system clock
    uint32_t freq;                  //frequency in HZ
    void (*fCallbackLoad)(void);    //callback load
    void (*fCallbackMatch)(void);   //callback match
}TIMERS_state_s;

#endif /* TIMERS_TIMERS_STRUCT_H_ */
