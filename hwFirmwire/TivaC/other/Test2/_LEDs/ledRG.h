/*
 * leds.h
 *
 *  Created on: 5 ����. 2020 �.
 *      Author: frolov
 */

#ifndef LEDS_H_
#define LEDS_H_

#include "headers.h"

typedef enum{
    LED_OFF,
    LED_RED,
    LED_GREEN,
    LED_BOTH
}LED_state_e;

void LED_Init(void);
void LED_Switch(LED_state_e state);
void LED_Blink_RG(uint8_t blink_id, uint32_t interval);

#endif /* LEDS_H_ */
