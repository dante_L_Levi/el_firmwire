################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../_CAN/can_config.c \
../_CAN/can_short.c 

C_DEPS += \
./_CAN/can_config.d \
./_CAN/can_short.d 

OBJS += \
./_CAN/can_config.obj \
./_CAN/can_short.obj 

OBJS__QUOTED += \
"_CAN\can_config.obj" \
"_CAN\can_short.obj" 

C_DEPS__QUOTED += \
"_CAN\can_config.d" \
"_CAN\can_short.d" 

C_SRCS__QUOTED += \
"../_CAN/can_config.c" \
"../_CAN/can_short.c" 


