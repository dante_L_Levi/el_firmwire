################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../_BaseProject/board_address.c \
../_BaseProject/bootloader.c \
../_BaseProject/protection.c \
../_BaseProject/wd.c 

C_DEPS += \
./_BaseProject/board_address.d \
./_BaseProject/bootloader.d \
./_BaseProject/protection.d \
./_BaseProject/wd.d 

OBJS += \
./_BaseProject/board_address.obj \
./_BaseProject/bootloader.obj \
./_BaseProject/protection.obj \
./_BaseProject/wd.obj 

OBJS__QUOTED += \
"_BaseProject\board_address.obj" \
"_BaseProject\bootloader.obj" \
"_BaseProject\protection.obj" \
"_BaseProject\wd.obj" 

C_DEPS__QUOTED += \
"_BaseProject\board_address.d" \
"_BaseProject\bootloader.d" \
"_BaseProject\protection.d" \
"_BaseProject\wd.d" 

C_SRCS__QUOTED += \
"../_BaseProject/board_address.c" \
"../_BaseProject/bootloader.c" \
"../_BaseProject/protection.c" \
"../_BaseProject/wd.c" 


