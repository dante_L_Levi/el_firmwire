################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c123gh6pm.cmd 

C_SRCS += \
../main.c \
../sys_setup.c \
../tm4c123gh6pm_startup_ccs.c 

C_DEPS += \
./main.d \
./sys_setup.d \
./tm4c123gh6pm_startup_ccs.d 

OBJS += \
./main.obj \
./sys_setup.obj \
./tm4c123gh6pm_startup_ccs.obj 

OBJS__QUOTED += \
"main.obj" \
"sys_setup.obj" \
"tm4c123gh6pm_startup_ccs.obj" 

C_DEPS__QUOTED += \
"main.d" \
"sys_setup.d" \
"tm4c123gh6pm_startup_ccs.d" 

C_SRCS__QUOTED += \
"../main.c" \
"../sys_setup.c" \
"../tm4c123gh6pm_startup_ccs.c" 


