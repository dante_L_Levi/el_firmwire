################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
PINMUX_SRCS += \
../_pinmux/TM4C123xCS.pinmux 

C_SRCS += \
./syscfg/pinout.c 

GEN_FILES += \
./syscfg/pinout.c 

GEN_MISC_DIRS += \
./syscfg/ 

C_DEPS += \
./syscfg/pinout.d 

OBJS += \
./syscfg/pinout.obj 

GEN_MISC_FILES += \
./syscfg/pinout.h \
./syscfg/summary.csv 

GEN_MISC_DIRS__QUOTED += \
"syscfg\" 

OBJS__QUOTED += \
"syscfg\pinout.obj" 

GEN_MISC_FILES__QUOTED += \
"syscfg\pinout.h" \
"syscfg\summary.csv" 

C_DEPS__QUOTED += \
"syscfg\pinout.d" 

GEN_FILES__QUOTED += \
"syscfg\pinout.c" 

PINMUX_SRCS__QUOTED += \
"../_pinmux/TM4C123xCS.pinmux" 

C_SRCS__QUOTED += \
"./syscfg/pinout.c" 


