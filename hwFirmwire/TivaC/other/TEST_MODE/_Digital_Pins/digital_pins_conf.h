#include "headers.h"
/**********************************************************
 * Author: FROLOV S
 *
 * Description: Digital pins configuration
 * Go to configuration section for setting configuration
 **********************************************************/


//**********************************************************
//DECLARATION SECTION
//-----------------------------------------
//Structure describing port/pin conformity
typedef struct
{
    uint32_t port;
    uint32_t pin;
}GPIO_struct;
//----------------------------------------
//Structure describing pin configuration
typedef struct
{
    uint32_t mode;        //type of output/input
    uint32_t strenght;    //strenght level
    uint32_t pull_type;   //pull up/pull down
    uint8_t level;        //level after init
    uint8_t pin_id;       //pin ID
}PIN_s;
//----------------------------------------
//Modes that available for pins
typedef enum
{
    F_NONE=0,
    D_INPUT,
    D_OUTPUT,
    D_INT_FALL,
    D_INT_RISE,
    D_INT_FALLRISE
}GPIO_e;
//----------------------------------------
//Structure describing current state for selected (by id) pin
typedef struct
{
    uint8_t pin_id;
    uint8_t pin_level;
}PIN_STATE_s;
//----------------------------------------------------------------------------------

/*
 * Declaration of structure for port/pins conformity
 */
#ifdef PART_TM4C123GH6PM
#define PIN_QUANTITY            43
GPIO_struct pin_s[PIN_QUANTITY] =
{
   //PORT A
   GPIO_PORTA_BASE,GPIO_PIN_0,
   GPIO_PORTA_BASE,GPIO_PIN_1,
   GPIO_PORTA_BASE,GPIO_PIN_2,
   GPIO_PORTA_BASE,GPIO_PIN_3,
   GPIO_PORTA_BASE,GPIO_PIN_4,
   GPIO_PORTA_BASE,GPIO_PIN_5,
   GPIO_PORTA_BASE,GPIO_PIN_6,
   GPIO_PORTA_BASE,GPIO_PIN_7,
   //PORT B
   GPIO_PORTB_BASE,GPIO_PIN_0,
   GPIO_PORTB_BASE,GPIO_PIN_1,
   GPIO_PORTB_BASE,GPIO_PIN_2,
   GPIO_PORTB_BASE,GPIO_PIN_3,
   GPIO_PORTB_BASE,GPIO_PIN_4,
   GPIO_PORTB_BASE,GPIO_PIN_5,
   GPIO_PORTB_BASE,GPIO_PIN_6,
   GPIO_PORTB_BASE,GPIO_PIN_7,
   //PORT C
   GPIO_PORTC_BASE,GPIO_PIN_0,
   GPIO_PORTC_BASE,GPIO_PIN_1,
   GPIO_PORTC_BASE,GPIO_PIN_2,
   GPIO_PORTC_BASE,GPIO_PIN_3,
   GPIO_PORTC_BASE,GPIO_PIN_4,
   GPIO_PORTC_BASE,GPIO_PIN_5,
   GPIO_PORTC_BASE,GPIO_PIN_6,
   GPIO_PORTC_BASE,GPIO_PIN_7,
   //PORT D
   GPIO_PORTD_BASE,GPIO_PIN_0,
   GPIO_PORTD_BASE,GPIO_PIN_1,
   GPIO_PORTD_BASE,GPIO_PIN_2,
   GPIO_PORTD_BASE,GPIO_PIN_3,
   GPIO_PORTD_BASE,GPIO_PIN_4,
   GPIO_PORTD_BASE,GPIO_PIN_5,
   GPIO_PORTD_BASE,GPIO_PIN_6,
   GPIO_PORTD_BASE,GPIO_PIN_7,
   //PORT E
   GPIO_PORTE_BASE,GPIO_PIN_0,
   GPIO_PORTE_BASE,GPIO_PIN_1,
   GPIO_PORTE_BASE,GPIO_PIN_2,
   GPIO_PORTE_BASE,GPIO_PIN_3,
   GPIO_PORTE_BASE,GPIO_PIN_4,
   GPIO_PORTE_BASE,GPIO_PIN_5,
   //PORT F
   GPIO_PORTF_BASE,GPIO_PIN_0,
   GPIO_PORTF_BASE,GPIO_PIN_1,
   GPIO_PORTF_BASE,GPIO_PIN_2,
   GPIO_PORTF_BASE,GPIO_PIN_3,
   GPIO_PORTF_BASE,GPIO_PIN_4
};
#endif
#ifdef PART_TM4C129ENCPDT
#define PIN_QUANTITY            90
GPIO_struct pin_s[PIN_QUANTITY] =
{
   //PORT A
   GPIO_PORTA_BASE,GPIO_PIN_0,
   GPIO_PORTA_BASE,GPIO_PIN_1,
   GPIO_PORTA_BASE,GPIO_PIN_2,
   GPIO_PORTA_BASE,GPIO_PIN_3,
   GPIO_PORTA_BASE,GPIO_PIN_4,
   GPIO_PORTA_BASE,GPIO_PIN_5,
   GPIO_PORTA_BASE,GPIO_PIN_6,
   GPIO_PORTA_BASE,GPIO_PIN_7,
   //PORT B
   GPIO_PORTB_BASE,GPIO_PIN_0,
   GPIO_PORTB_BASE,GPIO_PIN_1,
   GPIO_PORTB_BASE,GPIO_PIN_2,
   GPIO_PORTB_BASE,GPIO_PIN_3,
   GPIO_PORTB_BASE,GPIO_PIN_4,
   GPIO_PORTB_BASE,GPIO_PIN_5,
   //PORT C
   GPIO_PORTC_BASE,GPIO_PIN_0,
   GPIO_PORTC_BASE,GPIO_PIN_1,
   GPIO_PORTC_BASE,GPIO_PIN_2,
   GPIO_PORTC_BASE,GPIO_PIN_3,
   GPIO_PORTC_BASE,GPIO_PIN_4,
   GPIO_PORTC_BASE,GPIO_PIN_5,
   GPIO_PORTC_BASE,GPIO_PIN_6,
   GPIO_PORTC_BASE,GPIO_PIN_7,
   //PORT D
   GPIO_PORTD_BASE,GPIO_PIN_0,
   GPIO_PORTD_BASE,GPIO_PIN_1,
   GPIO_PORTD_BASE,GPIO_PIN_2,
   GPIO_PORTD_BASE,GPIO_PIN_3,
   GPIO_PORTD_BASE,GPIO_PIN_4,
   GPIO_PORTD_BASE,GPIO_PIN_5,
   GPIO_PORTD_BASE,GPIO_PIN_6,
   GPIO_PORTD_BASE,GPIO_PIN_7,
   //PORT E
   GPIO_PORTE_BASE,GPIO_PIN_0,
   GPIO_PORTE_BASE,GPIO_PIN_1,
   GPIO_PORTE_BASE,GPIO_PIN_2,
   GPIO_PORTE_BASE,GPIO_PIN_3,
   GPIO_PORTE_BASE,GPIO_PIN_4,
   GPIO_PORTE_BASE,GPIO_PIN_5,
   //PORT F
   GPIO_PORTF_BASE,GPIO_PIN_0,
   GPIO_PORTF_BASE,GPIO_PIN_1,
   GPIO_PORTF_BASE,GPIO_PIN_2,
   GPIO_PORTF_BASE,GPIO_PIN_3,
   GPIO_PORTF_BASE,GPIO_PIN_4,
   //PORT G
   GPIO_PORTG_BASE,GPIO_PIN_0,
   GPIO_PORTG_BASE,GPIO_PIN_1,
   //PORT H
   GPIO_PORTH_BASE,GPIO_PIN_0,
   GPIO_PORTH_BASE,GPIO_PIN_1,
   GPIO_PORTH_BASE,GPIO_PIN_2,
   GPIO_PORTH_BASE,GPIO_PIN_3,
   //PORT J
   GPIO_PORTJ_BASE,GPIO_PIN_0,
   GPIO_PORTJ_BASE,GPIO_PIN_1,
   //PORT K
   GPIO_PORTK_BASE,GPIO_PIN_0,
   GPIO_PORTK_BASE,GPIO_PIN_1,
   GPIO_PORTK_BASE,GPIO_PIN_2,
   GPIO_PORTK_BASE,GPIO_PIN_3,
   GPIO_PORTK_BASE,GPIO_PIN_4,
   GPIO_PORTK_BASE,GPIO_PIN_5,
   GPIO_PORTK_BASE,GPIO_PIN_6,
   GPIO_PORTK_BASE,GPIO_PIN_7,
   //PORT L
   GPIO_PORTL_BASE,GPIO_PIN_0,
   GPIO_PORTL_BASE,GPIO_PIN_1,
   GPIO_PORTL_BASE,GPIO_PIN_2,
   GPIO_PORTL_BASE,GPIO_PIN_3,
   GPIO_PORTL_BASE,GPIO_PIN_4,
   GPIO_PORTL_BASE,GPIO_PIN_5,
   GPIO_PORTL_BASE,GPIO_PIN_6,
   GPIO_PORTL_BASE,GPIO_PIN_7,
   //PORT M
   GPIO_PORTM_BASE,GPIO_PIN_0,
   GPIO_PORTM_BASE,GPIO_PIN_1,
   GPIO_PORTM_BASE,GPIO_PIN_2,
   GPIO_PORTM_BASE,GPIO_PIN_3,
   GPIO_PORTM_BASE,GPIO_PIN_4,
   GPIO_PORTM_BASE,GPIO_PIN_5,
   GPIO_PORTM_BASE,GPIO_PIN_6,
   GPIO_PORTM_BASE,GPIO_PIN_7,
   //PORT N
   GPIO_PORTN_BASE,GPIO_PIN_0,
   GPIO_PORTN_BASE,GPIO_PIN_1,
   GPIO_PORTN_BASE,GPIO_PIN_2,
   GPIO_PORTN_BASE,GPIO_PIN_3,
   GPIO_PORTN_BASE,GPIO_PIN_4,
   GPIO_PORTN_BASE,GPIO_PIN_5,
   //PORT P
   GPIO_PORTP_BASE,GPIO_PIN_0,
   GPIO_PORTP_BASE,GPIO_PIN_1,
   GPIO_PORTP_BASE,GPIO_PIN_2,
   GPIO_PORTP_BASE,GPIO_PIN_3,
   GPIO_PORTP_BASE,GPIO_PIN_4,
   GPIO_PORTP_BASE,GPIO_PIN_5,
   //PORT Q
   GPIO_PORTQ_BASE,GPIO_PIN_0,
   GPIO_PORTQ_BASE,GPIO_PIN_1,
   GPIO_PORTQ_BASE,GPIO_PIN_2,
   GPIO_PORTQ_BASE,GPIO_PIN_3,
   GPIO_PORTQ_BASE,GPIO_PIN_4
};
#endif
//----------------------------------------------------------
//************************************************************************


//************************************************************************
//Configuration Section
//************************************************************************
//Defines enabled PIN modes

PIN_s PIN_Modes[PIN_QUANTITY] =
{
 0,0,0,0,0,   //A_0
 0,0,0,0,0,   //A_1
 0,0,0,0,0,   //A_2
 0,0,0,0,0,   //A_3
 0,0,0,0,0,   //A_4
 0,0,0,0,0,   //A_5
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,4,   //A_6
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,5,   //A_7

 0,0,0,0,0,   //B_0
 0,0,0,0,0,   //B_1
 0,0,0,0,0,   //B_2
 0,0,0,0,0,   //B_3
 0,0,0,0,0,   //B_4
 0,0,0,0,0,   //B_5
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,1,   //B_6
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,2,   //B_7

 0,0,0,0,0,   //C_0
 0,0,0,0,0,   //C_1
 0,0,0,0,0,   //C_2
 0,0,0,0,0,   //C_3
 0,0,0,0,0,   //C_4
 0,0,0,0,0,   //C_5
 0,0,0,0,0,   //C_6
 0,0,0,0,0,   //C_7

 0,0,0,0,0,   //D_0
 0,0,0,0,0,   //D_1
 0,0,0,0,0,   //D_2
 0,0,0,0,0,   //D_3
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,6,   //D_4
 0,0,0,0,0,   //D_5
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,7,   //D_6
 0,0,0,0,0,   //D_7

 0,0,0,0,0,   //E_0
 0,0,0,0,0,   //E_1
 0,0,0,0,0,   //E_2
 0,0,0,0,0,   //E_3
 0,0,0,0,0,   //E_4
 0,0,0,0,0,   //E_5

 0,0,0,0,0,   //F_0
 0,0,0,0,0,   //F_1
 0,0,0,0,0,   //F_2
 0,0,0,0,0,   //F_3
 D_INPUT, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD, 1,3   //F_4
};

//--------------------------------------------------------------------------












