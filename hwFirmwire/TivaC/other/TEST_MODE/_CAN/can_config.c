/*
 * can_config.c
 *
 *  Created on: 13 ���. 2020 �.
 *      Author: peresaliak
 */
#include <stdint.h>

//#define GPIO_PORT_SIL_BASEC     GPIO_PORTK_BASE
//#define GPIO_PIN_SIL            GPIO_PIN_5

#define SYSCTL_PERIPH_CAN       SYSCTL_PERIPH_CAN1
//#define SYSCTL_PERIPH_GPIOCAN   SYSCTL_PERIPH_GPIOA
#define CAN_BASE                CAN1_BASE
//#define GPIO_Pxx_CANxRX         GPIO_PA0_CAN1RX
//#define GPIO_Pxx_CANxTX         GPIO_PA1_CAN1TX
//#define GPIO_PORTCAN_BASE       GPIO_PORTA_BASE
//#define GPIO_PIN_RXCAN          GPIO_PIN_0
//#define GPIO_PIN_TXCAN          GPIO_PIN_1

#define CAN_NUM_PACK_MAX        31
#define CAN_NUM_MSG_MAX         10
#define CAN_NUM_OBJ_MAX         32
#define CAN_NUM_OBJ_START       1

typedef struct{
    uint8_t nMsg;
    uint8_t nObj;
    uint8_t flagPack;
    uint8_t lenPack;
}canPack_s;

typedef struct{
    uint8_t lenMsg;
    uint8_t firstPack;
    uint8_t flagTypeRx;             //0 - tx pack, 1 - rx pack
    void (*fCallback)(uint8_t);     //callback
    uint8_t* targetAddrRx;
    uint8_t flagLock;
}canMsg_s;
