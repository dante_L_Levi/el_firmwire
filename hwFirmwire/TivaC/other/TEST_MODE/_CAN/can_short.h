/*
 * can_short.h
 *
 *  Created on: 13 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef CAN_CAN_SHORT_H_
#define CAN_CAN_SHORT_H_

#include "stdint.h"

int32_t CAN_init(uint32_t);
int32_t CAN_setBitrate(uint32_t);
int32_t CAN_addReceiver(uint32_t id_start, uint32_t id_step,  uint8_t* reciver, uint8_t lenData, uint8_t* numMsg);
int32_t CAN_addTransmitter(uint32_t, uint32_t,  uint8_t, uint8_t*);
int32_t CAN_setCallback(uint8_t nMsg, void (*fCallback)(uint8_t));

int32_t CAN_transmitMessage(uint8_t, uint8_t*, uint8_t);
int32_t CAN_messageReceivedEnd(uint8_t, uint8_t*);
int32_t CAN_messageTransmitEnd(uint8_t, uint8_t*);
int32_t CAN_messageReset(uint8_t);

int32_t CAN_clear();
int32_t CAN_resetIfError();

uint32_t CAN_getCountReceived();
uint32_t CAN_getCountTransmit();
uint32_t CAN_getCountErrors();



#endif /* CAN_CAN_SHORT_H_ */
