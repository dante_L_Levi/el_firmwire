/*
 * _LTC2602_settings.h
 *
 *  Created on: 25 mar. 2020
 *      Author: peresaliak
 */

#ifndef LTC2602__LTC2602_SETTINGS_H_
#define LTC2602__LTC2602_SETTINGS_H_

#define LTC2602_ADDR_A              0x00
#define LTC2602_ADDR_B              0x01
#define LTC2602_ADDR_BOTH           0x0F

#define LTC2602_COM_WRn             0x00
#define LTC2602_COM_UPDn            0x01
#define LTC2602_COM_WRn_UPall       0x02
#define LTC2602_COM_WRn_UPn         0x03
#define LTC2602_COM_POW_DWn         0x04


#define LTC2602_CHIPS_COUNT         2
#define _LTC2602_SSI_TX_DATA_LEN    16
#define _LTC2602_SSI_TX_PACK_LEN    4

#if defined(PART_TM4C123GH6PM) || defined(PART_TM4C1294NCPDT) || defined(PART_TM4C129ENCPDT)
static const uint32_t ssi_sysctl_array[] = {
                                        SYSCTL_PERIPH_SSI0,
                                        SYSCTL_PERIPH_SSI1,
                                        SYSCTL_PERIPH_SSI2,
                                        SYSCTL_PERIPH_SSI3
};
static const uint32_t ssi_base_array[] = {
                                        SSI0_BASE,
                                        SSI1_BASE,
                                        SSI2_BASE,
                                        SSI3_BASE
};
#endif

#endif /* LTC2602__LTC2602_SETTINGS_H_ */
