

/**************************************************************************************
|   File name  : analog_pins.c
|   Description:
|   Author: FROLOV S
**************************************************************************************/

/**************************************************************************************
 * ADC module
 * See analog_pins_conf.h file
 * for configuration channels
 **************************************************************************************/

#include "settings.h"
#include <_Analog_Pins/analog_pins.h>
#include <_Analog_Pins/analog_pins_conf.h>

typedef enum
{
    ADC_NO_INIT = 0x00,
    ADC_INIT,
    ADC_START,
    ADC_NEW_DATA,
    ADC_READY
} adc_state_e;

//---------------------------
typedef struct
{
    adc_state_e state;                          //state
    uint8_t byte_count;                         //count of bytes in sequence
    uint32_t adc_values[MAX_COUNT_STEP];        //return analog data
    float adc_values_f[MAX_COUNT_STEP];
    void (*adcCallback)(void);                  //callback function
}ADC_state_s;

ADC_state_s adc_state[ADC_COUNT];

static void ADC_Set_Periph(uint8_t adc_id);
static void ADC_Start_Sequence(uint8_t adc_id);
static void ADC_Handler0(void);
static void ADC_Handler1(void);
static void timerInit();
/**************************************************************************************
|
**************************************************************************************/
uint32_t ADC_value;
void (*adcCallback) (void);
//-------------------------------------------------------------------------------------------
//Empty callback functions
static void adc_empty_callback(void){}
/**************************************************************************************
|
**************************************************************************************/
/*
 * Function - Start ADC module
 * Each ADC have Sequences. This function uses only Sequence0 by default
 * Seq0 can contains 8 channels for polling (for ADC0 And ADC1 both - 16 channels)
 */

void ADC_Init(void)
{
    //timerInit();

    for(int8_t adc_id=0; adc_id<ADC_COUNT;adc_id++){
        adc_state[adc_id].state = ADC_NO_INIT;
        adc_state[adc_id].adcCallback = adc_empty_callback;      //callback handler for ADC
        adc_state[adc_id].byte_count = 0;                        //byte count for sequence in ADC0
        if(ADC_config[adc_id].count_steps!=0){
            ADC_Set_Periph(adc_id);                                         //init ADC module

            for(int i=0;i<ADC_config[adc_id].count_steps;i++){
                uint8_t an_in=ADC_config[adc_id].an_in_step[i];
                GPIOPinTypeADC(adc_pin_s[an_in].port,adc_pin_s[an_in].pin);             //set pin as ADC input
                if(i==(ADC_config[adc_id].count_steps-1))
                    ADCSequenceStepConfigure(ADC_config[adc_id].adc_base, 0, i,
                                             an_in|ADC_CTL_IE|ADC_CTL_END);             //set last step
                else
                    ADCSequenceStepConfigure(ADC_config[adc_id].adc_base, 0, i,an_in);  //set step
            }
            adc_state[adc_id].byte_count = ADC_config[adc_id].count_steps*4;
            ADC_Start_Sequence(adc_id);
            adc_state[adc_id].state = ADC_INIT;
        }
        else{
            adc_state[adc_id].state = ADC_NO_INIT;
        }
    }
}

static void timerInit(){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, (SYS_CLOCK / UPDATE_FRQ));
    TimerControlTrigger(TIMER1_BASE, TIMER_A, true);
    TimerEnable(TIMER1_BASE, TIMER_A);
}

//------------------------------------------------------------------------------------------
/*
 * Setting periph for ADC0 or ADC1
 */
static void ADC_Set_Periph(uint8_t adc_id)
{
    switch(ADC_config[adc_id].adc_base)
    {
      case ADC0_BASE:
      {
          SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
          while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0));
          ADCSequenceDisable(ADC0_BASE, 0);
          ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PIOSC | ADC_CLOCK_RATE_EIGHTH, 1);
          ADCReferenceSet(ADC0_BASE, ADC_REF_INT);
          ADCSequenceConfigure(ADC0_BASE,0, ADC_config[adc_id].adc_trigger, 0);
          if(ADC_config[adc_id].adc_trigger==ADC_TRIGGER_TIMER)
              timerInit();
          break;
      }
      case ADC1_BASE:
      {
          SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
          while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1));
          ADCSequenceDisable(ADC1_BASE, 0);
          ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PIOSC | ADC_CLOCK_RATE_EIGHTH, 1);
          ADCReferenceSet(ADC1_BASE, ADC_REF_INT);
          ADCSequenceConfigure(ADC1_BASE,0, ADC_config[adc_id].adc_trigger, 0);
          if(ADC_config[adc_id].adc_trigger==ADC_TRIGGER_TIMER)
              timerInit();
          break;
      }
    }
}
//------------------------------------------------------------------------------------------
/*
 * Set sequence and interrupt for ADC0, ADC1
 */
//static void ADC_Set_Sequence(uint8_t adc_id, int last_chan)
static void ADC_Start_Sequence(uint8_t adc_id)
{
    ADCSequenceEnable(ADC_config[adc_id].adc_base, 0);
    switch(adc_id)
    {
        case 0:
          ADCIntRegister(ADC_config[adc_id].adc_base, 0, ADC_Handler0);
          break;
        case 1:
          ADCIntRegister(ADC_config[adc_id].adc_base, 0, ADC_Handler1);
          break;
    }
    ADCIntEnable(ADC_config[adc_id].adc_base, 0);
    ADCProcessorTrigger(ADC_config[adc_id].adc_base, 0);
}
//-----------------------------------------------------------------------------------------
/*
 *  Set callback function
 */
void ADC_Set_Callback(uint8_t adc_num, void (*pfCallback)(void))
{
    adc_state[adc_num].adcCallback = pfCallback;
}
//*******************************************************************************************

//-----------------------------------------------------------------------------------------
//Interrupt for ADC0
static void ADC_Handler0(void)
{
    ADCIntClear(ADC0_BASE, 0);
    if(adc_state[0].state==ADC_INIT)
    {
        adc_state[0].state = ADC_NEW_DATA;
        adc_state[0].adcCallback();
    }
}
//-----------------------------------------------------------------------------------------
//Interrupt for ADC1
static void ADC_Handler1(void)
{
    ADCIntClear(ADC1_BASE, 0);
    if(adc_state[1].state==ADC_INIT)
    {
       adc_state[1].state = ADC_NEW_DATA;
       adc_state[1].adcCallback();
    }
}

//-------------------------------------------------------------------------------------------
//Copy data to selected buffer
// adc_id - number of ADC (0 or 1)
uint32_t * ADC_copy_data(uint8_t adc_id)
{
    if(adc_id>=ADC_COUNT)
        return adc_state[0].adc_values;

    for(int8_t i=0;(i<10)&&(adc_state[adc_id].state==ADC_NEW_DATA);i++){
        ADCSequenceDataGet(ADC_config[adc_id].adc_base, 0, adc_state[adc_id].adc_values);
        adc_state[adc_id].state = ADC_INIT;
    }
    if(ADC_config[adc_id].auto_reload)
        ADCProcessorTrigger(ADC_config[adc_id].adc_base, 0);
    adc_state[adc_id].state = ADC_INIT;
    return adc_state[adc_id].adc_values;
}

float * ADC_copy_data_f(uint8_t adc_id)
{
    ADC_copy_data(adc_id);
    for(int8_t i=0;i<ADC_config[adc_id].count_steps;i++){
        adc_state[adc_id].adc_values_f[i]=(adc_state[adc_id].adc_values[i]-
                ADC_config[adc_id].offset[i])*
                ADC_config[adc_id].multi[i];
    }
    return adc_state[adc_id].adc_values_f;
}
//-------------------------------------------------------------------------------------------
/*
 * Convert ADC values through coefficients
 */
void ADC_convertStart(uint8_t adc_id)
{
    ADCProcessorTrigger(ADC_config[adc_id].adc_base, 0);
}

//-------------------------------------------------------------------------------------------
/*
 * set multiply
 */
int32_t ADC_setMulti(uint8_t ch, uint8_t num, float value)
{
    ADC_config[ch].multi[num]=value;
    return 0;
}
//-------------------------------------------------------------------------------------------
/*
 * set multiply
 */
int32_t ADC_setOffset(uint8_t ch, uint8_t num, float value)
{
    ADC_config[ch].offset[num]=value;
    return 0;
}
/**************************************************************************************
|  End file
**************************************************************************************/
