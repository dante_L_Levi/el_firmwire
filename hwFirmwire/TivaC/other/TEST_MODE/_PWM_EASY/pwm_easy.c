/*
 * pwm.c
 *
 *  Created on: 17 ���. 2020 �.
 *      Author: peresaliak
 */

#include <_PWM_EASY/pwm_easy.h>
#include <_PWM_EASY/pwm_easy_settings.h>
#include <_PWM_EASY/pwm_easy_struct.h>
#include "settings.h"


static pwmEasy_state_s pwm_state[PWM_EASY_PINS];

void PWM_EASY_init(float freq)
{
    //initTestPWM();
    SysCtlPWMClockSet(SYSCTL_PWMDIV_1);

    for(int8_t pwmNum=0;pwmNum<PWM_EASY_PINS;pwmNum++){
        uint8_t pinPwm=pwm_pins_enable[pwmNum];
        pwmEasy_pinConfig_s* pinConfig=&pwmEasy_configPins[pinPwm];
        if(pinConfig->PWMx_BASE==PWM0_BASE){
            if(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0)){
                SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
                while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0));
            }
        }
        if(pinConfig->PWMx_BASE==PWM1_BASE){
            if(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1)){
                SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
                while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1));
            }
        }

        PWMGenConfigure(pinConfig->PWMx_BASE, pinConfig->PWM_GEN_x, PWM_EASY_GEN_MODE);
        GPIOPinConfigure(pinConfig->GPIO_Pxx_MxWMx);
        GPIOPinTypePWM(pinConfig->GPIO_PORTx_BASE, pinConfig->GPIO_PIN_x);
        pwm_state[pwmNum].state=PWM_EASY_STOP;
        PWM_EASY_setFreq(pwmNum, freq);
        PWM_EASY_setDutyCycle(pwmNum, 50.0f);
        PWMGenEnable(pinConfig->PWMx_BASE, pinConfig->PWM_GEN_x);
    }
}

void PWM_EASY_setFreq(uint8_t pwmNum, float freq)
{
    if(pwm_state[pwmNum].state==PWM_EASY_NO_INIT)
        return;
    if(freq<=0.0f)
        PWM_EASY_stop(pwmNum);
    else{
        pwmEasy_pinConfig_s* pinConfig=&pwmEasy_configPins[pwm_pins_enable[pwmNum]];
        float freqmin=(float)SYS_CLOCK/UINT32_MAX;
        uint32_t period=freqmin<freq?SYS_CLOCK/freq:UINT32_MAX;
        PWMGenPeriodSet(pinConfig->PWMx_BASE, pinConfig->PWM_GEN_x, period);
        pwm_state[pwmNum].freq=freq;
        uint32_t width=pwm_state[pwmNum].dutyCycle*period;
        PWMPulseWidthSet(pinConfig->PWMx_BASE, pinConfig->PWM_OUT_x, width);
    }
}

void PWM_EASY_stop(uint8_t pwmNum)
{
    if(pwm_state[pwmNum].state==PWM_EASY_NO_INIT)
        return;
    pwmEasy_pinConfig_s* pinConfig=&pwmEasy_configPins[pwm_pins_enable[pwmNum]];
    PWMOutputState(pinConfig->PWMx_BASE, pinConfig->PWM_OUT_x_BIT, false);
    pwm_state[pwmNum].state=PWM_EASY_STOP;
}

void PWM_EASY_start(uint8_t pwmNum)
{
    if(pwm_state[pwmNum].state==PWM_EASY_NO_INIT)
        return;
    pwmEasy_pinConfig_s* pinConfig=&pwmEasy_configPins[pwm_pins_enable[pwmNum]];
    PWMOutputState(pinConfig->PWMx_BASE, pinConfig->PWM_OUT_x_BIT, true);
    pwm_state[pwmNum].state=PWM_EASY_RUN;
}

void PWM_EASY_setDutyCycle(uint8_t pwmNum, float dutyCycle)
{
    if(pwm_state[pwmNum].state==PWM_EASY_NO_INIT)
        return;
    pwmEasy_pinConfig_s* pinConfig=&pwmEasy_configPins[pwm_pins_enable[pwmNum]];
    uint32_t width=dutyCycle*PWMGenPeriodGet(pinConfig->PWMx_BASE,
                                             pinConfig->PWM_GEN_x)/100.0f;
    PWMPulseWidthSet(pinConfig->PWMx_BASE, pinConfig->PWM_OUT_x, width);
    PWMOutputState(pinConfig->PWMx_BASE, pinConfig->PWM_OUT_x_BIT, true);
    pwm_state[pwmNum].dutyCycle=dutyCycle;
    pwm_state[pwmNum].state=PWM_EASY_RUN;
}
