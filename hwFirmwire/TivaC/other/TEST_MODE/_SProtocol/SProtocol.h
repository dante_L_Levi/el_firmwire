/*
 * ShortProtocol.h
 *
 *  Created on: 5 feb. 2020
 *      Author: peresaliak
 */

#ifndef SPROTOCOL_H_
#define SPROTOCOL_H_

#include "settings.h"
#include "SProtocol_connect.h"

#define SPROTOCOL_PAYLOAD_LEN           8
#define SPROTOCOL_TRMT_BUFF_SIZE        1024

void SProtocol_initBuffers(uint16_t id,
                    int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length));
void SProtocol_setId(uint16_t id);
void SProtocol_resetInput();
bool SProtocol_itemAvailable();
SProtocol_message* SProtocol_getNextItem();
SProtocol_message* SProtocol_getNowItem();

void SProtocol_addTxMessageNoBlock(SProtocol_message *p);
bool SProtocol_rxPkgAvailable();

void SProtocol_parserRX(uint8_t* data, int16_t length);
void SProtocol_endTX();
void SProtocol_endRX();




#endif /* SHORTPROTOCOL_SHORTPROTOCOL_H_ */
