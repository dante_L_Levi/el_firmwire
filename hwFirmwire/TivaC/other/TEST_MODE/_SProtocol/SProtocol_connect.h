/*
 * SProtocol_parser.h
 *
 *  Created on: 19 ����. 2020 �.
 *      Author: peresaliak
 */

#ifndef SPROTOCOL_SPROTOCOL_CONNECT_H_
#define SPROTOCOL_SPROTOCOL_CONNECT_H_

#define SPROTOCOL_COMMAND_GO_BOOT       0xFF
#define SPROTOCOL_COMMAND_ENABLE        0xFE

#include "settings.h"

typedef enum{
    SP_CMD_RESERVED_0,
    SP_CMD_SET_MODE,
    SP_CMD_MSG_ERROR,
    SP_CMD_MSG_SERVICE,
    SP_CMD_SET_SETTINGS,
    SP_CMD_ECHO_PAYLOAD,
    SP_CMD_GET_INPUTS,
    SP_CMD_SEND_INPUTS,
    SP_CMD_SET_START_MODE,
    SP_CMD_SET_START_PWM,
    SP_CMD_SET_RESISTORS,
    SP_CMD_GET_RESISTORS,
    SP_CMD_SEND_RESISTORS,
    SP_CMD_ADC_CALIBRATION,
    SP_CMD_ADC_COEF_SAVE,
    SP_CMD_SET_DAC_OUTPUT,
    SPROTOCOL_COMMANDS_COUNT
}sprotocol_commands;

typedef struct
{
    uint8_t length;
    uint8_t command;
    uint8_t* data;
}SProtocol_message;

typedef void (*SP_command_function)(SProtocol_message* msg);

void SProtocol_init(uint16_t id,
             int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length));
void SProtocol_setFunction(sprotocol_commands cmd, SP_command_function p_func);
void SProtocol_lockCommand(sprotocol_commands command, bool lock);
void SProtocol_resetFunction(sprotocol_commands cmd);
void SProtocol_resetAllFunctions();
bool SProtocol_parseCriticalPack(SProtocol_message* msg);
void SProtocol_analysis();
void SProtocol_startAnalysis(bool start);
void SProtocol_addTxMessage(SProtocol_message *p);


#endif /* SPROTOCOL_SPROTOCOL_CONNECT_H_ */
