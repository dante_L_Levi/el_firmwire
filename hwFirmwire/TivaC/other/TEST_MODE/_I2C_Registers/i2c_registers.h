/*
 * chips_registers.h
 *
 *  Created on: 10 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef I2C_REGISTERS_H_
#define I2C_REGISTERS_H_

#include <stdbool.h>
#include <stdint.h>

#pragma pack(push, 1)
typedef struct {
    uint8_t* data;
    int16_t dataCount;
    int16_t dataLen;
    bool isReady;
}I2C_REGISTERS_receive_package_s;
#pragma pack(pop)

void I2C_REGISTERS_init(void);
void I2C_REGISTERS_activateI2C(uint8_t i2c_number);
void I2C_REGISTERS_deactivateI2CAll(void);
void I2C_REGISTERS_resetI2C(uint8_t i2c_number);
uint8_t I2C_REGISTERS_checkI2C(uint8_t i2c_number);
void I2C_REGISTERS_continueI2C(uint8_t i2c_number);
void I2C_REGISTERS_set_registr8(uint8_t, uint8_t, uint8_t, uint8_t);
I2C_REGISTERS_receive_package_s* I2C_REGISTERS_read_registr8(uint8_t, uint8_t, uint8_t, uint8_t*);
I2C_REGISTERS_receive_package_s* I2C_REGISTERS_read_mem(uint8_t, uint8_t, uint8_t, uint8_t*, uint8_t);

#endif /* I2C_REGISTERS_I2C_REGISTERS_H_ */
