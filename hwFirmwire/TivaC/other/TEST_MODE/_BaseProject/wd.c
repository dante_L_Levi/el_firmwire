/*
 * wd.c
 *
 *  Created on: 4 ���. 2019 �.
 *      Author: Gorudko
 */

#include "settings.h"
#include "driverlib/watchdog.h"
#include "settings.h"
#include "driverlib/sysctl.h"
#include "stdint.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "wd.h"
#include "bootloader.h"
#include "driverlib/interrupt.h"


//#define WD_ENABLED

#define TIME_DELAY_WDT                  SYS_CLOCK/2;

void handler_WDT(void)
{
    unsigned int state = WatchdogIntStatus(WATCHDOG0_BASE, true);
    WatchdogIntClear(WATCHDOG0_BASE);
    SysCtlReset();
    //GoToBootloader();
}

void InitWatchDog(){
#ifdef WD_ENABLED

    SysCtlPeripheralEnable(SYSCTL_PERIPH_WDOG0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_WDOG0));
    if(WatchdogLockState(WATCHDOG0_BASE) == true)
        WatchdogUnlock(WATCHDOG0_BASE);
    WatchdogReloadSet(WATCHDOG0_BASE, TIME_DELAY_WDT);
    WatchdogResetEnable(WATCHDOG0_BASE);

    WatchdogIntRegister(WATCHDOG0_BASE, handler_WDT);
    WatchdogIntTypeSet(WATCHDOG0_BASE, WATCHDOG_INT_TYPE_INT);
    WatchdogIntEnable(WATCHDOG0_BASE);

    //NVIC
    IntEnable(INT_WATCHDOG);
    IntPrioritySet(INT_WATCHDOG, 1);
#endif
}

void FeedWatchDog(){
#ifdef WD_ENABLED
    WatchdogReloadSet(WATCHDOG0_BASE, TIME_DELAY_WDT);
#endif
}
