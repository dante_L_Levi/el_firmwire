/*
 * _EEPROM_Settings.h
 *
 *  Created on: 24 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef _EEPROM_SETTINGS_H_
#define _EEPROM_SETTINGS_H_

#include "headers.h"

typedef enum {
    _EEPROOM_SETTINGS_SYS,
    _EEPROOM_SETTINGS_BOARD_ID,
    _EEPROOM_SETTINGS_COUNT
}_EEPROM_settings_e;

static const uint32_t _EEPROM_settings_addr[_EEPROOM_SETTINGS_COUNT]={
    0,
    6440
};

static const uint16_t _EEPROM_settings_len[_EEPROOM_SETTINGS_COUNT]={
    148,
    4
};

int32_t _EEPROM_settings_init();
int32_t _EEPROM_settings_setSetting(_EEPROM_settings_e,uint32_t*, uint16_t);
int32_t _EEPROM_settings_getSetting(_EEPROM_settings_e,uint32_t*, uint16_t);



#endif /* _EEPROM_SETTINGS_H_ */
