#include "headers.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_SProtocol/SProtocol.h"
#include "_RS485/rs485.h"
#include "_Digital_Pins/digital_pins.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_PWM_EASY/pwm_easy.h"
#include "_LTC2602/_LTC2602.h"
#include "_CAN/can_short.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"

/**
 * main.c
 */
int main(void)
{
	return 0;
}
