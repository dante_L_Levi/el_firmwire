/*
 * settings.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Kuntsevich
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <stdint.h>
#include <stdbool.h>
#include "_pinmux/pinout.h"

#define SYS_CLOCK               80000000

#pragma DATA_SECTION(_board_id, ".board_id")
static volatile const uint8_t _board_id [4] = {0x11,0xF7,'D','T'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;
static const char firmware[8] = "0.0.1  ";
static const char model[8] = "TST DAC";

//todo Baudrate GSN-CS
#define CAN_BAUDRATE_BRIDGE     125000
#define CAN_DIGITAL_RES1_ID      0x13
#define CAN_DIGITAL_RES2_ID      0x14




#endif /* SETTINGS_H_ */
