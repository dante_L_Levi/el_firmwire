

/**
 * main.c
 */

#include "headers.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_Digital_Pins/digital_pins.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_PWM_EASY/pwm_easy.h"
#include "_LTC2602/_LTC2602.h"
#include "_CAN/can_short.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"

#include "RS485Protocol.h"
#include "Main_Logic.h"





int main(void)
{
    Sys_Setup();
    WriteBoardAddress();

    LED_Init();

    TIMERS_init();
    Main_Init();



    while (1)
    {
        RS485_Command_Update_Async();
        if(Get_Status_Trigger()==0)
        {
          continue;
        }
        Reset_Trigger();
        Data_Update();

    }
}







