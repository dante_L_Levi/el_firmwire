/*
 * can.c
 *
 *  Created on: 26 ���. 2019 �.
 *      Author: Gorudko
 */

#include "can.h"
#include "driverlib/can.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_can.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "settings.h"

#define CAN_NUM 1

#if CAN_NUM == 0

#define CANx_BASE               CAN0_BASE
#define SYSCTL_PERIPH_CANx      SYSCTL_PERIPH_CAN0
#define INT_CANx                INT_CAN0

#elif CAN_NUM == 1

#define CANx_BASE               CAN1_BASE
#define SYSCTL_PERIPH_CANx      SYSCTL_PERIPH_CAN1
#define INT_CANx                INT_CAN1

#else

#endif

static void default_callback(uint8_t * box, uint32_t * id, uint8_t * data,
                             uint8_t * len);

static can_rx_callback callback = default_callback;

static void CanIrqHandler()
{
    unsigned int state = CANIntStatus(CANx_BASE, CAN_INT_STS_CAUSE);
    CANIntClear(CANx_BASE, state);
    //TODO looks like exist better way to do it
    if (state > 0 && state < 33)
    {
        tCANMsgObject sMsgObjectRx;
        uint8_t data[8];
        sMsgObjectRx.pui8MsgData = data;
        CANMessageGet(CANx_BASE, state, &sMsgObjectRx, true);
        callback((uint8_t *) &state, &sMsgObjectRx.ui32MsgID, data,
                 (uint8_t *) &sMsgObjectRx.ui32MsgLen);
    }
}

void CAN_Init(int32_t baudrate)
{
    //
    // Enable the CAN1 module.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_CANx);
    //
    // Wait for the CAN1 module to be ready.
    //
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_CANx))
    {
    }

    //
    // Reset the state of all the message objects and the state of the CAN
    // module to a known state.
    //
    CANInit(CANx_BASE);

    CANBitRateSet(CANx_BASE, SYS_CLOCK, baudrate);

    CANIntRegister(CANx_BASE, CanIrqHandler);
    CANIntEnable(CANx_BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

    //NVIC
    IntEnable(INT_CANx);

    CANEnable(CANx_BASE);
}

void CAN_initReceiveBox(uint8_t box, uint8_t is_id_extended, uint32_t id,
                        uint32_t id_mask)
{
    box &= 0x1f;
    tCANMsgObject sMsgObjectRx;
    sMsgObjectRx.ui32MsgID = id;
    sMsgObjectRx.ui32MsgIDMask = id_mask;
    //TODO check flags
    sMsgObjectRx.ui32Flags =
            is_id_extended ?
                    (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_EXTENDED_ID
                            | MSG_OBJ_USE_EXT_FILTER) :
                    MSG_OBJ_RX_INT_ENABLE;
    CANMessageSet(CANx_BASE, box, &sMsgObjectRx, MSG_OBJ_TYPE_RX);
}

void CAN_setRxCallback(can_rx_callback new_callback)
{
    callback = *new_callback;
}

void CAN_sendPackage(uint32_t tx_id, uint8_t * data, uint8_t len,
                     uint8_t tx_box)
{

    CANMessageClear(CANx_BASE, tx_box);
    tCANMsgObject sMsgObjectTx;
    sMsgObjectTx.ui32MsgID = tx_id;
    sMsgObjectTx.ui32MsgIDMask = 0;
    sMsgObjectTx.ui32Flags = 0;
    sMsgObjectTx.ui32MsgLen = len;
    sMsgObjectTx.pui8MsgData = data;
    CANMessageSet(CANx_BASE, tx_box, &sMsgObjectTx, MSG_OBJ_TYPE_TX);

}

void CAN_recovery()
{
    if (HWREG(CANx_BASE + CAN_O_STS) | CAN_STS_BOFF)
    {
        //if bus-off flag is set
        HWREG(CANx_BASE + CAN_O_CTL) &= ~CAN_CTL_INIT; //set 0 INIT flag in control register
    }
}

static void default_callback(uint8_t * box, uint32_t * id, uint8_t * data,
                             uint8_t * len)
{

}
