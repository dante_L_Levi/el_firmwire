/*
 * DataLogic.h
 *
 *  Created on: 9 ����. 2021 �.
 *      Author: Lepatenko
 */

#ifndef DATALOGIC_H_
#define DATALOGIC_H_



#include "stdint.h"
#include "stdio.h"
#include "string.h"




#pragma pack(push, 1)
typedef union
{
   struct
   {
       uint8_t Out_ADG5401_1:1;
       uint8_t Out_ADG5401_2:1;
   }field;
   uint8_t AllData;

}DIG_OUTPUT;
#pragma pack(pop)


#pragma pack(push, 1)

typedef struct
{
    int16_t channelA;
    int16_t channelB;

}_LTC2602_DEF;

#pragma pack(pop)




#pragma pack(push, 1)
typedef struct
{
    int16_t omega1_gsn;
   int16_t omega2_gsn;
   uint16_t time_to_target_ms;
   union{
       struct{
           uint8_t cpt         :1;
           uint8_t on_cnt      :1;
           uint8_t expl        :1;
           uint8_t block_cnt   :1;
           uint8_t disconn     :1;
           uint8_t reserved    :3;
       };
       uint8_t fields_data;
   }fields;
   union{
       struct{
           uint8_t lvl     :3;
           uint8_t state   :5;
       };
       uint8_t fields_data;
   }sys_state;
}can_gsnl_auto_expl_s;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
   uint16_t Ampl1;
   uint16_t Ampl2;
   uint16_t Ampl3;
   uint16_t Ampl4;
}can_gsnl_auto_image_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    can_gsnl_auto_expl_s    _base;
    can_gsnl_auto_image_s _image;
}can_PayLoad;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint16_t                        _ID;



    DIG_OUTPUT                  _GPIO_Output;
    struct
    {
      uint8_t ltc2602_channel[4];
     }chips;

     _LTC2602_DEF             _dac_out;


    uint8_t                         sync_trigger;
    uint8_t                         num_sysTimer;
    uint8_t                         num_CanTimer;
    uint32_t                        sysTime_ms;
    uint32_t                        canTime_s;
    uint16_t                        Led_period_Indicate;

    can_PayLoad                     _testPacket_CAN;


    struct
    {
        can_PayLoad                     txData;
        uint8_t                         txMsgNum;
        uint8_t                         rxData[8];
        uint8_t                         rxMsgNum;

    }can;

}work_state_model_s;





#pragma pack(pop)






#endif /* DATALOGIC_H_ */
