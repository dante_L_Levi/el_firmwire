/*
 * can.h
 *
 *  Created on: 26 ���. 2019 �.
 *      Author: Gorudko
 */

#ifndef CAN_H_
#define CAN_H_

#include "stdint.h"
#include "stdbool.h"


typedef void (*can_rx_callback)(uint8_t * box, uint32_t * id, uint8_t * data,
                                uint8_t * len);

void CAN_Init(int32_t baudrate);
void CAN_initReceiveBox(uint8_t box, uint8_t is_id_extended, uint32_t id,
                        uint32_t id_mask);
void CAN_setRxCallback(can_rx_callback new_callback);
void CAN_sendPackage(uint32_t tx_id, uint8_t * data, uint8_t len,
                     uint8_t tx_box);
void CAN_recovery();

#endif /* CAN_H_ */
