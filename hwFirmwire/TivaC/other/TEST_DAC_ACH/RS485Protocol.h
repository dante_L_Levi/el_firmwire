/*
 * RS485Protocol.h
 *
 *  Created on: 22 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_RS485PROTOCOL_H_
#define INC_RS485PROTOCOL_H_


#include "headers.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"


typedef enum
{
    ECHO_CMD=0x56,
    BOOT_CMD=0x57,
    REQUEST_PC_CMD,
    REQUEST_PC_CONTINUE_CMD,
    RESPONSE_PC_CMD,
    REQUEST_MCU_CMD,
    RESPONSE_MCU_CMD,
    RESPONSE_MCU_CONTINUE_CMD,

}CommandDef;


#define SIZE_PAYLOAD    8

#pragma pack(push, 1)
typedef struct
{
    uint16_t Id;
    uint8_t cmd;
    uint8_t dataPayload[SIZE_PAYLOAD];
    uint16_t CRC_cnt;

}Protocol_RS485Def;
#pragma pack(pop)






#define SIZE_PACKET     sizeof(Protocol_RS485Def)



/*************************Set ID**************************/
void Set_ID_device(uint16_t _id);

/******************Function Transmit Packet**************************/
void Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length);

/*******************helper Save bytes in Buffer***********************/
void RS485_RessiveData_helper(void);
/*******************Init Hardware*****************************/
void UART_Init_HW(uint32_t Baud);

/*******************Get Message**************************/
void Get_RS485RxMessage(Protocol_RS485Def* _rs485);
/*******************End Packet**************************/
uint8_t  RS485_rxPkgAvailable(void);
/*******************Set Status Available**************************/
void SetPkgAvailable(uint8_t st);



#endif /* INC_RS485PROTOCOL_H_ */
