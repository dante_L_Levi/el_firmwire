/*
 * Main_Logic.c
 *
 *  Created on: 8 ����. 2021 �.
 *      Author: Lepatenko
 */

#include "Main_Logic.h"
#include  "DataLogic.h"
#include "RS485Protocol.h"
#include "_LTC2602/_LTC2602.h"
#include "_ADG5401/adg5401.h"
#include "sys_setup.h"
#include "_Timers/timers.h"
#include "settings.h"
#include "can.h"
#include "_CAN/can_short.h"
#include "RS485Protocol.h"



#include "math.h"



typedef enum
{
    rs485_default_handler_cmd,
    getBoardName_cmd,
    getFirmware_cmd,
    RS485_Connect_cmd,
    RS485_Disconnect_cmd,
    Set_DAC_Outputs_cmd,
    GET_DAC_Outputs_cmd,
    Get_Dig_In_cmd
}_RS485_cmd_Ddef;



#define MAIN_SYS_LOOP       1000
#define CAN_TRANSMIT_FREQ   500


#define DAC_CHANNEL_A1      0
#define DAC_CHANNEL_B1      1

#define DAC_CHANNEL_A2      2
#define DAC_CHANNEL_B2      3




work_state_model_s _model_state;







static void UpdateFast_Loop(void);
static void Middle_Loop(void);


static void Set_DAC_OUT(void);



static void Set_Digital_Autopilot_Packet(void)
{
    //int16_t data_om=(int32_t)(_model_state.num_CanTimer)*1000;

    _model_state._testPacket_CAN._base.time_to_target_ms=_model_state.canTime_s;

    _model_state._testPacket_CAN._base.fields.block_cnt=1;
    _model_state._testPacket_CAN._base.fields.cpt=1;
    _model_state._testPacket_CAN._base.fields.disconn=1;
    _model_state._testPacket_CAN._base.fields.expl=1;
    _model_state._testPacket_CAN._base.fields.on_cnt=1;
    _model_state._testPacket_CAN._base.fields.reserved=2;

    _model_state._testPacket_CAN._base.omega1_gsn=(int16_t)(sin(_model_state.canTime_s));//random data
    _model_state._testPacket_CAN._base.omega2_gsn=(int16_t)(sin(-_model_state.canTime_s));//random data

    _model_state._testPacket_CAN._base.sys_state.lvl=3;
    _model_state._testPacket_CAN._base.sys_state.state=4;

    _model_state._testPacket_CAN._image.Ampl1=1000;
    _model_state._testPacket_CAN._image.Ampl2=2000;
    _model_state._testPacket_CAN._image.Ampl3=3000;
    _model_state._testPacket_CAN._image.Ampl4=4000;

    _model_state.can.txData=_model_state._testPacket_CAN;


    CAN_messageReset(_model_state.can.txMsgNum);
    int error=CAN_transmitMessage(_model_state.can.txMsgNum, (uint8_t*)& _model_state.can.txData, sizeof(_model_state.can.txData));
    CAN_resetIfError();

}


static void UpdateFast_Loop(void)
{
    Set_DAC_OUT();

}


static void Middle_Loop(void)
{

}


static void Set_DAC_OUT(void)
{
    LTC2602_setVoltage(DAC_CHANNEL_A1,(float)(_model_state._dac_out.channelA/1000.0));
    LTC2602_setVoltage(DAC_CHANNEL_B1,(float)(_model_state._dac_out.channelB/1000.0));


}



void callback_timSys(void)
{
    _model_state.sysTime_ms++;
    _model_state.sync_trigger=1;

}

void callback_timCan(void)
{
    _model_state.canTime_s++;
    Set_Digital_Autopilot_Packet();





}


void can_DIG_AUTOPL_rx_handler(uint8_t *box, uint32_t *id, uint8_t *data,
                               uint8_t *len)
{
    switch(*box)
    {
        case 1:
        {
            if (*id != CAN_DIGITAL_RES1_ID)
            {

            }
            break;
        }
        default:
            break;
    }
}


/********************Init Logic and peripheral*************/
void Main_Init(void)
{

    _model_state.Led_period_Indicate=100;

    LTC2602_init(0, 2000000);
    LTC2602_addChips(0, GPIO_PORTA_BASE, GPIO_PIN_3, &_model_state.chips.ltc2602_channel[0]);
    LTC2602_addChips(0, GPIO_PORTF_BASE, GPIO_PIN_0, &_model_state.chips.ltc2602_channel[2]);
    LTC2602_setChannelKoef(_model_state.chips.ltc2602_channel[0], 32767.5f, -2621.4f);
    LTC2602_setChannelKoef(_model_state.chips.ltc2602_channel[1], 32767.5f, -2621.4f);
    LTC2602_setVoltage(1, 0.0f);
    LTC2602_setVoltage(0, 0.0f);
    ADG5401_init();
    ADG5401_setState(0,true);
    ADG5401_setState(1,true);

    _model_state._ID=*rs485_id;
    Set_ID_device(_model_state._ID);
    UART_Init_HW(115200);




    CAN_init(CAN_BAUDRATE_BRIDGE);
    CAN_clear();
    CAN_addTransmitter(0x22, 0x10, sizeof(_model_state.can.txData), &_model_state.can.txMsgNum);
    CAN_addReceiver(0x01, 0x01, _model_state.can.rxData, 0, &_model_state.can.rxMsgNum);


    _model_state.num_sysTimer=TIMERS_getNewTimer(Sys_freq()/MAIN_SYS_LOOP, true, &callback_timSys, true);
    _model_state.num_CanTimer=TIMERS_getNewTimer(Sys_freq()/1, true, &callback_timCan, true);


    //uint8_t data[8]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

    //Transmit_Packet((CommandDef)0x01, data, 8);

    IntMasterEnable();
}



uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}


/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}



static void rs485_default_handler(uint8_t *data)
{

}

static void getBoardName(uint8_t *data)
{
    uint8_t buffer[8]={0};
    memcpy(buffer,model,sizeof(buffer));
    Transmit_Packet((CommandDef)getBoardName_cmd,buffer,sizeof(buffer));
}

static void getFirmware(uint8_t *data)
{
    uint8_t buffer[8]={0};
    memcpy(buffer,firmware,sizeof(buffer));
    Transmit_Packet((CommandDef)getFirmware_cmd,buffer,sizeof(buffer));
}

static void RS485_Connect(uint8_t *data)
{
    _model_state.Led_period_Indicate=250;
}

static void RS485_Disconnect(uint8_t *data)
{
    _model_state.Led_period_Indicate=100;
}

static void Set_DAC_Outputs(uint8_t *data)
{
   //memcpy(&_model_state._dac_out,data,sizeof(_LTC2602_DEF));
    _LTC2602_DEF *temp=(_LTC2602_DEF*)data;
    _model_state._dac_out=*temp;

}

static void Get_DAC_Outputs(uint8_t *data)
{

}

static void Get_Dig_In(uint8_t *data)
{

}





/*==================================================================================*/

typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[]=
{
        (rs485_command_handler) rs485_default_handler,
        (rs485_command_handler) getBoardName,
        (rs485_command_handler) getFirmware,
        (rs485_command_handler)RS485_Connect,
        (rs485_command_handler)RS485_Disconnect,
        (rs485_command_handler)Set_DAC_Outputs,
        (rs485_command_handler)Get_DAC_Outputs,
        (rs485_command_handler)Get_Dig_In


};



/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
    if (RS485_rxPkgAvailable() == 0)
                        return;

            //hard fix of data align trouble

            Protocol_RS485Def mess;
            Get_RS485RxMessage(&mess);
            rs485_commands_array[mess.cmd](mess.dataPayload);
            SetPkgAvailable(0);
}


/***************Data Sync************/
void Data_Update(void)
{


    UpdateFast_Loop();
    LED_Blink_RG(0,_model_state.Led_period_Indicate);
    Middle_Loop();
}






