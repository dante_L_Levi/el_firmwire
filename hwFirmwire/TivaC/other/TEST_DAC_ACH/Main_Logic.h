/*
 * Main_Logic.h
 *
 *  Created on: 8 ����. 2021 �.
 *      Author: Lepatenko
 */

#ifndef MAIN_LOGIC_H_
#define MAIN_LOGIC_H_



#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"
#include "stdlib.h"
#include  "string.h"

#include "_LEDs/ledRG.h"
#include "_LTC2602/_LTC2602.h"
#include "_ADG5401/adg5401.h"



/********************Init Logic and peripheral*************/
void Main_Init(void);

uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Update(void);




#endif /* MAIN_LOGIC_H_ */
