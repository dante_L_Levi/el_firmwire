/*
 * protection.c
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Gorudko
 */
#include "protection.h"
#include "stdint.h"
#include "inc/hw_types.h"
#include "inc/hw_flash.h"

/*this function lock debug. unlock sequence erase all flash
* 0 - fail
* 1 - locked earlier
* 2 - locked just how need full power reset
*/

uint8_t PermanentLockDebug(){

    if ((HWREG(FLASH_BOOTCFG) & 0b11) == 0) return 1;
    HWREG(FLASH_FMD) = 0xFFFFFFFC;
    HWREG(FLASH_FMA) = 0x75100000;
    HWREG(FLASH_FMC) = 0xA4420008;
    uint32_t timeout = 1000000;
    while(HWREG(FLASH_FMC) & 0b1){
        if (timeout-- == 0) {
            return 0;
        }
    }
    return 2;
}



