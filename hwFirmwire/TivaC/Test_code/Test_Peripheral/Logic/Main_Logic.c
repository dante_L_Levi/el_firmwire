/*
 * Main_Logic.c
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */


#include "Main_Logic.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_SProtocol/SProtocol.h"
#include "_RS485/rs485.h"
#include "_Digital_Pins/digital_pins.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_PWM_EASY/pwm_easy.h"
#include "_LTC2602/_LTC2602.h"
#include "_CAN/can_short.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"
#include <_I2C_Registers/i2c_registers.h>
#include "_MPU6050/MPU6050_I2C.h"



#include "Logic_Helpers.h"

uint8_t dt=0;
work_state_model_s _model_state;


void callback_timSys(void)
{
    _model_state._time_ms++;
}


void callback_TimSync(void)
{
    _model_state.sync_trigger=1;
}

static void modeSelectFunction(SProtocol_message* msg)
{
    _model_state.mode=(mode_system_e)msg->data[0];
}

static void spGetInputs(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_MainData=true;
}

static void spEchoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);
    _model_state.echo_count++;
}

static void spGetFirmwire(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_DataBoard=true;
}

static void spGetSettings(SProtocol_message* msg)
{
    _model_state.rs485_flag_Model_Settings=true;
}


static void spsetSettings(SProtocol_message* msg)
{

}


void Rs485_Sprotocol_TestTransmit(void)
{
    SProtocol_message mess;
    mess.command=SP_CMD_RESERVED_0;
    uint8_t dt[8]={1,2,3,4,5,6,7,8};
    mess.data=dt;
    mess.length=sizeof(dt);
    SProtocol_addTxMessage(&mess);
}


void Main_Init(void)
{
    _model_state.mode=MODE_SETUP_INIT;
    LED_Init();
    TIMERS_init();

    RS485_initHalf(0, 921600, GPIO_PORTD_BASE, GPIO_PIN_6, 0, 0);
    SProtocol_init(*rs485_id, RS485_dataTx);
     RS485_setCallbeack(SProtocol_parserRX, 0, SProtocol_endRX);

     I2C_REGISTERS_init();
     I2C_REGISTERS_activateI2C(0);
     _model_state.i2c_num_mpu6050=0;


     //I2C_REGISTERS_receive_package_s* data=I2C_REGISTERS_read_registr8(_model_state.i2c_num_mpu6050,0x68,0x75,&dt);
     _model_state.mpu6050_id=MPU_6050_Init(Rate_1kHz,Gyro_2000dps_mask,Accel_16g_mask);
     SProtocol_resetAllFunctions();
     SProtocol_setFunction(SP_CMD_SET_MODE, &modeSelectFunction);
     SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&spEchoFunction);
     SProtocol_setFunction(SP_CMD_GET_INPUTS,&spGetInputs);
     SProtocol_setFunction(SP_CMD_FIRMWIRE, &spGetFirmwire);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spGetSettings);
     SProtocol_setFunction(SP_CMD_SET_SETTINGS, &spsetSettings);


    _model_state.number_ms_Timer=TIMERS_getNewTimer(Sys_freq()/1000, true, &callback_timSys, true);
    _model_state.number_sync_Timer=TIMERS_getNewTimer(Sys_freq()/4000,true,&callback_TimSync,true);


    _model_state.mode=MODE_WORK_INIT;

}

static void Select_Mode_work(void);

void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}


uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}


static void Main_Logic(void)
{
    _model_state._input_state.fields.sw1==0?(_model_state._out_state.fields.gpio1=0):(_model_state._out_state.fields.gpio1=1);
    _model_state._input_state.fields.sw2==0?(_model_state._out_state.fields.gpio2=0):(_model_state._out_state.fields.gpio2=1);
    Set_Gpio_Out(&_model_state._out_state);


}

static mode_system_e mode_workInit(void)
{
    _model_state._out_state.All=0x00;
    _model_state.Led_count_ctrl=0;

    _model_state.rs485_flag_Model_DataBoard=false;
    _model_state.rs485_flag_Model_MainData=false;
    _model_state.rs485_flag_Model_Settings=false;


    return MODE_WORK;
}

void Read_IMU_mpu6050_Async(void)
{

    if(_model_state.mpu6050_id!=0)
    {
        MPU6050_ReadAll(_model_state._hw_mpu6050.fields_Gyro.Gyro_Data,
                        _model_state._hw_mpu6050.fields_Accel.Accel_Data,
                        &_model_state._hw_mpu6050.Temp_hw);


    }


}

static void Select_Mode_work(void)
{
    switch(_model_state.mode)
    {
        case MODE_WORK_INIT:
        {
            _model_state.mode=mode_workInit();
            break;
        }

        case MODE_WORK:
        {
            //Read Data
            Get_Read_Gpio(&_model_state._input_state);
            MPU6050_ReadAll(_model_state._hw_mpu6050.fields_Gyro.Gyro_Data,
                            _model_state._hw_mpu6050.fields_Accel.Accel_Data,
                            &_model_state._hw_mpu6050.Temp_hw);

            //Main Logic
            Main_Logic();
            break;
        }

        case MODE_SETUP_INIT:
        {

            break;
        }

        case MODE_SETUP:
        {
            break;
        }

        case MODE_TEST_INIT:
        {
            break;
        }

        case MODE_TEST:
        {

            break;
        }


    }
}


static void First_Loop(void)
{
    Select_Mode_work();
}


static void Middle_Loop(void)
{
    static int32_t skip_counter = 1;
                if (skip_counter--)
                    return;
   skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;

   //Rs485_Sprotocol_TestTransmit();



}


void Main_Logic_Sync(void)
{
    First_Loop();
    Middle_Loop();
}



void RS485_Async(void)
{
    SProtocol_analysis();
    if(_model_state.rs485_flag_Model_DataBoard)
    {
        _model_state.rs485_flag_Model_DataBoard=false;
        _model_state._data1.ID_temp=*rs485_id;
        memcpy(_model_state._data1.Name, model,sizeof(model));
        memcpy(_model_state._data1.firmwire, firmware,sizeof(firmware));

        SProtocol_message mess;
        mess.command=SP_CMD_FIRMWIRE;
        mess.data=(uint8_t*)&_model_state._data1;
        mess.length=sizeof(Transmit_data1_Model_DataBoard);
        SProtocol_addTxMessage(&mess);

    }
    if(_model_state.rs485_flag_Model_MainData)
    {
        _model_state.rs485_flag_Model_MainData=false;

        _model_state._data2._input_state=_model_state._input_state;
        _model_state._data2._out_state=_model_state._out_state;
        _model_state._data2.mode=_model_state.mode;
        _model_state._data2._mpu6050=_model_state._mpu6050_mainData;



        SProtocol_message mess;
        mess.command=SP_CMD_GET_INPUTS;
        mess.data=(uint8_t*)&_model_state._data2;
        mess.length=sizeof(Transmit_data2_Model_MainData);
        SProtocol_addTxMessage(&mess);




    }
    if(_model_state.rs485_flag_Model_Settings)
    {
        _model_state.rs485_flag_Model_Settings=false;


    }

}

