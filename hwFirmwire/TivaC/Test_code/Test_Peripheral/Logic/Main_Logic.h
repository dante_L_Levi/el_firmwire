/*
 * Main_Logic.h
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_MAIN_LOGIC_H_
#define LOGIC_MAIN_LOGIC_H_

#include "Main.h"
#include "Logic_types.h"




void Main_Init(void);
void Reset_Trigger(void);
uint8_t Get_Status_Trigger(void);
void Main_Logic_Sync();
void RS485_Async(void);
void Read_IMU_mpu6050_Async(void);


#endif /* LOGIC_MAIN_LOGIC_H_ */
