/*
 * Logic_helpres.c
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "Logic_Helpers.h"


void Get_Read_Gpio(gpio_input_def* input)
{
    (!GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_0))?(input->fields.sw1=1):(input->fields.sw1=0);
    (!GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4))?(input->fields.sw2=1):(input->fields.sw2=0);
}


void Set_Gpio_Out(gpio_out_def* out)
{
    (out->fields.gpio1)?(GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, 0xFF)):(GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, 0x00));
    (out->fields.gpio2)?(GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0xFF)):(GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0x00));
}
