/*
 * MPU6050_I2C.c
 *
 *  Created on: 10 ���. 2021 �.
 *      Author: AlexPirs
 */

#include "MPU6050_I2C.h"

#include "headers.h"
#include "settings.h"
uint16_t delay = 350;

float sensivity[3];

typedef struct
{
    int32_t Temp_hw;
       union
       {
           struct
           {
               int32_t AX;
               int32_t AY;
               int32_t AZ;
           };
           int32_t Accel_Data[3];
       }fields_Accel;

       union
          {
              struct
              {
                  int32_t GX;
                  int32_t GY;
                  int32_t GZ;
              };
              int32_t Gyro_Data[3];
          }fields_Gyro;

}mpu6050_inner_state;


#define Dev_ADRESS                  0x68
#define Check_WHO_I_AM              0x68

#define I2C_REGISTERS_SYSCTL_PERIPH_I2C         SYSCTL_PERIPH_I2C0
#define I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE    SYSCTL_PERIPH_GPIOB
#define I2C_REGISTERS_I2C_BASE                  I2C0_BASE
#define I2C_REGISTERS_I2C_PORT_BASE             GPIO_PORTB_BASE
#define I2C_REGISTERS_SCL_PIN                   GPIO_PIN_2
#define I2C_REGISTERS_SDA_PIN                   GPIO_PIN_3
#define I2C_REGISTERS_GPIO_Pxx_I2CxSCL          GPIO_PB2_I2C0SCL
#define I2C_REGISTERS_GPIO_Pxx_I2CxSDA          GPIO_PB3_I2C0SDA

#define I2C_REGISTERS_LENGTH_SEND_BUFFER             128
#define I2C_REGISTERS_COUNT_RECEIVE_PACK              16

mpu6050_inner_state DataStruct;

static void MPU6050_Master_Transmit_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *value,uint8_t count);
static uint8_t MPU6050_Master_read_I2C(uint8_t addrDev,uint8_t Reg);
static void MPU6050_Master_read_All_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *data,uint8_t len);
static void MPU6050_Init_Hardware(void);




static void Select_Sensivity_Accel_mpu6050(Accel_Config range_accel)
{


    switch(range_accel)
    {
        case Accel_2g_mask:
        {
            sensivity[0]=(float)Sensivity_accel_2000mg;
        }

        case Accel_4g_mask:
        {
            sensivity[0]=(float)Sensivity_accel_4000mg;
            break;
        }

        case Accel_8g_mask:
        {
            sensivity[0]=(float)Sensivity_accel_8000mg;
            break;
        }

        case Accel_16g_mask:
        {
            sensivity[0]=(float)Sensivity_accel_16000mg;
            break;
        }

        default:
            sensivity[0]=1.0f;
            break;
    }

}


static void Select_Sensivity_Gyro_mpu6050(Gyro_Config range_gyro)
{
    switch(range_gyro)
        {
            case Gyro_250dps_mask:
            {
                sensivity[1]=(float)Sensivity_gyro_250dps;
            }

            case Gyro_500dps_mask:
            {
                sensivity[1]=(float)Sensivity_gyro_500dps;
                break;
            }

            case Gyro_1000dps_mask:
            {
                sensivity[1]=(float)Sensivity_gyro_1000dps;
                break;
            }

            case Gyro_2000dps_mask:
            {
                sensivity[1]=(float)Sensivity_gyro_2000dps;
                break;
            }

            default:
                sensivity[1]=1.0f;
                break;
        }
}





static void MPU6050_Master_Transmit_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *value,uint8_t count)
{
    uint16_t i = 0;


    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    for(i = 0; i < delay; i++);

    if(count == 1)
    {
        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value);
        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
        for(i = 0; i < delay; i++);
    }
    else
    {
        while(count)
                {
                    if(count > 1)
                    {
                        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value++);
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
                        for(i = 0; i < delay; i++);
                    }
                    else if(count  == 1)
                    {
                        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value);
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
                        for(i = 0; i < delay; i++);
                    }
                    count--;
                }
    }



}


static uint8_t MPU6050_Master_read_I2C(uint8_t addrDev,uint8_t Reg)
{
    uint16_t i = 0;

    uint8_t result=0x00;

    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    for(i = 0; i < delay; i++);
    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
    for(i = 0; i < delay; i++);
    result=(uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
    return result;

}


static void MPU6050_Master_read_All_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *data,uint8_t len)
{
    uint16_t i = 0;


    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    for(i = 0; i < delay; i++);

    if(len == 1)
    {
       I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
       I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
       for(i = 0; i < delay; i++);
       *data = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
    }
    else
    {
        I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
                I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
                for(i = 0; i < delay; i++);
                *data++ = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                len--;

                while(len)
                {
                    if(len > 1)
                    {
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                        for(i = 0; i < delay; i++);
                        *data++ = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                    }
                    else if(len == 1)
                    {
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
                        for(i = 0; i < delay; i++);
                        *data = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                    }
                    len--;
                }
    }


}




static void MPU6050_Init_Hardware(void)
{
    //Setting I2C
        SysCtlPeripheralEnable(I2C_REGISTERS_SYSCTL_PERIPH_I2C);
        while(!SysCtlPeripheralReady(I2C_REGISTERS_SYSCTL_PERIPH_I2C));

        SysCtlPeripheralEnable(I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE);
        while(!SysCtlPeripheralReady(I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE));

        GPIODirModeSet(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SCL_PIN | I2C_REGISTERS_SDA_PIN, GPIO_DIR_MODE_HW);
        GPIOPinConfigure(I2C_REGISTERS_GPIO_Pxx_I2CxSCL);
        GPIOPinConfigure(I2C_REGISTERS_GPIO_Pxx_I2CxSDA);
        GPIOPinTypeI2CSCL(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SCL_PIN);
        GPIOPinTypeI2C(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SDA_PIN);


        I2CMasterEnable(I2C_REGISTERS_I2C_BASE);
        I2CMasterInitExpClk(I2C_REGISTERS_I2C_BASE, SYS_CLOCK, false); //true - 400kbps, false - 100kbps

        //I2CIntRegister(I2C_REGISTERS_I2C_BASE, &interupt_handler);
        I2CMasterIntEnableEx(I2C_REGISTERS_I2C_BASE, (I2C_MASTER_INT_ARB_LOST |
                            I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |
                            I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));
}



uint8_t Read_Id(void)
{
    uint8_t check;
    // check device ID WHO_AM_I
    check = MPU6050_Master_read_I2C(Dev_ADRESS,WHO_AM_I_REG);
    return check;
}









/*****************************************Init MPU6050*******************************/
uint8_t MPU_6050_Init(Sample_Rate _rate,Gyro_Config _set_gyro,Accel_Config _set_accel)
{
    MPU6050_Init_Hardware();
    uint8_t ID=Read_Id();
    if(ID==Check_WHO_I_AM)
    {
        Select_Sensivity_Accel_mpu6050(_set_accel);
        Select_Sensivity_Gyro_mpu6050(_set_gyro);
        sensivity[2]=(1/340.0f)+36.53f;
        // Set DATA RATE of 1KHz by writing SMPLRT_DIV register
        uint8_t tempData=0x00;
        tempData=(uint8_t)_rate;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,SMPLRT_DIV_REG,&tempData,1);

        // power management register 0X6B we should write all 0's to wake the sensor up
        tempData=0x01;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,PWR_MGMT_1_REG,&tempData,1);


        // Set accelerometer configuration in ACCEL_CONFIG Register
        // XA_ST=0,YA_ST=0,ZA_ST=0, FS_SEL=0 ->2g
        tempData=0x00;
        tempData=MPU6050_Master_read_I2C(Dev_ADRESS,ACCEL_CONFIG_REG);
        tempData&=~MPU6050_ACC_FS_XL_MASK;
        tempData|=(uint8_t)_set_accel;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,ACCEL_CONFIG_REG,&tempData,1);

        // Set Gyroscopic configuration in GYRO_CONFIG Register
        // XG_ST=0,YG_ST=0,ZG_ST=0, FS_SEL=0 ->250 dps
        tempData=0x00;
        tempData=MPU6050_Master_read_I2C(Dev_ADRESS,GYRO_CONFIG_REG);
        tempData&=~MPU6050_GYRO_FS_G_MASK;
        tempData|=(uint8_t)_set_gyro;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,GYRO_CONFIG_REG,&tempData,1);

        tempData=0x01;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,INT_ENABLE,&tempData,1);


    }
    return ID;
}





/***********************************Read DataPayload MPU6050*******************/
void MPU6050_ReadAll(int32_t *Gyro,int32_t *Accel,int32_t *Temp)
{
    uint8_t Rec_Data[14];

    MPU6050_Master_read_All_I2C(Dev_ADRESS,ACCEL_XOUT_H_REG,Rec_Data,14);

    DataStruct.fields_Accel.AX = (int16_t) (Rec_Data[0] << 8 | Rec_Data[1]);
    DataStruct.fields_Accel.AY = (int16_t) (Rec_Data[2] << 8 | Rec_Data[3]);
    DataStruct.fields_Accel.AZ = (int16_t) (Rec_Data[4] << 8 | Rec_Data[5]);
    DataStruct.Temp_hw = (int16_t) (Rec_Data[6] << 8 | Rec_Data[7]);
    DataStruct.fields_Gyro.GX = (int16_t) (Rec_Data[8] << 8 | Rec_Data[9]);
    DataStruct.fields_Gyro.GY = (int16_t) (Rec_Data[10] << 8 | Rec_Data[11]);
    DataStruct.fields_Gyro.GZ = (int16_t) (Rec_Data[12] << 8 | Rec_Data[13]);

    for(int i=0;i<3;i++)
        {
        Accel[i]=DataStruct.fields_Accel.Accel_Data[i];
        Gyro[i]=DataStruct.fields_Gyro.Gyro_Data[i];

        }
        *Temp=DataStruct.Temp_hw;

}


void GetValue_mpu6050_real(float* accel,float* gyro, float* temp)
{

    for(int i=0;i<3;i++)
    {
        accel[i]=(float)(DataStruct.fields_Accel.Accel_Data[i])/sensivity[0];
        gyro[i]=(float)(DataStruct.fields_Gyro.Gyro_Data[i])/sensivity[1];

    }
    *temp=(float)DataStruct.Temp_hw*sensivity[2];

}








