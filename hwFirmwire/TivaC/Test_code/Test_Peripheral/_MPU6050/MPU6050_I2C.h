/*
 * MPU6050_I2C.h
 *
 *  Created on: 10 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_MPU6050_I2C_H_
#define INC_MPU6050_I2C_H_




#include "stdint.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#define WHO_AM_I_REG 0x75
#define PWR_MGMT_1_REG 0x6B
#define SMPLRT_DIV_REG 0x19
#define ACCEL_CONFIG_REG 0x1C
#define ACCEL_XOUT_H_REG 0x3B
#define TEMP_OUT_H_REG 0x41
#define GYRO_CONFIG_REG 0x1B
#define GYRO_XOUT_H_REG 0x43
#define INT_ENABLE          0x38



#define MPU6050_ACC_FS_XL_2g                               0x00
#define MPU6050_ACC_FS_XL_4g                               0x08
#define MPU6050_ACC_FS_XL_8g                               0x10
#define MPU6050_ACC_FS_XL_16g                              0x18
#define MPU6050_ACC_FS_XL_MASK                             0x18



//����������������
#define MPU6050_GYRO_FS_G_250dps                            0x00
#define MPU6050_GYRO_FS_G_500dps                            0x08
#define MPU6050_GYRO_FS_G_1000dps                           0x10
#define MPU6050_GYRO_FS_G_2000dps                           0x18
#define MPU6050_GYRO_FS_G_MASK                              0x18








typedef enum
{

    Accel_2g_mask=0x00,
    Accel_4g_mask=0x08,
    Accel_8g_mask=0x10,
    Accel_16g_mask=0x18


}Accel_Config;




typedef enum
{

    Gyro_250dps_mask=0x00,
    Gyro_500dps_mask=0x08,
    Gyro_1000dps_mask=0x10,
    Gyro_2000dps_mask=0x18

}Gyro_Config;

typedef enum
{
    Rate_1kHz=0x07,
    Rate_2kHz=0x02,
    Rate_4kHz=0x01,
    Rate_8kHz=0x00


}Sample_Rate;

typedef enum
{
    Sensivity_gyro_250dps=131000,
    Sensivity_gyro_500dps=65500,
    Sensivity_gyro_1000dps=32800,
    Sensivity_gyro_2000dps=16400


}Sensivity_mpu6050_gyro;


typedef enum
{
    Sensivity_accel_2000mg=16384,
    Sensivity_accel_4000mg=8192,
    Sensivity_accel_8000mg=4096,
    Sensivity_accel_16000mg=2048


}Sensivity_mpu6050_accel;

/*****************************************Init MPU6050*******************************/
 uint8_t MPU_6050_Init(Sample_Rate _rate,Gyro_Config _set_gyro,Accel_Config _set_accel);
/***********************************Read DataPayload MPU6050*******************/
 void MPU6050_ReadAll(int32_t *Gyro,int32_t *Accel,int32_t *Temp);
 void GetValue_mpu6050_real(float* accel,float* gyro, float* temp);


#endif /* INC_MPU6050_I2C_H_ */
