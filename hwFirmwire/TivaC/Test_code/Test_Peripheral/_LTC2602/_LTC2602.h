/*
 * _LTC2602.h
 *
 *  Created on: 25 mar. 2020
 *      Author: peresaliak
 */

#ifndef LTC2602__LTC2602_H_
#define LTC2602__LTC2602_H_

#include "headers.h"



int32_t LTC2602_init(uint8_t, uint32_t);
int32_t LTC2602_addChips(uint8_t, uint32_t , uint32_t , uint8_t*);
int32_t LTC2602_setChannelKoef(uint8_t, float, float);
int32_t LTC2602_setVoltage(uint8_t, float);


#endif /* LTC2602__LTC2602_H_ */
