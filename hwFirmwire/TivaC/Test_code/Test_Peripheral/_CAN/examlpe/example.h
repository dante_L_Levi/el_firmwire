/*
 * example.h
 *
 *  Created on: 29 jan. 2020
 *      Author: peresaliak
 */

#ifndef CAN_EXAMLPE_EXAMPLE_H_
#define CAN_EXAMLPE_EXAMPLE_H_

#pragma pack(push, 1)
typedef struct {
    uint8_t         data[16];
} example_slave1_data_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    uint8_t         data[8];
} example_slave2_data_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    example_slave1_data_s   data1;
    example_slave2_data_s   data2;
} example_rmsft_data_s;
#pragma pack(pop)


#endif /* CAN_EXAMLPE_EXAMPLE_H_ */
