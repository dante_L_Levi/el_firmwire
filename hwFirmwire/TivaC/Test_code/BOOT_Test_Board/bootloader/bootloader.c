/*
 * bootloader.c
 *
 *  Created on: 15 ����. 2017 �.
 *      Author: Gorudko
 */

#include "bootloader.h"



static uint8_t Bootloader_checkKeyInFlash();
static uint8_t Bootloader_isApplicationExist();
static void Bootloader_GoToUserApp();
static void Bootloader_FirmwareUpdate();
static void Bootloader_eriseKeyInFlash();

uint8_t Bootloader_checkKeyInFlash(){

	uint32_t * flash_key = (uint32_t *)(BOOTLOADER_KEY_START_ADDRESS);

	if (*flash_key == FIRMWARE_UPDATE_KEYWORD) {
		return 1;
	}
	else {
		return 0;
	}
}
void Bootloader_eriseKeyInFlash(){
    uint32_t * flash_key = (uint32_t *)(BOOTLOADER_KEY_START_ADDRESS);
    *flash_key = 0;
}

uint8_t Bootloader_isApplicationExist(){

	uint8_t * ptr = (uint8_t *)APP_PROGRAM_START_ADDRESS;

	 if(*ptr != 0xFF){
		//app exist
		return 1;
	}
	else {
		return 0;
	}
}


void __attribute__((naked))
CallApplication(uint_fast32_t ui32StartAddr)
{
    //
    // Set the vector table to the beginning of the app in flash.
    //
    volatile uint32_t * vtor;
    vtor = (volatile uint32_t *)NVIC_VTABLE;
    *vtor = ui32StartAddr;

    //
    // Load the stack pointer from the application's vector table.
    //
    __asm("    ldr     r1, [r0]\n"
          "    mov     sp, r1");

    //
    // Load the initial PC from the application's vector table and branch to
    // the application's entry point.
    //
    __asm("    ldr     r0, [r0, #4]\n"
          "    bx      r0\n");
}



void Bootloader_GoToUserApp()
{
    CallApplication(APP_PROGRAM_START_ADDRESS);
    //(*((void (*)(void))(*(uint32_t *)APP_PROGRAM_START_ADDRESS)))();
}


void Bootloader_FirmwareUpdate(){

	  uint32_t flash_address_carriage = APP_PROGRAM_START_ADDRESS;
	  uint8_t exit_flag = 0;
	  while (exit_flag == 0)
	  {
		 uint8_t command;
		 Uart_getByte(&command);
		 if (command != 27) Uart_sendBytes(&command, 1); //if not ESCAPE
		 switch (command)
		 {
			case 'A': //set address
				Uart_getBytes((uint8_t *)&flash_address_carriage, 4);
				Uart_sendBytes("\r", 1);// Send OK back.
			    break;
			case 'E': //erase page
			{
				uint8_t tmp;
				Uart_getBytes(&tmp, 1);
				if(tmp == '1'){
					Flash_erasePage(flash_address_carriage);
					Uart_sendBytes("\r", 1);// Send OK back.
				}
				break;
			}
			case 'R': //read subpage
			{
				Uart_sendBytes((uint8_t *)flash_address_carriage, SUBPAGE_SIZE);
				Uart_sendBytes("\r", 1); // Send OK back.
				break;
			}
			case 'W': //write subpage
			{
				uint8_t tmp_array[SUBPAGE_SIZE];
				Uart_getBytes(tmp_array,SUBPAGE_SIZE);
				Flash_writeBytes(flash_address_carriage,tmp_array,SUBPAGE_SIZE);
				Uart_sendBytes("\r", 1); // Send OK back.
				break;
			}
			case 'M': //which it's device?
			{
				uint8_t tmp;
				Uart_getBytes(&tmp, 1);
				if(tmp == '1'){
					Uart_sendBytes((uint8_t *)&MCU_code, 4);
					Uart_sendBytes("\r", 1);// Send OK back.
				}
				break;
			}

			case 'L': //load (read) EEPROM by address
			    Uart_sendBytes("\r", 1); // Send OK back.
			    break;
			case 'S': //set (write) EEPROM by address
			    Uart_sendBytes("\r", 1); // Send OK back.
			    break;
			case 'B': //read security bits
			    Uart_sendBytes("\r", 1); // Send OK back.
			    break;

			case 'I': // UID of MCU
			{
				Uart_sendBytes((uint8_t *)id_array, 8);
				Uart_sendBytes("\r", 1);// Send OK back.
				break;
			}
			case 'P': //get board id
            {
                uint32_t board_id;
                EEPROMRead(&board_id, 2044, 4);
                Uart_sendBytes((uint8_t *)&board_id, 4);
                Uart_sendBytes("\r", 1);// Send OK back.
                break;
            }
			case 'V': //which version bootloader?
			{
				uint8_t tmp;
				Uart_getBytes(&tmp, 1);
				if(tmp == '1'){
					Uart_sendBytes((uint8_t *)&bootloader_ver, 4);
					Uart_sendBytes("\r", 1);// Send OK back.
				}
				break;
			}
			case 'X': //exit from boatloder
				Bootloader_eriseKeyInFlash();
				exit_flag = 1;
				Uart_sendBytes("\r", 1);
				delay(1000000);
			    break;
			case 'Q': //ask witch bootloader
				Uart_sendBytes("\r", 1); // Send OK back.
			    break;
		 }
	  }
	  SysCtlReset();
}

void Bootloader_Main(){



	if ((Bootloader_isApplicationExist() == 0) || Bootloader_checkKeyInFlash() == 1) {
	    Bootloader_InitAll();
	    Bootloader_FirmwareUpdate();
	} else {
	    Bootloader_GoToUserApp();
	}
}
