/*
 * bootloader_hal.c
 *
 *  Created on: 15 ����. 2017 �.
 *      Author: Gorudko
 */


#include "bootloader_hal.h"
//clock

void Clock_Init() {

    //20MHz
    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | QUARTZ_TYPE);
}

void delay(uint32_t x) {
    while(x--)
        asm(" NOP ");
}

//uart
void Uart_Init(){


    //Setting GPIO
    SysCtlPeripheralEnable(BOOTLOADER_UART_RX_SYSCTL);
    while(!SysCtlPeripheralReady(BOOTLOADER_UART_RX_SYSCTL));
    GPIOPinConfigure(BOOTLOADER_UART_RX_PINCONF);
    GPIOPinTypeUART(BOOTLOADER_UART_RX_PORT, BOOTLOADER_UART_RX_PIN);
    GPIOPadConfigSet(BOOTLOADER_UART_RX_PORT, BOOTLOADER_UART_RX_PIN, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD_WPU);

    SysCtlPeripheralEnable(BOOTLOADER_UART_TX_SYSCTL);
    while(!SysCtlPeripheralReady(BOOTLOADER_UART_TX_SYSCTL));
    GPIOPinConfigure(BOOTLOADER_UART_TX_PINCONF);
    GPIOPinTypeUART(BOOTLOADER_UART_TX_PORT, BOOTLOADER_UART_TX_PIN);

    //Setting UARTx
    SysCtlPeripheralEnable(BOOTLOADER_UART_SYSCTL);
    while(!SysCtlPeripheralReady(BOOTLOADER_UART_SYSCTL));
    UARTClockSourceSet(BOOTLOADER_UART, UART_CLOCK_SYSTEM);
    UARTConfigSetExpClk(BOOTLOADER_UART, MCU_SPEED, BOOTLOADER_UART_SPEED, (UART_CONFIG_PAR_NONE | UART_CONFIG_STOP_ONE | UART_CONFIG_WLEN_8));
    UARTFIFOEnable(BOOTLOADER_UART);
    //ON UART0
    UARTEnable(BOOTLOADER_UART);
}
void Uart_sendBytes(uint8_t * array, uint8_t len){
	Rs485DirPin_Driver();
	delay(50);
	uint8_t i;
	for(i = 0; i < len; i++){
	    UARTCharPut(BOOTLOADER_UART, *array);
	    array++;
	}
	while(UARTBusy(BOOTLOADER_UART));
	Rs485DirPin_Receiver();
}


void Uart_getBytes(uint8_t *array, uint8_t len){
    uint8_t i;
    for(i = 0; i < len; i++){
        *array = UARTCharGet(BOOTLOADER_UART);
        array++;
    }
}
void Uart_getByte(uint8_t * data){
    *data = UARTCharGet(BOOTLOADER_UART);
}

void Rs485DirPin_Init(){
    SysCtlPeripheralEnable(RS485_DIR_PIN_SYSCTL);
    while(!SysCtlPeripheralReady(RS485_DIR_PIN_SYSCTL));
    GPIOPinTypeGPIOOutput(RS485_DIR_PIN_PORT, RS485_DIR_PIN);
    Rs485DirPin_Receiver();
}

void Rs485DirPin_Receiver()      {
    GPIOPinWrite(RS485_DIR_PIN_PORT, RS485_DIR_PIN, 0);
}

void Rs485DirPin_Driver(){
    GPIOPinWrite(RS485_DIR_PIN_PORT, RS485_DIR_PIN, 0xFF);
}



void EEPROM_Init(){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0));
    while(EEPROMInit() != EEPROM_INIT_OK) {};
}

void Bootloader_InitAll(){
	Clock_Init();
	Rs485DirPin_Init();
	Uart_Init();
	Flash_init();
    EEPROM_Init();
}
