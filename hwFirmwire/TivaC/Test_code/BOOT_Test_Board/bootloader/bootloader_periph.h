/*
 * bootloader_periph.h
 *
 *  Created on: 3 ���. 2020 �.
 *      Author: Gorudko
 */

#ifndef BOOTLOADER_BOOTLOADER_PERIPH_H_
#define BOOTLOADER_BOOTLOADER_PERIPH_H_

//----------------------------------rs485 direct pin----------------------------------------------------

#define RS485_DIR_PIN_SYSCTL                                SYSCTL_PERIPH_GPIOD
#define RS485_DIR_PIN_PORT                                  GPIO_PORTD_BASE
#define RS485_DIR_PIN                                       GPIO_PIN_5

//----------------------------------uart via halfduplex rs485---------------------------------------------
#define BOOTLOADER_UART_SYSCTL                              SYSCTL_PERIPH_UART1
#define BOOTLOADER_UART                                     UART1_BASE
#define BOOTLOADER_UART_SPEED                               38400

#define BOOTLOADER_UART_RX_SYSCTL                           SYSCTL_PERIPH_GPIOB
#define BOOTLOADER_UART_RX_PORT                             GPIO_PORTB_BASE
#define BOOTLOADER_UART_RX_PIN                              GPIO_PIN_0
#define BOOTLOADER_UART_RX_PINCONF                          GPIO_PB0_U1RX

#define BOOTLOADER_UART_TX_SYSCTL                           SYSCTL_PERIPH_GPIOB
#define BOOTLOADER_UART_TX_PORT                             GPIO_PORTB_BASE
#define BOOTLOADER_UART_TX_PIN                              GPIO_PIN_1
#define BOOTLOADER_UART_TX_PINCONF                          GPIO_PB1_U1TX

//-------------------CLOCK-------------------------
//#define QUARTZ_TYPE                                         SYSCTL_XTAL_16MHZ
#define QUARTZ_TYPE                                         SYSCTL_XTAL_25MHZ

#endif /* BOOTLOADER_BOOTLOADER_PERIPH_H_ */
