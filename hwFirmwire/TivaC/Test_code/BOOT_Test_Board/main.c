#include "bootloader/bootloader.h"

#pragma DATA_SECTION(_app_first_addr, ".app_first_addr")

static volatile const uint32_t _app_first_addr = 0xFFFFFFFF;

int main(void)
{
    volatile uint32_t tmp = _app_first_addr;
    Bootloader_Main();
    return 0;
}

