

#ifndef __RS485_H__
#define __RS485_H__

#include "stdint.h"
#include "BaseLib/ring_buffer.h"


int32_t RS485_initFull(uint8_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
int32_t RS485_initHalf(uint8_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
void RS485_setCallbeack(    void (*fCallbackRx)(uint8_t* data, int16_t length),
                            void (*fCallbackEndTx)(void),
                            void (*fCallbackEndRx)(void));
int32_t RS485_setBaudrate(uint32_t);
int32_t RS485_dataTx(uint8_t*, int16_t);
int32_t RS485_getLBufferLen(int32_t*);
int32_t RS485_getBuffer(uint8_t*, int32_t);
int32_t RS485_getRingBuffer(ringBuffer_s**);
uint8_t RS485_busy();

#endif
