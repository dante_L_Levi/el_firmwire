

#include <stdio.h>
#include "SProtocol_connect.h"
//#include "_BaseProject/bootloader.h"
#include "SProtocol.h"

enum _sprotocol_state{
    NO_INIT,
    ALL_MESAGE,
    CRITICAL_ONLY
}sprotocol_state;

static SP_command_function cmd_functions[SPROTOCOL_COMMANDS_COUNT];
static bool lock_commands[SPROTOCOL_COMMANDS_COUNT];

static void SP_runFunctionByMsg(SProtocol_message* item);
static void SP_nulFunction(SProtocol_message* item);

void SProtocol_init(uint16_t id,
             int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length))
{
    SProtocol_initBuffers(id, transmitFunctionSet);
    int16_t i=0;
    for(i=0;i<SPROTOCOL_COMMANDS_COUNT;i++){
        lock_commands[i]=false;
    }
    SProtocol_resetAllFunctions();
    sprotocol_state=ALL_MESAGE;
}

#define GO_TO_BOOTLOADER_RS485_KEY      0xf2784c28
bool SProtocol_parseCriticalPack(SProtocol_message* msg)
{
    if(sprotocol_state==NO_INIT)
        return false;
    switch(msg->command){
    case SPROTOCOL_COMMAND_GO_BOOT:
        if(msg->length==4){
            uint32_t * tmp = (uint32_t *) msg->data;
            if (*tmp == GO_TO_BOOTLOADER_RS485_KEY)
            {
                //boot
            }
        }
        return true;
    case SPROTOCOL_COMMAND_ENABLE:
        if(msg->length==1){
            switch(msg->data[0]){
            case 0:
                sprotocol_state=CRITICAL_ONLY;
                break;
            case 1:
                sprotocol_state=ALL_MESAGE;
                SProtocol_resetInput();
                break;
            default:
                break;
            }
        }
        return true;
    }
    return false;
}

void SProtocol_analysis()
{
    if((sprotocol_state==NO_INIT)||(sprotocol_state==CRITICAL_ONLY))
        return;
    while(SProtocol_itemAvailable()){
        SProtocol_message* item=SProtocol_getNextItem();
        SProtocol_parseCriticalPack(item);
        SP_runFunctionByMsg(item);
    }
}

void SProtocol_setFunction(sprotocol_commands command, SP_command_function p_func)
{
    uint8_t cmd=(uint8_t)command;
    if(cmd>=SPROTOCOL_COMMANDS_COUNT)
        return;
    cmd_functions[cmd]=p_func;
}

void SProtocol_lockCommand(sprotocol_commands command, bool lock)
{
    lock_commands[command]=lock;
}

void SProtocol_resetFunction(sprotocol_commands cmd)
{
    SProtocol_setFunction(cmd, &SP_nulFunction);
}

void SProtocol_resetAllFunctions()
{
    int16_t cmd=0;
    for(cmd=0;cmd<SPROTOCOL_COMMANDS_COUNT;cmd++){
        if(!lock_commands[cmd])
            SProtocol_setFunction((sprotocol_commands)cmd, &SP_nulFunction);
    }
}

int8_t SP_getStateTransmit()
{
    if(sprotocol_state==ALL_MESAGE)
        return 1;
    else
        return  0;
}

void SProtocol_startAnalysis(bool start)
{

    if((sprotocol_state==ALL_MESAGE)&&(!start))
        sprotocol_state=CRITICAL_ONLY;
    if((sprotocol_state==CRITICAL_ONLY)&&(start))
        sprotocol_state=ALL_MESAGE;
}

static void SP_nulFunction(SProtocol_message* item)
{
}

static void SP_runFunctionByMsg(SProtocol_message* item)
{
    if(item->command>=SPROTOCOL_COMMANDS_COUNT)
            return;
    if(cmd_functions[item->command]!=NULL)
        cmd_functions[item->command](item);
}


void SProtocol_addTxMessage(SProtocol_message *p)
{
    if(SP_getStateTransmit())
        SProtocol_addTxMessageNoBlock(p);
}
