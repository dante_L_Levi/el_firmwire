/*
 * Logic.h
 *
 *  Created on: 3 ����. 2021 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_H_
#define LOGIC_H_


#include "headers.h"
#include "settings.h"
#include "sys_setup.h"
#include "_RS485/rs485.h"
#include "_SProtocol/SProtocol.h"
#include "_SProtocol/SProtocol_connect.h"
#include "_SProtocol/SProtocol_struct.h"


#define COUNT_AIN               10

typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;

#pragma pack(push, 1)
typedef struct
{
   float AIN[COUNT_AIN];
}analog_value_state_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union {
    struct{
        uint8_t reserved   :1;
        uint8_t ON_CNT     :1;
        uint8_t TP9        :1;
        uint8_t Ready      :1;
        uint8_t Srez_CNT   :1;
        uint8_t OTD        :1;
        uint8_t PPS_inner  :1;
        uint8_t Raz_DLU    :1;
    }fields;
    uint8_t data;
}digital_pins_in_sys_s;
#pragma pack(pop)


#pragma pack(push, 1)

typedef struct{
    int16_t dataAIN[COUNT_AIN];
    uint8_t dataDIN;
}transmitData_s;
#pragma pack(pop)

typedef struct
{
    mode_system_e                   mode;
    digital_pins_in_sys_s           inputs;
    analog_value_state_s            ainputs;
    transmitData_s                  txData;
    uint32_t                        sysTime_ms;
    uint8_t                         sync_trigger;
    uint8_t                         flag_getData;
    uint32_t                        rs485_echo_count;


}state_system_s;


void Main_Init(void);


uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);

void Status_Work_Update(void);
/*****************Async Update RS485***************/
void RS485_Async_Update(void);

#endif /* LOGIC_H_ */
