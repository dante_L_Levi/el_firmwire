

#ifndef LIBS_LINE_BUFFER_H_
#define LIBS_LINE_BUFFER_H_

#include <string.h>

typedef struct {
    uint8_t* data;
    int16_t len;
    int16_t head;   //next data push
}lineBuffer_s;

inline void lineBuffer_init(lineBuffer_s* line, uint8_t* data, int16_t length)
{
    if(length<1){
        line->head=0;
        line->len=1;
        line->data=data;
    }
    else{
        line->head=0;
        line->len=length;
        line->data=data;
    }
}

inline bool lineBuffer_isFull(lineBuffer_s* line)
{
    return line->head==line->len;
}

inline bool lineBuffer_isEmpty(lineBuffer_s* line)
{
    return line->head==0;
}

inline int16_t lineBuffer_count(lineBuffer_s* line)
{
    return line->head;
}

inline int16_t lineBuffer_length(lineBuffer_s* line)
{
    return line->len;
}

inline int16_t lineBuffer_space(lineBuffer_s* line)
{
    return line->len-line->head;
}

inline void lineBuffer_push(lineBuffer_s* line, uint8_t byte)
{
    if(lineBuffer_isFull(line))
        return;
    line->data[line->head]=byte;
    line->head++;
}

inline void lineBuffer_pushBuff(lineBuffer_s* line, uint8_t* buff, int16_t length)
{
    if(length>lineBuffer_space(line))
        length=lineBuffer_space(line);
    memcpy(line->data+line->head,buff,length);
    line->head+=length;
}


//inline uint8_t lineBuffer_pull(lineBuffer_s* line)
//{
//    uint8_t data=0;
//    return data;
//}
//
//inline void lineBuffer_pop(lineBuffer_s* line)
//{
//}

inline void lineBuffer_flush(lineBuffer_s* line)
{
    line->head=0;
}

#endif /* LIBS_LINE_BUFFER_H_ */
