/*
 * settings.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Kuntsevich
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <stdint.h>
#include <stdbool.h>
#include "_pinmux/pinout.h"

#define SYS_CLOCK               80000000

#pragma DATA_SECTION(_board_id, ".board_id")
static volatile const uint8_t _board_id [4] = {0x01,0xF6,'T','P'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;


#endif /* SETTINGS_H_ */
