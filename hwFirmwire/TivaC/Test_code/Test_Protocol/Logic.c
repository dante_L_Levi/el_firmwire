/*
 * Logic.c
 *
 *  Created on: 3 ����. 2021 �.
 *      Author: Lepatenko
 */

#include  "Logic.h"


state_system_s state_main;

#define PORT_RGB    GPIO_PORTF_BASE
#define PIN_R       GPIO_PIN_1
#define PIN_G       GPIO_PIN_2
#define PIN_B       GPIO_PIN_3

#define LED_R_ON()          GPIOPinWrite(PORT_RGB,PIN_R,0x02)
#define LED_R_OFF()         GPIOPinWrite(PORT_RGB,PIN_R,0x00)

#define LED_G_ON()          GPIOPinWrite(PORT_RGB,PIN_G,0x04)
#define LED_G_OFF()         GPIOPinWrite(PORT_RGB,PIN_G,0x00)


#define LED_B_ON()          GPIOPinWrite(PORT_RGB,PIN_B,0x08)
#define LED_B_OFF()         GPIOPinWrite(PORT_RGB,PIN_B,0x00)

#define FREQ_SYNC           1000

uint32_t CLOCK_SYS;
uint8_t LED_Tim_Indicate=10;//10Hz
uint8_t count_Led=0;

static void Set_Mode_Work(SProtocol_message* msg);
static mode_system_e mode_workInit();
static void Tim_Init(void);
/*****************Function Indication work*******************/
static void Indication_Led(uint8_t dt)
{
    switch(dt)
    {
        case 0:
        {
            LED_R_ON();
            LED_G_OFF();
            LED_B_OFF();
            break;
        }
        case 1:
        {
            LED_G_ON();
            LED_R_OFF();
            LED_B_OFF();
            break;
        }
        case 2:
        {
            LED_B_ON();
            LED_R_OFF();
            LED_G_OFF();
            break;
        }
        default:
               {
                   LED_R_OFF();
                   LED_G_OFF();
                   LED_B_OFF();
                   break;
               }

    }
}

static void spEchoFunction(SProtocol_message* msg)
{
    RS485_dataTx(msg->data, msg->length);

}

static void spGetInputs(SProtocol_message* msg)
{
    state_main.flag_getData=1;
}


static mode_system_e mode_workInit()
{

    Tim_Init();
    SProtocol_resetAllFunctions();
    SProtocol_setFunction(SP_CMD_ECHO_PAYLOAD,&spEchoFunction);
    SProtocol_setFunction(SP_CMD_GET_INPUTS,&spGetInputs);

    return MODE_WORK;
}


static void Led_Indicate_Tim(void)
{
    TimerIntClear(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
           if(count_Led>3)
           {
               count_Led=0;
           }
           Indication_Led(count_Led);
           count_Led++;
}

static void IRQ_SYNC_Tim(void)
{
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    state_main.sync_trigger=1;
    state_main.sysTime_ms++;
}


/********************timer update Data struct Motor & Led Status*****************/
static void Tim_Init(void)
{
    //Timer1 - 10Hz
           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER3))
           {
           }
           TimerClockSourceSet(TIMER3_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER3_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER3_BASE, TIMER_A, CLOCK_SYS/LED_Tim_Indicate);
           TimerIntRegister(TIMER3_BASE, TIMER_A, Led_Indicate_Tim);
           TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER3_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER3A);
}

/******************Init default IO**************************/
static void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_R);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_G);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_B);
    GPIOPinWrite(PORT_RGB,PIN_R, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_G, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_B, 0x00);

}

static void Init_Sync_Tim(void)
{
    //Timer1 - 1000Hz
     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
     while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
     {
     }
     TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
      TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
      TimerLoadSet(TIMER1_BASE, TIMER_A, CLOCK_SYS/FREQ_SYNC);
      TimerIntRegister(TIMER1_BASE, TIMER_A, IRQ_SYNC_Tim);
      TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
      TimerEnable(TIMER1_BASE, TIMER_A);
      //NVIC
      IntEnable(INT_TIMER1A);
}




static void Set_Mode_Work(SProtocol_message* msg)
{
    state_main.mode=(mode_system_e)msg->data[0];
}

static void Update_Fast_Loop(void)
{
    state_main.ainputs.AIN[0]=state_main.sysTime_ms*0.01f;
    state_main.ainputs.AIN[1]=state_main.sysTime_ms*0.015f;
    state_main.ainputs.AIN[2]=state_main.sysTime_ms*0.035f;
    state_main.ainputs.AIN[3]=state_main.sysTime_ms*0.04f;
    state_main.ainputs.AIN[4]=state_main.sysTime_ms*0.022f;
    state_main.ainputs.AIN[5]=state_main.sysTime_ms*0.077f;
    state_main.ainputs.AIN[6]=state_main.sysTime_ms*0.0123f;
    state_main.ainputs.AIN[7]=state_main.sysTime_ms*0.02f;
    state_main.ainputs.AIN[8]=state_main.sysTime_ms*0.03f;
    state_main.ainputs.AIN[9]=state_main.sysTime_ms*0.06f;


    state_main.inputs.data=rand()%255;

}

void Main_Init(void)
{
    CLOCK_SYS=Sys_freq();
    GPIO_Init();
    RS485_initHalf(0, 115200, GPIO_PORTD_BASE, GPIO_PIN_6, 0, 0);
    SProtocol_init(*rs485_id, RS485_dataTx);
    RS485_setCallbeack(SProtocol_parserRX, 0, SProtocol_endRX);
    SProtocol_setFunction(SP_CMD_SET_MODE, &Set_Mode_Work);
    Init_Sync_Tim();
}


uint8_t Get_Status_Trigger(void)
{
    return state_main.sync_trigger;
}
/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
    state_main.sync_trigger=0;
}

/*****************Step Work***************/
void Status_Work_Update(void)
{
    switch(state_main.mode)
    {
    case MODE_WORK_INIT:
    {
        state_main.mode=mode_workInit();
        break;
    }
    case MODE_WORK:
    {
        Update_Fast_Loop();
        break;
    }

    }
}

/*****************Async Update RS485***************/
void RS485_Async_Update(void)
{
    if(state_main.flag_getData)
    {
        state_main.flag_getData=0;
        int8_t i=0;
        for(i=0;i<COUNT_AIN;i++)
        {
            state_main.txData.dataAIN[i]=(int16_t)(state_main.ainputs.AIN[i]*1000);
        }

        state_main.txData.dataDIN=state_main.inputs.data;

        SProtocol_message mess;
        mess.command=SP_CMD_SEND_INPUTS;
        mess.data=(uint8_t*)&state_main.txData;
        mess.length=sizeof(transmitData_s);
        SProtocol_addTxMessage(&mess);

    }
}



