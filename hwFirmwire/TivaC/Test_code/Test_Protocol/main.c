/*
 * main.c
 *
 *  Created on: 2 ����. 2021 �.
 *      Author: Lepatenko
 */

#include "headers.h"
#include "settings.h"
#include "sys_setup.h"
#include "_RS485/rs485.h"
#include "_SProtocol/SProtocol.h"
#include "_SProtocol/SProtocol_connect.h"
#include "_SProtocol/SProtocol_struct.h"
#include "Logic.h"




void main(void)
{
    Sys_Setup();
    Main_Init();
    IntMasterEnable();

    while(1)
    {
        RS485_Async_Update();
        if(Get_Status_Trigger()==0)
               {
                  continue;
               }
               Reset_Trigger();
               Status_Work_Update();
    }
}






