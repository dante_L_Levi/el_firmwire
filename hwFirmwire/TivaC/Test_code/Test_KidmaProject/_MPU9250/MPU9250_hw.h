/*
 * MPU9250_hw.h
 *
 *  Created on: 11 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef MPU9250_MPU9250_HW_H_
#define MPU9250_MPU9250_HW_H_


//to do resister map
#define MPU9250_CONFIG                          0x1A
#define MPU9250_WHO_AM_I                        0x75//
#define MPU9250_PWR_MGMT_1                      0x6B//
#define MPU9250_PWR_MGMT_2                      0x6C
#define MPU9250_ACCEL_CONFIG                    0x1C//
#define MPU9250_GYRO_CONFIG                     0x1B//
#define MPU9250_SMPDIV                          0x19//


#define MPU9250_ACCEL_XOUT_H                    0x3B//ACCEL_XOUT_H
#define MPU9250_GYRO_XOUT_H                     0x43//GYRO_XOUT_H

#define MPU9250_USER_CTRL                       0x6A
#define MPU9250_I2C_MST_CTRL                    0x24
#define MPU9250_I2C_MST_DELAY_CTRL              0x67

#define MPU9250_I2C_SLV0_ADDR                   0x25
#define MPU9250_I2C_SLV0_REG                    0x26
#define MPU9250_I2C_SLV0_CTRL                   0x27
#define MPU9250_I2C_SLV0_DO                     0x63

#define MPU9250_INT_PIN_CFG                     0x37

#define MPU9250_EXT_SENS_DATA_00                0x49
#define MPU9250_ID                              0x71
#define MPU9250_ADRESS                          0xFF



#define CLOCK_SEL_PLL                           0x01
#define SEN_ENABLE                              0x00
#define DLPF_184Hz                              0x01
#define DLPF_92Hz                               0x02
#define DLPF_41Hz                               0x03
#define DLPF_20Hz                               0x04
#define DLPF_10Hz                               0x05
#define DLPF_5Hz                                0x06


#endif /* MPU9250_MPU9250_HW_H_ */
