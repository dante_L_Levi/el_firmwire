/*
 * MPU9250.c
 *
 *  Created on: 11 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "MPU9250.h"
#include "MPU9250_hw.h"


const float Sensivity_Accel_Mpu9250[4]=
{
 16384,8192,4096,2048
};

const float Sensivity_Gyro_Mpu9250[4]=
{
 131,65.5,32.8,16.4
};



typedef struct
{
        uint32_t number_ssi;
        void (*RecieveFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len);
        void (*TransmitFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len);
        void (*InitFunction)(uint32_t ssi_num);
        uint8_t cmdbuf[15];
        float sensivity[3];
        union
              {
                  struct
                  {
                      int16_t AX;
                      int16_t AY;
                      int16_t AZ;
                  };
                  int16_t Accel_Data_hw[3];
              }field_Accel_Data;

              union
                     {
                         struct
                         {
                             int16_t GX;
                             int16_t GY;
                             int16_t GZ;
                         };
                         int16_t Gyro_Data_hw[3];
                     }field_Gyro_Data;


}mpu9250_config;


mpu9250_config _mainData_mpu9250;


static uint8_t check_id(void)
{
    uint8_t rslt;
    _mainData_mpu9250.RecieveFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_WHO_AM_I|0x80,&rslt,1);
    return rslt;
}


static void mpu9250_Hw_Init(MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale)
{
    /* MPU9250_Reset ----------------------------------------------------------*/
        uint8_t dataTemp=0x00;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_PWR_MGMT_1,&dataTemp,1);

   /* MPU9250_select clock source to gyro ----------------------------------------------------------*/
        dataTemp=CLOCK_SEL_PLL;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_PWR_MGMT_1,&dataTemp,1);

   /* enable accelerometer and gyro    ----------------------------------------------------*/
        dataTemp=SEN_ENABLE;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_PWR_MGMT_2,&dataTemp,1);
        // setting bandwidth to 184Hz as default
        dataTemp=DLPF_184Hz;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_ACCEL_CONFIG,&dataTemp,1);

        // setting gyro bandwidth to 184Hz
        dataTemp=DLPF_184Hz;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_CONFIG,&dataTemp,1);

        // setting the sample rate divider to 0 as default
        dataTemp=0x00;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_SMPDIV,&dataTemp,1);

        /* MPU9250_Set_Accel_Scale ---------------------------------------------------------*/
        uint8_t adress_Reg = MPU9250_ACCEL_CONFIG|0x80;
        dataTemp=0x00;
        _mainData_mpu9250.RecieveFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,adress_Reg,&dataTemp,1);

        dataTemp|=(uint8_t)accel_scale;
        _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_ACCEL_CONFIG,&dataTemp,1);


        /* MPU9250_GYRO_CONFIG ---------------------------------------------------------*/
        dataTemp=0x00;
        adress_Reg=0x00;
       adress_Reg = MPU9250_GYRO_CONFIG|0x80;
       _mainData_mpu9250.RecieveFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,adress_Reg,&dataTemp,1);


       dataTemp|=(uint8_t)gyro_scale;
       _mainData_mpu9250.TransmitFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,MPU9250_GYRO_CONFIG,&dataTemp,1);
}

static void Select_Sensivity(MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale)
{
    switch(accel_scale)
    {
        case ACCEL_SCALE_2G:
        {
            _mainData_mpu9250.sensivity[0]=(float)Sensivity_Accel_Mpu9250[sensivity_ACCEL_SCALE_2G];
            break;
        }
        case ACCEL_SCALE_4G:
        {
            _mainData_mpu9250.sensivity[0]=(float)Sensivity_Accel_Mpu9250[sensivity_ACCEL_SCALE_4G];
            break;
        }
        case ACCEL_SCALE_8G:
        {
            _mainData_mpu9250.sensivity[0]=(float)Sensivity_Accel_Mpu9250[sensivity_ACCEL_SCALE_8G];
            break;
        }
        case ACCEL_SCALE_16G:
        {
            _mainData_mpu9250.sensivity[0]=(float)Sensivity_Accel_Mpu9250[sensivity_ACCEL_SCALE_16G];
            break;
        }

        default:
            _mainData_mpu9250.sensivity[0]=(float)1.0f;
            break;
    }

    switch(gyro_scale)
    {
        case GYRO_SCALE_250dps:
        {
            _mainData_mpu9250.sensivity[1]=(float)Sensivity_Gyro_Mpu9250[sensivity_GYRO_SCALE_250dps];
            break;
        }
        case GYRO_SCALE_500dps:
        {
            _mainData_mpu9250.sensivity[1]=(float)Sensivity_Gyro_Mpu9250[sensivity_GYRO_SCALE_500dps];
            break;
        }
        case GYRO_SCALE_1000dps:
        {
            _mainData_mpu9250.sensivity[1]=(float)Sensivity_Gyro_Mpu9250[sensivity_GYRO_SCALE_1000dps];
            break;
        }
        case GYRO_SCALE_2000dps:
        {
            _mainData_mpu9250.sensivity[1]=(float)Sensivity_Gyro_Mpu9250[sensivity_GYRO_SCALE_2000dps];
            break;
        }


    }
}


void MPU9250_Set_Init(void (*InitFunction)(uint32_t ssi_num),
                      void (*RecieveFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len),
                      void (*TransmitFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len))
{
    _mainData_mpu9250.InitFunction=InitFunction;
    _mainData_mpu9250.RecieveFunction=RecieveFunction;
    _mainData_mpu9250.TransmitFunction=TransmitFunction;


}




/************************************Init MPU9250*****************************************/
void MPU9250_Init(uint32_t ssi_num,MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale)
{
    _mainData_mpu9250.number_ssi=ssi_num;
    _mainData_mpu9250.InitFunction(_mainData_mpu9250.number_ssi);
    if(check_id()==MPU9250_ID)
    {
        mpu9250_Hw_Init(accel_scale,gyro_scale);
        Select_Sensivity(accel_scale,gyro_scale);
    }

}
/**********************************Read Data MPU9250*************************************/
void MPU9250_Read_All(int16_t *dataGyro,int16_t *dataAccel,int16_t *Temp)
{



        uint8_t adress_Reg=0x00;
        adress_Reg=MPU9250_ACCEL_XOUT_H|0x80;
        _mainData_mpu9250.RecieveFunction(_mainData_mpu9250.number_ssi,MPU9250_ADRESS,adress_Reg,_mainData_mpu9250.cmdbuf,14);


       for(int i=0;i<3;i++)
        {
            _mainData_mpu9250.field_Accel_Data.Accel_Data_hw[i] =((int16_t)((int16_t)_mainData_mpu9250.cmdbuf[2*i] << 8) | _mainData_mpu9250.cmdbuf[2*i+1]);
            _mainData_mpu9250.field_Gyro_Data.Gyro_Data_hw[i] = ((int16_t)((int16_t)_mainData_mpu9250.cmdbuf[2*i+6] << 8) | _mainData_mpu9250.cmdbuf[2*i+7]);

            dataGyro[i]=_mainData_mpu9250.field_Gyro_Data.Gyro_Data_hw[i];
            dataAccel[i]=_mainData_mpu9250.field_Accel_Data.Accel_Data_hw[i];


        }




       // _mainData_mpu9250.Temp = ((uint16_t)cmdbuf[7] << 8) | cmdbuf[8];
       // *Temp = ((_mainData_mpu9250.Temp/333.87) + 21.0)*1000;

}
/**********************************Read Data MPU9250 Real********************************/
void MPU9250_Read_All_Real(float *dataGyro,float *dataAccel)
{
    for(int i=0;i<3;i++)
    {
        dataGyro[i]=((float)_mainData_mpu9250.field_Gyro_Data.Gyro_Data_hw[i]/_mainData_mpu9250.sensivity[1])*1000.0f;
        dataAccel[i]=((float)_mainData_mpu9250.field_Accel_Data.Accel_Data_hw[i]/_mainData_mpu9250.sensivity[0])*1000.0f;
    }


}

