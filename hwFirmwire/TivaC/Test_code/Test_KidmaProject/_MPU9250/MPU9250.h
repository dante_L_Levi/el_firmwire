/*
 * MPU9250.h
 *
 *  Created on: 11 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef MPU9250_MPU9250_H_
#define MPU9250_MPU9250_H_

#include "headers.h"
#include "settings.h"

typedef enum {
    ACCEL_SCALE_2G  = 0x00,
    ACCEL_SCALE_4G  = 0x08,
    ACCEL_SCALE_8G  = 0x10,
    ACCEL_SCALE_16G = 0x18
} MPU9250_ACCEL_SCALE_t;

typedef enum {
    GYRO_SCALE_250dps  = 0x00,
    GYRO_SCALE_500dps  = 0x08,
    GYRO_SCALE_1000dps = 0x10,
    GYRO_SCALE_2000dps = 0x18
} MPU9250_GYRO_SCALE_t;


typedef enum
{
    sensivity_GYRO_SCALE_250dps  = 0,
    sensivity_GYRO_SCALE_500dps  = 1,
    sensivity_GYRO_SCALE_1000dps = 2,
    sensivity_GYRO_SCALE_2000dps = 3

}sensivity_mpu6050_gyro;

typedef enum
{
    sensivity_ACCEL_SCALE_2G    = 0,
    sensivity_ACCEL_SCALE_4G    = 1,
    sensivity_ACCEL_SCALE_8G    = 2,
    sensivity_ACCEL_SCALE_16G   = 3

}sensivity_mpu6050_accel;






void MPU9250_Set_Init(void (*InitFunction)(uint32_t ssi_num),
                      void (*RecieveFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len),
                      void (*TransmitFunction)(uint32_t ssi_num,uint8_t DeviceAddr,uint8_t Register, uint8_t *data,uint16_t len));

/************************************Init MPU9250*****************************************/
void MPU9250_Init(uint32_t ssi_num,MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale);
/**********************************Read Data MPU9250*************************************/
void MPU9250_Read_All(int16_t *dataGyro,int16_t *dataAccel,int16_t *Temp);
/**********************************Read Data MPU9250 Real********************************/
void MPU9250_Read_All_Real(float *dataGyro,float *dataAccel);


#endif /* MPU9250_MPU9250_H_ */
