/*
 * adg5208.c
 *
 *  Created on: 4 mar. 2020 �.
 *      Author: peresaliak
 */

#include <_ADG520x/adg520x.h>
#include <_ADG520x/adg520x_setting.h>
#include "headers.h"
#include "_Analog_Pins/analog_pins.h"


float channel_data[ADG520x_COUNT_CHANNELS];
float returnData[ADG520x_COUNT_CHANNELS];
static uint8_t addr;
static bool flagUpdate;

static void ADG520x_switch(uint8_t addr);

void ADG520x_init()
{
    for(uint8_t i=0;i<ADG520x_COUNT_CHANNELS;i++)
        channel_data[i]=0;

    addr=0;
    flagUpdate=false;
    SysCtlPeripheralEnable(ADG520x_ADDR_PERIPH);
    while (!SysCtlPeripheralReady(ADG520x_ADDR_PERIPH));

    GPIOPinTypeGPIOOutput(ADG520x_ADDR_PORT, ADG520x_PIN_EN|ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2);
    GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_EN, ADG520x_PIN_EN);
    ADG520x_switch(addr);
}

float* ADG520x_getCopyData()
{
    for(int8_t i=0;(i<10)&&flagUpdate;i++){
        flagUpdate=false;
        memcpy(returnData,channel_data,sizeof(channel_data));
    }
    return returnData;
}

void ADG520x_adc0Update(void)
{
    int8_t adc_id=0;
    uint8_t addrNext;
        addrNext=(addr+1)&7;
    ADG520x_switch(addrNext);
    float *adc_v = ADC_copy_data_f(adc_id);
    for(uint8_t i=0;i<ADG520x_COUNT_MUX_CHANNELS;i++){
        channel_ADG520x_mux* mux=&(channels_mux[i]);
        if(mux->adc_id==adc_id){
            uint8_t num=mux->numS[addr&mux->mask];
            channel_data[num]=adc_v[mux->adc_ch];
        }
    }
    addr=addrNext;
    flagUpdate=true;
}

int32_t ADG520x_setMulti(uint8_t num, float value)
{
    int16_t s_ch=-1;
    for(uint8_t n_adc=0;(n_adc<ADG520x_COUNT_MUX_CHANNELS)&&(s_ch<0);n_adc++){
        channel_ADG520x_mux* mux=&(channels_mux[n_adc]);
        for(uint8_t i=0;i<=mux->mask;i++){
            if(mux->numS[i]==num){
                s_ch=n_adc;
                break;
            }
        }
    }
    if(s_ch<0)
        return 1;
    ADC_setMulti(0, s_ch, value);
    return 0;
}

int32_t ADG520x_setOffset(uint8_t num, float value)
{
    int16_t s_ch=-1;
    for(uint8_t n_adc=0;(n_adc<ADG520x_COUNT_MUX_CHANNELS)&&(s_ch<0);n_adc++){
        channel_ADG520x_mux* mux=&(channels_mux[n_adc]);
        for(uint8_t i=0;i<=mux->mask;i++){
            if(mux->numS[i]==num){
                s_ch=n_adc;
                break;
            }
        }
    }
    if(s_ch<0)
        return 1;
    ADC_setOffset(0, s_ch, value);
    return 0;
}

static void ADG520x_switch(uint8_t addr)
{
    addr&=ADG5208_ADDR_MASK;
    switch(addr){
    case 0:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     0);
        break;
    case 1:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A0);
        break;
    case 2:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A1);
        break;
    case 3:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A0|ADG520x_PIN_A1);
        break;
    case 4:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A2);
        break;
    case 5:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A0|ADG520x_PIN_A2);
        break;
    case 6:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A1|ADG520x_PIN_A2);
        break;
    case 7:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2);
        break;
    default:
        GPIOPinWrite(ADG520x_ADDR_PORT, ADG520x_PIN_A0|ADG520x_PIN_A1|ADG520x_PIN_A2,
                     0);
    }
}

