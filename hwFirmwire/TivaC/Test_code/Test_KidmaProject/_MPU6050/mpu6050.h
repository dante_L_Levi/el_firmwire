/*
 * mpu6050.h
 *
 *  Created on: 10 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef MPU6050_MPU6050_H_
#define MPU6050_MPU6050_H_

#include "headers.h"
#include "settings.h"
#include <stdint.h>
#include <stdio.h>


typedef enum
{

    Accel_2g_mask=0x00,
    Accel_4g_mask=0x08,
    Accel_8g_mask=0x10,
    Accel_16g_mask=0x18


}Accel_Config;




typedef enum
{

    Gyro_250dps_mask=0x00,
    Gyro_500dps_mask=0x08,
    Gyro_1000dps_mask=0x10,
    Gyro_2000dps_mask=0x18

}Gyro_Config;

typedef enum
{
    Rate_1kHz=7,
    Rate_2kHz=3,
    Rate_4kHz=1,
    Rate_8kHz=0


}Sample_Rate;

typedef enum
{
    Sensivity_gyro_250dps=131000,
    Sensivity_gyro_500dps=65500,
    Sensivity_gyro_1000dps=32800,
    Sensivity_gyro_2000dps=16400


}Sensivity_mpu6050_gyro;


typedef enum
{
    Sensivity_accel_2000mg=16384,
    Sensivity_accel_4000mg=8192,
    Sensivity_accel_8000mg=4096,
    Sensivity_accel_16000mg=2048


}Sensivity_mpu6050_accel;


void mpu6050_Init(uint8_t number_i2c,Sample_Rate _rate,Gyro_Config _g_conf,Accel_Config _a_conf);
void mpu6050_GetData_hw(int32_t* Gyro,int32_t* Accel,int32_t* Temp);
void mpu6050_GetData_real(int32_t* Gyro,int32_t* Accel,int32_t* Temp);


#endif /* MPU6050_MPU6050_H_ */
