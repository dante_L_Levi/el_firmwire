/*
 * mpu6050.c
 *
 *  Created on: 10 ��� 2022 �.
 *      Author: Lepatenko
 */

#include "mpu6050.h"
#include "_MPU6050/mpu6050_hw.h"
#include "_I2C_Registers/i2c_registers.h"

typedef struct
{
    uint8_t number_i2c;
    uint8_t ID_DEV;
    uint8_t buffer[14];
    float sensivity[3];
    float Temp;
    I2C_REGISTERS_receive_package_s* pack;
    I2C_REGISTERS_receive_package_s* packs_reg[6];
    I2C_REGISTERS_receive_package_s* pack_out_mpu6050;
    union
       {
           struct
           {
               uint8_t REG_SMPLRT_DIV;
               uint8_t REG_PWR_MGMT_1;
               uint8_t REG_CONFIG;
               uint8_t REG_ACCEL_CONFIG;
               uint8_t REG_GYRO_CONFIG;
               uint8_t REG_INT_ENABLE;
           };
           uint8_t Accel_Data[6];
       }fields_Registers;


       union
       {
           struct
           {
               int32_t AX;
               int32_t AY;
               int32_t AZ;
           };
           int32_t Accel_Data_hw[3];
       }field_Accel_Data;

       union
              {
                  struct
                  {
                      int32_t GX;
                      int32_t GY;
                      int32_t GZ;
                  };
                  int32_t Gyro_Data_hw[3];
              }field_Gyro_Data;




}mpu6050_config;


mpu6050_config _config_mpu6050;

static void mpu6050_Register_Init(Sample_Rate _rate,Gyro_Config _g_conf,Accel_Config _a_conf);
static void mpu6050_setRegisters8(uint8_t reg_addr, uint8_t value);
static void mpu6050_readMemRegister(uint8_t reg_addr, uint8_t* data,uint8_t len,I2C_REGISTERS_receive_package_s** package);
static void mpu6050_readRegister8(uint8_t reg_addr, uint8_t* data, I2C_REGISTERS_receive_package_s** package);
static void Check_Id(void);
static void Read_register_mpu6050(void);
static void Select_Sensivity_Accel_mpu6050(Accel_Config range_accel);
static void Select_Sensivity_Gyro_mpu6050(Gyro_Config range_gyro);



static void mpu6050_setRegisters8(uint8_t reg_addr, uint8_t value)
{
    I2C_REGISTERS_set_registr8(_config_mpu6050.number_i2c, DEVICE_ID, reg_addr, value);
}

static void mpu6050_readMemRegister(uint8_t reg_addr, uint8_t* data,uint8_t len,I2C_REGISTERS_receive_package_s** package)
{
    *package=I2C_REGISTERS_read_mem(_config_mpu6050.number_i2c, DEVICE_ID,reg_addr,data,len);
}

static void mpu6050_readRegister8(uint8_t reg_addr, uint8_t* data, I2C_REGISTERS_receive_package_s** package)
{
    *package=I2C_REGISTERS_read_registr8(_config_mpu6050.number_i2c, DEVICE_ID, reg_addr, data);
}


static void Check_Id(void)
{

    mpu6050_readRegister8(WHO_AM_I,&_config_mpu6050.ID_DEV,&_config_mpu6050.pack);

}

static void Select_Sensivity_Accel_mpu6050(Accel_Config range_accel)
{


    switch(range_accel)
    {
        case Accel_2g_mask:
        {
            _config_mpu6050.sensivity[0]=(float)Sensivity_accel_2000mg;
            break;
        }

        case Accel_4g_mask:
        {
            _config_mpu6050.sensivity[0]=(float)Sensivity_accel_4000mg;
            break;
        }

        case Accel_8g_mask:
        {
            _config_mpu6050.sensivity[0]=(float)Sensivity_accel_8000mg;
            break;
        }

        case Accel_16g_mask:
        {
            _config_mpu6050.sensivity[0]=(float)Sensivity_accel_16000mg;
            break;
        }

        default:
            _config_mpu6050.sensivity[0]=1.0f;
            break;
    }

}

static void Select_Sensivity_Gyro_mpu6050(Gyro_Config range_gyro)
{
    switch(range_gyro)
        {
            case Gyro_250dps_mask:
            {
                _config_mpu6050.sensivity[1]=(float)Sensivity_gyro_250dps;
                break;
            }

            case Gyro_500dps_mask:
            {
                _config_mpu6050.sensivity[1]=(float)Sensivity_gyro_500dps;
                break;
            }

            case Gyro_1000dps_mask:
            {
                _config_mpu6050.sensivity[1]=(float)Sensivity_gyro_1000dps;
                break;
            }

            case Gyro_2000dps_mask:
            {
                _config_mpu6050.sensivity[1]=(float)Sensivity_gyro_2000dps;
                break;
            }

            default:
                _config_mpu6050.sensivity[1]=1.0f;
                break;
        }
}

static void Read_register_mpu6050(void)
{
    //set the sampling rate
       mpu6050_readRegister8(SMPLRT_DIV,&_config_mpu6050.fields_Registers.REG_SMPLRT_DIV,&_config_mpu6050.packs_reg[0]);


       //clock of gyro with PLL
       mpu6050_readRegister8(PWR_MGMT_1,&_config_mpu6050.fields_Registers.REG_PWR_MGMT_1,&_config_mpu6050.packs_reg[1]);


       //disable external input and use highest bandwidth frequency
       mpu6050_readRegister8(CONFIG,&_config_mpu6050.fields_Registers.REG_CONFIG,&_config_mpu6050.packs_reg[2]);


       //select accelerometer full scale range �xg
       mpu6050_readRegister8(ACCEL_CONFIG,&_config_mpu6050.fields_Registers.REG_ACCEL_CONFIG,&_config_mpu6050.packs_reg[3]);


       //select gyro full scale range �xdps
       mpu6050_readRegister8(GYRO_CONFIG,&_config_mpu6050.fields_Registers.REG_GYRO_CONFIG,&_config_mpu6050.packs_reg[4]);


      //set the sampling rate
      mpu6050_readRegister8(INT_ENABLE,&_config_mpu6050.fields_Registers.REG_INT_ENABLE,&_config_mpu6050.packs_reg[5]);

}

static void mpu6050_Register_Init(Sample_Rate _rate,Gyro_Config _g_conf,Accel_Config _a_conf)
{

    Read_register_mpu6050();

   //--------------------------------------------------------------------------------------------------------------------
   //set the sampling rate to 1kHz
   uint8_t reg=0x00;
   reg=(uint8_t)_rate;
   mpu6050_setRegisters8(SMPLRT_DIV,reg);
   //----------------------------------------------------------------------------------------
   //uses clock of gyro with PLL
   reg=0x01;
   mpu6050_setRegisters8(PWR_MGMT_1,reg);
   //----------------------------------------------------------------------------------------
   // disable external input and use highest bandwidth frequency
   reg=0;
   mpu6050_setRegisters8(CONFIG,reg);
   //----------------------------------------------------------------------------------------
   // select accelerometer full scale range �xg
   reg=(uint8_t)_a_conf;
   mpu6050_setRegisters8(ACCEL_CONFIG,reg);
   //----------------------------------------------------------------------------------------
   // select gyro full scale range �xdps
   reg=(uint8_t)_g_conf;
   mpu6050_setRegisters8(GYRO_CONFIG,reg);
   //------------------------------------------------------------------------------------
   //Enable internal interrupt when data ready
   reg=0x01;
   mpu6050_setRegisters8(INT_ENABLE,reg);

   Read_register_mpu6050();



}


void mpu6050_Init(uint8_t number_i2c,Sample_Rate _rate,Gyro_Config _g_conf,Accel_Config _a_conf)
{
    _config_mpu6050.number_i2c=number_i2c;
    I2C_REGISTERS_activateI2C(_config_mpu6050.number_i2c);
    Check_Id();
    mpu6050_Register_Init(_rate,_g_conf,_a_conf);
    Select_Sensivity_Accel_mpu6050(_a_conf);
    Select_Sensivity_Gyro_mpu6050(_g_conf);
    _config_mpu6050.sensivity[2]=(1/340.0f)+36.53f;

}

void mpu6050_GetData_hw(int32_t* Gyro,int32_t* Accel,int32_t* Temp)
{

    mpu6050_readMemRegister(ACCEL_XOUT_H,_config_mpu6050.buffer,14,&_config_mpu6050.pack_out_mpu6050);
    _config_mpu6050.field_Accel_Data.AX = (int32_t) ( (_config_mpu6050.buffer[0] << 8 ) |_config_mpu6050.buffer[1] );
    _config_mpu6050.field_Accel_Data.AY = (int32_t) ( (_config_mpu6050.buffer[2] << 8 ) |_config_mpu6050.buffer[3] );
    _config_mpu6050.field_Accel_Data.AZ = (int32_t) ( (_config_mpu6050.buffer[4] << 8 ) |_config_mpu6050.buffer[5] );
    _config_mpu6050.Temp = (int32_t) ( (_config_mpu6050.buffer[6] << 8 ) |_config_mpu6050.buffer[7] );
    _config_mpu6050.field_Gyro_Data.GX = (int32_t) ( (_config_mpu6050.buffer[8] << 8 ) |_config_mpu6050.buffer[9] );
    _config_mpu6050.field_Gyro_Data.GY = (int32_t) ( (_config_mpu6050.buffer[10] << 8 ) |_config_mpu6050.buffer[11] );
    _config_mpu6050.field_Gyro_Data.GZ = (int32_t) ( (_config_mpu6050.buffer[12] << 8 ) |_config_mpu6050.buffer[13] );

}
void mpu6050_GetData_real(int32_t* Gyro,int32_t* Accel,int32_t* Temp)
{
    for(int i=0;i<3;i++)
        {
            Accel[i]=(float)(_config_mpu6050.field_Accel_Data.Accel_Data_hw[i])/_config_mpu6050.sensivity[0];
            Gyro[i]=(float)(_config_mpu6050.field_Gyro_Data.Gyro_Data_hw[i])/_config_mpu6050.sensivity[1];

        }
        *Temp=_config_mpu6050.Temp*_config_mpu6050.sensivity[2];
}




