/*
 * Logic_types.h
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_LOGIC_TYPES_H_
#define LOGIC_LOGIC_TYPES_H_


#include "Main.h"
#include "settings.h"
#include "headers.h"



typedef enum _mode_system_type{
    MODE_WORK_INIT,
    MODE_WORK,
    MODE_SETUP_INIT,
    MODE_SETUP,
    MODE_TEST_INIT,
    MODE_TEST,
    MODE_COUNT
}mode_system_e;








#pragma pack(push,1)
typedef union
{
    struct
    {
        uint8_t sw1:1;
        uint8_t sw2:1;
        uint8_t reserved:6;

    }fields;
    uint8_t All;
}gpio_input_def;
#pragma pack(pop)


#pragma pack(push,1)
typedef union
{
    struct
    {
        uint8_t gpio1:1;
        uint8_t gpio2:1;
        uint8_t reserved:6;

    }fields;
    uint8_t All;
}gpio_out_def;
#pragma pack(pop)




#pragma pack(push,1)
typedef struct
{
    int32_t Temp_hw;
   union
   {
       struct
       {
           int32_t AX;
           int32_t AY;
           int32_t AZ;
       };
       int32_t Accel_Data[3];
   }fields_Accel;

   union
      {
          struct
          {
              int32_t GX;
              int32_t GY;
              int32_t GZ;
          };
          int32_t Gyro_Data[3];
      }fields_Gyro;

}mpu6050_Data_hw;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    float Temp_hw;
       union
       {
           struct
           {
               float AX;
               float AY;
               float AZ;
           };
           float Accel_Data[3];
       }fields_Accel;

       union
          {
              struct
              {
                  float GX;
                  float GY;
                  float GZ;
              };
              float Gyro_Data[3];
          }fields_Gyro;

}mpu6050_state_s;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    uint16_t ID_temp;
    uint8_t Name[8];
    uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;
#pragma pack(pop)




#pragma pack(push,1)
typedef struct
{
    gpio_input_def          _input_state;
    gpio_out_def            _out_state;
    mode_system_e           mode;
    mpu6050_state_s         _mpu6050;


}Transmit_data2_Model_MainData;
#pragma pack(pop)

typedef struct
{


}Transmit_data3_Model_Settings;

#pragma pack(push,1)
typedef struct
{

    int16_t Temp;

    union
    {
        struct
        {
            int16_t AX;
            int16_t AY;
            int16_t AZ;
        };
        int16_t Accel_Data_hw[3];
    }field_Accel_Data;

    union
    {
         struct
         {
             int16_t GX;
             int16_t GY;
             int16_t GZ;
         };
         int16_t Gyro_Data_hw[3];
    }field_Gyro_Data;


}mpu9250_DataDef;

#pragma pack(pop)


#pragma pack(push,1)
typedef struct
{

    float Temp;

    union
    {
        struct
        {
            float AX;
            float AY;
            float AZ;
        };
        float Accel_Data_hw[3];
    }field_Accel_Data;

    union
    {
         struct
         {
             float GX;
             float GY;
             float GZ;
         };
         float Gyro_Data_hw[3];
    }field_Gyro_Data;


}mpu9250_DataRealDef;

#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{

    mode_system_e           mode;
    gpio_input_def          _input_state;
    gpio_out_def            _out_state;
    mpu6050_Data_hw         _hw_mpu6050;
    mpu6050_state_s         _mpu6050_mainData;
    mpu9250_DataDef         _hw_mpu9250;
    mpu9250_DataRealDef     _mpu9250_mainData;

    uint8_t                     sync_trigger;
    uint32_t                    _time_ms;

    uint32_t                    number_ms_Timer;
    uint32_t                    number_sync_Timer;
    uint32_t                    Led_count_ctrl;

    uint32_t                    echo_count;

    uint8_t                     i2c_num_mpu6050;


    Transmit_data1_Model_DataBoard  _data1;
    Transmit_data2_Model_MainData   _data2;
    Transmit_data3_Model_Settings   _data3;

    bool                    rs485_flag_Model_DataBoard;
    bool                    rs485_flag_Model_MainData;
    bool                    rs485_flag_Model_Settings;

}work_state_model_s;


#pragma pack(pop)




#endif /* LOGIC_LOGIC_TYPES_H_ */
