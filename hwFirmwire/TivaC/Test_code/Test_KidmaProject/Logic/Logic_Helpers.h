/*
 * Logic_Helpers.h
 *
 *  Created on: 6 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef LOGIC_LOGIC_HELPERS_H_
#define LOGIC_LOGIC_HELPERS_H_


#include "headers.h"
#include "Logic_types.h"



void Get_Read_Gpio(gpio_input_def* input);
void Set_Gpio_Out(gpio_out_def* out);

#endif /* LOGIC_LOGIC_HELPERS_H_ */
