/*
 * timers.c
 *
 *  Created on: 13 mar. 2020
 *      Author: peresaliak
 */

#include "timers.h"
#include "timers_conf.h"
#include "timers_struct.h"
#include "settings.h"
#include "headers.h"

TIMERS_state_s timers_state[TIMER_QUANTITY];
static void TIMERS_reg_interrupt(uint8_t numTimer);
//Interrupts handlers for timers
static void TIMERS_handler0(void);
static void TIMERS_handler1(void);
static void TIMERS_handler2(void);
static void TIMERS_handler3(void);
static void TIMERS_handler4(void);
static void TIMERS_handler5(void);

//--------------------------------------------------------------------------------
/*
 * Initialize timers configured in structure (timers_conf.h)
 */

void TIMERS_init()
{
    TIMERS_resetAll();
}

void TIMERS_resetAll()
{
    for(int8_t i=0;i<TIMER_QUANTITY;i++){
        if(timers_state[i].flag_lock==0){
            timers_state[i].fCallbackLoad=NULL;
            timers_state[i].fCallbackMatch=NULL;
            timers_state[i].freq=0;
            timers_state[i].match=0;
            timers_state[i].period=0;
            timers_state[i].state=TIMERS_NO_INIT;
        }
    }
}

int32_t TIMERS_lock(int8_t numTimer)
{
    if(timers_state[numTimer].state!=TIMERS_NO_INIT)
        timers_state[numTimer].flag_lock=1;
    return 0;
}

int32_t TIMERS_unlock(int8_t numTimer)
{
    timers_state[numTimer].flag_lock=0;
    return 0;
}

int8_t TIMERS_getNewTimer(uint32_t periodTime, bool isPeriodic, void (*pfCallback)(void), bool start)
{
    int8_t numTimer=-1;
    for(uint8_t num=0; num<TIMER_QUANTITY; num++){
        if(timers_state[num].state==TIMERS_NO_INIT){
            numTimer=num;
            break;
        }
    };
    if(numTimer==-1)
        return -1;

    SysCtlPeripheralEnable(timers_config[numTimer].sys_peryph);
    while (!SysCtlPeripheralReady(timers_config[numTimer].sys_peryph));
    TimerClockSourceSet(timers_config[numTimer].base,
                        timers_config[numTimer].clock_sourse);
    if(isPeriodic)
        TimerConfigure(timers_config[numTimer].base,
                       TIMER_CFG_A_PERIODIC);
    else
        TimerConfigure(timers_config[numTimer].base,
                       TIMER_CFG_A_ONE_SHOT);
    TimerLoadSet(timers_config[numTimer].base,TIMER_A, periodTime-1);
    IntEnable(timers_config[numTimer].int_base);
    TimerIntEnable(timers_config[numTimer].base,TIMER_TIMA_TIMEOUT);
    TIMERS_reg_interrupt(numTimer);

    timers_state[numTimer].fCallbackLoad = NULL;
    timers_state[numTimer].fCallbackMatch = NULL;
    timers_state[numTimer].period=periodTime;
    timers_state[numTimer].match=0;
    timers_state[numTimer].state=TIMERS_INIT;
    timers_state[numTimer].freq=SYS_CLOCK/periodTime;

    TIMERS_Set_CallbackLoad(numTimer, pfCallback);

    if(start)
        TIMERS_Start(numTimer);
    return numTimer;
}

void TIMERS_setMatch(int8_t numTimer, uint32_t matchTime, void (*pfCallback)(void))
{
    if((numTimer<0)||(numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;

    TimerMatchSet(timers_config[numTimer].base,TIMER_A, matchTime-1);
    timers_state[numTimer].match=matchTime;

    TIMERS_Set_CallbackMatch(numTimer, pfCallback);

    TimerIntEnable(timers_config[numTimer].base,TIMER_TIMA_MATCH);
}


void TIMERS_reset(int8_t numTimer)
{
    if((numTimer<0)||(numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;
    timers_state[numTimer].state=TIMERS_NO_INIT;
}

/*
 * Set callback functions
 */
void TIMERS_Set_CallbackLoad(int8_t numTimer, void (*pfCallback)(void))
{
    if((pfCallback==NULL)||(numTimer<0)||
            (numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;
    timers_state[numTimer].fCallbackLoad = pfCallback;
}

void TIMERS_Set_CallbackMatch(int8_t numTimer, void (*pfCallback)(void))
{
    if((pfCallback==NULL)||(numTimer<0)||
            (numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;
    timers_state[numTimer].fCallbackMatch = pfCallback;
}
//---------------------------------------------------------------------------------

void TIMERS_Start(int8_t numTimer)
{
    if((numTimer<0)||(numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;
    TimerEnable(timers_config[numTimer].base,TIMER_A);
}

void TIMERS_Stop(int8_t numTimer)
{
    if((numTimer<0)||(numTimer>=TIMER_QUANTITY)||
            (timers_state[numTimer].state==TIMERS_NO_INIT))
        return;
    TimerDisable(timers_config[numTimer].base,TIMER_A);
}
//---------------------------------------------------------------------------------
//***********************************************************************************



//***********************************************************************************
//---------------------------------------------------------------------------------
//Register interrupt handlers for each timer
void TIMERS_reg_interrupt(uint8_t numTimer)
{
    switch(numTimer)
    {
       case 0:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler0);
           break;
       case 1:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler1);
           break;
       case 2:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler2);
           break;
       case 3:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler3);
           break;
       case 4:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler4);
           break;
       case 5:
           TimerIntRegister(timers_config[numTimer].base,TIMER_A,TIMERS_handler5);
           break;
    }
}
//----------------------------------------------------------------------------------
void TIMERS_handler0(void)
{
    uint32_t status=TimerIntStatus(timers_config[0].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[0].base, TIMER_TIMA_MATCH);
        if(timers_state[0].fCallbackMatch)
            timers_state[0].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[0].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[0].fCallbackLoad)
            timers_state[0].fCallbackLoad();
    }
}

void TIMERS_handler1(void)
{
    uint32_t status=TimerIntStatus(timers_config[1].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[1].base, TIMER_TIMA_MATCH);
        if(timers_state[1].fCallbackMatch)
            timers_state[1].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[1].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[1].fCallbackLoad)
            timers_state[1].fCallbackLoad();
    }
}

void TIMERS_handler2(void)
{
    uint32_t status=TimerIntStatus(timers_config[2].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[2].base, TIMER_TIMA_MATCH);
        if(timers_state[2].fCallbackMatch)
            timers_state[2].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[2].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[2].fCallbackLoad)
            timers_state[2].fCallbackLoad();
    }
}

void TIMERS_handler3(void)
{
    uint32_t status=TimerIntStatus(timers_config[3].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[3].base, TIMER_TIMA_MATCH);
        if(timers_state[3].fCallbackMatch)
            timers_state[3].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[3].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[3].fCallbackLoad)
            timers_state[3].fCallbackLoad();
    }
}

void TIMERS_handler4(void)
{
    uint32_t status=TimerIntStatus(timers_config[4].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[4].base, TIMER_TIMA_MATCH);
        if(timers_state[4].fCallbackMatch)
            timers_state[4].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[4].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[4].fCallbackLoad)
            timers_state[4].fCallbackLoad();
    }
}

void TIMERS_handler5(void)
{
    uint32_t status=TimerIntStatus(timers_config[5].base, true);
    if(status&TIMER_TIMA_MATCH){
        TimerIntClear(timers_config[5].base, TIMER_TIMA_MATCH);
        if(timers_state[5].fCallbackMatch)
            timers_state[5].fCallbackMatch();
    }
    if(status&TIMER_TIMA_TIMEOUT){
        TimerIntClear(timers_config[5].base, TIMER_TIMA_TIMEOUT);
        if(timers_state[5].fCallbackLoad)
            timers_state[5].fCallbackLoad();
    }
}

/**************************************************************************************
|  End file
**************************************************************************************/




