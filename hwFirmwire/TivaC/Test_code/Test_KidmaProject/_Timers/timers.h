/*
 * timers.h
 *
 *  Created on: 28 ���. 2020 �.
 *      Author: frolov
 */

#include "headers.h"

#ifndef TIMERS_TIMERS_H_
#define TIMERS_TIMERS_H_

void TIMERS_init();
void TIMERS_resetAll();
int32_t TIMERS_lock(int8_t numTimer);
int32_t TIMERS_unlock(int8_t numTimer);
int8_t TIMERS_getNewTimer(uint32_t periodTime, bool isPeriodic, void (*pfCallback)(void), bool start);
void TIMERS_setMatch(int8_t numTimer, uint32_t matchTime, void (*pfCallback)(void));
void TIMERS_reset(int8_t numTimer);
void TIMERS_Set_CallbackLoad(int8_t numTimer, void (*pfCallback)(void));
void TIMERS_Set_CallbackMatch(int8_t numTimer, void (*pfCallback)(void));
void TIMERS_Start(int8_t numTimer);
void TIMERS_Stop(int8_t numTimer);

#endif /* TIMERS_TIMERS_H_ */
