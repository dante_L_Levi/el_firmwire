/*
 * I2C_registers_struct.c
 *
 *  Created on: 13 ��� 2020 �.
 *      Author: peresaliak
 */

#include "i2c_registers_settings.h"
#include "i2c_registers.h"
#include <stdbool.h>
#include <stdint.h>

typedef enum {
    STATE_NO_INIT,
    STATE_WAIT,
    STATE_TRANSMIT,
    STATE_ERROR
}chip_registr_state;

typedef enum {
    CHIPS_REGISRT_START_TRANSMIT,
    CHIPS_REGISRT_START_RECEIVE,
    CHIPS_REGISRT_DATA_TRANSMIT,
    CHIPS_REGISRT_DATA_RECEIVE
}type_data;

#pragma pack(push, 1)
typedef struct {
    uint32_t control;
    uint8_t data;
    type_data type;
}one_package_byte;
#pragma pack(pop)

typedef struct{
    one_package_byte pack[I2C_REGISTERS_LENGTH_SEND_BUFFER];
    int16_t number;
    int16_t last_number;
}I2C_registers_stream_s;

typedef struct{
    I2C_REGISTERS_receive_package_s pack[I2C_REGISTERS_COUNT_RECEIVE_PACK];
    uint8_t number;
    uint8_t last_number;
}I2C_registers_receive_data_s;

typedef struct{
    chip_registr_state state;
    I2C_registers_stream_s* streem;
    I2C_registers_receive_data_s* receive;
}I2C_registers_channal_s;



