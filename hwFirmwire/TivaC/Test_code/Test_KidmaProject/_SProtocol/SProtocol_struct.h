/*
 * SProtocol_struct.h
 *
 *  Created on: 19 ����. 2020 �.
 *      Author: peresaliak
 */

#ifndef SPROTOCOL_SPROTOCOL_STRUCT_H_
#define SPROTOCOL_SPROTOCOL_STRUCT_H_
#include "SProtocol.h"

#pragma pack(push, 1)
typedef union{
    struct{
        unsigned        ID:15;
        unsigned        master:1;
        uint8_t         len;
        uint8_t         cmd;
        uint8_t         payload[255];
        uint16_t        crc;
    };
    uint8_t dataPack[261];
}SProtocol_pack;
#pragma pack(pop)

typedef struct _SP_listItem{
    SProtocol_message       msg;
    struct _SP_listItem*    next;
}SP_listItem;


#endif /* SPROTOCOL_SPROTOCOL_STRUCT_H_ */
