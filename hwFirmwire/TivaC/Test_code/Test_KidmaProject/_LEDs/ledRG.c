/*
 * leds.c
 *
 *  Created on: 5 July 2020
 */


#include "ledRG.h"
#include "ledRG_conf.h"

static uint8_t led_count=0;
static uint16_t led_iter=0;
static bool status_led=false;
//-------------------------------------------------------------------------------------
/*
 * Initialize periphery for LEDs
 */
void LED_Init(void)
{
    led_count=0;
    led_iter=0;
    GPIOPinTypeGPIOOutput(PORT_RED, PIN_RED);
    GPIOPinTypeGPIOOutput(PORT_GREEN, PIN_GREEN);
    LED_Switch(LED_OFF);
}
//-------------------------------------------------------------------------------------
/*
 * Set LED on to color
 */
void LED_Switch(LED_state_e state)
{
    switch(state)
    {
       case LED_RED:
            GPIOPinWrite(PORT_RED, PIN_RED, 0xFF);
            GPIOPinWrite(PORT_GREEN, PIN_GREEN, 0x00);
            break;
       case LED_GREEN:
           GPIOPinWrite(PORT_RED, PIN_RED, 0x00);
           GPIOPinWrite(PORT_GREEN, PIN_GREEN, 0xFF);
            break;
       case LED_OFF:
           GPIOPinWrite(PORT_RED, PIN_RED, 0xFF);
           GPIOPinWrite(PORT_GREEN, PIN_GREEN, 0x00);
            break;
       case LED_BOTH:
           GPIOPinWrite(PORT_RED, PIN_RED, 0xFF);
           GPIOPinWrite(PORT_GREEN, PIN_GREEN, 0xFF);
            break;
    }
}
//-------------------------------------------------------------------------------------
/*
 * Blink LED from Red to Green
 */
void LED_Blink_RG(uint8_t led_id,uint32_t interval)
{
    led_iter++;
    if(led_iter >= interval)
    {
        led_iter=0;
        led_count++;
        if(led_count>=blinks[led_id].count)
            led_count=0;
        LED_Switch(blinks[led_id].state[led_count]);
    }
}

//-------------------------------------------------------------------------------------

/*
 * Blink LED  Red
 */
void Indicate_Status(uint32_t interval)
{
    led_iter++;
    if(led_iter >= interval)
    {
        led_iter=0;
        status_led=!status_led;
        status_led?(GPIOPinWrite(PORT_RED, PIN_RED, 0xFF)):(GPIOPinWrite(PORT_RED, PIN_RED, 0x00));
    }
}






