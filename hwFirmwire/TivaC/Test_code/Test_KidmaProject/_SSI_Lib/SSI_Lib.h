/*
 * SSI_Lib.h
 *
 *  Created on: 11 ��� 2022 �.
 *      Author: Lepatenko
 */

#ifndef SSI_LIB_SSI_LIB_H_
#define SSI_LIB_SSI_LIB_H_

#include <stdint.h>
#include <stdbool.h>


typedef void (*spi_cs_fptr_t)(uint8_t state);
typedef void (*spi_rx_callback_fptr_t)(uint8_t ssi_num, const uint8_t *rx_data, uint8_t len);

void Init_chipSelect(uint32_t port,uint32_t pin);
void SSI_Init(uint32_t number);
void SSI_Init_with_Interrupt(uint32_t,uint8_t ssi_mode);
void SSI_read(uint32_t number,uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len);
void SSI_write(uint32_t number,uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len);
void Set_Speed_SSI(uint32_t speed);

#endif /* SSI_LIB_SSI_LIB_H_ */
