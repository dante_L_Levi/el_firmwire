/*
 * pwm_no_sync_struct.h
 *
 *  Created on: 17 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef PWM_EASY_PWM_EASY_STRUCT_H_
#define PWM_EASY_PWM_EASY_STRUCT_H_

typedef enum{
    PWM_EASY_NO_INIT = 0x00,
    PWM_EASY_RUN,
    PWM_EASY_STOP
}pwmEasy_state_e;

typedef struct{
    pwmEasy_state_e state;
    float freq;
    float dutyCycle;
}pwmEasy_state_s;

#endif /* PWM_EASY_PWM_EASY_STRUCT_H_ */
