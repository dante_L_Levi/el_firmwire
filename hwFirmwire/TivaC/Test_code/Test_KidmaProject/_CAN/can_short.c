/*
 * can_short.c
 *
 *  Created on: 13 ���. 2020 �.
 *      Author: peresaliak
 */

#include "headers.h"
#include "can_short.h"
#include "can_config.c"

#include "settings.h"
#include "sys_setup.h"

struct {

    uint8_t rxPackData[CAN_NUM_PACK_MAX+1][8];
    tCANMsgObject canObj[CAN_NUM_OBJ_MAX+1];
    canPack_s pack[CAN_NUM_PACK_MAX+1];
    uint8_t nextNumObj;
    uint8_t nextNumPack;

    canMsg_s  msg[CAN_NUM_MSG_MAX];
    uint8_t nextNumMsg;

    bool flagCAN_FatalError;
    uint32_t countCAN_Transmit;
    uint32_t countCAN_Received;
    uint32_t countCAN_Error;
}CAN_state;

static uint8_t NULL_DATA[8]={0};

static void CAN_clearBuffers();
static uint8_t CAN_messageComplete(uint8_t);
static void CAN_error(uint32_t);
static void CAN_intHandler(void);

int32_t CAN_init(uint32_t baudrate)
{
    //CAN
//    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOCAN);
//    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOCAN));
//    GPIOPinConfigure(GPIO_Pxx_CANxRX);
//    GPIOPinConfigure(GPIO_Pxx_CANxTX);
//    GPIOPinTypeCAN(GPIO_PORTCAN_BASE, GPIO_PIN_RXCAN | GPIO_PIN_TXCAN);

//    GPIODirModeSet(GPIO_PORT_SIL_BASEC, GPIO_PIN_SIL, GPIO_DIR_MODE_OUT);
//    GPIOPadConfigSet(GPIO_PORT_SIL_BASEC, GPIO_PIN_SIL, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD_WPU);
//    GPIOPinWrite(GPIO_PORT_SIL_BASEC, GPIO_PIN_SIL, 0x00);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_CAN));
    CANInit(CAN_BASE);
    CANRetrySet(CAN_BASE, true);
    CANBitRateSet(CAN_BASE, Sys_freq(), baudrate);
    CANEnable(CAN_BASE);
    CANIntEnable(CAN_BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

    CAN_state.flagCAN_FatalError=false;
    CAN_state.countCAN_Transmit=0;
    CAN_state.countCAN_Received=0;
    CAN_state.countCAN_Error=0;

    int32_t error=CAN_clear();
    if(error)
        return error;

    CANIntRegister(CAN_BASE, CAN_intHandler);
    return 0;
}

int32_t CAN_setBitrate(uint32_t baudrate)
{
    CANBitRateSet(CAN_BASE, Sys_freq(), baudrate);
    return 0;
}

int32_t CAN_addReceiver(uint32_t id_start, uint32_t id_step,  uint8_t* reciver, uint8_t lenData, uint8_t* numMsg)
{
    if((CAN_state.nextNumMsg+1)>=CAN_NUM_MSG_MAX)
        return 1;
    *numMsg=0;
    CAN_state.msg[CAN_state.nextNumMsg].firstPack=CAN_state.nextNumPack;
    CAN_state.msg[CAN_state.nextNumMsg].flagTypeRx=1;
    CAN_state.msg[CAN_state.nextNumMsg].lenMsg=lenData;
    CAN_state.msg[CAN_state.nextNumMsg].targetAddrRx=reciver;

    int8_t count_pack=(lenData+7)/8;
    if(count_pack==0)
        count_pack=1;
    for(int8_t i=0, nPack=CAN_state.nextNumPack, nObj=CAN_state.nextNumObj;
            i<count_pack; i++, nPack++, nObj++){
        if(nPack>CAN_NUM_PACK_MAX)
            return 1;
        if(nObj>CAN_NUM_OBJ_MAX)
            return 2;
        CAN_state.pack[nPack].flagPack=0;
        CAN_state.pack[nPack].lenPack=8;
        CAN_state.pack[nPack].nMsg=CAN_state.nextNumMsg;
        CAN_state.pack[nPack].nObj=nObj;
        CAN_state.canObj[nPack].ui32MsgID=id_start+id_step*i;
        CAN_state.canObj[nPack].ui32Flags=MSG_OBJ_USE_ID_FILTER | MSG_OBJ_RX_INT_ENABLE;;
        CAN_state.canObj[nPack].ui32MsgIDMask=0x7FF;
        CAN_state.canObj[nPack].ui32MsgLen=8;
        CAN_state.canObj[nPack].pui8MsgData=CAN_state.rxPackData[nPack];
    }

    *numMsg=CAN_state.nextNumMsg;
    for(int8_t nPack=CAN_state.nextNumPack; nPack<(count_pack+CAN_state.nextNumPack); nPack++){
        CANMessageSet(CAN_BASE, CAN_state.pack[nPack].nObj,
                      &CAN_state.canObj[nPack], MSG_OBJ_TYPE_RX);
    }
    CAN_state.nextNumMsg++;
    CAN_state.nextNumObj+=count_pack;
    CAN_state.nextNumPack+=count_pack;

    return 0;
}

int32_t CAN_addTransmitter(uint32_t id_start, uint32_t id_step,  uint8_t lenData, uint8_t* numMsg)
{
    if((CAN_state.nextNumMsg+1)>=CAN_NUM_MSG_MAX)
        return 1;
    *numMsg=0;
    uint8_t nMsg=CAN_state.nextNumMsg;
    CAN_state.msg[nMsg].firstPack=CAN_state.nextNumPack;
    CAN_state.msg[nMsg].flagTypeRx=0;
    CAN_state.msg[nMsg].lenMsg=lenData;
    CAN_state.msg[nMsg].targetAddrRx=NULL;
    CAN_state.msg[nMsg].flagLock=0;

    int8_t count_pack=(lenData+7)/8;
    if(count_pack==0)
        count_pack=1;
    for(int8_t i=0, nPack=CAN_state.nextNumPack, nObj=CAN_state.nextNumObj;
            i<count_pack; i++, nPack++, nObj++){
        if(nPack>CAN_NUM_PACK_MAX)
            return 1;
        if(nObj>CAN_NUM_OBJ_MAX)
            return 2;
        CAN_state.pack[nPack].flagPack=1;
        CAN_state.pack[nPack].nMsg=nMsg;
        CAN_state.pack[nPack].nObj=nObj;
        CAN_state.canObj[nPack].ui32MsgID=id_start+id_step*i;
        CAN_state.canObj[nPack].ui32Flags=MSG_OBJ_TX_INT_ENABLE;
        CAN_state.canObj[nPack].ui32MsgIDMask=0x7FF;
        if(lenData>=8){
            CAN_state.pack[nPack].lenPack=8;
            CAN_state.canObj[nPack].ui32MsgLen=8;
            lenData-=8;
        }
        else{
            CAN_state.pack[nPack].lenPack=lenData;
            CAN_state.canObj[nPack].ui32MsgLen=lenData;
        }
        CAN_state.canObj[nPack].pui8MsgData=CAN_state.rxPackData[nPack];
    }

    *numMsg=CAN_state.nextNumMsg;
    CAN_state.nextNumMsg++;
    CAN_state.nextNumObj+=count_pack;
    CAN_state.nextNumPack+=count_pack;
//    for(int8_t nPack=CAN_state.nextNumPack; nPack<(count_pack+CAN_state.nextNumPack); nPack++){
//        CANMessageSet(CAN_BASE, CAN_state.pack[nPack].nObj, &CAN_state.canObj[nPack], MSG_OBJ_TYPE_TX);
//    }

    return 0;
}

int32_t CAN_setCallback(uint8_t nMsg, void (*fCallback)(uint8_t))
{
    if(nMsg>=CAN_state.nextNumMsg)
        return 1;
    if(CAN_state.msg[nMsg].firstPack==255)
        return 2;
    CAN_state.msg[nMsg].fCallback=fCallback;
    return 0;
}

int32_t CAN_messageReceivedEnd(uint8_t nMsg, uint8_t* rxEnd)
{
    if(CAN_state.msg[nMsg].flagTypeRx==0)
        return 1;
    *rxEnd=CAN_messageComplete(nMsg);
    return 0;
}

int32_t CAN_messageTransmitEnd(uint8_t nMsg, uint8_t* txEnd)
{
    if(CAN_state.msg[nMsg].flagTypeRx)
        return 1;
    *txEnd=CAN_messageComplete(nMsg);
    return 0;
}

int32_t CAN_transmitMessage(uint8_t nMsg, uint8_t* data, uint8_t len)
{
    if(CAN_state.msg[nMsg].lenMsg!=len)
        return 1;
    if(CAN_state.msg[nMsg].flagTypeRx)
        return 2;
    if(!CAN_messageComplete(nMsg))
        return 3;
    memcpy(CAN_state.rxPackData[CAN_state.msg[nMsg].firstPack], data, len);
    for(int8_t nPack=CAN_state.msg[nMsg].firstPack; CAN_state.pack[nPack].nMsg==nMsg; nPack++){
        CAN_state.pack[nPack].flagPack=0;
        CANMessageSet(CAN_BASE, CAN_state.pack[nPack].nObj, &CAN_state.canObj[nPack], MSG_OBJ_TYPE_TX);
    }
    return 0;
}

int32_t CAN_messageReset(uint8_t nMsg)
{
    if(nMsg>=CAN_NUM_MSG_MAX)
        return 1;
    for(uint8_t nPack=CAN_state.msg[nMsg].firstPack; CAN_state.pack[nPack].nMsg==nMsg; nPack++){
        if(CAN_state.msg[nMsg].flagTypeRx)
            CAN_state.pack[nPack].flagPack=0;
        else
            CAN_state.pack[nPack].flagPack=1;
    }
    CAN_state.msg[nMsg].flagLock=0;
    return 0;
}

uint32_t CAN_getCountReceived()
{
    return CAN_state.countCAN_Received;
}

uint32_t CAN_getCountTransmit()
{
    return CAN_state.countCAN_Transmit;
}

uint32_t CAN_getCountErrors()
{
    return CAN_state.countCAN_Error;
}

int32_t CAN_resetIfError()
{
    if(CAN_state.flagCAN_FatalError){
        CANEnable(CAN_BASE);
        CAN_state.flagCAN_FatalError=false;
        CAN_clearBuffers();
    }
    return 0;
}

int32_t CAN_clear()
{
    CAN_state.nextNumPack=0;
    CAN_state.nextNumMsg=0;
    CAN_state.nextNumObj=CAN_NUM_OBJ_START;
    for(int8_t nPack=0; nPack<=CAN_NUM_PACK_MAX; nPack++){
        CAN_state.pack[nPack].nObj=nPack+CAN_NUM_OBJ_START;
        CAN_state.pack[nPack].flagPack=0;
        CAN_state.pack[nPack].lenPack=0;
        CAN_state.pack[nPack].nMsg=255;
        CAN_state.canObj[nPack].ui32MsgID=0;
        CAN_state.canObj[nPack].ui32Flags=MSG_OBJ_USE_ID_FILTER;
        CAN_state.canObj[nPack].ui32MsgIDMask=0x7FF;
        CAN_state.canObj[nPack].ui32MsgLen=0;
        CAN_state.canObj[nPack].pui8MsgData=NULL_DATA;
        CANMessageSet(CAN_BASE, CAN_state.pack[nPack].nObj, &CAN_state.canObj[nPack], MSG_OBJ_TYPE_RX);
    }
    for(int8_t i=0; i<=CAN_NUM_MSG_MAX; i++){
        CAN_state.msg[i].firstPack=255;
        CAN_state.msg[i].flagTypeRx=0;
        CAN_state.msg[i].lenMsg=0;
        CAN_state.msg[i].targetAddrRx=0;
        CAN_state.msg[i].flagLock=0;
    }
    return 0;
}

static void CAN_clearBuffers()
{
    for(uint8_t i=0;i<CAN_state.nextNumPack;i++){
        if(CAN_state.msg[CAN_state.pack[i].nMsg].flagTypeRx)
            CAN_state.pack[i].flagPack=0;
        else
            CAN_state.pack[i].flagPack=1;
        for(int8_t j=0;j<8;j++)
            CAN_state.rxPackData[i][j]=0;
    }
    for(uint8_t i=0;i<CAN_state.nextNumMsg;i++){
        CAN_state.msg[i].flagLock=0;
    }
}

static uint8_t CAN_messageComplete(uint8_t nMsg)
{
    uint8_t flag=1;
    for(uint8_t n=CAN_state.msg[nMsg].firstPack;CAN_state.pack[n].nMsg==nMsg;n++)
        flag&=CAN_state.pack[n].flagPack;
    return flag;
}


static void CAN_error(uint32_t g_ui32ErrFlag)
{
    uint32_t status=CANStatusGet(CAN_BASE, CAN_STS_NEWDAT);
    // CAN controller has entered a Bus Off state.
    if(g_ui32ErrFlag & CAN_STATUS_BUS_OFF)
    {
        g_ui32ErrFlag &= ~(CAN_STATUS_BUS_OFF);
        CAN_state.flagCAN_FatalError=true;
        CAN_state.countCAN_Error++;
    }
    if(g_ui32ErrFlag & CAN_STATUS_EWARN)
    {
        g_ui32ErrFlag &= ~(CAN_STATUS_EWARN);
        CAN_state.countCAN_Error++;
        //flagCAN_FatalError=true;
    }
    if(g_ui32ErrFlag & CAN_STATUS_EPASS)
    {
        g_ui32ErrFlag &= ~(CAN_STATUS_EPASS);
        CAN_state.flagCAN_FatalError=true;
        CAN_state.countCAN_Error++;
    }
    if(g_ui32ErrFlag & CAN_STATUS_RXOK)
    {
        g_ui32ErrFlag &= ~(CAN_STATUS_RXOK);
        //CAN_state.countCAN_Received++;
        //CAN_state.countCAN_Error++;
    }
    if(g_ui32ErrFlag & CAN_STATUS_TXOK)
    {
        g_ui32ErrFlag &= ~(CAN_STATUS_TXOK);
        //CAN_state.countCAN_Transmit++;
        //CAN_state.countCAN_Error++;
    }

    // This is the mask for the last error code field.
    if(g_ui32ErrFlag & CAN_STATUS_LEC_MSK)
    {
        switch(g_ui32ErrFlag & CAN_STATUS_LEC_MSK){
            // A bit stuffing error has occurred.
        case CAN_STATUS_LEC_NONE:
            break;
        case CAN_STATUS_LEC_STUFF:
            break;
        case CAN_STATUS_LEC_FORM:
            break;
        case CAN_STATUS_LEC_ACK:
            if(status==1) break;
            status++;
            break;
        case CAN_STATUS_LEC_BIT1:
            break;
        case CAN_STATUS_LEC_BIT0:
            break;
        case CAN_STATUS_LEC_CRC:
            break;
        }
        g_ui32ErrFlag &= ~(CAN_STATUS_LEC_MSK);
        if(CAN_state.countCAN_Error++>50){
        }

    }
    if(g_ui32ErrFlag)
    {
        g_ui32ErrFlag=0;
    }
}

static void CAN_intHandler(void)
{
    uint32_t status = CANIntStatus(CAN_BASE, CAN_INT_STS_CAUSE);
    uint32_t objectsNum = CANIntStatus(CAN_BASE, CAN_INT_STS_OBJECT);
    CANIntClear(CAN_BASE, status);
    if(objectsNum){
        for(int8_t nPack=0;nPack<CAN_state.nextNumPack;nPack++){
            if(objectsNum&(1<<(CAN_state.pack[nPack].nObj-1))){
                objectsNum=objectsNum&(~(1<<(CAN_state.pack[nPack].nObj-1)));

                CAN_state.pack[nPack].flagPack=1;
                if(CAN_state.msg[CAN_state.pack[nPack].nMsg].flagTypeRx){
                    CANMessageGet(CAN_BASE, CAN_state.pack[nPack].nObj, &CAN_state.canObj[nPack], true);

                    if(((nPack+1)<CAN_state.nextNumPack)&&(CAN_state.pack[nPack+1].nMsg==CAN_state.pack[nPack].nMsg))
                        CAN_state.pack[nPack+1].flagPack=0;
                    else{
                        uint8_t nMsg=CAN_state.pack[nPack].nMsg;

                        if((!CAN_state.msg[nMsg].flagLock)&&CAN_messageComplete(nMsg)){
                            CAN_state.msg[nMsg].flagLock=1;
                            if(CAN_state.msg[nMsg].targetAddrRx){
                                memcpy(CAN_state.msg[nMsg].targetAddrRx, CAN_state.rxPackData[CAN_state.msg[nMsg].firstPack],
                                   CAN_state.msg[nMsg].lenMsg);
                            }
                            if(CAN_state.msg[nMsg].fCallback)
                                CAN_state.msg[nMsg].fCallback(nMsg);
                        }
                    }
                    CAN_state.countCAN_Received++;
                }
                else
                    CAN_state.countCAN_Transmit++;



            }
        }
        if(objectsNum>0){
            objectsNum=0;
        }
    }
    if((status == CAN_INT_INTID_STATUS)||(objectsNum))
    {
        status = CANStatusGet(CAN_BASE, CAN_STS_CONTROL);
        CAN_error(status);
    }
}

