/*
 * ecample.c
 *
 *  Created on: 29 jan. 2020
 *      Author: peresaliak
 */

#include "_CAN/can_short.h"
#include "headers.h"
#include "settings.h"
#include "example.h"



static bool timerFlag;

struct{
    uint8_t flag_timCan;
    uint8_t flag_rxNull;
    uint8_t txDataLong[25];
    uint8_t rxDataLong[25];
    uint8_t rxDataNull[0];
    uint8_t numTxLongMsg;
    uint8_t numRxLongMsg;
    uint8_t numRxNullMsg;
}can;

static void timer_handler();
static void callback_canRxNull(uint8_t);

int main_example(void)
{
    //Timer1 - 100Hz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1));
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, SYS_CLOCK / 100);
    TimerIntRegister(TIMER1_BASE, TIMER_A, timer_handler);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    IntPrioritySet(INT_TIMER1A, 3);
    IntEnable(INT_TIMER1A);
    TimerEnable(TIMER1_BASE, TIMER_A);

    CAN_init(250000);

    //add transmit message id=0x11 0x21 0x31 0x41
    CAN_addTransmitter(0x11, 0x10, sizeof(can.txDataLong), &can.numTxLongMsg);

    //add receiver message id=0x13 0x23 0x33 0x43
    CAN_addReceiver(0x13, 0x10, can.rxDataLong, sizeof(can.rxDataLong), &can.numRxLongMsg);

    //add receiver message id=0x14
    CAN_addReceiver(0x14, 0x10, can.rxDataNull, 0, &can.numRxNullMsg);
    //set callback by received message
    CAN_setCallback(can.numRxNullMsg, callback_canRxNull);

    for(int16_t i=0;i<sizeof(can.txDataLong);i++){
        can.txDataLong[i]=i;
    }



    while(1){
        if(timerFlag){
            timerFlag=false;

            CAN_resetIfError();

            if(can.flag_timCan){
                can.flag_timCan=0;
                uint8_t rxEnd;
                CAN_messageReceivedEnd(can.numRxLongMsg, &rxEnd);
                if(rxEnd){
                    for(int16_t i=0;((i<sizeof(can.txDataLong))&&
                            (i<sizeof(can.rxDataLong)));i++){
                        can.txDataLong[i]=can.rxDataLong[i];
                    }
                    CAN_messageReset(can.numRxLongMsg);

                    //Resets transmission restrictions (does not stop transmission). Sends new data.
                    CAN_messageReset(can.numTxLongMsg);
                    CAN_transmitMessage(can.numTxLongMsg, can.txDataLong,sizeof(can.txDataLong));
                }
            }

        }

        if(can.flag_rxNull){
            can.flag_rxNull=0;
            CAN_messageReset(can.numRxNullMsg);
            //Sends new data if the previous transfer was completed.
            CAN_transmitMessage(can.numTxLongMsg, can.txDataLong,sizeof(can.txDataLong));
        }
    }
}

void timer_handler(void)
{
    uint32_t state = TimerIntStatus(TIMER1_BASE, true);
    TimerIntClear(TIMER1_BASE, state);

    if(state & TIMER_TIMA_TIMEOUT){
        timerFlag=true;
    }
}

static void callback_canRxNull(uint8_t nMsg)
{
    can.flag_rxNull=1;
}

