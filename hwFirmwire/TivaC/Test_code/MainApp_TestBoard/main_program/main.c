

/**
 * main.c
 */

#include "headers.h"
#include "_ADG5401/adg5401.h"
#include "_LEDs/ledRG.h"
#include "_BaseProject/board_address.h"
#include "_BaseProject/protection.h"
#include "_BaseProject/wd.h"
#include "_EEPROM_Settings/_EEPROM_Settings.h"
#include "_Timers/timers.h"
#include "_SProtocol/SProtocol.h"
#include "_RS485/rs485.h"
#include "_Digital_Pins/digital_pins.h"
#include "_Analog_Pins/analog_pins.h"
#include "_ADG520x/adg520x.h"
#include "_PWM_EASY/pwm_easy.h"
#include "_LTC2602/_LTC2602.h"
#include "_CAN/can_short.h"
#include "libs/crc16.h"
#include "sys_setup.h"
#include "settings.h"
#include "_BaseProject/bootloader.h"


bool trig=false;

void callback_timSys(void)
{
    trig=!trig;
}


int main(void)
{
#ifdef RELEASE
    InitWatchDog();
#endif
    //PermanentLockDebug();

    Sys_Setup();
    WriteBoardAddress();

    LED_Init();
    TIMERS_init();
    TIMERS_getNewTimer(Sys_freq()/10, true, &callback_timSys, true);

    while (1)
    {
        if(trig)
        {
            LED_Blink_RG(0,35);

        }

#ifdef RELEASE
        FeedWatchDog();
#endif
    }
}
