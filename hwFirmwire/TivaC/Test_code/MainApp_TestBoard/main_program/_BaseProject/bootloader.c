/*
 * bootloader.c
 *
 *  Created on: 10 ���. 2017 �.
 *      Author: Gorudko
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "driverlib/sysctl.h"

#define FIRMWARE_UPDATE_KEYWORD             0x4321
#define BOOTLOADER_KEY_START_ADDRESS        (uint32_t *)0x20001000

void GoToBootloader(){

    uint32_t * p = BOOTLOADER_KEY_START_ADDRESS;
    *p = FIRMWARE_UPDATE_KEYWORD;
    SysCtlReset();
}



