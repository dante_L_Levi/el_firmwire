/*
 * board_address.c
 *
 *  Created on: 4 ����. 2019 �.
 *      Author: Gorudko
 */
#include "board_address.h"
#include "settings.h"
#include "driverlib/eeprom.h"
#include "driverlib/sysctl.h"



void WriteBoardAddress(){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0));
    while(EEPROMInit() != EEPROM_INIT_OK) {};
    uint32_t tmp;
    EEPROMRead(&tmp, 2044, 4);
    uint32_t * tmp2 = (uint32_t *)_board_id;
    if (tmp != *tmp2) {
        EEPROMProgram((uint32_t *)_board_id, 2044, 4);
    }
}

