/*
 * ShortProtocol.c
 *
 *  Created on: 5 feb. 2020
 *      Author: peresaliak
 */

#include <_SProtocol/SProtocol_connect.h>
#include <stdint.h>
#include <stdbool.h>
#include "_RS485/rs485.h"
#include "settings.h"
#include "headers.h"
#include "SProtocol.h"
#include "SProtocol_struct.h"
#include "libs/crc16.h"


//-----------------settings-----------------
#define PAYLOAD_BUFFER_SIZE         512
#define INPUT_BUFFER_SIZE           261
#define OUTPUT_BUFFER_SIZE          261
#define COUNT_LIST_ITEM             16

typedef enum
{
    RX_WAIT_ID_PART1,
    RX_WAIT_ID_PART2,
    RX_WAIT_LENGTH,
    RX_COLLECT_DATA,
    RX_TO_TX_STATE,
    TX_MODE
}SProtocol_states;

static SProtocol_states state = RX_WAIT_ID_PART1;

static uint16_t RS485_DEVICE_ID;
static uint16_t RS485_MASTER_ID;

static uint8_t rx_buffer[INPUT_BUFFER_SIZE];
static uint16_t rx_count = 0;
static uint16_t rx_length;
static uint16_t rx_payload_len;

static uint8_t payload_buffer[PAYLOAD_BUFFER_SIZE];
static uint8_t* first_payload;
static uint8_t* first_free;

static SP_listItem listItems[COUNT_LIST_ITEM];
static SP_listItem* itemFirst;
static SP_listItem* itemLast;
static SP_listItem* itemReturn;


static volatile uint8_t tx_buffer[OUTPUT_BUFFER_SIZE];
static SProtocol_pack txPack;

//static uint8_t checkRxPkgCrc();
//static uint8_t getPkgCrc8(uint8_t *data, int16_t len);
//static void goToBootloaderHandling();

//static uint16_t getCrc16(uint8_t * pcBlock, uint16_t len);

static int32_t nulTransmitFunction(uint8_t* data, int16_t length);
int32_t (*transmitFunction)(uint8_t* data, int16_t length);

void SProtocol_initBuffers(uint16_t id,
                    int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length))
{
    if(transmitFunctionSet==0)
        transmitFunction=nulTransmitFunction;
    else
        transmitFunction=transmitFunctionSet;
    SProtocol_setId(id);

    for(int8_t i=0;i<COUNT_LIST_ITEM;i++){
        listItems[i].next=&listItems[(i+1)%COUNT_LIST_ITEM];
    }

    SProtocol_resetInput();

    rx_length=6;                //head+crc
}
void SProtocol_setId(uint16_t id)
{
    RS485_DEVICE_ID = id&0x7FFF;
    RS485_MASTER_ID = id|0x8000;
}
uint16_t SProtocol_getId()
{
    return RS485_DEVICE_ID;
}

bool SProtocol_itemAvailable()
{
    return itemFirst!=itemLast;
}

SProtocol_message* SProtocol_getNextItem()
{
    if(SProtocol_itemAvailable()){
        itemReturn=itemFirst;
        itemFirst=itemFirst->next;
        first_payload=itemReturn->msg.data;
        return &itemReturn->msg;
    }
    else{
        itemReturn=itemFirst;
        first_payload=itemReturn->msg.data;
        //SProtocol_resetInput();
        return 0;
    }
}

SProtocol_message* SProtocol_getNowItem()
{
    return &itemFirst->msg;
}

void SProtocol_addTxMessageNoBlock(SProtocol_message *p)
{
    txPack.master=0;
    txPack.ID=RS485_DEVICE_ID;
    txPack.cmd=p->command;
    txPack.len=p->length;
    if(p->length>0)
        memcpy((void*)txPack.payload, p->data, p->length);
    txPack.crc=crc16_CCITT(txPack.dataPack, txPack.len+4);                 //4 bytes - head length
    txPack.payload[txPack.len]=(txPack.crc>>8)&0xFF;
    txPack.payload[txPack.len+1]=txPack.crc&0xFF;                         //2 bytes - crc16 length
    transmitFunction((void*)txPack.dataPack, txPack.len+6);             //6 bytes - head+crc length
}

//static functions
void SProtocol_parserRX(uint8_t* data, int16_t length)
{
    int16_t num=0;
    while (num<length)
    {
        switch(state)
        {
            case RX_COLLECT_DATA:
            {
                rx_buffer[rx_count] = data[num];
                rx_count++;

                if (rx_count >= rx_length)
                {
                    //package received
                    state = RX_WAIT_ID_PART1;
                    rx_count=0;
                    if (crc16_CCITT(rx_buffer, rx_length) == 0)
                    {
                        //CRC passed
                        bool isFree=false;
                        if(first_free>=first_payload){
                            if((first_free+rx_payload_len)<=(payload_buffer+PAYLOAD_BUFFER_SIZE))
                                isFree=true;
                            if((first_payload>=(payload_buffer+rx_payload_len))&!isFree){
                                first_free=payload_buffer;
                                isFree=true;
                            }
                        }
                        else{
                            if((first_free+rx_payload_len)<first_payload)
                                isFree=true;
                        }
                        itemLast->msg.length=rx_payload_len;
                        itemLast->msg.command=rx_buffer[3];
                        itemLast->msg.data=rx_buffer+4;
                        if(!SProtocol_parseCriticalPack(&itemLast->msg)&&isFree){
                            if(itemLast->next!=itemFirst){
                                memcpy((void *) first_free, (void *) (rx_buffer+4),rx_payload_len);       //4 bytes head
                                itemLast->msg.data=first_free;
                                first_free+=rx_payload_len;
                                itemLast=itemLast->next;
                            }
                            else{
                                state = RX_WAIT_ID_PART1;
                            }
                        }
                    }
                }
                break;
            }
            case RX_WAIT_LENGTH:
                rx_payload_len=data[num];
                rx_buffer[rx_count] = data[num];
                rx_length=data[num]+6;          //head+crc
                state = RX_COLLECT_DATA;
                rx_count++;
                break;
            case RX_WAIT_ID_PART2:
                if ((RS485_MASTER_ID >> 8) == data[num])
                {
                    state = RX_WAIT_LENGTH;
                    rx_buffer[rx_count] = data[num];
                    rx_count++;
                    break;
                }
                else{
                    state = RX_WAIT_ID_PART1;
                }
            case RX_WAIT_ID_PART1:
                if ((RS485_MASTER_ID & 0xFF) == data[num])
                {
                    state = RX_WAIT_ID_PART2;
                    rx_buffer[0] = data[num];
                    rx_count=1;
                }
                break;
            default:
                break;
        }
        num++;
    }
}

void SProtocol_endTX()
{
}

void SProtocol_endRX()
{
    state = RX_WAIT_ID_PART1;
    rx_count = 0;
}

void SProtocol_resetInput()
{
    itemFirst=&listItems[0];
    itemLast=itemFirst;
    itemReturn=itemFirst;
    for(int8_t i=0;i<COUNT_LIST_ITEM;i++){
        listItems[i].msg.command=0;
        listItems[i].msg.length=0;
        listItems[i].msg.data=payload_buffer;
    }
    first_payload=payload_buffer;
    first_free=payload_buffer;
}

static int32_t nulTransmitFunction(uint8_t* data, int16_t length)
{
    return 0;
}

