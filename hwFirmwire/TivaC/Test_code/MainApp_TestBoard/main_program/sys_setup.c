/*
 * sys_setup.c
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: frolov
 */

#include "sys_setup.h"
#include "settings.h"

struct{
    uint32_t freq;
    uint8_t pinoutSet;
}sys_status;


void Sys_Setup(void)
{
    sys_status.pinoutSet=0;
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
                    | SYSCTL_XTAL_25MHZ);
    sys_status.freq=SysCtlClockGet();

    FPUEnable();
    FPULazyStackingEnable();

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    PinoutSet();
    sys_status.pinoutSet=1;
}

uint32_t Sys_freq(void)
{
    return sys_status.freq;
}

uint8_t Sys_pinoutSet(void)
{
    return sys_status.pinoutSet;
}

