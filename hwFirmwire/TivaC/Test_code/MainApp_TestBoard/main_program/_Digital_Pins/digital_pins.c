
/**************************************************************************************
|   File name  : digital_pins.c
|   Description:
**************************************************************************************/

/**************************************************************************************
|
**************************************************************************************/
#include <_Digital_Pins/digital_pins.h>
#include <_Digital_Pins/digital_pins_conf.h>

//#include "board.h"
//#include "macros.h"


/**************************************************************************************
|
**************************************************************************************/

/**************************************************************************************
|
**************************************************************************************/
uint8_t pins_input[DIGITAL_PINS_MAX_COUNT]={0};
uint8_t pins_output[DIGITAL_PINS_MAX_COUNT]={0};
/**************************************************************************************
|
**************************************************************************************/
void DIG_Pins_Init(void)
{
    int i;
    for(i=0;i<PIN_QUANTITY;i++)
    {
        if(PIN_Modes[i].pin_id<DIGITAL_PINS_MAX_COUNT)
            switch (PIN_Modes[i].mode)
            {
                case D_INPUT:
                    GPIOPinTypeGPIOInput(pin_s[i].port, pin_s[i].pin);
                    GPIOPadConfigSet(pin_s[i].port,pin_s[i].pin, PIN_Modes[i].strenght,PIN_Modes[i].pull_type);
                    pins_input[PIN_Modes[i].pin_id]=i;
                    break;
                case D_OUTPUT:
                    GPIOPinTypeGPIOOutput(pin_s[i].port, pin_s[i].pin);
                    GPIOPadConfigSet(pin_s[i].port,pin_s[i].pin, PIN_Modes[i].strenght,PIN_Modes[i].pull_type);
                    if(PIN_Modes[i].level)
                        GPIOPinWrite(pin_s[i].port,pin_s[i].pin,0xFF);
                    else
                        GPIOPinWrite(pin_s[i].port,pin_s[i].pin,0);
                    pins_output[PIN_Modes[i].pin_id]=i;
                    break;
                case D_INT_FALL:
                    GPIODirModeSet(pin_s[i].port,pin_s[i].pin,GPIO_DIR_MODE_IN);
                    GPIOPadConfigSet(pin_s[i].port,pin_s[i].pin, PIN_Modes[i].strenght,PIN_Modes[i].pull_type);
                    //GPIO_Set_Interrupt(i, GPIO_FALLING_EDGE, &gpio_int_handler);
                    break;
                case D_INT_RISE:
                    GPIODirModeSet(pin_s[i].port,pin_s[i].pin,GPIO_DIR_MODE_IN);
                    GPIOPadConfigSet(pin_s[i].port,pin_s[i].pin, PIN_Modes[i].strenght,PIN_Modes[i].pull_type);
                    //GPIO_Set_Interrupt(i, GPIO_RISING_EDGE, &gpio_int_handler);
                    break;
                case D_INT_FALLRISE:
                    GPIODirModeSet(pin_s[i].port,pin_s[i].pin,GPIO_DIR_MODE_IN);
                    GPIOPadConfigSet(pin_s[i].port,pin_s[i].pin, PIN_Modes[i].strenght,PIN_Modes[i].pull_type);
                    //GPIO_Set_Interrupt(i, GPIO_BOTH_EDGES, &gpio_int_handler);
                    break;

            }
    }
}

//-------------------------------------------------------------------------------------
void GPIO_Set_Interrupt(int id, uint32_t edge, void (*pfnIntHandler)(void))
{
//    GPIOIntTypeSet(pin_s[id].port,pin_s[id].pin,edge);
//    GPIOIntRegister(pin_s[id].port,pfnIntHandler);
//    GPIOIntEnable(pin_s[id].port,pin_s[id].pin);
}

//-------------------------------------------------------------------------------------
void gpio_int_handler(void)
{
  //GPIOIntClear(pin_s[32].port,pin_s[32].pin);
  return;
}

//-------------------------------------------------------------------------------------
void DIG_read_input_pins(Pin_state_dig_s *pin_st)
{
    pin_st->data=0;
    for(int i=0;i<DIGITAL_PINS_MAX_COUNT;i++)
    {
        if(PIN_Modes[pins_input[i]].mode != F_NONE)
        {
             int32_t state = GPIOPinRead(pin_s[pins_input[i]].port, pin_s[pins_input[i]].pin);
             if(state)
                 pin_st->data |= 1<<(PIN_Modes[pins_input[i]].pin_id);
        }
    }
}
//-------------------------------------------------------------------------------------

void DIG_read_outputs_pins(Pin_state_dig_s *pin_st)
{
    pin_st->data=0;
    for(int i=0;i<DIGITAL_PINS_MAX_COUNT;i++)
    {
        if(PIN_Modes[pins_output[i]].mode == D_OUTPUT)
        {
             int32_t state = GPIOPinRead(pin_s[pins_output[i]].port, pin_s[pins_output[i]].pin);
             if(state)
                 pin_st->data |= 1<<(PIN_Modes[pins_output[i]].pin_id);
        }
    }
}

void DIG_write_output_pins(Pin_state_dig_s *pin_st)
{
    for(int i=0;i<DIGITAL_PINS_MAX_COUNT;i++)
    {
        if(PIN_Modes[pins_output[i]].mode == D_OUTPUT)
        {
            uint32_t p_id = PIN_Modes[pins_output[i]].pin_id;
            uint32_t state = pin_st->data & (1<<p_id);
            if(state)
                GPIOPinWrite(pin_s[pins_output[i]].port,pin_s[pins_output[i]].pin,pin_s[pins_output[i]].pin);
            else
                GPIOPinWrite(pin_s[pins_output[i]].port,pin_s[pins_output[i]].pin,0);
        }
    }
}
/**************************************************************************************
|  End file
**************************************************************************************/
