/*
 * pwm.h
 *
 *  Created on: 17 ���. 2020 �.
 *      Author: peresaliak
 */

#ifndef PWM_EASY_PWM_EASY_H_
#define PWM_EASY_PWM_EASY_H_

#include "headers.h"

void PWM_EASY_init(float freq);
void PWM_EASY_setFreq(uint8_t pwmNum, float freq);
void PWM_EASY_stop(uint8_t pwmNum);
void PWM_EASY_start(uint8_t pwmNum);
void PWM_EASY_setDutyCycle(uint8_t pwmNum, float dutyCycle);



#endif /* PWM_EASY_PWM_EASY_H_ */
