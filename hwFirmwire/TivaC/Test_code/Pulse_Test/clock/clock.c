/*
 * clock.c
 *
 *  Created on: 14 ����. 2021 �.
 *      Author: Gorudko
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "driverlib/sysctl.h"

const uint32_t dividers[] = { 0, SYSCTL_SYSDIV_1, SYSCTL_SYSDIV_2,
SYSCTL_SYSDIV_3,
                              SYSCTL_SYSDIV_4, SYSCTL_SYSDIV_5,
                              SYSCTL_SYSDIV_6,
                              SYSCTL_SYSDIV_7, SYSCTL_SYSDIV_8,
                              SYSCTL_SYSDIV_9,
                              SYSCTL_SYSDIV_10, SYSCTL_SYSDIV_11,
                              SYSCTL_SYSDIV_12,
                              SYSCTL_SYSDIV_13, SYSCTL_SYSDIV_14,
                              SYSCTL_SYSDIV_15,
                              SYSCTL_SYSDIV_16, SYSCTL_SYSDIV_17,
                              SYSCTL_SYSDIV_18,
                              SYSCTL_SYSDIV_19, SYSCTL_SYSDIV_20,
                              SYSCTL_SYSDIV_21,
                              SYSCTL_SYSDIV_22, SYSCTL_SYSDIV_23,
                              SYSCTL_SYSDIV_24,
                              SYSCTL_SYSDIV_25, SYSCTL_SYSDIV_26,
                              SYSCTL_SYSDIV_27,
                              SYSCTL_SYSDIV_28, SYSCTL_SYSDIV_29,
                              SYSCTL_SYSDIV_30,
                              SYSCTL_SYSDIV_31, SYSCTL_SYSDIV_32,
                              SYSCTL_SYSDIV_33,
                              SYSCTL_SYSDIV_34, SYSCTL_SYSDIV_35,
                              SYSCTL_SYSDIV_36,
                              SYSCTL_SYSDIV_37, SYSCTL_SYSDIV_38,
                              SYSCTL_SYSDIV_39,
                              SYSCTL_SYSDIV_40, SYSCTL_SYSDIV_41,
                              SYSCTL_SYSDIV_42,
                              SYSCTL_SYSDIV_43, SYSCTL_SYSDIV_44,
                              SYSCTL_SYSDIV_45,
                              SYSCTL_SYSDIV_46, SYSCTL_SYSDIV_47,
                              SYSCTL_SYSDIV_48,
                              SYSCTL_SYSDIV_49, SYSCTL_SYSDIV_50,
                              SYSCTL_SYSDIV_51,
                              SYSCTL_SYSDIV_52, SYSCTL_SYSDIV_53,
                              SYSCTL_SYSDIV_54,
                              SYSCTL_SYSDIV_55, SYSCTL_SYSDIV_56,
                              SYSCTL_SYSDIV_57,
                              SYSCTL_SYSDIV_58, SYSCTL_SYSDIV_59,
                              SYSCTL_SYSDIV_60,
                              SYSCTL_SYSDIV_61, SYSCTL_SYSDIV_62,
                              SYSCTL_SYSDIV_63,
                              SYSCTL_SYSDIV_64 };
const uint32_t half_dividers[] = { 0, 0, SYSCTL_SYSDIV_2_5, SYSCTL_SYSDIV_3_5,
SYSCTL_SYSDIV_4_5,
                                   SYSCTL_SYSDIV_5_5,
                                   SYSCTL_SYSDIV_6_5,
                                   SYSCTL_SYSDIV_7_5,
                                   SYSCTL_SYSDIV_8_5,
                                   SYSCTL_SYSDIV_9_5,
                                   SYSCTL_SYSDIV_10_5,
                                   SYSCTL_SYSDIV_11_5,
                                   SYSCTL_SYSDIV_12_5,
                                   SYSCTL_SYSDIV_13_5,
                                   SYSCTL_SYSDIV_14_5,
                                   SYSCTL_SYSDIV_15_5,
                                   SYSCTL_SYSDIV_16_5,
                                   SYSCTL_SYSDIV_17_5,
                                   SYSCTL_SYSDIV_18_5,
                                   SYSCTL_SYSDIV_19_5,
                                   SYSCTL_SYSDIV_20_5,
                                   SYSCTL_SYSDIV_21_5,
                                   SYSCTL_SYSDIV_22_5,
                                   SYSCTL_SYSDIV_23_5,
                                   SYSCTL_SYSDIV_24_5,
                                   SYSCTL_SYSDIV_25_5,
                                   SYSCTL_SYSDIV_26_5,
                                   SYSCTL_SYSDIV_27_5,
                                   SYSCTL_SYSDIV_28_5,
                                   SYSCTL_SYSDIV_29_5,
                                   SYSCTL_SYSDIV_30_5,
                                   SYSCTL_SYSDIV_31_5,
                                   SYSCTL_SYSDIV_32_5,
                                   SYSCTL_SYSDIV_33_5,
                                   SYSCTL_SYSDIV_34_5,
                                   SYSCTL_SYSDIV_35_5,
                                   SYSCTL_SYSDIV_36_5,
                                   SYSCTL_SYSDIV_37_5,
                                   SYSCTL_SYSDIV_38_5,
                                   SYSCTL_SYSDIV_39_5,
                                   SYSCTL_SYSDIV_40_5,
                                   SYSCTL_SYSDIV_41_5,
                                   SYSCTL_SYSDIV_42_5,
                                   SYSCTL_SYSDIV_43_5,
                                   SYSCTL_SYSDIV_44_5,
                                   SYSCTL_SYSDIV_45_5,
                                   SYSCTL_SYSDIV_46_5,
                                   SYSCTL_SYSDIV_47_5,
                                   SYSCTL_SYSDIV_48_5,
                                   SYSCTL_SYSDIV_49_5,
                                   SYSCTL_SYSDIV_50_5,
                                   SYSCTL_SYSDIV_51_5,
                                   SYSCTL_SYSDIV_52_5,
                                   SYSCTL_SYSDIV_53_5,
                                   SYSCTL_SYSDIV_54_5,
                                   SYSCTL_SYSDIV_55_5,
                                   SYSCTL_SYSDIV_56_5,
                                   SYSCTL_SYSDIV_57_5,
                                   SYSCTL_SYSDIV_58_5,
                                   SYSCTL_SYSDIV_59_5,
                                   SYSCTL_SYSDIV_60_5,
                                   SYSCTL_SYSDIV_61_5,
                                   SYSCTL_SYSDIV_62_5,
                                   SYSCTL_SYSDIV_63_5 };


uint32_t Clock_Init(uint8_t divider, uint8_t is_half)
{
    is_half = is_half ? 1 : 0;

    if (divider == 0)
    {
        divider = 1;
        is_half = 0;
    }
    else if (divider == 1 && is_half == 0)
    {
        divider = 2;
        is_half = 0;
    }
    else if (divider > 63)
    {
        divider = 64;
        is_half = 0;
    }

    uint32_t sys_div = is_half ? half_dividers[divider] : dividers[divider];

    SysCtlClockSet(
            sys_div | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
    uint16_t tmp = divider * 2 + is_half;
    return 400000000 / tmp;
}
