/*
 * clock.h
 *
 *  Created on: 14 ����. 2021 �.
 *      Author: Gorudko
 */

#ifndef CLOCK_CLOCK_H_
#define CLOCK_CLOCK_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint32_t Clock_Init(uint8_t divider, uint8_t is_half);

#endif /* CLOCK_CLOCK_H_ */
