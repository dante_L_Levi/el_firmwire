/*
 * settings.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Kuntsevich
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "pinout/pinout.h"

#define SYS_CLOCK               80000000

#define SSI_SPEED                   1000000


#pragma DATA_SECTION(_board_id, ".board_id")
static volatile const uint8_t _board_id [4] = {0x10,0xF6,'D','I'};
static const uint16_t * rs485_id =  (uint16_t *)_board_id;

#define FAST_LOOP_SPEED     4000
#define MIDDLE_LOOP_SPEED   10


static const char model[8] = "TestApp ";
static const char firmware[8] = "0.0.1  ";


#endif /* SETTINGS_H_ */
