/*
 * pwm.c
 *
 *  Created on: 22 ���. 2019 �.
 *      Author: Gorudko
 */

#include "pwm.h"
#include "driverlib/pwm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void PWM_Init(uint32_t period)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1))
        ;

    PWMGenConfigure(PWM1_BASE, PWM_GEN_2,
    PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_2, period);

    PWMGenEnable(PWM1_BASE, PWM_GEN_2);

    PWMOutputState( PWM1_BASE, PWM_OUT_5_BIT , true);
}

void PWM_SetDuty(int16_t ch1)
{
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_5, ch1);
}
