/*
 * pwm.h
 *
 *  Created on: 22 ���. 2019 �.
 *      Author: Gorudko
 */

#ifndef PWM_H_
#define PWM_H_

#include "pwm.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void PWM_Init(uint32_t period);
void PWM_SetDuty(int16_t ch1);

#endif /* PWM_H_ */
