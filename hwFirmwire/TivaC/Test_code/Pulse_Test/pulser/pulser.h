/*
 * pulser.h
 *
 *  Created on: 14 ����. 2021 �.
 *      Author: Gorudko
 */

#ifndef PULSER_PULSER_H_
#define PULSER_PULSER_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void Pulser_Init(uint32_t sys_clock, uint32_t period_ns, uint8_t duty,
                 int8_t trimmer);
void Pulser_Setup(uint32_t sys_clock, uint32_t freq, uint8_t duty,
                  int8_t trimmer);
void Pulser_Setup2(uint32_t sys_clock, uint32_t period_ns, uint8_t duty,
                   int8_t trimmer);
void Pulser_enable(uint8_t _enable);
#endif /* PULSER_PULSER_H_ */
