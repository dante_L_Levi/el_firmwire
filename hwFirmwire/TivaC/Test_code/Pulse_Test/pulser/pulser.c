/*
 * pulser.c
 *
 *  Created on: 14 ����. 2021 �.
 *      Author: Gorudko
 */

#include "pulser.h"
#include "driverlib/pwm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "stdbool.h"
#include "driverlib/gpio.h"

typedef struct
{
    uint16_t skip_reload;
    uint16_t duty;
} pulser_state_s;

static volatile int16_t skip_counter = 100;
static pulser_state_s pulser_state;
static uint8_t enable = 0;

static void pfnIntHandler();

void Pulser_Init(uint32_t sys_clock, uint32_t period_ns, uint8_t duty,
                 int8_t trimmer)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1))
        ;
    Pulser_Setup2(sys_clock, period_ns, duty, trimmer);

    PWMGenIntTrigEnable(PWM1_BASE, PWM_GEN_3, PWM_INT_CNT_AD);
    PWMGenIntRegister(PWM1_BASE, PWM_GEN_3, pfnIntHandler);
    PWMIntEnable(PWM1_BASE, PWM_INT_GEN_3);
    PWMGenEnable(PWM1_BASE, PWM_GEN_3);
    PWMOutputState( PWM1_BASE, PWM_OUT_6_BIT, true);

}
void Pulser_Setup(uint32_t sys_clock, uint32_t freq, uint8_t duty,
                  int8_t trimmer)
{
    uint32_t period = sys_clock / freq;
    uint32_t skip_reload = period / 65200;
    skip_reload++;
    uint32_t timer_period = sys_clock / skip_reload / freq;
    pulser_state.skip_reload = skip_reload - 1;
    pulser_state.duty = duty;
    skip_counter = 100;
    PWMGenConfigure(PWM1_BASE, PWM_GEN_3,
    PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, timer_period + trimmer);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, pulser_state.duty);
}

void Pulser_Setup2(uint32_t sys_clock, uint32_t period_ns, uint8_t duty,
                   int8_t trimmer)
{
    float ns_in_ticks = 1000000000.0f / (float) sys_clock;
    uint32_t total_period = (float) period_ns / ns_in_ticks;
    uint32_t skip_reload = total_period / 65300;
    skip_reload++;
    uint32_t timer_period = total_period / skip_reload;
    pulser_state.skip_reload = skip_reload - 1;
    pulser_state.duty = duty;
    skip_counter = 100;
    PWMGenConfigure(PWM1_BASE, PWM_GEN_3,
    PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, timer_period + trimmer);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, pulser_state.duty);
}

void Pulser_enable(uint8_t _enable)
{
    enable = _enable ? 1 : 0;
}

void pfnIntHandler()
{
    uint32_t status = PWMGenIntStatus(PWM1_BASE, PWM_GEN_3, 0);
    PWMGenIntClear(PWM1_BASE, PWM_GEN_3, status);
    if (skip_counter-- > 0)
    {
        PWMOutputState( PWM1_BASE, PWM_OUT_6_BIT, false);
        GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0);

    }
    else
    {
        skip_counter = pulser_state.skip_reload;
        PWMOutputState( PWM1_BASE, PWM_OUT_6_BIT, enable);
        GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, enable << 5);
    }
}

