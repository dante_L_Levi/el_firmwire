/*
 * main.c
 *
 *  Created on: 22 ��� 2022 �.
 *      Author: Lepatenko
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "settings.h"
#include "headers.h"
#include "pulser/pulser.h"
#include "pwm/pwm.h"
#include "clock/clock.h"


#define PWM_PERIOD              1000
uint32_t sys_clock=0;
typedef struct
{
    //4 bytes
    uint32_t pulse_period_ns;
    uint32_t res32[2];
    //2 bytes
    int16_t pwm_duty;
    int16_t res16[4];
    //1 byte
    uint8_t main_clock_divier;
    uint8_t pulser_duty;
    int8_t freq_correction;
    int8_t res8[5];
} settings_s;




settings_s settings;

static void TLDU_restoreDefaultSettings()
{
    settings.pulse_period_ns = 50000000;
    settings.freq_correction = -2;
    settings.main_clock_divier = 5;
    settings.pulser_duty = 1;
    settings.pwm_duty = 100;
}

void init()
{
    PinoutSet();
    TLDU_restoreDefaultSettings();

    sys_clock = Clock_Init(settings.main_clock_divier / 2,
                               settings.main_clock_divier & 1);

    Pulser_Init(sys_clock, settings.pulse_period_ns, settings.pulser_duty,
                    settings.freq_correction);
    PWM_Init(PWM_PERIOD);
    PWM_SetDuty(settings.pwm_duty);
    Pulser_enable(1);
}


void main(void)
{
    init();
    while (1)
    {

    }
}
