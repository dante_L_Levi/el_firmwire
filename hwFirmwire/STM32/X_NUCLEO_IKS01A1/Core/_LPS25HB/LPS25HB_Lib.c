/*
 * LPS25HB_Lib.c
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#include "LPS25HB_Lib.h"

LPS25_data_Def _lps25;



static uint8_t NullFunctionRecieve(uint16_t DeviceAddr, uint8_t Register)
{
	return 0;
}

static void NullFunctionTransmit(uint16_t DeviceAddr,uint8_t data, uint8_t Register)
{

}



void Set_CallBack_Read_Data_LPS25(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg))
{
	if(RecieveFunctionSet==0)
	{
		_lps25.RecieveFunctionSet=NullFunctionRecieve;
	}
	else
	{
		_lps25.RecieveFunctionSet=RecieveFunctionSet;
	}
}


void Set_CallBack_Transmit_Data_LPS25(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data))
{
	if(TransmitFunctionSet==0)
	{
		_lps25.TransmitFunctionSet=NullFunctionTransmit;
	}
	else
	{
		_lps25.TransmitFunctionSet=TransmitFunctionSet;
	}
}


/*******************Init I2C*******************/
void Init_HW_LPS25(void* callbackInit)
{
	_lps25.InitFunctionSet=callbackInit;
}


/*************************Check ID Device*******************/
uint8_t check_LPS25_ID(void)
{
	uint8_t ctrl = 0x00;
	ctrl = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_WHO_AM_I_REG);
	return ctrl;
}

/***********************Peripheral Init LPS25****************/
void LPS25_Peripheral_Settings(void)
{
	_lps25.InitFunctionSet();
	uint8_t ctrl=check_LPS25_ID();
	if(ctrl==LPS25HB_WHO_AM_I)
	{
		uint8_t value = 0;
		//пока выключим датчик (PD = 0)
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_PD_MASK;
			value|=LPS25HB_PD_POWERDOWN_MODE;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
		//Включим Data Rate 25 Гц
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_ODR_MASK;
			value|=LPS25HB_ODR_25HZ;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
		//Enaable Inteerupt Circuit
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_DIFF_EN_ENABLE;
			value|=LPS25HB_ODR_25HZ;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
		//Enable BDU
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_BDU_MASK;
			value|=LPS25HB_BDU_ENABLE;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
		//Set SPI mode 3 WIRE
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_SIM_MASK;
			value|=LPS25HB_SIM_3_WIRE;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
		//Set internal averaging sample counts for pessure and temperature
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_RES_CONF_REG);
			value&=~(LPS25HB_AVGT_MASK | LPS25HB_AVGP_MASK);
			value|=LPS25HB_AVGP_32;
			value|=LPS25HB_AVGT_16;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_RES_CONF_REG,value);
		//теперь включим датчик (PD = 1)
			value = _lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1);
			value&=~LPS25HB_PD_MASK;
			value|=LPS25HB_PD_ACTIVE_MODE;
			_lps25.TransmitFunctionSet(LPS25HB_ADDRESS,LPS25HB_CTRL_REG1,value);
	}
}


/*********************Get LPS25******************************/
int32_t LPS25_GetPressure(void)
{
	uint8_t buffer[3];
	buffer[0]=_lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_PRESS_OUT_XL_REG);
	buffer[1]=_lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_PRESS_OUT_L_REG);
	buffer[2]=_lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_PRESS_OUT_H_REG);
	for(int i=0;i<3;i++)
	{
		_lps25._Pressure.Pressure |=  (((uint32_t)buffer[i])<<(8*i));
	}
	return _lps25._Pressure.Pressure;
}


int32_t LS25_getTemperature(void)
{
	uint8_t buffer[2];
	int16_t raw_data=0;
	float raw_data_flow=0.0;
	buffer[0]=_lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_TEMP_OUT_L_REG);
	buffer[1]=_lps25.RecieveFunctionSet(LPS25HB_ADDRESS,LPS25HB_TEMP_OUT_H_REG);
	raw_data= (((uint16_t)buffer[1]<<8)+(uint16_t)buffer[0]);
	raw_data_flow=((float)raw_data)/480.0f+42.5f;
	_lps25.temperature=(int32_t)(raw_data_flow*1000.0);
	return _lps25.temperature;

}

