/*
 * Led_Conf.h
 *
 *  Created on: 27 нояб. 2021 г.
 *      Author: Lepatenko
 */

#ifndef LEDS_LED_CONF_H_
#define LEDS_LED_CONF_H_

#include "Led.h"


#define PORT_RED		 GPIOA
#define PIN_RED			 GPIO_PIN_6

#define PORT_GREEN		LED_GR_GPIO_Port
#define PIN_GREEN		LED_GR_Pin

typedef struct{
    uint8_t count;
    LED_state_e state[5];
}LED_blink_s;





#endif /* LEDS_LED_CONF_H_ */
