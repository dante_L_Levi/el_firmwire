/*
 * LSM6DS0_Lib.h
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#ifndef LSM6DS0_LSM6DS0_LIB_H_
#define LSM6DS0_LSM6DS0_LIB_H_


#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"



#pragma pack(push, 1)
typedef struct
{
	int16_t accel[3];



}Accel_Def;

typedef struct
{
	int16_t gyro[3];
}Gyro_Def;



typedef struct
{
	Accel_Def	_acceles;
	Gyro_Def	_gyroes;


	uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Register);
	void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t Register, uint8_t data);
	void (*InitFunctionSet)(void);

	float Sensivity_Gyro;
	float Sensivity_Accel;



}LSM6DS0_data_Def;
#pragma pack(pop)




#define CONFIG_ACCELEROMETTER_ENABLE					0x0A
#define CONFIG_GYROSCOPE_ENABLE							0x0B
#define CONFIG_ACC_GYRO_ENABLE 							0x0C
#define CONFIG_INT_ENABLE								0x01

#define CONFIG_ACCELEROMETTER_DISABLE					0x1A
#define CONFIG_GYROSCOPE_DISABLE						0x1B
#define CONFIG_ACC_GYRO_DISABLE							0x1C
#define CONFIG_INT_DISABLE								0x00

#define ID_DEV_LSM6DS0									0x68
#define LSM6DS0_DEV_ID									0xD6
#define LSM6DS0_WHO_I_AM								0x0F
#define CTRL_REG8										0xD6
#define LSM6DS0_ACC_GYRO_CTRL_REG5_XL   				0X1F
#define LSM6DS0_ACC_GYRO_CTRL_REG6_XL  	 				0X20
#define LSM6DS0_ACC_GYRO_CTRL_REG8    					0X22
#define LSM6DS0_ACC_GYRO_CTRL_REG1_G    				0X10
#define LSM6DS0_ACC_GYRO_CTRL_REG2_G    				0X11
#define LSM6DS0_ACC_GYRO_CTRL_REG3_C					0x12
#define LSM6DS0_ACC_GYRO_CTRL_REG4    					0X1E
//————————————————
#define LSM6DSO_ACC_GYRO_COUNTER_BDR_REG1				0x0B
#define LSM6DSO_ACC_GYRO_INT1_CTRL						0x0D

#define LSM6DS0_ACC_GYRO_BDU_DISABLE  					0x00
#define LSM6DS0_ACC_GYRO_BDU_ENABLE   					0x40
#define LSM6DS0_ACC_GYRO_BDU_MASK   					0x40
//————————————————
#define LSM6DS0_ACC_GYRO_ODR_XL_POWER_DOWN 				0x00
#define LSM6DS0_ACC_GYRO_ODR_XL_10Hz 					0x20
#define LSM6DS0_ACC_GYRO_ODR_XL_50Hz 					0x40
#define LSM6DS0_ACC_GYRO_ODR_XL_119Hz 					0x60
#define LSM6DS0_ACC_GYRO_ODR_XL_238Hz 					0x80
#define LSM6DS0_ACC_GYRO_ODR_XL_476Hz 					0xA0
#define LSM6DS0_ACC_GYRO_ODR_XL_952Hz 					0xC0
#define LSM6DS0_ACC_GYRO_ODR_XL_MASK    				0xE0
//————————————————
#define LSM6DS0_ACC_GYRO_ODR_G_POWER_DOWN        		0x00
#define LSM6DS0_ACC_GYRO_ODR_G_15Hz        				0x20
#define LSM6DS0_ACC_GYRO_ODR_G_60Hz        				0x40
#define LSM6DS0_ACC_GYRO_ODR_G_119Hz        			0x60
#define LSM6DS0_ACC_GYRO_ODR_G_238Hz        			0x80
#define LSM6DS0_ACC_GYRO_ODR_G_476Hz        			0xA0
#define LSM6DS0_ACC_GYRO_ODR_G_952Hz        			0xC0
#define LSM6DS0_ACC_GYRO_ODR_G_MASK        				0xE0
//————————————————
#define LSM6DS0_ACC_GYRO_FS_XL_2g 						0x00
#define LSM6DS0_ACC_GYRO_FS_XL_16g 						0x08
#define LSM6DS0_ACC_GYRO_FS_XL_4g 						0x10
#define LSM6DS0_ACC_GYRO_FS_XL_8g 						0x18
#define LSM6DS0_ACC_GYRO_FS_XL_MASK   					0x18
//————————————————
#define LSM6DS0_ACC_GYRO_FS_G_245dps        			0x00
#define LSM6DS0_ACC_GYRO_FS_G_500dps        			0x08
#define LSM6DS0_ACC_GYRO_FS_G_1000dps        			0x10
#define LSM6DS0_ACC_GYRO_FS_G_2000dps        			0x18
#define LSM6DS0_ACC_GYRO_FS_G_MASK        				0x18
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_SEL_BYPASS_HPF_AND_LPF2    0x00
#define LSM6DS0_ACC_GYRO_OUT_SEL_BYPASS_LPF2        	0x01
#define LSM6DS0_ACC_GYRO_OUT_SEL_USE_HPF_AND_LPF2       0x02
#define LSM6DS0_ACC_GYRO_OUT_SEL_MASK        			0x03
//————————————————
#define LSM6DS0_ACC_GYRO_BW_G_LOW        				0x00
#define LSM6DS0_ACC_GYRO_BW_G_NORMAL        			0x01
#define LSM6DS0_ACC_GYRO_BW_G_HIGH        				0x02
#define LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH        		0x03
#define LSM6DS0_ACC_GYRO_BW_G_MASK        				0x03
//————————————————
#define LSM6DS0_ACC_GYRO_XEN_XL_ENABLE 					0x08
#define LSM6DS0_ACC_GYRO_YEN_XL_ENABLE 					0x10
#define LSM6DS0_ACC_GYRO_ZEN_XL_ENABLE 					0x20
#define LSM6DS0_ACC_GYRO_XEN_XL_MASK   					0x08
#define LSM6DS0_ACC_GYRO_YEN_XL_MASK  					0x10
#define LSM6DS0_ACC_GYRO_ZEN_XL_MASK   					0x20
//————————————————
#define LSM6DS0_ACC_GYRO_XEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_XEN_G_ENABLE        			0x08
#define LSM6DS0_ACC_GYRO_YEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_YEN_G_ENABLE        			0x10
#define LSM6DS0_ACC_GYRO_ZEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_ZEN_G_ENABLE        			0x20
#define LSM6DS0_ACC_GYRO_XEN_G_MASK        				0x08
#define LSM6DS0_ACC_GYRO_YEN_G_MASK        				0x10
#define LSM6DS0_ACC_GYRO_ZEN_G_MASK        				0x20
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_X_L_XL   					0X28
#define LSM6DS0_ACC_GYRO_OUT_X_H_XL   					0X29
#define LSM6DS0_ACC_GYRO_OUT_Y_L_XL   					0X2A
#define LSM6DS0_ACC_GYRO_OUT_Y_H_XL   					0X2B
#define LSM6DS0_ACC_GYRO_OUT_Z_L_XL   					0X2C
#define LSM6DS0_ACC_GYRO_OUT_Z_H_XL   					0X2D
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_X_L_G    					0X18
#define LSM6DS0_ACC_GYRO_OUT_X_H_G    					0X19
#define LSM6DS0_ACC_GYRO_OUT_Y_L_G    					0X1A
#define LSM6DS0_ACC_GYRO_OUT_Y_H_G    					0X1B
#define LSM6DS0_ACC_GYRO_OUT_Z_L_G    					0X1C
#define LSM6DS0_ACC_GYRO_OUT_Z_H_G    					0X1D
//==================================================================






#define AXES_X	0
#define AXES_Y	1
#define AXES_Z	2



void Set_CallBack_Read_Data(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg));
void Set_CallBack_Transmit_Data(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data));


/*******************Init I2C*******************/
void Init_HW_LSM6DS0(void* callbackInit);
/*************************Check ID Device*******************/
uint8_t check_LSM6DS0_ID(void);
/*********************Get Gyro LSM6DS0******************************/
void LSM6DS0_Gyro_GetXYZ(int16_t *Data);
/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Accel_GetXYZ(int16_t *Data);
/***************************Init Gyroscope & Accel*************************/
void Init_LSM6DS0(uint8_t configAccelGyro,uint8_t Freq,uint8_t Range,uint8_t dps,uint8_t Int_config);
/***************************Reset Gyroscope & Accel*************************/
void LSM6DS0_Reset(void);

float Get_Sensivity_Gyro(void);
float Get_Sensivity_Accel(void);





#endif /* LSM6DS0_LSM6DS0_LIB_H_ */
