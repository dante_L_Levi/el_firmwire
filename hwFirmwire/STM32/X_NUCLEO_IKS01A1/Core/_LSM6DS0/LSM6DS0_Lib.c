/*
 * LSM6DS0_Lib.c
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */
#include "LSM6DS0_Lib.h"


LSM6DS0_data_Def maindata_Lsm6DS0;

static void Select_Sensivity_Accel(uint8_t Range)
{
	/*
	 * #define LSM6DS0_ACC_GYRO_FS_XL_2g 						0x00
		#define LSM6DS0_ACC_GYRO_FS_XL_16g 						0x08
		#define LSM6DS0_ACC_GYRO_FS_XL_4g 						0x10
		#define LSM6DS0_ACC_GYRO_FS_XL_8g 						0x18
	 *
	 *
	 */

	switch(Range)
	{
		case LSM6DS0_ACC_GYRO_FS_XL_2g:
		{
			maindata_Lsm6DS0.Sensivity_Accel=0.061;
			break;
		}

		case LSM6DS0_ACC_GYRO_FS_XL_4g:
		{
			maindata_Lsm6DS0.Sensivity_Accel=0.122;
			break;
		}

		case LSM6DS0_ACC_GYRO_FS_XL_8g:
		{
			maindata_Lsm6DS0.Sensivity_Accel=0.244;
			break;
		}

		case LSM6DS0_ACC_GYRO_FS_XL_16g:
		{
			maindata_Lsm6DS0.Sensivity_Accel=0.488;
			break;
		}

		default:
			maindata_Lsm6DS0.Sensivity_Accel=1;
			break;


	}
}

static void Select_Sensivity_Gyro(uint8_t Range)
{

	/*
	 * #define LSM6DS0_ACC_GYRO_FS_G_245dps        			0x00
		#define LSM6DS0_ACC_GYRO_FS_G_500dps        			0x08
		#define LSM6DS0_ACC_GYRO_FS_G_1000dps        			0x10
		#define LSM6DS0_ACC_GYRO_FS_G_2000dps        			0x18
	 *
	 *
	 */
	switch(Range)
		{
			case LSM6DS0_ACC_GYRO_FS_G_245dps:
			{
				maindata_Lsm6DS0.Sensivity_Gyro=8.75;
				break;
			}

			case LSM6DS0_ACC_GYRO_FS_G_500dps:
			{
				maindata_Lsm6DS0.Sensivity_Gyro=17.500;
				break;
			}

			case LSM6DS0_ACC_GYRO_FS_G_1000dps:
			{
				maindata_Lsm6DS0.Sensivity_Gyro=35.0;
				break;
			}

			case LSM6DS0_ACC_GYRO_FS_G_2000dps:
			{
				maindata_Lsm6DS0.Sensivity_Gyro=70;
				break;
			}

			default:
				maindata_Lsm6DS0.Sensivity_Gyro=1;
				break;


		}
}

/******************Init Register Accel LSM6DS0*************************/
static void LSM6DS0_ACCEL_Init_HRW(uint8_t Freq,uint8_t Range)
{
	uint8_t value=0;
			value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG8);
			value &= ~LSM6DS0_ACC_GYRO_BDU_MASK;
			value |= LSM6DS0_ACC_GYRO_BDU_ENABLE;
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG8, value);

			value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
			value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
			value |= LSM6DS0_ACC_GYRO_ODR_XL_POWER_DOWN;
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

			//Full scale selection
			value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
		    value &= ~LSM6DS0_ACC_GYRO_FS_XL_MASK;
			value |= Range;
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);
			Select_Sensivity_Accel(Range);

			value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG5_XL);
			value &= ~(LSM6DS0_ACC_GYRO_XEN_XL_MASK |
		               LSM6DS0_ACC_GYRO_YEN_XL_MASK |
		               LSM6DS0_ACC_GYRO_ZEN_XL_MASK);
			value |= (LSM6DS0_ACC_GYRO_XEN_XL_ENABLE |
		              LSM6DS0_ACC_GYRO_YEN_XL_ENABLE |
		              LSM6DS0_ACC_GYRO_ZEN_XL_ENABLE);
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG5_XL, value);



			value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
			value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
			value |= Freq;
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);
}
/******************Init Register Gyroscope LSM6DS0*************************/
static void LSM6DS0_GYRO_Init_HRW(uint8_t Freq,uint8_t Range)
{
	uint8_t value = 0;
	    value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
	    value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
	    value|=LSM6DS0_ACC_GYRO_ODR_G_POWER_DOWN;
	    maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
	    value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
	    value&=~LSM6DS0_ACC_GYRO_FS_G_MASK;
	    value|=Range;
	    maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
	    Select_Sensivity_Gyro(Range);
		    //Включим оси
	   	value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG4);
	   	value&=~(LSM6DS0_ACC_GYRO_XEN_G_ENABLE|\
	 					 LSM6DS0_ACC_GYRO_YEN_G_ENABLE|\
	   					 LSM6DS0_ACC_GYRO_ZEN_G_ENABLE);
	   	value|=(LSM6DS0_ACC_GYRO_XEN_G_MASK|\
	    					LSM6DS0_ACC_GYRO_YEN_G_MASK|\
	    					LSM6DS0_ACC_GYRO_ZEN_G_MASK);
	   	maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG4,value);
	   //ON HPF и LPF2 (Filters)
	    value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG2_G);
	    value&=~LSM6DS0_ACC_GYRO_OUT_SEL_MASK;
	    value|=LSM6DS0_ACC_GYRO_OUT_SEL_USE_HPF_AND_LPF2;
	    maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG2_G,value);
		    //capacity
	    uint8_t capacity=0;
	    if(Freq==LSM6DS0_ACC_GYRO_ODR_G_15Hz||Freq==LSM6DS0_ACC_GYRO_ODR_G_60Hz )
	    {
	   	capacity=LSM6DS0_ACC_GYRO_BW_G_LOW;
	    }
	    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_119Hz)
	     {
	    	capacity=LSM6DS0_ACC_GYRO_BW_G_NORMAL;
	     }
	    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_238Hz)
	     {
	    	capacity=LSM6DS0_ACC_GYRO_BW_G_HIGH;
	     }
	    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_476Hz)
	     {
	   	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
	      }
	    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_952Hz)
	      {
	    	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
	      }
	    value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
	    value&=~LSM6DS0_ACC_GYRO_BW_G_MASK;
	    value|=capacity;
	    maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
		    //Freq
	    value = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
	    value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
	    value|=Freq;
	    maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
}








static uint8_t NullFunctionRecieve(uint16_t DeviceAddr, uint8_t Register)
{
	return 0;
}

static void NullFunctionTransmit(uint16_t DeviceAddr,uint8_t data, uint8_t Register)
{

}


void Set_CallBack_Read_Data(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Register))
{
	if(RecieveFunctionSet==0)
	{
		maindata_Lsm6DS0.RecieveFunctionSet=NullFunctionRecieve;
	}
	else
	{
		maindata_Lsm6DS0.RecieveFunctionSet=RecieveFunctionSet;
	}

}
void Set_CallBack_Transmit_Data(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t Register, uint8_t Data))
{
	if(TransmitFunctionSet==0)
	{
		maindata_Lsm6DS0.TransmitFunctionSet=NullFunctionTransmit;
	}
	else
	{
		maindata_Lsm6DS0.TransmitFunctionSet=TransmitFunctionSet;
	}

}

/*******************Init I2C*******************/
void Init_HW_LSM6DS0(void* callbackInit)
{
	maindata_Lsm6DS0.InitFunctionSet=callbackInit;

}

/*************************Check ID Device*******************/
uint8_t check_LSM6DS0_ID(void)
{
	uint8_t ctrl = 0x00;
	ctrl=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_WHO_I_AM);
	return ctrl;
}

/*********************Get Gyro LSM6DS0******************************/
void LSM6DS0_Gyro_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_G);
	buffer[1]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_G);
	buffer[2]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_G);
	buffer[3]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_G);
	buffer[4]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_G);
	buffer[5]=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_G);
	for(i=0;i<3;i++)
	{
		maindata_Lsm6DS0._gyroes.gyro[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}
	Data[0]=maindata_Lsm6DS0._gyroes.gyro[AXES_X];
	Data[1]=maindata_Lsm6DS0._gyroes.gyro[AXES_Y];
	Data[2]=maindata_Lsm6DS0._gyroes.gyro[AXES_Z];

}
/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Accel_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_XL);
	buffer[1] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_XL);
	buffer[2] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_XL);
	buffer[3] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_XL);
	buffer[4] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_XL);
	buffer[5] = maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_XL);
	for(i=0;i<3;i++)
	{
		maindata_Lsm6DS0._acceles.accel[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}
	Data[0]=maindata_Lsm6DS0._acceles.accel[AXES_X];
	Data[1]=maindata_Lsm6DS0._acceles.accel[AXES_Y];
	Data[2]=maindata_Lsm6DS0._acceles.accel[AXES_Z];

}

/***************************Init Gyroscope & Accel*************************/
void Init_LSM6DS0(uint8_t configAccelGyro,uint8_t Freq,uint8_t Range,uint8_t dps,uint8_t Int_config)
{
	maindata_Lsm6DS0.InitFunctionSet();
	if(check_LSM6DS0_ID()==ID_DEV_LSM6DS0)
	{
		switch(configAccelGyro)
		{
			case CONFIG_ACCELEROMETTER_ENABLE://Enable only Accel
			{
				LSM6DS0_ACCEL_Init_HRW(Freq,Range);
				break;
			}
			case CONFIG_GYROSCOPE_ENABLE://Enable only Gyroscope
			{
				LSM6DS0_GYRO_Init_HRW(Freq,dps);
				break;
			}
			case CONFIG_ACC_GYRO_ENABLE://ALL
			{
				LSM6DS0_ACCEL_Init_HRW(Freq,Range);
				LSM6DS0_GYRO_Init_HRW(Freq,dps);
				break;
			}
			default:
			{
				break;
			}
		}

		if(Int_config==1)
		{
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DSO_ACC_GYRO_COUNTER_BDR_REG1,0x80);
			maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DSO_ACC_GYRO_INT1_CTRL,0x03);
		}


	}
}


/***************************Reset Gyroscope & Accel*************************/
void LSM6DS0_Reset(void)
{
	uint8_t temp=maindata_Lsm6DS0.RecieveFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG3_C);
	maindata_Lsm6DS0.TransmitFunctionSet(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG3_C,temp|0x01);//reset

}

float Get_Sensivity_Gyro(void)
{
	return maindata_Lsm6DS0.Sensivity_Gyro;
}
float Get_Sensivity_Accel(void)
{
	return maindata_Lsm6DS0.Sensivity_Accel;
}







