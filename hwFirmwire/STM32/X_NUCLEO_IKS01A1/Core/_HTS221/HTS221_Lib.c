/*
 * HTS221_Lib.c
 *
 *  Created on: Feb 8, 2022
 *      Author: Lepatenko
 */


#include "HTS221_Lib.h"


HTS221_MainData_Def maindata_HTS221;


static uint8_t NullFunctionRecieve(uint16_t DeviceAddr, uint8_t Register)
{
	return 0;
}

static void NullFunctionTransmit(uint16_t DeviceAddr,uint8_t data, uint8_t Register)
{

}


void Set_CallBack_Read_Data_HTS221(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg))
{
	if(RecieveFunctionSet==0)
		{
			maindata_HTS221.RecieveFunctionSet=NullFunctionRecieve;
		}
		else
		{
			maindata_HTS221.RecieveFunctionSet=RecieveFunctionSet;
		}
}


void Set_CallBack_Transmit_Data_HTS221(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data))
{
	if(TransmitFunctionSet==0)
		{
			maindata_HTS221.TransmitFunctionSet=NullFunctionTransmit;
		}
		else
		{
			maindata_HTS221.TransmitFunctionSet=TransmitFunctionSet;
		}
}



/*******************Init I2C*******************/
void Init_HW_HTS221(void* callbackInit)
{
	maindata_HTS221.InitFunctionSet=callbackInit;
}

/*************************Check ID Device*******************/
uint8_t check_HTS221_ID(void)
{
	uint8_t ctrl = 0x00;
	ctrl = maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_WHO_AM_I_REG);
	return ctrl;
}


/*********************Get Gyro HTS221******************************/
void HTS221_Get_Huminity(float *Data)
{

		uint8_t buffer[2];
		int16_t H0_T0_out, H1_T0_out, H_T_out;
		int16_t H0_rh, H1_rh;
		float tmp_f;
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H0_RH_X2);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H1_RH_X2);
		H0_rh = buffer[0] >> 1;
		H1_rh = buffer[1] >> 1;
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H0_T0_OUT_L);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H0_T0_OUT_H);
		H0_T0_out = (((uint16_t)buffer[1])<<8)|((uint16_t)buffer[0]);
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H1_T0_OUT_L);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_H1_T0_OUT_H);
		H1_T0_out = (((uint16_t)buffer[1])<<8)|((uint16_t)buffer[0]);
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_HR_OUT_L_REG);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_HR_OUT_H_REG);
		H_T_out = (((uint16_t)buffer[1])<<8)|((uint16_t)buffer[0]);

		tmp_f = (float)(H_T_out - H0_T0_out) * (float)(H1_rh - H0_rh) /\
		(float)(H1_T0_out - H0_T0_out) + H0_rh;
		*Data = (tmp_f>100.0f)?100.0f
			:(tmp_f<0.0f)?0.0f
			:tmp_f;
}

/*********************Get Accel HTS221******************************/
void HTS221_Get_Temperature(float *Data)
{

		int16_t T0_degC,T1_degC;
		int16_t T0_out, T1_out, T_out, T0_degC_x8_u16, T1_degC_x8_u16;
		uint8_t buffer[4],tmp;
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T0_DEGC_X8);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T1_DEGC_X8);
		tmp=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T0_T1_DEGC_H2);
		T0_degC_x8_u16 = (((uint16_t)(tmp & 0x03)) << 8) | ((uint16_t)buffer[0]);
		T1_degC_x8_u16 = (((uint16_t)(tmp & 0x0C)) << 6) | ((uint16_t)buffer[1]);
		T0_degC = T0_degC_x8_u16 >> 3;
		T1_degC = T1_degC_x8_u16 >> 3;
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T0_OUT_L);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T0_OUT_H);
		buffer[2]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T1_OUT_L);
		buffer[3]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_T1_OUT_H);
		T0_out = (((uint16_t)buffer[1])<<8)|((uint16_t)buffer[0]);
		T1_out = (((uint16_t)buffer[3])<<8)|((uint16_t)buffer[2]);
		buffer[0]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_TEMP_OUT_L_REG);
		buffer[1]=maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_TEMP_OUT_H_REG);
		T_out = (((uint16_t)buffer[1])<<8)|((uint16_t)buffer[0]);
		T_out = (((uint16_t)buffer[1]<<8)+(uint16_t)buffer[0]);


		*Data = (float)(T_out - T0_out) * (float)(T1_degC - T0_degC) /\
			(float)(T1_out - T0_out) + T0_degC;
}

/***************************Init Huminity & Temperature*************************/
void Init_HTS221(uint8_t Data_Rate)
{
	maindata_HTS221.InitFunctionSet();
	if(check_HTS221_ID()==HTS221_WHO_AM_I_VAL)
	{
		uint8_t value = 0;
		//пока выключим датчик (PD = 0)
			value = maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1);
			value&=~HTS221_PD_MASK;
			value|=HTS221_PD_POWERDOWN_MODE;
			maindata_HTS221.TransmitFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1,value);
		//Enable BDU
			value = maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1);
			value&=~HTS221_BDU_MASK;
			value|=HTS221_BDU_ENABLE;
			maindata_HTS221.TransmitFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1,value);
		//Включим Data Rate 12.5 Гц
			value = maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1);
			value&=~HTS221_ODR_MASK;
			value|=Data_Rate;
			maindata_HTS221.TransmitFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1,value);
		//Теперь включим датчик (PD = 1)
			value = maindata_HTS221.RecieveFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1);
			value&=~HTS221_PD_MASK;
			value|=HTS221_PD_ACTIVE_MODE;
			maindata_HTS221.TransmitFunctionSet(HTS221_ADDRESS,HTS221_CTRL_REG1,value);
	}
}

/***************************Reset Huminity & Temperature*************************/
void HTS221_Reset(void)
{

}

