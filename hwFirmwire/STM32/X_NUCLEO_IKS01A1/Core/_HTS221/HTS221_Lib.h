/*
 * HTS221_Lib.h
 *
 *  Created on: Feb 8, 2022
 *      Author: Lepatenko
 */

#ifndef HTS221_HTS221_LIB_H_
#define HTS221_HTS221_LIB_H_


#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

//------------------------------------------------
#define ABS(x)         (x < 0) ? (-x) : x
//------------------------------------------------


#define	HTS221_ADDRESS				0xBE
//------------------------------------------------
#define	HTS221_WHO_AM_I_REG			0x0F
#define	HTS221_CTRL_REG1			0x20
//------------------------------------------------
#define	HTS221_WHO_AM_I_VAL			0xBC
//------------------------------------------------
#define	HTS221_PD_ACTIVE_MODE		0x80
#define	HTS221_PD_POWERDOWN_MODE	0x00
#define	HTS221_PD_MASK				0x80
//------------------------------------------------
#define	HTS221_ODR_ONE_SHOT			0x00
#define	HTS221_ODR_1HZ				0x01
#define	HTS221_ODR_7HZ				0x02
#define	HTS221_ODR_12_5HZ			0x03
#define	HTS221_ODR_MASK				0x03
//------------------------------------------------
#define	HTS221_BDU_DISABLE			0x00
#define	HTS221_BDU_ENABLE			0x04
#define	HTS221_BDU_MASK				0x04
//------------------------------------------------
#define HTS221_HR_OUT_L_REG			0x28
#define HTS221_HR_OUT_H_REG			0x29
#define	HTS221_H0_RH_X2				0x30
#define	HTS221_H1_RH_X2				0x31
#define	HTS221_T0_DEGC_X8			0x32
#define	HTS221_T1_DEGC_X8			0x33
#define	HTS221_T0_T1_DEGC_H2		0x35
#define	HTS221_H0_T0_OUT_L			0x36
#define	HTS221_H0_T0_OUT_H			0x37
#define	HTS221_H1_T0_OUT_L			0x3A
#define	HTS221_H1_T0_OUT_H			0x3B
#define	HTS221_T0_OUT_L				0x3C
#define	HTS221_T0_OUT_H				0x3D
#define	HTS221_T1_OUT_L				0x3E
#define	HTS221_T1_OUT_H				0x3F
#define HTS221_TEMP_OUT_L_REG		0x2A
#define HTS221_TEMP_OUT_H_REG		0x2B
//------------------------------------------------







#pragma pack(push, 1)
typedef struct
{
	int32_t humanity;
	float Temperature;
}Data_HTS221;


typedef struct
{

	Data_HTS221 _data;
	uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Register);
	void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t Register, uint8_t data);
	void (*InitFunctionSet)(void);


}HTS221_MainData_Def;
#pragma pack(pop)



void Set_CallBack_Read_Data_HTS221(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg));
void Set_CallBack_Transmit_Data_HTS221(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data));


/*******************Init I2C*******************/
void Init_HW_HTS221(void* callbackInit);
/*************************Check ID Device*******************/
uint8_t check_HTS221_ID(void);
/*********************Get Gyro HTS221******************************/
void HTS221_Get_Huminity(float *Data);
/*********************Get Accel HTS221******************************/
void HTS221_Get_Temperature(float *Data);
/***************************Init Huminity & Temperature*************************/
void Init_HTS221(uint8_t Data_Rate);
/***************************Reset Huminity & Temperature*************************/
void HTS221_Reset(void);




#endif /* HTS221_HTS221_LIB_H_ */
