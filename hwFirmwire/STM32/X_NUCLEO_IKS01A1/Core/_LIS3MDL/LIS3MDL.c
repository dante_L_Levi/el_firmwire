/*
 * LIS3MDL.c
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#include "LIS3MDL.h"


LIS3MDL_data_Def		_lis3mdl;

static void Select_Sensivity_Magn(uint8_t Range)
{
	/*
	 *  #define LIS3MDL_MAG_FS_4Ga        			0x00
		#define LIS3MDL_MAG_FS_8Ga        			0x20
		#define LIS3MDL_MAG_FS_12Ga        			0x40
		#define LIS3MDL_MAG_FS_16Ga        			0x60
	 *
	 *
	 */

	switch(Range)
	{
		case LIS3MDL_MAG_FS_4Ga:
		{
			_lis3mdl.Sensivity_Magn=6842;
			break;
		}

		case LIS3MDL_MAG_FS_8Ga:
		{
			_lis3mdl.Sensivity_Magn=3421;
			break;
		}

		case LIS3MDL_MAG_FS_12Ga:
		{
			_lis3mdl.Sensivity_Magn=2281;
			break;
		}

		case LIS3MDL_MAG_FS_16Ga:
		{
			_lis3mdl.Sensivity_Magn=1711;
			break;
		}

		default:
			_lis3mdl.Sensivity_Magn=1;
			break;


	}
}



static uint8_t NullFunctionRecieve(uint16_t DeviceAddr, uint8_t Register)
{
	return 0;
}

static void NullFunctionTransmit(uint16_t DeviceAddr,uint8_t data, uint8_t Register)
{

}



void Set_CallBack_Read_Data_LIS3MDL(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg))
{
	if(RecieveFunctionSet==0)
	{
		_lis3mdl.RecieveFunctionSet=NullFunctionRecieve;
	}
	else
	{
		_lis3mdl.RecieveFunctionSet=RecieveFunctionSet;
	}
}


void Set_CallBack_Transmit_Data_LIS3MDL(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data))
{
	if(TransmitFunctionSet==0)
	{
		_lis3mdl.TransmitFunctionSet=NullFunctionTransmit;
	}
	else
	{
		_lis3mdl.TransmitFunctionSet=TransmitFunctionSet;
	}
}



/*******************Init I2C*******************/
void Init_HW_LIS3MDL(void* callbackInit)
{
	_lis3mdl.InitFunctionSet=callbackInit;
}


/*************************Check ID Device*******************/
uint8_t check_LIS3MDL_ID(void)
{
	uint8_t ctrl = 0x00;
	ctrl=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_WHO_AM_I_REG);
	return ctrl;
}

/*********************Get Gyro LSM6DS0******************************/
void LIS3MDL_Magn_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTX_L);
	buffer[1]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTX_H);
	buffer[2]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTY_L);
	buffer[3]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTY_H);
	buffer[4]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTZ_L);
	buffer[5]=_lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTZ_H);
	for(i=0;i<3;i++)
	{
		_lis3mdl._Magn.Magn[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}

	Data[0]=_lis3mdl._Magn.fields.GX;
	Data[1]=_lis3mdl._Magn.fields.GY;
	Data[2]=_lis3mdl._Magn.fields.GZ;

}

/***********************Peripheral Init LIS3MDL****************/
void LIS3MDL_Peripheral_Settings(uint8_t Freq,uint8_t scale,
		uint8_t perfomenceModeXY,uint8_t temp_sensor)
{
	_lis3mdl.InitFunctionSet();
	uint8_t key=check_LIS3MDL_ID();
	if(key==LIS3MDL_ID_DEV)
	{

		uint8_t value=0x00;
			//Sensor OFF
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3);
		value&=~LIS3MDL_MAG_MD_MASK;
		value|=LIS3MDL_MAG_MD_POWER_DOWN;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3,value);
			//______________________________________________________________
			//Enable BDU Block Data Update
		value=0x00;
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG5);
		value&=~LIS3MDL_MAG_BDU_MASK;
		value|=LIS3MDL_MAG_BDU_ENABLE;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG5,value);

			//______________________________________________________________
			//ON FREQ SCAN
		value=0x00;
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
		value&=~LIS3MDL_MAG_DO_MASK;
		value|=Freq;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
			 //______________________________________________________________
			 //Full scale selection  gauss
		value=0x00;
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG2);
		value&=~LIS3MDL_MAG_FS_MASK;
		value|=scale;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG2,value);
		Select_Sensivity_Magn(scale);
			 //______________________________________________________________
			 //ON HIGH perphomence Mode XY
		value=0x00;
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
		value&=~LIS3MDL_MAG_OM_MASK;
		value|=perfomenceModeXY;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
			//______________________________________________________________
			 //Sensor temperature
		value=0x00;
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
		value&=~temp_sensor;
		value|=LIS3MDL_MAG_OM_HIGH;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
			 //______________________________________________________________
			 //ON sensor (MD = 0x00)
		value = _lis3mdl.RecieveFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3);
		value&=~LIS3MDL_MAG_MD_MASK;
		value|=LIS3MDL_MAG_MD_CONTINUOUS;
		_lis3mdl.TransmitFunctionSet(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3,value);
	}
}

