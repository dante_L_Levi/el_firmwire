/*
 * Settings.h
 *
 *  Created on: Dec 5, 2021
 *      Author: Lepatenko
 */

#ifndef INC_SETTINGS_H_
#define INC_SETTINGS_H_

#include "Headers.h"

#define SYS_CLOCK			72000000
#define FAST_LOOP			Sync_4000Hz

#define FAST2_LOOP			Sync_1000Hz

#define FAST_LOOP_SPEED		4000
#define MIDDLE_LOOP_SPEED	100


#define CAN_BAUDRATE            1000000
#define RS485_BAUDRATE          921600

#define FLASH_STORAGE 			0x0800F000


#define CAN_ID_1_Main			0x22
#define CAN_ID_2_Main			0x32



static  const uint8_t _board_id[4] = { 0x01, 0xF6, 'T', 'B' };


static const char model[8] = "IKS01A1 ";
static const char firmware[8] = "0.0.1  ";






#endif /* INC_SETTINGS_H_ */
