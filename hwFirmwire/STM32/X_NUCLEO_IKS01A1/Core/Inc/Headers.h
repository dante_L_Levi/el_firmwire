/*
 * Headers.h
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef INC_HEADERS_H_
#define INC_HEADERS_H_


#include "stm32f3xx_hal.h"
#include "stm32f334x8.h"


#include "usart.h"
#include "gpio.h"
#include "i2c.h"


#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"


//#include "line_buffer.h"
//#include "ring_buffer.h"


#include "Led.h"
#include "RS485_Lib.h"
#include "Timers.h"
#include "LSM6DS0_Lib.h"
#include "LIS3MDL.h"
#include "LPS25HB_Lib.h"
#include "HTS221_Lib.h"
//#include "digital_pins.h"
//#include "ADC_Sense.h"
//#include "DAC_Transfer.h"
//#include "pwm_generate.h"
//#include "PID_PWR.h"
//#include "Flash_Work.h"
//#include "Settings.h"
#include "HAL_Helpers.h"
//#include "CAN_Lib.h"
#include "Timeout.h"



#endif /* INC_HEADERS_H_ */
