/*
 * IKS01A1_Logic.h
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#ifndef INC_IKS01A1_LOGIC_H_
#define INC_IKS01A1_LOGIC_H_

#include "Headers.h"
#include "Settings.h"

#pragma pack(push, 1)
typedef union
{
	struct
	{
		int16_t GX;
		int16_t GY;
		int16_t GZ;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Gyro_hw_s;

typedef union
{
	struct
	{
		int16_t AX;
		int16_t AY;
		int16_t AZ;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Accel_hw_s;


typedef union
{
	struct
	{
		int16_t MX;
		int16_t MY;
		int16_t MZ;
	}fields;
	int16_t AllData[3];
}LIS3MDL_Magn_hw_s;



typedef struct
{
	LSM6DS0_Gyro_hw_s  _dataGyro;
	LSM6DS0_Accel_hw_s _dataAccel;


}LSM6DS0_Data_s;

typedef struct
{
	int16_t biasAccel[3];
	int16_t biasGyro[3];
}Settings_LSM6DS0;

typedef struct
{
	LIS3MDL_Magn_hw_s _dataMagn;
	int16_t Bias[3];

}LIS3MDL_Magn_Data_s;

typedef struct
{
	int32_t Pressure;
}LPS25HB_Data_s;

typedef struct
{
	float Humidity;
	float Temperature;

}HT221_Data_s;


typedef struct
{
	uint16_t ID_temp;
	uint8_t Name[8];
	uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;

typedef struct
{
	LSM6DS0_Data_s               				_lsm6ds0;
	LIS3MDL_Magn_hw_s				            _LIS3MDL;
	LPS25HB_Data_s				                _LPS25HB;
	HT221_Data_s							    _Hts221;

}Transmit_data2_Model_MainData;


#define COUNT_PERIPHERAL			3
typedef struct
{
    float Kp;
    float Ki;
    float Kd;
    float Saturation_P;
    float Saturation_M;
}Settings_def;

typedef struct
{
    Settings_def        dataSettings[COUNT_PERIPHERAL];

}Transmit_data3_Model_Settings;





typedef struct
{
	uint16_t ID;

	LSM6DS0_Data_s		_dataLSM6DS0;
	LSM6DS0_Gyro_hw_s  	_dataGyro;
	LSM6DS0_Accel_hw_s 	_dataAccel;
	Settings_LSM6DS0	_dataSettingsLSM6DS0;

	LIS3MDL_Magn_Data_s		_dataLIS3;
	LIS3MDL_Magn_hw_s		_dataMagn;

	LPS25HB_Data_s			_dataLPS25HB;
	HT221_Data_s			_dataHT221;

	Settings_def        dataSettings[COUNT_PERIPHERAL];

	uint8_t 					sync_trigger;
	uint32_t 					_time_ms;



	Transmit_data1_Model_DataBoard	_transmitData1;
	Transmit_data2_Model_MainData	_transmitData2;
	Transmit_data3_Model_Settings	_transmitData3;




}work_state_model_s;

#pragma pack(pop)



void Main_Init(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
void IKS01A1_Sync(void);





#endif /* INC_IKS01A1_LOGIC_H_ */
