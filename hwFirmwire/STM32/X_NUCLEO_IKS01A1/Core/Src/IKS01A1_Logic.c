/*
 * IKS01A1_Logic.c
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#include "IKS01A1_Logic.h"

static void Fast_Loop_IKS01A1_Sync(void);
static void Middle_Loop_IKS01A1_Sync(void);

static void Read_LSM6DS0(void);
static void Read_LIS3MDL(void);
static void Read_LPS25(void);
static void Read_HT221(void);

static  const uint16_t * rs485_id =  (const uint16_t *)_board_id;


work_state_model_s _model_state;


void Update_sync(void)
{
	_model_state.sync_trigger=1;
}



void Main_Init(void)
{
	 MX_GPIO_Init();
	 Set_Period_Toggle(500);
	 LED_Init();


	 //LSM6DS0
	 Init_HW_LSM6DS0(MX_I2C1_Init);
	 Set_CallBack_Read_Data(I2C1_IO_Read);
	 Set_CallBack_Transmit_Data(I2C1_IO_Write);
	 Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,LSM6DS0_ACC_GYRO_ODR_G_952Hz,LSM6DS0_ACC_GYRO_FS_XL_16g,
			 LSM6DS0_ACC_GYRO_FS_G_2000dps,0);


	 //LIS3MDL
	 Init_HW_LIS3MDL(MX_I2C1_Init);
	 Set_CallBack_Transmit_Data_LIS3MDL(I2C1_IO_Write);
	 Set_CallBack_Read_Data_LIS3MDL(I2C1_IO_Read);
	 LIS3MDL_Peripheral_Settings(LIS3MDL_MAG_DO_80Hz,LIS3MDL_MAG_FS_4Ga,
							LIS3MDL_MAG_OM_HIGH,LIS3MDL_MAG_TEMP_EN_DISABLE);

	 //LPS25
	 Init_HW_LPS25(MX_I2C1_Init);
	 Set_CallBack_Read_Data_LPS25(I2C1_IO_Read);
	 Set_CallBack_Transmit_Data_LPS25(I2C1_IO_Write);
	 LPS25_Peripheral_Settings();

	 //HTS221
	 Init_HW_HTS221(MX_I2C1_Init);
	 Set_CallBack_Read_Data_HTS221(I2C1_IO_Read);
	 Set_CallBack_Transmit_Data_HTS221(I2C1_IO_Write);
	 Init_HTS221(HTS221_ODR_12_5HZ);

	 _model_state.ID=*rs485_id;
	 RS485_SProtocol_init(*rs485_id,RS485_BAUDRATE,*RS485_dataTX);
	 RS485_Reciever_Helper();

	 Init_Tim6_Sync(FAST_LOOP);
	 Set_CallBack(6,Update_sync);

}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}




void IKS01A1_Sync(void)
{
	Fast_Loop_IKS01A1_Sync();
	Middle_Loop_IKS01A1_Sync();
}










static void Fast_Loop_IKS01A1_Sync(void)
{
	Read_LSM6DS0();
	Read_LIS3MDL();

}

static void Middle_Loop_IKS01A1_Sync(void)
{
	static int32_t skip_counter = 1;
		    if (skip_counter--)
		        return;
		    skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;

    Read_LPS25();
	Read_HT221();
	//RS485_Self_Test();

}

static void Read_LSM6DS0(void)
{
	LSM6DS0_Gyro_GetXYZ(_model_state._dataGyro.AllData);
	for(int i=0;i<3;i++)
	{
		_model_state._dataLSM6DS0._dataGyro.AllData[i]=_model_state._dataGyro.AllData[i]-
				_model_state._dataSettingsLSM6DS0.biasGyro[i];
	}

	LSM6DS0_Accel_GetXYZ(_model_state._dataAccel.AllData);
	for(int i=0;i<3;i++)
	{
		_model_state._dataLSM6DS0._dataAccel.AllData[i]=_model_state._dataAccel.AllData[i]-
				_model_state._dataSettingsLSM6DS0.biasAccel[i];
	}

}
static void Read_LIS3MDL(void)
{
	LIS3MDL_Magn_GetXYZ(_model_state._dataMagn.AllData);
	for(int i=0;i<3;i++)
	{
		_model_state._dataLIS3._dataMagn.AllData[i]=_model_state._dataMagn.AllData[i]-_model_state._dataLIS3.Bias[i];
	}

}
static void Read_LPS25(void)
{
	_model_state._dataLPS25HB.Pressure=LPS25_GetPressure();
}
static void Read_HT221(void)
{
	HTS221_Get_Huminity(&_model_state._dataHT221.Humidity);
	HTS221_Get_Temperature(&_model_state._dataHT221.Temperature);
}



typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,
    Get_main_data,
    Get_setting_peripheral_board,

    Set_setting_peripheral_board,
    Set_Default_peripheral_board,
    Set_cmd_flash

}Command_Board;


static void function_Set_Connect(uint8_t *data)
{
	Set_Period_Toggle(1000);
}

static void function_Get_Board(uint8_t *data)
{
	_model_state._transmitData1.ID_temp=_model_state.ID;
	memcpy(_model_state._transmitData1.Name,model,8);
	memcpy(_model_state._transmitData1.firmwire,firmware,8);

	RS485_Sprotocol_Data_def msg;
	msg.cmd=Get_Board;
	msg.length=sizeof(Transmit_data1_Model_DataBoard);
	msg.data=(uint8_t*)&_model_state._transmitData1;
	SProtocol_addTxMessage(&msg);

}

static void function_Get_main_data(uint8_t *data)
{
	_model_state._transmitData2._Hts221=_model_state._dataHT221;
	_model_state._transmitData2._LIS3MDL=_model_state._dataLIS3._dataMagn;
	_model_state._transmitData2._lsm6ds0=_model_state._dataLSM6DS0;
	_model_state._transmitData2._LPS25HB=_model_state._dataLPS25HB;

	RS485_Sprotocol_Data_def msg;
	msg.cmd=Get_main_data;
	msg.length=sizeof(Transmit_data2_Model_MainData);
	msg.data=(uint8_t*)&_model_state._transmitData2;
	SProtocol_addTxMessage(&msg);
}

static void function_Get_setting_peripheral_board(uint8_t *data)
{
	memcpy(_model_state._transmitData3.dataSettings,_model_state.dataSettings,sizeof(Transmit_data3_Model_Settings));
	RS485_Sprotocol_Data_def msg;
	msg.cmd=Get_main_data;
	msg.length=sizeof(Transmit_data3_Model_Settings);
	msg.data=(uint8_t*)&_model_state._transmitData3.dataSettings;
	SProtocol_addTxMessage(&msg);
}

static void function_Set_setting_peripheral_board(uint8_t *data)
{
	memcpy(_model_state.dataSettings,_model_state._transmitData3.dataSettings,sizeof(_model_state.dataSettings));
}

static void function_Set_Default_peripheral_board(uint8_t *data)
{
	for(int i=0;i<3;i++)
	{
		_model_state.dataSettings[i].Kd=1;
		_model_state.dataSettings[i].Kp=1;
		_model_state.dataSettings[i].Ki=1;
		_model_state.dataSettings[i].Saturation_M=1;
		_model_state.dataSettings[i].Saturation_P=1;
	}

}

static void function_Set_cmd_flash(uint8_t *data)
{

}






/*==================================================================================*/
#define SIZE_NUM_CMD				7
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler) function_Set_Connect,

		(rs485_command_handler) function_Get_Board,
		(rs485_command_handler)	function_Get_main_data,
		(rs485_command_handler)	function_Get_setting_peripheral_board,

		(rs485_command_handler)	function_Set_setting_peripheral_board,
		(rs485_command_handler) function_Set_Default_peripheral_board,
		(rs485_command_handler)function_Set_cmd_flash

};


/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
				        return;

		//hard fix of data align trouble
		//uint32_t memory[10];
		//RS485_Sprotocol_Data_def *mess=(RS485_Sprotocol_Data_def *)((uint8_t *)memory);
		RS485_Sprotocol_Data_def mess;
		Get_RS485RxMessage(&mess);
		rs485_commands_array[mess.cmd](mess.dataRxPayload);
		RS485_SetPkgAvailable(0);
}

