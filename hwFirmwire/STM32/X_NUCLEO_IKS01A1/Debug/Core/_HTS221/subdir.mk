################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/_HTS221/HTS221_Lib.c 

OBJS += \
./Core/_HTS221/HTS221_Lib.o 

C_DEPS += \
./Core/_HTS221/HTS221_Lib.d 


# Each subdirectory must supply rules for building sources it contributes
Core/_HTS221/%.o: ../Core/_HTS221/%.c Core/_HTS221/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F334x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -I../Core/_LEDs -I../Core/_RS485 -I../Core/Helpers -I../Core/_LIS3MDL -I../Core/_LSM6DS0 -I../Core/_LPS25HB -I../Core/_HTS221 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-_HTS221

clean-Core-2f-_HTS221:
	-$(RM) ./Core/_HTS221/HTS221_Lib.d ./Core/_HTS221/HTS221_Lib.o

.PHONY: clean-Core-2f-_HTS221

