################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/_LEDs/Led.c 

OBJS += \
./Core/_LEDs/Led.o 

C_DEPS += \
./Core/_LEDs/Led.d 


# Each subdirectory must supply rules for building sources it contributes
Core/_LEDs/%.o: ../Core/_LEDs/%.c Core/_LEDs/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F334x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -I../Core/_LEDs -I../Core/_RS485 -I../Core/Helpers -I../Core/_DAC -I../Core/_ADC -I../Core/_PWM -I../Core/Logic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-_LEDs

clean-Core-2f-_LEDs:
	-$(RM) ./Core/_LEDs/Led.d ./Core/_LEDs/Led.o

.PHONY: clean-Core-2f-_LEDs

