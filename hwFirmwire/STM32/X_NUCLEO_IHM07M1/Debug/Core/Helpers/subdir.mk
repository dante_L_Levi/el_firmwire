################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Helpers/HAL_Helpers.c \
../Core/Helpers/Timeout.c \
../Core/Helpers/Timers.c 

OBJS += \
./Core/Helpers/HAL_Helpers.o \
./Core/Helpers/Timeout.o \
./Core/Helpers/Timers.o 

C_DEPS += \
./Core/Helpers/HAL_Helpers.d \
./Core/Helpers/Timeout.d \
./Core/Helpers/Timers.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Helpers/%.o: ../Core/Helpers/%.c Core/Helpers/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F334x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -I../Core/_LEDs -I../Core/_RS485 -I../Core/Helpers -I../Core/_DAC -I../Core/_ADC -I../Core/_PWM -I../Core/Logic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Helpers

clean-Core-2f-Helpers:
	-$(RM) ./Core/Helpers/HAL_Helpers.d ./Core/Helpers/HAL_Helpers.o ./Core/Helpers/Timeout.d ./Core/Helpers/Timeout.o ./Core/Helpers/Timers.d ./Core/Helpers/Timers.o

.PHONY: clean-Core-2f-Helpers

