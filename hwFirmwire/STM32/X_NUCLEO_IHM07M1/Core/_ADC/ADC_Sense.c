/*
 * ADC_Sense.c
 *
 *  Created on: Nov 30, 2021
 *      Author: Lepatenko
 */


#include "ADC_Sense.h"

//#define MODE_INJECT

#define COUNT_CH_INJECT			4
#define VREF					3.3
#define ADC_SEQ					4095.0
#define K_conv					VREF/ADC_SEQ

#define adc_CHANNEL_COUNT		2
ADC_ChannelConfTypeDef sConfig = {0};
static uint32_t HAL_RCC_ADC12_CLK_ENABLED=0;

uint16_t adc_inject_Array_value[COUNT_CH_INJECT];
uint16_t adc_regular_Array[adc_CHANNEL_COUNT]={0};


static void Timer_Event_Update_Init(void);
static void GPio_Init_ADC_Injected(void);
//static void GPIO_Init_regular_Channel(uint16_t channel, GPIO_TypeDef* port);




/******************Init Timer Update**************************/
//static void ADC_TIM_Triger_Init(void);



void DMA1_Channel4_IRQHandler(void)
{
	DMA1->ISR &=DMA_ISR_TCIF4;

}


#ifdef MODE_INJECT
void ADC1_2_IRQHandler(void)
{
	ADC1->ISR |= ADC_ISR_JEOS;
	adc_inject_Array_value[0]=ADC1->JDR1;
	adc_inject_Array_value[1]=ADC1->JDR2;
	adc_inject_Array_value[2]=ADC1->JDR3;
	adc_inject_Array_value[3]=ADC1->JDR4;

}
#endif



static void GPio_Init_ADC_Injected(void)
{

	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_RESET);


		  /*Configure GPIO pins : PA0 PA1 PA2 PA3  */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/*
static void GPIO_Init_regular_Channel(uint16_t channel, GPIO_TypeDef* port)
{
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	HAL_GPIO_WritePin(port, channel, GPIO_PIN_RESET);



	GPIO_InitStruct.Pin = channel;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(port, &GPIO_InitStruct);
}
*/

/************************Rread data*******************/
void Read_regular_data(uint16_t *data)
{
	data[0]=adc_regular_Array[0];
	data[1]=adc_regular_Array[1];

}

void Lib_HAL_ADC_Regular(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};


	  /* USER CODE BEGIN ADC2_MspInit 0 */

	  /* USER CODE END ADC2_MspInit 0 */
	    /* ADC2 clock enable */
	    HAL_RCC_ADC12_CLK_ENABLED++;
	    if(HAL_RCC_ADC12_CLK_ENABLED==1){
	      __HAL_RCC_ADC12_CLK_ENABLE();
	    }

	    __HAL_RCC_GPIOB_CLK_ENABLE();
	    /**ADC2 GPIO Configuration
	    PB12     ------> ADC2_IN13
	    PB14     ------> ADC2_IN14
	    */
	    GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_14;
	    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  /* USER CODE BEGIN ADC2_MspInit 1 */

	  /* USER CODE END ADC2_MspInit 1 */


	    /* USER CODE BEGIN ADC2_Init 0 */

	      /* USER CODE END ADC2_Init 0 */



	      /* USER CODE BEGIN ADC2_Init 1 */

	      /* USER CODE END ADC2_Init 1 */
	      /** Common config
	      */
	      hadc2.Instance = ADC2;
	      hadc2.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	      hadc2.Init.Resolution = ADC_RESOLUTION_12B;
	      hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
	      hadc2.Init.ContinuousConvMode = ENABLE;
	      hadc2.Init.DiscontinuousConvMode = DISABLE;
	      hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	      hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	      hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	      hadc2.Init.NbrOfConversion = 2;
	      hadc2.Init.DMAContinuousRequests = DISABLE;
	      hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	      hadc2.Init.LowPowerAutoWait = DISABLE;
	      hadc2.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
	      if (HAL_ADC_Init(&hadc2) != HAL_OK)
	      {
	        Error_Handler();
	      }
}

void ADC_Regular_Select_CH1(void)
{
	sConfig.Channel = ADC_CHANNEL_14;
	sConfig.Rank = 1;
	//sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES_5;
	//sConfig.OffsetNumber = ADC_OFFSET_NONE;
	//sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
	{
	  //Error_Handler();
	}
}

void ADC_Regular_Select_CH2(void)
{
	sConfig.Channel = ADC_CHANNEL_13;
	sConfig.Rank = 1;
	//sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.SamplingTime = ADC_SAMPLETIME_19CYCLES_5;
	//sConfig.OffsetNumber = ADC_OFFSET_NONE;
	//sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
	{
		  //Error_Handler();
	}
}


void Read_ADC_regular(void)
{
	ADC_Regular_Select_CH1();
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 1000);
	adc_regular_Array[0]=HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);

	ADC_Regular_Select_CH2();
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 1000);
	adc_regular_Array[1]=HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);


}



void Init_ADC_Sense_Inject(void)
{
	RCC->AHBENR |= RCC_AHBENR_ADC12EN;
	GPio_Init_ADC_Injected();
	Timer_Event_Update_Init();
	StartCallibrationAdc();
	ADC1->JSQR |= 0x1030817B;
	ADC1->IER |= ADC_IER_JEOSIE;
	NVIC_EnableIRQ(ADC1_2_IRQn);            // Enable interrupt ADC1 and ADC2
	NVIC_SetPriority(ADC1_2_IRQn,1);
	ADC1->CR |= ADC_CR_ADEN;                // Enable ADC1
	while(!(ADC1->ISR & ADC_ISR_ADRDY));    // Wait ready ADC1
    ADC1->CR |= ADC_CR_JADSTART;            // Enable injector conversion


}


static void Timer_Event_Update_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	TIM6->PSC = 1-1;
	TIM6->ARR = 36;
	TIM6->CR2 |= TIM_CR2_MMS_1;         // Enable generation TRGO for ADC
	TIM6->CR1  |= TIM_CR1_CEN;
}



void StartCallibrationAdc(void)
{
	ADC1->CR &= ~ADC_CR_ADVREGEN;
	ADC1->CR |= ADC_CR_ADVREGEN_0;      // Enable Vref
	ADC1->CR &= ~ADC_CR_ADCALDIF;

	ADC1->CR |= ADC_CR_ADCAL;           // Start calibration
	while (ADC1->CR & ADC_CR_ADCAL);    // Wait end calibration
}


uint16_t Get_ADC_Inject_Channels_ADC_Code(uint8_t channel)
{
	if(channel>3)
			return 0;


	return adc_inject_Array_value[channel];
}

int16_t Get_ADC_Inject_Channels(uint8_t channel)
{
	if(channel>3)
		return 0;
	float value=(adc_inject_Array_value[channel]*K_conv)*1000;
	return (int16_t)value;

}






