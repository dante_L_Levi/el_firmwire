/*
 * ADC_regular.c
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#include "ADC_Regular.h"


static void Select_Ch1(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
		/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
			  */
			  sConfig.Channel = ADC_CHANNEL_1;
			  sConfig.Rank = 1;
			  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
			  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
			  {
			    Error_Handler();
			  }

			  HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1000);
	*datacode = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);



}

static void Select_Ch2(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
			/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
				  */
				  sConfig.Channel = ADC_CHANNEL_2;
				  sConfig.Rank = 1;
				  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLE_5;
				  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
				  {
				    Error_Handler();
				  }
				  HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1000);
	*datacode = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);





}

static void Select_Ch6(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
				/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
					  */
					  sConfig.Channel = ADC_CHANNEL_6;
					  sConfig.Rank = 1;
					  sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES_5;
					  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
					  {
					    Error_Handler();
					  }
					  HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1000);
	*datacode = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);





}

static void Select_Ch7(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
					/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
						  */
						  sConfig.Channel = ADC_CHANNEL_7;
						  sConfig.Rank = 1;
						  sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
						  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
						  {
						    Error_Handler();
						  }
						  HAL_ADC_Start(&hadc1);
						  HAL_ADC_PollForConversion(&hadc1, 1000);
						  	*datacode = HAL_ADC_GetValue(&hadc1);
						  	HAL_ADC_Stop(&hadc1);


}

static void Select_Ch8(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
						/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
							  */
							  sConfig.Channel = ADC_CHANNEL_8;
							  sConfig.Rank = 1;
							  sConfig.SamplingTime = ADC_SAMPLETIME_19CYCLES_5;
							  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
							  {
							    Error_Handler();
							  }
							  HAL_ADC_Start(&hadc1);
							  HAL_ADC_PollForConversion(&hadc1, 1000);
							  	*datacode = HAL_ADC_GetValue(&hadc1);
							  	HAL_ADC_Stop(&hadc1);


}

static void Select_Ch9(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
							/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
								  */
								  sConfig.Channel = ADC_CHANNEL_9;
								  sConfig.Rank = 1;
								  sConfig.SamplingTime = ADC_SAMPLETIME_61CYCLES_5;
								  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
								  {
								    Error_Handler();
								  }
								  HAL_ADC_Start(&hadc1);
								  HAL_ADC_PollForConversion(&hadc1, 1000);
								  	*datacode = HAL_ADC_GetValue(&hadc1);
								  	HAL_ADC_Stop(&hadc1);



}

static void Select_Ch11(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
								/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
									  */
									  sConfig.Channel = ADC_CHANNEL_11;
									  sConfig.Rank = 1;
									  sConfig.SamplingTime = ADC_SAMPLETIME_181CYCLES_5;
									  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
									  {
									    Error_Handler();
									  }
									  HAL_ADC_Start(&hadc1);
									  HAL_ADC_PollForConversion(&hadc1, 1000);
									  	*datacode = HAL_ADC_GetValue(&hadc1);
									  	HAL_ADC_Stop(&hadc1);


}

static void Select_Ch12(uint16_t *datacode)
{
	ADC_ChannelConfTypeDef sConfig = {0};
									/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
										  */
										  sConfig.Channel = ADC_CHANNEL_12;
										  sConfig.Rank = 1;
										  sConfig.SamplingTime = ADC_SAMPLETIME_601CYCLES_5;
										  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
										  {
										    Error_Handler();
										  }
										  HAL_ADC_Start(&hadc1);
										  HAL_ADC_PollForConversion(&hadc1, 1000);
										  	*datacode = HAL_ADC_GetValue(&hadc1);
										  	HAL_ADC_Stop(&hadc1);
}


void ADC_Select_Channel(uint8_t channel,uint16_t *data_code)
{
	switch(channel)
	{
		case 1:
		{
			Select_Ch1(data_code);
			break;
		}

		case 2:
				{
					Select_Ch2(data_code);
					break;
				}
		case 6:
				{
					Select_Ch6(data_code);
					break;
				}
		case 7:
				{
					Select_Ch7(data_code);
					break;
				}

		case 8:
				{
					Select_Ch8(data_code);
					break;
				}

		case 9:
				{
					Select_Ch9(data_code);
					break;
				}

		case 11:
				{
					Select_Ch11(data_code);
					break;
				}

		case 12:
				{
					Select_Ch12(data_code);
					break;
				}


	}
}
