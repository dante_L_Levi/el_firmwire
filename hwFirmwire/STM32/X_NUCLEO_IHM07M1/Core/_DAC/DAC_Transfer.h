/*
 * DAC_Transfer.h
 *
 *  Created on: Nov 30, 2021
 *      Author: Lepatenko
 */

#ifndef DAC_DAC_TRANSFER_H_
#define DAC_DAC_TRANSFER_H_


#include "Headers.h"


#define DAC_CHANNEL1		0x00
#define DAC_CHANNEL2		0x01

/***********************INIT DAC*************************/
void DAC_Init(void);
/*******************SET value DAC***********************/
void Set_DAC_Value(uint16_t data,uint8_t ch);
/*******************GET value DAC***********************/
uint16_t Get_DAC_Value(uint8_t ch);
/********************ENABLE DAC******************/
void DAC_ENABLE(uint8_t ch);
/********************DISABLE DAC******************/
void DAC_DISABLE(uint8_t ch);



#endif /* DAC_DAC_TRANSFER_H_ */
