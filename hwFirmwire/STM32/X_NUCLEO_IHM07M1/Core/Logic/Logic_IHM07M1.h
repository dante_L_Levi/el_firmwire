/*
 * Logic_IHM07M1.h
 *
 *  Created on: Mar 7, 2022
 *      Author: Lepatenko
 */

#ifndef INC_LOGIC_IHM07M1_H_
#define INC_LOGIC_IHM07M1_H_


#include "Headers.h"
#include "Settings.h"

#include "BLDC_Controller.h"




#pragma pack(push, 1)
typedef union
{
	struct
	{
		uint32_t Current_U;
		uint32_t VBUS;
		uint32_t Current_W;
		uint32_t Current_V;
		uint32_t temp_NTC;
		uint32_t BEMF1;
		uint32_t BEMF2;
		uint32_t POT_SPEED;

	}fields;
	uint32_t All_adc_data_hw[8];
}hw_state_adc1;


typedef union
{
	struct
		{
			int16_t Current_U;
			int16_t VBUS;
			int16_t Current_W;
			int16_t Current_V;
			int16_t temp_NTC;
			int16_t BEMF1;
			int16_t BEMF2;
			int16_t POT_SPEED;

		}fields;
		int16_t All_adc_data[8];

}adc1_s;

typedef union
{
	struct
	{
		float Current_U_Kp;
		float VBUS_Kp;
		float Current_W_Kp;
		float Current_V_Kp;
		float temp_NTC_Kp;
		float BEMF1_Kp;
		float BEMF2_Kp;
		float POT_SPEED_Kp;
		float BEMF3_Kp;
	}fields;
	float setting_K[9];

}adc_result_Settings_K;


typedef union
{
	struct
	{
		uint32_t BEMF3;

	}fields;
	uint32_t All_adc_data_hw[1];
}hw_state_adc2;

typedef union
{
	struct
	{
		int16_t BEMF3;

	}fields;
	int16_t All_adc_data_hw[1];
}adc2_s;


typedef union
{
	struct
	{
		uint8_t GPIO_BEMF:1;
		uint8_t GPIO1:1;
		uint8_t GPIO2:1;
		uint8_t GPIO3:1;
		uint8_t DIG_EN:1;
		uint8_t EN_DRV1:1;
		uint8_t EN_DRV2:1;
		uint8_t EN_DRV3:1;

	}fields;
	uint8_t _gpio_state;

}gpio_out_s;


typedef union
{
	struct
	{
		uint8_t CPOUT:1;
	}fields;
	uint8_t gpio_input;
}gpio_input_s;

typedef struct
{
	uint16_t Dac_value;
}Dac_Out_s;

typedef union
{
	struct
	{
		uint16_t Duty_U;
		uint16_t Duty_V;
		uint16_t Duty_W;

	}fields;
	uint16_t pwm_out_value[3];

}pwm_Duty_halfBridge_s;

typedef union
{
	struct
	{
		uint8_t Hall_U:1;
		uint8_t Hall_V:1;
		uint8_t Hall_W:1;

	}fields;
	uint8_t Hall_sensor;
}Hall_sensor_s;



typedef struct
{
	uint16_t ID;
	uint8_t 					sync_trigger;
	uint32_t 					_time_ms;
	uint32_t					Array[8];
	hw_state_adc1				_adc1_hw;
	adc1_s						adc1;
	hw_state_adc2				_adc2_hw;
	adc2_s						adc2;
	adc_result_Settings_K		_setting_adc_out;
	gpio_out_s					_gpio_out;
	gpio_input_s				_gpio_in;
	Dac_Out_s					_dac_hw;
	pwm_Duty_halfBridge_s		_pwm_hw;



}work_state_model_s;


#pragma pack(pop)


void Main_Init(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
void Get_ADC_Bemf_Phase(uint16_t *dataBemf);

void IHM07M1_Sync(void);


void Set_GPIO_State(gpio_out_s *_state);
void Set_ENABLE_DRV(uint8_t channel,bool flag);
uint16_t  Get_VBus(void);

#endif /* INC_LOGIC_IHM07M1_H_ */
