/*
 * BLDC_Controller.h
 *
 *  Created on: 14 мар. 2022 г.
 *      Author: Lepatenko
 */

#ifndef LOGIC_BLDC_CONTROLLER_H_
#define LOGIC_BLDC_CONTROLLER_H_


#include "Headers.h"

//============= Settings ===============
// PWM Frequency = 72000000/1/BLDC_CHOPPER_PERIOD
#define BLDC_CHOPPER_PERIOD htim1.Init.Period


#define BLDC_START_STEPS 				24
#define BLDC_START_DELAY 				10

#define BLDC_TIMING_INIT 				10 // timing in degrees 0..30

#define BLDC_STOP						0
#define BLDC_START						1

#define BEMF_TIMEOUT					1		//filter timeout 1...5

typedef enum
{
	PWM_TopKeys,
	PWM_BottomKeys,
	PWM_ComplementaryKeys

}Mode_Comutation_BLDC_Motor;

typedef enum
{
	index_UH=0,
	index_UL=1,
	index_VH=2,
	index_VL=3,
	index_WH=4,
	index_WL=5,

}step_index_Bemf_mode;



typedef enum
	{
		Step_Mode_Bemf,
		Step_Mode_Hall,
		FOC_Mode

	}Motor_Control_Type;

typedef	enum
		{
			BLDC_Motor,
			PLSM_Motor

		}Motor_Type;


/*
 * EN1,EN2,EN3-ctrl High side
 * ---------------------------
 * IN1,IIN2,IN3-ctrl Low side
 * ---------------------------
 *
 */


void _Init_Motor_Logic(Motor_Control_Type ctrl,Motor_Type typeM,Mode_Comutation_BLDC_Motor _state);
void BLDC_PWMTimerInit(void);
void Bldc_Tim3_Async(void);
void Bldc_Tim2_Async(void);
void Bldc_Tim1_Async(void);
void Start_Motor(void);
void Stop_Motor(void);


#endif /* LOGIC_BLDC_CONTROLLER_H_ */
