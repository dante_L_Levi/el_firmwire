/*
 * Logic_IHM07M1.c
 *
 *  Created on: Mar 7, 2022
 *      Author: Lepatenko
 */


#include "Logic_IHM07M1.h"
#include "IHM07M1_Helpers.h"


work_state_model_s _model_state;
uint16_t buffer[8]={0};
uint16_t buffer_adc2[1]={0};
volatile uint8_t adcConversionComplete=0;

static void Update_sync(void)
{
	_model_state.sync_trigger=1;
}

static void work_time_ms(void)
{
	_model_state._time_ms++;
}

static void Config_Koef(void)
{
	_model_state._setting_adc_out.fields.BEMF1_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.BEMF2_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.BEMF3_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.POT_SPEED_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.VBUS_Kp=15.189;//mV

}

void Main_Init(void)
{
	Config_Koef();
	MX_GPIO_Init();
	Set_Period_Toggle(8000);
	LED_Init();
	ADC_DMA_SetCallBack(MX_ADC1_Init,MX_DMA_Init);
	ADC_DMA_Init();

	ADC_Reserve_DMA_SetCallBack(MX_ADC2_Init,MX_DMA_Init);
	ADC_Reserve_DMA_Init();


	Init_Tim6_Sync(FAST_LOOP);
	Set_CallBack(6,Update_sync);

	Init_Tim7_One_ms();
	Set_CallBack(7,work_time_ms);


	ADC_DMA_Event(&hadc1,(uint32_t*)(buffer), 8);
	ADC_DMA_Event(&hadc2,(uint32_t*)(buffer_adc2), 1);

	//_Init_Motor_Logic(Step_Mode_Bemf,BLDC_Motor,PWM_ComplementaryKeys);

}



uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}

void Set_GPIO_State(gpio_out_s *_state)
{
	_model_state._gpio_out=*_state;
	Set_Gpio_Helper(&_model_state._gpio_out);


}


void Set_ENABLE_DRV(uint8_t channel,bool flag)
{
	switch(channel)
	{
		case 1:
		{
			flag==true?(_model_state._gpio_out.fields.EN_DRV1=1):(_model_state._gpio_out.fields.EN_DRV1=0);
			break;
		}

		case 2:
		{
			flag==true?(_model_state._gpio_out.fields.EN_DRV2=1):(_model_state._gpio_out.fields.EN_DRV2=0);
			break;
		}

		case 3:
		{
			flag==true?(_model_state._gpio_out.fields.EN_DRV3=1):(_model_state._gpio_out.fields.EN_DRV3=0);
			break;
		}


	}

	Set_Gpio_Helper(&_model_state._gpio_out);
}



static void Analog_read(void)
{
	for(int i=0;i<8;i++)
	{
		_model_state._adc1_hw.All_adc_data_hw[i]=buffer[i];
	}
	_model_state._adc2_hw.fields.BEMF3=buffer_adc2[0];

	for(int i=0;i<8;i++)
	{
		_model_state.adc1.All_adc_data[i]=_model_state._adc1_hw.All_adc_data_hw[i]*
				_model_state._setting_adc_out.setting_K[i];
	}

	_model_state.adc2.fields.BEMF3=_model_state._adc2_hw.fields.BEMF3*_model_state._setting_adc_out.setting_K[8];







}

static void Fast_Loop(void)
{
	Analog_read();
}

static void Middle_Loop(void)
{
	static int32_t skip_counter = 1;
			    if (skip_counter--)
			        return;
			    skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;


}


void IHM07M1_Sync(void)
{
	Fast_Loop();
	Middle_Loop();
}

void Get_ADC_Bemf_Phase(uint16_t *dataBemf)
{
	dataBemf[0]=_model_state._adc1_hw.fields.BEMF1;
	dataBemf[1]=_model_state._adc1_hw.fields.BEMF2;
	dataBemf[2]=_model_state._adc2_hw.fields.BEMF3;
}

uint16_t  Get_VBus(void)
{
	return _model_state._adc1_hw.fields.VBUS;
}








/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{

}

