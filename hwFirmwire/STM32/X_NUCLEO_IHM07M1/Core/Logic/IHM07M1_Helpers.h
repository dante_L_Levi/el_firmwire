/*
 * IHM07M1_Helpers.h
 *
 *  Created on: Mar 13, 2022
 *      Author: Lepatenko
 */

#ifndef LOGIC_IHM07M1_HELPERS_H_
#define LOGIC_IHM07M1_HELPERS_H_

#include "Headers.h"
#include "Logic_IHM07M1.h"

#define LED_EXT_ON()		HAL_GPIO_WritePin(EXT_LED_GPIO_Port, EXT_LED_Pin, GPIO_PIN_SET)
#define LED_EXT_OFF()		HAL_GPIO_WritePin(EXT_LED_GPIO_Port, EXT_LED_Pin, GPIO_PIN_RESET)

#define EN1_ON()			HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin, GPIO_PIN_SET)
#define EN1_OFF()			HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin, GPIO_PIN_RESET)

#define EN2_ON()			HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin, GPIO_PIN_SET)
#define EN2_OFF()			HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin, GPIO_PIN_RESET)

#define EN3_ON()			HAL_GPIO_WritePin(EN3_GPIO_Port, EN3_Pin, GPIO_PIN_SET)
#define EN3_OFF()			HAL_GPIO_WritePin(EN3_GPIO_Port, EN3_Pin, GPIO_PIN_RESET)


#define BEMF_EN()			HAL_GPIO_WritePin(GPIO_BEMF_GPIO_Port, GPIO_BEMF_Pin, GPIO_PIN_SET)
#define BEMF_DEN()			HAL_GPIO_WritePin(GPIO_BEMF_GPIO_Port, GPIO_BEMF_Pin, GPIO_PIN_RESET)



#define DIG_EN_ON()			HAL_GPIO_WritePin(DIG_EN_GPIO_Port, DIG_EN_Pin, GPIO_PIN_SET)
#define DIG_EN_OFF()		HAL_GPIO_WritePin(DIG_EN_GPIO_Port, DIG_EN_Pin, GPIO_PIN_RESET)


#define GPIO1_ON()			HAL_GPIO_WritePin(GPIO1_GPIO_Port, GPIO1_Pin, GPIO_PIN_SET)
#define GPIO1_OFF()			HAL_GPIO_WritePin(GPIO1_GPIO_Port, GPIO1_Pin, GPIO_PIN_RESET)

#define GPIO2_ON()			HAL_GPIO_WritePin(GPIO2_GPIO_Port, GPIO2_Pin, GPIO_PIN_SET)
#define GPIO2_OFF()			HAL_GPIO_WritePin(GPIO2_GPIO_Port, GPIO2_Pin, GPIO_PIN_RESET)





void Set_Gpio_Helper(gpio_out_s* state);
void Get_State_Input_Helper(gpio_input_s* state);

bool Programm_Compare(uint16_t data,uint16_t Threshold,uint16_t offset);
uint8_t Programm_Compare_All(uint16_t *data,uint16_t Threshold,uint16_t offset);

#endif /* LOGIC_IHM07M1_HELPERS_H_ */
