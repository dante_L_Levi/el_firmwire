/*
 * IHM07M1_Helpers.c
 *
 *  Created on: Mar 13, 2022
 *      Author: Lepatenko
 */


#include "IHM07M1_Helpers.h"

void Set_Gpio_Helper(gpio_out_s* state)
{
	(state->fields.DIG_EN==1)?(DIG_EN_ON()):(DIG_EN_OFF());
	(state->fields.EN_DRV1==1)?(EN1_ON()):(EN1_OFF());
	(state->fields.EN_DRV2==1)?(EN2_ON()):(EN2_OFF());
	(state->fields.EN_DRV3==1)?(EN3_ON()):(EN3_OFF());
	(state->fields.GPIO1==1)?(GPIO1_ON()):(DIG_EN_OFF());
	(state->fields.GPIO2==1)?(GPIO2_ON()):(DIG_EN_OFF());
	(state->fields.GPIO_BEMF==1)?(BEMF_EN()):(BEMF_DEN());


}


void Get_State_Input_Helper(gpio_input_s* state)
{
	(HAL_GPIO_ReadPin (CPOUT_GPIO_Port, CPOUT_Pin))?(state->fields.CPOUT=1):(state->fields.CPOUT=0);
}


bool Programm_Compare(uint16_t data,uint16_t Threshold,uint16_t offset)
{
	return (((data<=Threshold)||(data<Threshold+offset))?(true):false);
}


uint8_t Programm_Compare_All(uint16_t *data,uint16_t Threshold,uint16_t offset)
{
	uint8_t result=0x00;
	((data[0]<=Threshold)||(data[0]<=Threshold+offset))?(result|=0x01):(result&=~0x01);
	((data[1]<=Threshold)||(data[1]<=Threshold+offset))?(result|=0x02):(result&=~0x02);
	((data[2]<=Threshold)||(data[2]<=Threshold+offset))?(result|=0x04):(result&=~0x04);
	return result;
}
