/*
 * pwm_Lib_Helper.c
 *
 *  Created on: Mar 11, 2022
 *      Author: Lepatenko
 */
#include "pwm_Lib_Helper.h"


/* ---------------------- TIM registers bit mask ------------------------ */
#define SMCR_ETR_Mask               ((uint16_t)0x00FF)
#define CCMR_Offset                 ((uint16_t)0x0018)
#define CCER_CCE_Set                ((uint16_t)0x0001)
#define	CCER_CCNE_Set               ((uint16_t)0x0004)



Mode_pwm_def status_pwm=Idle;

void (*callbackInit)(void);

void Init_Base_Tim1(void *func,Mode_pwm_def mode)
{
	callbackInit=func;
	callbackInit();
	status_pwm=mode;
}

void Start_Tim1_Pwm(uint32_t channel)
{
	switch(status_pwm)
	{
		case Single:
		{
			HAL_TIM_PWM_Start(&htim1, channel);
			break;
		}

		case Complimentary:
		{
			HAL_TIM_PWM_Start(&htim1, channel);
			HAL_TIMEx_PWMN_Start(&htim1, channel);
			break;
		}

		default:
			break;
	}
}

void Stop_Tim1_Pwm(uint32_t channel)
{
	switch(status_pwm)
		{
			case Single:
			{
				HAL_TIM_PWM_Stop(&htim1, channel);
				break;
			}

			case Complimentary:
			{
				HAL_TIM_PWM_Stop(&htim1, channel);
				HAL_TIMEx_PWMN_Stop(&htim1, channel);
				break;
			}

			default:
				break;
		}
}



void Set_Duty_Tim1_pwm(uint32_t channel, uint32_t Duty)
{
	switch(channel)
	{
		case TIM_CHANNEL_1:
		{
			TIM1->CCR1=Duty;
			break;
		}

		case TIM_CHANNEL_2:
		{
			TIM1->CCR2=Duty;
			break;
		}
		case TIM_CHANNEL_3:
		{
			TIM1->CCR3=Duty;
			break;
		}

		case TIM_CHANNEL_4:
		{
			TIM1->CCR4=Duty;
			break;
		}


	}
}



void Init_Tim1_pwm_Channel1(GPIO_TypeDef * _port,uint32_t pin,Mode_pwm_def mode ,uint32_t ALoad,uint32_t prescaler)
{
	switch(mode)
	{
		case Single:
		{
			__HAL_RCC_GPIOA_CLK_ENABLE();
				 __HAL_RCC_GPIOB_CLK_ENABLE();
				 __HAL_RCC_GPIOC_CLK_ENABLE();
				 __HAL_RCC_GPIOD_CLK_ENABLE();

				GPIO_InitTypeDef GPIO_InitStruct = {0};


				    __HAL_RCC_GPIOA_CLK_ENABLE();


				    GPIO_InitStruct.Pin = pin;
				    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
				    GPIO_InitStruct.Pull = GPIO_NOPULL;
				    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
				    GPIO_InitStruct.Alternate = GPIO_AF6_TIM1;
				    HAL_GPIO_Init(_port, &GPIO_InitStruct);

				    __HAL_RCC_TIM1_CLK_ENABLE();



				      TIM_ClockConfigTypeDef sClockSourceConfig = {0};
				      TIM_MasterConfigTypeDef sMasterConfig = {0};
				      TIM_OC_InitTypeDef sConfigOC = {0};
				      TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};


				      htim1.Instance = TIM1;
				      htim1.Init.Prescaler = prescaler-1;
				      htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
				      htim1.Init.Period = ALoad-1;
				      htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
				      htim1.Init.RepetitionCounter = 0;
				      htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
				      if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
				      {

				      }
				      sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
				      if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
				      {

				      }
				      if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
				      {

				      }
				      sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
				      sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
				      sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
				      if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
				      {

				      }
				      sConfigOC.OCMode = TIM_OCMODE_PWM1;
				      sConfigOC.Pulse = 0;
				      sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
				      sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
				      sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
				      sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
				      sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
				      if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
				      {

				      }

				      sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
				      sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
				      sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
				      sBreakDeadTimeConfig.DeadTime = 0;
				      sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
				      sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
				      sBreakDeadTimeConfig.BreakFilter = 0;
				      sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
				      sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
				      sBreakDeadTimeConfig.Break2Filter = 0;
				      sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
				      if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
				      {

				      }

			break;
		}
		case Complimentary:
		{

			break;
		}
		default:
			break;
	}



}


void Init_Tim1_pwm_Channel2(GPIO_TypeDef * _port,uint32_t pin,Mode_pwm_def mode ,uint32_t ALoad,uint32_t prescaler)
{
	switch(mode)
	{
		case Single:
		{
			__HAL_RCC_GPIOA_CLK_ENABLE();
				 __HAL_RCC_GPIOB_CLK_ENABLE();
				 __HAL_RCC_GPIOC_CLK_ENABLE();
				 __HAL_RCC_GPIOD_CLK_ENABLE();

				GPIO_InitTypeDef GPIO_InitStruct = {0};


				    __HAL_RCC_GPIOA_CLK_ENABLE();


				    GPIO_InitStruct.Pin = pin;
				    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
				    GPIO_InitStruct.Pull = GPIO_NOPULL;
				    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
				    GPIO_InitStruct.Alternate = GPIO_AF6_TIM1;
				    HAL_GPIO_Init(_port, &GPIO_InitStruct);

				    __HAL_RCC_TIM1_CLK_ENABLE();



				      TIM_ClockConfigTypeDef sClockSourceConfig = {0};
				      TIM_MasterConfigTypeDef sMasterConfig = {0};
				      TIM_OC_InitTypeDef sConfigOC = {0};
				      TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};


				      htim1.Instance = TIM1;
				      htim1.Init.Prescaler = prescaler-1;
				      htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
				      htim1.Init.Period = ALoad-1;
				      htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
				      htim1.Init.RepetitionCounter = 0;
				      htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
				      if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
				      {

				      }
				      sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
				      if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
				      {

				      }
				      if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
				      {

				      }
				      sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
				      sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
				      sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
				      if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
				      {

				      }
				      sConfigOC.OCMode = TIM_OCMODE_PWM1;
				      sConfigOC.Pulse = 0;
				      sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
				      sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
				      sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
				      sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
				      sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
				      if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
				      {

				      }

				      sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
				      sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
				      sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
				      sBreakDeadTimeConfig.DeadTime = 0;
				      sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
				      sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
				      sBreakDeadTimeConfig.BreakFilter = 0;
				      sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
				      sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
				      sBreakDeadTimeConfig.Break2Filter = 0;
				      sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
				      if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
				      {

				      }

			break;
		}
		case Complimentary:
		{

			break;
		}
		default:
			break;
	}



}


void Init_Tim1_pwm_Channel3(GPIO_TypeDef * _port,uint32_t pin,Mode_pwm_def mode ,uint32_t ALoad,uint32_t prescaler)
{
	switch(mode)
	{
		case Single:
		{
			__HAL_RCC_GPIOA_CLK_ENABLE();
				 __HAL_RCC_GPIOB_CLK_ENABLE();
				 __HAL_RCC_GPIOC_CLK_ENABLE();
				 __HAL_RCC_GPIOD_CLK_ENABLE();

				GPIO_InitTypeDef GPIO_InitStruct = {0};


				    __HAL_RCC_GPIOA_CLK_ENABLE();


				    GPIO_InitStruct.Pin = pin;
				    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
				    GPIO_InitStruct.Pull = GPIO_NOPULL;
				    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
				    GPIO_InitStruct.Alternate = GPIO_AF6_TIM1;
				    HAL_GPIO_Init(_port, &GPIO_InitStruct);

				    __HAL_RCC_TIM1_CLK_ENABLE();



				      TIM_ClockConfigTypeDef sClockSourceConfig = {0};
				      TIM_MasterConfigTypeDef sMasterConfig = {0};
				      TIM_OC_InitTypeDef sConfigOC = {0};
				      TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};


				      htim1.Instance = TIM1;
				      htim1.Init.Prescaler = prescaler-1;
				      htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
				      htim1.Init.Period = ALoad-1;
				      htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
				      htim1.Init.RepetitionCounter = 0;
				      htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
				      if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
				      {

				      }
				      sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
				      if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
				      {

				      }
				      if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
				      {

				      }
				      sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
				      sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
				      sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
				      if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
				      {

				      }
				      sConfigOC.OCMode = TIM_OCMODE_PWM1;
				      sConfigOC.Pulse = 0;
				      sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
				      sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
				      sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
				      sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
				      sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
				      if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
				      {

				      }

				      sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
				      sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
				      sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
				      sBreakDeadTimeConfig.DeadTime = 0;
				      sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
				      sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
				      sBreakDeadTimeConfig.BreakFilter = 0;
				      sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
				      sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
				      sBreakDeadTimeConfig.Break2Filter = 0;
				      sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
				      if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
				      {

				      }

			break;
		}
		case Complimentary:
		{

			break;
		}
		default:
			break;
	}



}


void Init_Tim1_pwm_Channel4(GPIO_TypeDef * _port,uint32_t pin,Mode_pwm_def mode ,uint32_t ALoad,uint32_t prescaler)
{
	switch(mode)
	{
		case Single:
		{
			__HAL_RCC_GPIOA_CLK_ENABLE();
				 __HAL_RCC_GPIOB_CLK_ENABLE();
				 __HAL_RCC_GPIOC_CLK_ENABLE();
				 __HAL_RCC_GPIOD_CLK_ENABLE();

				GPIO_InitTypeDef GPIO_InitStruct = {0};


				    __HAL_RCC_GPIOA_CLK_ENABLE();


				    GPIO_InitStruct.Pin = pin;
				    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
				    GPIO_InitStruct.Pull = GPIO_NOPULL;
				    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
				    GPIO_InitStruct.Alternate = GPIO_AF6_TIM1;
				    HAL_GPIO_Init(_port, &GPIO_InitStruct);

				    __HAL_RCC_TIM1_CLK_ENABLE();



				      TIM_ClockConfigTypeDef sClockSourceConfig = {0};
				      TIM_MasterConfigTypeDef sMasterConfig = {0};
				      TIM_OC_InitTypeDef sConfigOC = {0};
				      TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};


				      htim1.Instance = TIM1;
				      htim1.Init.Prescaler = prescaler-1;
				      htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
				      htim1.Init.Period = ALoad-1;
				      htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
				      htim1.Init.RepetitionCounter = 0;
				      htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
				      if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
				      {

				      }
				      sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
				      if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
				      {

				      }
				      if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
				      {

				      }
				      sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
				      sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
				      sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
				      if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
				      {

				      }
				      sConfigOC.OCMode = TIM_OCMODE_PWM1;
				      sConfigOC.Pulse = 0;
				      sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
				      sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
				      sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
				      sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
				      sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
				      if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
				      {

				      }

				      sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
				      sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
				      sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
				      sBreakDeadTimeConfig.DeadTime = 0;
				      sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
				      sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
				      sBreakDeadTimeConfig.BreakFilter = 0;
				      sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
				      sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
				      sBreakDeadTimeConfig.Break2Filter = 0;
				      sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
				      if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
				      {

				      }

			break;
		}
		case Complimentary:
		{

			break;
		}
		default:
			break;
	}



}




void HAL_TIM_Select_OCx_M(TIM_HandleTypeDef *Timx,uint32_t channel,uint32_t Mode)
{

	TIM_OC_InitTypeDef sConfigOC = {0};
	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};
	switch(Mode)
	{
		case TIM_OCMODE_PWM1:
		{


			Timx->Instance = TIM1;
			Timx->Init.Prescaler = 1;
			Timx->Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
			Timx->Init.Period = 4000;
			Timx->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
			Timx->Init.RepetitionCounter = 0;
			Timx->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
			if (HAL_TIM_Base_Init(Timx) != HAL_OK)
			{

			}

			sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
			  if (HAL_TIM_ConfigClockSource(Timx, &sClockSourceConfig) != HAL_OK)
			  {

			  }
			  if (HAL_TIM_PWM_Init(Timx) != HAL_OK)
			  {

			  }
			  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
			  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
			  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
			  if (HAL_TIMEx_MasterConfigSynchronization(Timx, &sMasterConfig) != HAL_OK)
			  {
			    Error_Handler();
			  }
			  sConfigOC.OCMode = TIM_OCMODE_PWM1;
			  sConfigOC.Pulse = 0;
			  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
			  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
			  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
			  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
			  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
			  if (HAL_TIM_PWM_ConfigChannel(Timx, &sConfigOC, channel) != HAL_OK)
			  {

			  }

			sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
			  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
			  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
			  sBreakDeadTimeConfig.DeadTime = 0;
			  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
			  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
			  sBreakDeadTimeConfig.BreakFilter = 0;
			  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
			  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
			  sBreakDeadTimeConfig.Break2Filter = 0;
			  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
			  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
			  {

			  }
			  HAL_TIM_MspPostInit(Timx);
			break;
		}

		case TIM_OCMODE_ACTIVE:
		{
			Timx->Instance = TIM1;
			Timx->Init.Prescaler = 1;
			Timx->Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
			Timx->Init.Period = 4000;
			Timx->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
			Timx->Init.RepetitionCounter = 0;
			Timx->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;

			if (HAL_TIM_Base_Init(Timx) != HAL_OK)
			{

			}
			sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
			if (HAL_TIM_ConfigClockSource(Timx, &sClockSourceConfig) != HAL_OK)
			{

    		}

			if (HAL_TIM_PWM_DeInit(&htim1) != HAL_OK)
			{

			}
			sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
			sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
			sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
			if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
			{

			}
			sConfigOC.OCMode = TIM_OCMODE_ACTIVE;
			sConfigOC.Pulse = 0;
			sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
			sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
			sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
			sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
			sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
			if (HAL_TIM_OC_ConfigChannel(Timx, &sConfigOC, channel) != HAL_OK)
			{

			}

			HAL_TIM_MspPostInit(Timx);

			break;
		}
		case TIM_OCMODE_INACTIVE:
		{

			Timx->Instance = TIM1;
			Timx->Init.Prescaler = 1;
			Timx->Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
			Timx->Init.Period = 4000;
			Timx->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
			Timx->Init.RepetitionCounter = 0;
			Timx->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;


			if (HAL_TIM_Base_Init(Timx) != HAL_OK)
			{

			}
			sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
			if (HAL_TIM_ConfigClockSource(Timx, &sClockSourceConfig) != HAL_OK)
			{

			}

			if (HAL_TIM_PWM_DeInit(&htim1) != HAL_OK)
			{

			}
			sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
			sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
			sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
			if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
			{

			}
			sConfigOC.OCMode = TIM_OCMODE_INACTIVE;
			sConfigOC.Pulse = 0;
			sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
			sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
			sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
			sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
			sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
			if (HAL_TIM_OC_ConfigChannel(Timx, &sConfigOC, channel) != HAL_OK)
			{

			}

			HAL_TIM_MspPostInit(Timx);
			break;
		}
	}







}



void TIM_CCxCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCx)
{
	uint16_t tmp = 0;
	tmp = CCER_CCE_Set << TIM_Channel;
	/* Reset the CCxE Bit */
	  TIMx->CCER &= (uint16_t)~ tmp;
	  /* Set or reset the CCxE Bit */
	    TIMx->CCER |=  (uint16_t)(TIM_CCx << TIM_Channel);

}

void TIM_CCxNCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCxN)
{
	uint16_t tmp = 0;
	tmp = CCER_CCNE_Set << TIM_Channel;

	/* Reset the CCxNE Bit */
	TIMx->CCER &= (uint16_t) ~tmp;

	/* Set or reset the CCxNE Bit */
	TIMx->CCER |=  (uint16_t)(TIM_CCxN << TIM_Channel);
}


void TIM_SelectOCxM(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_OCMode)
{
	uint32_t tmp = 0;
	uint16_t tmp1 = 0;

	tmp = (uint32_t) TIMx;
	tmp += CCMR_Offset;

	tmp1 = CCER_CCE_Set << (uint16_t)TIM_Channel;

	  /* Disable the Channel: Reset the CCxE Bit */
	  TIMx->CCER &= (uint16_t) ~tmp1;

	  if((TIM_Channel == TIM_CHANNEL_1) ||(TIM_Channel == TIM_CHANNEL_3))
	  {
	    tmp += (TIM_Channel>>1);

	    /* Reset the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp &= (uint32_t)~((uint32_t)TIM_CCMR1_OC1M);

	    /* Configure the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp |= TIM_OCMode;
	  }
	  else
	  {
	    tmp += (uint16_t)(TIM_Channel - (uint16_t)4)>> (uint16_t)1;

	    /* Reset the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp &= (uint32_t)~((uint32_t)TIM_CCMR1_OC2M);

	    /* Configure the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp |= (uint16_t)(TIM_OCMode << 8);
	  }

}





