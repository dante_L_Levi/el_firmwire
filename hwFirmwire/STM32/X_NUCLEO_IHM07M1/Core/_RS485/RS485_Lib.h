/*
 * RS485_Lib.h
 *
 *  Created on: Nov 29, 2021
 *      Author: Lepatenko
 */

#ifndef RS485_RS485_LIB_H_
#define RS485_RS485_LIB_H_


#include "main.h"

typedef enum
{
    ECHO_CMD=127,
	MCU_MUTE,
    BOOT_CMD,
    REQUEST_PC_CMD,
	REQUEST_PC_CONTINUE_CMD,
    RESPONSE_PC_CMD,
    REQUEST_MCU_CMD,
	RESPONSE_MCU_CMD,
    RESPONSE_MCU_CONTINUE_CMD,

}CommandDef;

#define SIZE_RX_PACKET	2+1+1+32+2//38
#define SIZE_RX_PAYLOAD			32




#pragma pack(push, 1)
typedef union{
    struct{
        uint16_t        ID;
        uint8_t         len;
        uint8_t         cmd;
        uint8_t         payload[255];
        uint16_t        crc;
    };
    uint8_t dataPack[261];
}RS485_pack;

typedef struct
{
	uint8_t length;
	uint8_t cmd;
	uint8_t *data;
	uint8_t dataRxPayload[32];

}RS485_Sprotocol_Data_def;


#pragma pack(pop)


#define SIZE_PACKET		sizeof(RS485_pack)



int32_t RS485_dataTX(uint8_t *data,int16_t Length);
void RS485_Reciever_Helper(void);
/*******************End Packet**************************/
uint8_t  RS485_rxPkgAvailable(void);
/*******************Set Status Available**************************/
void RS485_SetPkgAvailable(uint8_t st);

void RS485_SProtocol_init(uint16_t id,uint32_t Baud,
             int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length));
void SProtocol_addTxMessage(RS485_Sprotocol_Data_def *p);

void Get_RS485RxMessage(RS485_Sprotocol_Data_def *p);

void RS485_Self_Test(void);

#endif /* RS485_RS485_LIB_H_ */
