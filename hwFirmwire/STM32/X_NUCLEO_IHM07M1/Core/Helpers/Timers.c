/*
 * Timers.c
 *
 *  Created on: 29 нояб. 2021 г.
 *      Author: Lepatenko
 */

#include "Timers.h"

#define USE_BLDC

#define TIMER_COUNT		20

#define INDEX_TIM1		0
#define INDEX_TIM2		1
#define INDEX_TIM3		2
#define INDEX_TIM4		3
#define INDEX_TIM5		4
#define INDEX_TIM6		5
#define INDEX_TIM7		6
#define INDEX_TIM8		7
#define INDEX_TIM9		8
#define INDEX_TIM10		9
#define INDEX_TIM11		10
#define INDEX_TIM12		11
#define INDEX_TIM13 	12
#define INDEX_TIM14		13
#define INDEX_TIM15 	14
#define INDEX_TIM16 	15
#define INDEX_TIM17 	16
#define INDEX_TIM18 	17
#define INDEX_TIM19 	18
#define INDEX_TIM20 	19

typedef void (*callback_t)(void);
static callback_t callback_array[16] = { 0 };


static void Init_Tim1_Sync(Sync_Value_Tim freq);
static void Init_Tim2_Sync(Sync_Value_Tim freq);
static void Init_Tim3_Sync(Sync_Value_Tim freq);
static void Init_Tim15_Sync(Sync_Value_Tim freq);
static void Init_Tim16_Sync(Sync_Value_Tim freq);
static void Init_Tim17_Sync(Sync_Value_Tim freq);



typedef struct
{
	uint32_t PSC_Prescaler;
	uint32_t ARR_Period;

}config_tim_def;

config_tim_def Settings[30];

#define HANDLER_TEMPLATE(x)    \
           if (callback_array[x]) {    \
            callback_array[x]();    \
        }


void TIM1_BRK_TIM15_IRQHandler(void)
{
	TIM15->SR &= ~TIM_SR_UIF;
			HANDLER_TEMPLATE(INDEX_TIM15);
}



#ifdef USE_TIM
void TIM3_IRQHandler(void)
{
	TIM3->SR &= ~TIM_SR_UIF;
		HANDLER_TEMPLATE(INDEX_TIM3);
}


void TIM2_IRQHandler(void)
{
	TIM2->SR &= ~TIM_SR_UIF;
		HANDLER_TEMPLATE(INDEX_TIM2);
}



void TIM1_UP_TIM16_IRQHandler(void)
{
	TIM16->SR &= ~TIM_SR_UIF;
				HANDLER_TEMPLATE(INDEX_TIM16);
}

#endif

void TIM1_TRG_COM_TIM17_IRQHandler(void)
{
	TIM17->SR &= ~TIM_SR_UIF;
					HANDLER_TEMPLATE(INDEX_TIM17);
}

/******************Hundler ReadInputs******************/
void TIM6_DAC_IRQHandler(void)
{
	TIM6->SR &= ~TIM_SR_UIF;
	HANDLER_TEMPLATE(INDEX_TIM6);

}



void TIM7_DAC2_IRQHandler(void)
{
	TIM7->SR &= ~TIM_SR_UIF;
	HANDLER_TEMPLATE(INDEX_TIM7);
}





static config_tim_def Hundler_Select_Settings_Tim_main(Sync_Value_Tim _data)
{
	config_tim_def _setting_conf;

		switch(_data)
			{
				case Sync_10Hz:
				case Sync_100Hz:
				case Sync_200Hz:
				case Sync_500Hz:
				case Sync_1000Hz:
				{
					_setting_conf.PSC_Prescaler=35999;
					_setting_conf.ARR_Period=(uint8_t)(_data)-1;
					break;
				}

				case Sync_2000Hz:

				{
					_setting_conf.PSC_Prescaler=3599;
					_setting_conf.ARR_Period=(uint8_t)(_data)-1;
					break;
				}

				case Sync_4000Hz:
				{

					_setting_conf.PSC_Prescaler=359;
					_setting_conf.ARR_Period=(uint8_t)(_data)-1;
					break;
				}
			}



	return _setting_conf;
}


void Init_Timer_For_Sync(uint8_t tim,Sync_Value_Tim sync)
{
	switch(tim)
	{
		case 1:
		{
			Init_Tim1_Sync(sync);
			break;
		}

		case 2:
		{
			Init_Tim2_Sync(sync);
			break;
		}

		case 3:
		{
			Init_Tim3_Sync(sync);
			break;
		}

		case 6:
		{
			Init_Tim6_Sync(sync);
			break;
		}

		case 7:
		{
			Init_Tim7_Sync(sync);
			break;
		}

		case 15:
		{
			Init_Tim15_Sync(sync);
			break;
		}

		case 16:
		{
			Init_Tim16_Sync(sync);
			break;
		}

		case 17:
		{
			Init_Tim17_Sync(sync);
			break;
		}

	}
}



static void Init_Tim1_Sync(Sync_Value_Tim freq)
{


}
static void Init_Tim2_Sync(Sync_Value_Tim freq)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM2EN;
	Settings[INDEX_TIM2]=Hundler_Select_Settings_Tim_main(freq);
	TIM2->PSC=Settings[INDEX_TIM2].PSC_Prescaler;
	TIM2->ARR=Settings[INDEX_TIM2].ARR_Period;
	TIM2->DIER |= TIM_DIER_UIE;
	TIM2->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM2_IRQn );
	NVIC_SetPriority (TIM2_IRQn, 1);
}
static void Init_Tim3_Sync(Sync_Value_Tim freq)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	Settings[INDEX_TIM3]=Hundler_Select_Settings_Tim_main(freq);
	TIM3->PSC=Settings[INDEX_TIM3].PSC_Prescaler;
	TIM3->ARR=Settings[INDEX_TIM3].ARR_Period;
	TIM3->DIER |= TIM_DIER_UIE;
	TIM3->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM3_IRQn );
	NVIC_SetPriority (TIM3_IRQn, 1);
}
static void Init_Tim15_Sync(Sync_Value_Tim freq)
{
	RCC->APB2ENR|=RCC_APB2ENR_TIM15EN;
	Settings[INDEX_TIM15]=Hundler_Select_Settings_Tim_main(freq);
	TIM15->PSC=Settings[INDEX_TIM15].PSC_Prescaler;
	TIM15->ARR=Settings[INDEX_TIM15].ARR_Period;
	TIM15->DIER |= TIM_DIER_UIE;
	TIM15->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM15_IRQn );
	NVIC_SetPriority (TIM15_IRQn, 1);
}
static void Init_Tim16_Sync(Sync_Value_Tim freq)
{
	RCC->APB2ENR|=RCC_APB2ENR_TIM16EN;
	Settings[INDEX_TIM16]=Hundler_Select_Settings_Tim_main(freq);
	TIM16->PSC=Settings[INDEX_TIM16].PSC_Prescaler;
	TIM16->ARR=Settings[INDEX_TIM16].ARR_Period;
	TIM16->DIER |= TIM_DIER_UIE;
	TIM16->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM16_IRQn );
	NVIC_SetPriority (TIM16_IRQn, 1);
}
static void Init_Tim17_Sync(Sync_Value_Tim freq)
{
	RCC->APB2ENR|=RCC_APB2ENR_TIM17EN;
	Settings[INDEX_TIM17]=Hundler_Select_Settings_Tim_main(freq);
	TIM17->PSC=Settings[INDEX_TIM17].PSC_Prescaler;
	TIM17->ARR=Settings[INDEX_TIM17].ARR_Period;
	TIM17->DIER |= TIM_DIER_UIE;
	TIM17->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM17_IRQn );
	NVIC_SetPriority (TIM17_IRQn, 1);
}


void Init_Tim6_Sync(Sync_Value_Tim freq)
{

	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	Settings[INDEX_TIM6]=Hundler_Select_Settings_Tim_main(freq);
	TIM6->PSC=Settings[INDEX_TIM6].PSC_Prescaler;
	TIM6->ARR=Settings[INDEX_TIM6].ARR_Period;
	TIM6->DIER |= TIM_DIER_UIE;
	TIM6->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
	NVIC_SetPriority (TIM6_DAC_IRQn, 2);

}


void Init_Tim7_Sync(Sync_Value_Tim freq)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	Settings[INDEX_TIM7]=Hundler_Select_Settings_Tim_main(freq);
	TIM7->PSC=Settings[INDEX_TIM6].PSC_Prescaler;
	TIM7->ARR=Settings[INDEX_TIM6].ARR_Period;
	TIM7->DIER |= TIM_DIER_UIE;
	TIM7->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
	NVIC_SetPriority (TIM7_IRQn, 2);
}

void Init_Tim7_One_ms(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	TIM7->PSC=3600-1;
	TIM7->ARR=20;
	TIM7->DIER |= TIM_DIER_UIE;
	TIM7->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
	NVIC_SetPriority (TIM7_IRQn, 1);
}


int8_t Set_CallBack(uint8_t num,void * callback)
{
	if(num-1>TIMER_COUNT)
		return -1;
	callback_array[num-1] = (callback_t) callback;

	return 1;

}
