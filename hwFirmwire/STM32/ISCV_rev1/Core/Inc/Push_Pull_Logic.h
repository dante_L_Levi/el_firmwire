/*
 * Push_Pull_Logic.h
 *
 *  Created on: Dec 2, 2021
 *      Author: Lepatenko
 */

#ifndef INC_PUSH_PULL_LOGIC_H_
#define INC_PUSH_PULL_LOGIC_H_



#include "Headers.h"


typedef enum
{
	//todo add STATUS INIT
	STATUS_INIT,
	STATUS_NORMAL,
	STATUS_NORMAL_GEN_ON,
	STATUS_NORMAL_GEN_OFF,
	STATUS_NORMAL_Charge_GEN_OFF,//перекачка энергии из GB1 в GB2
	STATUS_TEST,

	STATUS_ERROR_Input_Voltage,//необходимо обслуживание GB1
	STATUS_ERROR_OUTPUT_Voltage,////необходимо обслуживание GB2
	STATUS_ERROR_Breakage_OUT_NET,//обрыв цепи заряда
	STATUS_ERROR_Short_OUT_NET,		//кз цепи заряда
	STATUS_ERROR_Short_GB1,//КЗ банок GB1
	STATUS_ERROR_Short_GB2,//КЗ банок GB2
	STATUS_ERROR_Low_Voltage_D,//низкое напряжение на клеме D
	STATUS_ERROR_Low_Input_Voltage,
	STATUS_ERROR_High_Input_Voltage,//высокое входное напряжение

	STATUS_ERROR
}status_work_Converter;


typedef enum
{
	CV_Work=1,
	CC_Work=2,
	None_Work=3


}status_type_Regulator;

#pragma pack(push,1)
typedef union
{
	struct
	{
		int16_t 	input_current;
		int16_t 	output_current;
		int16_t 	output_28V;
		int16_t 	output_14V;

	}fields;
	int16_t all_channel[4];

}analog1_s;
#pragma pack(pop)


#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	input_current;
		uint16_t 	output_current;
		uint16_t 	output_28V;
		uint16_t 	output_14V;

	}fields;
	uint16_t all_channel[4];

}analog1_hw_s;
#pragma pack(pop)



typedef struct
{
	float Kp;
}Settings_ADC_def;


typedef struct
{
	analog1_s				_data;
	Settings_ADC_def		_settings[4];

}analog1_sense_s;

#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	extern_in_D;
		uint16_t 	temperature_sense;


	}fields;
	uint16_t all_channel[2];

}analog2_hw_s;
#pragma pack(pop)



#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	temperature_sense;
		uint16_t 	extern_in_D;

	}fields;
	uint16_t all_channel[2];

}analog2_s;
#pragma pack(pop)




typedef struct
{
	analog2_s				_data;
	Settings_ADC_def		_settings[2];

}analog2_sense_s;


#pragma pack(push,1)
typedef union
{
	struct
	{
		uint8_t CTRL_M:1;
		uint8_t CTRL_LED:1;
		uint8_t CTRL_R:1;
		uint8_t ENA:1;
		uint8_t ENB:1;
	}fields;
	uint8_t all;
}gpio_out_s;
#pragma pack(pop)

typedef struct
{
	struct
	{
		uint8_t PROT_Current:1;
	}fields;
	uint8_t all;

	GPIO_TypeDef*			portProt;
	uint16_t				PIN_Prot;
}HAL_GPIO_EXT_CURR_Prot;

#pragma pack(push,1)
typedef struct
{
	uint16_t outDAC;
}DAC_Value_Def;
#pragma pack(pop)


#pragma pack(push,1)
typedef struct
{
	uint16_t dutyA;
	uint16_t dutyB;
}pwm_value_s;
#pragma pack(pop)


#pragma pack(push,1)
typedef struct
{
	analog1_s				_adc_value1;
	analog2_s				_adc_value2;

}ain_dataTX_s;

typedef struct
{
	ain_dataTX_s		_input;
	gpio_out_s			_inputDIN;
}TransmitData_s;
#pragma pack(pop)


#pragma pack(push,1)
typedef union
{
	struct
	{
		uint8_t GB1_percent;
		uint8_t GB2_percent;
		uint8_t Status_work;
	};
	uint8_t fields[3];
}Can_Packet1_def;

typedef union
{
	struct
	{
		int16_t  GB1_voltage;
		int16_t GB2_voltage;
		int16_t Status_work;
	};
	int16_t fields[3];
}Can_Packet2_def;


#pragma pack(pop)

typedef struct
{
	gpio_out_s 				gpio_out;
	GPIO_TypeDef*			portENA;
	GPIO_TypeDef*			portENB;
	GPIO_TypeDef*			portM;
	GPIO_TypeDef*			portLED;
	GPIO_TypeDef*			portCTRL_R;
	uint16_t				PIN_M;
	uint16_t				PIN_LED;
	uint16_t				PIN_CTRL_R;
	uint16_t				PIN_ENA;
	uint16_t				PIN_ENB;

}HAL_GPIO_OUT_def;



#define Voltage_INDEX_PID		0
#define Current_INDEX_PID		1

#pragma pack(push,1)


typedef struct
{
	float Kp;
	float Ki;
	float Kd;

	float MaxLimit;
	float MinLimit;
	float Saturation_H;
	float Saturation_L;
	float REF;

}Regulator_flash_data_s;

typedef struct
{
	uint16_t 							ID_DEVICE;
	Regulator_flash_data_s 				_datareg_s[2];



}Settings_flash_s;


#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
	uint16_t ID_temp;
	uint8_t Name[8];
	uint8_t firmwire[8];

}Transmit_Packet1_RS485;

typedef struct
{
	analog1_s						_IO_VOlt_Curr;
	analog2_s						_Temp_EXT_D;
	gpio_out_s						_gpio;
	uint8_t         				_data4_protected_pin;
	DAC_Value_Def					_dac_value;
	pwm_value_s						_pwm;
	uint8_t 						_Mode_work;



}Transmit_Packet2_RS485;


typedef struct
{
    float Kp;
    float Ki;
    float Kd;
    float Saturation_P;
    float Saturation_N;
    float MAX;
    float MIN;
    float REF;

}Setting_Reg_PID;

typedef struct
{
	Setting_Reg_PID _pid[2];


}Transmit_Packet3_RS485;


#pragma pack(pop)


typedef struct
{
	float GB1;
	float GB2;

	float Current_charge_GB2;

}push_pull_Battery_s;

typedef struct
{
	uint16_t 					DEVICE;

	Settings_flash_s			_state_config;

	status_work_Converter		status;
	status_type_Regulator		status_regulator;
	bool 						status_prot;
	bool 						RS485_Connect;

	HAL_GPIO_OUT_def			_gpio_out;
	HAL_GPIO_EXT_CURR_Prot		_prot_pin;

	DAC_Value_Def				_dac_value;
	uint16_t 					Threshold_Protected;

	analog1_sense_s				_adc_value1;
	analog2_sense_s				_adc_value2;

	analog1_hw_s				_adc1;
	analog2_hw_s				_adc2;
	pwm_value_s					_pwm_value;

	push_pull_Battery_s 		_state_Baterries;

	PID_Controller				_dataPID[2];
	uint32_t 					step_work;
	float 						duty_res;
	float 						VoltageRef;
	float 						CurrentRef;
	float 						Current_threshold;


	uint8_t 					sync_trigger;

	uint32_t 					_time_ms;
	uint32_t 					_time_start_timeout;
	timeout_s					_State_Charge_GB1_toGB2;

	Can_Packet1_def				_tx_Packet1_can;
	Can_Packet2_def				_tx_Packet2_can;
	CAN_RxHeaderTypeDef			_rx_header;
	CAN_TxHeaderTypeDef			_tx_header;
	uint32_t 					Mail_Tx_Box1;
	uint32_t 					Mail_Tx_Box2;



}work_state_model_s;

void Main_Init(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
void Push_Pull_Sync(void);
uint8_t Led_Status(void);


#endif /* INC_PUSH_PULL_LOGIC_H_ */
