/*
 * ACS723LLCTR.h
 *
 *  Created on: 6 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef OTHERLIB_ACS723LLCTR_H_
#define OTHERLIB_ACS723LLCTR_H_

#include "Headers.h"


void Set_PWR_ACS723(int16_t voltage);
void Set_Sensivity(int16_t sensivity);
void Set_Vnull_MUX(float _sensivity_Mux);
int32_t Get_Current_ACS723(int16_t Vo);


#endif /* OTHERLIB_ACS723LLCTR_H_ */
