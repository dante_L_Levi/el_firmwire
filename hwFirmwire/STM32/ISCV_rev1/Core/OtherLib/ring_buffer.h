#ifndef LIBS_RING_BUFFER_H_
#define LIBS_RING_BUFFER_H_

#include <string.h>
#include <stdbool.h>

typedef struct {
    uint8_t* data;
    int16_t len;
    int16_t tail;   //next data pull
    int16_t head;   //next data push
    bool full;
}ringBuffer_s;

inline void ringBuffer_init(ringBuffer_s* ring, uint8_t* data, int16_t length)
{
    if(length<1){
        ring->head=0;
        ring->tail=0;
        ring->len=1;
        ring->data=data;
        ring->full=true;
    }
    else{
        ring->head=0;
        ring->tail=0;
        ring->len=length;
        ring->data=data;
        ring->full=false;
    }
}

inline bool ringBuffer_isFull(ringBuffer_s* ring)
{
    return ring->full;
}

inline bool ringBuffer_isEmpty(ringBuffer_s* ring)
{
    if(ring->full)
        return false;
    return ring->head==ring->tail;
}

inline int16_t ringBuffer_count(ringBuffer_s* ring)
{
    if(ring->full)
        return ring->len;
    return (ring->head-ring->tail+ring->len)%ring->len;
}

inline int16_t ringBuffer_length(ringBuffer_s* ring)
{
    return ring->len;
}

inline int16_t ringBuffer_space(ringBuffer_s* ring)
{
    int16_t space=ring->len-ringBuffer_count(ring);
    return space;
}

inline void ringBuffer_push(ringBuffer_s* ring, uint8_t byte)
{
    if(ring->full)
        return;
    ring->data[ring->head]=byte;
    ring->head=(ring->head+1)%ring->len;
    ring->full=ring->head==ring->tail;
}

inline void ringBuffer_pushBuff(ringBuffer_s* ring, uint8_t* buff, int16_t length)
{
    if(ringBuffer_space(ring)<length)
        return;
    int16_t i;
    for(i=0;i<length;i++){
        ring->data[ring->head]=buff[i];
        ring->head=(ring->head+1)%ring->len;
    }
    ring->full=ring->head==ring->tail;
}

inline uint8_t ringBuffer_pull(ringBuffer_s* ring)
{
    if(ring->head==ring->tail){
        if(ring->full)
            ring->full=false;
        else
            return 0;
    }
    uint8_t data=ring->data[ring->tail];
    ring->tail=(ring->tail+1)%ring->len;
    return data;
}

inline void ringBuffer_pullBuff(ringBuffer_s* ring, uint8_t* buff, int16_t length)
{
    if(length<=0)
        return;
    if(ringBuffer_count(ring)<length){
        return;
    }
    int16_t i;
    for(i=0;i<length;i++){
        buff[i]=ring->data[ring->tail];
        ring->tail=(ring->tail+1)%ring->len;
    }
    ring->full=false;
}


inline void ringBuffer_pop(ringBuffer_s* ring)
{
    if(ring->head==ring->tail){
        if(ring->full)
            ring->full=false;
        else
            return;
    }
    ring->tail=(ring->tail+1)%ring->len;
}

inline void ringBuffer_popLen(ringBuffer_s* ring, int16_t length)
{
    if(ringBuffer_count(ring)<length){
        return;
    }
    ring->full=false;
    ring->tail=(ring->tail+length)%ring->len;
}

inline void ringBuffer_flush(ringBuffer_s* ring)
{
    ring->head=0;
    ring->tail=0;
    ring->full=false;
}

#endif /* LIBS_RING_BUFFER_H_ */
