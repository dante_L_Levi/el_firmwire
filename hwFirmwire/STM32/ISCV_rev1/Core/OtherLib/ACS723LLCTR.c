/*
 * ACS723LLCTR.c
 *
 *  Created on: 6 дек. 2021 г.
 *      Author: Lepatenko
 */

#include "ACS723LLCTR.h"

int16_t voltage_pwr=5000;
int16_t voltage_sensivity=40;
float Sensivity_Mux=0.5;
uint16_t Vo_null;



void Set_Vnull_MUX(float _sensivity_Mux)
{
	if(_sensivity_Mux>0)
	{
		Sensivity_Mux=_sensivity_Mux;
		Vo_null=voltage_pwr*Sensivity_Mux;
	}

	return;

}


void Set_Sensivity(int16_t sensivity)
{
	if(sensivity>0)
	{
		voltage_sensivity=sensivity;
	}

	return;

}


void Set_PWR_ACS723(int16_t voltage)
{
	if(voltage>0)
	{
		voltage_pwr=voltage;
		Vo_null=voltage_pwr*Sensivity_Mux;
	}
	return;
}


int32_t Get_Current_ACS723(int16_t Vo)
{
	float current =((float)(Vo-Vo_null)/(voltage_sensivity));
	int32_t current_out=current*1000.0;
	return current_out;
}

