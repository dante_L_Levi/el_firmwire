/*
 * Push_Pull.c
 *
 *  Created on: Dec 2, 2021
 *      Author: Lepatenko
 */

#include "Push_Pull_Logic.h"
#include "ACS723LLCTR.h"


#define PWR_ACS723			5000
#define Current_K1			-0.0784
#define Current_K2			2.5096

#define Voltage_MAX_GB		14

#define TIMEOUT_charge_mode_MS			120000


#define DAC_Threshold_Default		2855 //20A


static  const uint16_t * rs485_id =  (const uint16_t *)_board_id;


uint16_t count_pause=0;
uint8_t  count_Toogle_Led=0;
uint16_t timeout_Pause=0;
static uint16_t iter=0;
bool statusLed_R=1;
bool pause_state_indicate=false;
work_state_model_s _model_state;
uint8_t status_work=1;

bool test_mode=false;


static void update_GPIO_OUT(void);
static void Read_Adc_Sense(void);
static void Set_Enable_DRV(bool status);
//static void Test_RS485_Transmit(void);
static void update_hrtim(void);

static void Fast_Loop_push_pull_Sync(void);
static void Middle_Loop_push_pull_Sync(void);
static void update_Indicate_External(status_work_Converter status);
static void indicate_extern(uint8_t count_Led);
static void Read_Adc_Sense_2(void);
static void Can_Packet_1(void);
static void Can_Packet_2(void);
static void update_status_Charge(void);
static void Set_ToFlash_Data(void);

void Update_sync(void)
{
	_model_state.sync_trigger=1;

}


void Update_work(void)
{
	_model_state._time_ms++;
	update_status_Charge();
}


void Interrupt_Prot_Current(void)
{
	//stop pwm
	Set_Enable_DRV(false);
	Stop_Hrtim();
	_model_state.status=STATUS_ERROR;
	_model_state.status_regulator=None_Work;
	_model_state.status_prot=false;

}

void Can_Hundler_Reciever(uint8_t* data,CAN_RxHeaderTypeDef *dataHeader)
{
	//add hundler recieve

}


static void update_status_Charge(void)
{
	(_model_state._adc_value2._data.fields.extern_in_D<12000 &&
			_model_state._adc_value1._data.fields.output_14V<16000 &&
			_model_state._adc_value1._data.fields.output_14V>12000)?(_model_state._time_start_timeout=_model_state._time_ms):
					(_model_state._time_start_timeout=0);

	if(_model_state._adc_value2._data.fields.extern_in_D<12000
			&& _model_state._state_Baterries.GB1>13.200 &&
			 _model_state._state_Baterries.GB1<16.000 &&
			 _model_state._adc_value1._data.fields.output_28V>20000 &&
	(_model_state._time_ms-_model_state._time_start_timeout>=120000))
	{
		_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;
		_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;
		SetReference(_model_state.CurrentRef/4,&_model_state._dataPID[Current_INDEX_PID]);
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_SET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_SET);

	}
}


static void Settings_Analog_Input(void)
{
	_model_state._adc_value1._settings[0].Kp=1.0;
	_model_state._adc_value1._settings[1].Kp=2.0;
	_model_state._adc_value1._settings[2].Kp=5.5;
	_model_state._adc_value1._settings[3].Kp=5.5;

	_model_state._adc_value2._settings[0].Kp=5.883;
	_model_state._adc_value2._settings[1].Kp=5.55;

	Set_PWR_ACS723(PWR_ACS723);//type sensor
	Set_Sensivity(200);//sensivity mV
	Set_Vnull_MUX(0.1);//MUX for Converted

}


static void App_GetSettings_flash(void)
{
	uint8_t read_data[sizeof(Settings_flash_s)]={0};
	read_flash_Page(FLASH_STORAGE,read_data);
	memcpy(&_model_state._state_config,read_data,sizeof(Settings_flash_s));

}

void Restore_Pid_Settings(void)
{


	for(int i=0;i<2;i++)
	{
		_model_state._state_config._datareg_s[i].Kp=1.0;
		_model_state._state_config._datareg_s[i].Ki=1.0;
		_model_state._state_config._datareg_s[i].Kd=1.0;

		_model_state._state_config._datareg_s[i].MaxLimit=0.0;
		_model_state._state_config._datareg_s[i].MinLimit=0.0;
		_model_state._state_config._datareg_s[i].REF=0;
		_model_state._state_config._datareg_s[i].Saturation_H=0;
		_model_state._state_config._datareg_s[i].Saturation_L=0;


	}

	_model_state._state_config.ID_DEVICE=*rs485_id;

	_model_state.DEVICE=_model_state._state_config.ID_DEVICE;
	//todo переделать через функции
	for(int i=0;i<2;i++)
	{
		SetCoefficient_Kp(_model_state._state_config._datareg_s[i].Kp,&_model_state._dataPID[i]);
		SetCoefficient_Ki(_model_state._state_config._datareg_s[i].Ki,&_model_state._dataPID[i]);
		SetCoefficient_Kd(_model_state._state_config._datareg_s[i].Kd, &_model_state._dataPID[i]);
	}

	//write flash
	uint8_t buffer[sizeof(Settings_flash_s)]={0};
	memcpy(buffer,&_model_state._state_config,sizeof(Settings_flash_s));
	save_to_flash(buffer);


}


static void Set_ToFlash_Data(void)
{
	//write flash
	uint8_t buffer[sizeof(Settings_flash_s)]={0};
	memcpy(buffer,&_model_state._state_config,sizeof(Settings_flash_s));
	save_to_flash(buffer);
}



//todo переделать функцию проверки работы ...
static void check_work_Push_pull(void)
{
	if(_model_state.status==STATUS_TEST)
	{
		_model_state.step_work=0;
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_SET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_SET);
		_model_state._pwm_value.dutyA=0;
		_model_state._pwm_value.dutyB=0;
		HRTIM_Set_Duty(ChannelA,_model_state._pwm_value.dutyA);
		HRTIM_Set_Duty(ChannelA,_model_state._pwm_value.dutyB);
		_model_state._dac_value.outDAC=0;
		Set_DAC_Value(_model_state._dac_value.outDAC,0);


		return;
	}

	if(_model_state._state_Baterries.GB1<13.200 && _model_state._state_Baterries.GB1>5.000)
		{
			_model_state.status=STATUS_ERROR_Low_Input_Voltage;
			DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
			DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
			return;
		}
	if((_model_state._state_Baterries.GB1<5.000))
	{
		_model_state.status=STATUS_ERROR_Short_GB1;
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
		return;
	}

	if(_model_state._state_Baterries.GB1>16.000)
		{
			_model_state.status=STATUS_ERROR_High_Input_Voltage;
			DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
			DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
			return;
		}

	if(_model_state._state_Baterries.GB2<1.000)
		{
			_model_state.status=STATUS_ERROR_Breakage_OUT_NET;
			DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
			DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
			return;
		}
	if(_model_state._adc_value1._data.fields.input_current>50000||
				_model_state._adc_value1._data.fields.input_current<-10000)
		{
			_model_state.status=STATUS_ERROR_Short_GB1;
			DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
			DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
			return;
		}

	if(_model_state._state_Baterries.Current_charge_GB2>28.0)
	{
		_model_state.status=STATUS_ERROR_Short_GB2;
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
		return;

	}

	if(_model_state._state_Baterries.GB1>12.3&&_model_state._state_Baterries.GB1<16.0 &&
			_model_state._adc_value2._data.fields.extern_in_D<5000)
	{
		_model_state.status=STATUS_NORMAL_GEN_OFF;
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);
		return;

	}

	if(_model_state._adc_value2._data.fields.extern_in_D<12000 &&
			_model_state._state_Baterries.GB1>13.200 &&
						 _model_state._state_Baterries.GB1<16.000)
	{

	}

	//test 1 STATUS_NORMAL_Charge_GEN_OFF
	if(_model_state._adc_value2._data.fields.extern_in_D<12000
			&& _model_state._state_Baterries.GB1>13.200 &&
			 _model_state._state_Baterries.GB1<16.000 &&
			 _model_state._adc_value1._data.fields.output_28V>20000
	/*add check timout*/)
	{

		_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;
		SetReference(_model_state.CurrentRef/4,&_model_state._dataPID[Current_INDEX_PID]);
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_SET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_SET);
		return;
	}
	//test 2 STATUS_NORMAL_Charge_GEN_ON Main Mode
	if(_model_state._adc_value2._data.fields.extern_in_D>=12100 &&
			_model_state._adc_value1._data.fields.output_14V>13000 &&
			_model_state._adc_value1._data.fields.output_14V<16000)
	{
		_model_state.status=STATUS_NORMAL_GEN_ON;
		SetReference(_model_state.CurrentRef,&_model_state._dataPID[Current_INDEX_PID]);
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_SET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_SET);
		return;
	}



}




static void Init_Logic(void)
{
	//read flash config
	//App_GetSettings_flash();
	Restore_Pid_Settings();


	_model_state.status_prot=true;
	_model_state.RS485_Connect=false;

	_model_state._gpio_out.gpio_out.fields.CTRL_LED=0;
	_model_state._gpio_out.gpio_out.fields.CTRL_M=0;

	_model_state._gpio_out.portM=GPIO_M_GPIO_Port;
	_model_state._gpio_out.portLED=GPIO_LED_GPIO_Port;

	_model_state._gpio_out.portCTRL_R=CTRL_R_GPIO_Port;
	_model_state._gpio_out.PIN_CTRL_R=CTRL_R_Pin;

	_model_state._gpio_out.portENA=GPIO_ENA_GPIO_Port;
	_model_state._gpio_out.PIN_ENA=GPIO_ENA_Pin;

	_model_state._gpio_out.portENB=GPIO_ENB_GPIO_Port;
	_model_state._gpio_out.PIN_ENB=GPIO_ENB_Pin;


	_model_state._prot_pin.portProt=GPIO_Prot_GPIO_Port;
	_model_state._prot_pin.PIN_Prot=GPIO_Prot_Pin;

	Settings_Analog_Input();


	//set Value DAC for REF Protected

	_model_state.Threshold_Protected=DAC_Threshold_Default;
	_model_state._dac_value.outDAC=DAC_Threshold_Default;

	_model_state._pwm_value.dutyA=0;
	_model_state._pwm_value.dutyB=0;

	_model_state.CurrentRef=19.0;
	_model_state.VoltageRef=28.0;
	_model_state.Current_threshold=19.0f;

	_model_state.status_regulator=None_Work;
	_model_state._time_ms=0;


	_model_state._tx_header.IDE=CAN_ID_STD;
	_model_state._tx_header.StdId=CAN_ID_1_Main;
	_model_state._tx_header.RTR=CAN_RTR_DATA;


	_model_state._dataPID[Voltage_INDEX_PID]._configure_PID.MaxOutValue=0.45;
	_model_state._dataPID[Current_INDEX_PID]._configure_PID.MaxOutValue=0.45;

	_model_state._dataPID[Voltage_INDEX_PID]._configure_PID.MinimumOutValue=0.01;
	_model_state._dataPID[Current_INDEX_PID]._configure_PID.MinimumOutValue=0.01;



}


void Main_Init(void)
{

	_model_state.status=STATUS_INIT;
	Init_Logic();


	LED_Init();
	DIG_Pin_Init(_model_state._gpio_out.portLED,_model_state._gpio_out.PIN_LED,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);

	DIG_Pin_Init(_model_state._gpio_out.portM,_model_state._gpio_out.PIN_M,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);

	DIG_Pin_Init(_model_state._gpio_out.portCTRL_R,_model_state._gpio_out.PIN_CTRL_R,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);

	DIG_Pin_Init(_model_state._gpio_out.portCTRL_R,_model_state._gpio_out.PIN_CTRL_R,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);


	DIG_Pin_Init(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);

	DIG_Pin_Init(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,
			GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_LOW,GPIO_PIN_RESET,NONE_IRQ);



	DIG_Pin_Init(_model_state._prot_pin.portProt,_model_state._prot_pin.PIN_Prot,GPIO_MODE_IT_FALLING,
			GPIO_NOPULL,0,GPIO_PIN_RESET,EXTI15_10_IRQn);
	Set_EXT_Calback(12,Interrupt_Prot_Current);


	DIG_Pin_SET(_model_state._gpio_out.portLED,_model_state._gpio_out.PIN_LED,GPIO_PIN_RESET);
    DIG_Pin_SET(_model_state._gpio_out.portM,_model_state._gpio_out.PIN_M,GPIO_PIN_SET);//Set Connect GB1 and GB2
    DIG_Pin_SET(_model_state._gpio_out.portCTRL_R,_model_state._gpio_out.PIN_CTRL_R,GPIO_PIN_SET);//Init Not Light LED invers Logic

    _model_state.DEVICE=*rs485_id;
    RS485_SProtocol_init(*rs485_id,RS485_BAUDRATE,*RS485_dataTX);
    RS485_Reciever_Helper();

    DAC_Init();
    DAC_ENABLE(DAC_CHANNEL1);
    Set_DAC_Value(_model_state.Threshold_Protected,DAC_CHANNEL1);

    Lib_HAL_ADC_Regular();
    Init_ADC_Sense_Inject();

	Init_HRTIM_Push_Pull_Mode();

	SetReference(_model_state.VoltageRef,&_model_state._dataPID[Voltage_INDEX_PID]);
	SetReference(_model_state.CurrentRef,&_model_state._dataPID[Current_INDEX_PID]);

	SetSaturation(-0.2,0.2,&_model_state._dataPID[Voltage_INDEX_PID]);
	SetSaturation(-0.2,0.2,&_model_state._dataPID[Current_INDEX_PID]);


	Set_Period_Toggle(4000);
	Init_Tim6_Sync(FAST_LOOP);
	Set_CallBack(6,Update_sync);

	Init_Tim7_Sync(FAST2_LOOP);
	Set_CallBack(7,Update_work);

	//CAN Init
	//CAN_Init(8,CAN_BS1_5TQ,CAN_BS1_3TQ,CAN_RX0_IRQn);
	CAN_INit_HW(MX_CAN_Init);
	CAN_Set_Filter(0xFFFF,0,0xFFFF,0);
	Set_CAN_Rx_Config(_model_state._rx_header);
	Set_RxPacket_CallBack(Can_Hundler_Reciever);







}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}


/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


static void Main_Push_pull_Regulator(void)
{
	_model_state.step_work++;
	if(_model_state.status==STATUS_NORMAL||_model_state.status==STATUS_NORMAL_GEN_ON||
			_model_state.status==STATUS_NORMAL_Charge_GEN_OFF)
	{
		if(_model_state.step_work==1)
		{
			_model_state.duty_res=0.18;
			uint16_t duty=_model_state.duty_res*Get_Max_Duty();
			_model_state._pwm_value.dutyA=duty;
			_model_state._pwm_value.dutyB=duty;
			HRTIM_Set_Duty(ChannelA, _model_state._pwm_value.dutyA);
			HRTIM_Set_Duty(ChannelB, _model_state._pwm_value.dutyB);
		}

		if((float)(_model_state._adc_value1._data.fields.output_28V/1000.0)<_model_state.VoltageRef)
		{
			_model_state.status_regulator=CC_Work;
			SetFeedBack((float)(_model_state._adc_value1._data.fields.output_current/1000.0),
							&_model_state._dataPID[Current_INDEX_PID]);
		}
		if((float)(_model_state._adc_value1._data.fields.output_current/1000.0)<_model_state.Current_threshold||
				(float)(_model_state._adc_value1._data.fields.output_28V/1000.0)<=_model_state._dataPID[Voltage_INDEX_PID]._configure_PID.reference-0.5f)
		{
			_model_state.status_regulator=CV_Work;
			SetFeedBack((float)(_model_state._adc_value1._data.fields.output_28V/1000.0),
										&_model_state._dataPID[Voltage_INDEX_PID]);


		}

		switch(_model_state.status_regulator)
		{
			case CV_Work:
			{
				//regulator CV
				Compute_PID(&_model_state._dataPID[Voltage_INDEX_PID]);
				float dutyResult=GetPID_Value(&_model_state._dataPID[Voltage_INDEX_PID]);
				_model_state.duty_res+=dutyResult;
				break;
			}
			case CC_Work:
			{
				//regulator CC
				Compute_PID(&_model_state._dataPID[Current_INDEX_PID]);
				float dutyResult=GetPID_Value(&_model_state._dataPID[Current_INDEX_PID]);
				_model_state.duty_res+=dutyResult;
				break;
			}

			default:
				break;
		}

		uint16_t duty=_model_state.duty_res*Get_Max_Duty();
		_model_state._pwm_value.dutyA=duty;
		_model_state._pwm_value.dutyB=duty;
		update_hrtim();
	}
}


static void Fast_Loop_push_pull_Sync(void)
{

	Read_Adc_Sense();
	Read_Adc_Sense_2();
	if(test_mode==false)
		check_work_Push_pull();

	Main_Push_pull_Regulator();


}

static void Middle_Loop_push_pull_Sync(void)
{
	static int32_t skip_counter = 1;
	    if (skip_counter--)
	        return;
	    skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;

	    update_GPIO_OUT();
	update_Indicate_External(_model_state.status);
	//RS485_Self_Test();
	//CAN_Transmit_self_Test();

}


void Push_Pull_Sync(void)
{

	Fast_Loop_push_pull_Sync();
	Middle_Loop_push_pull_Sync();



}

static void Set_Enable_DRV(bool status)
{
	if(status)
	{
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_SET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_SET);
	}
	else
	{
		DIG_Pin_SET(_model_state._gpio_out.portENA,_model_state._gpio_out.PIN_ENA,GPIO_PIN_RESET);
		DIG_Pin_SET(_model_state._gpio_out.portENB,_model_state._gpio_out.PIN_ENB,GPIO_PIN_RESET);

	}
}

static void update_GPIO_OUT(void)
{
	_model_state._gpio_out.gpio_out.fields.CTRL_LED==1?
		(DIG_Pin_SET(_model_state._gpio_out.portLED,_model_state._gpio_out.PIN_LED,GPIO_PIN_SET)):
		(DIG_Pin_SET(_model_state._gpio_out.portLED,_model_state._gpio_out.PIN_LED,GPIO_PIN_RESET));

	_model_state._gpio_out.gpio_out.fields.CTRL_M==1?
		(DIG_Pin_SET(_model_state._gpio_out.portM,_model_state._gpio_out.PIN_M,GPIO_PIN_SET)):
		(DIG_Pin_SET(_model_state._gpio_out.portM,_model_state._gpio_out.PIN_M,GPIO_PIN_RESET));

	_model_state._gpio_out.gpio_out.fields.CTRL_R==1?
		(DIG_Pin_SET(_model_state._gpio_out.portCTRL_R,_model_state._gpio_out.PIN_CTRL_R,GPIO_PIN_SET)):
		(DIG_Pin_SET(_model_state._gpio_out.portCTRL_R,_model_state._gpio_out.PIN_CTRL_R,GPIO_PIN_RESET));


}


static void Read_Adc_Sense(void)
{
	_model_state._adc1.fields.input_current=Get_ADC_Inject_Channels_ADC_Code(0);
	_model_state._adc1.fields.output_current=Get_ADC_Inject_Channels_ADC_Code(1);
	_model_state._adc1.fields.output_28V=Get_ADC_Inject_Channels_ADC_Code(2);
	_model_state._adc1.fields.output_14V=Get_ADC_Inject_Channels_ADC_Code(3);



	//ADC1 Read converted
	_model_state._adc_value1._data.fields.input_current=(Current_K2-Get_ADC_Inject_Channels(0))/Current_K1;
	_model_state._adc_value1._data.fields.output_current=Get_Current_ACS723(Get_ADC_Inject_Channels(1)*
			_model_state._adc_value1._settings[1].Kp);
	_model_state._adc_value1._data.fields.output_28V=Get_ADC_Inject_Channels(2)*
			_model_state._adc_value1._settings[2].Kp;
	_model_state._adc_value1._data.fields.output_14V=Get_ADC_Inject_Channels(3)*
			_model_state._adc_value1._settings[3].Kp;


	//Model data Logic
	_model_state._state_Baterries.GB1=(float)_model_state._adc_value1._data.fields.output_14V/1000.0;
	_model_state._state_Baterries.GB2=((float)_model_state._adc_value1._data.fields.output_28V/1000.0)-
			_model_state._state_Baterries.GB1;
	_model_state._state_Baterries.Current_charge_GB2=(float)(_model_state._adc_value1._data.fields.output_current/1000.0);



}

static void Read_Adc_Sense_2(void)
{

	Read_ADC_regular();
	Read_regular_data(_model_state._adc2.all_channel);
	_model_state._adc_value2._data.fields.extern_in_D=((int16_t)
				((_model_state._adc2.fields.extern_in_D*3.30/4095.0)*1000.0))*
						_model_state._adc_value2._settings[0].Kp;

	_model_state._adc_value2._data.fields.temperature_sense=((int16_t)
					((_model_state._adc2.fields.temperature_sense*3.30/4095.0)*1000.0))*
							_model_state._adc_value2._settings[1].Kp;

}


static void update_hrtim(void)
{

	HRTIM_Set_Duty(ChannelA,_model_state._pwm_value.dutyA);
	HRTIM_Set_Duty(ChannelB,_model_state._pwm_value.dutyB);


}



static void update_Indicate_External(status_work_Converter status)
{
	switch(status)
	{
		case STATUS_INIT:
		{
			//function Indicate

			//function CAN Packet Transmit

			break;
		}

		case STATUS_NORMAL:
		case STATUS_NORMAL_GEN_ON:
		{
			Can_Packet_1();
			break;
		}

		case STATUS_NORMAL_GEN_OFF:
		{


			break;
		}

		case STATUS_TEST:
		{

			break;
		}

		case STATUS_NORMAL_Charge_GEN_OFF:
		{
			//function Indicate
			indicate_extern(12);
			//function CAN Packet Transmit
			Can_Packet_1();

			break;
		}

		case STATUS_ERROR_Input_Voltage:
		{
			//function Indicate
			indicate_extern(2);
			//function CAN Packet Transmit
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_OUTPUT_Voltage:
		{
			//function Indicate
			indicate_extern(3);
			//function CAN Packet Transmit
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_Breakage_OUT_NET:
		{
			HAL_GPIO_WritePin(_model_state._gpio_out.portCTRL_R,
					_model_state._gpio_out.PIN_CTRL_R, 1);
			//CAN Packet
			Can_Packet_2();
			break;
		}


		case STATUS_ERROR_Short_OUT_NET:
		{
			indicate_extern(1);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_Low_Input_Voltage:
		{
			indicate_extern(5);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_Low_Voltage_D:
		{
			indicate_extern(4);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_Short_GB1:
		{
			indicate_extern(6);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Short_GB2:
		{
			indicate_extern(7);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_High_Input_Voltage:
		{
			indicate_extern(8);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		default:
			break;

	}
}

static void Can_Packet_1(void)
{
	_model_state._tx_Packet1_can.Status_work=_model_state.status;
	_model_state._tx_Packet1_can.GB1_percent=(100*_model_state._state_Baterries.GB1)
					/Voltage_MAX_GB;
	_model_state._tx_Packet1_can.GB2_percent=(100*_model_state._state_Baterries.GB2)
						/Voltage_MAX_GB;
	_model_state._tx_header.StdId=CAN_ID_1_Main;
	_model_state._tx_header.IDE=CAN_ID_STD;
	_model_state._tx_header.RTR=CAN_RTR_DATA;
	_model_state._tx_header.DLC=sizeof(_model_state._tx_Packet1_can);
	_model_state.Mail_Tx_Box1=1;
	CAN_Transmit_Packet((uint8_t*)&_model_state._tx_Packet1_can,
				_model_state._tx_header,_model_state.Mail_Tx_Box1);
}
static void Can_Packet_2(void)
{
	_model_state._tx_Packet2_can.Status_work=_model_state.status;
	_model_state._tx_Packet2_can.GB1_voltage=(int16_t)_model_state._adc_value1._data.fields.output_14V;
	_model_state._tx_Packet1_can.GB2_percent=(int16_t)_model_state._adc_value1._data.fields.output_28V;
	_model_state._tx_header.StdId=CAN_ID_2_Main;
	_model_state._tx_header.IDE=CAN_ID_STD;
	_model_state._tx_header.RTR=CAN_RTR_DATA;
	_model_state._tx_header.DLC=sizeof(_model_state._tx_Packet2_can);
	_model_state.Mail_Tx_Box2=2;
	CAN_Transmit_Packet((uint8_t*)&_model_state._tx_Packet2_can,
			_model_state._tx_header,_model_state.Mail_Tx_Box2);
}

static void indicate_extern(uint8_t count_Led)
{

	if(pause_state_indicate)
	{
		timeout_Pause++;
		if(timeout_Pause>1000)
		{
			timeout_Pause=0;
			pause_state_indicate=!pause_state_indicate;
		}
		return;
	}

	if(!pause_state_indicate)
	{
		iter++;
			if(iter>=800)
			{
				statusLed_R=!statusLed_R;
				iter=0;
				HAL_GPIO_WritePin(_model_state._gpio_out.portCTRL_R, _model_state._gpio_out.PIN_CTRL_R,statusLed_R);
				if(statusLed_R)
					count_Toogle_Led++;
			}
	}


	if(count_Toogle_Led>=count_Led)
		pause_state_indicate=true;


}





/**************************************************************************************/


typedef enum
{
    Set_Connect=0x00,

    Get_Board=0x01,//ID,FIRMWIRE,NAME
    Get_main_data,//Vin,Vout,Iout,Iin,PWM,DAC,Mode
    Get_setting_peripheral_board,//Kp,Ki,Kd,Saturation+,Saturation-,Max,Min

    Set_Mode_Work,
    Set_DAC,
    Set_PWM,
    Set_Gpio,
    Set_setting_peripheral_board,
    Set_Default_peripheral_board,
    Set_cmd_flash

}Command_Board;



void function_Set_Connect(uint8_t *data)
{
	Set_Period_Toggle(1000);
}

void function_Get_Board(uint8_t *data)
{
	Transmit_Packet1_RS485 _packet;
	_packet.ID_temp=_model_state.DEVICE;
	memcpy(_packet.Name,model,8);
	memcpy(_packet.firmwire,firmware,8);

	RS485_Sprotocol_Data_def msg;
	msg.cmd=Get_Board;
	msg.length=sizeof(Transmit_Packet1_RS485);
	msg.data=(uint8_t*)&_packet;
	SProtocol_addTxMessage(&msg);

}

void function_Get_main_data(uint8_t *data)
{
	Transmit_Packet2_RS485 _packet;
	_packet._IO_VOlt_Curr=_model_state._adc_value1._data;
	_packet._Temp_EXT_D=_model_state._adc_value2._data;
	_packet._Mode_work=_model_state.status;
	_packet._dac_value=_model_state._dac_value;
	_packet._data4_protected_pin=(uint8_t)_model_state.status_prot;
	_packet._gpio=_model_state._gpio_out.gpio_out;
	_packet._pwm=_model_state._pwm_value;

	RS485_Sprotocol_Data_def msg;
	msg.cmd=Get_main_data;
	msg.length=sizeof(Transmit_Packet2_RS485);
	msg.data=(uint8_t*)&_packet;
	SProtocol_addTxMessage(&msg);




}

void function_Get_setting_peripheral_board(uint8_t *data)
{
	Transmit_Packet3_RS485 _packet;

	for(int i=0;i<2;i++)
	{
		_packet._pid[i].Kp=
					_model_state._dataPID[i]._configure_PID.coefficient.proportional;

			_packet._pid[i].Kd=
						_model_state._dataPID[i]._configure_PID.coefficient.derivative;

			_packet._pid[i].Ki=
						_model_state._dataPID[i]._configure_PID.coefficient.integral;

			_packet._pid[i].REF=_model_state._dataPID[i]._configure_PID.reference;
			_packet._pid[i].Saturation_N=_model_state._dataPID[i]._configure_PID.saturation.lowThershold;
			_packet._pid[i].Saturation_P=_model_state._dataPID[i]._configure_PID.saturation.highThershold;


			_packet._pid[i].MAX=_model_state._dataPID[i]._configure_PID.MaxOutValue;
			_packet._pid[i].MIN=_model_state._dataPID[i]._configure_PID.MinimumOutValue;


	}


		RS485_Sprotocol_Data_def msg;
		msg.cmd=Get_setting_peripheral_board;
		msg.length=sizeof(Transmit_Packet3_RS485);
		msg.data=(uint8_t*)&_packet;
		SProtocol_addTxMessage(&msg);




}

void function_Set_Mode_Work(uint8_t *data)
{
	test_mode=true;
	_model_state.status_regulator=false;
	//_model_state.status=(status_work_Converter)data[0];
}

void function_Set_DAC(uint8_t *data)
{
	memcpy(&_model_state._dac_value,data,sizeof(DAC_Value_Def));
	Set_DAC_Value(_model_state._dac_value.outDAC,DAC_CHANNEL1);

}

void function_Set_PWM(uint8_t *data)
{
	memcpy(&_model_state._pwm_value,data,sizeof(pwm_value_s));
	update_hrtim();

}

void function_Set_Gpio(uint8_t *data)
{
	_model_state._gpio_out.gpio_out.all=data[0];
}

void function_Set_setting_peripheral_board(uint8_t *data)
{
	Transmit_Packet3_RS485 _packet;
	memcpy(&_packet,data,sizeof(Transmit_Packet3_RS485));



	for(int i=0;i<2;i++)
	{
		_model_state._dataPID[i]._configure_PID.coefficient.proportional=
					_packet._pid[i].Kp;

			_model_state._dataPID[i]._configure_PID.coefficient.integral=
						_packet._pid[i].Ki;

			_model_state._dataPID[i]._configure_PID.coefficient.derivative=
						_packet._pid[i].Kd;

			_model_state._dataPID[i]._configure_PID.reference=
						_packet._pid[i].REF;

			_model_state._dataPID[i]._configure_PID.saturation.highThershold=
						_packet._pid[i].Saturation_P;

			_model_state._dataPID[i]._configure_PID.saturation.lowThershold=
							_packet._pid[i].Saturation_N;

			_model_state._dataPID[i]._configure_PID.MaxOutValue=
								_packet._pid[i].MAX;

			_model_state._dataPID[i]._configure_PID.MinimumOutValue=
									_packet._pid[i].MIN;




			_model_state._state_config._datareg_s[i].Kp=
					_model_state._dataPID[i]._configure_PID.coefficient.proportional;

			_model_state._state_config._datareg_s[i].Ki=
								_model_state._dataPID[i]._configure_PID.coefficient.integral;

			_model_state._state_config._datareg_s[i].Kd=
								_model_state._dataPID[i]._configure_PID.coefficient.derivative;

			_model_state._state_config._datareg_s[i].MaxLimit=
					_model_state._dataPID[i]._configure_PID.MaxOutValue;

			_model_state._state_config._datareg_s[i].MinLimit=
					_model_state._dataPID[i]._configure_PID.MinimumOutValue;

			_model_state._state_config._datareg_s[i].REF=
					_model_state._dataPID[i]._configure_PID.reference;

			_model_state._state_config._datareg_s[i].Saturation_H=
					_model_state._dataPID[i]._configure_PID.saturation.highThershold;

			_model_state._state_config._datareg_s[i].Saturation_L=
								_model_state._dataPID[i]._configure_PID.saturation.lowThershold;






	}
	Set_ToFlash_Data();



}

void function_Set_Default_peripheral_board(uint8_t *data)
{

}

void function_Set_cmd_flash(uint8_t *data)
{

}


/*==================================================================================*/
#define SIZE_NUM_CMD				11
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler) function_Set_Connect,
		(rs485_command_handler) function_Get_Board,
		(rs485_command_handler)	function_Get_main_data,
		(rs485_command_handler)	function_Get_setting_peripheral_board,

		(rs485_command_handler)function_Set_Mode_Work,
		(rs485_command_handler)function_Set_DAC,
		(rs485_command_handler)function_Set_PWM,
		(rs485_command_handler)function_Set_Gpio,
		(rs485_command_handler)function_Set_setting_peripheral_board,
		(rs485_command_handler)function_Set_Default_peripheral_board,

		(rs485_command_handler)function_Set_cmd_flash,



};


/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
			        return;

	//hard fix of data align trouble
	RS485_Sprotocol_Data_def mess;
	Get_RS485RxMessage(&mess);
	rs485_commands_array[mess.cmd](mess.dataRxPayload);
	RS485_SetPkgAvailable(0);

}

