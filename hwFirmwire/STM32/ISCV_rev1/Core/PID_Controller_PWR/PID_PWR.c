/*
 * PID_PWR.c
 *
 *  Created on: Dec 5, 2021
 *      Author: Lepatenko
 */

#include "PID_PWR.h"




void  SetReference(float ref,PID_Controller *pid)
{
	pid->_configure_PID.reference=ref;
}


void SetFeedBack(float feedback,PID_Controller *pid)
{
	pid->_configure_PID.feedback=feedback;
}

void SetCoefficient(float Kp, float Ki, float Kd, float BackSaturation, float filterDerivative,
		PID_Controller *pid)
{
	pid->_configure_PID.coefficient.proportional=Kp;
	pid->_configure_PID.coefficient.integral=Ki;
	pid->_configure_PID.coefficient.derivative=Kd;

	pid->_configure_PID.coefficient.filterDerivative=filterDerivative;
	pid->_configure_PID.coefficient.coefficientBackSaturation=BackSaturation;
}


void SetCoefficient_Kp(float Kp,PID_Controller *pid)
{
	pid->_configure_PID.coefficient.proportional=Kp;
}
void SetCoefficient_Ki(float Ki,PID_Controller *pid)
{
	pid->_configure_PID.coefficient.integral=Ki;
}
void SetCoefficient_Kd(float Kd,PID_Controller *pid)
{
	pid->_configure_PID.coefficient.derivative=Kd;
}


void  SetSaturation(float lowLimit,float highLimit,PID_Controller *pid)
{
	pid->_configure_PID.saturation.highThershold=highLimit;
	pid->_configure_PID.saturation.lowThershold=lowLimit;

}


void Compute_PID(PID_Controller *pid)
{
	pid->_mainPID.error=pid->_configure_PID.reference-pid->_configure_PID.feedback;

	pid->_mainPID.proportionalComponent=pid->_configure_PID.coefficient.proportional*pid->_mainPID.error;

	pid->_mainPID.integralComponent+=pid->_configure_PID.deltaTimeSampling*pid->_mainPID.integralFilter;
	pid->_mainPID.integralFilter=pid->_configure_PID.coefficient.integral*pid->_mainPID.error+
			pid->_configure_PID.coefficient.coefficientBackSaturation
			*(pid->_mainPID.outputPID-pid->_mainPID.tempPID);

	pid->_mainPID.derivativeFilter+=pid->_configure_PID.deltaTimeSampling*pid->_mainPID.derivativeComponent;
	pid->_mainPID.derivativeComponent=(pid->_configure_PID.coefficient.derivative*pid->_mainPID.error-
			pid->_mainPID.derivativeFilter)*pid->_configure_PID.coefficient.filterDerivative;

	pid->_mainPID.tempPID=pid->_mainPID.proportionalComponent+pid->_mainPID.integralComponent+
			pid->_mainPID.derivativeComponent;
	pid->_mainPID.outputPID=pid->_mainPID.tempPID;

	pid->_mainPID.outputPID=fmin(fmax(pid->_mainPID.outputPID,pid->_configure_PID.saturation.lowThershold),
			pid->_configure_PID.saturation.highThershold);


}


float GetPID_Value(PID_Controller *pid)
{
	return pid->_mainPID.outputPID;
}


