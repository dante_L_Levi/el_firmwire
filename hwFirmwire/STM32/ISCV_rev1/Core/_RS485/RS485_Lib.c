/*
 * RS485_Lib.c
 *
 *  Created on: Nov 29, 2021
 *      Author: Lepatenko
 */

#include "RS485_Lib.h"


#define RS485_PORT_EN					RE_GPIO_Port
#define RS485_PIN_EN					RE_Pin






#define RS485_PORT_UART					GPIOB
#define RS485_PIN_RX					GPIO_PIN_4
#define RS485_PIN_TX					GPIO_PIN_3
#define RS485_TRANSMIT HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_SET)
#define RS485_RECEIVE  HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_RESET)
#define RS485_PIN_EN_CLK			 __HAL_RCC_GPIOA_CLK_ENABLE()
#define CLOCK_EN_UART				__HAL_RCC_USART2_CLK_ENABLE()
#define PIN_TX_RX_CLK				__HAL_RCC_GPIOB_CLK_ENABLE()

static RS485_pack txPack;

static RS485_Sprotocol_Data_def rxMessage;
uint16_t ID_DEV=0x0000;

uint8_t IsMuted=0;
uint8_t IsDataAvalible=0;

#define	Serial_Obj		huart2
#define HW_HUNDLER_UART		USART2

uint8_t RX_BUFF[SIZE_RX_PACKET]={0};
uint8_t rbuff_temp[SIZE_RX_PACKET-4]={0};

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len);
/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);

static void RS485_RessiveData_Hundler(void);

int32_t (*transmitFunction)(uint8_t* data, int16_t length);




static void SProtocol_addTxMessageNoBlock(RS485_Sprotocol_Data_def *p);



 static int32_t nulTransmitFunction(uint8_t* data, int16_t length)
{
    return 0;
}




/****************Interrupt USART*********************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart==&Serial_Obj)
		{
			RS485_RessiveData_Hundler();
		}
}




static uint8_t RS485_checkId()
{
	uint16_t temp_ID=(RX_BUFF[1]<<8)|(RX_BUFF[0]);
	if(temp_ID==ID_DEV)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

static void RS485_RessiveData_Hundler(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_RX_PACKET);
	if(RX_BUFF[3]==(uint8_t)BOOT_CMD)
	{
			IsMuted=1;
	}

	if(RS485_checkId())
	{
		if(checkCRC16(RX_BUFF,SIZE_RX_PACKET))
		{
			if(RX_BUFF[3]==BOOT_CMD)
			{
				//go to boot
			}

			int j=0;
			for(int i=2;i<sizeof(rbuff_temp);i++)
			{
				rbuff_temp[j]=RX_BUFF[i];
				j++;
			}
			//memcpy(&rxMessage,(void*)rbuff_temp,sizeof(RS485_Sprotocol_Data_def));
			rxMessage.length=rbuff_temp[0];
			rxMessage.cmd=rbuff_temp[1];
			int k=2;
			for(int i=0;i<SIZE_RX_PAYLOAD;i++)
			{
				rxMessage.dataRxPayload[i]=rbuff_temp[k];
				k++;
			}
		    IsDataAvalible=1;
		}
	}

}


void RS485_Reciever_Helper(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_RX_PACKET);
}


/*******************Init Harsware*****************************/
static void UART_Init_HW(uint32_t Baud)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	 /* USART2 clock enable */
	CLOCK_EN_UART;

	PIN_TX_RX_CLK;
	    /**USART2 GPIO Configuration
	        PB3     ------> USART2_TX
	        PB4     ------> USART2_RX
	    */
	 GPIO_InitStruct.Pin = RS485_PIN_RX|RS485_PIN_TX;
	 GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	 GPIO_InitStruct.Pull = GPIO_NOPULL;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	 HAL_GPIO_Init(RS485_PORT_UART, &GPIO_InitStruct);

	 RS485_PIN_EN_CLK;
	/**RS485 ENABLE **/
	//PA1     ------> ENABLE
	 /*Configure GPIO pins : PA1  */
	 	GPIO_InitStruct.Pin = RS485_PIN_EN;
	 	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	 	GPIO_InitStruct.Pull = GPIO_NOPULL;
	 	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 	HAL_GPIO_Init(RS485_PORT_EN, &GPIO_InitStruct);

	Serial_Obj.Instance = HW_HUNDLER_UART;
	Serial_Obj.Init.BaudRate = Baud;
	Serial_Obj.Init.WordLength = UART_WORDLENGTH_8B;
	Serial_Obj.Init.StopBits = UART_STOPBITS_1;
	Serial_Obj.Init.Parity = UART_PARITY_NONE;
	Serial_Obj.Init.Mode = UART_MODE_TX_RX;
	Serial_Obj.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Serial_Obj.Init.OverSampling = UART_OVERSAMPLING_16;
	Serial_Obj.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	Serial_Obj.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&Serial_Obj) != HAL_OK)
	  {
	    //Error_Handler();
	  }

	  /* USART1 interrupt Init */
	      HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
	      HAL_NVIC_EnableIRQ(USART2_IRQn);
	    /* USER CODE BEGIN USART1_MspInit 1 */
}



void RS485_SProtocol_init(uint16_t id,uint32_t Baud,
             int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length))
{
	UART_Init_HW(Baud);
	ID_DEV=id;
	if(transmitFunctionSet==0)
	{
		transmitFunction=nulTransmitFunction;
	}
	else
	{
		transmitFunction=transmitFunctionSet;
	}

}


int32_t RS485_dataTX(uint8_t *data,int16_t Length)
{
	RS485_TRANSMIT;
	HAL_UART_Transmit(&Serial_Obj, (uint8_t*)data, Length, 0x100);
	RS485_RECEIVE;
	return 0;
}

static void SProtocol_addTxMessageNoBlock(RS485_Sprotocol_Data_def *p)
{
	txPack.ID=ID_DEV;
	txPack.cmd=p->cmd;
	txPack.len=p->length;
	if(p->length>0)
	{
		memcpy((void*)txPack.payload, p->data, p->length);
		txPack.crc=Calculate_CRC16(txPack.dataPack,txPack.len+4);
		txPack.payload[txPack.len]=(txPack.crc>>8)&0xFF;
		txPack.payload[txPack.len+1]=txPack.crc&0xFF;
		transmitFunction((void*)txPack.dataPack, txPack.len+6);
	}
}


void SProtocol_addTxMessage(RS485_Sprotocol_Data_def *p)
{
	SProtocol_addTxMessageNoBlock(p);
}



/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
			uint16_t crc = 0xFFFF;
	       uint8_t i;

	       while (len--)
	       {
	           crc ^= *data++ << 8;

	           for (i = 0; i < 8; i++)
	               crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	       }
	       return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len)
{
	uint16_t crc = 0xFFFF;
	     uint8_t i;
	     uint16_t des_buf=(data[len-1]<<8)|data[len-2];
	     uint8_t len_dst=len-2;
	     while (len_dst--)
	     {
	         crc ^= *data++ << 8;

	         for (i = 0; i < 8; i++)
	             crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	     }
	     if(crc==des_buf)
	     {
	         return  true;
	     }
	     else
	     {
	        return  false;
	     }
}



void Get_RS485RxMessage(RS485_Sprotocol_Data_def *p)
{
	memcpy(p,&rxMessage,sizeof(RS485_Sprotocol_Data_def));
}


/*******************End Packet**************************/
uint8_t  RS485_rxPkgAvailable(void)
{
	return IsDataAvalible;
}

/*******************Set Status Available**************************/
void RS485_SetPkgAvailable(uint8_t st)
{
	IsDataAvalible=st;
}


void RS485_Self_Test(void)
{
	RS485_Sprotocol_Data_def dt;
		dt.cmd=0x01;
		dt.length=1;
		uint8_t data=0xFF;
		dt.data=&data;
		SProtocol_addTxMessageNoBlock(&dt);
}





