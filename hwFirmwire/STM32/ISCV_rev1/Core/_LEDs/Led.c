/*
 * Led.c
 *
 *  Created on: 27 нояб. 2021 г.
 *      Author: Lepatenko
 */

#include "Led_Conf.h"
#include "Led.h"


static uint8_t led_count=0;
static uint8_t led_iter=0;
static uint8_t devider=12;

static LED_blink_s blinks[3];

uint32_t period;
uint32_t iter;
bool status=1;



static void Init_Led_gpio_HW(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	 /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(PORT_RED, PIN_RED, GPIO_PIN_RESET);

	  /*Configure GPIO pins : PAPin PAPin */
	    GPIO_InitStruct.Pin = PIN_RED;
	    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	    HAL_GPIO_Init(PORT_RED, &GPIO_InitStruct);


	    /*Configure GPIO pin Output Level */
	    HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_RESET);

	    	  /*Configure GPIO pins : PAPin PAPin */
	    GPIO_InitStruct.Pin = PIN_GREEN;
	    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	    HAL_GPIO_Init(PORT_GREEN, &GPIO_InitStruct);

}


//-------------------------------------------------------------------------------------
/*
 * Initialize periphery for LEDs
 */
void LED_Init(void)
{
    led_count=0;
    led_iter=0;



    blinks[0].count=2;
    blinks[0].state[0]=LED_RED;
    blinks[0].state[1]=LED_GREEN;
    blinks[0].state[2]=LED_OFF;
    blinks[0].state[3]=LED_OFF;
    blinks[0].state[4]=LED_OFF;

    blinks[1].count=3;
    blinks[1].state[0]=LED_RED;
    blinks[1].state[1]=LED_GREEN;
    blinks[1].state[2]=LED_BOTH;
    blinks[1].state[3]=LED_OFF;
    blinks[1].state[4]=LED_OFF;

    blinks[2].count=2;
    blinks[2].state[0]=LED_GREEN;
    blinks[2].state[1]=LED_OFF;
    blinks[2].state[2]=LED_OFF;
    blinks[2].state[3]=LED_OFF;
    blinks[2].state[4]=LED_OFF;

    Init_Led_gpio_HW();
    LED_Switch(LED_OFF);
}

//-------------------------------------------------------------------------------------
/*
 * Set LED on to color
 */
void LED_Switch(LED_state_e state)
{
    switch(state)
    {
       case LED_RED:
    	    HAL_GPIO_WritePin(PORT_RED, PIN_RED, GPIO_PIN_SET);
            HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_RESET);
            break;
       case LED_GREEN:
    	   HAL_GPIO_WritePin(PORT_RED, PIN_RED, GPIO_PIN_RESET);
    	   HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_SET);
            break;
       case LED_OFF:
    	   HAL_GPIO_WritePin(PORT_RED, PIN_RED, GPIO_PIN_RESET);
    	   HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_RESET);
            break;
       case LED_BOTH:
    	   HAL_GPIO_WritePin(PORT_RED, PIN_RED, GPIO_PIN_SET);
    	   HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_SET);
            break;
    }
}


//-------------------------------------------------------------------------------------
/*
 * Blink LED from Red to Green
 */
void LED_Blink_RG(uint8_t led_id,uint32_t interval)
{
    led_iter++;
    if(led_iter >= interval)
    {
        led_iter=0;
        led_count++;
        if(led_count>=blinks[led_id].count)
            led_count=0;
        LED_Switch(blinks[led_id].state[led_count]);
    }
}
//-------------------------------------------------------------------------------------


void Len_Indication_work(uint8_t status)
{
	status%2==0?(HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_RESET)):
			(HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN, GPIO_PIN_RESET));

}


void Set_Period_Toggle(uint32_t dt)
{
	period=dt;
	iter=0;
}


void Set_devider_Led(uint8_t dev)
{
	if(dev>0)
		devider=dev;

}

void Indication_Status(void)
{
	iter++;
	if(iter>=period/devider)
	{
		status=!status;
		iter=0;
		HAL_GPIO_WritePin(PORT_GREEN, PIN_GREEN,status);
	}
}

