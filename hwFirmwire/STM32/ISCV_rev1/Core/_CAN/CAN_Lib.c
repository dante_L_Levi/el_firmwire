/*
 * CAN_Lib.c
 *
 *  Created on: Dec 25, 2021
 *      Author: Lepatenko
 */

#include "CAN_Lib.h"


#define CAN_PORT_EN					CAN_ST_GPIO_Port
#define CAN_PIN_EN					CAN_ST_Pin

#define	can_Obj						hcan
#define HW_HUNDLER_can				CAN

uint8_t Rx_Buffer[8]={0};
CAN_RxHeaderTypeDef			_rx_config;

static can_rx_callback		_mainCallBack={0};

#define CAN_PORT					GPIOB
#define CAN_PIN_RX					GPIO_PIN_8
#define CAN_PIN_TX					GPIO_PIN_9
#define CAN_MASTER_EN 				HAL_GPIO_WritePin(CAN_PORT_EN, CAN_PIN_EN, GPIO_PIN_RESET)
#define CAN_MASTER_DEN  			HAL_GPIO_WritePin(CAN_PORT_EN, CAN_PIN_EN, GPIO_PIN_SET)
#define CAN_PIN_EN_CLK			 	__HAL_RCC_GPIOB_CLK_ENABLE()
#define CLOCK_EN_CAN				__HAL_RCC_CAN1_CLK_ENABLE()
#define PIN_TX_RX_CLK				__HAL_RCC_GPIOB_CLK_ENABLE()


CAN_Configure_Def  				_mainCan;

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(hcan->Instance==HW_HUNDLER_can)
	{
		HAL_CAN_GetRxMessage(can_Obj, CAN_RX_FIFO0, &_rx_config, Rx_Buffer);
		_mainCallBack(Rx_Buffer,&_rx_config);
	}
}




static void Init_GPIO_CAN(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* USER CODE BEGIN CAN_MspInit 0 */

	  /* USER CODE END CAN_MspInit 0 */
	    /* CAN clock enable */
		CLOCK_EN_CAN;

		PIN_TX_RX_CLK;
		CAN_PIN_EN_CLK;
	    /**CAN GPIO Configuration
	    PB8     ------> CAN_RX
	    PB9     ------> CAN_TX
	    */
	    GPIO_InitStruct.Pin = CAN_PIN_RX|CAN_PIN_TX;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	    GPIO_InitStruct.Alternate = GPIO_AF9_CAN;
	    HAL_GPIO_Init(CAN_PORT, &GPIO_InitStruct);
}




void CAN_Init(uint16_t prescaler,uint32_t Tb1,uint32_t Tb2,IRQn_Type typeRx)
{
	Init_GPIO_CAN();


	can_Obj.Instance = HW_HUNDLER_can;
	can_Obj.Init.Prescaler = prescaler;
	can_Obj.Init.Mode = CAN_MODE_NORMAL;
	can_Obj.Init.SyncJumpWidth = CAN_SJW_1TQ;
	can_Obj.Init.TimeSeg1 = Tb1;
	can_Obj.Init.TimeSeg2 = Tb2;
	can_Obj.Init.TimeTriggeredMode = DISABLE;
	can_Obj.Init.AutoBusOff = DISABLE;
	can_Obj.Init.AutoWakeUp = DISABLE;
	can_Obj.Init.AutoRetransmission = DISABLE;
	can_Obj.Init.ReceiveFifoLocked = DISABLE;
	can_Obj.Init.TransmitFifoPriority = DISABLE;


	 /* CAN interrupt Init */
	 HAL_NVIC_SetPriority(typeRx, 2, 0);
	 HAL_NVIC_EnableIRQ(typeRx);
	  /* USER CODE BEGIN CAN_MspInit 1 */




}


void CAN_INit_HW(void *InitFunction)
{
	_mainCan.InitFunctionSet=InitFunction;
}



void Set_CAN_Rx_Config(CAN_RxHeaderTypeDef _setting)
{
	_rx_config=_setting;
}


/***
 *
 *
 * _filter_config.FilterIdHigh-ID для проверки (сдвигается на 5 так как STD)
 * _filter_config.FilterMaskIdHigh маска для сравнения по битно
 *
 * ID RECIEVE: 			0010001010
 * FilterIdHigh:		0010001010
 * FilterMaskIdHigh:	0010001010
 * там где стоят 1 то он проверяет совпадение приходящего ID с заданным для сравнения
 */

void CAN_Set_Filter(
		uint32_t filterIdHigh,
		uint32_t filterIdLow,
		uint32_t FilterMaskIdHigh,
		uint32_t FilterMaskIdLow)
{

	_mainCan.InitFunctionSet();


	CAN_FilterTypeDef _filter_config;
	_filter_config.FilterActivation=CAN_FILTER_ENABLE;
	_filter_config.FilterBank=18;
	_filter_config.FilterFIFOAssignment=CAN_FILTER_FIFO0;
	_filter_config.FilterIdHigh=filterIdHigh<<5;
	_filter_config.FilterIdLow=0;
	_filter_config.FilterMaskIdHigh=filterIdHigh<<5;
	_filter_config.FilterMaskIdLow=0x0000;
	_filter_config.FilterMode=CAN_FILTERMODE_IDMASK;
	_filter_config.FilterScale=CAN_FILTERSCALE_32BIT;
	_filter_config.SlaveStartFilterBank=20;
	HAL_CAN_ConfigFilter(&can_Obj, &_filter_config);
	HAL_CAN_Start(&can_Obj);
	HAL_CAN_ActivateNotification(&can_Obj, CAN_IT_RX_FIFO0_MSG_PENDING);
}


void CAN_Transmit_Packet(uint8_t *data,CAN_TxHeaderTypeDef config,uint32_t MailBox)
{
	HAL_CAN_AddTxMessage(&can_Obj, &config, data, &MailBox);
}

void Set_RxPacket_CallBack(void* can_callback)
{
	_mainCallBack=(can_rx_callback)can_callback;
}

void CAN_Transmit_self_Test(void)
{
	CAN_TxHeaderTypeDef TxHeader;
	uint8_t buf[8]={0,1,2,3,4,5,6,7};
	uint32_t TxMainBox;
	TxHeader.DLC=8;
	TxHeader.IDE=CAN_ID_STD;
	TxHeader.RTR=CAN_RTR_DATA;
	TxHeader.StdId=0x446;

	HAL_CAN_AddTxMessage(&can_Obj, &TxHeader, buf, &TxMainBox);
}





