/*
 * digital_pins.h
 *
 *  Created on: 27 нояб. 2021 г.
 *      Author: Lepatenko
 */

#ifndef DIGITAL_PINS_DIGITAL_PINS_H_
#define DIGITAL_PINS_DIGITAL_PINS_H_

#include "Headers.h"

#define NONE_IRQ		0

void DIG_Pin_Init(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,uint32_t mode,uint32_t pull,uint32_t sp,uint16_t InitState,
		uint8_t IRQ_flag);

bool DIG_Pin_Read(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin);
uint32_t DIG_PORT_Read(GPIO_TypeDef* GPIOx);

void DIG_Pin_SET(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,bool status);
void DIG_Pins_SET(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,uint32_t status);

void Set_EXT_Calback(uint8_t pin,void *callback);




#endif /* DIGITAL_PINS_DIGITAL_PINS_H_ */
