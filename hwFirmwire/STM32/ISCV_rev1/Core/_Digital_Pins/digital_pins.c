/*
 * digital_pins.c
 *
 *  Created on: 27 нояб. 2021 г.
 *      Author: Lepatenko
 */

#include "digital_pins.h"

typedef void (*callback_gpio_t)(void);
static callback_gpio_t callback_array[16] = { 0 };


#define COUNT_EXT						16
#define HANDLER_TEMPLATE(x)    \
           if (callback_array[x]) {    \
            callback_array[x]();    \
        }



void EXTI0_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR0)
	{

	}
	EXTI->PR |= EXTI_PR_PR0;

}


void EXTI1_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR1)
	{

	}
	EXTI->PR |= EXTI_PR_PR1;

}


void EXTI2_TSC_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR2)
	{

	}
	EXTI->PR |= EXTI_PR_PR2;

}


void EXTI3_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR3)
	{

	}
	EXTI->PR |= EXTI_PR_PR3;

}


void EXTI4_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR4)
	{

	}
	EXTI->PR |= EXTI_PR_PR4;

}


void EXTI9_5_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR5)
	{

	}
	else if(EXTI->PR & EXTI_PR_PR6)
	{

	}
	else if(EXTI->PR & EXTI_PR_PR7)
	{

	}
	else if(EXTI->PR & EXTI_PR_PR8)
	{

	}
	else if(EXTI->PR & EXTI_PR_PR9)
	{

	}

	EXTI->PR |= EXTI_PR_PR5;
	EXTI->PR |= EXTI_PR_PR6;
	EXTI->PR |= EXTI_PR_PR7;
	EXTI->PR |= EXTI_PR_PR8;
	EXTI->PR |= EXTI_PR_PR9;

}



void EXTI15_10_IRQHandler(void)
{
		if(EXTI->PR & EXTI_PR_PR10)
		{

		}
		else if(EXTI->PR & EXTI_PR_PR11)
		{

		}
		else if(EXTI->PR & EXTI_PR_PR12)
		{

		}
		else if(EXTI->PR & EXTI_PR_PR13)
		{

		}
		else if(EXTI->PR & EXTI_PR_PR14)
		{

		}
		else if(EXTI->PR & EXTI_PR_PR15)
		{

		}


		EXTI->PR |= EXTI_PR_PR10;
		EXTI->PR |= EXTI_PR_PR11;
		EXTI->PR |= EXTI_PR_PR12;
		EXTI->PR |= EXTI_PR_PR13;
		EXTI->PR |= EXTI_PR_PR14;
		EXTI->PR |= EXTI_PR_PR15;

}






void DIG_Pin_Init(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,uint32_t mode,uint32_t pull,uint32_t sp,uint16_t InitState,
		uint8_t IRQ_flag)
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};
	  bool statust_interupt=false;
	  GPIO_InitStruct.Pin = GPIO_Pin;
	  GPIO_InitStruct.Mode = mode;
	  GPIO_InitStruct.Pull = pull;
	  if(mode==GPIO_MODE_IT_RISING||mode==GPIO_MODE_IT_FALLING||mode==GPIO_MODE_IT_RISING_FALLING)
	  	  statust_interupt=true;
	  else
	  	  GPIO_InitStruct.Speed = sp;


	  HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
	  if(statust_interupt)
	  {
		  NVIC_SetPriority(IRQ_flag, 0);
		  NVIC_EnableIRQ(IRQ_flag);
	  }

	  InitState==GPIO_PIN_SET?(HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET))
	  			  :(HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET));

	  statust_interupt=false;


}

bool DIG_Pin_Read(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin)
{
	return HAL_GPIO_ReadPin(GPIOx,GPIO_Pin);
}
uint32_t DIG_PORT_Read(GPIO_TypeDef* GPIOx)
{
	assert_param(IS_GPIO_PIN(GPIO_Pin));
	return GPIOx->ODR;
}


void DIG_Pin_SET(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,bool status)
{
	HAL_GPIO_WritePin(GPIOx,GPIO_Pin,(uint8_t)status);
}
void DIG_Pins_SET(GPIO_TypeDef* GPIOx,uint32_t GPIO_Pin,uint32_t status)
{
	assert_param(IS_GPIO_PIN(GPIO_Pin));
	GPIOx->ODR|=status;
}


void Set_EXT_Calback(uint8_t pin,void *callback)
{
	if(pin-1>COUNT_EXT)
		return;

	callback_array[pin]=(callback_gpio_t) callback;
}







