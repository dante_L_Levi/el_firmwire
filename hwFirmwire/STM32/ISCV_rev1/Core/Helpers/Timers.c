/*
 * Timers.c
 *
 *  Created on: 29 нояб. 2021 г.
 *      Author: Lepatenko
 */

#include "Timers.h"

#define TIMER_COUNT		18

#define INDEX_TIM1		0
#define INDEX_TIM2		1
#define INDEX_TIM3		2
#define INDEX_TIM4		3
#define INDEX_TIM5		4
#define INDEX_TIM6		5
#define INDEX_TIM7		6
#define INDEX_TIM8		7
#define INDEX_TIM9		8
#define INDEX_TIM10		9
#define INDEX_TIM11		10
#define INDEX_TIM12		11
#define INDEX_TIM13 	12
#define INDEX_TIM14		13
#define INDEX_TIM15 	14


typedef void (*callback_t)(void);
static callback_t callback_array[16] = { 0 };

typedef struct
{
	uint32_t PSC_Prescaler;
	uint32_t ARR_Period;

}config_tim_def;

config_tim_def Settings[15];

#define HANDLER_TEMPLATE(x)    \
           if (callback_array[x]) {    \
            callback_array[x]();    \
        }




/******************Hundler ReadInputs******************/
void TIM6_DAC_IRQHandler(void)
{
	TIM6->SR &= ~TIM_SR_UIF;
	HANDLER_TEMPLATE(INDEX_TIM6);

}


void TIM7_DAC2_IRQHandler(void)
{
	TIM7->SR &= ~TIM_SR_UIF;
	HANDLER_TEMPLATE(INDEX_TIM7);
}





static config_tim_def Hundler_Select_Settings_Tim_main(Sync_Value_Tim _data)
{
	config_tim_def _setting_conf;

		switch(_data)
			{
				case Sync_10Hz:
				case Sync_100Hz:
				case Sync_200Hz:
				case Sync_500Hz:
				case Sync_1000Hz:
				{
					_setting_conf.PSC_Prescaler=35999;
					_setting_conf.ARR_Period=_data;
					break;
				}

				case Sync_2000Hz:

				{
					_setting_conf.PSC_Prescaler=3599;
					_setting_conf.ARR_Period=_data;
					break;
				}

				case Sync_4000Hz:
				{
					//todo edit with clock=20Mhz
					//_setting_conf.PSC_Prescaler=2999;
					//_setting_conf.ARR_Period=5;

					_setting_conf.PSC_Prescaler=3599;
					_setting_conf.ARR_Period=_data;
					break;
				}
			}



	return _setting_conf;
}



void Init_Tim6_Sync(Sync_Value_Tim freq)
{

	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	Settings[INDEX_TIM6]=Hundler_Select_Settings_Tim_main(freq);
	TIM6->PSC=Settings[INDEX_TIM6].PSC_Prescaler;
	TIM6->ARR=Settings[INDEX_TIM6].ARR_Period;
	TIM6->DIER |= TIM_DIER_UIE;
	TIM6->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
	NVIC_SetPriority(TIM6_DAC_IRQn,1);

}


void Init_Tim7_Sync(Sync_Value_Tim freq)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	Settings[INDEX_TIM7]=Hundler_Select_Settings_Tim_main(freq);
	TIM7->PSC=Settings[INDEX_TIM6].PSC_Prescaler;
	TIM7->ARR=Settings[INDEX_TIM6].ARR_Period;
	TIM7->DIER |= TIM_DIER_UIE;
	TIM7->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
	NVIC_SetPriority(TIM7_IRQn,1);
}


int8_t Set_CallBack(uint8_t num,void * callback)
{
	if(num-1>TIMER_COUNT)
		return -1;
	callback_array[num-1] = (callback_t) callback;

	return 1;

}
