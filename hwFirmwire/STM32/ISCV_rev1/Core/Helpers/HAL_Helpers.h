/*
 * HAL_Helpers.h
 *
 *  Created on: Dec 19, 2021
 *      Author: Lepatenko
 */

#ifndef HELPERS_HAL_HELPERS_H_
#define HELPERS_HAL_HELPERS_H_


#include "Headers.h"



bool float_Window(float lowThreshold,float HighThreshold,float value);


#endif /* HELPERS_HAL_HELPERS_H_ */
