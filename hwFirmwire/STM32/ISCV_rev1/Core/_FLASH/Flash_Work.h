/*
 * Flash_Work.h
 *
 *  Created on: Dec 5, 2021
 *      Author: Lepatenko
 */

#ifndef FLASH_FLASH_WORK_H_
#define FLASH_FLASH_WORK_H_



#include "Headers.h"

/********************  FLASH_Error_Codes   ***********************//*
HAL_FLASH_ERROR_NONE      0x00U  // No error
HAL_FLASH_ERROR_PROG      0x01U  // Programming error
HAL_FLASH_ERROR_WRP       0x02U  // Write protection error
HAL_FLASH_ERROR_OPTV      0x04U  // Option validity error
*/



#define page_size FLASH_PAGE_SIZE

uint32_t Flash_Write_Data (uint32_t StartPageAddress, uint32_t *Data, uint16_t numberofwords);
void Flash_Read_Data (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords);
void Convert_To_Str (uint32_t *Data, char *Buf);
void Flash_Write_NUM (uint32_t StartSectorAddress, float Num);
float Flash_Read_NUM (uint32_t StartSectorAddress);


void save_to_flash(uint8_t *data);
void read_flash(uint8_t* data);

void save_to_flash_Page(uint32_t ADDR_Page,uint8_t *data);
void read_flash_Page(uint32_t ADDR_Page, uint8_t* data);

#endif /* FLASH_FLASH_WORK_H_ */
