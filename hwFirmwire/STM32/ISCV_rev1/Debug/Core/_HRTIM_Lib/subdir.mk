################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/_HRTIM_Lib/pwm_generate.c 

OBJS += \
./Core/_HRTIM_Lib/pwm_generate.o 

C_DEPS += \
./Core/_HRTIM_Lib/pwm_generate.d 


# Each subdirectory must supply rules for building sources it contributes
Core/_HRTIM_Lib/%.o: ../Core/_HRTIM_Lib/%.c Core/_HRTIM_Lib/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F334x8 -DDEBUG -c -I../Core/_LEDs -I../Core/_RS485 -I../Core/_DAC -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Core/Helpers -I../Core/OtherLib -I../Core/_ADC -I../Drivers/CMSIS/Include -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Core/_Digital_Pins -I../Core/Inc -I../Core/_FLASH -I../Core/PID_Controller_PWR -I../Core/_HRTIM_Lib -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Core/_CAN -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-_HRTIM_Lib

clean-Core-2f-_HRTIM_Lib:
	-$(RM) ./Core/_HRTIM_Lib/pwm_generate.d ./Core/_HRTIM_Lib/pwm_generate.o

.PHONY: clean-Core-2f-_HRTIM_Lib

