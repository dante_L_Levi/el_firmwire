################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Logic/Logic_IHM07M1.c \
../Core/Logic/hal.c 

OBJS += \
./Core/Logic/Logic_IHM07M1.o \
./Core/Logic/hal.o 

C_DEPS += \
./Core/Logic/Logic_IHM07M1.d \
./Core/Logic/hal.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Logic/%.o: ../Core/Logic/%.c Core/Logic/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F401xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Core/_ADC -I../Core/_FLASH -I../Core/_RS485 -I../Core/Helpers -I../Core/_LEDs -I../Core/_PWM -I../Core/Logic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Logic

clean-Core-2f-Logic:
	-$(RM) ./Core/Logic/Logic_IHM07M1.d ./Core/Logic/Logic_IHM07M1.o ./Core/Logic/hal.d ./Core/Logic/hal.o

.PHONY: clean-Core-2f-Logic

