################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/_ADC/ADC_DMA_regular.c \
../Core/_ADC/ADC_Sense.c 

OBJS += \
./Core/_ADC/ADC_DMA_regular.o \
./Core/_ADC/ADC_Sense.o 

C_DEPS += \
./Core/_ADC/ADC_DMA_regular.d \
./Core/_ADC/ADC_Sense.d 


# Each subdirectory must supply rules for building sources it contributes
Core/_ADC/%.o: ../Core/_ADC/%.c Core/_ADC/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F401xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Core/_ADC -I../Core/_FLASH -I../Core/_RS485 -I../Core/Helpers -I../Core/_LEDs -I../Core/_PWM -I../Core/Logic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-_ADC

clean-Core-2f-_ADC:
	-$(RM) ./Core/_ADC/ADC_DMA_regular.d ./Core/_ADC/ADC_DMA_regular.o ./Core/_ADC/ADC_Sense.d ./Core/_ADC/ADC_Sense.o

.PHONY: clean-Core-2f-_ADC

