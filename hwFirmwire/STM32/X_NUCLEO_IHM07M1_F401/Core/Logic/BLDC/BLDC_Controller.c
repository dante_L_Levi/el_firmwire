/*
 * BLDC_Controller.c
 *
 *  Created on: 14 апр. 2022 г.
 *      Author: Lepatenko
 */

#include "BLDC_Controller.h"
#include "Logic_IHM07M1.h"
#include "hal.h"

#define BLDC_SPEED_TIMER_PRESCALER	21
#define BLDC_SPEED_TIMER_PERIOD	0xFFFF


#define TEST_SPEED_BLDC				200


#define BLDC_TIMING_INIT 10 // timing in degrees 0..30


static void Bldc_SpeedTimer_Init_Bemf(void);
static uint8_t BLDC_MotorSpeedIsOK(void);
static uint8_t BLDC_Next_Step(void);
void BLDC_MotorCommutation_Bemf(uint16_t pos);

void Bldc_Timer3_Status(bool status);
void Bldc_Timer4_Status(bool status);

static void BLDC_MotorCommutation_PWM_TopKeys(uint16_t pos);
static void BLDC_MotorCommutation_PWM_BottomKeys(uint16_t pos);
static void BLDC_MotorCommutation_PWM_ComplementaryKeys(uint16_t pos);

bool IsTim1_InterruptFlag=false;
bool IsTim4_InterruptFlag=false;
bool IsTim3_InterruptFlag=false;

typedef struct
{

	Motor_Control_Type 		_M_ctrl;
	Motor_Type				_M_type;

	struct
	{
		uint16_t BLDC_Speed;
		uint16_t BLDC_Speed_prev;
		uint8_t BLDC_COMP_PHASE_STATE;
		uint8_t BEMF_TIMEOUT_COUNTER;
		uint8_t BLDC_MotorSpin;
		uint8_t BLDC_CURRENT_STEP;
		uint8_t BLDC_TIMING;
		uint8_t BLDC_STATE[6];
		uint8_t BLDC_STATE_PREV[6];
		uint8_t BLDC_PHASE_STATE[6];

		Mode_Comutation_BLDC_Motor _comutation_mode;

	}BLDC_Bemf_Settings;




}Settings_Motor;


// BLDC motor steps table
static const uint8_t BLDC_BRIDGE_STATE_SL[6][6] =
{
//	UH,UL		VH,VL	WH,WL
   { 0,1	,	1,0	,	0,0 },
   { 0,1	,	0,0	,	1,0 },
   { 0,0	,	0,1	,	1,0 },
   { 1,0	,	0,1	,	0,0 },
   { 1,0	,	0,0	,	0,1 },
   { 0,0	,	1,0	,	0,1 },
};

Settings_Motor _main_setting_motor;

static uint8_t Programm_Compare_All(uint16_t *data,uint16_t Threshold,uint16_t offset)
{
	uint8_t result=0x00;
	((data[0]<=Threshold)||(data[0]<=Threshold+offset))?(result|=0x01):(result&=~0x01);
	((data[1]<=Threshold)||(data[1]<=Threshold+offset))?(result|=0x02):(result&=~0x02);
	((data[2]<=Threshold)||(data[2]<=Threshold+offset))?(result|=0x04):(result&=~0x04);
	return result;
}

void Bldc_Tim1_Async(void)
{
	if(IsTim1_InterruptFlag)
	{
		uint8_t STATE=0;
		uint16_t COUNT=TIM1->CNT;
		uint16_t tempData=BLDC_CHOPPER_PERIOD/2;
		if(COUNT<tempData)
		{
				//get ADC Value Bemf1-3

			if(_main_setting_motor.BLDC_Bemf_Settings.BEMF_TIMEOUT_COUNTER>BEMF_TIMEOUT)
			{
				//State=Converted ADC value to Treshold Compare Programming(SET,RESET)
				uint16_t dataADC[3]={0};
				Get_ADC_Bemf_Phase(dataADC);
				uint16_t Threshold=Get_VBus()/2;
				STATE= Programm_Compare_All(dataADC,Threshold,100);
				if((_main_setting_motor.BLDC_Bemf_Settings.BLDC_COMP_PHASE_STATE!=STATE)&
						(STATE==_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP]))
				{
					_main_setting_motor.BLDC_Bemf_Settings.BLDC_COMP_PHASE_STATE=STATE;
					_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed_prev=_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed;
					_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed=TIM1->CNT;
					Bldc_Timer3_Status(true);
					TIM3->CNT=0;
					// It requires at least two measurement to correct calculate the rotor speed
					if (BLDC_MotorSpeedIsOK())
					{
						if((_main_setting_motor.BLDC_Bemf_Settings.BLDC_TIMING>0)&
								(_main_setting_motor.BLDC_Bemf_Settings.BLDC_TIMING<31))
						{
							TIM2->ARR=1+(_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed/2)-
									_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed/
									(60/_main_setting_motor.BLDC_Bemf_Settings.BLDC_TIMING);

						}
						else
						{
							TIM2->ARR=1+(_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed/2);
						}

						TIM2->CNT=0;
						Bldc_Timer4_Status(true);
					}
				}

			}
			else
			{
				_main_setting_motor.BLDC_Bemf_Settings.BEMF_TIMEOUT_COUNTER++;
			}
		}
	}
	IsTim1_InterruptFlag=false;
}


static uint8_t BLDC_MotorSpeedIsOK(void)
{

	return ((_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed_prev > 0) &
			(_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed > 0));
}

void Bldc_Tim3_Async(void)
{
	if(IsTim3_InterruptFlag)
	{
		if (BLDC_MotorSpeedIsOK())
		{
			Stop_Motor();
		}


	}
	IsTim3_InterruptFlag=false;
}


void Bldc_Tim4_Async(void)
{
	if(IsTim4_InterruptFlag)
	{

		Bldc_Timer4_Status(false);
		if(_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin==BLDC_START)
			BLDC_MotorCommutation_Bemf(BLDC_Next_Step());

	}
	IsTim4_InterruptFlag=false;
}

void TIM3_IRQHandler(void)
{
	TIM3->SR &= ~TIM_SR_UIF;
	IsTim3_InterruptFlag=true;

}

void TIM4_IRQHandler(void)
{
	TIM4->SR &= ~TIM_SR_UIF;
	IsTim4_InterruptFlag=true;
}

void TIM1_UP_TIM10_IRQHandler(void)
{
	TIM1->SR &= ~TIM_SR_UIF;

	IsTim1_InterruptFlag=true;
}

void _Init_Motor_Logic(Motor_Control_Type ctrl,Motor_Type typeM,Mode_Comutation_BLDC_Motor _state)
{
	_main_setting_motor._M_ctrl=ctrl;
		_main_setting_motor._M_type=typeM;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed=0;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed_prev=0;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_COMP_PHASE_STATE=0;
		_main_setting_motor.BLDC_Bemf_Settings.BEMF_TIMEOUT_COUNTER=0;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin=0;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP=0;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_TIMING=BLDC_TIMING_INIT;

		for(int i=0;i<6;i++)
			_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE[0]=0;

		for(int i=0;i<6;i++)
		{
			if(i%2==0)
			{
				_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[i]=0;
			}
			else
			{
				_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[i]=1;
			}
		}



		switch(_main_setting_motor._M_ctrl)
		{
			case Step_Mode_Bemf:
			{
				BLDC_PWMTimerInit();
				Bldc_SpeedTimer_Init_Bemf();

				break;
			}

			case Step_Mode_Hall:
			{

				break;
			}
			case FOC_Mode:
			{

				break;
			}


		}

		_main_setting_motor.BLDC_Bemf_Settings._comutation_mode=_state;
}



void Bldc_Timer3_Status(bool status)
{
	(status==true)?(TIM3->CR1 |= TIM_CR1_CEN):(TIM3->CR1 &= ~TIM_CR1_CEN);
}

void Bldc_Timer4_Status(bool status)
{
	(status==true)?(TIM4->CR1 |= TIM_CR1_CEN):(TIM4->CR1 &= ~TIM_CR1_CEN);
}





void Bldc_SpeedTimer_Init_Bemf(void)
{
	//Init Timer3
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM3->PSC=BLDC_SPEED_TIMER_PRESCALER;
	TIM3->ARR=BLDC_SPEED_TIMER_PERIOD;
	TIM3->DIER |= TIM_DIER_UIE;
	TIM3->CNT=0;
	NVIC_EnableIRQ(TIM3_IRQn );
	NVIC_SetPriority (TIM3_IRQn, 0);

	RCC->APB1ENR|=RCC_APB1ENR_TIM4EN;
	TIM4->PSC=BLDC_SPEED_TIMER_PRESCALER;
	TIM4->ARR=BLDC_SPEED_TIMER_PERIOD;
	TIM4->DIER |= TIM_DIER_UIE;
	TIM4->CNT=0;

	NVIC_EnableIRQ(TIM4_IRQn );
	NVIC_SetPriority (TIM4_IRQn, 0);


}


void BLDC_PWMTimerInit(void)
{
	Init_Base_Tim1(MX_TIM1_Init,Single);
	TIM1->DIER|=TIM_DIER_UIE;
	NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn );
	NVIC_SetPriority (TIM1_UP_TIM10_IRQn, 1);
	TIM1->CR1 |=TIM_CR1_CEN;
	HAL_TIM_Base_Start_IT(&htim1);
	Start_Tim1_Pwm(TIM_CHANNEL_1);
	Start_Tim1_Pwm(TIM_CHANNEL_2);
	Start_Tim1_Pwm(TIM_CHANNEL_3);
	BLDC_Set_PWM_Duty(1);
}

void BLDC_Set_PWM_Duty(uint16_t Duty)
{
	Set_Duty_Tim1_pwm(TIM_CHANNEL_1,Duty);
	Set_Duty_Tim1_pwm(TIM_CHANNEL_2,Duty);
	Set_Duty_Tim1_pwm(TIM_CHANNEL_3,Duty);
}


void Start_Motor(void)
{
	_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin=BLDC_STOP;
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP=0;
		BLDC_MotorCommutation_Bemf(_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP);
		BLDC_Set_PWM_Duty(1000);

		for (int i=0; i < BLDC_START_STEPS; i++)
		{
			BLDC_MotorCommutation_Bemf(BLDC_Next_Step());
			if (i==BLDC_START_STEPS-1)
			{
				_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin=BLDC_START;
				break;
			}

		}
}

void Stop_Motor(void)
{
	BLDC_Set_PWM_Duty(0);
	Stop_Tim1_Pwm(TIM_CHANNEL_1);
	Stop_Tim1_Pwm(TIM_CHANNEL_2);
	Stop_Tim1_Pwm(TIM_CHANNEL_3);

	_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin=BLDC_STOP;

	memset(_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV,
			0,
			sizeof(_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV));
	Bldc_Timer3_Status(false);
	Bldc_Timer4_Status(false);
	_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed=0;
	_main_setting_motor.BLDC_Bemf_Settings.BLDC_Speed_prev=0;
	_main_setting_motor.BLDC_Bemf_Settings.BEMF_TIMEOUT_COUNTER=0;

}

uint8_t BLDC_MotorGetSpin(void)
{
	return _main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin;
}

void BLDC_MotorSetSpin(uint8_t spin)
{
	_main_setting_motor.BLDC_Bemf_Settings.BLDC_MotorSpin=spin;
}


static uint8_t BLDC_Next_Step(void)
{
	_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP++;
	if(_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP>5)
	{
		_main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP=0;
	}

	return _main_setting_motor.BLDC_Bemf_Settings.BLDC_CURRENT_STEP;
}



static void BLDC_MotorCommutation_PWM_TopKeys(uint16_t pos)
{

}

static void BLDC_MotorCommutation_PWM_BottomKeys(uint16_t pos)
{
	memcpy(_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE,BLDC_BRIDGE_STATE_SL[pos],
			sizeof(_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE));

	// Disable if need
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UH]) TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Disable);
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UL]) TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Disable);
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VH]) TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Disable);
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VL]) TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Disable);
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WH]) TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Disable);
	if (!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WL]) TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Disable);


	// Enable if need. If previous state is Enabled then not enable again. Else output do flip-flop.
	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_UH])
	{
		TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_ForcedAction_Active);
		TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable);
	}

	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_UL])
	{
			TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_OCMode_PWM1);
			TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable);
	}
	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_VH])
	{
			TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_ForcedAction_Active);
			TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable);
	}
	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_VL])
	{
		TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_OCMode_PWM1);
			TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);
	}
	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_WH])
	{
			TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_ForcedAction_Active);
			TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable);
	}
	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WL] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WH] &
			!_main_setting_motor.BLDC_Bemf_Settings.BLDC_STATE_PREV[index_WL])
	{
		TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_OCMode_PWM1);
		TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable);
	}




}

static void BLDC_MotorCommutation_PWM_ComplementaryKeys(uint16_t pos)
{
	memcpy(_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE,BLDC_BRIDGE_STATE_SL[pos],
				sizeof(_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE));


	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UH]) {
			TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_OCMode_PWM1);
			TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable);
			//EN1 ON
			Set_Enable(1,true);
			//TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable);
		} else {
			// Low side FET: OFF
			TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Disable);
			if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_UL]){
				// High side FET: ON
				//On EN1
				Set_Enable(1,true);
				//TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_ForcedAction_Active);
				//TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable);

			} else {
				// High side FET: OFF
				//off EN1
				Set_Enable(1,false);
				//TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Disable);
			}
		}

	//------------------------------------------------

	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VH]) {
			TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_OCMode_PWM1);
			TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable);
			//ON EN2
			Set_Enable(2,true);
			//TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);
		} else {
			// Low side FET: OFF
			TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Disable);
			if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_VL]){
				// High side FET: ON
				//ON EN2
				Set_Enable(2,true);
				//TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_ForcedAction_Active);
				//TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);
			} else {
				// High side FET: OFF
				Set_Enable(2,false);
				//TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Disable);

			}
		}

	//---------------------------------------------------

	if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WH]) {
			TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_OCMode_PWM1);
			TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable);
			//ON EN2
			Set_Enable(3,true);
			//TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable);
		} else {
			// Low side FET: OFF
			TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Disable);
			if (_main_setting_motor.BLDC_Bemf_Settings.BLDC_PHASE_STATE[index_WL]){
				// High side FET: ON
				//ON EN2
				Set_Enable(3,true);
				//TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_ForcedAction_Active);
				//TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable);
			} else {
				// High side FET: OFF
				Set_Enable(3,false);
				TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Disable);
			}
		}


	_main_setting_motor.BLDC_Bemf_Settings.BEMF_TIMEOUT_COUNTER=0;
}

void BLDC_MotorCommutation_Bemf(uint16_t pos)
{

	switch(_main_setting_motor.BLDC_Bemf_Settings._comutation_mode)
	{
		case  PWM_TopKeys:
		{
			BLDC_MotorCommutation_PWM_TopKeys(pos);
			break;
		}

		case  PWM_BottomKeys:
		{
			BLDC_MotorCommutation_PWM_BottomKeys(pos);
			break;
		}

		case  PWM_ComplementaryKeys:
		{
			BLDC_MotorCommutation_PWM_ComplementaryKeys(pos);
			break;
		}
		default:
		{

			break;
		}
	}

}
