/*
 * Logic_IHM07M1.c
 *
 *  Created on: Mar 7, 2022
 *      Author: Lepatenko
 */


#include "Logic_IHM07M1.h"
#include "BLDC/BLDC_Controller.h"


work_state_model_s _model_state={0};
uint16_t buffer_dma_Adc[9]={0};
volatile uint8_t adcConversionComplete=0;


static void Analog_read(void);
static void Update_sync(void);
static void work_time_ms(void);
static void Fast_Loop(void);
static void Middle_Loop(void);
static void Init_Logic(void);


static void Update_sync(void)
{
	_model_state.sync_trigger=1;
}

static void work_time_ms(void)
{
	_model_state._time_ms++;
}



static void Init_Logic(void)
{
	uint8_t dataBUFF[4]={0};
	uint32_t data_test=ReadBoardID(FLASH_ID_ADDR);
	dataBUFF[3]=(data_test&0xff000000)>>24;
	dataBUFF[2]=(data_test&0x00ff0000)>>16;
	dataBUFF[1]=(data_test&0x0000ff00)>>8;
	dataBUFF[0]=(data_test&0x000000ff);
	_model_state.ID=(uint16_t)(dataBUFF[1]<<8)|(dataBUFF[0]);



	_model_state._setting_adc_out.fields.BEMF1_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.BEMF2_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.BEMF3_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.POT_SPEED_Kp=0.805;//mV
	_model_state._setting_adc_out.fields.VBUS_Kp=15.189;//mV

}

void Main_Init(void)
{
	WriteBoardID(FLASH_ID_ADDR,(uint32_t*)_board_id, 1);
	Init_Logic();


	MX_GPIO_Init();
	Set_Period_Toggle(8000);
	LED_Init();

	Set_CallBack(5,Update_sync);
	Init_Timer_For_Sync(5,Sync_4000Hz);

	Set_CallBack(11,work_time_ms);
	Init_Timer_For_Sync(11,Sync_1000Hz);

	ADC_DMA_SetCallBack(MX_ADC1_Init,MX_DMA_Init);
	ADC_DMA_Init();
	ADC_DMA_Event(&hadc1,(uint32_t*)(buffer_dma_Adc), 9);

	_Init_Motor_Logic(Step_Mode_Bemf,BLDC_Motor,PWM_BottomKeys);
	BLDC_PWMTimerInit();

}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


static void Analog_read(void)
{
	for(int i=0;i<9;i++)
	{
		_model_state._adc1_hw.All_adc_data_hw[i]=buffer_dma_Adc[i];
	}


	for(int i=0;i<9;i++)
	{
		_model_state.adc1.All_adc_data[i]=_model_state._adc1_hw.All_adc_data_hw[i]*
				_model_state._setting_adc_out.setting_K[i];
	}


}

static void Fast_Loop(void)
{
	Analog_read();
}

static void Middle_Loop(void)
{
	static int32_t skip_counter = 1;
			    if (skip_counter--)
			        return;
			    skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;


}



void IHM07M1_Sync(void)
{
	Fast_Loop();
	Middle_Loop();
}





void RS485_Command_Update_Async()
{

}

