/*
 * hal.c
 *
 *  Created on: 11 апр. 2022 г.
 *      Author: Lepatenko
 */

#include "hal.h"
#include "Logic_IHM07M1.h"

#define LED_EXT_ON()		HAL_GPIO_WritePin(EXT_LED_GPIO_Port, EXT_LED_Pin, GPIO_PIN_SET)
#define LED_EXT_OFF()		HAL_GPIO_WritePin(EXT_LED_GPIO_Port, EXT_LED_Pin, GPIO_PIN_RESET)

#define EN1_ON()			HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin, GPIO_PIN_SET)
#define EN1_OFF()			HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin, GPIO_PIN_RESET)

#define EN2_ON()			HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin, GPIO_PIN_SET)
#define EN2_OFF()			HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin, GPIO_PIN_RESET)

#define EN3_ON()			HAL_GPIO_WritePin(EN3_GPIO_Port, EN3_Pin, GPIO_PIN_SET)
#define EN3_OFF()			HAL_GPIO_WritePin(EN3_GPIO_Port, EN3_Pin, GPIO_PIN_RESET)


#define BEMF_EN()			HAL_GPIO_WritePin(GPIO_BEMF_GPIO_Port, GPIO_BEMF_Pin, GPIO_PIN_SET)
#define BEMF_DEN()			HAL_GPIO_WritePin(GPIO_BEMF_GPIO_Port, GPIO_BEMF_Pin, GPIO_PIN_RESET)



#define DIG_EN_ON()			HAL_GPIO_WritePin(DIG_EN_GPIO_Port, DIG_EN_Pin, GPIO_PIN_SET)
#define DIG_EN_OFF()		HAL_GPIO_WritePin(DIG_EN_GPIO_Port, DIG_EN_Pin, GPIO_PIN_RESET)


#define GPIO1_ON()			HAL_GPIO_WritePin(GPIO1_GPIO_Port, GPIO1_Pin, GPIO_PIN_SET)
#define GPIO1_OFF()			HAL_GPIO_WritePin(GPIO1_GPIO_Port, GPIO1_Pin, GPIO_PIN_RESET)

#define GPIO2_ON()			HAL_GPIO_WritePin(GPIO2_GPIO_Port, GPIO2_Pin, GPIO_PIN_SET)
#define GPIO2_OFF()			HAL_GPIO_WritePin(GPIO2_GPIO_Port, GPIO2_Pin, GPIO_PIN_RESET)







extern work_state_model_s _model_state;

void Set_Enable(uint8_t channel,bool status)
{
	if(channel<=0) return;
	switch(channel)
	{
		case 1:
		{
			_model_state._gpio_out.fields.EN_DRV1=status;
			_model_state._gpio_out.fields.EN_DRV1?(EN1_ON()):(EN1_OFF());
			break;
		}

		case 2:
		{
			_model_state._gpio_out.fields.EN_DRV2=status;
			_model_state._gpio_out.fields.EN_DRV2?(EN2_ON()):(EN2_OFF());
			break;
		}

		case 3:
		{
			_model_state._gpio_out.fields.EN_DRV3=status;
			_model_state._gpio_out.fields.EN_DRV3?(EN3_ON()):(EN3_OFF());
			break;
		}

		default:
			break;
	}


}
void Set_GPIO_BEMF(bool status)
{
	_model_state._gpio_out.fields.GPIO_BEMF=status;
	_model_state._gpio_out.fields.GPIO_BEMF?(BEMF_EN()):(BEMF_DEN());
}


void Get_ADC_Bemf_Phase(uint16_t* dataADC)
{
	dataADC[0]=_model_state.adc1.fields.BEMF1;
	dataADC[1]=_model_state.adc1.fields.BEMF2;
	dataADC[2]=_model_state.adc1.fields.BEMF3;



}


uint16_t Get_VBus()
{
	return _model_state.adc1.fields.VBUS;
}



