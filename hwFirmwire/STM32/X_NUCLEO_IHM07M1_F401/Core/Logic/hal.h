/*
 * hal.h
 *
 *  Created on: 11 апр. 2022 г.
 *      Author: Lepatenko
 */

#ifndef LOGIC_HAL_H_
#define LOGIC_HAL_H_

#include "Headers.h"


void Set_Enable(uint8_t channel,bool status);
void Set_GPIO_BEMF(bool status);

void Get_ADC_Bemf_Phase(uint16_t* dataADC);
uint16_t Get_VBus();


#endif /* LOGIC_HAL_H_ */
