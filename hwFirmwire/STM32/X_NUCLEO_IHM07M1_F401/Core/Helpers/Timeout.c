/*
 * Timeout.c
 *
 *  Created on: 26 дек. 2021 г.
 *      Author: Lepatenko
 */
#include "Timeout.h"


void Timeout_Init(timeout_s * p, uint32_t update_frq, uint32_t timeoutMs)
{
	 p->timeout_reload_value = update_frq * timeoutMs / 1000;
	    p->timeout_counter = 0;
}


int8_t Timeout_Update(timeout_s * p)
{
	if (p->timeout_counter > 0)
	        p->timeout_counter--;

	return Timeout_getState(p);

}


void Timeout_Reload(timeout_s * p)
{
	p->timeout_counter = p->timeout_reload_value;
}

int8_t Timeout_getState(timeout_s * p)
{
	return (p->timeout_counter > 0) ? 0 : 1;
}

