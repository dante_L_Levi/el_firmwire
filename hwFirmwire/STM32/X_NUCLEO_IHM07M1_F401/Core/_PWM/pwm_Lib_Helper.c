/*
 * pwm_Lib_Helper.c
 *
 *  Created on: Mar 11, 2022
 *      Author: Lepatenko
 */
#include "pwm_Lib_Helper.h"


/* ---------------------- TIM registers bit mask ------------------------ */
#define SMCR_ETR_Mask               ((uint16_t)0x00FF)
#define CCMR_Offset                 ((uint16_t)0x0018)
#define CCER_CCE_Set                ((uint16_t)0x0001)
#define	CCER_CCNE_Set               ((uint16_t)0x0004)



Mode_pwm_def status_pwm=Idle;

void (*callbackInit)(void);

void Init_Base_Tim1(void *func,Mode_pwm_def mode)
{
	callbackInit=func;
	callbackInit();
	status_pwm=mode;
}

void Start_Tim1_Pwm(uint32_t channel)
{
	switch(status_pwm)
	{
		case Single:
		{
			HAL_TIM_PWM_Start(&htim1, channel);
			break;
		}

		case Complimentary:
		{
			HAL_TIM_PWM_Start(&htim1, channel);
			HAL_TIMEx_PWMN_Start(&htim1, channel);
			break;
		}

		default:
			break;
	}
}

void Stop_Tim1_Pwm(uint32_t channel)
{
	switch(status_pwm)
		{
			case Single:
			{
				HAL_TIM_PWM_Stop(&htim1, channel);
				break;
			}

			case Complimentary:
			{
				HAL_TIM_PWM_Stop(&htim1, channel);
				HAL_TIMEx_PWMN_Stop(&htim1, channel);
				break;
			}

			default:
				break;
		}
}



void Set_Duty_Tim1_pwm(uint32_t channel, uint32_t Duty)
{
	switch(channel)
	{
		case TIM_CHANNEL_1:
		{
			TIM1->CCR1=Duty;
			break;
		}

		case TIM_CHANNEL_2:
		{
			TIM1->CCR2=Duty;
			break;
		}
		case TIM_CHANNEL_3:
		{
			TIM1->CCR3=Duty;
			break;
		}

		case TIM_CHANNEL_4:
		{
			TIM1->CCR4=Duty;
			break;
		}


	}
}




void TIM_CCxCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCx)
{
	uint16_t tmp = 0;
	tmp = CCER_CCE_Set << TIM_Channel;
	/* Reset the CCxE Bit */
	  TIMx->CCER &= (uint16_t)~ tmp;
	  /* Set or reset the CCxE Bit */
	    TIMx->CCER |=  (uint16_t)(TIM_CCx << TIM_Channel);

}

void TIM_CCxNCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCxN)
{
	uint16_t tmp = 0;
	tmp = CCER_CCNE_Set << TIM_Channel;

	/* Reset the CCxNE Bit */
	TIMx->CCER &= (uint16_t) ~tmp;

	/* Set or reset the CCxNE Bit */
	TIMx->CCER |=  (uint16_t)(TIM_CCxN << TIM_Channel);
}


void TIM_SelectOCxM(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_OCMode)
{
	uint32_t tmp = 0;
	uint16_t tmp1 = 0;

	tmp = (uint32_t) TIMx;
	tmp += CCMR_Offset;

	tmp1 = CCER_CCE_Set << (uint16_t)TIM_Channel;

	  /* Disable the Channel: Reset the CCxE Bit */
	  TIMx->CCER &= (uint16_t) ~tmp1;

	  if((TIM_Channel == TIM_CHANNEL_1) ||(TIM_Channel == TIM_CHANNEL_3))
	  {
	    tmp += (TIM_Channel>>1);

	    /* Reset the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp &= (uint32_t)~((uint32_t)TIM_CCMR1_OC1M);

	    /* Configure the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp |= TIM_OCMode;
	  }
	  else
	  {
	    tmp += (uint16_t)(TIM_Channel - (uint16_t)4)>> (uint16_t)1;

	    /* Reset the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp &= (uint32_t)~((uint32_t)TIM_CCMR1_OC2M);

	    /* Configure the OCxM bits in the CCMRx register */
	    *(__IO uint32_t *) tmp |= (uint16_t)(TIM_OCMode << 8);
	  }

}





