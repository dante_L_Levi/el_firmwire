/*
 * pwm_Lib_Helper.h
 *
 *  Created on: Mar 11, 2022
 *      Author: Lepatenko
 */

#ifndef PWM_PWM_LIB_HELPER_H_
#define PWM_PWM_LIB_HELPER_H_

#include "Headers.h"

#define TIM_Channel_1                      ((uint16_t)0x0000)
#define TIM_Channel_2                      ((uint16_t)0x0004)
#define TIM_Channel_3                      ((uint16_t)0x0008)
#define TIM_Channel_4                      ((uint16_t)0x000C)
#define TIM_Channel_5                      ((uint16_t)0x0010)
#define TIM_Channel_6                      ((uint16_t)0x0014)


#define TIM_CCx_Enable                      ((uint16_t)0x0001)
#define TIM_CCx_Disable                     ((uint16_t)0x0000)
#define IS_TIM_CCX(CCX) (((CCX) == TIM_CCx_Enable) || \
                         ((CCX) == TIM_CCx_Disable))


#define TIM_CCxN_Enable                     ((uint16_t)0x0004)
#define TIM_CCxN_Disable                    ((uint16_t)0x0000)
#define IS_TIM_CCXN(CCXN) (((CCXN) == TIM_CCxN_Enable) || \
                           ((CCXN) == TIM_CCxN_Disable))

#define TIM_ForcedAction_Active            ((uint16_t)0x0050)
#define TIM_ForcedAction_InActive          ((uint16_t)0x0040)
#define IS_TIM_FORCED_ACTION(ACTION) (((ACTION) == TIM_ForcedAction_Active) || \
                                      ((ACTION) == TIM_ForcedAction_InActive))


#define TIM_OCMode_Timing                  ((uint32_t)0x00000)
#define TIM_OCMode_Active                  ((uint32_t)0x00010)
#define TIM_OCMode_Inactive                ((uint32_t)0x00020)
#define TIM_OCMode_Toggle                  ((uint32_t)0x00030)
#define TIM_OCMode_PWM1                    ((uint32_t)0x00060)
#define TIM_OCMode_PWM2                    ((uint32_t)0x00070)

#define TIM_OCMode_Retrigerrable_OPM1      ((uint32_t)0x10000)
#define TIM_OCMode_Retrigerrable_OPM2      ((uint32_t)0x10010)
#define TIM_OCMode_Combined_PWM1           ((uint32_t)0x10040)
#define TIM_OCMode_Combined_PWM2           ((uint32_t)0x10050)
#define TIM_OCMode_Asymmetric_PWM1         ((uint32_t)0x10060)
#define TIM_OCMode_Asymmetric_PWM2         ((uint32_t)0x10070)





typedef enum
{
	Idle,
	Single,
	Complimentary

}Mode_pwm_def;


void Init_Base_Tim1(void *func,Mode_pwm_def mode);


void Set_Duty_Tim1_pwm(uint32_t channel, uint32_t Duty);
void Start_Tim1_Pwm(uint32_t channel);
void Stop_Tim1_Pwm(uint32_t channel);




void TIM_CCxNCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCxN);
void TIM_CCxCmd(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_CCx);
void TIM_SelectOCxM(TIM_TypeDef* TIMx, uint16_t TIM_Channel, uint16_t TIM_OCMode);




#endif /* PWM_PWM_LIB_HELPER_H_ */
