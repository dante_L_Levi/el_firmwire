/*
 * ADC_Sense.h
 *
 *  Created on: Nov 30, 2021
 *      Author: Lepatenko
 */

#ifndef ADC_ADC_SENSE_H_
#define ADC_ADC_SENSE_H_


#include "Headers.h"

typedef enum
{
	ADC_Channel_A0,
	ADC_Channel_A1,
	ADC_Channel_A2,
	ADC_Channel_A3,
	ADC_Channel_A4,
	ADC_Channel_A5,
	ADC_Channel_A6,
	ADC_Channel_A7

}ADC_Regular_channels;




void Init_ADC_Sense_Inject(void);
void init_ADC_Regular_Channels(void);
void StartCallibrationAdc(void);
int16_t Get_ADC_Inject_Channels(uint8_t channel);
uint16_t Get_ADC_Inject_Channels_ADC_Code(uint8_t channel);

void Read_regular_data(uint16_t *data);

void Lib_HAL_ADC_Regular(void);
void ADC_Regular_Select_CH1(void);
void ADC_Regular_Select_CH2(void);
void Read_ADC_regular(void);

#endif /* ADC_ADC_SENSE_H_ */
