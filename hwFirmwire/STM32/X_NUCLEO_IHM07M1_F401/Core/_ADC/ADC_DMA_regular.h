/*
 * ADC_DMA_regular.h
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#ifndef ADC_ADC_DMA_REGULAR_H_
#define ADC_ADC_DMA_REGULAR_H_


#include "Headers.h"


void ADC_DMA_SetCallBack(void *callbackAdc,void *callbackdma);
void ADC_DMA_Event(ADC_HandleTypeDef *adc,uint32_t *buffer, uint8_t Length);
void ADC_DMA_Init(void);


void ADC_Reserve_DMA_SetCallBack(void *callbackAdc,void *callbackdma);
void ADC_Reserve_DMA_Init(void);


#endif /* ADC_ADC_DMA_REGULAR_H_ */
