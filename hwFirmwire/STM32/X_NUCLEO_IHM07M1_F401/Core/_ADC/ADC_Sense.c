/*
 * ADC_Sense.c
 *
 *  Created on: Nov 30, 2021
 *      Author: Lepatenko
 */


#include "ADC_Sense.h"

//#define MODE_INJECT
#define None


#ifdef INJECT_F334
#define COUNT_CH_INJECT			4
#define VREF					3.3
#define ADC_SEQ					4095.0
#define K_conv					VREF/ADC_SEQ

#define adc_CHANNEL_COUNT		2
ADC_ChannelConfTypeDef sConfig = {0};
static uint32_t HAL_RCC_ADC12_CLK_ENABLED=0;

uint16_t adc_inject_Array_value[COUNT_CH_INJECT];
uint16_t adc_regular_Array[adc_CHANNEL_COUNT]={0};


static void Timer_Event_Update_Init(void);
static void GPio_Init_ADC_Injected(void);
//static void GPIO_Init_regular_Channel(uint16_t channel, GPIO_TypeDef* port);




/******************Init Timer Update**************************/
//static void ADC_TIM_Triger_Init(void);



void DMA1_Channel4_IRQHandler(void)
{
	DMA1->ISR &=DMA_ISR_TCIF4;

}


#ifdef MODE_INJECT
void ADC1_2_IRQHandler(void)
{
	ADC1->ISR |= ADC_ISR_JEOS;
	adc_inject_Array_value[0]=ADC1->JDR1;
	adc_inject_Array_value[1]=ADC1->JDR2;
	adc_inject_Array_value[2]=ADC1->JDR3;
	adc_inject_Array_value[3]=ADC1->JDR4;

}
#endif



static void GPio_Init_ADC_Injected(void)
{

	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_RESET);


		  /*Configure GPIO pins : PA0 PA1 PA2 PA3  */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}




void Init_ADC_Sense_Inject(void)
{
	RCC->AHBENR |= RCC_AHBENR_ADC12EN;
	GPio_Init_ADC_Injected();
	Timer_Event_Update_Init();
	StartCallibrationAdc();
	ADC1->JSQR |= 0x1030817B;
	ADC1->IER |= ADC_IER_JEOSIE;
	NVIC_EnableIRQ(ADC1_2_IRQn);            // Enable interrupt ADC1 and ADC2
	NVIC_SetPriority(ADC1_2_IRQn,1);
	ADC1->CR |= ADC_CR_ADEN;                // Enable ADC1
	while(!(ADC1->ISR & ADC_ISR_ADRDY));    // Wait ready ADC1
    ADC1->CR |= ADC_CR_JADSTART;            // Enable injector conversion


}


static void Timer_Event_Update_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	TIM6->PSC = 1-1;
	TIM6->ARR = 36;
	TIM6->CR2 |= TIM_CR2_MMS_1;         // Enable generation TRGO for ADC
	TIM6->CR1  |= TIM_CR1_CEN;
}



void StartCallibrationAdc(void)
{
	ADC1->CR &= ~ADC_CR_ADVREGEN;
	ADC1->CR |= ADC_CR_ADVREGEN_0;      // Enable Vref
	ADC1->CR &= ~ADC_CR_ADCALDIF;

	ADC1->CR |= ADC_CR_ADCAL;           // Start calibration
	while (ADC1->CR & ADC_CR_ADCAL);    // Wait end calibration
}


uint16_t Get_ADC_Inject_Channels_ADC_Code(uint8_t channel)
{
	if(channel>3)
			return 0;


	return adc_inject_Array_value[channel];
}

int16_t Get_ADC_Inject_Channels(uint8_t channel)
{
	if(channel>3)
		return 0;
	float value=(adc_inject_Array_value[channel]*K_conv)*1000;
	return (int16_t)value;

}

#endif




