/*
 * Led.h
 *
 *  Created on: 27 нояб. 2021 г.
 *      Author: Lepatenko
 */

#ifndef LEDS_LED_H_
#define LEDS_LED_H_


#include "Headers.h"

typedef enum{
    LED_OFF,
    LED_RED,
    LED_GREEN,
    LED_BOTH
}LED_state_e;


void LED_Init(void);
void LED_Switch(LED_state_e state);
void LED_Blink_RG(uint8_t blink_id, uint32_t interval);
void Len_Indication_work(uint8_t status);
void Set_Period_Toggle(uint32_t dt);
void Indication_Status(void);
void Set_devider_Led(uint8_t dev);


#endif /* LEDS_LED_H_ */
