################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DSP/ComplementaryFilter/ComplementaryFilt.c 

OBJS += \
./DSP/ComplementaryFilter/ComplementaryFilt.o 

C_DEPS += \
./DSP/ComplementaryFilter/ComplementaryFilt.d 


# Each subdirectory must supply rules for building sources it contributes
DSP/ComplementaryFilter/%.o: ../DSP/ComplementaryFilter/%.c DSP/ComplementaryFilter/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F401xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/[SENSORS]" -I"E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/_LEDs" -I../DSP/FIR -I../DSP/Settings_Filters -I../DSP/DIFFER -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/DSP/ComplementaryFilter" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-DSP-2f-ComplementaryFilter

clean-DSP-2f-ComplementaryFilter:
	-$(RM) ./DSP/ComplementaryFilter/ComplementaryFilt.d ./DSP/ComplementaryFilter/ComplementaryFilt.o

.PHONY: clean-DSP-2f-ComplementaryFilter

