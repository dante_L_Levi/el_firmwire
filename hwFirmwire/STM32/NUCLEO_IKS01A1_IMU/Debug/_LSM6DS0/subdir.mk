################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/[SENSORS]/_LSM6DS0/LSM6DS0_Lib.c 

OBJS += \
./_LSM6DS0/LSM6DS0_Lib.o 

C_DEPS += \
./_LSM6DS0/LSM6DS0_Lib.d 


# Each subdirectory must supply rules for building sources it contributes
_LSM6DS0/LSM6DS0_Lib.o: E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/[SENSORS]/_LSM6DS0/LSM6DS0_Lib.c _LSM6DS0/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F401xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/[SENSORS]" -I"E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/_LEDs" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/ComplementaryFilter" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/DIFFER" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/FIR" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/Settings_Filters" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Helpers" -I"E:/Repository/EL_Firmwire/hwFirmwire/Library/STM/_RS485" -I../Core/Logic -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/IIR" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/IMU" -I"E:/Repository/EL_Firmwire/hwFirmwire/STM32/NUCLEO_IKS01A1_IMU/Core/IMU/Madgwick" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-_LSM6DS0

clean-_LSM6DS0:
	-$(RM) ./_LSM6DS0/LSM6DS0_Lib.d ./_LSM6DS0/LSM6DS0_Lib.o

.PHONY: clean-_LSM6DS0

