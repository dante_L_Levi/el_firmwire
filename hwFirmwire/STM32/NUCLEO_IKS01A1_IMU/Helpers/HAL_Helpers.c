/*
 * HAL_Helpers.c
 *
 *  Created on: Dec 19, 2021
 *      Author: Lepatenko
 */

#include "HAL_Helpers.h"

#define I2C_WORK 1

#ifdef I2C_WORK
#define LSM6DS0_I2C_hundler				hi2c1
#endif


bool float_Window(float lowThreshold,float HighThreshold,float value)
{
	if(value>lowThreshold &&value<HighThreshold)
		return true;
	else
		return false;

}

#ifdef I2C_WORK
/***************************Write I2C Data****************************************/
void I2C1_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value)
{
	HAL_StatusTypeDef status = HAL_OK;
	    status = HAL_I2C_Mem_Write(&LSM6DS0_I2C_hundler, DeviceAddr, (uint16_t)RegisterAddr, I2C_MEMADD_SIZE_8BIT, &Value, 1, 0x10000);
	    if(status != HAL_OK)
	    {
	    		//Event Error
	    }
}

/***************************Read I2C Data in LSM6DS0****************************************/
uint8_t I2C1_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr)
{
	HAL_StatusTypeDef status = HAL_OK;
    uint8_t value = 0;
    status = HAL_I2C_Mem_Read(&LSM6DS0_I2C_hundler, DeviceAddr, RegisterAddr, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);
     if(status != HAL_OK)
     {
    	 //Event Error
     }
      return value;
}

#endif




