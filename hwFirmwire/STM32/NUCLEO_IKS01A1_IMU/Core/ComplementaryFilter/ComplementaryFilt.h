/*
 * ComplementaryFilt.h
 *
 *  Created on: 1 июл. 2022 г.
 *      Author: Lepatenko
 */

#ifndef INC_COMPLEMENTARYFILT_H_
#define INC_COMPLEMENTARYFILT_H_

#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"


typedef struct
{
	float accelPitch;
	float accelRoll;

	float roll;
	float pitch;
	float yaw;

	float dt;
	float alfa;

}ComplementaryFilter_Model;

void ComplementaryFilter_Update(ComplementaryFilter_Model* data,float *accel,float *gyro);


#endif /* INC_COMPLEMENTARYFILT_H_ */
