

#include "ComplementaryFilt.h"

#define AXIS_X			0
#define AXIS_Y			1
#define AXIS_Z			2
#define RAD2DEG 57.2957795131


void ComplementaryFilter_Update(ComplementaryFilter_Model* data,float *accel,float *gyro)
{
	 // Complementary filter
	data->accelPitch=atan2(accel[AXIS_Y],accel[AXIS_Z])*RAD2DEG;
	data->accelRoll=atan2(accel[AXIS_X],accel[AXIS_Z])*RAD2DEG;

	float dt=data->dt;
	float alpha=data->alfa;
	float accelRoll=data->accelRoll;
	float accelPitch=data->accelPitch;
	float roll,pitch,yaw;
	roll=data->roll;
	pitch=data->pitch;
	yaw=data->yaw;
	float GY=gyro[AXIS_Y];
	float GX=gyro[AXIS_X];
	float GZ=gyro[AXIS_Z];
	roll=alpha*(roll-GY*dt)+(1-alpha)*accelRoll;
	pitch=alpha*(pitch-GX*dt)+(1-alpha)*accelPitch;
	yaw+=GZ*dt;

	data->roll=roll;
	data->pitch=pitch;
	data->yaw=yaw;

}
