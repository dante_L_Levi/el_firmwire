/*
 * IMU_Types.h
 *
 *  Created on: 1 июл. 2022 г.
 *      Author: Lepatenko
 */

#ifndef INC_IMU_TYPES_H_
#define INC_IMU_TYPES_H_

#include "Headers.h"
#include "Settings.h"
#include "FIR.h"
#include "FIR_Coeff.h"
#include "differ.h"
#include "ComplementaryFilt.h"
#include "IIR.h"
#include "Madgwick_Filter.h"
#include "Madgvick_V2.h"



#pragma pack(push, 1)
typedef union
{
	struct
	{
		int16_t GX;
		int16_t GY;
		int16_t GZ;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Gyro_hw_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union
{
	struct
	{
		int16_t GX_bias;
		int16_t GY_bias;
		int16_t GZ_bias;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Gyro_Settings;

typedef union
{
	struct
	{
		int16_t AX_bias;
		int16_t AY_bias;
		int16_t AZ_bias;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Accel_Settings;

typedef struct
{
	float gyroSensivity;
	float accelSensivity;
	LSM6DS0_Gyro_Settings _biasGyro;
	LSM6DS0_Accel_Settings _biasAccel;

}LSM6DS0_Settings;

#pragma pack(pop)
#pragma pack(push, 1)
typedef union
{
	struct
	{
		int16_t AX;
		int16_t AY;
		int16_t AZ;
	}fields;
	int16_t AllData[3];
}LSM6DS0_Accel_hw_s;
#pragma pack(pop)




#pragma pack(push, 1)
typedef union
{
	struct
	{
		int16_t MX;
		int16_t MY;
		int16_t MZ;
	}fields;
	int16_t AllData[3];
}LIS3MDL_Magn_hw_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union
{
	struct
	{
		float GyroX;
		float GyroY;
		float GyroZ;

	}fields;
	float AllData[3];
}Vector3_LSM6DS0_Gyro_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union
{
	struct
	{
		float AccelX;
		float AccelY;
		float AccelZ;

	}fields;
	float AllData[3];
}Vector3_LSM6DS0_Accel_s;
#pragma pack(pop)


#pragma pack(push, 1)
typedef union
{
	struct
	{
		float MX;
		float MY;
		float MZ;
	}fields;
	float AllData[3];
}Vector3_LIS3MDL_Magn_s;
#pragma pack(pop)


typedef struct
{
	fir_f_filter_s fir_Gyro[3];
	fir_f_filter_s fir_Accel[3];
	fir_f_filter_s fir_Magn[3];

	IIRFirstOrder	iir_Gyro[3];
	IIRFirstOrder	iir_Accel[3];
	float 			Alpha_iir;


}IMU_Filters_Settings;

#pragma pack(push, 1)
typedef struct
{
	Quaternion 	_dataquat;
	Ealer_data	_angle_Ealer;

}Madgvick_Data;


typedef struct
{
	struct quaternion quat;

	float pitch;
	float roll;
	float yaw;


}MadgvickSystem;

#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
	uint16_t ID_temp;
	uint8_t Name[8];
	uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;

typedef struct
{
	Vector3_LSM6DS0_Gyro_s	_lsm6ds0_gyro;
	Vector3_LSM6DS0_Accel_s	_lsm6ds0_accel;
	Vector3_LIS3MDL_Magn_s	_lis3DL_Magn_s;


}Transmit_data2_Model_MainData;

typedef struct//Data Magvick Filter
{



}Transmit_data3_Model_Magvick;


typedef struct//Data Kalman Filter
{



}Transmit_data4_Model_Kalman;

typedef struct//Data Complementary Filter
{
	ComplementaryFilter_Model _dataComplementaryFilter;

}Transmit_data5_Model_ComplementaryFilter;

#pragma pack(pop)


typedef struct
{
	uint16_t 					ID;

	uint32_t 					_time_ms;
	uint8_t 					sync_trigger;
	uint16_t 					Led_Interval;

	LSM6DS0_Gyro_hw_s			_lsm6ds0_hw_gyro_s;
	LSM6DS0_Accel_hw_s			_lsm6ds0_hw_accel_s;
	LIS3MDL_Magn_hw_s			_lis3MDL_hw;
	LSM6DS0_Settings			_settingsData;

	Vector3_LSM6DS0_Gyro_s		_vector3_gyro;
	Vector3_LSM6DS0_Accel_s		_vector3_accel;
	Vector3_LIS3MDL_Magn_s		_vector3_Magn;

	Vector3_LSM6DS0_Gyro_s		_vector3_gyro_fir;
	Vector3_LSM6DS0_Accel_s		_vector3_accel_fir;
	Vector3_LIS3MDL_Magn_s		_vector3_Magn_fir;

	Vector3_LSM6DS0_Gyro_s		_vector3_gyro_iir;
	Vector3_LSM6DS0_Accel_s		_vector3_accel_iir;
	Vector3_LIS3MDL_Magn_s		_vector3_Magn_iir;


	ComplementaryFilter_Model _dataComplementaryFilter;
	IMU_Filters_Settings		_FiltersData;
	differ_s					diff;
	Madgvick_Data				_madgvick;
	MadgvickSystem				_madgvick_system;



}work_state_model_s;



#endif /* INC_IMU_TYPES_H_ */
