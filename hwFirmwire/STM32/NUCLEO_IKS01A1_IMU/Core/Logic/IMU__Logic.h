/*
 * IMU__Logic.h
 *
 *  Created on: 3 июл. 2022 г.
 *      Author: Lepatenko
 */

#ifndef INC_IMU__LOGIC_H_
#define INC_IMU__LOGIC_H_

#include "Headers.h"
#include "Settings.h"


void Main_Init(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);

void DataUpdate(void);



#endif /* INC_IMU__LOGIC_H_ */
