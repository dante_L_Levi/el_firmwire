/*
 * IMU_Logic.c
 *
 *  Created on: 3 июл. 2022 г.
 *      Author: Lepatenko
 */

#include "IMU__Logic.h"
#include "Timers.h"
#include "IMU_Types.h"
#include "math.h"

float fir_accel_buffer[3][FIR_3_DEPTH * 2];
float fir_Gyro_buffer[3][FIR_3_DEPTH * 2];
float fir_Magn_buffer[3][FIR_3_DEPTH * 2];

static void Update_IMU_Madgvick(void);


static void Read_LSM6DS0(void);
static void Read_LIS3MDL(void);

static  const uint16_t * rs485_id =  (const uint16_t *)_board_id;

work_state_model_s _model_state;

void Update_sync(void)
{
	_model_state.sync_trigger=1;
}

void work_time_ms(void)
{
	_model_state._time_ms++;
}


static void Set_LS6DS0_Bias_default(void)
{

	_model_state._settingsData._biasGyro.fields.GX_bias=-150;
	_model_state._settingsData._biasGyro.fields.GY_bias=168;
	_model_state._settingsData._biasGyro.fields.GZ_bias=-40;

	_model_state._settingsData._biasAccel.fields.AX_bias=22;
	_model_state._settingsData._biasAccel.fields.AY_bias=0;
	_model_state._settingsData._biasAccel.fields.AZ_bias=0;
}

static void Set_Config_fir_Filter(void)
{
	for (int i = 0; i < 3; i++)
	{
		_model_state._FiltersData.fir_Gyro[i].buff=fir_Gyro_buffer[i];
		_model_state._FiltersData.fir_Gyro[i].coef=fir_coeff_float_3;
		_model_state._FiltersData.fir_Gyro[i].depth = FIR_3_DEPTH;
		_model_state._FiltersData.fir_Gyro[i].mask = FIR_3_DEPTH - 1;
	}

	for (int i = 0; i < 3; i++)
		{
			_model_state._FiltersData.fir_Accel[i].buff=fir_accel_buffer[i];
			_model_state._FiltersData.fir_Accel[i].coef=fir_coeff_float_3;
			_model_state._FiltersData.fir_Accel[i].depth = FIR_3_DEPTH;
			_model_state._FiltersData.fir_Accel[i].mask = FIR_3_DEPTH - 1;
		}
}


static void Set_Config_iir_Filter(void)
{

	for(int i=0;i<3;i++)
	{
		IIRFirstOrder_Init(&_model_state._FiltersData.iir_Gyro[i],0.9f);
	}


	//увеличение альфы , уменьшает частоту среза, уменьшение альфы увеличивает частоту среза
	for(int i=0;i<3;i++)
	{
		IIRFirstOrder_Init(&_model_state._FiltersData.iir_Accel[i],0.9f);
	}


}


static void Set_Complementary_Filter(void)
{
	_model_state._dataComplementaryFilter.dt=0.0005f;
	_model_state._dataComplementaryFilter.alfa=0.05f;



}



void Main_Init(void)
{
	MX_GPIO_Init();
	Set_Period_Toggle(500);
	LED_Init();
 //LSM6DS0
	Init_HW_LSM6DS0(MX_I2C1_Init);
	 Set_CallBack_Read_Data(I2C1_IO_Read);
	 Set_CallBack_Transmit_Data(I2C1_IO_Write);
	 Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,LSM6DS0_ACC_GYRO_ODR_G_952Hz,LSM6DS0_ACC_GYRO_FS_XL_16g,
	 LSM6DS0_ACC_GYRO_FS_G_2000dps,0);
	 Set_LS6DS0_Bias_default();
	 Set_Config_fir_Filter();
	 Set_Config_iir_Filter();
	 Set_Complementary_Filter();
	 Madgvick_Init(&_model_state._madgvick._dataquat);
	 Madgvick_set_FreqUpdate(2000);

	 //LIS3MDL
	Init_HW_LIS3MDL(MX_I2C1_Init);
	Set_CallBack_Transmit_Data_LIS3MDL(I2C1_IO_Write);
	Set_CallBack_Read_Data_LIS3MDL(I2C1_IO_Read);
	LIS3MDL_Peripheral_Settings(LIS3MDL_MAG_DO_80Hz,LIS3MDL_MAG_FS_4Ga,
				LIS3MDL_MAG_OM_HIGH,LIS3MDL_MAG_TEMP_EN_DISABLE);

	Set_CallBack(10,Update_sync);
	Init_Timer_For_Sync(10,Sync_4000Hz);

	Set_CallBack(11,work_time_ms);
	Init_Timer_For_Sync(11,Sync_1000Hz);


	_model_state.ID=*rs485_id;
	RS485_SProtocol_init(*rs485_id,RS485_BAUDRATE,*RS485_dataTX);
	RS485_Reciever_Helper();



}

uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}


void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}



static void Read_LSM6DS0(void)
{
	LSM6DS0_Gyro_GetXYZ(_model_state._lsm6ds0_hw_gyro_s.AllData);
	LSM6DS0_Accel_GetXYZ(_model_state._lsm6ds0_hw_accel_s.AllData);


	for(int i=0;i<3;i++)
	{
		_model_state._lsm6ds0_hw_gyro_s.AllData[i]+=_model_state._settingsData._biasGyro.AllData[i];
		_model_state._lsm6ds0_hw_accel_s.AllData[i]+=_model_state._settingsData._biasAccel.AllData[i];

	}

}

static void Read_LIS3MDL(void)
{
	LIS3MDL_Magn_GetXYZ(_model_state._lis3MDL_hw.AllData);
}


static void Update_FIR_Data(void)
{
	//--------------------------------------FIR FILTER--------------------------------------------------------
		for(int i=0;i<3;++i)
		{
			_model_state._vector3_gyro_fir.AllData[i]=fir_f(&_model_state._FiltersData.fir_Gyro[i],
																	_model_state._vector3_gyro.AllData[i]);
		}

		for(int i=0;i<3;++i)
		{
				_model_state._vector3_accel_fir.AllData[i]=fir_f(&_model_state._FiltersData.fir_Accel[i],
																		_model_state._vector3_accel.AllData[i]);
		}

		//--------------------------------------------------------------------------------------------------------
}

static void Update_IIR_Filter(void)
{
	//------------------------------------IIR FILTER----------------------------------------------------------
		for(int i=0;i<3;++i)
		{
			IIRFirstOrder_Update(&_model_state._FiltersData.iir_Gyro[i],_model_state._vector3_gyro.AllData[i]);
			_model_state._vector3_gyro_iir.AllData[i]=_model_state._FiltersData.iir_Gyro[i].out;
		}

		for(int i=0;i<3;++i)
		{
				IIRFirstOrder_Update(&_model_state._FiltersData.iir_Accel[i],_model_state._vector3_accel.AllData[i]);
				_model_state._vector3_accel_iir.AllData[i]=_model_state._FiltersData.iir_Accel[i].out;
		}


		//--------------------------------------------------------------------------------------------------------


}

static void Get_Complemetary_Filter(void)
{
	ComplementaryFilter_Update(&_model_state._dataComplementaryFilter,_model_state._vector3_accel_fir.AllData,
			_model_state._vector3_gyro_fir.AllData);
}

static void Update_IMU_Madgvick(void)
{
	/*
	MadgwickAHRSupdateIMU(PI*((_model_state._vector3_gyro_fir.fields.GyroX)/180.0f),
			PI*((_model_state._vector3_gyro_fir.fields.GyroY)/180.0f),
			PI*((_model_state._vector3_gyro_fir.fields.GyroZ)/180.0f),
			_model_state._vector3_accel_fir.fields.AccelX,
			_model_state._vector3_accel_fir.fields.AccelY,
			_model_state._vector3_accel_fir.fields.AccelZ,
			&_model_state._madgvick._dataquat);

	Get_Angles_Ealer(&_model_state._madgvick._angle_Ealer.yaw,
			&_model_state._madgvick._angle_Ealer.pitch,
			&_model_state._madgvick._angle_Ealer.roll,
			_model_state._madgvick._dataquat);*/

	imu_filter(_model_state._vector3_accel_fir.fields.AccelX/1000.0f,
			_model_state._vector3_accel_fir.fields.AccelY/1000.0f,
			_model_state._vector3_accel_fir.fields.AccelZ/1000.0f,
			PI*((_model_state._vector3_gyro_fir.fields.GyroX/1000.0f)/180.0f),
			PI*((_model_state._vector3_gyro_fir.fields.GyroY/1000.0f)/180.0f),
			PI*((_model_state._vector3_gyro_fir.fields.GyroZ/1000.0f)/180.0f),
			&_model_state._madgvick_system.quat
			);

	eulerAngles(_model_state._madgvick_system.quat,&_model_state._madgvick_system.roll,
			&_model_state._madgvick_system.pitch,&_model_state._madgvick_system.yaw);

}

static void Update_VectorData_IMU(void)
{
	_model_state._settingsData.gyroSensivity=Get_Sensivity_Gyro();
	_model_state._settingsData.accelSensivity=Get_Sensivity_Accel()+0.262;

	for(int i=0;i<3;i++)
	{
		_model_state._vector3_gyro.AllData[i]=(float)(_model_state._lsm6ds0_hw_gyro_s.AllData[i])
				*_model_state._settingsData.gyroSensivity;
		_model_state._vector3_accel.AllData[i]=(float)(_model_state._lsm6ds0_hw_accel_s.AllData[i])
				*_model_state._settingsData.accelSensivity;
	}








}


void DataUpdate(void)
{
	Read_LSM6DS0();
	Read_LIS3MDL();
	Update_VectorData_IMU();
	Update_FIR_Data();
	Update_IIR_Filter();
	Get_Complemetary_Filter();
	Update_IMU_Madgvick();

}




void RS485_Command_Update_Async(void)
{

}

