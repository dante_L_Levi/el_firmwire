#include "FIR.h"

float fir_f(fir_f_filter_s* self, float NewSample)
{
    //Calculate the new output
    self->buff[self->carriage] = NewSample;
    self->buff[self->carriage + self->depth] = NewSample;

    const float * a = &self->coef[0];
    float * b = &self->buff[self->carriage];
    self->carriage++;
    self->carriage &= self->mask;
    float accum = 0;
    uint16_t i = self->depth;
    while (i--)
        accum += *a++ * *b++;

    return accum;
}

int32_t fir_int2(fir_int_filter_s* self, int32_t NewSample)
{

    //Calculate the new output
    self->buff[self->carriage] = NewSample;
    self->buff[self->carriage + self->depth] = NewSample;

    const int32_t * a = &self->coef[0];
    int32_t * b = &self->buff[self->carriage];
    self->carriage++;
    self->carriage &= self->mask;
    int32_t accum = 0;
    uint16_t i = self->depth;
    while (i--)
        accum += *a++ * *b++;

    return accum >> self->out_lsh;
}

int32_t fir_int3(fir_int_filter_s* self, int32_t NewSample)
{

    //Calculate the new output
    self->buff[self->carriage] = NewSample;
    self->carriage++;
    self->carriage &= self->mask;

    int64_t accum = 0;
    for (int16_t i = 0; i < self->depth; i++)
    {
        accum += self->coef[i] * self->buff[i & self->mask];
    }

    return accum >> self->out_lsh;
}
