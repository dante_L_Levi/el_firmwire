

#ifndef INC_FIR_H_
#define INC_FIR_H_


#include "Headers.h"

typedef struct
{
    const float* coef;
    float* buff;
    int16_t depth;
    int16_t mask;
    int16_t carriage;
} fir_f_filter_s;

typedef struct
{
    const int32_t* coef;
    int32_t* buff;
    int16_t carriage;
    int32_t out_lsh;
    int16_t depth;
    int16_t mask;
} fir_int_filter_s;

float fir_f(fir_f_filter_s* self, float NewSample);

int32_t fir_int2(fir_int_filter_s* self, int32_t NewSample);
int32_t fir_int3(fir_int_filter_s* self, int32_t NewSample);


#endif /* INC_FIR_H_ */
