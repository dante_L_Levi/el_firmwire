/*
 * Madgvick_Filter.c
 *https://github.com/bjohnsonfl/Madgwick_Filter/blob/master/madgwickFilter.c
 *  Created on: 13 июл. 2022 г.
 *      Author: Lepatenko
 */

#include "Madgwick_Filter.h"
#include "math.h"
#include "string.h"


float sampleFreq;

#define betaDef		0.1f		// 2 * proportional gain


static float invSqrt(float x)
{
	float halfx = 0.5f * x;
	float y = x;
	long i = *(long*)&y;
	i = 0x5f3759df - (i>>1);
	y = *(float*)&i;
	y = y * (1.5f - (halfx * y * y));
	return y;
}


void Madgvick_Init(Quaternion* dt)
{
	dt->beta=betaDef;
	dt->q0=1.0f;
	dt->q1=0.0f;
	dt->q2=0.0f;
	dt->q3=0.0f;

}


void Madgvick_set_FreqUpdate(float freq)
{
	if(freq>0)
		sampleFreq=freq;


}

void Madgvick_SetBeta(float data,Quaternion* dt)
{
	if(data>0)
	{
		dt->beta=data;
	}
}

void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az,Quaternion* dt)
{
	Quaternion _data_quaternion;
	memcpy(&_data_quaternion,dt,sizeof(Quaternion));
	float recipNorm;
	float s0, s1, s2, s3;
	float qDot1, qDot2, qDot3, qDot4;
	float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

	// Rate of change of quaternion from gyroscope
	qDot1 = 0.5f * (-_data_quaternion.q1 * gx - _data_quaternion.q2 * gy - _data_quaternion.q3 * gz);
	qDot2 = 0.5f * (_data_quaternion.q0 * gx + _data_quaternion.q2 * gz - _data_quaternion.q3 * gy);
	qDot3 = 0.5f * (_data_quaternion.q0 * gy - _data_quaternion.q1 * gz + _data_quaternion.q3 * gx);
	qDot4 = 0.5f * (_data_quaternion.q0 * gz + _data_quaternion.q1 * gy - _data_quaternion.q2 * gx);

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
	{
		// Normalise accelerometer measurement
		recipNorm = invSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;
		// Auxiliary variables to avoid repeated arithmetic
		_2q0 = 2.0f * _data_quaternion.q0;
		_2q1 = 2.0f * _data_quaternion.q1;
		_2q2 = 2.0f * _data_quaternion.q2;
		_2q3 = 2.0f * _data_quaternion.q3;
		_4q0 = 4.0f * _data_quaternion.q0;
		_4q1 = 4.0f * _data_quaternion.q1;
		_4q2 = 4.0f * _data_quaternion.q2;
		_8q1 = 8.0f * _data_quaternion.q1;
		_8q2 = 8.0f * _data_quaternion.q2;
		q0q0 = _data_quaternion.q0 * _data_quaternion.q0;
		q1q1 = _data_quaternion.q1 * _data_quaternion.q1;
		q2q2 = _data_quaternion.q2 * _data_quaternion.q2;
		q3q3 = _data_quaternion.q3 * _data_quaternion.q3;


		// Gradient decent algorithm corrective step
		s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
		s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * _data_quaternion.q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
		s2 = 4.0f * q0q0 * _data_quaternion.q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
		s3 = 4.0f * q1q1 * _data_quaternion.q3 - _2q1 * ax + 4.0f * q2q2 * _data_quaternion.q3 - _2q2 * ay;
		recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
		s0 *= recipNorm;
		s1 *= recipNorm;
		s2 *= recipNorm;
		s3 *= recipNorm;

		// Apply feedback step
		qDot1 -= _data_quaternion.beta * s0;
		qDot2 -= _data_quaternion.beta * s1;
		qDot3 -= _data_quaternion.beta * s2;
		qDot4 -= _data_quaternion.beta * s3;


	}

	// Integrate rate of change of quaternion to yield quaternion
	_data_quaternion.q0 += qDot1 * (1.0f / sampleFreq);
	_data_quaternion.q1 += qDot2 * (1.0f / sampleFreq);
	_data_quaternion.q2 += qDot3 * (1.0f / sampleFreq);
	_data_quaternion.q3 += qDot4 * (1.0f / sampleFreq);

	// Normalise quaternion
	recipNorm = invSqrt(_data_quaternion.q0 * _data_quaternion.q0 + _data_quaternion.q1 * _data_quaternion.q1 + _data_quaternion.q2 * _data_quaternion.q2 + _data_quaternion.q3 * _data_quaternion.q3);
	_data_quaternion.q0 *= recipNorm;
	_data_quaternion.q1 *= recipNorm;
	_data_quaternion.q2 *= recipNorm;
	_data_quaternion.q3 *= recipNorm;

	dt->beta=_data_quaternion.beta;
	dt->q0=_data_quaternion.beta;
	dt->q1=_data_quaternion.q1;
	dt->q2=_data_quaternion.q2;
	dt->q3=_data_quaternion.q3;


}


void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz,Quaternion* dt)
{

}


void Get_Angles_Ealer(float *yaw, float *pitch, float *roll,Quaternion _data_quaternion)
{
	Ealer_data _data_Ealer;

	_data_Ealer.roll=atan2f(_data_quaternion.q0*_data_quaternion.q1+_data_quaternion.q2*_data_quaternion.q3,
			0.5f-_data_quaternion.q1*_data_quaternion.q1-_data_quaternion.q2*_data_quaternion.q2);

	_data_Ealer.pitch=asinf(-2.f*(_data_quaternion.q1*_data_quaternion.q3-_data_quaternion.q0*_data_quaternion.q2));
	_data_Ealer.yaw=atan2f(_data_quaternion.q1*_data_quaternion.q2+_data_quaternion.q0*_data_quaternion.q3,
			0.5f-_data_quaternion.q2*_data_quaternion.q2-
			_data_quaternion.q3*_data_quaternion.q3);


	    *yaw=_data_Ealer.yaw;
	    *pitch=_data_Ealer.pitch;
	    *roll=_data_Ealer.roll;

	    //*pitch *= 180.0f / PI;
	    //*yaw   *= 180.0f / PI;
	   // *yaw   -= 13.8f; // Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
	   // *roll  *= 180.0f / PI;


}

