/*
 * Madgwick_Filter.h
 *
 *  Created on: 13 июл. 2022 г.
 *      Author: Lepatenko
 */

#ifndef IMU_MADGWICK_MADGWICK_FILTER_H_
#define IMU_MADGWICK_MADGWICK_FILTER_H_

#include "math.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"

#define PI          3.14

#pragma pack(push, 1)
typedef struct
{
    float q0,q1,q2,q3;
    float beta;

}Quaternion;

typedef struct
{
    float yaw;
    float pitch;
    float roll;

}Ealer_data;
#pragma pack(pop)


void Madgvick_Init(Quaternion* dt);
void Madgvick_SetBeta(float data, Quaternion* dt);
void Madgvick_set_FreqUpdate(float freq);

void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz,Quaternion* dt);
void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az,Quaternion* dt);

void Get_Angles_Ealer(float *yaw, float *pitch, float *roll,Quaternion _data_quaternion);

#endif /* IMU_MADGWICK_MADGWICK_FILTER_H_ */
