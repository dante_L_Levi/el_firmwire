/*
 * IIR.h
 *
 *  Created on: 4 июл. 2022 г.
 *      Author: Lepatenko
 */

#ifndef IIR_H_
#define IIR_H_


#include "stdint.h"
#include "stdio.h"

typedef struct {
	float alpha;
	float out;
} IIRFirstOrder;

void IIRFirstOrder_Init(IIRFirstOrder *filt, float alpha);
float IIRFirstOrder_Update(IIRFirstOrder *filt, float in);



#endif /* IIR_H_ */
