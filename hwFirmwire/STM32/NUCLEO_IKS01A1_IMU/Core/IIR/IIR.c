/*
 * IIR.c
 *
 *  Created on: 4 июл. 2022 г.
 *      Author: Lepatenko
 */

#include "IIR.h"



void IIRFirstOrder_Init(IIRFirstOrder *filt, float alpha)
{
	filt->alpha = alpha;
	filt->out   = 0.0f;
}

float IIRFirstOrder_Update(IIRFirstOrder *filt, float in)
{
	filt->out = filt->alpha * filt->out + (1.0f - filt->alpha) * in;
	return filt->out;
}
