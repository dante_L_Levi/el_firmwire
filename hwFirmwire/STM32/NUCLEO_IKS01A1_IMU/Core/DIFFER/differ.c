/*
 * differ.c
 *
 *  Created on: 16 ���. 2020 �.
 *      Author: Gorudko
 */
#include "differ.h"

int32_t Differ_Update(differ_s *self, int32_t in)
{
    int32_t delta = in - self->prev;
    self->prev = in;
    return delta;
}

