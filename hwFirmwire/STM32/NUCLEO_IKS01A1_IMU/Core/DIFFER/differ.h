/*
 * differ.h
 *
 *  Created on: 16 ���. 2020 �.
 *      Author: Gorudko
 */

#ifndef DIFFER_H_
#define DIFFER_H_

#include "stdint.h"
#include "stdbool.h"

typedef struct
{
    int32_t prev;
} differ_s;

int32_t Differ_Update(differ_s *self, int32_t in);

#endif /* DIFFER_H_ */
