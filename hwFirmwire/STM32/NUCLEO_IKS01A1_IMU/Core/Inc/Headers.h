/*
 * Headers.h
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef INC_HEADERS_H_
#define INC_HEADERS_H_


#include "stm32f4xx_hal.h"
#include "stm32f401xe.h"


#include "i2c.h"
#include "usart.h"
#include "gpio.h"


#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"


//#include "line_buffer.h"
//#include "ring_buffer.h"


#include "Led.h"

#include "_LSM6DS0/LSM6DS0_Lib.h"
#include "_LIS3MDL/LIS3MDL.h"
#include "_LPS25HB/LPS25HB_Lib.h"
#include "RS485_Lib.h"
#include "FIR.h"

#include "HAL_Helpers.h"
#include "Timers.h"
#include "Timeout.h"



#endif /* INC_HEADERS_H_ */
