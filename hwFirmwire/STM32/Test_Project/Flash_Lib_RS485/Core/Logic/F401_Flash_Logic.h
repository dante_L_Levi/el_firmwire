/*
 * F401_Flash_Logic.h
 *
 *  Created on: Mar 22, 2022
 *      Author: Lepatenko
 */

#ifndef LOGIC_F401_FLASH_LOGIC_H_
#define LOGIC_F401_FLASH_LOGIC_H_


#include "Headers.h"
#include "Settings.h"


#pragma pack(push, 1)
typedef union
{
	struct
	{
		uint16_t channel1;
		uint16_t channel2;
		uint16_t channel3;
		uint16_t channel4;
		uint16_t channel5;
		uint16_t channel6;


	}fields;
	uint16_t All_adc_data_hw[6];
}hw_state_adc1;

typedef union
{
	struct
		{
		int16_t channel1;
		int16_t channel2;
		int16_t channel3;
		int16_t channel4;
		int16_t channel5;
		int16_t channel6;

		}fields;
		int16_t All_adc_data[6];

}adc1_s;


//multiple 4 size%4=0!!!
typedef struct
{

	float adc_channels_Kp[6];
	float adc_ofsset_channels[6];


}FLASH_SETTINGS_Data_s;


typedef struct
{
	uint16_t ID_temp;
	uint8_t Name[8];
	uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;

typedef struct
{
	adc1_s						adc1;

}Transmit_data2_Model_MainData;

typedef struct
{
	FLASH_SETTINGS_Data_s		_settings;
}Transmit_data3_Model_SettingData;


typedef struct
{
	uint16_t 							ID;
	uint8_t 							sync_trigger;
	uint32_t 							_time_ms;
	FLASH_SETTINGS_Data_s				_settings;
	hw_state_adc1						_adc1_hw;
	adc1_s								adc1;

	Transmit_data1_Model_DataBoard		_data1;
	Transmit_data2_Model_MainData		_data2;
	Transmit_data3_Model_SettingData	_data3;


}work_state_model_s;



#pragma pack(pop)

void Main_Init(void);
uint8_t Get_Status_Trigger(void);
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
void F401_Sync(void);






#endif /* LOGIC_F401_FLASH_LOGIC_H_ */
