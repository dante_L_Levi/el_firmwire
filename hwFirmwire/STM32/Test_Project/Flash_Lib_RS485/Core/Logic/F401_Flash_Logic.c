/*
 * F401_Flash_Logic.c
 *
 *  Created on: Mar 22, 2022
 *      Author: Lepatenko
 */

#include "F401_Flash_Logic.h"


#define INDEX_CH1		0
#define INDEX_CH2		1
#define INDEX_CH3		2
#define INDEX_CH4		3
#define INDEX_CH5		4
#define INDEX_CH6		5

uint16_t buffer[6]={0};

work_state_model_s 			_model_state;
const uint16_t * rs485_id =  (const uint16_t *)_board_id;
uint32_t data_test={0};
uint8_t dataBUFF[4]={0};
static void Fast_Loop(void);
static void Middle_Loop(void);

void defaults_settings(void);
static void Get_Settings_flash(void);

void defaults_settings(void)
{
	_model_state._settings.adc_channels_Kp[INDEX_CH1]=0.805f;
	_model_state._settings.adc_channels_Kp[INDEX_CH2]=0.805f;
	_model_state._settings.adc_channels_Kp[INDEX_CH3]=0.805f;
	_model_state._settings.adc_channels_Kp[INDEX_CH4]=0.805f;
	_model_state._settings.adc_channels_Kp[INDEX_CH5]=0.805f;
	_model_state._settings.adc_channels_Kp[INDEX_CH6]=0.805f;

	_model_state._settings.adc_ofsset_channels[INDEX_CH1]=1;
	_model_state._settings.adc_ofsset_channels[INDEX_CH2]=1;
	_model_state._settings.adc_ofsset_channels[INDEX_CH3]=1;
	_model_state._settings.adc_ofsset_channels[INDEX_CH4]=1;
	_model_state._settings.adc_ofsset_channels[INDEX_CH5]=1;
	_model_state._settings.adc_ofsset_channels[INDEX_CH6]=1;

	//uint8_t buffer[sizeof(_model_state._settings)]={0};

	//memcpy(buffer,&_model_state._settings,sizeof(buffer));
	FlashSettings_write((uint8_t*)&_model_state._settings, sizeof(_model_state._settings));

}

static void Get_Settings_flash(void)
{
	uint8_t buffer[sizeof(_model_state._settings)]={0};
	FlashSettings_read(buffer, sizeof(_model_state._settings));
	FLASH_SETTINGS_Data_s data={0};
	memcpy(&data,buffer,sizeof(_model_state._settings));
	_model_state._settings=data;
}


void Update_sync(void)
{
	_model_state.sync_trigger=1;
}


void work_time_ms(void)
{
	_model_state._time_ms++;
}

void Init_Logic(void)
{

	data_test=ReadBoardID(FLASH_ID_ADDR);

	dataBUFF[3]=(data_test&0xff000000)>>24;
	dataBUFF[2]=(data_test&0x00ff0000)>>16;
	dataBUFF[1]=(data_test&0x0000ff00)>>8;
	dataBUFF[0]=(data_test&0x000000ff);


	_model_state.ID=(uint16_t)(dataBUFF[1]<<8)|(dataBUFF[0]);
	//defaults_settings();
	Get_Settings_flash();

}

void Main_Init(void)
{
	WriteBoardID(FLASH_ID_ADDR,(uint32_t*)_board_id, 1);
	Init_Logic();


	MX_GPIO_Init();
	Set_Period_Toggle(8000);
	LED_Init();

	Set_CallBack(10,Update_sync);
	Init_Timer_For_Sync(10,Sync_4000Hz);

	Set_CallBack(11,work_time_ms);
	Init_Timer_For_Sync(11,Sync_1000Hz);

	ADC_DMA_SetCallBack(MX_ADC1_Init,MX_DMA_Init);
	ADC_DMA_Init();
	ADC_DMA_Event(&hadc1,(uint32_t*)(buffer), 6);

	RS485_SProtocol_init(_model_state.ID,RS485_BAUDRATE,*RS485_dataTX);
	RS485_Reciever_Helper();



}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}

static void Analog_read(void)
{
	for(int i=0;i<6;i++)
	{
		_model_state._adc1_hw.All_adc_data_hw[i]=buffer[i];
	}

	for(int i=0;i<6;i++)
	{
		_model_state.adc1.All_adc_data[i]=_model_state._adc1_hw.All_adc_data_hw[i]*
					_model_state._settings.adc_channels_Kp[i]+_model_state._settings.adc_ofsset_channels[i];
	}

}

static void Fast_Loop(void)
{
	Analog_read();
}


static void Middle_Loop(void)
{

}


void F401_Sync(void)
{
	Fast_Loop();
	Middle_Loop();
}


typedef enum
{
    cmd_Set_Connect=0x00,

	cmd_Get_Board=0x01,
	cmd_Get_main_data,
	cmd_Get_setting_peripheral_board,

	cmd_Set_setting_adc_Kp_peripheral_board,
	cmd_Set_setting_adc_offset_peripheral_board,
	cmd_Set_Default_setting_peripheral_board,
	cmd_Set_cmd_flash

}Command_Board;

static void Set_Connect(uint8_t* data)
{
	Set_Period_Toggle(4000);
}

static void Get_Board(uint8_t* data)
{
	_model_state._data1.ID_temp=_model_state.ID;
	memcpy(_model_state._data1.Name,model,8);
	memcpy(_model_state._data1.firmwire,firmware,8);

	RS485_Sprotocol_Data_def msg;
	msg.cmd=cmd_Get_Board;
	msg.length=sizeof(Transmit_data1_Model_DataBoard);
	msg.data=(uint8_t*)&_model_state._data1;
	SProtocol_addTxMessage(&msg);
}

static void Get_main_data(uint8_t* data)
{
	_model_state._data2.adc1=_model_state.adc1;

	RS485_Sprotocol_Data_def msg;
	msg.cmd=cmd_Get_main_data;
	msg.length=sizeof(Transmit_data2_Model_MainData);
	msg.data=(uint8_t*)&_model_state._data2;
	SProtocol_addTxMessage(&msg);
}

static void Get_setting_peripheral_board(uint8_t* data)
{
	_model_state._data3._settings=_model_state._settings;

	RS485_Sprotocol_Data_def msg;
	msg.cmd=cmd_Get_setting_peripheral_board;
	msg.length=sizeof(_model_state._settings);
	msg.data=(uint8_t*)&_model_state._data3;
	SProtocol_addTxMessage(&msg);
}

static void Set_setting_adc_Kp_peripheral_board(uint8_t* data)
{
	float *temp=(float *)data;
	for(int i=0;i<6;i++)
	{
		_model_state._settings.adc_channels_Kp[i]=temp[i];
		//_model_state._settings.adc_ofsset_channels[i]=temp->adc_ofsset_channels[i];
	}



}

void Set_setting_adc_offset_peripheral_board(uint8_t* data)
{
	float *temp=(float *)data;
		for(int i=0;i<6;i++)
		{
			//_model_state._settings.adc_channels_Kp[i]=temp->adc_channels_Kp[i];
			_model_state._settings.adc_ofsset_channels[i]=temp[i];
		}

}

static void Set_Default_setting_peripheral_board(uint8_t* data)
{
	defaults_settings();
}


static void Set_cmd_flash(uint8_t* data)
{
	Erace_Sector(FLASH_ID_ADDR);
	WriteBoardID(FLASH_ID_ADDR,(uint32_t*)_board_id, 1);
	FlashSettings_write((uint8_t*)&_model_state._settings, sizeof(_model_state._settings));
	NVIC_SystemReset();

}


/*==================================================================================*/
#define SIZE_NUM_CMD				8
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{

		(rs485_command_handler)Set_Connect,
		(rs485_command_handler)Get_Board,
		(rs485_command_handler)Get_main_data,
		(rs485_command_handler)Get_setting_peripheral_board,
		(rs485_command_handler)Set_setting_adc_Kp_peripheral_board,
		(rs485_command_handler)Set_setting_adc_offset_peripheral_board,
		(rs485_command_handler)Set_Default_setting_peripheral_board,
		(rs485_command_handler)Set_cmd_flash

};



/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
					        return;

			//hard fix of data align trouble
			//uint32_t memory[10];
			//RS485_Sprotocol_Data_def *mess=(RS485_Sprotocol_Data_def *)((uint8_t *)memory);
			RS485_Sprotocol_Data_def mess;
			Get_RS485RxMessage(&mess);
			rs485_commands_array[mess.cmd](mess.dataRxPayload);
			RS485_SetPkgAvailable(0);
}


