/*
 * Flash_Setting.c
 *
 *  Created on: Mar 22, 2022
 *      Author: Lepatenko
 */

#include "Flash_Setting.h"

uint32_t ADDR_SETTING=0x0000000;
uint32_t DataLength=0;
#define page_size 0x1FFFF
#define F4_7
int32_t getCrcSum(void * data_ptr, int32_t len);


#ifdef F3
void WriteBoardID(uint32_t ADDR,uint8_t *data_ptr, uint8_t len)
{
	volatile uint32_t data_to_FLASH[(strlen((char*)data_ptr)/4)	+ (int)((strlen((char*)data_ptr) % 4)!= 0)];
			memset((uint8_t*)data_to_FLASH, 0, strlen((char*)data_to_FLASH));
			strcpy((char*)data_to_FLASH, (char*)data_ptr);

			volatile uint32_t data_length = (strlen((char*)data_to_FLASH) / 4)
											+ (int)((strlen((char*)data_to_FLASH) % 4) != 0);

			volatile uint16_t pages = (strlen((char*)data_ptr)/page_size)
											+ (int)((strlen((char*)data_ptr)%page_size) != 0);
			  /* Unlock the Flash to enable the flash control register access *************/
			  HAL_FLASH_Unlock();

			  /* Allow Access to option bytes sector */
			  HAL_FLASH_OB_Unlock();

			  /* Fill EraseInit structure*/

			  FLASH_EraseInitTypeDef EraseInitStruct;
			  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
			  EraseInitStruct.PageAddress = ADDR;
			  EraseInitStruct.NbPages = pages;
			  uint32_t PageError;


			  volatile uint32_t write_cnt=0, index=0;

			  volatile HAL_StatusTypeDef status;
			  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
			  while(index < data_length)
			  {
				  if (status == HAL_OK)
				  {
					  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_STORAGE+write_cnt, data_to_FLASH[index]);
					  if(status == HAL_OK)
					  {
						  write_cnt += 4;
						  index++;
					  }
				  }
			  }


			  HAL_FLASH_OB_Lock();
			  HAL_FLASH_Lock();
}

uint16_t  ReadBoardID(uint32_t ADDR)
{
	volatile uint32_t read_data;
	volatile uint32_t read_cnt=0;
	uint8_t data_ptr[2]={0};
	do
	{
			read_data = *(uint32_t*)(ADDR + read_cnt);
			data_ptr[read_cnt] = (uint8_t)read_data;
			data_ptr[read_cnt + 1] = (uint8_t)(read_data >> 8);
			read_cnt += 1;

		}while(read_cnt>1);
	uint16_t data_ID=(uint16_t)((data_ptr[1]<<8)|(data_ptr[0]));
	return data_ID;
}

void Set_Flash_Adress_Start(uint32_t Adress)
{
	ADDR_SETTING=Adress;
}

void FlashSettings_write(uint8_t* data_ptr, uint32_t len)
{
	volatile uint32_t data_to_FLASH[(strlen((char*)data_ptr)/4)	+ (int)((strlen((char*)data_ptr) % 4)!= 0)];
		memset((uint8_t*)data_to_FLASH, 0, strlen((char*)data_to_FLASH));
		uint32_t crc = getCrcSum(data_ptr, len);
		strcpy((char*)data_to_FLASH, (char*)data_ptr);

		volatile uint32_t data_length = (strlen((char*)data_to_FLASH) / 4)
										+ (int)((strlen((char*)data_to_FLASH) % 4) != 0);

		volatile uint16_t pages = (strlen((char*)data_ptr)/page_size)
										+ (int)((strlen((char*)data_ptr)%page_size) != 0);
		  /* Unlock the Flash to enable the flash control register access *************/
		  HAL_FLASH_Unlock();

		  /* Allow Access to option bytes sector */
		  HAL_FLASH_OB_Unlock();

		  /* Fill EraseInit structure*/

		  FLASH_EraseInitTypeDef EraseInitStruct;
		  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
		  EraseInitStruct.PageAddress = FLASH_STORAGE;
		  EraseInitStruct.NbPages = pages;
		  uint32_t PageError;


		  volatile uint32_t write_cnt=0, index=0;

		  volatile HAL_StatusTypeDef status;
		  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
		  while(index < data_length)
		  {
			  if (status == HAL_OK)
			  {
				  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_STORAGE+write_cnt, data_to_FLASH[index]);
				  if(status == HAL_OK)
				  {
					  write_cnt += 4;
					  index++;
				  }
			  }
		  }

			  if (status == HAL_OK)
			  {
				  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_STORAGE+len, crc);
			  }


		  HAL_FLASH_OB_Lock();
		  HAL_FLASH_Lock();

}
void FlashSettings_read(uint8_t* data_ptr, uint32_t len)
{
	volatile uint32_t read_data;
	volatile uint32_t read_cnt=0;

	do
	{
		read_data = *(uint32_t*)(FLASH_STORAGE + read_cnt);
		if(read_data != 0xFFFFFFFF)
		{
			data_ptr[read_cnt] = (uint8_t)read_data;
			data_ptr[read_cnt + 1] = (uint8_t)(read_data >> 8);
			data_ptr[read_cnt + 2] = (uint8_t)(read_data >> 16);
			data_ptr[read_cnt + 3] = (uint8_t)(read_data >> 24);
			read_cnt += 4;
		}
	}while(read_data != 0xFFFFFFFF);


}
uint8_t FlashSettings_isValid(uint32_t len)
{
	uint32_t crc = getCrcSum((void *) FLASH_STORAGE, len);
	uint32_t * flash_crc_ptr = (uint32_t *) FLASH_STORAGE;
	uint32_t use_align = (len & 0b11) ? 1 : 0;
	len = len >> 2;
	len += use_align;
	flash_crc_ptr += len;
	return (*flash_crc_ptr == crc) ? 1 : 0;

}






#endif

uint8_t FlashSettings_isValid(uint32_t len)
{
	uint32_t crc = getCrcSum((void *) FLASH_STORAGE, len);
	uint32_t * flash_crc_ptr = (uint32_t *) FLASH_STORAGE;
	uint32_t use_align = (len & 0b11) ? 1 : 0;
	len = len >> 2;
	len += use_align;
	flash_crc_ptr += len;
	return (*flash_crc_ptr == crc) ? 1 : 0;

}

int32_t getCrcSum(void * data_ptr, int32_t len)
{
    int32_t crc_sum = 0;
    int32_t * struct_p = (int32_t *) data_ptr;
    uint32_t use_align = (len & 0b11) ? 1 : 0;
    len = len >> 2;
    len += use_align;
    while (len-- > 0)
    {
        crc_sum += *struct_p;
        struct_p++;
    }
    return ~crc_sum;
}

#ifdef F4_7


 /* DEFINE the SECTORS according to your reference manual
 * STM32F446RE have:-
 *  Sector 0 to Sector 3 each 16KB
 *  Sector 4 as 64KB
 *  Sector 5 to Sector 7 each 128KB
 */

static uint32_t GetSector(uint32_t Address)
{
  uint32_t sector = 0;

  if((Address < 0x08003FFF) && (Address >= 0x08000000))
  {
    sector = FLASH_SECTOR_0;
  }
  else if((Address < 0x08007FFF) && (Address >= 0x08004000))
  {
    sector = FLASH_SECTOR_1;
  }
  else if((Address < 0x0800BFFF) && (Address >= 0x08008000))
  {
    sector = FLASH_SECTOR_2;
  }
  else if((Address < 0x0800FFFF) && (Address >= 0x0800C000))
  {
    sector = FLASH_SECTOR_3;
  }
  else if((Address < 0x0801FFFF) && (Address >= 0x08010000))
  {
    sector = FLASH_SECTOR_4;
  }
  else if((Address < 0x0803FFFF) && (Address >= 0x08020000))
  {
    sector = FLASH_SECTOR_5;
  }
  else if((Address < 0x0805FFFF) && (Address >= 0x08040000))
  {
    sector = FLASH_SECTOR_6;
  }
  else if((Address < 0x0807FFFF) && (Address >= 0x08060000))
  {
    sector = FLASH_SECTOR_7;
  }
/*  else if((Address < 0x0809FFFF) && (Address >= 0x08080000))
  {
    sector = FLASH_SECTOR_8;
  }
  else if((Address < 0x080BFFFF) && (Address >= 0x080A0000))
  {
    sector = FLASH_SECTOR_9;
  }
  else if((Address < 0x080DFFFF) && (Address >= 0x080C0000))
  {
    sector = FLASH_SECTOR_10;
  }
  else if((Address < 0x080FFFFF) && (Address >= 0x080E0000))
  {
    sector = FLASH_SECTOR_11;
  }
  else if((Address < 0x08103FFF) && (Address >= 0x08100000))
  {
    sector = FLASH_SECTOR_12;
  }
  else if((Address < 0x08107FFF) && (Address >= 0x08104000))
  {
    sector = FLASH_SECTOR_13;
  }
  else if((Address < 0x0810BFFF) && (Address >= 0x08108000))
  {
    sector = FLASH_SECTOR_14;
  }
  else if((Address < 0x0810FFFF) && (Address >= 0x0810C000))
  {
    sector = FLASH_SECTOR_15;
  }
  else if((Address < 0x0811FFFF) && (Address >= 0x08110000))
  {
    sector = FLASH_SECTOR_16;
  }
  else if((Address < 0x0813FFFF) && (Address >= 0x08120000))
  {
    sector = FLASH_SECTOR_17;
  }
  else if((Address < 0x0815FFFF) && (Address >= 0x08140000))
  {
    sector = FLASH_SECTOR_18;
  }
  else if((Address < 0x0817FFFF) && (Address >= 0x08160000))
  {
    sector = FLASH_SECTOR_19;
  }
  else if((Address < 0x0819FFFF) && (Address >= 0x08180000))
  {
    sector = FLASH_SECTOR_20;
  }
  else if((Address < 0x081BFFFF) && (Address >= 0x081A0000))
  {
    sector = FLASH_SECTOR_21;
  }
  else if((Address < 0x081DFFFF) && (Address >= 0x081C0000))
  {
    sector = FLASH_SECTOR_22;
  }
  else if (Address < 0x081FFFFF) && (Address >= 0x081E0000)
  {
    sector = FLASH_SECTOR_23;
  }*/
  return sector;
}


void WriteBoardID(uint32_t ADDR,uint32_t *data_ptr, uint8_t len)
{
	//static FLASH_EraseInitTypeDef EraseInitStruct;
	//uint32_t SECTORError;
	int sofar=0;
	DataLength++;
	/* Unlock the Flash to enable the flash control register access *************/
	HAL_FLASH_Unlock();

	 /* Erase the user Flash area */

	 /* Get the number of sector to erase from 1st sector */

	// uint32_t StartSector = GetSector(ADDR);
	// uint32_t EndSectorAddress = ADDR + len*4;
	// uint32_t EndSector = GetSector(EndSectorAddress);
  /* Fill EraseInit structure*/
	// EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
	// EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
	// EraseInitStruct.Sector        = StartSector;
	// EraseInitStruct.NbSectors     = (EndSector - StartSector) + 1;

		  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
		     you have to make sure that these data are rewritten before they are accessed during code
		     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
		     DCRST and ICRST bits in the FLASH_CR register. */
	// if (HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError) != HAL_OK)
	// {

	// }


	 /* Program the user Flash area word by word
	 	    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
	 uint32_t StartSectorAddress=ADDR;
	 while (sofar<len)
	 {
		 if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, StartSectorAddress, data_ptr[sofar]) == HAL_OK)
		 {
			 StartSectorAddress += 4;  // use StartPageAddress += 2 for half word and 8 for double word
			 sofar++;
		 }
		 else
		 {
			 /* Error occurred while writing data in Flash memory*/

		 }
	 }


	 /* Lock the Flash to disable the flash control register access (recommended
	 	     to protect the FLASH memory against possible unwanted operation) *********/
	HAL_FLASH_Lock();


}

void Erace_Sector(uint32_t ADDR)
{
	static FLASH_EraseInitTypeDef EraseInitStruct;
		uint32_t SECTORError;

		HAL_FLASH_Unlock();
		 uint32_t StartSector = GetSector(ADDR);
		 uint32_t EndSectorAddress = ADDR + DataLength*4;
		 uint32_t EndSector = GetSector(EndSectorAddress);

		 EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
		 EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
		 EraseInitStruct.Sector        = StartSector;
		 EraseInitStruct.NbSectors     = (EndSector - StartSector) + 1;

		 HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError);


		HAL_FLASH_Lock();
}


uint32_t ReadBoardID(uint32_t ADDR)
{
	volatile uint32_t read_data=0;
	read_data=*(uint32_t*)ADDR;

	return read_data;
}



void FlashSettings_write(uint8_t* data_ptr, uint32_t len)
{

	  int32_t data_length1=len/4;
	  int32_t data_length2=len%4;
	  int32_t LENGTH_FLASH=data_length1+data_length2;

	  int32_t DATA_FLASH[LENGTH_FLASH];
	  DataLength=LENGTH_FLASH;
	  int j=0;
	  for(int i=0;i<LENGTH_FLASH;i++)
	  {
	    DATA_FLASH[i]= data_ptr[j] | (data_ptr[j+1] << 8) | (data_ptr[j+2] << 16) | (data_ptr[j+3] << 24);
	    j+=4;

	   }

		//static FLASH_EraseInitTypeDef EraseInitStruct;
		//uint32_t SECTORError;


		/* Unlock the Flash to enable the flash control register access *************/
		HAL_FLASH_Unlock();

		 /* Erase the user Flash area */

		 /* Get the number of sector to erase from 1st sector */

		 //uint32_t StartSector = GetSector(FLASH_STORAGE);
		// uint32_t EndSectorAddress = FLASH_STORAGE + (LENGTH_FLASH)*4;
		// uint32_t EndSector = GetSector(EndSectorAddress);
	  /* Fill EraseInit structure*/
		// EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
		// EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
		// EraseInitStruct.Sector        = StartSector;
		// EraseInitStruct.NbSectors     = (EndSector - StartSector) + 1;

			  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
			     you have to make sure that these data are rewritten before they are accessed during code
			     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
			     DCRST and ICRST bits in the FLASH_CR register. */
		// if (HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError) != HAL_OK)
		// {

		// }


		 /* Program the user Flash area word by word
		 	    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
		 uint32_t StartSectorAddress=FLASH_STORAGE;
		 volatile uint32_t  index=0;
		 while (index<LENGTH_FLASH)
		 {
			 if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, StartSectorAddress, DATA_FLASH[index]) == HAL_OK)
			 {
				 StartSectorAddress += 4;  // use StartPageAddress += 2 for half word and 8 for double word
				 index++;
			 }
			 else
			 {
				 /* Error occurred while writing data in Flash memory*/

			 }
		 }


		 /* Lock the Flash to disable the flash control register access (recommended
		 	     to protect the FLASH memory against possible unwanted operation) *********/
		HAL_FLASH_Lock();

}

void FlashSettings_read(uint8_t* data_ptr, uint32_t len)
{
	uint16_t index_count=len+4;
	uint8_t buffer[index_count];
	volatile uint32_t read_data;
	volatile uint32_t read_cnt=0;

		do
		{
			read_data = *(uint32_t*)(FLASH_STORAGE + read_cnt);
			if(read_data != 0xFFFFFFFF)
			{
				buffer[read_cnt] = (uint8_t)read_data;
				buffer[read_cnt + 1] = (uint8_t)(read_data >> 8);
				buffer[read_cnt + 2] = (uint8_t)(read_data >> 16);
				buffer[read_cnt + 3] = (uint8_t)(read_data >> 24);
				read_cnt += 4;
			}
		}while(read_data != 0xFFFFFFFF);
		//add checkCRC


		memcpy(data_ptr,buffer,len);

}





#endif


