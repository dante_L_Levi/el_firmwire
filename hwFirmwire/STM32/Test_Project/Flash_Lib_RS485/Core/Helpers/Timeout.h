/*
 * Timeout.h
 *
 *  Created on: 26 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef HELPERS_TIMEOUT_H_
#define HELPERS_TIMEOUT_H_

#include "Timeout.h"
#include "stdint.h"
#include "stdbool.h"

typedef struct{
    int32_t timeout_counter;
    int32_t timeout_reload_value;
}timeout_s;



void Timeout_Init(timeout_s * p, uint32_t update_frq, uint32_t timeoutMs);
int8_t Timeout_Update(timeout_s * p);
void Timeout_Reload(timeout_s * p);
int8_t Timeout_getState(timeout_s * p);






#endif /* HELPERS_TIMEOUT_H_ */
