/*
 * HAL_Helpers.h
 *
 *  Created on: Dec 19, 2021
 *      Author: Lepatenko
 */

#ifndef HELPERS_HAL_HELPERS_H_
#define HELPERS_HAL_HELPERS_H_


#include "Headers.h"



bool float_Window(float lowThreshold,float HighThreshold,float value);
/***************************Write I2C Data****************************************/
void I2C1_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value);
/***************************Read I2C Data in LSM6DS0****************************************/
uint8_t I2C1_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr);



#endif /* HELPERS_HAL_HELPERS_H_ */
