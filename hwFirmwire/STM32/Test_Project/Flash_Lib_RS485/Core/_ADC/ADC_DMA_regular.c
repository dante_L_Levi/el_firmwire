/*
 * ADC_DMA_regular.c
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#include "ADC_DMA_regular.h"

typedef struct
{
	void (*InitFunctionAdc)(void);
	void (*InitFunctiondma)(void);

	void (*InitFunctionAdc_reserve)(void);
	void (*InitFunctiondma_reserve)(void);

}ADC_dma_Settings;

ADC_dma_Settings adc_setting;



void ADC_DMA_SetCallBack(void *callbackAdc,void *callbackdma)
{
	adc_setting.InitFunctionAdc=callbackAdc;
	adc_setting.InitFunctiondma=callbackdma;

}


void ADC_DMA_Init(void)
{
	adc_setting.InitFunctiondma();
	adc_setting.InitFunctionAdc();


}


void ADC_DMA_Event(ADC_HandleTypeDef *adc,uint32_t *buffer, uint8_t Length)
{
	HAL_ADC_Start_DMA(adc, buffer, Length);
}


void ADC_Reserve_DMA_SetCallBack(void *callbackAdc,void *callbackdma)
{
	adc_setting.InitFunctiondma_reserve=callbackdma;
	adc_setting.InitFunctionAdc_reserve=callbackAdc;


}
void ADC_Reserve_DMA_Init(void)
{
	adc_setting.InitFunctiondma_reserve();
	adc_setting.InitFunctionAdc_reserve();

}



