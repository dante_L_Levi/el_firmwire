#include "PI_regulator.h"

//#define DIVIDER 65536
#define DIVIDER 1
inline int32_t PI_update(PI_struct * pi, int32_t error){

	int32_t up = (pi->Kp * error) / DIVIDER;
	int32_t ui = (pi->no_overlim)?(up * pi->Ki + pi->integral):(pi->integral);
	pi->integral = ui;

	int32_t prelimit_out = up + (ui/DIVIDER);

	if(prelimit_out > pi->max){
		pi->out = pi->max;
		pi->no_overlim = 0;
	}
	else{
		if (prelimit_out < pi->min) {
			pi->out = pi->min;
			pi->no_overlim = 0;
		}
		else{
			pi->no_overlim = 1;
			pi->out = prelimit_out;
		}
	}
	//TODO do better integral limitation
	if (pi->integral > (pi->max * DIVIDER)) {
		pi->integral = pi->max * DIVIDER;
	}
	if (pi->integral < (pi->min * DIVIDER)) {
		pi->integral = pi->min * DIVIDER;
	}
	return pi->out;

}

void PI_setPgain(PI_struct * pi, int32_t P_gain){
	pi->Kp = P_gain;
}
void PI_setIgain(PI_struct * pi, int32_t I_gain){
	pi->Ki = I_gain;
}
void PI_setMaxLimit(PI_struct * pi, int32_t max_limit){
	pi->max = max_limit;
}
void PI_setMinLimit(PI_struct * pi, int32_t min_limit){
	pi->max = min_limit;
}
void PI_resetIntegrator(PI_struct * pi){
	pi->integral = 0;
}
void PI_init(PI_struct * pi, int32_t P_gain, int32_t I_gain, int32_t max_limit, int32_t min_limit){
	pi->Kp = P_gain;
	pi->Ki = I_gain;
	pi->max = max_limit;
	pi->min = min_limit;
	pi->integral = 0;
	pi->no_overlim = 1;
}
uint8_t PI_getNoOverlim(PI_struct * pi){
	return (pi->no_overlim);
}
