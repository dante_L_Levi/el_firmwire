#ifndef __PI_REGULATOR_H__
#define __PI_REGULATOR_H__
#include "stdint.h"

typedef struct{
	int32_t Ki, Kp;
	int32_t integral;
	int32_t max;
	int32_t min;
	int32_t out;
	int8_t no_overlim;
} PI_struct;


int32_t PI_update(PI_struct * pi, int32_t error);
void PI_init();
void PI_setPgain(PI_struct * pi, int32_t Kp);
void PI_setIgain(PI_struct * pi, int32_t Ki);
void PI_setMaxLimit(PI_struct * pi, int32_t max_limit);
void PI_setMinLimit(PI_struct * pi, int32_t min_limit);
void PI_resetIntegrator(PI_struct * pi);
uint8_t PI_getNoOverlim(PI_struct * pi);
#endif
