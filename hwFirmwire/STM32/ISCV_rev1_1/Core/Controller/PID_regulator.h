#ifndef __PID_REGULATOR_H__
#define __PID_REGULATOR_H__
#include "stdint.h"

typedef struct{
    int32_t Ki, Kp, Kd;
    int32_t integral;
    int32_t max;
    int32_t min;
    int32_t out;
    int16_t no_overlim;
} PID_struct;


int32_t PID_update(PID_struct * pid, int32_t error, int32_t d_error);
void PID_init(PID_struct * pid, int32_t P_gain, int32_t I_gain, int32_t D_gain, int32_t max_limit, int32_t min_limit);
void PID_setPgain(PID_struct * pid, int32_t Kp);
void PID_setIgain(PID_struct * pid, int32_t Ki);
void PID_setMaxLimit(PID_struct * pid, int32_t max_limit);
void PID_setMinLimit(PID_struct * pid, int32_t min_limit);
void PID_resetIntegrator(PID_struct * pid);
uint16_t PID_getNoOverlim(PID_struct * pid);
#endif
