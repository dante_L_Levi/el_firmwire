#include "PID_regulator.h"

#define DIVIDER 65536

int32_t PID_update(PID_struct * pid, int32_t error, int32_t d_error){

    int32_t up = (pid->Kp * error) / DIVIDER;
    int32_t ud = (pid->Kd * d_error) / DIVIDER;
//    int32_t ui = (pid->no_overlim)?(up * pid->Ki + pid->integral):(pid->integral);
    int32_t ui = (pid->no_overlim)?(error * pid->Ki + pid->integral):(pid->integral);
    pid->integral = ui;

    int32_t prelimit_out = ud + up + (ui/DIVIDER);

    if(prelimit_out > pid->max){
        pid->out = pid->max;
        pid->no_overlim = 0;
    }
    else{
        if (prelimit_out < pid->min) {
            pid->out = pid->min;
            pid->no_overlim = 0;
        }
        else{
            pid->no_overlim = 1;
            pid->out = prelimit_out;
        }
    }
    //TODO do better integral limitation
    if (pid->integral > (pid->max * DIVIDER)) {
        pid->integral = pid->max * DIVIDER;
    }
    if (pid->integral < (pid->min * DIVIDER)) {
        pid->integral = pid->min * DIVIDER;
    }
    return pid->out;

}

void PID_setPgain(PID_struct * pid, int32_t P_gain){
    pid->Kp = P_gain;
}
void PID_setIgain(PID_struct * pid, int32_t I_gain){
    pid->Ki = I_gain;
}
void PID_setDgain(PID_struct * pid, int32_t D_gain){
    pid->Kd = D_gain;
}
void PID_setMaxLimit(PID_struct * pid, int32_t max_limit){
    pid->max = max_limit;
}
void PID_setMinLimit(PID_struct * pid, int32_t min_limit){
    pid->max = min_limit;
}
void PID_resetIntegrator(PID_struct * pid){
    pid->integral = 0;
}
void PID_init(PID_struct * pid, int32_t P_gain, int32_t I_gain, int32_t D_gain, int32_t max_limit, int32_t min_limit){
    pid->Kp = P_gain;
    pid->Ki = I_gain;
    pid->Kd = D_gain;
    pid->max = max_limit;
    pid->min = min_limit;
    pid->integral = 0;
    pid->no_overlim = 1;
}
uint16_t PID_getNoOverlim(PID_struct * pid){
    return (pid->no_overlim);
}
