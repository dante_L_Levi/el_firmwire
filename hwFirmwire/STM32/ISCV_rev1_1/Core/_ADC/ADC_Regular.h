/*
 * ADC_Regular.h
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#ifndef ADC_ADC_REGULAR_H_
#define ADC_ADC_REGULAR_H_

#include "Headers.h"


void ADC_init_RegChannels(void);
void ADC_Regul_Read(void);
void ADC_Regul_GetValue(uint16_t* data);


#endif /* ADC_ADC_REGULAR_H_ */
