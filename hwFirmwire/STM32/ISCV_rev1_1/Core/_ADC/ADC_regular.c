/*
 * ADC_regular.c
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#include "ADC_Regular.h"

uint16_t adc_value[2]={0};

ADC_ChannelConfTypeDef Config = {0};


void ADC_init_RegChannels(void)
{
	 /* USER CODE BEGIN ADC2_Init 0 */

	  /* USER CODE END ADC2_Init 0 */



	  /* USER CODE BEGIN ADC2_Init 1 */

	  /* USER CODE END ADC2_Init 1 */
	  /** Common config
	  */
	  hadc2.Instance = ADC2;
	  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
	  hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
	  hadc2.Init.ContinuousConvMode = ENABLE;
	  hadc2.Init.DiscontinuousConvMode = DISABLE;
	  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	  hadc2.Init.NbrOfConversion = 1;
	  hadc2.Init.DMAContinuousRequests = DISABLE;
	  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	  hadc2.Init.LowPowerAutoWait = DISABLE;
	  hadc2.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
	  if (HAL_ADC_Init(&hadc2) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  /* USER CODE BEGIN ADC2_Init 2 */

	  /* USER CODE END ADC2_Init 2 */
}



void ADC_Select_CH14(void)
{
	/** Configure Regular Channel
		  */
	Config.Channel = ADC_CHANNEL_14;
	Config.Rank = ADC_REGULAR_RANK_1;
	Config.SingleDiff = ADC_SINGLE_ENDED;
	Config.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
	Config.OffsetNumber = ADC_OFFSET_NONE;
	Config.Offset = 0;
		  if (HAL_ADC_ConfigChannel(&hadc2, &Config) != HAL_OK)
		  {

		  }
}

void ADC_Select_CH13(void)
{
	/** Configure Regular Channel
		  */
	Config.Channel = ADC_CHANNEL_13;
	Config.Rank = ADC_REGULAR_RANK_1;
	Config.SingleDiff = ADC_SINGLE_ENDED;
	Config.SamplingTime = ADC_SAMPLETIME_61CYCLES_5;
	Config.OffsetNumber = ADC_OFFSET_NONE;
	Config.Offset = 0;
		  if (HAL_ADC_ConfigChannel(&hadc2, &Config) != HAL_OK)
		  {

		  }
}



void ADC_Regul_Read(void)
{
	ADC_Select_CH14();
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 1000);
	adc_value[0]=HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);

	ADC_Select_CH13();
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 1000);
	adc_value[1]=HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);

}

void ADC_Regul_GetValue(uint16_t* data)
{
	data[0]=adc_value[0];
	data[1]=adc_value[1];
}
