/*
 * PID_PWR.h
 *
 *  Created on: Dec 5, 2021
 *      Author: Lepatenko
 */

#ifndef PID_CONTROLLER_PWR_PID_PWR_H_
#define PID_CONTROLLER_PWR_PID_PWR_H_


#include "Headers.h"


typedef struct
{
	float error;
	float integralFilter;
	float derivativeFilter;
	float proportionalComponent;
	float integralComponent;
	float derivativeComponent;
	float tempPID;
	float outputPID;

}Main_PID;


typedef struct
{
	float feedback;
	float reference;
	float deltaTimeSampling;
	float MaxOutValue;
	float MinimumOutValue;

	struct {
	   float proportional;
	   float integral;
	   float derivative;
	   float coefficientBackSaturation;
	   float filterDerivative;
	        } coefficient;

	        struct {
	                    float lowThershold;
	                    float highThershold;
	                } saturation;


}Config_PWR_PID;


typedef struct
{
	Main_PID	 		_mainPID;
	Config_PWR_PID		_configure_PID;
}PID_Controller;



void  SetReference(float ref,PID_Controller *pid);
void SetFeedBack(float feedback,PID_Controller *pid);

void SetCoefficient(float Kp, float Ki, float Kd, float BackSaturation, float filterDerivative,
		PID_Controller *pid);

void SetCoefficient_Kp(float Kp,PID_Controller *pid);
void SetCoefficient_Ki(float Ki,PID_Controller *pid);
void SetCoefficient_Kd(float Kd,PID_Controller *pid);

void  SetSaturation(float lowLimit,float highLimit,PID_Controller *pid);

void Compute_PID(PID_Controller *pid);

float GetPID_Value(PID_Controller *pid);





#endif /* PID_CONTROLLER_PWR_PID_PWR_H_ */
