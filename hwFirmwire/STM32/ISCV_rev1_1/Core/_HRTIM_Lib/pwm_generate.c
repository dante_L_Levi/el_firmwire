/*
 * HRTIM.c
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Lepatenko
 */

#include <pwm_generate.h>

static uint16_t periodHrpwm = 45000;     // Fsw = 207 kHz


static void HRTIM_Init_GPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC->AHBENR  |= RCC_AHBENR_GPIOAEN;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);

		  	  /* Configure HRTIM output: TA1 (PA8)*/
	GPIO_InitStruct.Pin = GPIO_PIN_8 ;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;;
	GPIO_InitStruct.Alternate = GPIO_AF13_HRTIM1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	    /* Configure HRTIM output: TB1 (PA10)*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
	GPIO_InitStruct.Pin = GPIO_PIN_10 ;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;;
	GPIO_InitStruct.Alternate = GPIO_AF13_HRTIM1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}



void Init_HRTIM(void)
{
		HRTIM_Init_GPIO();

		RCC->CFGR3   |= RCC_CFGR3_HRTIM1SW_PLL;                                                 // Enable multiplier x2 for PLL frequency
	    RCC->APB2ENR |= RCC_APB2ENR_HRTIM1EN;                                                   // Enable clock for HRPWM

	    HRTIM1->sCommonRegs.DLLCR |= HRTIM_DLLCR_CAL | HRTIM_DLLCR_CALEN;                       // Start timer's calibration
	    while ((HRTIM1->sCommonRegs.ISR & HRTIM_ISR_DLLRDY) == RESET);                          // Waiting for the end fo calibration

	    HRTIM1->sTimerxRegs[0].PERxR = periodHrpwm;                                      // Set period for timer
	    HRTIM1->sTimerxRegs[0].CMP1xR = 20000;                                                      // Set starting duty
	    HRTIM1->sTimerxRegs[1].PERxR = periodHrpwm;
	    HRTIM1->sTimerxRegs[1].CMP1xR = 20000;

	    HRTIM1->sTimerxRegs[0].OUTxR |= HRTIM_OUTR_DTEN;                                        // Enable dead-time
	    HRTIM1->sTimerxRegs[0].DTxR  |= (3 << HRTIM_DTR_DTPRSC_Pos);                            // Set Tdtg = (2^3) * 868 ps = 6.94 ns
	    HRTIM1->sTimerxRegs[0].DTxR  |= (5 << HRTIM_DTR_DTR_Pos) | (5 << HRTIM_DTR_DTF_Pos);    // Set dead-time rising and falling = 5 * Ttg = 34.7 ns
	    HRTIM1->sTimerxRegs[0].DTxR  |= HRTIM_DTR_DTFSLK | HRTIM_DTR_DTRSLK;                    // Lock value dead-time
	    HRTIM1->sTimerxRegs[1].OUTxR |= HRTIM_OUTR_DTEN;
	    HRTIM1->sTimerxRegs[1].DTxR  |= (3 << HRTIM_DTR_DTPRSC_Pos);
	    HRTIM1->sTimerxRegs[1].DTxR  |= (5 << HRTIM_DTR_DTR_Pos) | (5 << HRTIM_DTR_DTF_Pos);
	    HRTIM1->sTimerxRegs[1].DTxR  |= HRTIM_DTR_DTFSLK | HRTIM_DTR_DTRSLK;

	    HRTIM1->sTimerxRegs[0].SETx1R |= HRTIM_SET1R_PER;	                                    // Event forces the output to active state
	    HRTIM1->sTimerxRegs[0].RSTx1R |= HRTIM_RST1R_CMP1;                                      // Event forces the output to inactive state
	    HRTIM1->sTimerxRegs[1].SETx1R |= HRTIM_SET1R_PER;
	    HRTIM1->sTimerxRegs[1].RSTx1R |= HRTIM_RST1R_CMP1;

	    HRTIM1->sTimerxRegs[0].TIMxCR |= HRTIM_TIMCR_CONT;                                      // Continuous mode
	    HRTIM1->sTimerxRegs[1].TIMxCR |= HRTIM_TIMCR_CONT;

	    HRTIM1->sCommonRegs.OENR |= HRTIM_OENR_TA1OEN | HRTIM_OENR_TB1OEN;                      // Enable output PWM channel A and B

	    HRTIM1->sMasterRegs.MPER = periodHrpwm;                                          // Period for master timer
	    HRTIM1->sMasterRegs.MCR |= HRTIM_MCR_MCEN | HRTIM_MCR_TACEN | HRTIM_MCR_TBCEN;          // Enable counter for Master and timer A and B
}

void Init_HRTIM_Push_Pull_Mode(void)
{
	HRTIM_Init_GPIO();

	RCC->CFGR3   |= RCC_CFGR3_HRTIM1SW_PLL;                                                 // Enable multiplier x2 for PLL frequency
	RCC->APB2ENR |= RCC_APB2ENR_HRTIM1EN;                                                   // Enable clock for HRPWM

	HRTIM1->sCommonRegs.DLLCR |= HRTIM_DLLCR_CAL | HRTIM_DLLCR_CALEN;                       // Start timer's calibration
	while ((HRTIM1->sCommonRegs.ISR & HRTIM_ISR_DLLRDY) == RESET);                          // Waiting for the end fo calibration

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].PERxR=periodHrpwm;												// Set period for timer
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP1xR = 0;														// Set starting duty

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_B].PERxR=periodHrpwm;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_B].CMP1xR = 0;

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].RSTxR |= HRTIM_RSTR_MSTPER;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].SETx1R |= HRTIM_SET1R_MSTPER;//Event forces the output to active state for channel A
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].RSTx1R |= HRTIM_RST1R_CMP1;	//Event forces the output to inactive state for channel A

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_B].RSTxR |= HRTIM_RSTR_MSTCMP1;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_B].SETx1R |= HRTIM_SET1R_MSTCMP1;//Event forces the output to active state for channel B
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_B].RSTx1R |= HRTIM_RST1R_CMP1;//Event forces the output to inactive state for channel B

	/*
		 *
		 * Select to Continuos mode + update Master timer
		 */
	HRTIM1->sTimerxRegs[0].TIMxCR|=HRTIM_TIMCR_CONT|HRTIM_TIMCR_MSTU;
	HRTIM1->sTimerxRegs[1].TIMxCR|=HRTIM_TIMCR_CONT|HRTIM_TIMCR_MSTU;

		// Enable output PWM channel A and B
	 HRTIM1->sCommonRegs.OENR |= HRTIM_OENR_TA1OEN | HRTIM_OENR_TB1OEN;
		 // Period for master timer
	HRTIM1->sMasterRegs.MPER = periodHrpwm;

	HRTIM1->sMasterRegs.MCMP1R=22500;
	HRTIM1->sMasterRegs.MCMP2R=11250;
		 //Continius mode
		 //preed Load
		 //Enable update
	HRTIM1->sMasterRegs.MCR|=HRTIM_MCR_CONT|HRTIM_MCR_PREEN|HRTIM_MCR_MREPU;
		//Start
	//HRTIM1->sMasterRegs.MCR |= HRTIM_MCR_MCEN|HRTIM_MCR_TACEN|HRTIM_MCR_TBCEN;          // Enable counter for Master and timer A and B


}

uint32_t Get_Max_Duty(void)
{
	return periodHrpwm;
}

void Start_Hrtim(void)
{
	HRTIM1->sMasterRegs.MCR |= HRTIM_MCR_MCEN|HRTIM_MCR_TACEN|HRTIM_MCR_TBCEN;
}


void Stop_Hrtim(void)
{
	HRTIM1->sMasterRegs.MCR &=~( HRTIM_MCR_MCEN|HRTIM_MCR_TACEN|HRTIM_MCR_TBCEN);
}



void HRTIM_Set_Duty(Channels_PWM _ch,uint16_t duty)
{
	switch(_ch)
	{
		case ChannelA:
		{
			 HRTIM1->sTimerxRegs[0].CMP1xR = duty;
			break;
		}
		case ChannelB:
		{
			HRTIM1->sTimerxRegs[1].CMP1xR = duty;
			break;
		}

		case ChannelC:
		{
			HRTIM1->sTimerxRegs[2].CMP1xR = duty;
			break;
		}
		case ChannelD:
		{
			HRTIM1->sTimerxRegs[3].CMP1xR = duty;
			break;
		}

		case ChannelE:
		{
			HRTIM1->sTimerxRegs[4].CMP1xR = duty;
			break;
		}
	}
}
