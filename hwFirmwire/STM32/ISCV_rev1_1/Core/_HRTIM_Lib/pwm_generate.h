/*
 * HRTIM.h
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef HRTIM_LIB_PWM_GENERATE_H_
#define HRTIM_LIB_PWM_GENERATE_H_


#include "Headers.h"

typedef enum
{
	ChannelA ,
	ChannelB,
	ChannelC,
	ChannelD,
	ChannelE
}Channels_PWM;


void Init_HRTIM_Push_Pull_Mode(void);
void HRTIM_Set_Duty(Channels_PWM _ch,uint16_t duty);
uint32_t Get_Max_Duty(void);
void Init_HRTIM(void);
void Stop_Hrtim(void);
void Start_Hrtim(void);


#endif /* HRTIM_LIB_PWM_GENERATE_H_ */
