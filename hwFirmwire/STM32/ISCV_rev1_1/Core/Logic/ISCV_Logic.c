/*
 * ISCV_Logic.c
 *
 *  Created on: Apr 17, 2022
 *      Author: Lepatenko
 */
#include "ISCV_Logic.h"
#include "ISCV_types.h"
#include "ISCV_Hal.h"
#include "math.h"



#define PWR_ACS723			5000
#define Current_K1			-0.0784
#define Current_K2			2.5096
#define Voltage_MAX_GB		14
#define DAC_Threshold_Default		2855 //20A

static  const uint16_t * rs485_id =  (const uint16_t *)_board_id;
work_state_model_s _model_state;


static void check_work_Push_pull(void);
static void Set_ToFlash_Data(void);

static void App_GetSettings_flash(void);
static void Settings_Analog_Input(void);
static void update_status_Charge(void);
static void Read_Adc_Sense(void);
static void Middle_Loop_push_pull_Sync(void);
static void Fast_Loop_push_pull_Sync(void);
static void update_Indicate_External(status_work_Converter status);

void Update_sync(void)
{
	_model_state.sync_trigger=1;

}

void Update_work(void)
{
	_model_state._time_ms++;

	update_status_Charge();
}

void Interrupt_Prot_Current(void)
{
	//stop pwm
	Set_Enable_DRV(false);
	Stop_Hrtim();
	_model_state.status=STATUS_ERROR;
	_model_state.status_regulator=None_Work;
	_model_state.status_prot=false;

}

void Can_Hundler_Reciever(uint8_t* data,CAN_RxHeaderTypeDef *dataHeader)
{
	//add hundler recieve

}

static void update_status_Charge(void)
{
	(_model_state._adc_value2._data.fields.extern_in_D<12000 &&
			_model_state._adc_value1._data.fields.output_14V<16000 &&
			_model_state._adc_value1._data.fields.output_14V>12000)?(_model_state._time_start_timeout=_model_state._time_ms):
					(_model_state._time_start_timeout=0);

	if(_model_state._adc_value2._data.fields.extern_in_D<12000
			&& _model_state._state_Baterries.GB1>13.200 &&
			 _model_state._state_Baterries.GB1<16.000 &&
			 _model_state._adc_value1._data.fields.output_28V>20000 &&
	(_model_state._time_ms-_model_state._time_start_timeout>=120000))
	{
		_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;
		_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;

		//SetReference(_model_state.CurrentRef/4,&_model_state._dataPID_Current);

		//TODO add function GPIO
		Set_Enable_DRV(true);
	}
}

static void Settings_Analog_Input(void)
{
	_model_state._adc_value1._settings[0].Kp=1.0;
	_model_state._adc_value1._settings[1].Kp=2.0;
	_model_state._adc_value1._settings[2].Kp=8.844;
	_model_state._adc_value1._settings[3].Kp=4.43;

	_model_state._adc_value2._settings[0].Kp=5.883;
	_model_state._adc_value2._settings[1].Kp=5.55;

	Set_PWR_ACS723(PWR_ACS723);//type sensor
	Set_Sensivity(200);//sensivity mV
	Set_Vnull_MUX(0.1);//MUX for Converted

}

static void App_GetSettings_flash(void)
{

}

void Restore_Pid_Settings(void)
{

}

static void Set_ToFlash_Data(void)
{

}

//todo переделать функцию проверки работы ...
static void check_work_Push_pull(void)
{
	if(_model_state.status==STATUS_TEST)
	{
		_model_state.step_work=0;
		Set_Enable_DRV(true);

		return;

	}

	if(_model_state._state_Baterries.GB1<13.200 &&
			_model_state._state_Baterries.GB1>5.000)
	{
		_model_state.status=STATUS_ERROR_Low_Input_Voltage;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;

	}

	if((_model_state._state_Baterries.GB1<5.000))
	{
		_model_state.status=STATUS_ERROR_Short_GB1;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}

	if(_model_state._state_Baterries.GB1>16.000)
	{
		_model_state.status=STATUS_ERROR_High_Input_Voltage;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}

	if(_model_state._state_Baterries.GB2<1.000)
	{
		_model_state.status=STATUS_ERROR_Breakage_OUT_NET;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}

	if(_model_state._adc_value1._data.fields.input_current>50000||
			_model_state._adc_value1._data.fields.input_current<-10000)
	{
		_model_state.status=STATUS_ERROR_Short_GB1;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}
	if(_model_state._state_Baterries.Current_charge_GB2>28.0)
	{
		_model_state.status=STATUS_ERROR_Short_GB2;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}

	if(_model_state._state_Baterries.GB1>12.3&&
	   _model_state._state_Baterries.GB1<16.0 &&
	   _model_state._adc_value2._data.fields.extern_in_D<5000)
	{
		_model_state.status=STATUS_NORMAL_GEN_OFF;
		Set_Enable_DRV(false);
		Stop_Hrtim();
		return;
	}

	if(_model_state._adc_value2._data.fields.extern_in_D<12000 &&
				_model_state._state_Baterries.GB1>13.200 &&
							 _model_state._state_Baterries.GB1<16.000)
	{

	}

	//test 1 STATUS_NORMAL_Charge_GEN_OFF
		if(_model_state._adc_value2._data.fields.extern_in_D<12000
		&& _model_state._state_Baterries.GB1>13.200 &&
		_model_state._state_Baterries.GB1<16.000 &&
		_model_state._adc_value1._data.fields.output_28V>20000
		/*add check timout*/)
		{
			_model_state.status=STATUS_NORMAL_Charge_GEN_OFF;
			//SetReference(_model_state.CurrentRef/4,&_model_state._dataPID_Current);
			Start_Hrtim();
			Set_Enable_DRV(true);
			return;
		}

		//test 2 STATUS_NORMAL_Charge_GEN_ON Main Mode
		if(_model_state._adc_value2._data.fields.extern_in_D>=12100
		&&_model_state._adc_value1._data.fields.output_14V>13000 &&
		_model_state._adc_value1._data.fields.output_14V<16000)
		{
			_model_state.status=STATUS_NORMAL_GEN_ON;
			//SetReference(_model_state.CurrentRef,&_model_state._dataPID_Current);
			Set_Enable_DRV(true);
			Start_Hrtim();
			_model_state._pwm_value.dutyA=200;
			_model_state._pwm_value.dutyB=200;
			HRTIM_Set_Duty(ChannelA, _model_state._pwm_value.dutyA);
			HRTIM_Set_Duty(ChannelB, _model_state._pwm_value.dutyB);
			return;
		}



}

static void Init_Logic(void)
{
	//read flash config
		//App_GetSettings_flash();
		Restore_Pid_Settings();


		_model_state.status_prot=true;
		_model_state.RS485_Connect=false;

		Settings_Analog_Input();


		//set Value DAC for REF Protected
		_model_state._current_DAC.Threshold_Protected=DAC_Threshold_Default;
		_model_state._dac_value.outDAC=DAC_Threshold_Default;

		_model_state._pwm_value.dutyA=0;
		_model_state._pwm_value.dutyB=0;




		_model_state.status_regulator=None_Work;
		_model_state._time_ms=0;


		_model_state._tx_header.IDE=CAN_ID_STD;
		_model_state._tx_header.StdId=CAN_ID_1_Main;
		_model_state._tx_header.RTR=CAN_RTR_DATA;


		_model_state._settings_dataPID[0].fields.Kp=10;
		_model_state._settings_dataPID[0].fields.Ki=0.0f;
		_model_state._settings_dataPID[0].fields.MAX=10;
		_model_state._settings_dataPID[0].fields.MIN=-10;
		_model_state._regulator.CurrentRef=19.0;
		_model_state._regulator.VoltageRef=28.0;
		_model_state._regulator.Current_threshold=0.160f;

		PI_init(&_model_state._regulator.V_regulator,
				(int32_t)_model_state._settings_dataPID[0].fields.Kp,
				(int32_t)_model_state._settings_dataPID[0].fields.Ki,
				(int32_t)_model_state._settings_dataPID[0].fields.MAX,
				(int32_t)_model_state._settings_dataPID[0].fields.MIN

				);












}

void Main_Init(void)
{
	_model_state.status=STATUS_INIT;
	Init_Logic();

	Set_Period_Toggle(8000);
	LED_Init();
	MX_GPIO_Init();

	DAC_Init();
	DAC_ENABLE(DAC_CHANNEL1);
	Set_DAC_Value(_model_state._current_DAC.Threshold_Protected,DAC_CHANNEL1);
	_model_state.DEVICE=*rs485_id;

	RS485_SProtocol_init(_model_state.DEVICE,RS485_BAUDRATE,*RS485_dataTX);
	RS485_Reciever_Helper();

	Init_HRTIM_Push_Pull_Mode();


	CAN_INit_HW(MX_CAN_Init);
	CAN_Set_Filter(0xFFFF,0,0xFFFF,0);
	Set_CAN_Rx_Config(_model_state._rx_header);
	Set_RxPacket_CallBack(Can_Hundler_Reciever);

	Init_ADC_Sense_Inject();





		_model_state._pwm_value.dutyA=180;
		_model_state._pwm_value.dutyB=180;
		HRTIM_Set_Duty(ChannelA, _model_state._pwm_value.dutyA);
		HRTIM_Set_Duty(ChannelB, _model_state._pwm_value.dutyB);

	Init_Tim6_Sync(FAST_LOOP);
	Set_CallBack(6,Update_sync);

	Init_Tim7_Sync(FAST2_LOOP);
	Set_CallBack(7,Update_work);


	Start_Hrtim();


}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}


/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


static void Read_Adc_Sense(void)
{
	_model_state._adc1.fields.input_current=Get_ADC_Inject_Channels_ADC_Code(0);
	_model_state._adc1.fields.output_current=Get_ADC_Inject_Channels_ADC_Code(1);
	_model_state._adc1.fields.output_28V=Get_ADC_Inject_Channels_ADC_Code(2);
	_model_state._adc1.fields.output_14V=Get_ADC_Inject_Channels_ADC_Code(3);



	//ADC1 Read converted
	_model_state._adc_value1._data.fields.input_current=(Current_K2-Get_ADC_Inject_Channels(0))/Current_K1;
	_model_state._adc_value1._data.fields.output_current=Get_Current_ACS723(Get_ADC_Inject_Channels(1)*
			_model_state._adc_value1._settings[1].Kp);
	_model_state._adc_value1._data.fields.output_28V=_model_state._adc1.fields.output_28V*
			_model_state._adc_value1._settings[2].Kp;
	_model_state._adc_value1._data.fields.output_14V=_model_state._adc1.fields.output_14V*
			_model_state._adc_value1._settings[3].Kp;




	//Model data Logic
	_model_state._state_Baterries.GB1=(float)_model_state._adc_value1._data.fields.output_14V/1000.0;
	_model_state._state_Baterries.GB2=((float)_model_state._adc_value1._data.fields.output_28V/1000.0)-
			_model_state._state_Baterries.GB1;
	_model_state._state_Baterries.Current_charge_GB2=(float)(_model_state._adc_value1._data.fields.output_current/1000.0);


}


static void Control_Voltage_regulator(void)
{
	_model_state._regulator.error=_model_state._regulator.VoltageRef*1000
				-_model_state._adc_value1._data.fields.output_28V;
		_model_state._regulator.duty_res=PI_update(&_model_state._regulator.V_regulator,_model_state._regulator.error);

		if(_model_state._pwm_value.dutyA>120)
		{
			_model_state._pwm_value.dutyA+=(int16_t)_model_state._regulator.duty_res;
			_model_state._pwm_value.dutyB+=(int16_t)_model_state._regulator.duty_res;
		}
		else if(_model_state._regulator.duty_res>0)
		{
			_model_state._pwm_value.dutyA+=(int16_t)_model_state._regulator.duty_res;
			_model_state._pwm_value.dutyB+=(int16_t)_model_state._regulator.duty_res;
		}
		Update_Pwm_Hrtim(&_model_state._pwm_value);
}

static void Control_Current_regulator(void)
{
	_model_state._regulator.error=_model_state._regulator.CurrentRef*1000
					-_model_state._adc_value1._data.fields.output_current;

	_model_state._regulator.duty_res=PI_update(&_model_state._regulator.I_regulator,_model_state._regulator.error);
	if(_model_state._pwm_value.dutyA>120)
	{
		_model_state._pwm_value.dutyA+=(int16_t)_model_state._regulator.duty_res;
		_model_state._pwm_value.dutyB+=(int16_t)_model_state._regulator.duty_res;
	}
	else if(_model_state._regulator.duty_res>0)
	{
		_model_state._pwm_value.dutyA+=(int16_t)_model_state._regulator.duty_res;
		_model_state._pwm_value.dutyB+=(int16_t)_model_state._regulator.duty_res;
	}
	Update_Pwm_Hrtim(&_model_state._pwm_value);
}

static void Main_Push_pull_Regulator(void)
{
	if(_model_state._adc_value1._data.fields.output_current<_model_state._regulator.Current_threshold*1000.0f)//CV
	{
		_model_state.status_regulator=CV_Work;
		Control_Voltage_regulator();
	}
	else if((_model_state._adc_value1._data.fields.output_28V<_model_state._regulator.VoltageRef*1000.0f) &&
			_model_state._adc_value1._data.fields.output_current<(_model_state._regulator.CurrentRef*900.0f))//CC
	{
		_model_state.status_regulator=CC_Work;
		Control_Current_regulator();
	}

}

static void Rs485_MainData_Packet(void)
{
	_model_state._data2._Battery_State=_model_state._state_Baterries;

	_model_state._data2._ainData1=_model_state._adc_value1._data;

	_model_state._data2._ainData2=_model_state._adc_value2._data;

	_model_state._data2._current_DAC=_model_state._current_DAC;

	_model_state._data2._gpioState=_model_state._gpio_state;

	_model_state._data2._pwm_Value=_model_state._data2._pwm_Value;

	_model_state._data2._status=(uint8_t)_model_state.status;
	_model_state._data2._status_regulator=(uint8_t)_model_state.status_regulator;



}

void ISCV_Sync(void)
{
	Fast_Loop_push_pull_Sync();
	Middle_Loop_push_pull_Sync();
}

static void Fast_Loop_push_pull_Sync(void)
{
	Read_Adc_Sense();
	//todo edit logic for regulator
	Main_Push_pull_Regulator();
	/*if(_model_state.status!=STATUS_TEST)
	{
		check_work_Push_pull();
		Main_Push_pull_Regulator();
	}*/
	Rs485_MainData_Packet();




}

static void Middle_Loop_push_pull_Sync(void)
{
	static int32_t skip_counter = 1;
		    if (skip_counter--)
		        return;
	skip_counter = (FAST_LOOP_SPEED / MIDDLE_LOOP_SPEED) - 1;

	Read_state_Gen();
	GPIO_Helpers_Set(&_model_state._gpio_state);
	update_Indicate_External(_model_state.status);

}




static void Can_Packet_1(void)
{
	_model_state._tx_Packet1_can.Status_work=_model_state.status;
	_model_state._tx_Packet1_can.GB1_percent=(100*_model_state._state_Baterries.GB1)
					/Voltage_MAX_GB;
	_model_state._tx_Packet1_can.GB2_percent=(100*_model_state._state_Baterries.GB2)
						/Voltage_MAX_GB;
	_model_state._tx_header.StdId=CAN_ID_1_Main;
	_model_state._tx_header.IDE=CAN_ID_STD;
	_model_state._tx_header.RTR=CAN_RTR_DATA;
	_model_state._tx_header.DLC=sizeof(_model_state._tx_Packet1_can);
	_model_state.Mail_Tx_Box1=1;
	CAN_Transmit_Packet((uint8_t*)&_model_state._tx_Packet1_can,
				_model_state._tx_header,_model_state.Mail_Tx_Box1);
}
static void Can_Packet_2(void)
{
	_model_state._tx_Packet2_can.Status_work=_model_state.status;
	_model_state._tx_Packet2_can.GB1_voltage=(int16_t)_model_state._adc_value1._data.fields.output_14V;
	_model_state._tx_Packet1_can.GB2_percent=(int16_t)_model_state._adc_value1._data.fields.output_28V;
	_model_state._tx_header.StdId=CAN_ID_2_Main;
	_model_state._tx_header.IDE=CAN_ID_STD;
	_model_state._tx_header.RTR=CAN_RTR_DATA;
	_model_state._tx_header.DLC=sizeof(_model_state._tx_Packet2_can);
	_model_state.Mail_Tx_Box2=2;
	CAN_Transmit_Packet((uint8_t*)&_model_state._tx_Packet2_can,
			_model_state._tx_header,_model_state.Mail_Tx_Box2);
}

static void update_Indicate_External(status_work_Converter status)
{
	switch(status)
	{
		case STATUS_INIT:
		{
			//function Indicate

			//function CAN Packet Transmit

			break;
		}
		case STATUS_NORMAL:
		case STATUS_NORMAL_GEN_ON:
		{
			Can_Packet_1();
			break;
		}

		case STATUS_NORMAL_GEN_OFF:
		{


			break;
		}

		case STATUS_TEST:
		{

			break;
		}

		case STATUS_NORMAL_Charge_GEN_OFF:
		{
			indicate_Extern_Status(12);
			Can_Packet_1();
			break;
		}

		case STATUS_ERROR_Input_Voltage:
		{
			//function Indicate
			indicate_Extern_Status(2);
			//function CAN Packet Transmit
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_OUTPUT_Voltage:
		{
			//function Indicate
			indicate_Extern_Status(3);
			//function CAN Packet Transmit
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Breakage_OUT_NET:
		{
			indicate_Extern_Status(0);
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Short_OUT_NET:
		{
			indicate_Extern_Status(1);
			//CAN Packet
			Can_Packet_2();
			break;
		}

		case STATUS_ERROR_Low_Input_Voltage:
		{
			indicate_Extern_Status(5);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Low_Voltage_D:
		{
			indicate_Extern_Status(4);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Short_GB1:
		{
			indicate_Extern_Status(6);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_Short_GB2:
		{
			indicate_Extern_Status(7);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		case STATUS_ERROR_High_Input_Voltage:
		{
			indicate_Extern_Status(8);
			//CAN Packet
			Can_Packet_2();
			break;
		}
		default:
			break;

	}

}



typedef enum
{
    cmd_Set_Connect=0x00,

	cmd_Get_Board=0x01,
	cmd_Get_main_data,
	cmd_Get_setting_peripheral_board,

	cmd_Set_Enable_CTRL,
	cmd_Set_setting_peripheral_board_PID_Voltage,
	cmd_Set_setting_peripheral_board_PID_Current,
	cmd_Set_Default_setting_peripheral_board,
	cmd_Set_Gpio_state,
	cmd_Set_PWM_Duty,
	cmd_Set_DAC_state,

	cmd_Set_cmd_flash

}Command_Board;




static void function_Set_Connect(uint8_t* data)
{
	Set_Period_Toggle(1000);
}

static void function_Get_Board(uint8_t* data)
{
	_model_state._data1.ID_temp=_model_state.DEVICE;
	memcpy(_model_state._data1.Name,model,8);
	memcpy(_model_state._data1.firmwire,firmware,8);

	RS485_Sprotocol_Data_def msg;
	msg.cmd=cmd_Get_Board;
	msg.length=sizeof(Transmit_data1_Model_DataBoard);
	msg.data=(uint8_t*)&_model_state._data1;
	SProtocol_addTxMessage(&msg);
}

static void function_Get_main_data(uint8_t* data)
{
	RS485_Sprotocol_Data_def msg;
	msg.cmd=cmd_Get_main_data;
	msg.length=sizeof(Transmit_data2_Model_MainData);
	msg.data=(uint8_t*)&_model_state._data2;
	SProtocol_addTxMessage(&msg);
}

static void function_Get_setting_peripheral_board(uint8_t* data)
{

}

static void function_Set_Enable_CTRL(uint8_t* data)
{
	data[0]==1?(_model_state.status=STATUS_TEST):
			(_model_state.status=STATUS_INIT);

	Start_Hrtim();
	_model_state._pwm_value.dutyA=300;
	_model_state._pwm_value.dutyB=300;
	Update_Pwm_Hrtim(&_model_state._pwm_value);

}

static void function_Set_setting_peripheral_board_PID_Voltage(uint8_t* data)
{

}

static void function_Set_setting_peripheral_board_PID_Current(uint8_t* data)
{

}

static void function_Set_Default_setting_peripheral_board(uint8_t* data)
{

}

static void function_Set_Gpio_state(uint8_t* data)
{
	gpio_out_s*_statesGpio=(gpio_out_s*)&data[0];
	_model_state._gpio_state.all=_statesGpio->all;
	GPIO_Helpers_Set(&_model_state._gpio_state);
}

static void function_Set_PWM_Duty(uint8_t* data)
{

	pwm_value_s* _pwm=(pwm_value_s*)data;

	_model_state._pwm_value.dutyA=_pwm->dutyA;
	_model_state._pwm_value.dutyB=_pwm->dutyB;
	Update_Pwm_Hrtim(&_model_state._pwm_value);

}

static void function_Set_DAC_state(uint8_t* data)
{
	memcpy(&_model_state._current_DAC,data,sizeof(Current_Protected_def));
	Set_DAC_Value(_model_state._current_DAC.Threshold_Protected,DAC_CHANNEL1);
}

static void function_Set_cmd_flash(uint8_t* data)
{

}


/*==================================================================================*/
#define SIZE_NUM_CMD				12
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{

		(rs485_command_handler)function_Set_Connect,
		(rs485_command_handler)function_Get_Board,
		(rs485_command_handler)function_Get_main_data,
		(rs485_command_handler)function_Get_setting_peripheral_board,
		(rs485_command_handler)function_Set_Enable_CTRL,
		(rs485_command_handler)function_Set_setting_peripheral_board_PID_Voltage,
		(rs485_command_handler)function_Set_setting_peripheral_board_PID_Current,
		(rs485_command_handler)function_Set_Default_setting_peripheral_board,
		(rs485_command_handler)function_Set_Gpio_state,
		(rs485_command_handler)function_Set_PWM_Duty,
		(rs485_command_handler)function_Set_DAC_state,
		(rs485_command_handler)function_Set_cmd_flash
};

/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
					        return;

			RS485_Sprotocol_Data_def mess;
			Get_RS485RxMessage(&mess);
			rs485_commands_array[mess.cmd](mess.dataRxPayload);
			RS485_SetPkgAvailable(0);
}
