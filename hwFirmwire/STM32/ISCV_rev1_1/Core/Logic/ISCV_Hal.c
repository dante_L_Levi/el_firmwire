/*
 * ISCV_Hal.c
 *
 *  Created on: 20 апр. 2022 г.
 *      Author: Lepatenko
 */

#include "ISCV_Hal.h"


extern work_state_model_s _model_state;
uint8_t count_saveToogle_Led=0;
uint8_t count_external_Led=0;
uint8_t CNT_LED=0;
uint32_t iter;
bool status_ext_led=false;

void GPIO_Helpers_Set(gpio_out_s *state_gpio)
{
	/*
	 *
	 * #define STATUS_Pin GPIO_PIN_7
		#define STATUS_GPIO_Port GPIOA
		#define GPIO_LED_Pin GPIO_PIN_10
		#define GPIO_LED_GPIO_Port GPIOB
		#define GPIO_M_Pin GPIO_PIN_11
		#define GPIO_M_GPIO_Port GPIOB
		#define CTRL_R_Pin GPIO_PIN_15
		#define CTRL_R_GPIO_Port GPIOB
		#define RE_Pin GPIO_PIN_15
		#define RE_GPIO_Port GPIOA
		#define CAN_ST_Pin GPIO_PIN_7
		#define CAN_ST_GPIO_Port GPIOB
	 *
	 *
	 */
	HAL_GPIO_WritePin(CTRL_R_GPIO_Port, CTRL_R_Pin, state_gpio->fields.CTRL_R);
	HAL_GPIO_WritePin(GPIO_M_GPIO_Port, GPIO_M_Pin, state_gpio->fields.CTRL_M);
	HAL_GPIO_WritePin(GPIO_LED_GPIO_Port, GPIO_LED_Pin, state_gpio->fields.CTRL_LED);
	HAL_GPIO_WritePin(GPIO_ENA_GPIO_Port, GPIO_ENA_Pin, state_gpio->fields.ENA);
	HAL_GPIO_WritePin(GPIO_ENB_GPIO_Port, GPIO_ENB_Pin, state_gpio->fields.ENB);
}

void Set_Enable_DRV(bool status)
{
	_model_state._gpio_state.fields.ENA=status;
	_model_state._gpio_state.fields.ENB=status;
	HAL_GPIO_WritePin(GPIO_ENA_GPIO_Port,
			GPIO_ENA_Pin,
			_model_state._gpio_state.fields.ENA);

	HAL_GPIO_WritePin(GPIO_ENB_GPIO_Port,
			GPIO_ENB_Pin,
			_model_state._gpio_state.fields.ENB);
}


void Set_status_External_LED(bool status)
{
	status==true?(_model_state._gpio_state.fields.CTRL_R=1):(_model_state._gpio_state.fields.CTRL_R=0);
	HAL_GPIO_WritePin(CTRL_R_GPIO_Port,CTRL_R_Pin,_model_state._gpio_state.fields.CTRL_R);
}


void indicate_Extern_Status(uint8_t count_ToogleLed)
{
	if(count_ToogleLed==0)
	{
		Set_status_External_LED(true);
		return;
	}
	count_saveToogle_Led=count_ToogleLed;
	if(count_external_Led>=count_saveToogle_Led)
	{
		CNT_LED++;
	}
	if(CNT_LED>4)
	{
		CNT_LED=0;
		count_external_Led=0;
		return;
	}
	iter++;
	if(iter>1000)
	{
		status_ext_led=!status_ext_led;
		iter=0;
		Set_status_External_LED(status_ext_led);
		count_external_Led++;
	}
}


void Update_Pwm_Hrtim(pwm_value_s *_pwm)
{
	HRTIM_Set_Duty(ChannelA,_pwm->dutyA);
	HRTIM_Set_Duty(ChannelB,_pwm->dutyB);
}


void Read_state_Gen(void)
{
	_model_state._gpio_in_state.fields.GEN_STATUS= HAL_GPIO_ReadPin(ON_GEN_GPIO_Port, ON_GEN_Pin);
	(_model_state._gpio_in_state.fields.GEN_STATUS)?(_model_state._adc_value2._data.fields.extern_in_D=12600):
													(_model_state._adc_value2._data.fields.extern_in_D=200);
}


