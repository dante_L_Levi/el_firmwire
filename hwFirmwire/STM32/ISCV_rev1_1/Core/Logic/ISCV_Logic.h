/*
 * ISCV_Logic.h
 *
 *  Created on: Apr 17, 2022
 *      Author: Lepatenko
 */

#ifndef LOGIC_ISCV_LOGIC_H_
#define LOGIC_ISCV_LOGIC_H_

#include "Headers.h"
#include "PID_PWR.h"
#include "PI_regulator.h"
#include "PID_regulator.h"


void Main_Init(void);
uint8_t Get_Status_Trigger(void);

void Reset_Trigger(void);

/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);

void ISCV_Sync(void);


#endif /* LOGIC_ISCV_LOGIC_H_ */
