/*
 * ISCV_types.h
 *
 *  Created on: Apr 17, 2022
 *      Author: Lepatenko
 */

#ifndef LOGIC_ISCV_TYPES_H_
#define LOGIC_ISCV_TYPES_H_

#include "Headers.h"
#include "ISCV_Logic.h"


#define Voltage_INDEX_PID		0
#define Current_INDEX_PID		1

typedef enum
{
	//todo add STATUS INIT
	STATUS_INIT,
	STATUS_NORMAL,
	STATUS_NORMAL_GEN_ON,
	STATUS_NORMAL_GEN_OFF,
	STATUS_NORMAL_Charge_GEN_OFF,//перекачка энергии из GB1 в GB2
	STATUS_TEST,

	STATUS_ERROR_Input_Voltage,//необходимо обслуживание GB1
	STATUS_ERROR_OUTPUT_Voltage,////необходимо обслуживание GB2
	STATUS_ERROR_Breakage_OUT_NET,//обрыв цепи заряда
	STATUS_ERROR_Short_OUT_NET,		//кз цепи заряда
	STATUS_ERROR_Short_GB1,//КЗ банок GB1
	STATUS_ERROR_Short_GB2,//КЗ банок GB2
	STATUS_ERROR_Low_Voltage_D,//низкое напряжение на клеме D
	STATUS_ERROR_Low_Input_Voltage,
	STATUS_ERROR_High_Input_Voltage,//высокое входное напряжение

	STATUS_ERROR
}status_work_Converter;

typedef enum
{
	CV_Work=0,
	CC_Work=1,
	None_Work=2


}status_type_Regulator;


#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	input_current;
		uint16_t 	output_current;
		uint16_t 	output_28V;
		uint16_t 	output_14V;

	}fields;
	uint16_t all_channel[4];

}analog1_hw_s;
#pragma pack(pop)

#pragma pack(push,1)
typedef union
{
	struct
	{
		int32_t 	input_current;
		int32_t 	output_current;
		int32_t 	output_28V;
		int32_t 	output_14V;

	}fields;
	int16_t all_channel[4];

}analog1_s;


typedef struct
{
	float Kp;
}Settings_ADC_def;


typedef struct
{
	analog1_s				_data;
	Settings_ADC_def		_settings[4];

}analog1_sense_s;
#pragma pack(pop)

#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	extern_in_D;
		uint16_t 	temperature_sense;


	}fields;
	uint16_t all_channel[2];

}analog2_hw_s;
#pragma pack(pop)

#pragma pack(push,1)
typedef union
{
	struct
	{
		uint16_t 	temperature_sense;
		uint16_t 	extern_in_D;

	}fields;
	uint16_t all_channel[2];

}analog2_s;
#pragma pack(pop)

typedef struct
{
	analog2_s				_data;
	Settings_ADC_def		_settings[2];

}analog2_sense_s;

#pragma pack(push,1)
typedef union
{
	struct
	{
		uint8_t CTRL_M:1;
		uint8_t CTRL_LED:1;
		uint8_t CTRL_R:1;
		uint8_t ENA:1;
		uint8_t ENB:1;
	}fields;
	uint8_t all;
}gpio_out_s;

typedef union
{
	struct
	{
		uint8_t GEN_STATUS:1;
		uint8_t Reserve:7;
	}fields;
	uint8_t all;
}gpio_in_s;

#pragma pack(pop)


#pragma pack(push,1)
typedef struct
{
	uint16_t outDAC;
}DAC_Value_Def;


typedef struct
{
	uint16_t 					Threshold_Protected;
	DAC_Value_Def				_DAC_Value;

}Current_Protected_def;
#pragma pack(pop)



#pragma pack(push,1)
typedef struct
{
	uint16_t dutyA;
	uint16_t dutyB;
}pwm_value_s;

typedef struct
{
	float GB1;
	float GB2;

	float Current_charge_GB2;

}push_pull_Battery_s;


#pragma pack(pop)


typedef union
{
	struct
	{
		float Kp;
		float Ki;
		float Kd;
		float Saturation_P;
		float Saturation_N;
		float MAX;
		float MIN;
		float REF;
	}fields;
	float AllData[8];


}Setting_Reg_PID;



typedef struct
{
	uint16_t ID_temp;
	uint8_t Name[8];
	uint8_t firmwire[8];

}Transmit_data1_Model_DataBoard;


typedef struct
{
	analog1_s 					_ainData1;
	analog2_s					_ainData2;
	gpio_out_s					_gpioState;
	Current_Protected_def		_current_DAC;
	pwm_value_s					_pwm_Value;
	push_pull_Battery_s			_Battery_State;
	uint8_t						_status;
	uint8_t						_status_regulator;



}Transmit_data2_Model_MainData;

typedef struct
{
	Setting_Reg_PID				DataSettingsPID[2];

}Transmit_data3_Model_Settings;


#pragma pack(push,1)
typedef union
{
	struct
	{
		uint8_t GB1_percent;
		uint8_t GB2_percent;
		uint8_t Status_work;
	};
	uint8_t fields[3];
}Can_Packet1_def;

typedef union
{
	struct
	{
		int16_t  GB1_voltage;
		int16_t GB2_voltage;
		int16_t Status_work;
	};
	int16_t fields[3];
}Can_Packet2_def;


typedef struct
{
	float 					error;
	float 					duty_res;
	float 					VoltageRef;
	float 					CurrentRef;
	float 					Current_threshold;
	PI_struct				V_regulator;
	PI_struct				I_regulator;

}push_pull_reg_fields;

typedef struct
{
	uint16_t 					DEVICE;



	status_work_Converter		status;
	status_type_Regulator		status_regulator;
	bool 						status_prot;
	bool 						RS485_Connect;


	DAC_Value_Def				_dac_value;


	analog1_sense_s				_adc_value1;
	analog2_sense_s				_adc_value2;

	analog1_hw_s				_adc1;
	analog2_hw_s				_adc2;


	gpio_out_s					_gpio_state;
	gpio_in_s					_gpio_in_state;
	pwm_value_s					_pwm_value;
	Current_Protected_def		_current_DAC;
	push_pull_Battery_s 		_state_Baterries;
	push_pull_reg_fields		_regulator;

	Setting_Reg_PID				_settings_dataPID[2];
	uint32_t 					step_work;

	uint8_t 					sync_trigger;

	uint32_t 					_time_ms;
	uint32_t 					_time_start_timeout;
	timeout_s					_State_Charge_GB1_toGB2;

	Can_Packet1_def				_tx_Packet1_can;
	Can_Packet2_def				_tx_Packet2_can;
	CAN_RxHeaderTypeDef			_rx_header;
	CAN_TxHeaderTypeDef			_tx_header;
	uint32_t 					Mail_Tx_Box1;
	uint32_t 					Mail_Tx_Box2;
	Transmit_data1_Model_DataBoard	_data1;
	Transmit_data2_Model_MainData	_data2;
	Transmit_data3_Model_Settings	_data3;


}work_state_model_s;
#pragma pack(pop)

#endif /* LOGIC_ISCV_TYPES_H_ */
