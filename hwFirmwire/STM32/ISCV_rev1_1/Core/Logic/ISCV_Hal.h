/*
 * ISCV_Hal.h
 *
 *  Created on: 20 апр. 2022 г.
 *      Author: Lepatenko
 */

#ifndef LOGIC_ISCV_HAL_H_
#define LOGIC_ISCV_HAL_H_

#include "Headers.h"
#include "ISCV_types.h"



void GPIO_Helpers_Set(gpio_out_s *state_gpio);
void Set_status_External_LED(bool status);
void Set_Enable_DRV(bool status);
void Update_Pwm_Hrtim(pwm_value_s *_pwm);
void Read_state_Gen(void);
void indicate_Extern_Status(uint8_t count_ToogleLed);
#endif /* LOGIC_ISCV_HAL_H_ */
