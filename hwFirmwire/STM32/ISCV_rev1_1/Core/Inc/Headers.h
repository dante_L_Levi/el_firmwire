/*
 * Headers.h
 *
 *  Created on: 1 дек. 2021 г.
 *      Author: Lepatenko
 */

#ifndef INC_HEADERS_H_
#define INC_HEADERS_H_


//#include "stm32f4xx_hal.h"
//#include "stm32f401xe.h"
#include "stm32f3xx_hal.h"
#include "stm32f334x8.h"


#include "adc.h"
#include "can.h"
#include "dac.h"
#include "hrtim.h"
#include "usart.h"
#include "gpio.h"
//#include "dma.h"


#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"


//#include "line_buffer.h"
//#include "ring_buffer.h"


#include "Led.h"
#include "RS485_Lib.h"
#include "Timers.h"
//#include "pwm_Lib_Helper.h"
#include "pwm_generate.h"
#include "ADC_Sense.h"
#include "ADC_DMA_regular.h"
#include "DAC_Transfer.h"
//#include "PID_PWR.h"
//#include "Flash_Work.h"
#include "Settings.h"
#include "HAL_Helpers.h"
#include "CAN_Lib.h"
#include "Timeout.h"
#include "Flash_Setting.h"

#include "ADC_Regular.h"

//#include "BLDC_Controller.h"

#include "ACS723LLCTR.h"


#endif /* INC_HEADERS_H_ */
