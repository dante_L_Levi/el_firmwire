/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Headers.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define STATUS_Pin GPIO_PIN_7
#define STATUS_GPIO_Port GPIOA
#define GPIO_LED_Pin GPIO_PIN_10
#define GPIO_LED_GPIO_Port GPIOB
#define GPIO_M_Pin GPIO_PIN_11
#define GPIO_M_GPIO_Port GPIOB
#define ON_GEN_Pin GPIO_PIN_14
#define ON_GEN_GPIO_Port GPIOB
#define CTRL_R_Pin GPIO_PIN_15
#define CTRL_R_GPIO_Port GPIOB
#define RE_Pin GPIO_PIN_15
#define RE_GPIO_Port GPIOA
#define CAN_ST_Pin GPIO_PIN_7
#define CAN_ST_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define GPIO_ENA_Pin GPIO_PIN_5
#define GPIO_ENA_GPIO_Port GPIOB

#define GPIO_ENB_Pin GPIO_PIN_6
#define GPIO_ENB_GPIO_Port GPIOB
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
