/*
 * CAN_Lib.h
 *
 *  Created on: Dec 25, 2021
 *      Author: Lepatenko
 */

#ifndef CAN_CAN_LIB_H_
#define CAN_CAN_LIB_H_

#include "Headers.h"




typedef void (*can_rx_callback)(uint8_t *pData,CAN_RxHeaderTypeDef *_setting);

typedef struct
{
	void (*InitFunctionSet)(void);
	void (*can_rx_callback)(uint8_t *pData,CAN_RxHeaderTypeDef *_setting);
	uint32_t filterIdHigh;
	uint32_t filterIdLow;
	uint32_t FilterMaskIdHigh;
	uint32_t FilterMaskIdLow;


}CAN_Configure_Def;

void CAN_Init(uint16_t prescaler,uint32_t Tb1,uint32_t Tb2,IRQn_Type typeRx);
void CAN_INit_HW(void *InitFunction);
void CAN_Set_Filter(
		uint32_t filterIdHigh,
		uint32_t filterIdLow,
		uint32_t FilterMaskIdHigh,
		uint32_t FilterMaskIdLow);

void Set_CAN_Rx_Config(CAN_RxHeaderTypeDef _setting);
void CAN_Transmit_Packet(uint8_t *data,CAN_TxHeaderTypeDef config,uint32_t MailBox);
void Set_RxPacket_CallBack(void* can_callback);
void CAN_Transmit_self_Test(void);



#endif /* CAN_CAN_LIB_H_ */
