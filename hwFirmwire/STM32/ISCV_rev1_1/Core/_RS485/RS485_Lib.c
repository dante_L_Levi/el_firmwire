/*
 * RS485_Lib.c
 *
 *  Created on: Nov 29, 2021
 *      Author: Lepatenko
 */

#include "RS485_Lib.h"

#define F334
#define RS485_PORT_EN					RE_GPIO_Port
#define RS485_PIN_EN					RE_Pin






#define RS485_PORT_UART					GPIOB
#define RS485_PIN_RX					GPIO_PIN_4
#define RS485_PIN_TX					GPIO_PIN_3
#define RS485_TRANSMIT HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_SET)
#define RS485_RECEIVE  HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_RESET)
#define RS485_PIN_EN_CLK			 __HAL_RCC_GPIOA_CLK_ENABLE()
#define CLOCK_EN_UART				__HAL_RCC_USART2_CLK_ENABLE()
#define PIN_TX_RX_CLK				__HAL_RCC_GPIOB_CLK_ENABLE()

static RS485_pack txPack;

static RS485_Sprotocol_Data_def rxMessage;
uint16_t ID_DEV=0x0000;

uint8_t IsMuted=0;
uint8_t IsDataAvalible=0;

#define	Serial_Obj		huart2
#define HW_HUNDLER_UART		USART2

uint8_t RX_BUFF[SIZE_RX_PACKET]={0};
uint8_t rbuff_temp[SIZE_RX_PACKET-4]={0};

const unsigned short Crc16Table[256] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
    0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
    0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
    0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
    0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
    0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
    0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
    0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
    0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
    0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
    0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
    0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
    0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
    0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
    0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
    0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
    0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
    0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
    0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
    0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
    0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
    0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
    0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
};


uint16_t crc16_CCITT(uint8_t * pcBlock, uint16_t len)
{
    uint16_t crc = 0xFFFF;

    while (len--)
        crc = (crc << 8) ^ Crc16Table[(crc >> 8) ^ *pcBlock++];

    return crc;
}






/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len);
/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);

static void RS485_RessiveData_Hundler(void);

int32_t (*transmitFunction)(uint8_t* data, int16_t length);




static void SProtocol_addTxMessageNoBlock(RS485_Sprotocol_Data_def *p);



 static int32_t nulTransmitFunction(uint8_t* data, int16_t length)
{
    return 0;
}

 void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
 {
	 if(huart==&Serial_Obj)
	 {
		 RS485_RessiveData_Hundler();
	 }
 }


/****************Interrupt USART*********************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart==&Serial_Obj)
		{
			RS485_RessiveData_Hundler();
		}
}




static uint8_t RS485_checkId()
{
	uint16_t temp_ID=(RX_BUFF[1]<<8)|(RX_BUFF[0]);
	if(temp_ID==ID_DEV)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

static void RS485_RessiveData_Hundler(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_RX_PACKET);
	if(RX_BUFF[3]==(uint8_t)BOOT_CMD)
	{
			IsMuted=1;
	}

	if(RS485_checkId())
	{
		if(crc16_CCITT(RX_BUFF,SIZE_RX_PACKET)==0)
		{
			if(RX_BUFF[3]==BOOT_CMD)
			{
				//go to boot
			}

			int j=0;
			for(int i=2;i<sizeof(rbuff_temp);i++)
			{
				rbuff_temp[j]=RX_BUFF[i];
				j++;
			}
			rxMessage.length=rbuff_temp[0];
			rxMessage.cmd=rbuff_temp[1];
			int k=2;
			for(int i=0;i<SIZE_RX_PAYLOAD;i++)
			{
				rxMessage.dataRxPayload[i]=rbuff_temp[k];
				k++;
			}
		    IsDataAvalible=1;
		}
	}

}


void RS485_Reciever_Helper(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_RX_PACKET);
}

void RS485_Self_Test(void)
{
	RS485_Sprotocol_Data_def dt;
	dt.cmd=0x01;
	dt.length=1;
	uint8_t data=0xFF;
	dt.data=&data;
	SProtocol_addTxMessageNoBlock(&dt);
}


/*******************Init Harsware*****************************/
static void UART_Init_HW(uint32_t Baud)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	 /* USART2 clock enable */
	CLOCK_EN_UART;

	PIN_TX_RX_CLK;
	    /**USART2 GPIO Configuration
	        PB3     ------> USART2_TX
	        PB4     ------> USART2_RX
	    */
	 GPIO_InitStruct.Pin = RS485_PIN_RX|RS485_PIN_TX;
	 GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	 GPIO_InitStruct.Pull = GPIO_NOPULL;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	 HAL_GPIO_Init(RS485_PORT_UART, &GPIO_InitStruct);

	 RS485_PIN_EN_CLK;
	/**RS485 ENABLE **/
	//PA1     ------> ENABLE
	 /*Configure GPIO pins : PA1  */
	 	GPIO_InitStruct.Pin = RS485_PIN_EN;
	 	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	 	GPIO_InitStruct.Pull = GPIO_NOPULL;
	 	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 	HAL_GPIO_Init(RS485_PORT_EN, &GPIO_InitStruct);

	Serial_Obj.Instance = HW_HUNDLER_UART;
	Serial_Obj.Init.BaudRate = Baud;
	Serial_Obj.Init.WordLength = UART_WORDLENGTH_8B;
	Serial_Obj.Init.StopBits = UART_STOPBITS_1;
	Serial_Obj.Init.Parity = UART_PARITY_NONE;
	Serial_Obj.Init.Mode = UART_MODE_TX_RX;
	Serial_Obj.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Serial_Obj.Init.OverSampling = UART_OVERSAMPLING_16;
	#ifdef F334
	Serial_Obj.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	Serial_Obj.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	#endif
	  if (HAL_UART_Init(&Serial_Obj) != HAL_OK)
	  {
	    //Error_Handler();
	  }

	  /* USART1 interrupt Init */
	      HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
	      HAL_NVIC_EnableIRQ(USART2_IRQn);
	    /* USER CODE BEGIN USART1_MspInit 1 */
}



void RS485_SProtocol_init(uint16_t id,uint32_t Baud,
             int32_t (*transmitFunctionSet)(uint8_t* data, int16_t length))
{
	UART_Init_HW(Baud);
	ID_DEV=id;
	if(transmitFunctionSet==0)
	{
		transmitFunction=nulTransmitFunction;
	}
	else
	{
		transmitFunction=transmitFunctionSet;
	}

}


int32_t RS485_dataTX(uint8_t *data,int16_t Length)
{
	RS485_TRANSMIT;
	HAL_UART_Transmit(&Serial_Obj, (uint8_t*)data, Length, 0x1000);
	RS485_RECEIVE;
	return 0;
}

static void SProtocol_addTxMessageNoBlock(RS485_Sprotocol_Data_def *p)
{
	txPack.ID=ID_DEV;
	txPack.cmd=p->cmd;
	txPack.len=p->length;
	if(p->length>0)
	{
		memcpy((void*)txPack.payload, p->data, p->length);
		txPack.crc=crc16_CCITT(txPack.dataPack,txPack.len+4);
		txPack.payload[txPack.len]=(txPack.crc>>8)&0xFF;
		txPack.payload[txPack.len+1]=txPack.crc&0xFF;
		transmitFunction((void*)txPack.dataPack, txPack.len+6);
	}
}


void SProtocol_addTxMessage(RS485_Sprotocol_Data_def *p)
{
	SProtocol_addTxMessageNoBlock(p);
}



/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
			uint16_t crc = 0xFFFF;
	       uint8_t i;

	       while (len--)
	       {
	           crc ^= *data++ << 8;

	           for (i = 0; i < 8; i++)
	               crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	       }
	       return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len)
{
	uint16_t crc = 0xFFFF;
	     uint8_t i;
	     uint16_t des_buf=(data[len-1]<<8)|data[len-2];
	     uint8_t len_dst=len-2;
	     while (len_dst--)
	     {
	         crc ^= *data++ << 8;

	         for (i = 0; i < 8; i++)
	             crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	     }
	     if(crc==des_buf)
	     {
	         return  true;
	     }
	     else
	     {
	        return  false;
	     }
}



void Get_RS485RxMessage(RS485_Sprotocol_Data_def *p)
{
	memcpy(p,&rxMessage,sizeof(RS485_Sprotocol_Data_def));
}


/*******************End Packet**************************/
uint8_t  RS485_rxPkgAvailable(void)
{
	return IsDataAvalible;
}

/*******************Set Status Available**************************/
void RS485_SetPkgAvailable(uint8_t st)
{
	IsDataAvalible=st;
}





