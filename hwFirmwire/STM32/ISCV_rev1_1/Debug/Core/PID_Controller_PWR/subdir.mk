################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/PID_Controller_PWR/PID_PWR.c 

OBJS += \
./Core/PID_Controller_PWR/PID_PWR.o 

C_DEPS += \
./Core/PID_Controller_PWR/PID_PWR.d 


# Each subdirectory must supply rules for building sources it contributes
Core/PID_Controller_PWR/%.o: ../Core/PID_Controller_PWR/%.c Core/PID_Controller_PWR/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F334x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -I../Core/_ADC -I../Core/_CAN -I../Core/_DAC -I../Core/_HRTIM_Lib -I../Core/_LEDs -I../Core/_RS485 -I../Core/Helpers -I../Core/_FLASH -I../Core/PID_Controller_PWR -I../Core/Logic -I../Core/Controller -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-PID_Controller_PWR

clean-Core-2f-PID_Controller_PWR:
	-$(RM) ./Core/PID_Controller_PWR/PID_PWR.d ./Core/PID_Controller_PWR/PID_PWR.o

.PHONY: clean-Core-2f-PID_Controller_PWR

