################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
OBJCOPY_BIN := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Core/Controller \
Core/Helpers \
Core/Logic \
Core/PID_Controller_PWR \
Core/Src \
Core/Startup \
Core/_ADC \
Core/_CAN \
Core/_DAC \
Core/_FLASH \
Core/_HRTIM_Lib \
Core/_LEDs \
Core/_RS485 \
Drivers/STM32F3xx_HAL_Driver/Src \

