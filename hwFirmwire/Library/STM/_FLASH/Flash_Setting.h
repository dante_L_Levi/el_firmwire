/*
 * Flash_Setting.h
 *
 *  Created on: Mar 22, 2022
 *      Author: Lepatenko
 */

#ifndef FLASH_FLASH_SETTING_H_
#define FLASH_FLASH_SETTING_H_

#include "Settings.h"
#include "stdint.h"
#include "stdbool.h"


void Set_Flash_Adress_Start(uint32_t Adress);
void WriteBoardID(uint32_t ADDR,uint32_t *data,uint8_t len);
uint32_t  ReadBoardID(uint32_t ADDR);
void FlashSettings_write(uint8_t* data_ptr, uint32_t len);
void FlashSettings_read(uint8_t*  data_ptr, uint32_t len);
uint8_t FlashSettings_isValid(uint32_t len);
void Erace_Sector(uint32_t ADDR);

#endif /* FLASH_FLASH_SETTING_H_ */
