/*
 * LPS25HB_Lib.h
 *
 *  Created on: Jan 23, 2022
 *      Author: Lepatenko
 */

#ifndef LPS25HB_LPS25HB_LIB_H_
#define LPS25HB_LPS25HB_LIB_H_

#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "math.h"


//------------------------------------------------
#define LPS25HB_ADDRESS     0xBA
//------------------------------------------------
#define	LPS25HB_WHO_AM_I_REG	0x0F
#define	LPS25HB_RES_CONF_REG	0x10
#define	LPS25HB_CTRL_REG1	0x20
//------------------------------------------------
#define	LPS25HB_WHO_AM_I	0xBD
//------------------------------------------------
#define	LPS25HB_PD_ACTIVE_MODE	0x80
#define	LPS25HB_PD_POWERDOWN_MODE	0x00
#define	LPS25HB_PD_MASK	0x80
//------------------------------------------------
#define	LPS25HB_ODR_ONE_SHOT	0x00
#define	LPS25HB_ODR_1HZ	0x10
#define	LPS25HB_ODR_7HZ	0x20
#define	LPS25HB_ODR_12_5HZ	0x30
#define	LPS25HB_ODR_25HZ	0x40
#define	LPS25HB_ODR_MASK	0x70
//------------------------------------------------
#define	LPS25HB_DIFF_EN_ENABLE	0x08
#define	LPS25HB_DIFF_EN_DISABLE	0x00
#define	LPS25HB_DIFF_EN_MASK	0x08
//------------------------------------------------
#define	LPS25HB_BDU_DISABLE	0x00
#define	LPS25HB_BDU_ENABLE	0x04
#define	LPS25HB_BDU_MASK	0x04
//------------------------------------------------
#define LPS25HB_SIM_3_WIRE	0x01
#define LPS25HB_SIM_4_WIRE	0x00
#define	LPS25HB_SIM_MASK	0x01
//------------------------------------------------
#define	LPS25HB_AVGP_8	0x00
#define	LPS25HB_AVGP_32	0x01
#define	LPS25HB_AVGP_128	0x02
#define	LPS25HB_AVGP_512	0x03
#define	LPS25HB_AVGT_8	0x00
#define	LPS25HB_AVGT_16	0x04
#define	LPS25HB_AVGT_32	0x08
#define	LPS25HB_AVGT_64	0x0C
#define	LPS25HB_AVGT_MASK	0x0C
#define	LPS25HB_AVGP_MASK	0x03
//------------------------------------------------
#define	LPS25HB_TEMP_OUT_L_REG	0x2B
#define	LPS25HB_TEMP_OUT_H_REG	0x2C
//------------------------------------------------
#define	LPS25HB_PRESS_OUT_XL_REG	0x28
#define	LPS25HB_PRESS_OUT_L_REG	0x29
#define	LPS25HB_PRESS_OUT_H_REG	0x2A
//------------------------------------------------

#pragma pack(push, 1)

typedef struct
{
	int32_t Pressure;

}LPS25_Pressure_Def;

typedef struct
{
	LPS25_Pressure_Def	_Pressure;
	int32_t temperature;

	uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Register);
	void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t Register, uint8_t data);
	void (*InitFunctionSet)(void);

	float Sensivity_Magn;


}LPS25_data_Def;

#pragma pack(pop)

void Set_CallBack_Read_Data_LPS25(uint8_t (*RecieveFunctionSet)(uint16_t DeviceAddr, uint8_t Reg));
void Set_CallBack_Transmit_Data_LPS25(void (*TransmitFunctionSet)(uint16_t DeviceAddr,uint8_t reg, uint8_t data));



/*******************Init I2C*******************/
void Init_HW_LPS25(void* callbackInit);
/*************************Check ID Device*******************/
uint8_t check_LPS25_ID(void);
/*********************Get LPS25******************************/
int32_t LPS25_GetPressure(void);
int32_t LS25_getTemperature(void);
/***********************Peripheral Init LPS25****************/
void LPS25_Peripheral_Settings(void);








#endif /* LPS25HB_LPS25HB_LIB_H_ */
