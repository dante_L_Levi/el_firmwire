/*
 * Timers.h
 *
 *  Created on: 29 нояб. 2021 г.
 *      Author: Lepatenko
 */

#ifndef HELPERS_TIMERS_H_
#define HELPERS_TIMERS_H_


#include "Headers.h"



typedef enum
{
	Sync_10Hz=200,
	Sync_100Hz=20,
	Sync_200Hz=10,
	Sync_500Hz=4,
	Sync_1000Hz=2,
	Sync_2000Hz=11,
	Sync_4000Hz=50


}Sync_Value_Tim;




void Init_Tim6_Sync(Sync_Value_Tim freq);
void Init_Tim7_Sync(Sync_Value_Tim freq);
void Init_Tim7_One_ms(void);

int8_t Set_CallBack(uint8_t num,void * callback);

void Init_Timer_For_Sync(uint8_t tim,Sync_Value_Tim sync);


#endif /* HELPERS_TIMERS_H_ */
