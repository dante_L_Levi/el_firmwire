/*
 * ADC_Regular.h
 *
 *  Created on: Mar 8, 2022
 *      Author: Lepatenko
 */

#ifndef ADC_ADC_REGULAR_H_
#define ADC_ADC_REGULAR_H_

#include "Headers.h"



void ADC_Select_Channel(uint8_t channel,uint16_t *data_code);

#endif /* ADC_ADC_REGULAR_H_ */
