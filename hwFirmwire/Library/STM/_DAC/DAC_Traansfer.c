/*
 * DAC_Traansfer.c
 *
 *  Created on: Nov 30, 2021
 *      Author: Lepatenko
 */


#include "DAC_Transfer.h"

#define DAC_PORT_OUT1		GPIOA
#define DAC_PIN				GPIO_PIN_4

#define RCC_DAC_INIT		 __HAL_RCC_DAC1_CLK_ENABLE()



static void GPIO_Init_DAC(void)
{
	 GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = DAC_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DAC_PORT_OUT1, &GPIO_InitStruct);

}



void DAC_Init(void)
{
	 __HAL_RCC_DAC1_CLK_ENABLE();
	 GPIO_Init_DAC();

}

/*******************SET value DAC***********************/
void Set_DAC_Value(uint16_t data,uint8_t ch)
{
	if(data<=4095)
	{
		switch(ch)
		{
			case DAC_CHANNEL1:
			{
				//загружаем 12 битные данные в регистр с правым выравниванием
				DAC->DHR12R1 = data;
				break;
			}

			case DAC_CHANNEL2:
			{
				//загружаем 12 битные данные в регистр с правым выравниванием
				DAC->DHR12R2 = data;
				break;
			}
		}
	}


}


/********************ENABLE DAC******************/
void DAC_ENABLE(uint8_t ch)
{
	ch==DAC_CHANNEL2?(DAC->CR |= DAC_CR_EN2):(DAC->CR |= DAC_CR_EN1);

}

/********************DISABLE DAC******************/
void DAC_DISABLE(uint8_t ch)
{
	ch==DAC_CHANNEL2?(DAC->CR |= DAC_CR_EN2):(DAC->CR |= DAC_CR_EN1);
}



