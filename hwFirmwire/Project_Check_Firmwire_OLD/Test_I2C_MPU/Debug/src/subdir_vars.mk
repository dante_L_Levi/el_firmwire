################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/BaseLogic.c \
../src/MPU6050_I2C.c \
../src/main.c 

C_DEPS += \
./src/BaseLogic.d \
./src/MPU6050_I2C.d \
./src/main.d 

OBJS += \
./src/BaseLogic.obj \
./src/MPU6050_I2C.obj \
./src/main.obj 

OBJS__QUOTED += \
"src\BaseLogic.obj" \
"src\MPU6050_I2C.obj" \
"src\main.obj" 

C_DEPS__QUOTED += \
"src\BaseLogic.d" \
"src\MPU6050_I2C.d" \
"src\main.d" 

C_SRCS__QUOTED += \
"../src/BaseLogic.c" \
"../src/MPU6050_I2C.c" \
"../src/main.c" 


