/*
 * LoRa_E32_868T20D.c
 *
 *  Created on: Jul 28, 2021
 *      Author: AlexPirs
 */

#include "LoRa_E32_868T20D.h"


#define CLOCK_EN_UART				__HAL_RCC_USART1_CLK_ENABLE()
#define PIN_TX_RX_CLK				__HAL_RCC_GPIOA_CLK_ENABLE()

#define CLOCK_M0_M1()					__HAL_RCC_GPIOC_CLK_ENABLE()

#define HUART_Hundler					huart1
#define UART_HW							USART1
#define PIN_USART_TX					GPIO_PIN_9
#define PIN_USART_RX					GPIO_PIN_10
#define PORT_UART_PIN					GPIOA
#define Alternative_MACROS				GPIO_AF7_USART1
#define IRQ_Hundler_LoRa				USART1_IRQn
#define SIZE_PACKET						sizeof(LoRa_Protocol_data_Def)
uint8_t TX_BUFF[SIZE_PACKET];
uint8_t RX_BUFF[SIZE_PACKET];


LoRa_Protocol_data_Def   				_dataMain_Transmit;
LoRa_Protocol_data_Def   				_dataMain_Reciever;
LoRa_Config_Reg							_configData;
uint8_t IsDataAvalible_LoRa=0;
uint16_t I_DEVICE;


static void Lora_SetMode(Operation_Mode_Lora_def status);
static void Init_GPIO_LoRa(void);
static void Init_Uart_LoRa(uint32_t Baud);
static void LoRa_Send_Command(LoRa_Config_Reg _configReg);
/******************Check ID**********************/
static uint8_t LoRa_checkId(void);

/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len);





static void Init_GPIO_LoRa(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO Ports Clock Enable */
		CLOCK_M0_M1();


	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(M1_GPIO_Port, M1_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(M0_GPIO_Port, M0_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(AUX_GPIO_Port, AUX_Pin, GPIO_PIN_RESET);




	  GPIO_InitStruct.Pin = M1_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M1_GPIO_Port, &GPIO_InitStruct);

	  GPIO_InitStruct.Pin = M0_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M0_GPIO_Port, &GPIO_InitStruct);

	  GPIO_InitStruct.Pin = AUX_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  //GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(AUX_GPIO_Port, &GPIO_InitStruct);


}



static void Init_Uart_LoRa(uint32_t Baud)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
		PIN_TX_RX_CLK;
		CLOCK_EN_UART;
	    /**USART1 GPIO Configuration
	    PA9     ------> USART1_TX
	    PA10     ------> USART1_RX
	    */
	    GPIO_InitStruct.Pin = PIN_USART_TX|PIN_USART_RX;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	    GPIO_InitStruct.Alternate = Alternative_MACROS;
	    HAL_GPIO_Init(PORT_UART_PIN, &GPIO_InitStruct);




	HUART_Hundler.Instance = UART_HW;
	HUART_Hundler.Init.BaudRate = Baud;
	HUART_Hundler.Init.WordLength = UART_WORDLENGTH_8B;
	HUART_Hundler.Init.StopBits = UART_STOPBITS_1;
	HUART_Hundler.Init.Parity = UART_PARITY_NONE;
	HUART_Hundler.Init.Mode = UART_MODE_TX_RX;
	HUART_Hundler.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	HUART_Hundler.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&HUART_Hundler) != HAL_OK)
	{

	}

	/* USART1 interrupt Init */
	HAL_NVIC_SetPriority(IRQ_Hundler_LoRa, 0, 0);
	HAL_NVIC_EnableIRQ(IRQ_Hundler_LoRa);

}



static void Lora_SetMode(Operation_Mode_Lora_def status)
{
	switch(status)
	{
		case NORMAL_MODE:
		{
			SET_M0();
			SET_M1();
			break;
		}

		case WAKE_UP_MODE:
		{
			RESET_M0();
			SET_M1();
			break;
		}
		case POWER_SAVING_MODE:
		{
			SET_M0();
			RESET_M1();
			break;
		}
		case SLEEP_MODE:
		{
			RESET_M0();
			RESET_M1();
			break;
		}
	}
}




static uint8_t LoRa_checkId()
{
	uint16_t temp_ID=(RX_BUFF[1]<<8)|(RX_BUFF[0]);
	if(temp_ID==I_DEVICE)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

static void LoRa_Send_Command(LoRa_Config_Reg _configReg)
{

}


static void Init_Config_data(void)
{
	_configData.HEAD_REG=0xC0;
	_configData.AADH_REG=0x00;
	_configData.ADDL_REG=0x00;


}


/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
			uint16_t crc = 0xFFFF;
	       uint8_t i;

	       while (len--)
	       {
	           crc ^= *data++ << 8;

	           for (i = 0; i < 8; i++)
	               crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	       }
	       return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len)
{
	uint16_t crc = 0xFFFF;
	     uint8_t i;
	     uint16_t des_buf=(data[len-1]<<8)|data[len-2];
	     uint8_t len_dst=len-2;
	     while (len_dst--)
	     {
	         crc ^= *data++ << 8;

	         for (i = 0; i < 8; i++)
	             crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	     }
	     if(crc==des_buf)
	     {
	         return  true;
	     }
	     else
	     {
	        return  false;
	     }
}

/*********************Uart Reciever Helper for Recieve Buffer Uart****************/
void LoRa_Reciever_Helper(void)
{
	HAL_UART_Receive_IT(&HUART_Hundler, (uint8_t*)RX_BUFF, SIZE_PACKET);
	if(LoRa_checkId())
	{
		if(checkCRC16(RX_BUFF,SIZE_PACKET))
		{
			memcpy(&_dataMain_Reciever,(void*)RX_BUFF,sizeof(_dataMain_Reciever));
			IsDataAvalible_LoRa=1;
		}

	}
}

/*********************Uart Reciever Helper for Recieve Buffer Uart****************/
void LoRa_Helper_Recieve(void)
{
	HAL_UART_Receive_IT(&HUART_Hundler, (uint8_t*)RX_BUFF, SIZE_PACKET);
}


/****************************Transmit Uart Packet*****************************/
void LoRa_Transmit_Packet(uint8_t *data,uint8_t Length)
{

		SET_M0();
		SET_M1();
		 if(Length<=SizePAYLOAD)
		 {

			 memcpy(&(_dataMain_Transmit.Buffer),(void *)data,Length);
			 _dataMain_Transmit.crc=0x0000;
			 memcpy((void*)TX_BUFF,&_dataMain_Transmit,SIZE_PACKET);
			 uint16_t crc=Calculate_CRC16(TX_BUFF,SIZE_PACKET-2);
			 _dataMain_Transmit.crc=crc;
			 memcpy((void*)TX_BUFF,&_dataMain_Transmit,SIZE_PACKET);
			 HAL_UART_Transmit(&HUART_Hundler, (uint8_t*)TX_BUFF, SIZE_PACKET, 0x100);
		 }

}


/*******************Get Message**************************/
void Get_LoRa_RxMessage(LoRa_Protocol_data_Def* _dataGet)
{
	memcpy(_dataGet,&_dataMain_Reciever,sizeof(LoRa_Protocol_data_Def));

}



/**************Init Lora default************/
void LoRa_Init_Default(uint16_t _Id)
{
	_dataMain_Transmit._ID=_Id;
	I_DEVICE=_Id;
	Init_Config_data();
	Init_GPIO_LoRa();
	Init_Uart_LoRa(9600);
	Lora_SetMode(NORMAL_MODE);
}




/*************************************Init LoRa Set***************************************/
void Set_LoRa_Init(LoRa_UART_FORMAT_TYPE_Def _uart_format,
		LoRa_UART_BPS_TYPE_Def _uart_baud,
		LoRa_AIR_BPS_TYPE_Def _air_bps,AIR_CHAN_TYPE_Def _air_channel,
		Transmission_PWR_Def _pwr,
		FEC_Def _bitFit,Wireless_wake_up_time_def _time, Drive_Mode gpio,
		Fixed_Transm_en_bit _transmit,uint16_t _Id)
{
	Init_Config_data();
	Init_GPIO_LoRa();
	_dataMain_Transmit._ID=_Id;
	I_DEVICE=_Id;
	Lora_SetMode(SLEEP_MODE);

	_configData.SPED_REG=0x00;
		_configData.SPED_REG&=(uint8_t)_uart_format;//format parity
		_configData.SPED_REG&=(uint8_t)_uart_baud;//BaudRate
		_configData.SPED_REG&=(uint8_t)_air_bps;//Air data rate

		//config Baudrate

		//config Parity

	switch(_uart_baud)
	{
		case UART_BPS_1200:
		{
			Init_Uart_LoRa(1200);
			break;
		}

		case UART_BPS_2400:
		{
			Init_Uart_LoRa(2400);
			break;
		}

		case UART_BPS_4800:
		{
			Init_Uart_LoRa(4800);
			break;
		}

		case UART_BPS_9600:
		{
			Init_Uart_LoRa(9600);
			break;
		}

		case UART_BPS_19200:
		{
			Init_Uart_LoRa(19200);
			break;
		}

		case UART_BPS_38400:
		{
			Init_Uart_LoRa(38400);
			break;
		}

		case UART_BPS_57600:
		{
			Init_Uart_LoRa(57600);
			break;
		}

		case UART_BPS_115200:
		{
			Init_Uart_LoRa(115200);
			break;
		}

	}


	_configData.CHAN_REG=0x00;
	_configData.CHAN_REG|=(uint8_t)_air_channel;

	_configData.OPTION_REG=0x00;
	_configData.OPTION_REG&=_pwr;
	_configData.OPTION_REG&=_bitFit;
	_configData.OPTION_REG&=_time;
	_configData.OPTION_REG&=gpio;
	//config GPIO

	_configData.OPTION_REG&=_transmit;



	LoRa_Send_Command(_configData);


}


/*****************************Set Mode Work Lora*********************************/
void Set_LoRa_ModeWork(Operation_Mode_Lora_def _status)
{
	Init_GPIO_LoRa();
	Lora_SetMode(_status);
}
