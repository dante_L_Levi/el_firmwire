/*
 * LTC2602.h
 *
 *  Created on: 10 ���. 2019 �.
 *      Author: Gorudko
 */

#ifndef LTC2602_H_
#define LTC2602_H_

#include "stdint.h"

typedef void (*spi8_tx_t)(uint8_t * data, uint8_t len);
typedef void (*gpo_t)(uint8_t state);

typedef struct
{
    spi8_tx_t transfer;
    gpo_t cs;
} ltc2602_s;

void LTC2602_SetChannelA(ltc2602_s *self, uint16_t value);
void LTC2602_SetChannelB(ltc2602_s *self, uint16_t value);
void LTC2602_SetAllChannel(ltc2602_s *self, uint16_t value);
void LTC2602_PowerDown(ltc2602_s *self);

#endif /* LTC2602_H_ */
