/*
 * LIS3MDL.c
 *
 *  Created on: 31 дек. 2020 г.
 *      Author: AlexPirs
 */
#include "LIS3MDL.h"


#define LIS3MDL_I2C_hundler				hi2c1
#define LIS3MDL_CMSIS_HEADER			I2C1
#define I2C_GPIO_ENABLE					__HAL_RCC_GPIOB_CLK_ENABLE()
#define I2C_ENABLE						__HAL_RCC_I2C1_CLK_ENABLE()
#define PIN_SDA							GPIO_PIN_8
#define PIN_SCL							GPIO_PIN_9
#define PORT_I2C						GPIOB
#define INTERRUPT_EV					I2C1_EV_IRQn
#define INTERRUPT_ERR					I2C1_ER_IRQn


/*******************Init GPIO***********************/
static void I2C_GPIO_Init_LIS3MDL(void);
/*******************Init I2C HAL***********************/
static void Peripheral_I2C_LIS3MDL(void);
/***************************Write I2C Data in LSM6DS0****************************************/
static void LIS3MDL_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value);
/***************************Read I2C Data in LSM6DS0****************************************/
static uint8_t LIS3MDL_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr);

/*******************Init LIS3MDL*******************/
void Init_LIS3MDL(uint8_t Freq,uint8_t scale,
		uint8_t perfomenceModeXY,uint8_t temp_sensor)
{
	I2C_GPIO_Init_LIS3MDL();
	Peripheral_I2C_LIS3MDL();
	uint8_t key=check_LIS3MDL_ID();
		if(key==LIS3MDL_ID_DEV)
		{
			LIS3MDL_Peripheral_Settings(Freq,scale,perfomenceModeXY,temp_sensor);
		}

}


/*************************Check ID Device*******************/
uint8_t check_LIS3MDL_ID(void)
{
	 uint8_t ctrl = 0x00;
	 ctrl=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_WHO_AM_I_REG);
     return ctrl;
}

/*********************Get Gyro LSM6DS0******************************/
void LIS3MD_Magn_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
			uint8_t i=0;
			buffer[0]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTX_L);
		    buffer[1]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTX_H);
		    buffer[2]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTY_L);
		    buffer[3]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTY_H);
		    buffer[4]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTZ_L);
		    buffer[5]=LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_OUTZ_H);
		    for(i=0;i<3;i++)
		    {
		      Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
		    }
}





/*******************Init GPIO***********************/
static void I2C_GPIO_Init_LIS3MDL(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
		I2C_GPIO_ENABLE;
		    /**I2C1 GPIO Configuration
		    PB8     ------> I2C1_SCL
		    PB9     ------> I2C1_SDA
		    */
		    GPIO_InitStruct.Pin = PIN_SDA|PIN_SCL;
		    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		    GPIO_InitStruct.Pull = GPIO_PULLUP;
		    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
		    HAL_GPIO_Init(PORT_I2C, &GPIO_InitStruct);

		    /* I2C1 clock enable */
		    I2C_ENABLE;
}

/***********************Peripheral Init LIS3MDL****************/
void LIS3MDL_Peripheral_Settings(uint8_t Freq,uint8_t scale,
		uint8_t perfomenceModeXY,uint8_t temp_sensor)
{
	uint8_t value=0x00;
	//Sensor OFF
	value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3);
	value&=~LIS3MDL_MAG_MD_MASK;
	value|=LIS3MDL_MAG_MD_POWER_DOWN;
	LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3,value);
	//______________________________________________________________
	//Enable BDU Block Data Update
    value=0x00;
	value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG5);
    value&=~LIS3MDL_MAG_BDU_MASK;
    value|=LIS3MDL_MAG_BDU_ENABLE;
    LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG5,value);

	//______________________________________________________________
    //ON FREQ SCAN
    value=0x00;
    value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
     value&=~LIS3MDL_MAG_DO_MASK;
     value|=Freq;
     LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
     //______________________________________________________________
     //Full scale selection  gauss
     value=0x00;
     value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG2);
     value&=~LIS3MDL_MAG_FS_MASK;
     value|=scale;
     LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG2,value);
     //______________________________________________________________
     //ON HIGH perphomence Mode XY
     value=0x00;
     value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
     value&=~LIS3MDL_MAG_OM_MASK;
     value|=perfomenceModeXY;
     LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
     //______________________________________________________________
     //Sensor temperature
     value=0x00;
     value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1);
     value&=~temp_sensor;
     value|=LIS3MDL_MAG_OM_HIGH;
     LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG1,value);
     //______________________________________________________________
     //ON sensor (MD = 0x00)
     value = LIS3MDL_IO_Read(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3);
     value&=~LIS3MDL_MAG_MD_MASK;
     value|=LIS3MDL_MAG_MD_CONTINUOUS;
     LIS3MDL_IO_Write(MAG_I2C_ADDRESS,LIS3MDL_MAG_CTRL_REG3,value);

}

/*******************Init I2C HAL***********************/
static void Peripheral_I2C_LIS3MDL(void)
{
	LIS3MDL_I2C_hundler.Instance = LIS3MDL_CMSIS_HEADER;
	LIS3MDL_I2C_hundler.Init.Timing = 0x2000090E;
	LIS3MDL_I2C_hundler.Init.OwnAddress1 = 0;
	LIS3MDL_I2C_hundler.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	LIS3MDL_I2C_hundler.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	LIS3MDL_I2C_hundler.Init.OwnAddress2 = 0;
	LIS3MDL_I2C_hundler.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	LIS3MDL_I2C_hundler.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	LIS3MDL_I2C_hundler.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
		  if (HAL_I2C_Init(&LIS3MDL_I2C_hundler) != HAL_OK)
		  {
		   // Error_Handler();
		  }
		  /** Configure Analogue filter
		  */
		  if (HAL_I2CEx_ConfigAnalogFilter(&LIS3MDL_I2C_hundler, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
		  {

		  }
		  /** Configure Digital filter
		  */
		  if (HAL_I2CEx_ConfigDigitalFilter(&LIS3MDL_I2C_hundler, 0) != HAL_OK)
		  {

		  }

		  /* I2C1 interrupt Init */
		  HAL_NVIC_SetPriority(INTERRUPT_EV, 0, 0);
		  HAL_NVIC_EnableIRQ(INTERRUPT_EV);
		  HAL_NVIC_SetPriority(INTERRUPT_ERR, 0, 0);
		  HAL_NVIC_EnableIRQ(INTERRUPT_ERR);
		  	  /* USER CODE BEGIN I2C1_MspInit 1 */

}


/***************************Write I2C Data in LSM6DS0****************************************/
static void LIS3MDL_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value)
{
	HAL_StatusTypeDef status = HAL_OK;
    status = HAL_I2C_Mem_Write(&LIS3MDL_I2C_hundler, DeviceAddr, (uint16_t)RegisterAddr, I2C_MEMADD_SIZE_8BIT, &Value, 1, 0x10000);
    if(status != HAL_OK)
    	{
    		//Event Error
    	}
}

/***************************Read I2C Data in LSM6DS0****************************************/
static uint8_t LIS3MDL_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr)
{
	HAL_StatusTypeDef status = HAL_OK;
    uint8_t value = 0;
    status = HAL_I2C_Mem_Read(&LIS3MDL_I2C_hundler, DeviceAddr, RegisterAddr, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);
     if(status != HAL_OK)
     {
    	 //Event Error
     }
      return value;
}





