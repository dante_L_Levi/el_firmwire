/*
 * IMU_Logic.h
 *
 *  Created on: Jun 17, 2021
 *      Author: AlexPirs
 */

#ifndef INC_IMU_LOGIC_H_
#define INC_IMU_LOGIC_H_

#include "stm32f334x8.h"
#include "main.h"

#define GPIO_LED				GPIO_PIN_5
#define GPIO_PORT_OUT1_LED		GPIOA

#define LED_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_SET)
#define LED_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_RESET)



#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_Dev;
	int16_t Gyro[3];
	uint8_t Res;
}LSM6DS0_Gyro_DataDef;

#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_Dev;
	int16_t Accel[3];
	uint8_t Res;
}LSM6DS0_Accel_DataDef;

#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_Dev;
	int16_t Magn[3];
	uint8_t Res;
}LIS3DL_Magn_DataDef;

#pragma pack(pop)


#define AXES_X			0
#define AXES_Y			1
#define AXES_Z			2



typedef struct
{
	float Sensivity_Gyro;
	float Sensivity_Accel;
	float Sensivity_Magn;
	float Converted_Value;

	int16_t Kg_bias;
	int16_t Ka_bias;
	int16_t Km_bias;

	int16_t Gyro_Bias[3];
	int16_t Accel_Bias[3];
	int16_t Magn_Bias[3];

}Settings_Gyro_Accel_Magn;


typedef struct
{
	uint16_t 						_ID;
	Settings_Gyro_Accel_Magn 		_lsm6ds0_settings;
	uint8_t 						sync_trigger;
	uint8_t 						state_Ledx;
	uint32_t 						state_count_btn;
	bool 							EN_Transmit_Status;
	uint8_t 						count_transmit_index;

	LSM6DS0_Gyro_DataDef 			_gyro;
	LSM6DS0_Accel_DataDef 			_accel;
	LIS3DL_Magn_DataDef				_magn;


}work_state_model_s;


/********************Init Model && HARDWARE************/
void IMU_Init_Sync(void);

uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Get_Sync(void);


#endif /* INC_IMU_LOGIC_H_ */
