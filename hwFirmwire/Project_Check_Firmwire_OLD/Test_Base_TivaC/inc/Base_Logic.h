/*
 * Base_Logic.h
 *
 *  Created on: 21 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_BASE_LOGIC_H_
#define INC_BASE_LOGIC_H_

#include "main.h"


#define PORT_RGB    GPIO_PORTF_BASE
#define PIN_R       GPIO_PIN_1
#define PIN_G       GPIO_PIN_2
#define PIN_B       GPIO_PIN_3

#define PORT_INPUT  GPIO_PORTF_BASE
#define INPUT2      GPIO_PIN_0
#define INPUT1      GPIO_PIN_4

#define PORT_OUT    GPIO_PORTA_BASE
#define OUT1       GPIO_PIN_2
#define OUT2       GPIO_PIN_3

#define LED_R_ON()          GPIOPinWrite(PORT_RGB,PIN_R,0x02)
#define LED_R_OFF()         GPIOPinWrite(PORT_RGB,PIN_R,0x00)

#define LED_G_ON()          GPIOPinWrite(PORT_RGB,PIN_G,0x04)
#define LED_G_OFF()         GPIOPinWrite(PORT_RGB,PIN_G,0x00)


#define LED_B_ON()          GPIOPinWrite(PORT_RGB,PIN_B,0x08)
#define LED_B_OFF()         GPIOPinWrite(PORT_RGB,PIN_B,0x00)

#define OUT1_ON()          GPIOPinWrite(PORT_OUT,OUT1,0x01)
#define OUT1_OFF()         GPIOPinWrite(PORT_OUT,OUT1,0x00)

#define OUT2_ON()          GPIOPinWrite(PORT_OUT,OUT2,0x02)
#define OUT2_OFF()         GPIOPinWrite(PORT_OUT,OUT2,0x00)



#pragma pack(push, 1)
typedef union
{
 struct
 {
     uint8_t Out1:1;
     uint8_t Out2:1;
     uint8_t Out3:1;
     uint8_t Out4:1;
     uint8_t Out5:1;
     uint8_t Out6:1;
     uint8_t Out7:1;
     uint8_t Out8:1;


 }fields;
 uint8_t AllData;

}Digital_Output;


typedef union
{
 struct
 {
     uint8_t In1:1;
     uint8_t In2:1;
     uint8_t In3:1;
     uint8_t In4:1;
     uint8_t In5:1;
     uint8_t In6:1;
     uint8_t In7:1;
     uint8_t In8:1;


 }fields;
 uint8_t AllData;

}Digital_Input;




typedef struct
{

        uint16_t ADC_In1;
        uint16_t ADC_In2;
        uint16_t ADC_In3;
        uint16_t ADC_In4;


}Analog_Input;


typedef union
{
    struct
    {
        uint8_t flag_pwm_work:1;
        uint8_t res1:1;
        uint8_t res2:1;
        uint8_t res3:1;
        uint8_t res4:1;
        uint8_t res5:1;
        uint8_t res6:1;
        uint8_t RS485_Connect:1;
    };
    uint8_t Flag_All;
}Flag_Work;

typedef struct
{
    uint16_t pwm_val;
}PWM_Value;

#pragma pack(pop)




typedef struct
{
    uint16_t                        _ID;
    PWM_Value                       _pwmValue;
    Flag_Work                       _statusFlag;
    Analog_Input                    _adc_values;
    Digital_Output                  _GPIO_Output;
    Digital_Input                   _GPIO_Input;

    uint8_t                         sync_trigger;
}work_state_model_s;



/********************Init Peripheral*****************/
void Main_Init_BaseLogic(void);
/***********************PWM Enable *******************/
void PWM_Enable(void);
/***********************PWM Disable *******************/
void PWM_Disable(void);
/*****************Set PWM Value***************/
void PWM_SetDuty(uint16_t A);


uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Get_Sync(void);





#endif /* INC_BASE_LOGIC_H_ */
