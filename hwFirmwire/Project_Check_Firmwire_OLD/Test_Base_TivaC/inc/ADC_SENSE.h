/*
 * ADC_SENSE.h
 *
 *  Created on: 22 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_ADC_SENSE_H_
#define INC_ADC_SENSE_H_

#include "main.h"


#define ADC_AIN0        GPIO_PIN_3
#define ADC_AIN1        GPIO_PIN_2
#define ADC_AIN2        GPIO_PIN_1


#define INDEX_AIN0      0
#define INDEX_AIN1      1
#define INDEX_AIN2      2

#define NUM_CHANNEL     3

#define VREF         3.3/4095





/*****************Analog Init*******************/
void AnalogSens_Init(void);
/***********************Read Buffer ADC******************/
void Read_ADC(uint32_t *buffadc);


#endif /* INC_ADC_SENSE_H_ */
