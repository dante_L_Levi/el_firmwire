/*
 * Logic.c
 *
 *  Created on: Jul 31, 2021
 *      Author: AlexPirs
 */

#include "Logic.h"
#include <stdio.h>
#include <stdlib.h>



#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7



#define PRC_State_Sync_TIM			32-1
#define ARR_State_SYNC_TIM			250

#define PSR_TIM_Indicate				31999
#define ARR_TIM_Indicate				200
#define ARR_TIM_Indicate_RS_CONN		800

#define LED_CLOCK_GPIO_LED1				__HAL_RCC_GPIOC_CLK_ENABLE();
#define LED_CLOCK_GPIO_LED2				__HAL_RCC_GPIOC_CLK_ENABLE();


work_state_model_s _model_state;
const uint16_t Dev=0x09;
uint64_t TxpipeAddrs = 0x11223344AA;



void TIM6_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;

			_model_state.state_Ledx++;

}


void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;

}


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=Dev;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
	_model_state._dataMain.ID_Dev=0x08;
	_model_state._dataMain.ADC_Value[0]=2048;
	_model_state._dataMain.ADC_Value[1]=4095;
	_model_state._dataMain.ADC_Value[2]=990;
	_model_state._dataMain.ADC_Value[3]=1024;


}

static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	LED_CLOCK_GPIO_LED1;
	LED_CLOCK_GPIO_LED2;
		 /*Configure GPIO pin Output Level */
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

		   /*Configure GPIO pins : PC9 */
		GPIO_InitStruct.Pin = LED1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);


		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
		 /*Configure GPIO pins : PC8 */
		GPIO_InitStruct.Pin = LED2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED2_GPIO_Port, &GPIO_InitStruct);

}


/******************Init Timer for Indicate******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=PSR_TIM_Indicate;
	work_state_tim->ARR=ARR_TIM_Indicate;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_IRQn);
}


/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}


/********************Read Data Sync************/
void Data_Get_Sync(void)
{
	for(int i=0;i<4;i++)
	{
		_model_state._dataMain.ADC_Value[i]=rand()%10000;
	}
	_model_state._dataMain.Res=rand()%255;
	uint8_t DataBuffer[sizeof(_model_state._dataMain)]={0};
	uint8_t SizeBuf=sizeof(_model_state._dataMain);
	memcpy(DataBuffer,&_model_state._dataMain,sizeof(_model_state._dataMain));
	if(NRF24_write(DataBuffer, SizeBuf))
	{
		//NRF24_read(AckPayload, 32);
		HAL_UART_Transmit(&huart1, (uint8_t *)"Transmitted Successfully\r\n", strlen("Transmitted Successfully\r\n"), 10);

					//char myDataack[80];
					//sprintf(myDataack, "AckPayload:  %s \r\n", AckPayload);
					//HAL_UART_Transmit(&huart2, (uint8_t *)myDataack, strlen(myDataack), 10);
	}



}


/*******************Main Init ************************/
void Init_Logic(void)
{
	GPIO_INit();
	Init_Timer_StateWork();
	NRF24_begin();
	nrf24_DebugUART_Init();
	printRadioSettings();
	//**** TRANSMIT - ACK ****//
	NRF24_stopListening();
	NRF24_openWritingPipe(TxpipeAddrs);
	NRF24_setAutoAck(true);
	NRF24_setChannel(52);
	NRF24_setPayloadSize(32);

	NRF24_enableDynamicPayloads();
	NRF24_enableAckPayload();



	Init_Sync_Tim();
	Init_Model_Work();

}


uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/******************Indicate work MCU*****************/
void Indicate_Led_MCU(void)
{
	if(_model_state.state_Ledx%2==0)
		{
			LED1_ON();
			LED2_OFF();
		}
		else
		{
			LED1_OFF();
			LED2_ON();

		}
}

