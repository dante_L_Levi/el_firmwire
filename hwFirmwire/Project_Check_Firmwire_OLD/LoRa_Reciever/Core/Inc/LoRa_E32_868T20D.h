/*
 * LoRa_E32_868T20D.h
 *
 *  Created on: Jul 28, 2021
 *      Author: AlexPirs
 */

#ifndef INC_LORA_E32_868T20D_H_
#define INC_LORA_E32_868T20D_H_


#include "main.h"


typedef enum
{
	NORMAL_MODE=0,
	WAKE_UP_MODE,
	POWER_SAVING_MODE,
	SLEEP_MODE

}Operation_Mode_Lora_def;


typedef enum
{
  UART_FORMAT_8N1 = 0x3F,  /*no   parity bit one stop*/
  UART_FORMAT_8O1 = 0x7F,  /*odd  parity bit one stop*/
  UART_FORMAT_8E1 = 0xBF   /*even parity bitone stop*/
}LoRa_UART_FORMAT_TYPE_Def;


typedef enum
{
  UART_BPS_1200 = 0xC7,
  UART_BPS_2400 = 0xCF,
  UART_BPS_4800 = 0xD7,
  UART_BPS_9600 = 0xDF,
  UART_BPS_19200 = 0xE7,
  UART_BPS_38400 = 0xEF,
  UART_BPS_57600 = 0xF7,
  UART_BPS_115200 = 0xFF
}LoRa_UART_BPS_TYPE_Def;


typedef enum
{
	AIRT_BPS_300 = 0xF8,
	AIR_BPS_1200 = 0xF9,
	AIR_BPS_2400 = 0xFA,
	AIR_BPS_4800 = 0xFB,
	AIR_BPS_9600 = 0xFC,
	AIR_BPS_19200 = 0xFD

}LoRa_AIR_BPS_TYPE_Def;


//410~441MHz : 410M + CHAN*1M
typedef enum
{
  AIR_CHAN_410M = 0x00,
  AIR_CHAN_433M = 0x17,
  AIR_CHAN_441M = 0x1F
}AIR_CHAN_TYPE_Def;





typedef enum
{
	dBm30_TRAN_PWR=0xFF,
	dBm27_TRAN_PWR=0xFD,
	dBm24_TRAN_PWR=0xFE,
	dBm21_TRAN_PWR=0xFC


}Transmission_PWR_Def;


typedef enum
{
	TURN_off_FEC=0xFB,
	TURN_on_FEC=0xFF

}FEC_Def;


typedef enum
{
	time_250ms=0xC7,
	time_500ms=0xCF,
	time_750ms=0xD7,
	time_1000ms=0xDF,
	time_1250ms=0xE7,
	time_1500ms=0xEF,
	time_1750ms=0xF7,
	time_2000ms=0xFF

}Wireless_wake_up_time_def;

typedef enum
{
	push_pull_uart=0xFF,
	open_collector_uart=0xBF

}Drive_Mode;


typedef enum
{
	TRANSPARENT_trans_mode=0xFF,
	FIXED_trans_mode=0x7F

}Fixed_Transm_en_bit;


#define SizePAYLOAD		54


#pragma pack(push,1)
typedef struct
{
	uint16_t _ID;
	uint8_t Buffer[SizePAYLOAD];
	uint16_t crc;
}LoRa_Protocol_data_Def;



typedef struct
{
	uint8_t HEAD_REG;
	uint8_t AADH_REG;
	uint8_t ADDL_REG;
	uint8_t SPED_REG;
	uint8_t CHAN_REG;
	uint8_t OPTION_REG;

}LoRa_Config_Reg;



#pragma pack(pop)





#define M1_Pin 						GPIO_PIN_8
#define M0_Pin 						GPIO_PIN_9
#define AUX_Pin 					GPIO_PIN_7

#define M1_GPIO_Port 				GPIOC
#define M0_GPIO_Port 				GPIOC
#define AUX_GPIO_Port 				GPIOC

#define SET_M0()			HAL_GPIO_WritePin(M0_GPIO_Port,M0_Pin,GPIO_PIN_SET)
#define RESET_M0()			HAL_GPIO_WritePin(M0_GPIO_Port,M0_Pin,GPIO_PIN_RESET)

#define SET_M1()			HAL_GPIO_WritePin(M1_GPIO_Port,M1_Pin,GPIO_PIN_SET)
#define RESET_M1()			HAL_GPIO_WritePin(M1_GPIO_Port,M1_Pin,GPIO_PIN_RESET)






/**************Init Lora default************/
void LoRa_Init_Default(uint16_t _Id);
/*************************************Init LoRa Set***************************************/
void Set_LoRa_Init(LoRa_UART_FORMAT_TYPE_Def _uart_format,
		LoRa_UART_BPS_TYPE_Def _uart_baud,
		LoRa_AIR_BPS_TYPE_Def _air_bps,AIR_CHAN_TYPE_Def _air_channel,
		Transmission_PWR_Def _pwr,
		FEC_Def _bitFit,Wireless_wake_up_time_def _time, Drive_Mode gpio,
		Fixed_Transm_en_bit _transmit,uint16_t _Id);


/*****************************Set Mode Work Lora*********************************/
void Set_LoRa_ModeWork(Operation_Mode_Lora_def _status);

/****************************Transmit Uart Packet*****************************/
void LoRa_Transmit_Packet(uint8_t *data,uint8_t Length);
/*********************Uart Reciever Helper for Recieve Buffer Uart****************/
void LoRa_Reciever_Helper(void);
/*********************Uart Reciever Helper for Recieve Buffer Uart****************/
void LoRa_Helper_Recieve(void);
/*******************Get Message**************************/
void Get_LoRa_RxMessage(LoRa_Protocol_data_Def* _dataGet);

#endif /* INC_LORA_E32_868T20D_H_ */
